#from flask import (Blueprint, send_from_directory, abort, request,render_template, current_app, render_template, redirect,url_for, current_app)
from app import app
from flask import request,render_template,redirect,url_for
from flask_login import login_required
from app.auth.auth_models import User,db

#@app.route('/')
#def index_katalyo():
#    return 'Hello, Flask Katalyo!<br><a href="/login">Go to login page</a>'

@app.route('/home',methods=["GET","POST"])
@login_required
def home():
    """For POSTS, create User table"""
    msgs=[]
    if request.method=="POST":
        with app.app_context():
            db.metadata.create_all(bind=db.engine,checkfirst=True,tables=[db.metadata.tables['user']])
        msgs.append("User table created!")
    return render_template("home.html",messages=msgs)