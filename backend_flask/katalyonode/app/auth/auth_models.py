from flask_sqlalchemy import SQLAlchemy
from app import app

db = SQLAlchemy()

db.init_app(app)

class User(db.Model):
    """User table for storing application users
    :param int id: primary key
    :param str username: unique username of the user
    :param str email: email address of user
    :param str first_name: first name  of user
    :param str last_name: last name of the user
    :param str password: encrypted password for the user
    """
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    username = db.Column(db.String,unique=True)
    email = db.Column(db.String)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    password = db.Column(db.String)
    authenticated = db.Column(db.Boolean, default=False)
    active = db.Column(db.Boolean, default=True)
    
    def is_active(self):
        """Return True if the user is active."""
        return self.active

    def get_id(self):
        """Return id to satify Flask-Login's requirements."""
        return self.id

    def is_authenticated(self):
        """Return True if the user is authenticated."""
        return self.authenticated

    def is_anonymous(self):
        """False, as anonymous users aren't supported."""
        return False

class Group(db.Model):
    """Groups definition
    :param str name: name of the group
    :param User CreatedBy: user that created the group
    """
    __tablename__ = 'group'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String,unique=True)
    #createdby = db.Column(db.ForeignKey)
    #owners = db.Column(db.ForeignKey)
