#from flask import (Blueprint, send_from_directory, abort, request,render_template, current_app, render_template, redirect,url_for, current_app)
from flask import request,render_template,redirect,url_for
from flask_bcrypt import Bcrypt
from flask_login import LoginManager,login_required, login_user, logout_user, current_user

from app import app
import pdb

from app.auth.auth_models import User,db

login_manager = LoginManager(app)
bcrypt = Bcrypt(app)

@login_manager.user_loader
def user_loader(user_id):
    """Given *user_id*, return the associated User object.
    :param unicode user_id: user_id (user_id) user to retrieve
    """
    return User.query.get(user_id)

@app.route('/',methods=["GET","POST"])
@app.route('/login',methods=["GET","POST"])
def login():
    """For GET requests, display the login form. For POSTS, login the current user by processing the form."""
    #print db
    if request.method=="POST":
        username = request.form["username"]
        password = request.form["password"]
        auth_errors=[]
        user_errors=[]
        pass_errors=[]
        #pdb.set_trace()
     
        if username=="":
            user_errors.append("Username is required")
        if password=="":
            pass_errors.append("Password is required")
        if len(user_errors)==0 and len(pass_errors)==0: 
        
            users = User.query.filter_by(username=username)
            #pdb.set_trace()
            if users.count()==1:
                user = users[0]
                if bcrypt.check_password_hash(user.password, password):
                    user.authenticated = True
                    db.session.add(user)
                    db.session.commit()
                    login_user(user, remember=True)
                    return redirect(url_for("home"))
            else:
                auth_errors.append("Login failed!")
                return render_template("login.html",user_errors=user_errors,pass_errors=pass_errors,auth_errors=auth_errors)
        else:
            auth_errors.append("Login failed!")
            return render_template("login.html",user_errors=user_errors,pass_errors=pass_errors,auth_errors=auth_errors)
    return render_template("login.html")


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    """Logout the current user."""
    user = current_user
    user.authenticated = False
    db.session.add(user)
    db.session.commit()
    logout_user()
    return render_template("logout.html")

@app.route('/createuser',methods=["GET","POST"])
@login_required
def create_user():
    """For POSTS, create new User"""
    msgs=[]
    if request.method=="POST":
        username = request.form["username"]
        password = request.form["password"]
        auth_errors=[]
        user_errors=[]
        pass_errors=[]
        #pdb.set_trace()

        if username=="":
            user_errors.append("Username is required")
        if password=="":
            pass_errors.append("Password is required")
        if len(user_errors)==0 and len(pass_errors)==0: 
            pw_hash = bcrypt.generate_password_hash(password).decode('utf-8')
            user = User(username=username,password=pw_hash)
            db.session.add(user)
            db.session.commit()
            msgs.append("user "+username+" created")
            return render_template("createuser.html",messages=msgs)
        else:
            auth_errors.append("Create user failed!")
            return render_template("createuser.html",user_errors=user_errors,pass_errors=pass_errors,auth_errors=auth_errors)
    return render_template("createuser.html")
    
   