"""Katalyo no-code backend engine"""
"""Initiation script"""
import os

from flask import Flask
from jinja2 import Environment, FileSystemLoader, ChoiceLoader, PackageLoader

app = Flask(__name__)
app.config['SECRET_KEY']  = 'fi_9%z)fgmpxya-&f6vpn9g(un9yyx4n0(512%0v1b)*=-82r7'
app.config['SQLALCHEMY_DATABASE_URI']="postgresql://postgres:Ivica123@localhost:5432/katalyodb"
app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False
#app.config.from_object('config') - ToDo - move configuration to config.py

from app import views
from app.auth import auth_views

#app.jinja_loader = ChoiceLoader([FileSystemLoader(os.path.join(os.getcwd(), 'templates')),PackageLoader('app'),])
#app.register_blueprint(app)
#app.register_blueprint(auth)

__version__ = '0.1.1'
