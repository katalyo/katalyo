'use strict';

export function ktDate () {

	var datePickerCtrl = ['$scope','$rootScope','GridsterService','CodesService','$compile','$element','$templateCache', function ($scope,$rootScope,GridsterService,CodesService,$compile,$element,$templateCache) {
        
      //if ($scope.fd.localVars===undefined)
       $scope.CTRL = $scope.CTRL  + ':ktdate'
       $scope.fd.localVars = {}        
       $scope[$scope.fd.id]  = {'fd': $scope.fd, 'localvars':$scope.fd.localVars}
       //$scope.fd['lvars'+$scope.fd.id] = $scope.fd.localVars
       $scope.gsrvc = GridsterService;

       if ($scope.fd.ParametersLang==undefined || $scope.fd.ParametersLang==null) $scope.fd.ParametersLang={};
        //console.log("u datepickerctrl");
		$scope.fd.isOpen=false;
        if ($scope.localVars==undefined) $scope.localVars={};
		$scope.fd.localVars.search = [
		{id:1,filter:'gt',code_name:'gte_date',name:"From date",icon:"fa fa-chevron-right"},
		{id:2,filter:'lt',code_name:'lte_date',name:"To date",icon:"fa fa-chevron-left"},
		{id:3,filter:'eq',code_name:'eq_date',name:"Exact date",icon:"fa fa-bars"},
		{id:4,filter:'between',code_name:'between_date',name:"Between <date1> and <date2>",icon:"fa fa-expand"}
    ];
	
  
            $scope.fd.localVars.processFilters = function(filterList)
                {
                    for (let i=0;i<$scope[$scope.fd.id]['fd'].localVars.search.length;i++)
                    {
                        let pos = filterList.map(function(e) { return e.id; }).indexOf($scope[$scope.fd.id]['fd'].localVars.search[i].code_name);
                        if (pos>=0) $scope[$scope.fd.id]['fd'].localVars.search[i].name = filterList[pos].name
                    }
                   
                    
                    if ($scope.fd.Search===undefined || $scope.fd.Search===null)   $scope.fd.Search=$scope[$scope.fd.id]['fd'].localVars.search[2];//initialize to equals
                    if ($scope.fd.Search)  $scope.fd.Search.Value=$scope.formData[0][$scope.fd.FieldName]; 

                    $scope[$scope.fd.id]['fd'].localVars.filterReady=true
                }

                if ($scope.showSearchFilters || $scope.resourceDefinition.form_definition)
                {
                  let lang=$rootScope.globals.currentUser.lang;
                    CodesService.getCodesByName('search_filters',lang).then(function (data) {
                                //debugger;
                                try {
                                 //$scope.fd.localVars.processFilters(data.data) // BUG TODO
                                 $scope[$scope.fd.id]['fd'].localVars.processFilters(data.data)
                                
                                } catch (err) {
                                    debugger
                                }
                                
                    });
                }
	let itemValue = null;
        if ($scope.formData!=undefined)
        {
                if ($scope.formData.length==0) $scope.formData=[{}];
                itemValue=$scope.formData[0][$scope.fd.FieldName]
                
        }else $scope.formData=[{}];
        
        let itemValueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+'-boolValueStateName'+$scope.fd.PresentationId,itemValue);
        let itemValueStateName = itemValueState.subject.name;
        
        
        let value_changed = function(data)
        {
            itemValueState.subject.setStateChanges = data;
            itemValueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(itemValueState,"valueChanged",value_changed);
       
        $scope.fd.on_change_action=function()
        {
                
                
                if ($scope.fd.Dbtype==="datefielddefinition" && $scope.formData[0][$scope.fd.FieldName]!==undefined && $scope.formData[0][$scope.fd.FieldName]!==null) {
                    
                    $scope.formData[0][$scope.fd.FieldName].setHours(12,0,0,0);
                    
                }
                //$scope.formData[0][$scope.fd.FieldName]=dateVal.getUTCDate();
                $scope.ksm.executeAction(itemValueState,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                //$scope.fd.DefaultValue = $scope.formData[0][$scope.fd.FieldName];
                
                if ($scope.fd.ShowSearchForm) $scope.fd.refreshSearchValue();
        };
       
          $scope.fd.on_change_between=function(between)
        {
                
                
                if ($scope.fd.Dbtype==="datefielddefinition" && $scope.fd.Search[between]!==undefined && $scope.fd.Search[between]!==null) {
                    
                    $scope.fd.Search[between].setHours(12,0,0,0);
                    
                }
        }; 
        
        let apply_value = function(data) {
                if (data!=undefined) $scope.formData[0][$scope.fd.FieldName] = data;
        };
        
        if ($scope.fd?.Parameters?.DependentFieldId!=undefined && $scope.fd?.Parameters?.DependentFieldId!=null)   
        {
            if ($scope.fd.Parameters.DependentFieldId!="")
            {
                let dependent_field = "Field"+$scope.fd.PresentationId;
                let subscribedSubject = $scope.resourceDefinition.page_state_id+'-'+$scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
                let state = $scope.ksm.getState(subscribedSubject);
                if (state.status=="ok") $scope.formData[0][$scope.fd.FieldName] = state.state;
            }
        }
        
        
   
    
 $scope.fd.refreshSearchValue = function() {
         if ($scope.fd.Search && $scope.formData !=undefined && $scope.formData.length>0) $scope.fd.Search.value=$scope.formData[0][$scope.fd.FieldName];
        } 
	  $scope.applyDateFilters = function(index) {//ovo je aktivna funkcija!!

		$scope.fd.Search=$scope.fd.localVars.search[index];
      
        if ($scope.formData !=undefined && $scope.formData.length>0)
        {
            if ($scope.formData[0][$scope.fd.FieldName]!=undefined) $scope.fd.Search.value=$scope.formData[0][$scope.fd.FieldName];// ponovno dodaj value
        }    
		if ($scope.fd.Search.id==4)  $scope.fd.Search.ShowSecondDate=true // kad je  between
		else  $scope.fd.Search.ShowSecondDate=false
        
        //if ($scope.fd.Search.name=="eq") $scope.fd.Search=null;
            
		//$scope.fd.isOpen=false;		
	  };

    $scope.showHideProperties = function(value)
    {
        $scope.gsrvc.setFieldPropertiesOpened(value);
        $scope.localVars.showFieldProperties=$scope.gsrvc.getFieldPropertiesOpened();
        if (value) $scope.gsrvc.setPropertiesPosition();
    };

  $scope.fd.formats = [{id:1,name:'dd-MMMM-yyyy HH:mm:ss:sss'}, {id:2,name:'yyyy/MM/dd HH:mm:ss:sss'}, {id:3,name:'dd.MM.yyyy HH:mm:ss:sss'}];
  $scope.fd.formatsTime = [{id:1,name:'HH:mm:ss'}, {id:2,name:'HH:mm:ss:sss'}, {id:2,name:'HH:mm'}];
  $scope.fd.formatsDate = [{id:1,name:'dd-MMMM-yyyy'}, {id:2,name:'yyyy/MM/dd'}, {id:3,name:'dd.MM.yyyy'}, {id:4,name:'shortDate'}];
 
  $scope.fd.setFormat = function()
      {
      $scope.fd.format="";
      if  ($scope.fd.ParametersLang.format1!=undefined && $scope.fd.ParametersLang.format1!=null)
      {
        $scope.fd.format = $scope.fd.ParametersLang.format1.name;
      }
      
      if  ($scope.fd.ParametersLang.format2!=undefined && $scope.fd.ParametersLang.format2!=null)
      {
        if ($scope.fd.format!="")
        {
         $scope.fd.format = $scope.fd.format+" "+$scope.fd.ParametersLang.format2.name;
        }else
        {
          $scope.fd.format = $scope.fd.ParametersLang.format2.name;
          
        
        }
      }
      
      if ($scope.fd.format =="")
      {
        $scope.fd.format = $scope.fd.formats[2].name;
      }
  }
  
            let template;
            if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktDate.html');
            else template = $templateCache.get('ktdirectivetemplates/ktDateExe.html');
      
            $element.append($compile(template)($scope));
            
            if (itemValueState.subject.currentStateIndex>=0)
            {
           
            $scope.formData[0][$scope.fd.FieldName] = itemValueState.subject.getState;

            }   
    
            if ($scope.formData[0][$scope.fd.FieldName]!=undefined) $scope.ksm.executeAction(itemValueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);

                
}];//datepickerCtrl

      // console.log("u direktivi kt-date ");
        return {
            restrict: 'A',
			controller: datePickerCtrl,
             link: function (scope, elem, attrs,ctrl) {
              
              

            

              scope.fd.colLength=[];
              for (var i=0;i<24;i++) scope.fd.colLength.push('col-lg-'+(i+1));
              
             scope.fd.dateFormatList = [{id:1,name:'dd.MM.yyyy'},{id:2,name:'dd-MM-yyyy'},{id:3,name:'dd/MM/yyyy'},{id:4,name:'dd.MM.yy'}];
       
        
             scope.fd.timeFormatList = [{id:1,name:'HH:mm',mask:'99:99'},{id:2,name:'HH:mm:ss'},{id:3,name:'HH:mm'},{id:4,name:'HH:mm:ss:sss'}];
         
               
            var posFormatDate = scope.fd.dateFormatList.map(function(e) { return e.name.toString(); }).indexOf(scope.fd.ParametersLang.format1);
              
                           if (posFormatDate>=0){
             
                             scope.fd.format1=scope.fd.dateFormatList[posFormatDate];
                                  
                            }
            var posFormatTime = scope.fd.timeFormatList.map(function(e) { return e.name.toString(); }).indexOf(scope.fd.ParametersLang.format2);
              
                           if (posFormatTime>=0){
             
                             scope.fd.ParametersLang.format2=scope.fd.timeFormatList[posFormatTime];
                                  
                            }
            scope.fd.dirLoaded = true

            scope.fd.setFormat(); 
               //find object
             scope.fd.showProperties = function()
                {
                  scope.localVars.showFieldProperties = true;
                  scope.gsrvc.setFieldProperties(scope.fd,scope.inputArray);
       
                };   
               //console.log("u direktivi kt-date link funkcija");
          scope.fd.setSystemDate=function(){
                if (scope.fd.UseSystemDate) {
                  //code
                  if (scope.formData !=undefined && scope.formData.length>0)
                  {
                      scope.formData[0][scope.fd.FieldName]=new Date();
                  }
                 
               
               }
                
          }
             
        
             
              if (scope.formData !=undefined && scope.formData.length>0)
               {
                    if (scope.formData[0][scope.fd.FieldName]!= null && scope.formData[0][scope.fd.FieldName]!="")
                   
                   {
                      scope.formData[0][scope.fd.FieldName] = new Date (scope.formData[0][scope.fd.FieldName]);
    
                   }
               }
               

                scope.fd.SetDefault = function()
                {
                    scope.fd.DefaultValue = {value:scope.formData[0][scope.fd.FieldName]};
                }
                
                if (scope.fd.UpdateWithDefault && !scope.fd.NoUpdate) scope.formData[0][scope.fd.FieldName] = new Date (scope.fd.DefaultValue?.value)
                if (scope.fd.UseSystemDate) scope.fd.setSystemDate();

              
               
            }
        };
    };
function DatePickerCtrl2 ($scope) {

  		$scope.CTRLNAME='DatePickerCtrl2';
        $scope.fd.localVars.search=[                     /*  MIGOR TODO - VIDJETI DA LI OVO TREBA zaKOMENTIRATI ILI NE */
		{id:1,filter:'gte',name:"From <date>",icon:"fa fa-chevron-right"},
		{id:2,filter:'lte',name:"To <date>",icon:"fa fa-chevron-left"},
		{id:3,filter:'eq',name:"On <date>",icon:"fa fa-bars"},      // MIGOR TODO - vidjeti zasto se pokazuje na pocetku od datePicker2 kontrolera umjesto prvog
		{id:4,filter:'between',name:"Between <date> and <date>",icon:"fa fa-expand"} ];
 
    if ($scope.fd.ParametersLang==undefined || $scope.fd.ParametersLang==null) $scope.fd.ParametersLang={};
 
    //if ($scope.fd.Search==undefined) $scope.fd.Search=$scope.search[2];
        $scope.CTRL='datepicker2';

        
        $scope.refreshSearchValue = function() {
                 if ($scope.fd.Search) $scope.fd.Search.value=$scope.fd.sItemValue;      
        } 
        
	  $scope.applyDateFilters = function(index) {
      
           // $scope.fd.Search=$scope.search[index]; // ovo pregazi value      // MIGOR TODO  - mozda treba ovdje dodati jer postoji u ktdate
          // if ($scope.fd.sItemValue!=undefined) $scope.fd.Search.value=$scope.fd.sItemValue;// ponovno dodaj value  // MIGOR TODO  - mozda treba ovdje dodati jer postoji u ktdate
           
		  var i=$scope.fd.localVars.search.selected.id; 

		  if ($scope.fd.localVars.search.selected.id==4)  $scope.fd.localVars.search.showSecondDate=true // if (scope.fd.search.name=="Equals") scope.fd.search=null;
		  else  $scope.fd.localVars.search.showSecondDate=false
		  		
	  };

  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'yy',
   // maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  }

  $scope.toggleMin = function() {
    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  };

  $scope.toggleMin();

  $scope.open1 = function() {
    $scope.popup1.opened = true;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true; //DatePickerCtrl2
  };
  
  $scope.applyDateFilters = function() {
	  var sc=$scope;
	  var t=sc.fd;
	
    $scope.popup2.opened = true;
  };
   
  

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.fd.formats = [{id:1,name:'dd-MMMM-yyyy HH:mm:ss:sss'}, {id:2,name:'yyyy/MM/dd HH:mm:ss:sss'}, {id:3,name:'dd.MM.yyyy HH:mm:ss:sss'}];
  $scope.fd.formatsTime = [{id:1,name:'HH:mm:ss'}, {id:2,name:'HH:mm:ss:sss'}, {id:2,name:'HH:mm'}];
  $scope.fd.formatsDate = [{id:1,name:'dd-MMMM-yyyy'}, {id:2,name:'yyyy/MM/dd'}, {id:3,name:'dd.MM.yyyy'}, {id:4,name:'shortDate'}];
 
  $scope.setFormat = function()
      {
      $scope.format="";
      if  ($scope.fd.ParametersLang.format1!=undefined && $scope.fd.ParametersLang.format1!=null)
      {
        $scope.format = $scope.fd.ParametersLang.format1.name;
      }
      
      if  ($scope.fd.ParametersLang.format2!=undefined && $scope.fd.ParametersLang.format2!=null)
      {
        if ($scope.format!="")
        {
         $scope.format = $scope.format+" "+$scope.fd.ParametersLang.format2.name;
        }else
        {
          $scope.format = $scope.fd.ParametersLang.format2.name;
          
        
        }
      }
      
      if ($scope.format =="")
      {
        $scope.format = $scope.formats[2];
      }
  }
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
    return '';
  }
};