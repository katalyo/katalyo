'use strict';

export function ktMenuClick () {

        return {
          restrict: 'A',
          templateUrl: 'ktdirectivetemplates/ktMenuClick.html',
          controller: function($scope,$stateParams){
            
            if ($scope.localVars==undefined) $scope.localVars={};
            if ($scope.fd==undefined) $scope.fd={};
            if ($scope.taskDefinition==undefined) $scope.taskDefinition={};
                   
            $scope.localVars.menuId = $stateParams.id;
            $scope.localVars.previousTask=$stateParams.prev_task_execute_id;
            $scope.localVars.outcomeId=$stateParams.outcome_id;
            $scope.localVars.OpenTaskType = "newPage";
            //$scope.localVars.ShowReturnBtn = true;
            if ($scope.localVars.previousTask==undefined) $scope.localVars.previousTask = 0;
            if ($scope.localVars.outcomeId==undefined) $scope.localVars.outcomeId = 0;
            $scope.initType = $stateParams.init_type;
            $scope.localVars.processMenuClicked = true;
            if ($scope.initType=="t")
            {
              $scope.localVars.processMenuClicked = false;
              $scope.localVars.processStartTask = true;
            }
           
                
            },
          link: function (scope, elem, attrs) {
       
       
       
        
          
      }
      
          
    }
};