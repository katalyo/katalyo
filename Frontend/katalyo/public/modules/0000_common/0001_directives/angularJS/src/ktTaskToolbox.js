'use strict';

export function ktTaskToolbox () {

        return {
                restrict: 'A',
                templateUrl: 'ktdirectivetemplates/ktTaskToolbox.html',
               
                controller: ['$scope','$rootScope','GridsterService','TaskDefinitionService','KatalyoStateManager','$timeout', function($scope,$rootScope,GridsterService,TaskDefinitionService,KatalyoStateManager,$timeout){
              
                        $scope.CTRL='ktTaskToolbox';
                        $scope.gsrvc = GridsterService;
                        $scope.toolboxItems = [];
                        $scope.tds = TaskDefinitionService;
                        $scope.collapsed={};
                        $scope.reloadingToolbox = true;
                        $scope.gsrvc.destroyDragula('bag-task');
                        
                        var widgetHandle = 'task-element-drag-handle';
        
                        var setupDragulaResult;
                        
                        $scope.gsrvc.setupDragula('bag-task','task-toolbox',widgetHandle);
                        $scope.taskToolboxItems= $scope.gsrvc.getTaskToolboxItems();
                        
                        //register to state manager
                        $scope.ksm = KatalyoStateManager;

                        let toolbox_loaded = function(data)
                        {
                            $scope.taskToolboxLoaded(data.state);
                        };
                        
                         let toolbox_reload = function(data)
                        {
                          $scope.taskToolboxLoaded(data);
                        };
                        
                        let toolboxState= $scope.ksm.addSubject("taskToolBox",null);
                        let toolboxStateName = toolboxState.subject.name;

                        let action_toolbox_loaded = $scope.ksm.addAction(toolboxStateName,"toolboxLoaded",toolbox_loaded);
                        
                        let action_toolbox_reload = $scope.ksm.addAction(toolboxStateName,"toolboxReload",toolbox_reload);
                        
                         $scope.setCollapse=function(value,element_type){
                            if (element_type=="toolboxgroup")
                            {
                                if ($scope.collapsed[value]===undefined) $scope.collapsed[value] = true
                                else
                                {
                                    if ($scope.collapsed[value]) $scope.collapsed[value] = false
                                    else $scope.collapsed[value] = true;
                                }
                            }
                        }

                        $scope.taskToolboxLoaded=function(value){ 
                                if (!value) {
                                    $scope.gsrvc.destroyDragula('bag-task'); 
                                }
                                else {
                                    $scope.taskToolboxItems.length=0
                                    angular.extend($scope.taskToolboxItems, $scope.gsrvc.getTaskToolboxItems())
                                    //$scope.taskToolboxItems= $scope.gsrvc.getTaskToolboxItems();
                                    //$timeout(function(){$scope.taskToolboxItems= $scope.gsrvc.getTaskToolboxItems();})   
                                    //$timeout(function(){$scope.gsrvc.SetTaskToolboxLoaded(value);$scope.reloadingToolbox = true;});
                                }
               
                        };
                        
                        $scope.toolboxStatus=function(value){
        
                         $scope.gsrvc.SetTaskToolboxLoaded(value);
                        };
             
             }],
             link: function (scope, elem, attrs) {
              
                       
              }
      }
};