'use strict';



export function ktPublishedResourceSelector  () {

        return {
             restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktPublishedResourceSelector.html',
             scope:{
                	 localVars : "=",
                       fd : "=",
             },
             controller: function($scope,CodesService,ResourcesDataService,$rootScope,ProcessingService,dragularService,$timeout,CODES_CONST,MessagingService)
             {
                $scope.fd.formPriority='e';
              $scope.fd.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_NEW_;
              $scope.fd.RelatedPopulateType=1;
              if ($scope.fd.Parameters==undefined) $scope.fd.Parameters ={};              
              if ($scope.fd.PopulateType!=null && $scope.fd.PopulateType!="")
              {
                var pType = $scope.fd.PopulateType;
                var formPriority = pType.toString().substr(1,1);
                $scope.fd.RelatedPopulateType=pType.toString().substr(0,1);
                if (formPriority=="i" || formPriority=="e")
                {
                  
                  $scope.fd.formPriority = formPriority;
                }
              }

              if ($scope.fd.localVars.selectedResourceItem!=undefined)
              {
                $scope.fd.localVars.selectedResourceItem.src_type_text = $scope.fd.localVars.src_type_text[$scope.fd.RelatedPopulateType];
              }else
              {
                $scope.fd.localVars.selectedResourceItem = {src_type_text:$scope.fd.localVars.src_type_text[$scope.fd.RelatedPopulateType]};
              }
              
             


                  
                  //get-published-resources/(?P<resourceType>.+)/(?P<publishType>.+)'
        $scope.fd.localVars.GetPublishedResourcesByType = function()  {	
                
                if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
                var lang=$rootScope.globals.currentUser.lang;
                
                ResourcesDataService.GetPublishedResourcesByType($scope.fd.localVars.resourceType,'form').then(function (data) {       
                        $scope.fd.localVars.publishedResourceList = data.data;
                        
                           
                        if ($scope.fd.Parameters.PublishedResourceId!=null) {
                            $scope.fd.localVars.setSelectedResource($scope.fd.Parameters.PublishedResourceId);
                        }  
                 },function(error){    
                            MessagingService.addMessage(error.msg,'error');
                  });
               } 
    
    
     
    
	if ($scope.fd.localVars.publishedResourceList==undefined) {
            $scope.fd.localVars.GetPublishedResourcesByType();
    }
         
    /* if ($scope.fd.localVars.resourceList==undefined) {
            $scope.fd.localVars.getResources();
    }
    */     
       $scope.fd.localVars.changePriority= function(fp){ 
             $scope.fd.formPriority =fp; 
            };
         
          
         $scope.fd.localVars.confirm= function(){
               
                    $scope.fd.localVars.selectedResourceItem = $scope.fd.localVars.selectedResource
                   //$scope.fd.localVars.selectedResourceItem.src_type_text = $scope.fd.localVars.src_type_text[$scope.fd.RelatedPopulateType];
                  // $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                   $scope.fd.PopulateType=$scope.fd.RelatedPopulateType+$scope.fd.formPriority;
                   $scope.fd.localVars.datasetMapperType=0;
                   $scope.fd.localVars.setSelectedResource($scope.fd.localVars.selectedResourceItem.id);
             
               
             //  $scope.localVars.toggleProperties(0); //MIGOR dodao da se sakrije property kad stisne prozor , TODO - treba potvrditi da se nesto s time ne kvari
            };
				  
              
              
              $scope.fd.localVars.onSelectResourceCallback = function (item, model){

                  	var eventResultId=item.id;
                  	var eventResultName=item.name;
                    var eventResultParentElementId =item.parentElementId;
                   
                   // if (item.id!=$scope.fd.localVars.selectedResourceItem.id && $scope.fd.localVars.relatedPopulateType!=1){
                      // TODO - kad se promjeni dataset onda se treba resetirati GRIDSTATE
                     // $scope.fd.GridState = {} TODO i ovo dodati?
                     //$scope.fd.Parameters.GridState = {state:[]}
                      
                      //$scope.fd.localVars.getFormWidgets(item.id);
                      
                  //  }
                    
              
                	  $scope.fd.localVars.selectedResourceItem_c = {id:eventResultId, name : eventResultName,parentElementId:eventResultParentElementId,Form:item.Form,Version:item.Version,ResourceDefId:item.ResourceDefId};
                    
                    
              }
            
             $scope.fd.localVars.setSelectedSource = function(source_id){
             
                  if (source_id==-1)
                  {
                    
                    $scope.fd.RelatedPopulateType = '3';
                    $scope.fd.formPriority = 'e';
                    
                  }else
                  {
                   
                    $scope.fd.RelatedPopulateType = '2';
                    $scope.fd.formPriority = source_id.form_type;
                    $scope.fd.SrcWidgetId = source_id.parentElementId;
                    if (source_id.type=='rlist') $scope.fd.RelatedPopulateType = '4';
                    
                  }
             };
              
             $scope.fd.localVars.getFormWidgets = function(resource_id){
          

                  var lang=$rootScope.globals.currentUser.lang;
                  if ($scope.fd.PresentationId == undefined || $scope.fd.PresentationId==null) $scope.fd.PresentationId=0;
          
                  ResourcesDataService.getFormWidgets(resource_id,$scope.fd.localVars.taskId,$scope.fd.PresentationId,1,lang).then(function(data) {
                               
                             
                                 
                                    $scope.fd.localVars.currentTaskInitiateDatasets = data.data.initiate_widgets;
                                    
                                    $scope.fd.localVars.currentTaskExecuteDatasets = data.data.execute_widgets;
                                  
                                    $scope.fd.localVars.currentDownloaded = true;
                                  
                                  
                                  
                                    $scope.fd.localVars.previousTaskInitiateDatasets = data.data.initiate_widgets;
                                    $scope.fd.localVars.previousTaskExecuteDatasets = data.data.execute_widgets;
                                 
                                    $scope.fd.localVars.previousDownloaded = true;
                                  
                                  
                                  
                                     },function(error){
                                       MessagingService.addMessage(error.msg,'error');
                                });
             };
           
           
              $scope.fd.localVars.setPopulateMethod= function(populateMethod){
                        
                       $scope.fd.PopulateMethod=populateMethod;
                       
                       if (populateMethod=='A')
                        {
                          if ($scope.fd.localVars.selectedResourceItem_c!=undefined)
                          {
                                 var id=$scope.fd.localVars.selectedResourceItem_c.id;
                                 if (id==undefined) id=$scope.fd.localVars.selectedResourceItem_c.relatedId;
                                 if (id!=undefined) $scope.fd.localVars.getFormWidgets($scope.fd.localVars.selectedResourceItem_c.id);
                          }
                          
                        }
                  };
                  
              $scope.fd.localVars.setPopulateMethod($scope.fd.PopulateMethod);
              
              
              $scope.fd.localVars.setPopulateType= function(populateType){
                        
                       $scope.fd.RelatedPopulateType=populateType;
                       
                          if ($scope.fd.localVars.selectedResourceItem_c!=undefined)
                          {
                            $scope.fd.localVars.getFormWidgets($scope.fd.localVars.selectedResourceItem_c.relatedId);
                          }
                          
                        
                  }
          
               },
             link: function (scope, elem, attrs) {
                       
             
             }
        }
};