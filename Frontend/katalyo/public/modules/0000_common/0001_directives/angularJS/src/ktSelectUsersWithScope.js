'use strict';

export function ktSelectUsersWithScope (UserService) {

    return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktSelectUsersWithScope.html',
             scope:{
              selectedItems:'=',
              size:'=?',
              selectId:'=',
             },
              controller: function(UserService){},
             link: function (scope, elem, attrs) {


              
       scope.userList=[];
        //if (scope.selectedItems==undefined) scope.selectedItems=[]
        scope.selected={users:scope.selectedItems};
        
        
           
             scope.$watch('selected.users',function(newValue,oldValue){
              
              if (newValue!==oldValue && newValue!==undefined)
              {
               scope.selectedItems=newValue;
              }
            });
         

        scope.getUsers = function () {
            //debugger
            
          UserService.getUsersGroups('users').then(function(data){
          scope.userList = angular.copy(data.data);
          scope.usersDownloaded = true;
				});

        }
				scope.getUsers();

            
            
        scope.userSelected =  function(item,model) {
        
     
        /*
            var pos = scope.selectedItems.map(function(e) { return e.id; }).indexOf(item.id);
              
            if (pos<0){
                 scope.selectedItems.push(angular.copy(item));
                
              }
          */  

        };
      
      }
    }
};