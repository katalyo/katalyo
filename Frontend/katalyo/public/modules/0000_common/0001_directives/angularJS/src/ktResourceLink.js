'use strict';

export function ktResourceLinks () {
        return {
        restrict: 'A', 
        controller: function ($scope, $rootScope,ResourcesDataService,ProcessingService,$timeout,MessagingService,GridsterService) {
	
        $scope.gsrvc = GridsterService;
        
        if ($scope.localVars==undefined) $scope.localVars={};        
        if ($scope.formData == undefined) $scope.formData=[{}];
        if ($scope.formData.length == 0) $scope.formData[0]={};
 
        $scope.localVars.isResourceSelected = false;
      
        $scope.localVars.pickButtonClicked=false;
				$scope.localVars.pickButtonLabel='Choose dataset..';
			  $scope.selectedResource=null;	
				$scope.selectedDataRecord=false;
				$scope.taskId=0;	
        $scope.localVars.gridOptions = {
              columnDefs: [],
              data: [],
							enableGridMenu: true,
							showGridFooter: true,
							showColumnFooter: false,
							enableRowSelection: true,
						   multiSelect: true,
						   enableHorizontalScrollbar : 2, 
						   enableVerticalScrollbar : 2,
						   enableFiltering: true,
						   enableRowHeaderSelection: true,
						   enableColumnResizing: true,
						   autoResize:true,
						   minRowsToShow: 4,
						   minimumColumnSize: 40,
				
					   }
					   
					   
					   
					   $scope.localVars.gridOptions2 = {
							//enableGridMenu: true,
							showGridFooter: false,
							showColumnFooter: true,
							enableRowSelection: false,
						   multiSelect: false,
						  enableHorizontalScrollbar : true, 
						  enableVerticalScrollbar : true,
						  // enableFiltering: true,
						  enableRowHeaderSelection: true, //false
              showHeader:true,
						  enableColumnResizing: true,
						   autoResize:true,
						   minRowsToShow: 4,
               displayselectionCheckbox:false,
               enableGridMenu: true,
						 
					   }
					   
						$scope.relationshipType = $scope.fd.maxLength;
            //if ($scope.relationshipType>2) $scope.relationshipType=2;
						
          $scope.colLength = ['col-lg-2','col-lg-4','col-lg-6','col-lg-8','col-lg-10','col-lg-12','col-lg-14','col-lg-16','col-lg-18','col-lg-20','col-lg-22','col-lg-24'];
       
          $scope.fd.tItemValue = $scope.fd.formDef;
          if (typeof $scope.fd.tItemValue=="string") $scope.fd.tItemValue=[]; 
					$scope.fd.resourcesList=[];
          $scope.resourceListLength=0;
          $scope.fd.selectedDataset = 0;
          if ($scope.fd.related!=null) $scope.fd.selectedDataset = $scope.fd.related;
					// POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
					$scope.fd.getResources = function(search)  {
						var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByTypeFilter(lang,'dataset',search,$scope.fd.selectedDataset).then(
						function(data) {
										
											  $scope.fd.resourcesList = data.data;
                        
                        if ($scope.fd.related!=null)
                        {
                          $scope.resourceId = $scope.fd.related;
                          $scope.resourceListLength = $scope.fd.resourcesList.length;
                          //select dataset from dropdown
                           var pos = $scope.fd.resourcesList.map(function(e) { return e.id; }).indexOf($scope.resourceId);

                          if (pos>=0){
                                $scope.selectedResource=$scope.fd.resourcesList[pos];
                                $scope.localVars.isResourceSelected=true;
                                $scope.localVars.pickButtonLabel="Choose "+$scope.selectedResource.name+" record...";
                                
                          }
                          
                          if ($scope.resourceId != undefined) {	
                              $scope.fd.getResourceData();
                          }   
                        }      
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					}
				  
				  //$scope.getResources();
				  $scope.localVars.pickButtonClicked=false;
				  $scope.resourceDataRecordSelected=false;
				  
				     
              
						// *******************             ZA GRID 1 
						
						$scope.localVars.gridOptions.onRegisterApi = function (gridApi) {
									$scope.gridApi = gridApi;
									$scope.gridApi.grid.registerRowsProcessor($scope.fd.singleFilter, 200);
									 $scope.gridApi.treeBase.on.rowExpanded(null, function(row) {
										updatePagination(row.treeNode.children.length);
									  });
									 $scope.gridApi.treeBase.on.rowCollapsed(null, function(row) {
										updatePagination(-row.treeNode.children.length);
									  });
							
							
							gridApi.selection.on.rowSelectionChanged($scope,function(row){
								
                  // Ubaci odabrane u FD
                   $scope.fd.Value=$scope.gridApi.selection.getSelectedRows().map( function (a) { return a.id; });
							 
                 // Ubaci odabrane u GRID2
                 $scope.localVars.gridOptions2.data=$scope.gridApi.selection.getSelectedRows();
                 // koliko ih je odabranih
                 $scope.resourceDataRecordSelected=$scope.fd.Value.length; 
							});     
						 }
 
						// *******************             ZA GRID 2
						
						$scope.localVars.gridOptions2.onRegisterApi = function (gridApi) {
									$scope.gridApi2 = gridApi;
            }
            
             function updatePagination(rowsDifference) {
						  //updating pagination will trigger singleFilter where the height is being reset
						  $scope.gridOptions.paginationPageSizes = [$scope.gridOptions.paginationPageSize + rowsDifference, 25, 50, 100, 500];
						  $scope.gridOptions.paginationPageSize = $scope.gridOptions.paginationPageSize + rowsDifference;
						}
						//you can name the function anything, I was doing global external filtering inside this, so just named this.
						$scope.fd.singleFilter = function(renderableRows) {
							$timeout(function() {
							  var newHeight = ($scope.gridApi.grid.getVisibleRowCount() * 30) + 145;
							  angular.element(document.getElementsByClassName('myGrid')[0]).css('height', newHeight + 'px');
							}, 10);
							return renderableRows;
						  }
  

						
						// when PICK BUTTON pressed
						$scope.localVars.pickResourceData = function ()
						{
							//console.log($scope.resourceDataRecordSelected);
							$scope.localVars.pickButtonClicked=!$scope.localVars.pickButtonClicked;
							
							if (!$scope.localVars.pickButtonClicked) { // RESOURCE_SELECTED stanje
								$scope.localVars.pickButtonLabel="Choose "+$scope.selectedResource.name+" record...";

							}
							
							if ($scope.localVars.pickButtonClicked) { //CHOOSE_RESOURCE  stanje
								
								$scope.localVars.pickButtonLabel='Click to finish';

							}

					 }
         
           $scope.fd.getResourceDataRefresh = function (retValue) 
            {
              var lang = $rootScope.globals.currentUser.lang;
              //ToDo ovo prebaciti sve u jedan API poziv??
              ResourcesDataService.getResourceDataWithRelated($scope.parentResourceDefId,$scope.resourceId,$scope.resourceRecordId,$scope.taskId,$scope.fd.itemId,lang).then(function(data) {
                            $scope.formData[0]['Field'+$scope.fd.itemId] = data.m2m; 
                            $scope.setupGrid(data);                
                },function(error){    
                    MessagingService.addMessage(error.msg,'error');
									});
                      
            }
            $scope.localVars.setupGrid= function(data)
            {
              $scope.localVars.gridOptions.data = data.data;
             
              $scope.localVars.gridOptions.columnDefs = data.columnsDef;
              $scope.localVars.gridOptions2.columnDefs = data.columnsDef;
  
              if ($scope.formData[0]['Field'+$scope.fd.itemId]!=undefined){
              for (var i=0;i<$scope.formData[0]['Field'+$scope.fd.itemId].length;i++)
                {
                   var pos = $scope.localVars.gridOptions.data.map(function(e) { return e.id; }).indexOf($scope.formData[0]['Field'+$scope.fd.itemId][i]);

                  if (pos>=0){
                      //prebaci u grid 2
                      $scope.localVars.gridOptions2.data.push(angular.copy($scope.gridOptions.data[pos]));
  
                      //selektiraj odabrane
                      $scope.fd.gridApi.grid.modifyRows($scope.localVars.gridOptions.data);
                      $scope.fd.gridApi.selection.selectRow($scope.localVars.gridOptions.data[pos]);
                  }            
                    
                }
              }

                            
                $timeout(function(){
                $scope.restoreGridState();
               
               });
            }
             
              $scope.fd.getResourceData = function () 
								{
								// $scope.activity.activityMessage = ProcessingService.setActivityMsg('Downloading resource data for '+$scope.resourceId);
								 var lang = $rootScope.globals.currentUser.lang;
								 ResourcesDataService.getResourceData($scope.resourceId,$scope.taskId,lang).then(function(data) {
												 
														$scope.localVars.setupGrid(data);
                            
											  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
									});
								};
            
            
            		 $scope.fd.saveGridState= function() {
                    var state = $scope.gridApi2.saveState.save();
                   $scope.fd.gridState = state;
                  MessagingService.addMessage("To make changes stick please save form",'info');
      
                  }
  
                  $scope.restoreGridState= function() {
                   if ($scope.fd.gridState!=null)
                   {
                      if ($scope.gridApi2!=undefined)
                      {
                     
                       $scope.gridApi2.saveState.restore($scope,$scope.fd.gridState);
                       
                      }else
                      {
                       $timeout(function(){
                           $scope.restoreGridState();
                       });
                      }
                   }
                 }
            
                        		
					// Resource is selected
              $scope.fd.onSelectCallback = function (item, model){
                var eventResultId=item.id;
                $scope.fd.selectedDatasetName=item.name;
               
               
                $scope.fd.selectedResource={name :item.name};
                //console.log("onSelectCallback :" +  $scope.eventResultName+ ' ' + eventResultId);
                $scope.localVars.pickButtonLabel="Choose "+$scope.eventResultName+" record...";
						 
                 $scope.resourceId=eventResultId;
                 $scope.localVars.isResourceSelected=true;
						 
							
						  
								
                 if ($scope.resourceId != undefined) {	
                		$scope.fd.getResourceData();
                	}
                	$scope.fd.related=$scope.resourceId;						
              } 
          
            
				},//...controller
				
            templateUrl: 'ktdirectivetemplates/ktResourceLink.html',
            link: function (scope, elem, attrs) {
				  
            }
    
    }
};