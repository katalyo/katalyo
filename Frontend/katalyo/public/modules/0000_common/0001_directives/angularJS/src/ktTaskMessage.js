'use strict';

export function ktTaskMessage () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskMessage.html',
             controller: function($scope,GridsterService,$sce){
                
                $scope.gsrvc= GridsterService;
                
                if ($scope.localVars == undefined) $scope.localVars={};
                
                 $scope.localVars.showMenu = false
      
                  $scope.localVars.toggleDropdown = function(){
                                       $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
                  }
      
                $scope.localVars.ResponseMsg = $sce.trustAsHtml($scope.fd.ResponseMsg);

                },
             
            link: function (scope, elem, attrs) {
                
              
        }       
              
    }
  
};