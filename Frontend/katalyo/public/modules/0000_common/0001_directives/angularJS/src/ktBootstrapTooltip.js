'use strict';

export function ktBootstrapTooltip () {

        return {
            restrict: 'A',
         
            link: function (scope, elem, attrs) {
                
                 attrs.$observe('title',function(title){
                    // Destroy any existing tooltips (otherwise new ones won't get initialized)
                    elem.tooltip('dispose');
                    // Only initialize the tooltip if there's text (prevents empty tooltips)
                    if (jQuery.trim(title)) elem.tooltip();
                  });
              
                  elem.on('$destroy', function() {
                    elem.tooltip('dispose');
                    delete attrs.$$observers['title'];
                  });
    
              
               
            
            }
        }
  
};