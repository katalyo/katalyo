'use strict';

export function ktRegexValidator (){ 
   return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, elem, attr, ngModel) {           
          ngModel.$validators.ktRegexValidator = function(modelValue, viewValue) {
            
            if (modelValue===undefined && viewValue===undefined) return true;
            
              let regex = attr.ktRegexValidator;
              let value = modelValue || viewValue;
              let valid = value.match(regex);
              if (valid!==null) return true;
              else
              {
                ngModel.$setValidity(scope.fd.ErrorMsg,false);
                return false;
              }
          };    
      }
   };
};