'use strict';

export function ktBoolean () {
        return {
                restrict: 'A',
                controller: function ($scope,GridsterService,KatalyoStateManager) {
                        
                        $scope.gsrvc = GridsterService;
                        
                         $scope.showProperties = function(event)
                          {
                             $scope.gsrvc.setFieldProperties($scope.fd, event);
                          };
                          
                          
                        let itemValue = null;
                        if ($scope.formData!=undefined)
                        {
                                if ($scope.formData.length==0) $scope.formData=[{}];
                                itemValue=$scope.formData[0][$scope.fd.FieldName]
                                
                        }else $scope.formData=[{}];

                        if ($scope.formData[0][$scope.fd.FieldName] ===undefined) $scope.formData[0][$scope.fd.FieldName] = false;
                        
                        let itemValueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+'-boolValueStateName'+$scope.fd.PresentationId,itemValue);
                        let itemValueStateName = itemValueState.subject.name;
                        
                        
                        let value_changed = function(data)
                        {
                            itemValueState.subject.setStateChanges = data;
                            itemValueState.subject.notifyObservers("valueChanged");
                        };
                        
                        let action_value_changed = $scope.ksm.addAction(itemValueState,"valueChanged",value_changed);
                       
                        $scope.on_blur_action=function()
                        {
                                
                                $scope.ksm.executeAction(itemValueState,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                                //$scope.fd.DefaultValue = $scope.formData[0][$scope.fd.FieldName];
                        };
                       
                        
                        
                        let apply_value = function(data) {
                                if (data!=undefined) $scope.formData[0][$scope.fd.FieldName] = data;
                        };
                        
                        if ($scope.fd?.Parameters?.DependentFieldId!=undefined && $scope?.fd.Parameters?.DependentFieldId!=null)   
                        {
                            if ($scope.fd.Parameters.DependentFieldId!="")
                            {
                                let dependent_field = "Field"+$scope.fd.PresentationId;
                                let subscribedSubject = $scope.resourceDefinition.page_state_id+'-'+$scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
                                let state = $scope.ksm.getState(subscribedSubject);
                                if (state.status=="ok") $scope.formData[0][$scope.fd.FieldName] = state.state;
                            }
                        }
                        
                        
                         if (itemValueState.subject.currentStateIndex>=0)
                        {
                               
                                $scope.formData[0][$scope.fd.FieldName] = itemValueState.subject.getState;
                    
                        }   
                        
                        if ($scope.formData[0][$scope.fd.FieldName]!=undefined) $scope.ksm.executeAction(itemValueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                
                          
                          
                          
                      
                        },
        templateUrl: 'ktdirectivetemplates/ktBoolean.html',
        link: function (scope, elem, attrs) {
				
           if (scope.formData===undefined) scope.formData = [{'Field':0}];
          if (scope.formParams===undefined) scope.formParams = {formViewType:1};
          if (scope.formParamsParent===undefined) scope.formParamsParent = {formViewType:1};
          
          if (scope.fd.ParametersLang===undefined) scope.fd.ParametersLang={format1:'HH:mm'};
          
          scope.fd.colLength = [];
          for (var i=0;i<24;i++) scope.fd.colLength.push('col-lg-'+(i+1));
             
          scope.fd.timeFormatList = [{id:1,name:'HH:mm',mask:'99:99'},{id:2,name:'HH:mm:ss',mask:'99:99:99'},{id:3,name:'HH:mm'},{id:4,name:'HH:mm:ss:sss'}];
          
          if (scope.fd.ParametersLang!=undefined && scope.fd.ParametersLang!=null)
          {
             
              var posFormatTime = scope.fd.timeFormatList.map(function(e) { return e.name.toString(); }).indexOf(scope.fd.ParametersLang.format1);
                  
              if (posFormatTime>=0){
                 
                scope.fd.ParametersLang.format1=scope.fd.timeFormatList[posFormatTime];
                                      
              }
                 
              scope.localVars.format = scope.fd.ParametersLang.format1;           
          }  
        }
    };

}
