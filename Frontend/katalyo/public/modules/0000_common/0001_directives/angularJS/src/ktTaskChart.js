'use strict';

//import {Chart} from 'chart';
//import ChartDataLabels from 'chartjs-plugin-datalabels';

export function ktTaskChart () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktTaskChart.html',

             controller: function($scope,$rootScope,$timeout,UserService,ResourcesDataService,TaskDataService,MessagingService,GridsterService) {
                 
                 const numFormat = new Intl.NumberFormat("de-DE", {
                      maximumFractionDigits:  $scope.fd.Parameters.chartFractionDigits || 2,
                      minimumFractionDigits:  $scope.fd.Parameters.chartFractionDigits || 2
                    });


                
                 $scope.gsrvc= GridsterService;
                 if (!Array.isArray($scope.fd.selectedTasks)) $scope.fd.selectedTasks=[];
                if ($scope.resourceDefinition!=undefined)  {
                  $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                }
                if ($scope.localVars == undefined) $scope.localVars={};
                 
                $scope.localObj={loadedTasks : []};
                $scope.localVars.taskItem = null;   
                $scope.localVars.selectedTask = {};
                $scope.localVars.showMenu = false
                if ($scope.fd.localVars == undefined) $scope.fd.localVars={};
                
                $scope.fd.localVars.dashboard = {
                    id: '1',
                    name: 'Home',
                    widgets: {}
				} ;
                 $scope.fd.localVars.taskItem = null;
                 $scope.fd.localVars.selectedTask = {};
                
                //MIGOR - PREBACIO IZ TASK ADD RESOURCE, TODO - IZBACITI VISAK
                $scope.taskId=$scope.resourceDefinition.ResourceDefId;
                if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
                if ($scope.taskId==undefined) $scope.taskId=0;
                $scope.fd.localVars.taskId=$scope.taskId;
                $scope.fd.ParentDatasetDefId=$scope.fd.Related;
                $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
                $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
                $scope.source_type=$scope.resourceDefinition.source_type;
                $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
                $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                $scope.formDefinition = $scope.resourceDefinition.form_definition;
                $scope.fd.localVars.resourceRecordId=0;
                
                $scope.formDefinition = $scope.formDefinition || false
                $scope.taskInitiateId = $scope.taskInitiateId || 0
                $scope.taskExecuteId = $scope.taskExecuteId || 0
                $scope.prevTaskExeId = $scope.prevTaskExeId || 0
                    
                  // input: h in [0,360] and s,v in [0,1] - output: r,g,b in [0,1]  // NESTO NE RADI DOBRO
                  let hsvtorgb = function (h,s,v)   {                              
                          let f= (n,k=(n+h/60)%6) => v - v*s*Math.max( Math.min(k,4-k,1), 0);     
                          //return [f(5),f(3),f(1)]; 
                          return   "#" + Math.round(f(5) * 255).toString(16) +  Math.round(f(3) * 255).toString(16) + Math.round(f(1) * 255) .toString(16) 
                        }  


                let HSV2RGB = function(h, s, v) {
                    var r, g, b, i, f, p, q, t;
                    if (arguments.length === 1) {
                        s = h.s, v = h.v, h = h.h;
                    }
                    i = Math.floor(h * 6);
                    f = h * 6 - i;
                    p = v * (1 - s);
                    q = v * (1 - f * s);
                    t = v * (1 - (1 - f) * s);
                    switch (i % 6) {
                        case 0: r = v, g = t, b = p; break;
                        case 1: r = q, g = v, b = p; break;
                        case 2: r = p, g = v, b = t; break;
                        case 3: r = p, g = q, b = v; break;
                        case 4: r = t, g = p, b = v; break;
                        case 5: r = v, g = p, b = q; break;
                    }
                    //return {   r: Math.round(r * 255),     g: Math.round(g * 255),     b: Math.round(b * 255)         };
                     return    "#" + Math.round(r * 255).toString(16) +    Math.round(g * 255).toString(16) + Math.round(b * 255) .toString(16) 
                }


                $scope.fd.localVars.toggleDropdown = function(){
                    $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                }
                
                
                $scope.fd.localVars.copyToClipboard = function(text){
                    $scope.fd.localVars.copyUuid = true;
                    navigator.clipboard.writeText(text).then( 
                        () => {
                            $timeout(function(){$scope.fd.localVars.copyUuid = false;},1000);
                        },
                        () => {    /* clipboard write failed */
                            MessagingService.addMessage('Copy to clipboard failed','error');
                            $timeout(function(){$scope.fd.localVars.copyUuid = false;},1000);
                    });
                                    
                }
        
                
                var  process_observed_data = function(data) 
                {     
                    if (data!=undefined) 
                    { 
                       let analytics = data?.state?.sharedData?.analytics
                        //debugger //process_observed_data
                        //PREPARE CHART
                       if (analytics!=undefined) {
                              if ($scope.fd.Parameters.SelectedChartAnalytics)  
                                  $scope.fd.localVars.SelectedChartAnalytics = $scope.fd.Parameters.SelectedChartAnalytics
                              else 
                                   $scope.fd.localVars.SelectedChartAnalytics=0
      
                              $scope.fd.localVars.analytics =  analytics                        
                              $scope.fd.localVars.data = Object.values(analytics[$scope.fd.localVars.SelectedChartAnalytics][$scope.fd.Parameters.CalculateFunction.name])
                              $scope.fd.localVars.labels = Object.keys(analytics[$scope.fd.localVars.SelectedChartAnalytics][$scope.fd.Parameters.CalculateFunction.name])
                              
                              if ($scope.fd.Parameters?.chartType?.name == 'pie' || $scope.fd.Parameters?.chartType?.name == 'doughnut' || $scope.fd.Parameters?.chartType?.name == 'polarArea') {
                                      
                                      let colors=[] 
                                      let golden_ratio = 0.618033988749895
                                      let h = Math.random() // use random start value
                                       
                                      for (let item of $scope.fd.localVars.data) { 
                                           h += golden_ratio
                                           h %= 1  
                                           colors.push( HSV2RGB( h, 0.5, 0.95) )   
                                      }
                                      
                                      //convert to array 
                                      $scope.fd.localVars.chartColors = [   { backgroundColor: colors }        ]
                                      $scope.fd.localVars.data =     [    $scope.fd.localVars.data    ]                                   
             
                              }
                              
                              if ($scope.fd.Parameters?.chartType?.name!=undefined) 
                                  $scope.fd.localVars.chartType =  $scope.fd.Parameters.chartType.name 
                              else 
                                  $scope.fd.localVars.chartType =  $scope.fd.Parameters.chartType //counter does not have .name
                  
                              
                              $scope.fd.localVars.chartLabel =  $scope.fd.Parameters.chartLabel
                               
                              $scope.fd.localVars.sharedData = data?.sharedData
                              //summing all values for counter
                              $scope.fd.localVars.countAll = $scope.fd.localVars.data.reduce((partialSum, a) => partialSum + a, 0); 
     
  
                        }             
                    }              
                }; 
    
            $scope.fd.localVars.formatNumber = function(num)  {
                  let newFormat , useGrouping;
                  
                  useGrouping = $scope.fd.Parameters.useGroupingSeparator ;         
                  if ($scope.fd.Parameters.chartFractionDigits)
                         newFormat = new Intl.NumberFormat("de-DE", { useGrouping: useGrouping,  maximumFractionDigits: $scope.fd.Parameters.chartFractionDigits ,  minimumFractionDigits: $scope.fd.Parameters.chartFractionDigits  });
                    else 
                       newFormat = new Intl.NumberFormat("de-DE", { useGrouping: useGrouping });               

               try {
                    if ($scope.fd.Parameters.disableFormatting) 
                        return num          
                      else  
                        return newFormat.format(num)
                    
                    } catch (err) {
                            MessagingService.addMessage(error.msg,'error');
                            return 'Error!'
                    }
                    
            }
            
    
            $scope.fd.localVars.setSelectedDataset = function(id,saved)  {
            //debugger
              if ($scope.fd.localVars.selectedResourceItem==undefined) $scope.fd.localVars.selectedResourceItem={}
              //pick orher parameters from database
              $scope.fd.localVars.getPublishedResourceById(id,saved);
              
		}
        
        
        $scope.fd.localVars.getPublishedResourceById = function(id,saved) 
        {
            let pos
            if (saved && $scope.fd.FormDef!==undefined && $scope.fd.FormDef!==null)
              { 
                    pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                    if (pos>=0){
                        $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.publishedResourceList[pos].name;
                        $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos]
                    }
                    $scope.fd.localVars.selectedResourceItem.id = id;
                    $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                    $scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem;
                    $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                    $scope.fd.localVars.formLoaded=true;

              }else{
                  pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                  if (pos>=0){
                    ResourcesDataService.GetPublishedResourceById(id).then(function (data) {

                            $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos];
                            $scope.fd.localVars.dashboard.widgets.layout = data.data.Form
                            $scope.fd.FormDef = $scope.fd.localVars.dashboard.widgets.layout
                            if ($scope.fd.localVars.selectedResourceItem_c!==undefined)
                            {
                                $scope.fd.Parameters.PublishedResourceId=$scope.fd.localVars.selectedResourceItem_c.id;
                                $scope.fd.Parameters.PublishedResourceVersion=$scope.fd.localVars.selectedResourceItem_c.Version;
                                $scope.fd.Related=$scope.fd.localVars.selectedResourceItem_c.ResourceDefId; 
                                $scope.fd.localVars.freshForm=angular.copy($scope.fd.localVars.selectedResourceItem_c.Form)
                                $scope.fd.Related = $scope.fd.localVars.publishedResourceList[pos].ResourceDefId

                            }else MessagingService.addMessage('Error - published dataset not selected ktTAskAdd','error')
          
                            $scope.fd.localVars.formLoaded=true;

                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                    });

                }
            }
                   
}
       

       $scope.fd.localVars.calculateFunctions= [ {id:1,name:'sum'},{id:2,name:'mean'},{id:3,name:'min'},{id:4,name:'max'},{id:5,name:'count'} ]     


// options.plugins.legend :  true


         $scope.fd.localVars.chartOptions= {
                    scale: {
                        ticks: {
                          //precision:  defined as parameter
                        }
                      }  ,
                         plugins: {  
                                    legend: {
                                        display: true,
                                        position: 'bottom',
                                        labels: {
                                            color: 'rgb(255, 99, 132)'
                                        }
                                    },                           
                                      labels: {  
                                        render: 'label',  // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage
                                        precision: 0,// precision for percentage, default is 0
                                        showZero: false, //  whether or not labels of value 0 are displayed, default is false  
                                        fontSize: 14,   
                                        fontColor: '#000',// font color, can be color array for each data or function for dynamic color, default is defaultFontColor
                                        fontStyle: 'normal',   // font style, default is defaultFontStyle
                                        fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",         // font family, default is defaultFontFamily                  
                                        textShadow: false, // draw text shadows under labels, default is false
                                      }
                        }        
          }
          
          
          if ($scope.fd.Parameters.chartPrecision!=undefined && $scope.fd.Parameters.chartPrecision!=null ) {     
               $scope.fd.localVars.chartOptions.scale.ticks.precision = $scope.fd.Parameters.chartPrecision
          }
             
        
        if ($scope.fd.Parameters?.chartType?.name  === 'bar'  || $scope.fd.Parameters?.chartType?.name   === 'line' || $scope.fd.Parameters?.chartType?.name   === 'radar' || $scope.fd.Parameters?.chartType?.name   === 'bubble')  {
            $scope.fd.localVars.chartOptions.plugins.legend.display= false
        }

                $scope.fd.localVars.chartTypes = [
                            {id:1,name:'line'},{id:2,name:'bar'},{id:3,name:'radar'},
                            {id:4,name:'doughnut'},{id:5,name:'pie'},{id:6,name:'polarArea'},
                            {id:7,name:'bubble'} //,{id:99,name:'counter'}
                ]
            
                $scope.localVars.toggleDropdown = function()
                {
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
                }
                              
                $scope.$on('InlineTaskClosed',function(event,data){
                    var taskId=data;
                    var pos = $scope.localObj.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
                    if (pos>=0) {
                      $scope.localObj.loadedTasks.splice(pos,1);  
                    }
                });
               
                if ($scope.fd.selectedTasks===undefined || $scope.fd.selectedTasks===null) $scope.fd.selectedTasks = [];
                
                if ($scope.fd.FormDef!=null && $scope.fd.FormDef!==undefined) {
                    $scope.fd.selectedTasks = $scope.fd.FormDef;
                }else   {
                    $scope.fd.FormDef = [];  
                } 
                
                let initiate_task = function(data)
                {
                    let loadTask=false;
                    $scope.localVars.taskLoaded = false;
                    if ($scope.localObj.loadedTasks.length>0)  {
                        if ($scope.localObj.loadedTasks[0].taskId.id != data.taskId) {
                            $scope.localObj.loadedTasks.length=0;
                            loadTask=true;
                        }
                    } else loadTask=true;
                    
                    $scope.localObj.loadedTasks.splice(0,1);
                         
                    for (let j = 0;j<$scope.fd.selectedTasks.length;j++) {
                            if ($scope.fd.selectedTasks[j].taskId.id==data.taskId) {
                                $timeout(function(){
                                    $scope.localObj.loadedTasks.push(data);
                                    $scope.localVars.taskLoaded = true;
                                });
                            }
                        }
                }
                                
                  //subscribe to events - initially only refresh data    
                        
                  if ($scope.resourceDefinition.page_state_id && $scope.fd.Parameters.widgetReference ) {
                      
                        let whoami ='chart'+ $scope.fd.UuidRef  // valueStateName           
                        let subscribedSubject = $scope.resourceDefinition.page_state_id + '-'+ $scope.fd.Parameters.widgetReference //odabraniUUIDnekogELEMENTA    
                        let observer = $scope.ksm.subscribe(subscribedSubject,whoami,"valueChanged",process_observed_data);   
                  }
                   
               
               let lang=$rootScope.globals.currentUser.lang;  
               
                ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {                                   
                                $scope.fd.taskList = data.data;
                                $scope.fd.tasksLoaded=true;   
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
                            

                $scope.localVars.taskIsOpen = function(taskId)
                  {
                    var isOpenPos = $scope.localObj.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (isOpenPos>=0) return true;
                    else return false;
                  }
                 
                 $scope.fd.pasteFromClipboard = function(task){
                        
                        navigator.clipboard.readText().then( (data) => {
                            /* clipboard successfully read */
                            $timeout(function(){task.widgetId = data})
                        },
                        () => {
                            /* clipboard read failed */
                            MessagingService.addMessage('Paste from clipboard failed','error');
                        });
                       
                }

                  
                $scope.fd.AddTaskToList = function ()
                {
                    $scope.fd.selectedTasks.push({widgetId:null,taskId:null});
                }
                  
                $scope.fd.ConfirmTaskList = function ()
                {
                     $scope.fd.FormDef = $scope.fd.selectedTasks;
                     $scope.fd.showConfirmed=true;
                     $timeout(function(){$scope.fd.showConfirmed=false},5000);
                }
                
                $scope.fd.RemoveTaskFromList = function (index){
                  
                   if (index>=0){
                             $scope.fd.selectedTasks.splice(index,1);         
                   }    
                }
          
          
                 $scope.localVars.saveTaskToResource = function(tdid,tei,datasetDefId,datasetRecordId){
                          TaskDataService.saveTaskToResource(tei,datasetDefId,datasetRecordId,$scope.fd.presentationId).then(function(data) {
                                                    
                              $state.go('app.menu.tasks.initiate',{id: tdid,fromStateName:$state.current.name,fromStateParams:$state.params,outcome_id:0,prev_task_execute_id:tei});
    
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
             }
             
             
           $scope.fd.localVars.showProperties = function(datasetMapperType,showPropertiesRight)
          {
               //debugger
                if ($scope.fd.localVars.datasetMapperType==undefined)
                {
                   $scope.fd.localVars.datasetMapperType = datasetMapperType;
                }else
                {
                   if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                        $scope.fd.localVars.datasetMapperType = datasetMapperType;
                   else 
                       $scope.fd.localVars.datasetMapperType=0;
                }
               
                 
                if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);
           
                if ($scope.fd.localVars.dashboard != undefined)  {
                    //debugger
                     $scope.fd.localVars.fieldsList = []
                     $scope.fd.localVars.getFieldNamesFromObject($scope.fd.localVars.dashboard, $scope.fd.localVars.fieldsList)
                    
                }
          };
          
          
               //IGORCHART
       $scope.fd.localVars.getFieldNamesFromObject = function (obj, fieldsList)   // find FieldNames in given Dashboard object recursively 
     {// fieldsList  is return variable (used for multiple select)
        //debugger
        var o = {};
        Object.keys(obj).forEach(function(key) {
          
            if ( (typeof(obj[key]) === 'object') && obj[key]    && key != 'NameField')  { //Do NOT parse 'NameField'  (we need it as it is)
              $scope.fd.localVars.getFieldNamesFromObject(obj[key], fieldsList)
            } 
            else {          
                    if (key == 'FieldName'  ) {  
                        //debugger
                        o[key]= obj[key];
                        }
                  
                    if (key == 'PresentationId' ) {
                        o['id']= obj[key];
                    }    
                    if (key == 'Label' ) {
                        o['name']= obj[key];
                    }    
                }
            }) ; 
        
        if ( o['FieldName'] && Object.keys(o).length > 0 ) fieldsList.push(o)   
    }; 

                
         },
             link: function (scope, elem, attrs) {
                
               
        }       
              
    }
  
};


/*
 $scope.fd.localVars.newdata =  {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }*/
    
    
            /*
            $scope.fd.localVars.chartOptions.plugins.labels.generateLabels(chart) {
                
                    const datasets = chart.data.datasets;
                    const {labels: {usePointStyle, pointStyle, textAlign, color}} = chart.legend.options;
                    
                    const style = meta.controller.getStyle(usePointStyle ? 0 : undefined);
                    const borderWidth = toPadding(style.borderWidth);
                    return {
                            text: datasets[meta.index].label,
                            fillStyle: style.backgroundColor,
                            fontColor: color,
                            hidden: !meta.visible,
                            lineCap: style.borderCapStyle,
                            lineDash: style.borderDash,
                            lineDashOffset: style.borderDashOffset,
                            lineJoin: style.borderJoinStyle,
                            lineWidth: (borderWidth.width + borderWidth.height) / 4,
                            strokeStyle: style.borderColor,
                            pointStyle: pointStyle || style.pointStyle,
                            rotation: style.rotation,
                            textAlign: textAlign || style.textAlign,
                            borderRadius: 0,
                            datasetIndex: meta.index
                        };
               
            }
            */
            
            