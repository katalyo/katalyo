'use strict';

export function ktAppNavbar () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktAppNavbar.html',
            scope:{
                localVars:'=',
                navbarItemsLeft:'=',
                navbarItemsRight:'=',
                resource:'=',
            },
            controller: ['$scope','$timeout','dragulaService','$rootScope','MenuService','ResourcesDataService','MessagingService','GridsterService','$state',
          
            function($scope,$timeout,dragulaService,$rootScope,MenuService,ResourcesDataService,MessagingService,GridsterService,$state){
               
                $scope.gsrvc = GridsterService;   
                $scope.CTRL='ktAppNavbar'

                let currentUser=$rootScope.globals.currentUser;
                 //debugger
                if ($scope.localVars == undefined) $scope.localVars={};
                $scope.localVars.pagesLoaded=false;    
                let lang = $rootScope.globals.currentUser.lang;
                
                if ($scope.navbarItemsLeft==undefined) $scope.navbarItemsLeft = [];
                if ($scope.navbarItemsRight==undefined) $scope.navbarItemsRight = [];
                
                let widgetHandle = 'element-drag-handle';
                let setupDragula = $scope.gsrvc.setupDragula('app-menu-bag','app-menu-task-list',widgetHandle);
                let dragula_bag="app-menu-bag";
                let source_container_page = 'app-menu-page-list';
                
                let bag = dragulaService.find($rootScope, dragula_bag);
                if (!bag) {                
                
                   dragulaService.options($rootScope, dragula_bag, {
                                     revertOnSpill: true,
                                     removeOnSpill: false,
                                     direction:'horizontal',
                                     copy:function (el,source){
                                       return source.id==source.id==source_container_page;
                                      },
                                     accept:function (el,target, source) {
                                         return target.id!= source_container_page;
                                        
                                     },
                                     moves: function (el, container, handle) {
                                               return handle.classList.contains(drag_handle);
                                     }
                   
                   });
                    
                }
                
                bag.drake.on('drop',function(el,target,source,sibling){
                    let index;
                    
                    if (target.id==="navbarLeft" || target.id==="navbarRight")
                    {
                        if (sibling!==null) index = parseInt(sibling.id);
                        else
                        {
                            if (target.id==="navbarLeft") index = $scope.navbarItemsLeft.length;
                            else index = $scope.navbarItemsRight.length;
                        }
                        MenuService.setCurrentNavbarId(index);
                        $scope.lastIndex = index;
                        if ($scope.localVars.selectedNavbar!==undefined && $scope.localVars.selectedNavbar!==null)
                        {
                            if ($scope.localVars.selectedNavbar?.index<index) $scope.localVars.selectedNavbar.index=index-1;
                            else $scope.localVars.selectedNavbar.index=index;
                        }
                    
                    }else
                    {
                        if (sibling!==null) index = parseInt(sibling.id);
                        else index = $scope.localVars.selectedNavbar.layout.length;
                        $scope.lastIndex = index;
                        if ($scope.localVars.selectedMenu!==undefined && $scope.localVars.selectedMenu!==null)
                        {
                            if ($scope.localVars.selectedMenu?.index<index) $scope.localVars.selectedMenu.index=index-1;
                            else $scope.localVars.selectedMenu.index=index;
                        }
                    
                    }
                });
                
                bag.drake.on('drop-model',function(el,target,source){
          
                    if (target.id==="navbarLeft") $scope.localVars.getNavbarDetails($scope.navbarItemsLeft,$scope.lastIndex); 
                    if (target.id==="navbarRight")  $scope.localVars.getNavbarDetails($scope.navbarItemsRight,$scope.lastIndex);
                    if (target.id==="sideMenu") $scope.localVars.getMenuDetails($scope.localVars.selectedNavbar.layout,$scope.lastIndex);
                });
                                
                 
                  bag.drake.on('$destroy', function() {
                    dragulaService.destroy($rootScope, dragula_bag);
                  });
     
                
                $scope.localVars.getNavbarDetails = function(navbar,index)
                {
                  if (navbar.length===index && navbar.length>0)    index--;
                  $scope.localVars.showNavbarDetails = false;
                  $scope.localVars.showMenuDetails = false;
                  $scope.localVars.selectedNavbar  = navbar[index];
                  
                  if ($scope.localVars.selectedNavbar != undefined)    {   
                          $scope.localVars.selectedNavbar.index=index;
                          $scope.localVars.selectedMenu = null;
                    
                          if ($scope.localVars.selectedNavbar.Target==undefined) $scope.localVars.selectedNavbar.Target = {id:navbar[index].id,name:navbar[index].name};//TODO - fec ima target ako dobije sa servera
                          if ($scope.localVars.selectedNavbar.katalyonode_menus_Name == undefined) $scope.localVars.selectedNavbar.katalyonode_menus_Name = navbar[index].name;   
                          if ($scope.localVars.selectedNavbar.layout==undefined) $scope.localVars.selectedNavbar.layout = [];
                        
                          var menuId = $scope.localVars.selectedNavbar.katalyonode_menus_id;
                          
                           //debugger
                          if (menuId!=null) {
                               // MenuService.getUsersGroupsForMenu(menuId,lang).then(
                                 //   function(data) {
                                          //$scope.localVars.selectedNavbar.Users = data.Users;
                                         // $scope.localVars.selectedNavbar.Groups = data.Groups;     
                                          $scope.localVars.selectedNavbar.UsersGroupsDownloaded = true;
                                          $scope.localVars.showNavbarDetails = true;
                                         $scope.localVars.showNavbarDetailsFirst = true;
                                     
                                //    },function(error){    
                                 //     MessagingService.addMessage(error.msg,'error');
                              //  });
                          } else {
                                  $timeout(function(){
                                    $scope.localVars.selectedNavbar.UsersGroupsDownloaded = true;
                                    $scope.localVars.showNavbarDetails = true;
                                    $scope.localVars.showNavbarDetailsFirst = true;
                                    $scope.selectedUsers =[]
                                     $scope.selectedGroups =[]
                                })          
                          }
                    
                    
                  }
                };
                
              
                
                $scope.localVars.getMenuDetails = function(menu,index)
                {
                  //$scope.localVars.showMenuDetails = false;
                  if (menu.length===index) index--;
                  $scope.localVars.selectedMenu  = menu[index];
                  $scope.localVars.selectedMenu.index=index;
                  $scope.localVars.showMenuDetails = false;
                  $scope.localVars.showNavbarDetails = false;
                  if ($scope.localVars.selectedMenu.Target==undefined) $scope.localVars.selectedMenu.Target = {id:menu[index].id,name:menu[index].name};
                  if ($scope.localVars.selectedMenu.katalyonode_menus_Name==undefined) $scope.localVars.selectedMenu.katalyonode_menus_Name = menu[index].name;
                  
                  
                   $timeout(function(){    
                        $scope.localVars.showMenuDetails = true;
                    })
                    
                };
                
                 $scope.localVars.deleteNavbar = function(navbar,index,type) {
                   
                   if (type==="left") {
                       //debugger
                        if (navbar.katalyonode_menus_id!==null && navbar.katalyonode_menus_id!==undefined) {
                            if (navbar.deleteItem) 
                                    navbar.deleteItem = false;
                            else navbar.deleteItem = true;                     
                        }
                        else {
                            $scope.navbarItemsLeft.splice(index,1);
                            $scope.localVars.selectedNavbar = null;
                        }
                           
                   }   else // not left
                   {
                    if (navbar.katalyonode_menus_id!==null) 
                        navbar.deleteItem = true;
                    else  {
                        $scope.navbarItemsRight.splice(index,1);
                        $scope.localVars.selectedNavbar = null;
                        $scope.localVars.showNavbarDetails = false;
                    }
                    
                   }
                  };
                  
                  $scope.localVars.deleteMenu = function(menu,index) {
                   
                    if (menu.katalyonode_menus_id!==null && menu.katalyonode_menus_id!==undefined)
                    
                    {
                       if (menu.deleteItem) menu.deleteItem = false;
                       else menu.deleteItem = true;
                    }
                    else
                    {
                        $scope.localVars.selectedNavbar.layout.splice(index,1);
                        $scope.localVars.selectedMenu = null;
                        $scope.localVars.showMenuDetails = false;
                    }
                  };
                  
                $scope.activeLinkEditNavbarLeft = function(index) {
                   
                   for (let i=0;i<$scope.navbarItemsLeft.length;i++)
                   {
                    if (i==index)    return "active";
                    else "";
                
                   }
                   
                  };
                
                $scope.buildDataset = function(e)
                {
                   // e.target.tooltip("dispose");
                    $state.go("app.menu.resources.new",{'resource_type':'dataset'});
                };
                
                 $scope.buildPage = function(e)
                {
                   // e.target.tooltip("dispose");
                    $state.go("app.menu.resources.new",{'resource_type':'page'});
                };
                
                $scope.$watch('navbarItemsLeft',function(newValue,oldValue){
                   let a=newValue
                   //debugger
                }, true)
                
                $scope.saveMenu = function()
                {
                    //debugger //saveMenu
                    let navbarLeft = angular.copy($scope.navbarItemsLeft);
                    let navbarRight = angular.copy($scope.navbarItemsRight); 
                    
                    $scope.localVars.showNavbarDetails = false;
                    $scope.localVars.showMenuDetails = false;
                    $scope.localVars.selectedNavbar = null;
                    
                    MenuService.saveFullAppMenu($scope.resource.id, navbarLeft, navbarRight).then(function(data) {
                        //debugger  
                         $timeout(function(){
                                $scope.navbarItemsLeft = data.navbarItemsLeft;
                                $scope.navbarItemsRight = data.navbarItemsRight;
                            })
 
                        MessagingService.addMessage(data.msg,'success');
                        
                    },
                    function(error){    
                        MessagingService.addMessage(error.msg,'error');
                    });
                };
                //get pages
                ResourcesDataService.getResourceDefListByType(lang,'page').then(function(data) {
										
					$scope.localVars.pageList = data.data;
                    for (let i=0;i<$scope.localVars.pageList.length;i++)
                    {
                       $scope.localVars.pageList[i].katalyonode_menus_PopulateType="navbar"; 
                    }
                    $scope.localVars.pagesLoaded=true;    
											 
                    },function(error){    
                    MessagingService.addMessage(error.msg,'error');
				});
                //get pages
            
            }],
            link: function (scope, elem, attrs) {
              
              if (scope.navbarItemsRight.length==0)
              {
                let settings = {id:null,name:"Settings",katalyonode_menus_id:null,katalyonode_menus_Name:"Settings",katalyonode_menus_MenuPosition:"navbarright",katalyonode_menus_Icon:"fa fa-cog",katalyonode_menus_MenuType:2};
                scope.navbarItemsRight.push(settings);
                
                }
            }
        }
  
};