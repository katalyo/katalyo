'use strict';

export function appMessages () {
        return {
            restrict: 'EA',
            templateUrl: 'ktdirectivetemplates/appmsg.html',
            //replace: true,
           // transclude:true,
            scope: {
               messageList : '='

            },
            controller: function($scope,$sce,MessagingService)
            {
                 $scope.msvc = MessagingService;
                  $scope.messageList = $scope.msvc.getMessages();
            
            $scope.showMessageList= false;
            
           
            
            $scope.closeAllClicked = function(){
             
                $scope.closeAllClickedVar = true;
                $scope.msvc.removeAllMessages();
               setTimeout(function(){$scope.closeAllClickedVar=false},2000);
        
            };
            
             $scope.closeAlert = function(id){
             
                $scope.messageList[id].show = false;
             
                var visible = $scope.msvc.checkForVisibleMessages();
             
                if (visible==0) $scope.msvc.removeAllMessages();
        
            };
            
            $scope.countVisible = function(){
              
                var countVisibleMsg=0;
                for (var i=0;i< $scope.messageList.length;i++)
                {
                  if ($scope.messageList[i].show) countVisibleMsg++;
                  
                }
                return countVisibleMsg;
            }
            
            
             $scope.convertToHtml = function(msg){
               return $sce.trustAsHtml(msg);
            };
            },
            link: function (scope, element, attrs) {
            
            
          
            
           


            }
        };
};
