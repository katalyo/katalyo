'use strict';

export function ktNumber () {
        return {
            restrict: 'A',
		controller: function ($scope,$rootScope,CodesService,GridsterService,$templateCache,$compile,$element) {
                
                 $scope.gsrvc = GridsterService;
                 $scope.fd.localVars =  {};
                 $scope[$scope.fd.id]  = {'fd': $scope.fd}
                 if ($scope.formData==undefined)  $scope.formData = [{}];
                 $scope.fd.colLength=[];
                 for (let i=0;i<24;i++) $scope.fd.colLength.push('col-'+(i+1));
                
                 if ($scope.fd.Parameters==undefined) $scope.fd.Parameters = {};
                 
                 $scope.fd.setStep = function() {
                  $scope.fd.stepValue =parseFloat(Math.pow(0.1,$scope.fd.Parameters.NoOfDecimals)).toFixed($scope.fd.Parameters.NoOfDecimals);
                 }; 
                
                if ($scope.fd.stepValue==null || $scope.fd.stepValue==undefined) $scope.fd.setStep();
               
               
               
                $scope.fd.SetDefault = function()
                {
                    $scope.fd.DefaultValue = {value:$scope.formData[0][$scope.fd.FieldName]};
                }
                
                
                
                $scope.fd.localVars.search=[
			  	{id:1,filter:'gt',code_name:'gte',name:"Greater than",icon:"fa fa-chevron-right"},
			  	{id:2,filter:'between',code_name:'between',name:"Between",icon:"fa fa-expand"},
			  	{id:3,filter:'lt',code_name:'lte',name:"Lower than",icon:"fa fa-chevron-left"},
			  	{id:4,filter:'eq',code_name:'eq',name:"Equals",icon:"fa fa-bars"} ];
                

                $scope.fd.localVars.processFilters = function(filterList)
                {
                    for (let i=0;i< $scope[$scope.fd.id]['fd'].localVars.search.length;i++)
                    {
                        let pos = filterList.map(function(e) { return e.id; }).indexOf( $scope[$scope.fd.id]['fd'].localVars.search[i].code_name);
                        if (pos>=0)  $scope[$scope.fd.id]['fd'].localVars.search[i].name = filterList[pos].name
                    }
                   
                    
                    if ($scope.fd.Search===undefined || $scope.fd.Search===null)   $scope.fd.Search=$scope.fd.localVars.search[3];//initialize to equals
                    if ($scope.fd.Search)  $scope.fd.Search.Value=$scope.formData[0][$scope.fd.FieldName]; 

                    $scope[$scope.fd.id]['fd'].localVars.filterReady=true
                }

                if ($scope.showSearchFilters || $scope.resourceDefinition.form_definition)
                {
                    let lang=$rootScope.globals.currentUser.lang;
                    CodesService.getCodesByName('search_filters',lang).then(function (data) {
                                //debugger;
                                //$scope.fd.localVars.processFilters(data.data)
                                $scope[$scope.fd.id]['fd'].localVars.processFilters(data.data); 
                    });
                }

                let itemValue = null;
                if ($scope.formData!=undefined)
                {
                        if ($scope.formData.length==0) $scope.formData=[{}];
                        itemValue=$scope.formData[0][$scope.fd.FieldName]
                        
                }else $scope.formData=[{}];
                
                let itemValueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+'-numberValueStateName'+$scope.fd.PresentationId,itemValue);
                let itemValueStateName = itemValueState.subject.name;
                
                
                let value_changed = function(data)
                {
                    itemValueState.subject.setStateChanges = data;
                    itemValueState.subject.notifyObservers("valueChanged");
                };
                
                let action_value_changed = $scope.ksm.addAction(itemValueState,"valueChanged",value_changed);
               
                $scope.on_blur_action=function()
                {
                        
                        $scope.ksm.executeAction(itemValueState,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                        //$scope.fd.DefaultValue = $scope.formData[0][$scope.fd.FieldName];
                };
               
                
                
                let apply_value = function(data) {
                        if (data!=undefined) $scope.formData[0][$scope.fd.FieldName] = data;
                };
                
                if ($scope.fd?.Parameters?.DependentFieldId!=undefined && $scope.fd?.Parameters?.DependentFieldId!=null)   
                {
                    if ($scope.fd.Parameters.DependentFieldId!="")
                    {
                        let dependent_field = "Field"+$scope.fd.PresentationId;
                        let subscribedSubject = $scope.resourceDefinition.page_state_id+'-'+$scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                        let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
                        let state = $scope.ksm.getState(subscribedSubject);
                        if (state.status=="ok") $scope.formData[0][$scope.fd.FieldName] = state.state;
                    }
                }
                
                
                
                
                
                
                 
                $scope.fd.localVars.formatNumber = function(inputNumber)
                {
                  
                  let formatedNumber = ""

                  if ($scope.formData.length>0 && $scope.formData[0][$scope.fd.FieldName]!=undefined)
                  {
                    let NoOfDecimals = 0
                    
                    if ($scope.fd.MaxLength>0) NoOfDecimals = $scope.fd.MaxLength

                    //if ($scope.fd.Parameters.UseFormat) 

                    if ($scope.fd.Dbtype!='integerfielddefinition') formatedNumber = inputNumber?.toLocaleString(navigator.language,{minimumFractionDigits:NoOfDecimals,maximumFractionDigits:NoOfDecimals});
                    else formatedNumber=inputNumber?.toString()
                    
                  } 

                  return formatedNumber
                }
                
               
                
                 $scope.fd.formatNumberBlockchain = function(input)
                {
                  let formatedNumber;
                  let web3;
                  if ($scope.fd.Parameters.ConvertBlockchainFormat)
                  {
                        if (input!=undefined)
                        {
                               

                                if (window.ethereum)
                                {
                                  web3 = new Web3(window.ethereum);
                                }else if (window.web3)
                                {
                                  web3 = new Web3(window.web3);
                                   //console.log(web3);
                                }
                                
                                else if ($rootScope.globals.currentUser.web3Infura)
                                {
                                   web3 = new Web3($rootScope.globals.currentUser.web3Infura);     
                                } else
                                {
                                   console.log('web3 provider not available');
                                   $scope.fd.WalletError = 'web3 provider not available';
                                   return input;
                                }
                                if ($scope.fd.Parameters.FromArray!=undefined) input = $scope.formData[0][$scope.fd.Parameters.SourceFieldName][$scope.fd.Parameters.FromArray];
                                
                                formatedNumber = web3.utils.fromWei(input.toLocaleString('fullwide', {useGrouping:false}), 'ether').toLocaleString(undefined,{minimumFractionDigits:$scope.fd.Parameters.NoOfDecimals,maximumFractionDigits:18});
                                
                  
                        }else  formatedNumber = input;
                  }else formatedNumber=input;
                  if ($scope.fd.Parameters.ConvertToDays) formatedNumber = formatedNumber/86400;                              
                  return formatedNumber;
                }
                
                $scope.fd.localVars.formatNumber();
                let template;
                if ($scope.resourceDefinition.form_definition)  template = $templateCache.get('ktdirectivetemplates/ktNumber.html');
                else template = $templateCache.get('ktdirectivetemplates/ktNumberExe.html');
                $element.append($compile(template)($scope)); 
                
                
                 if (itemValueState.subject.currentStateIndex>=0)
                {
                       
                        $scope.formData[0][$scope.fd.FieldName] = itemValueState.subject.getState;
            
                }   
                
                if ($scope.formData[0][$scope.fd.FieldName]!=undefined) $scope.ksm.executeAction(itemValueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                
                
                
                
            },
             
            link: function (scope, elem, attrs) {
  
				scope.isOpen=true;
            
                scope.refreshSearchValue = function() {
                         if (scope.fd.Search) scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]
                }; 
                        
               
                
               
                scope.showProperties = function(event) {
                 scope.gsrvc.setFieldProperties(scope.fd,event); 
                };
             
                scope.applyNumFilters = function(index) {
				    scope.fd.Search=scope.fd.localVars.search[index];
                    if (scope.fd.Search && scope.formData!=undefined && scope.formData.length>0) {
                            scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 
                    }
                    if (scope.fd.Search.Value==undefined) scope.fd.Search.Value = "";		
                };

                if (scope.fd.UpdateWithDefault && !scope.fd.NoUpdate) scope.formData[0][scope.fd.FieldName] = scope.fd.DefaultValue?.value
       
            }
            
        }
};