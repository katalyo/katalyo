'use strict';

export function ktTaskProcExecute () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskProcExecute.html',
            scope:{
                   taskDefinition : "=",
                   dashboard: "=",
                   resourceDefinition:"=",
                   taskExecuteInstance: "=",
                   taskInitiateInstance: "=",
                   localVars: "=",
                   
      			},
                controller: function($scope,$stateParams,$cookies,$rootScope,TaskDataService,GridsterService,MessagingService,KatalyoStateManager){
                    
                $scope.CTRL ="ktTaskProcExecute"   
                if ($scope.fd==undefined) $scope.fd={};
                if ($scope.lVars==undefined) $scope.lVars={};
                $scope.gsrvc = GridsterService;
                 var lang=$rootScope.globals.currentUser.lang;
                 if ($scope.localVars==undefined) $scope.localVars={};
                 $scope.lVars.isCollapsed3=true;
                 $scope.taskOutcomes = $scope.localVars.taskOutcomes;   
                 $scope.executeTaskDataLoaded=false;
                 $scope.taskDefinition.page_state_id=$scope.localVars.page_state_id;

                 if ($scope.taskDefinition.page_state_id===undefined)  $scope.taskDefinition.page_state_id=$scope.resourceDefinition.page_state_id;
                 
                $scope.fromStateName=$stateParams.fromStateName;
                $scope.fromStateParams=$stateParams.fromStateParams;
                $scope.cookieFromStateName=$cookies.getObject('executeTaskFromStateName');
                $scope.cookieFromStateParams=$cookies.getObject('executeTaskFromStateParams');
                $scope.noBackbutton = false;
                $scope.backBtnFromCookie=false;
                $scope.ksm = KatalyoStateManager;
                

                let executeTaskState= $scope.ksm.addSubject('executeTaskStateName'+$scope.taskDefinition.transaction_id,$scope.taskDefinition.task_execute_id);
                let executeTaskStateName = executeTaskState.subject.name;

                 let task_executed = function(data)
                {
                    executeTaskState.subject.setStateChanges = data;
                    executeTaskState.subject.notifyObservers("task_executed");
                    
                };
                
                let action_task_executed = $scope.ksm.addAction(executeTaskStateName,"task_executed",task_executed);

               if ($scope.taskInitiateInstance.length>0)
               {
                $scope.task_initiate_id = $scope.taskInitiateInstance[0].id;
                $scope.taskDefinition.taskInitiateInstance = $scope.taskInitiateInstance[0];
               }
               else $scope.task_initiate_id = 0;
               
               if ($scope.taskExecuteInstance.length>0)
               {
                $scope.task_execute_id = $scope.taskExecuteInstance[0].id;
                $scope.taskDefinition.taskExecuteInstance = $scope.taskExecuteInstance[0];
               }
               else $scope.task_execute_id=0;
                if ($scope.fromStateName==undefined || $scope.fromStateName=="")
                {
                    if ($scope.cookieFromStateName==undefined || $scope.cookieFromStateName=="")
                    {
                      $scope.noBackButton = true;
                    }else
                    {
                      $scope.fromStateName =  $scope.cookieFromStateName;
                      $scope.backBtnFromCookie = true;
                    }
                    
                  
                }else{
                  $cookies.putObject('executeTaskFromStateName',$scope.fromStateName);
                  
                }
                
                if ($scope.fromStateParams==undefined)
                {
                    if ($scope.cookieFromStateParams!=undefined)
                    {
                        if ($scope.backBtnFromCookie)
                        {
                          $scope.fromStateParams =  $scope.cookieFromStateParams;
                        }
                    }
                    
                }else if ($scope.cookieFromStateParams==undefined)
                {
                  $cookies.putObject('executeTaskFromStateParams',$scope.fromStateParams);
                  
                }else
                {
                    if ($scope.backBtnFromCookie)
                    {
                  
                      $scope.fromStateParams =  $scope.cookieFromStateParams;
                  
                    }else
                    {
                        $cookies.putObject('executeTaskFromStateParams',$scope.fromStateParams);    
                    }
                }
                
                $scope.gridsterType="task";
                //$scope.gridsterExeFormOpts = GridsterService.getGridsterOptions();
                $scope.formType="e";
               
                $scope.isCollapsedTaskDetails=true;
                
                $scope.lVars.changeIsCollapsed3 = function()
                    {
                     if ($scope.lVars.isCollapsed3) $scope.lVars.isCollapsed3 = false;
                     else $scope.lVars.isCollapsed3 = true;
               
                    }
                
                
                $scope.setForm = function(form)
                    {
                        $scope.forma = form;
               
                    }
                
                $scope.FormLockUnlock = function()
                {
                  $scope.resourceDefinition.formParams.formLocked=!$scope.resourceDefinition.formParams.formLocked;
               
                }
                
                $scope.goBack = function(){
                                    $state.go($scope.fromStateName,$scope.fromStateParams);
                             
                };
                
                $scope.returnToPreviousTask = function(){
                
                    $scope.gsrvc.setOpenTaskInNewPage(false);
                             
                };
                
                
                $scope.fd.CancelClicked = function()
                {
                       $scope.taskDefinition.proc_type = 0;
                       $scope.taskDefinition.inlineInitiateFormLoaded=false;
                       $scope.taskDefinition.showInlineTask = false;
                       $scope.localVars.showInlineTask = false;
                       $scope.taskDefinition.inlineTaskInitiated = false;
                       $scope.$emit('InlineTaskClosed',$scope.localVars.taskId);
                };
            
                
                
                $scope.ExecuteTaskAction = function(action){
                     
                     
                        var lang=$rootScope.globals.currentUser.lang;
                      
                       TaskDataService.ExecuteTaskAction($scope.localVars.task_initiate_id,$scope.localVars.task_execute_id,action,lang).then(function(data) {
                 
                                             
                                              $scope.taskInitiateInstance =data.data.data.taskInitiateInstance[0];
                                              $scope.taskExecuteInstance =data.data.data.taskExecuteInstance[0];
                                              if ($scope.taskExecuteInstance!=undefined)
                                                {
                                                $scope.taskDefinition.task_execute_id = $scope.taskExecuteInstance.id;
                                             
                                                }
                                                
                                              if (action=="claim")
                                              
                                              {
                                                //$scope.pageChanged();
                                                $rootScope.$broadcast('RefreshData0',data.data.ret_value);
                                              }
                                              
                                              MessagingService.addMessage(data.data.msg,'success');
                                             
               
                                        },function(error){
                                          
                                      
                                            MessagingService.addMessage(error.msg,'error');
                                      });
                }
                
                
                 $scope.$on('ExecuteTask'+$scope.task_execute_id,function(event,data){
                        
                        let form = $scope.forma;
                        form.$setSubmitted()
                        //if (form.$submitted) $scope.localVars.setSubmitted(form);
                        $scope.localVars.ExecuteTask(data.outcomeId,form);
                    });
                
                
                
                
                $scope.localVars.ExecuteTask = function(outcomeId,form){
                        
                        
                    var lang=$rootScope.globals.currentUser.lang;
                   
                    if (form.$valid) {
                      
                      
                        TaskDataService.executeTask($scope.task_execute_id,$scope.dashboard.data,outcomeId,lang).then(function(data) {
                                      
                                if (data?.data?.msg!=="") MessagingService.addMessage(data.data.msg,'success');
                                $rootScope.$broadcast($scope.taskDefinition.page_state_id+'-'+"TaskPostProcess"+$scope.taskDefinition.transaction_id,data.data.ret_value);
                                $rootScope.$broadcast('ExecuteTaskPostProcess'+$scope.task_execute_id,data.data.ret_value);
                                  
                                   let taskStateName = "executeTaskStateName"+$scope.taskDefinition.transaction_id;
                                   $scope.ksm.executeAction(taskStateName,"task_executed",{task_id:$scope.taskDefinition.task_execute_id,action:'executetask'});

                                },function(error){
                                  
                                  MessagingService.addMessage(error.msg,'error');           
                                            
                        });
                          
                    } // if (isValid)
                    else
                    {
                      MessagingService.addMessage($scope.resourceDefinition.ParametersLang.InvalidFormMessage,'warning');
                      
                    }
                };
                   // 
               
               $scope.localVars.setSubmitted = function (form) {
                  angular.forEach(form, function(item) {
                  if(item && item.$$parentForm === form && item.$setSubmitted) {
                      $scope.localVars.setSubmitted(item);
                  }
                  });
                } 
                    
                    },
             link: function (scope, elem, attrs) {
                
                
              }
        }             
};