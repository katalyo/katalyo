'use strict';

export function ktTaskformlayout () {
        return {
            restrict: 'A',
            
			controller: function ($scope,dragulaService,$rootScope,WidgetDataExchangeService,GridsterService,$compile,$element,$templateCache) {
                    
                $scope.CTRL="ktTaskformlayout"
				$scope.removeField = function (index){
						// console.log("REMOVE FIELD "+index);
						if (index > -1) {
                        
                            WidgetDataExchangeService.deRegisterWidget($scope.inputArray.layout[index].LocalId);
                            if ($scope.inputArray.layout[index].Saved==true && $scope.inputArray.layout[index].PresentationId!=null && $scope.inputArray.layout[index].PresentationId!=undefined) {
								
                                 $scope.inputArray.layout[index].Status="Deleted";
							 }else
                             {
                                $scope.inputArray.layout.splice(index,1);
                                
                             }
                             
						}

				};
                
       
                let template;
               if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktTaskFormLayout.html');
               else template = $templateCache.get('ktdirectivetemplates/ktTaskFormLayoutExe.html');
          
               $element.append($compile(template)($scope));

			},
			 link: function (scope, elem, attrs) {

                if (scope.inputArray.layout==undefined || scope.inputArray.layout==null) scope.inputArray.layout = [];
          
            }
        };
};