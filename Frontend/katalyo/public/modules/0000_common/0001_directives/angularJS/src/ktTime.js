'use strict';

export function ktTime () {
        return {
                restrict: 'A',
                controller: function ($scope,GridsterService,KatalyoStateManager,$templateCache,$element,$compile) {
                        
                        $scope.gsrvc = GridsterService;
                         $scope.showProperties = function()
                          {
                             $scope.gsrvc.setFieldProperties($scope.fd);
                          };
                      
                        let template;
                        if ($scope.resourceDefinition.form_definition)  template = $templateCache.get('ktdirectivetemplates/ktTime.html');
                        else template = $templateCache.get('ktdirectivetemplates/ktTimeExe.html');
                        
                         $element.append($compile(template)($scope));  

                        },
        link: function (scope, elem, attrs,ngModelController) {
				
                  if (scope.formData===undefined) scope.formData = [{'Field':0}];
                  if (scope.formParams===undefined) scope.formParams = {formViewType:1};
                  if (scope.formParamsParent===undefined) scope.formParamsParent = {formViewType:1};
                  if (scope.fd.LabelLength === undefined) scope.fd.labelLength= 14;
                  if (scope.fd.localVars===undefined) scope.fd.localVars={}
                  if (scope.fd.ParametersLang===undefined) scope.fd.ParametersLang={format1:'HH:mm'};
                  
                  scope.fd.colLength = [];
                  for (var i=0;i<24;i++) scope.fd.colLength.push('col-lg-'+(i+1));
                     
                  scope.fd.timeFormatList = [{id:1,name:'HH:mm',mask:'99:99'},{id:2,name:'HH:mm:ss',mask:'99:99:99'},{id:3,name:'HH:mm'},{id:4,name:'HH:mm:ss:sss'}];
                  
                  if (scope.fd.ParametersLang!=undefined && scope.fd.ParametersLang!=null)
                  {
                     
                      var posFormatTime = scope.fd.timeFormatList.map(function(e) { return e.name.toString(); }).indexOf(scope.fd.ParametersLang.format1);
                          
                      if (posFormatTime>=0){
                         
                        scope.fd.ParametersLang.format1=scope.fd.timeFormatList[posFormatTime];
                                              
                      }
                         
                      scope.fd.localVars.format = scope.fd.ParametersLang.format1           

                  }  else scope.fd.localVars.format = scope.fd.timeFormatList[0]



                scope.search=[                    
                {id:1,filter:'gt',name:"From <time>",icon:"fa fa-chevron-right"},
                {id:2,filter:'lt',name:"To <time>",icon:"fa fa-chevron-left"},
                {id:3,filter:'eq',name:"On <time>",icon:"fa fa-bars"},      
                {id:4,filter:'between',name:"Between <time1> and <time2>",icon:"fa fa-expand"} ];
 
    
 
                if (scope.showSearchFilters)  {
                        if (scope.fd.Search===undefined || scope.fd.Search===null)   scope.fd.Search=scope.search[2];//initialize to equals
                        if (scope.fd.Search)  scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 
                }
                scope.CTRL='ktTime';

        
                scope.refreshSearchValue = function() {
                        
                          if (scope.fd.Search && scope.formData!=undefined && scope.formData.length>0) {
                                    scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 
                            }    
                } 
        
                scope.applyTimeFilters = function(index) {

                  scope.fd.Search = scope.search[index]
                  if (scope.fd.Search && scope.formData!=undefined && scope.formData.length>0) {
                            scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 
                  }
                  if (scope.fd.Search.Value==undefined) scope.fd.Search.Value = "";

                  var i=scope.search[index].id; 

                  if (scope.search[index].id==4) scope.search.showSecondTime=true  
                  else  scope.search.showSecondTime=false
                                
                };

                
        }

   }

}

export function ktTimeParser () {
        return {
                restrict: 'A',
                require:'ngModel',
       
        link: function (scope, elem, attrs,ngModelController) {

                function formatInputTime(value){

                        let model_value

                        if (typeof value ==="string") model_value = new Date(value)
                        else model_value = value


                        return model_value

                }


                ngModelController.$formatters.push(formatInputTime)

            
        }

   }


}
