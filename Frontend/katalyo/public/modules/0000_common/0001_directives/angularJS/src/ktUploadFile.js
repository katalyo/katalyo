'use strict';

export function ktUploadFile ($timeout,uiUploader,MessagingService,WidgetDataExchangeService,ResourcesDataService,TaskDataService,$cookies,$rootScope,KatalyoStateManager) {
        return {
        restrict: 'A', 
             templateUrl: 'ktdirectivetemplates/ktUploadFile.html',
             controller: function ($scope, $element,$rootScope,$uibModal,MessagingService,ProcessingService,$log,GridsterService,WidgetDataExchangeService,KatalyoStateManager,$timeout,$stateParams) {

                //console.log("U kontroleru");
                $scope.gsrvc = GridsterService
                $scope.animationsEnabled = true;
                if ($scope.fd.localVars==undefined)  $scope.fd.localVars = {datasetMapperType:0};
                $scope.fd.localVars.files = [];
                $scope.fd.localVars.hideAll=true
                $scope.fd.uploadReady = true

                $scope.fd.localVars.showMenu = false
                
                 $scope.fd.localVars.ShowUploadFile = true
                
                $scope.fd.localVars.showAll= function(x) {
                    //debugger
                    // $scope.fd.localVars.hideAll = x
                      for (const f of $scope.fd.localVars.files) {
                              f.hideForm = x
                          }   
                }

            $scope.fd.localVars.getPublishedResourceById = function(id,index) 
 
            {
    
                let pos
                if ($scope.fd.Parameters.rules[index].FormDef===undefined || $scope.fd.Parameters.rules[index].FormDef===null)
                {
                
                  pos = $scope.fd.listOfFileTypes.map(function(e) { return e.id; }).indexOf(id);
                  
                    if (pos>=0){
                        ResourcesDataService.GetPublishedResourceById(id).then(function (data) {

                                $scope.fd.Parameters.rules[index].FormDef = data.data.Form
                                $scope.fd.Related = $scope.fd.Parameters.rules[index].ResourceDefId
                                //$scope.fd.Related = $scope.fd.localVars.listOfFileTypes[pos].ResourceDefId

                     
                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                        })

                    }
             
       
                }
            }

               
                $scope.taskId=$scope.resourceDefinition.ResourceDefId;
                if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
                if ($scope.taskId==undefined) $scope.taskId=0;
                $scope.fd.localVars.taskId=$scope.taskId;
                //$scope.fd.ParentDatasetDefId=$scope.fd.Related;
                $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
                $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
                $scope.source_type=$scope.resourceDefinition.source_type;
                $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
                $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                $scope.formDefinition = $scope.resourceDefinition.form_definition;
                $scope.fd.localVars.resourceRecordId=0;
                $scope.fd.localVars.resourceDefinition = $scope.resourceDefinition
                $scope.formDefinition = $scope.formDefinition || false
                $scope.taskInitiateId = $scope.taskInitiateId || 0
                $scope.taskExecuteId = $scope.taskExecuteId || 0
                $scope.prevTaskExeId = $scope.prevTaskExeId || 0
                $scope.fd.colLength=[];
                for (let i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));

                $scope.fd.localVars.toggleDropdown = function(){
                           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                }
                  
                if ($scope.resourceDefinition.form_type=='i')
                   {
                         $scope.fd.taskInstanceId = $scope.resourceDefinition.task_initiate_id;
                   }else
                   {
                        $scope.fd.taskInstanceId = $scope.resourceDefinition.task_execute_id;
                   }

                $scope.fd.localVars.showProperties=function(datasetMapperType)
                {
                 
                        if ($scope.fd.localVars.datasetMapperType==undefined)
                        {
                           $scope.fd.localVars.datasetMapperType = datasetMapperType;
                        }else
                        {
                           if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) $scope.fd.localVars.datasetMapperType = datasetMapperType;
                           else $scope.fd.localVars.datasetMapperType=0;
                        }
                 
                };
            
		       
            var out_fields = ['resourceId'];
            var widgetDefinition = {name: 'ktUploadFileWidget',
            widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.fd.Related, output:out_fields, };
            $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
            $scope.fd.localVars.LocalId=$scope.fd.localVars.localWidgetId;
            
            $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
            
            $scope.ksm = KatalyoStateManager;
            let dependent_field_pid = $scope.fd.LocalId;
            let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
            let dependent_field_pid2
          
                

		 $scope.fd.showMetadata = function(fileIndex,fileId,form_definition){ 

                                        $scope.fd.localVars.files[fileIndex].formParams = {formViewType:2};
                                        $scope.fd.localVars.files[fileIndex].gridsterType = "file"
                                        if ($scope.fd.localVars.files[fileIndex].dashboard===undefined) $scope.fd.localVars.files[fileIndex].dashboard={}
                                        if (!$scope.fd.localVars.files[fileIndex].uploadSuccess || !$scope.fd.localVars.files[fileIndex].metaSuccess)
                                        {
                                            $scope.fd.localVars.files[fileIndex].dashboard.formData=[{}];
					                        
                                        }else{
                                           
                                            $scope.fd.localVars.files[fileIndex].hideForm = true
                                            
                                        }
                                        $scope.fd.localVars.files[fileIndex].dashboard.widgets={layout:form_definition}
                                        $scope.fd.localVars.files[fileIndex].formLoaded = true;
                                        

			  };
      
      				
                        // FUNCTION ONSELECT CALLBACK II
                $scope.fd.onSelectCallback2 = function (item, model){
                        $scope.fd.selectedFileName=item.name
                        $scope.fd.selectedFileType=item

                }//onSelectCallback II
                 
				 
				 $scope.fd.addFilesClick = function()

                 {

                    document.getElementById('fileWidget'+$scope.fd.PresentationId).click()
                 }
				 
                // FUNCTION - ON_SELECT_FILETYPE 
                // NAKON STO SE ZA POJEDINAcNI FILE ODABERE FILE-TYPE  --> $scope.resourceId
                
                $scope.fd.onSelectFileType = function (item,fileIndex){
                        var selectedFileTypeId=item.ResourceDefId;
                        let form_def
                       
                         $scope.isResourceSelected=true;
                        if ($scope.fd.localVars.files[fileIndex].fileResourceID!=selectedFileTypeId) {
                         $scope.fd.localVars.files[fileIndex].fileResourceID = selectedFileTypeId;
                         $scope.fd.localVars.files[fileIndex].filePublishId = item.id;
                         $scope.fd.localVars.files[fileIndex].widgetId = $scope.fd.PresentationId;
                         $scope.fd.localVars.files[fileIndex].formLoaded = false;
                         $scope.fd.localVars.files[fileIndex].fileTypeSelected = true;
                         //form_def = item.Form -- form is not here
                        }
                
                        $scope.fileIndex=fileIndex;
                        
                                        
                        
                        $scope.fd.showMetadata(fileIndex,$scope.fd.localVars.files[fileIndex].fileResourceID,form_def );
                        
                        var err=$scope.fd.testRules();
        
                }//onSelectFileType
				 
				 
				    
				var lang = $rootScope.globals.currentUser.lang;
				var type="file"; // PROMJENITI - ZASAD JE HARD-CODED !!!!! MIGOR
					 
				
                    // poziva se iz ktUploadFile.html
                    $scope.fd.loadForm = function (index) {
                         
                        $scope.fd.selectedRuleIndex = null
                        $scope.fd.Parameters.rules[index].formData=[{}];
                        $scope.fd.LoadedForm = {layout:$scope.fd.Parameters.rules[index].FormDef}
                        $scope.fd.Parameters.rules[index].gridsterType = "file"
                        $scope.fd.Parameters.rules[index].formParams = {formViewType:1,showGridNavigation:true};

                       $timeout(function(){ 
                            $scope.fd.selectedRuleIndex = index
                            $scope.fd.Parameters.rules[index].formLoaded = true;
                       })
                    }

					// poziva se iz ktUploadFile.html
					$scope.fd.remove = function (index) {
						
						$scope.fd.Parameters.rules.splice(index,1);
						$scope.isOpen=true;
						var err=$scope.fd.testRules();	
					}


                    // POPUNI LISTU  lIST OF PUBLISHED FILE TYPES
                    ResourcesDataService.GetPublishedResourcesByType(type,'form').then(
                        function (data) {       
                            $scope.fd.listOfFileTypes = data.data;

                            if ($scope.fd.Parameters.rules.length>1) $scope.fd.showFileSelect = true
                                else if ($scope.fd.Parameters.rules.length==1){
                                $scope.fd.showFileSelect = false
                                $scope.fd.Related = $scope.fd.Parameters.rules[0].ResourceDefID
                                $scope.fd.PublishId = $scope.fd.Parameters.rules[0].id
                                $scope.fd.localVars.getPublishedResourceById($scope.fd.PublishId,0)
                            } 
                                     
                        },function(error){    
                            MessagingService.addMessage(error.msg,'error');
                        });
                	            
     $scope.fd.getPreviousTaskWidget  = function(deiType)
    {
      var pTaskPkg = WidgetDataExchangeService.getPrevTaskData($scope.resourceDefinition.prev_task_id);
             var datasetId=0,datasetRecordId=0,pTaskData,pTaskDataArray;
             if (!Array.isArray(pTaskPkg))
             {
                          pTaskDataArray = [pTaskPkg];
             }else
             {
                          pTaskDataArray = pTaskPkg;        
             }
             for (let l=0;l<pTaskDataArray.length;l++)
             {
                          pTaskData = pTaskDataArray[l];
                          if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
                          {
                          for (var i=0;i<pTaskData.DEI.length;i++)
                          {
                            if (pTaskData.DEI[i].name==deiType)
                            {
                              if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                              {
                                for (var j=0;j<pTaskData.DEI[i].value.length;j++)
                                {
                                 if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined) datasetId=pTaskData.DEI[i].value[j].value;
                                 if (pTaskData.DEI[i].value[j].name=="resourceRecordId" && pTaskData.DEI[i].value[j].value!=undefined) datasetRecordId=pTaskData.DEI[i].value[j].value;
                                 
                                  
                                }
                                   
                                  
                              }
                                
                            }
                              
                            }
                          }
             }
            
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };
	
    $scope.fd.localVars.ResetUpload = function() 
 
    {
        $scope.fd.localVars.files=[]
        $scope.fd.localVars.ShowUploadFile = true
        $scope.fd.localVars.setupFileElement()
    }
    
    $scope.fd.localVars.processTaskReturn = function(data)
    {

             if (data!=undefined){
                let rv
                if (Array.isArray(data))
                {
                 rv  = data
                }else rv =[data]

                  for(var i=0;i<rv.length;i++) {
                    
                    if ($scope.fd.PresentationId==rv[i].widgetId)
                    {
                      WidgetDataExchangeService.setData(rv[i]);
                     // $scope.fd.localVars.setField($scope.fd.localVars.dashboard.widgets.layout,'ReadOnly',true); //set all elements read-only
                    }
                  }
            }
            
        }


         $scope.$on('InitiateTaskPostProcess'+$scope.resourceDefinition.transaction_id,function(event, data){
            $scope.fd.localVars.processTaskReturn(data)
              
         });
            
        $scope.$on('ExecuteTaskPostProcess'+$scope.resourceDefinition.task_execute_id,function(event, data){
              
            $scope.fd.localVars.processTaskReturn(data)
            $scope.fd.localVars.ShowUploadFile = false
              
          });
     
			 },// controler_ktUploadFile_END
			

			//  ***************************************************  KT-UPLOAD-FILE   LINK FUNKCIJA *****************************
			require: '^form', 
			link: function (scope, elem, attrs,formCtrl) { 
                        
                    
                    if (scope.fd.localVars==undefined)  scope.fd.localVars = {datasetMapperType:0};
                
                    scope.fd.localVars.ShowUploadFile = true
                

                                if (scope.fd.Parameters==undefined || scope.fd.Parameters==null) scope.fd.Parameters={};
                                if (scope.fd.Parameters.rules==undefined || scope.fd.Parameters.rules==null) scope.fd.Parameters.rules=[];
                                
                                scope.fd.uiUploader = uiUploader;
                                if (scope.fd.PresentationId!=undefined) scope.fd.uiUploader.createUploader(scope.fd.PresentationId);
                                
                                				
                                //MIGOR TODO - prilagoditi widgetdefinition za uploader da salje i povezani related dataset
                                var out_fields = ['resourceId'];
                                var widgetDefinition = {DEI: [{'name':'resourceLinkR','value':[{name:'resourceId',value:scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'ktUploader',localWidgetId:scope.fd.LocalId,widgetId: scope.fd.PresentationId,taskId:  scope.lTaskId,resourceDefId : scope.selectedResource, output:out_fields, };
                                //var widgetDefinition = {DEI: [{'name':'resourceLinkR','value':false}],actions: [{name:'PrepareData',status:false}],name: 'ktUploader',localWidgetId:scope.fd.localId,widgetId: scope.fd.presentationId,taskId:  scope.lTaskId,resourceDefId : scope.selectedResource, output:out_fields, };
		  
                                scope.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition); //zamijeniti gdje se sve pojavljujue i staviti LOCAL TODO
                                scope.fd.LocalId=scope.localWidgetId;         
      
                                scope.fd.uiUploader.removeAll(scope.fd.PresentationId); //MIGOR TEMP- TREBA nekako obrisati inicijalne fajlove kod reigstracija ali ovdje ne radi !!!
                                //console.log(element.fd.localVars.files);
                                
                                scope.fd.localVars.setupFileElement = function()
                                {
                                    var element

                                    $timeout(function(){
                                        element = elem[0].getElementsByClassName('fileElement');
                
                                        element[0].addEventListener('change', function(e) {
                                            $timeout(function(){
                                                    var files = e.target.files;
                                                    scope.fd.uiUploader.addFiles(files,scope.fd.PresentationId);				
                                                    scope.fd.localVars.files =scope.fd.uiUploader.getFiles(scope.fd.PresentationId); 
                                                    //let fileIndex =  scope.fd.localVars.files.length -1
                                                    if (!scope.fd.showFileSelect) 
                                                    {
                                                        let form_def = scope.fd.Parameters.rules[0].FormDef
                                                        for(let fileIndex=0; fileIndex < scope.fd.localVars.files.length; fileIndex++) {
                                                            scope.fd.localVars.files[fileIndex].fileResourceID = scope.fd.Parameters.rules[0].fileType.ResourceDefId;
                                                            scope.fd.localVars.files[fileIndex].filePublishId = scope.fd.Parameters.rules[0].fileType.id;
                                                            scope.fd.localVars.files[fileIndex].widgetId = scope.fd.PresentationId;
                                                            scope.fd.localVars.files[fileIndex].formLoaded = false;
                                                            scope.fd.localVars.files[fileIndex].fileTypeSelected = true;
                                
                                                            scope.fd.showMetadata(fileIndex,scope.fd.localVars.files[fileIndex].fileResourceID,form_def);
                                                        }
                                                    }
                                                    var err=scope.fd.testRules();
                                            });
                                        });

                                    });
                                }    
    	
                                scope.fd.localVars.setupFileElement()
                        var fileCounter = 0;
			//scope.fd.Parameters.rules=[];
			if (scope.fd.MetaData==undefined)  scope.fd.MetaData=[];        
			
				
				// ADD RULE DEFINITION - poziva se iz ktUploader.html
				scope.fd.addRuleDefinition=function(name,min,max,minTotal){
					
					if (min==undefined) min=0;			
					
					for (var i=1, l=scope.fd.Parameters.rules.length; i<l; i++) 
								if (scope.fd.Parameters.rules[i].name==name) name='';
							
					if (name!=undefined && name != '') {
                        
                       
						scope.fd.Parameters.rules.push({'fileType':scope.fd.selectedFileType,'min':min,'max':max});

                        scope.fd.localVars.getPublishedResourceById(scope.fd.selectedFileType.id,scope.fd.Parameters.rules.length-1)
						//scope.fd.Parameters.rules.minTotal=minTotal;	
					}
				}
            
                scope.btn_remove = function(index,file) {
                       
                        scope.fd.uiUploader.removeFile(file,scope.fd.PresentationId);
                        var err=scope.fd.testRules();
                                
                };      
			

                scope.btn_clean = function() {
                   scope.fd.uiUploader.removeAll(scope.fd.PresentationId);
                };


        scope.fd.testRules = function(){
		  // debugger
                var errMsg=null;
                for (var i=0; i < scope.fd.Parameters.rules.length; i++) { //search rules
                        
                      /*  
                        var filesFound=0;
                        for(var j = 0; j < scope.fd.localVars.files.length; j++ ) {//search files
                                if (scope.fd.localVars.files[j].fileResourceID == scope.fd.Parameters.rules[i].name) filesFound=filesFound+1;
                        }
                        
                        NISAM SIGURAN STO SAM HTIO S OVIM POSTICI*/
                        let filesFound = scope.fd.localVars.files.length
                        
                        if (scope.fd.Parameters.rules[i].min!=undefined)
                            if (filesFound < scope.fd.Parameters.rules[i].min) {
                                formCtrl['uploadFileForm'+scope.fd.PresentationId].$setValidity('NotEnoughFiles', false);
                                scope.fd.ErrorMsg = scope.fd.Parameters?.ErrorNotEnoughFiles || ('You need to select at least '  + scope.fd.Parameters.rules[i].min + ' file!')                              
                            }	  else {
                                formCtrl['uploadFileForm'+scope.fd.PresentationId].$setValidity('NotEnoughFiles', true);  
                            }
                        
                        
                        if (scope.fd.Parameters.rules[i].max!=undefined )
                            if (filesFound > scope.fd.Parameters.rules[i].max) {
                                formCtrl['uploadFileForm'+scope.fd.PresentationId].$setValidity('TooManyFiles', false);
                                 scope.fd.ErrorMsg = scope.fd.Parameters?.ErrorTooManyFiles || ('More than '  + scope.fd.Parameters.rules[i].max + ' file(s) selected!')
                            }	else    {  formCtrl['uploadFileForm'+scope.fd.PresentationId].$setValidity('TooManyFiles', true);
                            }
                 
                }
					
		//if (scope.fd.Parameters.rules.length>0)
                       // if (scope.fd.localVars.files.length < scope.fd.Parameters.rules[0].min) formCtrl['uploadFileForm'+scope.fd.PresentationId].$setValidity('MinimumFilesNeeded', false); 
                        //else formCtrl['uploadFileForm'+scope.fd.PresentationId].$setValidity('MinimumFilesNeeded', true); }
  
		
                return errMsg;
					
         }//test_RULES
               
	scope.verify_files = function(){
        
                                 
                var csrf_token = $cookies.get('csrftoken'); 
			
                // ZA svaki FAJL pojedinaèno !
                scope.fd.uiUploader.startUpload(scope.fd.PresentationId,{  
                        url: 'api/verify-file/', 
                        headers: {'X-CSRFToken' : csrf_token },
                                //concurrency: 3,
                                // u data napuniti podatke vezane za fajl - fileType , ID resursa
                                //data: {form_data:file.form_data,resource_id:file.fileResourceID},
                                //data: {form_data:scope.form_data},
                                onProgress: function(file) {
                                        //$log.info(file.name + '=' + file.humanSize);
                                        $timeout(function() { file.status='Uploading...'; });
                                },
                                onCompleted: function(file, response,status) {
                                        
                                       
                                       if (status==200) {
                                                response = JSON.parse(response);
                                                file.uploadSuccess=true;
                                                if (response.jsonResponseEOS.rows.length>0)
                                                {
                                                        file.blockchainHash = response.jsonResponseEOS.rows[0].hash;
                                                        file.fileFound=true;
                                                }
                                                
                                                else
                                                {
                                                        MessagingService.addMessage("File not found!",'warning');
                                                        file.blockchainHash="";
                                                }
                                                file.eosResponse2 = JSON.stringify(response.jsonResponseEOS,null,2);
                                                file.fileHash = response.fileHash;
                                                
                                                
                                                    
                                        }
                                        else file.uploadSuccess=false;
                                }//on_completed
                        }); //startUpload                                                
                                                              
        }
     	//*******************************************************************************************************
	// 				kt-upload-file BTN_UPLOAD
	//*******************************************************************************************************
        scope.btn_upload = function() {//CHECK IF THIS IS USED OR NOT !!!
            				
        // CHECK THE RULES FIRST
        debugger 
		var errMsg=scope.fd.testRules();
        let upload_form = formCtrl['uploadFileForm'+scope.fd.PresentationId]
        upload_form.$setSubmitted()

		if (errMsg==null && upload_form.$valid) {
	
        		var csrf_token = $cookies.get('csrftoken'); //'miro';//$cookieStore.get('csrftoken');
			
        		// ZA svaki FAJL pojedinaèno !
        		scope.fd.uiUploader.startUpload(scope.fd.PresentationId,{  
				url: 'api/upload-file/'+scope.fd.PresentationId+'/', 
				headers: {'X-CSRFToken' : csrf_token,'Authorization':$rootScope.globals.currentUser.authdata },
					//concurrency: 3,
					// u data napuniti podatke vezane za fajl - fileType , ID resursa
					//data: {form_data:file.form_data,resource_id:file.fileResourceID},
                                        //data: {form_data:scope.form_data},
					onProgress: function(file) {
						//$log.info(file.name + '=' + file.humanSize);
                                                $timeout(function() { file.status='Uploading...'; });
                                        },
					onCompleted: function(file, response,status) {
                                                
                                              
                                               if (status==201) {
                                                
                                                        response = JSON.parse(response);
                                                        let file_id=response.file_id; 
                                                        
                                                        $timeout(function() { file.uploadSuccess=true;  });
                                                        file.eosResponse = JSON.stringify(response.jsonResponseEOS,null,2);
                                                        file.eosResponse2 = JSON.stringify(response.jsonResponseEOS2,null,2);
                                                        
                                                        if (file.dashboard.formData && (!scope.fd.Parameters.NoMeta || scope.fd.HasParentDataset))
                                                        {
                                                                var parentDataset=scope.fd.getPreviousTaskWidget("resourceLinkDatasetList");
      
                                                                if (parentDataset['datasetId']==undefined || parentDataset['datasetId']=="") parentDataset['datasetId'] = 0;
                                                                if (parentDataset['datasetRecordId']==undefined || parentDataset['datasetRecordId']=="") parentDataset['datasetRecordId'] = 0;
                                                                file.metaStatus='Saving metadata...'
                                                                ResourcesDataService.fileMetaUpdate(file_id,file.fileResourceID,file.filePublishId,scope.fd.taskInstanceId,scope.resourceDefinition.prev_task_id,parentDataset['datasetId'],parentDataset['datasetRecordId'],file.dashboard.formData,scope.fd).then(
                                                                                        
                                                                        function(data) {
                                                                                                                                                
                                                                                MessagingService.addMessage(data.data.msg,'success');
                                                                                file.metaSuccess=true;
                                                                        },function(error){
                                                                                MessagingService.addMessage(error.msg,'error');
                                                                                file.metaError=true;
                                                                                file.metaStatus = error.msg;
                                                                                                                                                
                                                                                                                                                                 
                                                                        });
                                                        } //IF
                                                            
                                                }else 

                                                {
                                                         $timeout(function() { 
                                                                file.uploadSuccess=false;        
                                                               
                                                        })
                                                }
                                        }//on_completed
                                }); //startUpload
		} else {
            MessagingService.addMessage(scope.fd.ErrorMsg,'warning');
		}
        }; // FUNCTION b.n_up1oad
        
        let process_upload = function(data)
        {
           if (scope.resourceDefinition.form_type=='i')
           {
                scope.fd.taskInstanceId = data.task_initiate_id;
           }else
           {
                scope.fd.taskInstanceId = data.task_execute_id;  
           }
           if (scope.fd.localVars.files.length>0 && scope.fd.Parameters.VerifyOnly) scope.verify_files();
           else scope.btn_upload();
           
        }  
        
        scope.ksm = KatalyoStateManager;
        let dependent_field = "Field"+scope.fd.PresentationId;
        let subscribedSubject = scope.resourceDefinition.page_state_id+'-'+"TaskPostProcess"+scope.resourceDefinition.transaction_id;
        let observer = scope.ksm.subscribe(subscribedSubject,dependent_field,"TaskPostProcess",process_upload);
        let state = scope.ksm.getState(subscribedSubject);
        
        // replaced broadcast with state manager
        scope.$on(scope.resourceDefinition.page_state_id+'-'+"TaskPostProcess"+scope.resourceDefinition.transaction_id,function(event, data){
           
           if (scope.resourceDefinition.form_type=='i')
           {
                scope.fd.taskInstanceId = data.task_initiate_id;
                if (scope.fd.taskInstanceId === undefined) scope.fd.taskInstanceId = scope.resourceDefinition.task_initiate_id;
           }else
           {
                scope.fd.taskInstanceId = data.task_execute_id;
                if (scope.fd.taskInstanceId === undefined) scope.fd.taskInstanceId = scope.resourceDefinition.task_execute_id;
           }
           if (scope.fd.localVars.files.length>0 && scope.fd.Parameters.VerifyOnly) scope.verify_files();
           else scope.btn_upload();
          });
          
          
         

     } //LINK_END 'ktUploadFile'
 

 };
};

export function ktFileDropzone(){
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var checkSize, isTypeValid, processDragOverOrEnter, validMimeTypes;
      
      processDragOverOrEnter = function(event) {
        if (event != null) {
          event.preventDefault();
        }
        event.dataTransfer.effectAllowed = 'copy';
        return false;
      };
      validMimeTypes = attrs.fileDropzone;

      checkSize = function(size) {
        var _ref;

        if (((_ref = attrs.maxFileSize) === (void 0) || _ref === '') || (size / 1024) / 1024 < attrs.maxFileSize) {
          return true;
        } else {
          return false;
        }
      };

      isTypeValid = function(type) {
        if ((validMimeTypes === (void 0) || validMimeTypes === '') || validMimeTypes.indexOf(type) > -1) {
          return true;
        } else {
          alert("Invalid file type.  File must be one of following types " + validMimeTypes);
          return false;
        }
      };

      element.bind('dragover', processDragOverOrEnter);
      element.bind('dragenter', processDragOverOrEnter);

      return element.bind('drop', function(event) {
        var file, name, reader, size, type;
        if (event != null) {
          event.preventDefault();
        }
        reader = new FileReader();
        reader.onload = function(evt) {
          if (checkSize(size) && isTypeValid(type)) {
            return scope.$apply(function() {
              scope.file = evt.target.result;
              if (angular.isString(scope.fileName)) {
                return scope.fileName = name;
              }
            });
          }
        };
        file = event.dataTransfer.files[0];
        name = file.name;
        type = file.type;
        size = file.size;
        reader.readAsDataURL(file);
        return false;
      });
    }
  }
}
