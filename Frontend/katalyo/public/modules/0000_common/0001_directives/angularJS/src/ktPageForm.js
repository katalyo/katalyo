'use strict';

export function ktPageForm () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktPageForm.html',
             controller: function($scope,MessagingService,GridsterService,WidgetRegisterService,ProcessingService,
              ResourcesDataService,KatalyoStateManager,WidgetDataExchangeService,dragulaService,$stateParams,$rootScope,$timeout){
                 
                $scope.resourceObject = $stateParams.resource;
                $scope.CTRL='ktPageForm'
                $scope.formLoaded=false;
                $scope.resourceId=$stateParams.id;
                $scope.gsrvc = GridsterService;
                $scope.showFieldProperties = false;
                $scope.resourceDefinition.ResourceDefId=$scope.resourceId;
                $scope.resourceDefinition.form_type=$scope.formType;
                $scope.resourceDefinition.form_definition=true;

                $scope.gridsterType="page";
                $scope.refreshForm=false;
                $scope.dashboard = {};
                $scope.reloadTaskToolbox = false;
                if ($scope.localVars==undefined) $scope.localVars={};
                
                 WidgetDataExchangeService.deRegisterAllWidgets();

                 
              
                 $scope.dashboardLocal ={widgets:{layout: GridsterService.getEmptyTaskForm(),data:{}}};
                 let lang=$rootScope.globals.currentUser.lang;
                
                $scope.ksm = KatalyoStateManager;

                var form_saved = function(data)
                    {
                        formDefinitionState.subject.setStateChanges = data;
                        formDefinitionState.subject.notifyObservers("formSaved");
                    };
                
               
                var formDefinitionState,formDefinitionStateName;   
                $scope.setupKatalyoStateManager = function()
                {
                    formDefinitionState= $scope.ksm.addSubject('formDefinitionState'+$scope.resourceId+$scope.resourceDefinition.form_type,null);
                    formDefinitionStateName = formDefinitionState.subject.name;
                    
                    let action_value_changed = $scope.ksm.addAction(formDefinitionStateName,"formSaved",form_saved);
                    
                }
                 $scope.setupKatalyoStateManager();
                 
                $scope.ShowFormView = function(viewId,locked)
                {
                   //$timeout(function(){
                    if ($scope.resourceDefinition.formParams===undefined) 
                    {
                       $scope.resourceDefinition.formParams={} 
                    }
                       
                        $scope.resourceDefinition.formParams.formViewType=viewId;
                        $scope.gsrvc.setFormViewType($scope.resourceDefinition.formParams.formViewType);

                  //  })
                  
                         
                };
            
                      
                $scope.showHideProperties = function(value)
                {
                        $scope.gsrvc.setFieldPropertiesOpened(value);
                        $scope.showFieldProperties=$scope.gsrvc.getFieldPropertiesOpened();
             
                };
            
            $scope.$watch('gsrvc.getFieldPropertiesOpened()',function(newValue,oldValue){
                    
                    if (newValue!=oldValue)
                    {
                     $scope.selectedField=$scope.gsrvc.getFieldProperties();
                     $scope.showFieldProperties = $scope.gsrvc.getFieldPropertiesOpened();
                    }
            });
            
            
             $scope.$watch('gsrvc.getFieldProperties()',function(newValue,oldValue){
                    
                    if (newValue!=oldValue)
                    {
                     $scope.selectedField=$scope.gsrvc.getFieldProperties();
                     $scope.showFieldProperties = $scope.gsrvc.getFieldPropertiesOpened();
                    }
            });
   
   
     
            $scope.formFromServerLoaded = {};
            
            $scope.localVars.getResourceForm = function () {
                  
                    ResourcesDataService.getResourceForm($scope.resourceId,'i',lang).then(function(data) {
                                      
                            var resourceFormData = data.data
                            $scope.resourceDefinition = resourceFormData.resourceDefinition
                            $scope.resourceDefinition.form_definition = true                    
                                
                            if (resourceFormData.widgets.length>0) {
                                $scope.dashboard={widgets:{layout:resourceFormData.widgets}}
                             }else {
                              $scope.dashboard = {widgets:{layout:$scope.resourceDefinition.ResourceExtended.DefaultForm}}
                             }
                            
                             $scope.formVersion = $scope.resourceDefinition.FormVersion;
                            
                             $scope.gsrvc.setTaskFormVersion($scope.resourceDefinition.id,$scope.formVersion); 

                             $scope.resourceDefinition.formParams={formViewType:1,showGridNavigation:false};
                             
                           
                             $scope.formLoaded=true;
                             $scope.formFromServerLoaded = true;
                             $scope.ShowFormView($scope.resourceDefinition.formParams.formViewType,false);
                             
                           
                  },function(error){
               
                    
                   MessagingService.addMessage(error.msg,'error');
                  
                  
               });
                  
            };
            
            $scope.localVars.getResourceForm()

            /*
            $scope.localVars.getResourceForm = function (afterSave,afterSaveMsg) {
                  if (!$scope.formFromServerLoaded)
                  {
                    ResourcesDataService.getResourceForm($scope.resourceId,'i',lang).then(function(data) {
                                      
                                         var resourceFormData = data.data
                                         $scope.resourceDefinition = resourceFormData.resourceDefinition
                                          ResourcesDataService.GetWidgetToolbox($scope.resourceDefinition.ResourceCode).then(
                                          
                                          function (widgetData) {
                                            
                                             $scope.gsrvc.setTaskToolboxItems(widgetData.data);
                                                
                                                
                                              if (resourceFormData.widgets.length>0) {
                                                $scope.dashboard={widgets:{layout:resourceFormData.widgets}}
                                             }else {
                                              $scope.dashboard = {widgets:{layout:$scope.resourceDefinition.ResourceExtended.DefaultForm}}
                                             }
                                            
                                             $scope.formVersion = $scope.resourceDefinition.FormVersion;
                                            
                                             $scope.gsrvc.setTaskFormVersion($scope.resourceDefinition.id+$scope.formType,$scope.formVersion); 

                                             $scope.resourceDefinition.formParams={formViewType:1,showGridNavigation:false};
                                             
                                           
                                             $scope.formLoaded=true;
                                             $scope.formFromServerLoaded = true;
                                             $scope.ShowFormView($scope.resourceDefinition.formParams.formViewType,false);
                                             //debugger; //getResourceForm migor
                                        
                                        
                                            
                                         },function(error){
                                             MessagingService.addMessage(error.msg,'error');  
                                         }); 
                                         
                                         
                        

                                         
                                         
                                         if (afterSave)
                                         {
                                           MessagingService.addMessage(afterSaveMsg,'success');
                                          
                                         }
                                      },function(error){
                                   
                                        
                                       MessagingService.addMessage(error.msg,'error');
                                      
                                      
                                   });
                  }
            };
            */ 
        //**********************************************************
          //           SAVE TASK FORM DEFINITION
          //**********************************************************
      
        $scope.saveForm = function () {
 
               $scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving page form for '+$scope.resourceId);
               $scope.savingTaskForm = true;
               ResourcesDataService.saveResourceForm($scope.dashboard.widgets.layout,$scope.resourceId,$scope.formType).then(function(data) {
                                
                                        MessagingService.addMessage(data.msg,data.msgType);
                                        $scope.formVersion= data.version;
                                        $scope.resourceDefinition.FormVersion=$scope.formVersion
                                        $scope.gsrvc.setTaskFormVersion($scope.resourceDefinition.id+$scope.formType,$scope.formVersion); 
                                        $scope.ksm.executeAction(formDefinitionStateName,"formSaved",data.new_items);
                                        $scope.savingPageForm = false;
                                         
                          
                                        if (data.data.formData.length>0) {
                                            $scope.dashboard={widgets:{layout:data.data.formData}}
                                           
                                            
                                        }   
  
                                        $scope.formLoaded=true;
                                        $scope.formFromServerLoaded = true;
                                        // $scope.ShowFormView($scope.resourceDefinition.formParams.formViewType,false);
                                         
        
                                   },function(error){
                                     
                                       $scope.savingPageForm = false;
                                       MessagingService.addMessage(error.msg,'error');
                                      
                                    });
                                      
                                  
              };
              
         //**********************************************************
          //           SAVE TASK BUTTONS
          //**********************************************************      
        $scope.saveFormBtns = function () {
       
          
               //$scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving task btns for '+$scope.resourceId);
               
               TaskDataService.SaveTaskBtns($scope.resourceDefinition.initTaskBtnDef,$scope.resourceId,lang).then(function(data) {
                                       
                                        
                                       MessagingService.addMessage(data.msg,'success');
                                  
                                   },function(error){
                                        
                                       MessagingService.addMessage(error.msg,'error');
                                      
                                    });
                                      
                                  
       
              };
              

             
                             
            $scope.$on('CtrlPBPressed',function(event,data){
                  
                var key=data;
                if (key=='b' || key=='B')
                {
                    if ($scope.resourceDefinition.formParams.formViewType!=1) $scope.ShowFormView(1);
            
                }
                if (key=='p' || key=='P')
                {
                    if ($scope.resourceDefinition.formParams.formViewType!=2) $scope.ShowFormView(2);
                }
                  
            });  
          
         
           
            
                
                },
             link: function (scope, elem, attrs) {
              
            
            
             
                  
                  
            }
        }
};