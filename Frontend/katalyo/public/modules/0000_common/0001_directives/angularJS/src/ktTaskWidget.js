'use strict';

export function ktTaskWidget ($timeout) {
             var controller = ['$scope', '$stateParams','$state','$rootScope','GridsterService','$templateCache','$compile','$element',
                               function ($scope,$stateParams,$state,$rootScope,GridsterService,$templateCache,$compile,$element) {

             
             if ($scope.localVars==undefined) $scope.localVars={};
             if ($scope.fd==undefined) $scope.fd={};
             if ($scope.taskDefinition==undefined) $scope.taskDefinition={};
                   
             $scope.localVars.taskId = $scope.taskId;
             if ($scope.localVars.previousTask==undefined) $scope.localVars.previousTask = 0;
             if ($scope.localVars.outcomeId==undefined) $scope.localVars.outcomeId = 0;
             $scope.localVars.loadTaskDirect = true;
             
            $scope.localVars.ReloadTask=false;
            
             $scope.gsrvc=GridsterService;
             $scope.openedTaskInNewPage={localVars:{page_state_id:$scope.localVars.page_state_id},taskDefinition:null};

             $scope.gsrvc.setOpenTaskType("current");
   
             $scope.openTaskInNewPage=function(){
        
                          $scope.openedTaskInNewPage = $scope.gsrvc.getTaskDataInNewPage();
           
        
             };
                   
             let template;
             template = $templateCache.get('ktdirectivetemplates/ktTaskWidget.html');
             
          
             $element.append($compile(template)($scope));

          }];
          
          
        return {
            restrict: 'A',
            controller:controller,
            scope:{
             taskId: '=',
             fd: '=',
             localVars: '=?',
            },
             link: function (scope, elem, attrs) {
                 
                scope.$on('RefreshTask'+scope.localVars.taskId,function(event,data){

                      scope.localVars.ReloadTask=true;
                      $timeout(function(){ scope.localVars.ReloadTask=false;})
                });

              }
        }             
};