'use strict';

export function ktNotificationList () {
   
    var controller = ['$scope', '$uibModal','$stateParams','$timeout','CodesService','ResourcesDataService','$rootScope','ProcessingService','WidgetDataExchangeService','GridsterService','MessagingService','CODES_CONST','TaskDataService',
                      function ($scope,$uibModal,$stateParams,$timeout,CodesService,ResourcesDataService,$rootScope,ProcessingService,WidgetDataExchangeService,GridsterService,MessagingService,CODES_CONST,TaskDataService) {
      
      
    
    $scope.localVars={};
    $scope.localVars.datasetMapperType=0;
    $scope.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_; 
    $scope.localVars.searchFormDownloaded = false;
    $scope.localVars.showSearchFilters=true;
    $scope.localVars.resourceDataDownloaded = true;
    $scope.taskId=$scope.resourceDefinition.ResourceDefId;
    if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
    if ($scope.taskId==undefined) $scope.taskId=0
    $scope.localVars.taskId=$scope.taskId;
    $scope.localVars.widgetType = 4;
    
    $scope.localVars.parentDatasetDefId=$scope.fd.related;
    $scope.localVars.animationsEnabled = true;
    $scope.localVars.gridsterType="resource";
          

     $scope.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: GridsterService.getEmptyDatasetForm()
				} ;
        
        
       $scope.localVars.gridOptionsResourceData = {
           //  columnDefs: colDefs,
        enableGridMenu: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: $scope.fd.multiline,
      //  enableHorizontalScrollbar : 1,
       // enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: true,
        enableColumnResizing: true,
        enableColumnReordering: true,
        useExternalPagination: true,
        paginationPageSizes: [10, 25, 50,100,200,500,1000],
        enablePaginationControls: false,
      };

  var paginationOptions = {
    pageNumber: 1,
    pageSize: 10,
    sort: null
    };
    
    
       $scope.localVars.gridOptionsRelated = {
      //  columnDefs: colDefs,
       enableGridMenu: true,
       showGridFooter: true,
       showColumnFooter: true,
       enableRowSelection: false,
       multiSelect: false,
       enableHorizontalScrollbar : 2, 
       nableVerticalScrollbar : 2,
       enableFiltering: true,
       enableRowHeaderSelection: true,
       enableColumnResizing: true,
       minRowsToShow: 2,
      
   }
    
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        
        //$scope.fd.populateType=1;
        $scope.fd.parentDatasetDefId=$scope.fd.related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.localVars.formType=$scope.resourceDefinition.form_type;
        $scope.localVars.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        $scope.formDefinition = $scope.resourceDefinition.form_definition;
        if  ($scope.formDefinition==undefined)
        {
          
          $scope.formDefinition = false;
        }
        
        
        
        
          if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        
        $scope.localVars.resourceRecordId = 0;
        $scope.fd.parentElementId = 0;
        
        
        
      var out_fields = ['resourceRecordId']
      //resourceLink
      var widgetDefinition = {DEI: [{'name':'resourceLinkDatasetList','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'datasetListWidget',widgetId: $scope.fd.presentationId,taskId:  $scope.lTaskId,
      resourceDefId : $scope.fd.related, //resourceDefId : $scope.selectedResource
      resourceRecordId:$scope.localVars.resourceRecordId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId, output:out_fields };
      $scope.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
      $scope.localVars.localId=$scope.localWidgetId;
      
      
      $scope.localVars.gridOptionsResourceData.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.localVars.gridApi = gridApi;
           
          gridApi.selection.on.rowSelectionChanged($scope,function(row){

            $scope.localVars.datasetRecordSelected=row.isSelected;
            if ($scope.localVars.datasetRecordSelected)
              {
                if (row.entity.id!=$scope.localVars.selectedDatasetRecordId)
                {
                  $scope.localVars.selectedDatasetRecordId = row.entity.id;
                }
              }else
              {
                $scope.localVars.selectedDatasetRecordId = 0;
                
              }
              $scope.localVars.gridOptionsRelated.columnDefs=[];
              $scope.localVars.gridOptionsRelated.data=[];
              $scope.localVars.showGrid=false;
              $scope.localVars.prevTaskPackage ={resourceDefId:$scope.fd.related,records:gridApi.selection.getSelectedRows()}
              
               if (gridApi.selection.getSelectedRows().length>0)
              {
                //$rootScope.$broadcast('EventresourceLink'+$scope.fd.presentationId,$scope.selectedDatasetRecordId);
                WidgetDataExchangeService.setData({
                  DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                        ,{name:'resourceRecordIds',value:gridApi.selection.getSelectedRows()},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.presentationId,task_execute_id:$scope.taskExecuteId});
            
              }
              
          });
          
            //external pagination
          gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,newPage,pageSize);
          });
      
           gridApi.selection.on.rowSelectionChangedBatch($scope,function(row){

              $scope.localVars.prevTaskPackage ={resourceDefId:$scope.fd.related,records:gridApi.selection.getSelectedRows()}
              
          });
     }
     
     
     $scope.localVars.ProcessSearchForm=function (form){
            
            if (form!=undefined){
     					for(var i=0;i<form.length;i++) {
                
                    if ('type' in form[i])
                    {
                      if (form[i].type == 'resource' && form[i].autoSearch)
                      {
                        var pos = $scope.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(form[i].presentationId);
                        if (pos<0) $scope.localVars.childWidgets.push({widgetId:form[i].presentationId,processed:false})
                        
                      }
                     }
                      if ('layout' in form[i])
                      {
                           
                       $scope.localVars.ProcessSearchForm(form[i].layout);
                      }
              }
          } 
           
      };
      
      $scope.localVars.SearchFormProcessed=false;

      /*
      $scope.localVars.setDatasetMapperType= function(type){
            $scope.localVars.datasetMapperType = type;
          };      
      */
       $scope.localVars.setSelectedDataset = function(id)  {
              
              if ($scope.localVars.selectedResourceItem==undefined) $scope.localVars.selectedResourceItem={};
              var pos = $scope.localVars.datasetList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.localVars.selectedResourceItem.id = $scope.localVars.datasetList[pos].id;
                $scope.localVars.selectedResourceItem.name = $scope.localVars.datasetList[pos].name;
                $scope.localVars.selectedDataset = $scope.localVars.datasetList[pos];
                $scope.localVars.selectedResourceItem.name_confirmed = $scope.localVars.selectedResourceItem.name +' ('+$scope.localVars.selectedResourceItem.id +')'
                $scope.localVars.selectedResourceItem_c=$scope.localVars.selectedResourceItem;
                }
					};
          
       // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
        $scope.localVars.getDatasets = function()  {
						
            if ($scope.localVars.datasetList==undefined) $scope.localVars.datasetList=[];
          
            var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'notification').then(
						function(data) {
                      $scope.localVars.datasetList = data.data;
                       if ($scope.fd.related!=null)
                       {
                        $scope.localVars.setSelectedDataset($scope.fd.related);
                
                        }     
											 
		 },function(error){    
                                MessagingService.addMessage(error.msg,'error');
	 });
	}
            
      $scope.localVars.GetDatasetData=function(data)

      {

            fromWidget = data.widgetId;
            var allProcessed=true;
            var pos = $scope.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(fromWidget);
            //if (pos<0) $scope.fd.childWidgets.push({widgetId:fromWidget,processed:true});
            for (var i=0;i<$scope.localVars.childWidgets.length;i++)
              {
                
                if ($scope.localVars.childWidgets[i].widgetId == fromWidget) $scope.localVars.childWidgets[i].processed = true;
                if (!$scope.localVars.childWidgets[i].processed) allProcessed = $scope.localVars.childWidgets[i].processed;
              }
             // if (!$scope.SearchFormProcessed) $timeout(function() {$scope.GetDatasetData(fromWidget);},1000);
              if($scope.fd.autoSearch && allProcessed && $scope.localVars.SearchFormProcessed) {
                  $scope.localVars.selectedResource=$scope.fd.related;
                  $scope.localVars.getResourceColumns(true);
                                      
                }
      };
       
      if ($scope.localVars!=undefined) $scope.localVars.childWidgets = [];
      else $scope.localVars = {childWidgets : []};
     // $scope.uncaughtWidgets = [];
      var fromWidget=0;
   
 
      $scope.$on('SearchFormReady'+$scope.taskExecuteId,function(event, data){
      
           $scope.localVars.GetDatasetData(data); 
            
      });     
     
   
   //**********************************************************
	//           GET DATASET RELATED RECORDS
	//**********************************************************      
        $scope.localVars.getDatasetRelatedRecords = function (relatedDatasetId,recordId,modelId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataRelated($scope.fd.related,recordId,relatedDatasetId,0,modelId,lang).then(function(data) {
                                
                                     $scope.localVars.gridOptionsRelated.data = data.data;
                                     $scope.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                                     $scope.localVars.showGrid=true;
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
        
        
 //**********************************************************
	//           GET DATASET SINGLE RECORD
	//**********************************************************      
      $scope.localVars.getDatasetSingleRecord = function (relatedDatasetId,recordId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataDirectFilter(relatedDatasetId,0,lang,{id: recordId}).then(function(data) {
                                
                                     $scope.localVars.gridOptionsRelated.data = data.data;
                                     $scope.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                               
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
    
        $scope.$on('SearchFormReady'+$scope.taskExecuteId+$scope.fd.presentationId,function(event, data1){
      
                                        $scope.localVars.GetDatasetData(data1); 
            
        });
        
    $scope.localVars.getFormResource = function(){   
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.presentationId==null) {
                $scope.fd.presentationId = 0;
              }
              
               
              ResourcesDataService.getResourceFormExtendedWithData($scope.fd.related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.populateType,$scope.fd.parentElementId,$scope.fd.presentationId,$scope.prevTaskExeId,$scope.localVars.formType,$scope.localVars.widgetType,$scope.localVars.resourceRecordId,"f",lang).then(function(data) {
                 
                                    $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                
                             
                                    $scope.localVars.selectedResourceItem_c = {'name':$scope.localVars.resourceWidgetDefinition.name,'id':$scope.fd.related,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                              
                               
                                    $scope.fd.fItemValue=data.data.widgets;

                                    $scope.localVars.searchFormDownloaded = true;
                                    $scope.localVars.setField($scope.fd.fItemValue,'required',false);
                                    $scope.localVars.formLoaded=true;
                                   //prepare search form
                                    $scope.localVars.ProcessSearchForm($scope.fd.fItemValue);
                                    $scope.localVars.SearchFormProcessed = true;
                                    if ($scope.localVars.childWidgets.length>0)
                                    {
                                      WidgetDataExchangeService.registerSearch({widgetId:$scope.fd.presentationId,taskExecuteId:$scope.taskExecuteId,prevTaskExeId:$scope.prevTaskExeId,childWidgets:$scope.localVars.childWidgets});
                                    }
                                    else
                                    {
                                       $rootScope.$broadcast('SearchFormReady'+$scope.taskExecuteId,{widgetId:$scope.fd.presentationId});
                                    }
                                    $scope.localVars.dashboard.widgets.layout =  data.data.widgets;    
                                    $scope.formData[$scope.fd.presentationId] =  data.data.form_data;
                                    $scope.fd.search = $scope.formData[$scope.fd.presentationId];
               
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                             });
                              
      };//getFormResource_END
    
  $scope.localVars.getFormResourceOriginal = function(){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.presentationId==null)
              {
                $scope.fd.presentationId = 0;
                
              }
              ResourcesDataService.getResourceForm($scope.fd.related,"i",lang).then(function(data) {
                            
                                $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                $scope.localVars.dashboard.widgets.layout=data.data.widgets;
                                 $scope.localVars.selectedResourceItem_c = {'name':$scope.localVars.resourceWidgetDefinition.name,'id':$scope.fd.related,'relatedId':$scope.fd.related, 'presentationId':$scope.fd.presentationId  ,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                            
                             
                                $scope.fd.fItemValue=data.data.widgets;
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.presentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.localVars.resourceRecordId}]}]});
						
                                  $scope.localVars.searchFormDownloaded = true;
                                   $timeout(function() {
                                    $scope.localVars.formLoaded=true;
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
   
   $scope.localVars.datasetRecordSelected = false;
   $scope.localVars.selectedDatasetRecordId = 0;
 
     
      $scope.localVars.getResourceData = function () {

        // scope.activity.activityMessage = ProcessingService.setActivityMsg('Downloading resource data for '+scope.selectedResource);
        var lang = $rootScope.globals.currentUser.lang;
        $scope.localVars.resourceDataDownloading = true;
        if ($scope.localVars.pageSize==undefined) $scope.localVars.pageSize = paginationOptions.pageSize;
        ResourcesDataService.getResourceData($scope.localVars.selectedResource,$scope.taskId,lang).then(function(data) {
                               
                                     $scope.localVars.gridOptionsResourceData.data = data.data;
                                     $scope.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                                     $scope.localVars.restoreGridState();
                                     $scope.localVars.resourceDataDownloaded = true;
                                    
                                 },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                   
                             });
        };

		 
      //**********************************************************
      //           GET RESOURCE DATA PAGINATED
      //**********************************************************      
    $scope.localVars.getResourceDataPaginated = function (urlNext,urlPrev,newPage,pageSize) {
        
        
        $scope.localVars.resourceDataDownloaded = false;        
        if (Math.ceil($scope.localVars.totalCount/pageSize)<newPage)
        {
          newPage = Math.ceil($scope.localVars.totalCount/pageSize);
        }
         
         var lang = $rootScope.globals.currentUser.lang;
          
         if (pageSize==undefined)
         
         {
          $scope.localVars.pageSize = paginationOptions.pageSize;
          pageSize= $scope.localVars.pageSize;
         }
         
         if ($scope.fd.populateMethod=='M')
         {
          ResourcesDataService.getResourceDataPaginatedFilter($scope.localVars.selectedResource,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.search, $scope.localVars.dashboard.widgets.layout).then(function(data) {
                                    
                                    
                                      
                                    $scope.localVars.gridOptionsResourceData.data = data.data.results;
                                    $scope.localVars.restoreGridState();
                                    $scope.localVars.resourceDataDownloaded = true;
                                    
                                    $scope.localVars.nextPage = data.data.next;
                                    $scope.localVars.previousPage = data.data.previous;
                                    $scope.localVars.totalCount=data.data.count;
                                    $scope.localVars.currentPage=newPage;
                                    $scope.localVars.numPages = Math.ceil($scope.localVars.totalCount/pageSize);
                                    $scope.localVars.pageSize = pageSize;
                                    $scope.localVars.gridOptionsResourceData.totalItems=$scope.localVars.totalCount;
                                    
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.presentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.presentationId+"Completed",$scope.localVars.gridOptionsResourceData.data);
                                   
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
         }
         else if ($scope.fd.populateMethod=='A')
         {
          var task_instance=0;  
          if ($scope.localVars.formType=='i') task_instance = $scope.taskInitiateId;
          else task_instance = $scope.taskExecuteId;
    
          ResourcesDataService.getResourceDataPaginatedAuto($scope.localVars.selectedResource,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.presentationId,task_instance,$scope.localVars.formType).then(function(data) {
                                    
                                    
                                      
                                    $scope.localVars.gridOptionsResourceData.data = data.data.results;
                                    $scope.localVars.restoreGridState();
                                    $scope.localVars.resourceDataDownloaded = true;
                                    
                                    $scope.localVars.nextPage = data.data.next;
                                    $scope.localVars.previousPage = data.data.previous;
                                    $scope.localVars.totalCount=data.data.count;
                                    $scope.localVars.currentPage=newPage;
                                    $scope.localVars.numPages = Math.ceil($scope.localVars.totalCount/pageSize);
                                    $scope.localVars.pageSize = pageSize;
                                    $scope.localVars.gridOptionsResourceData.totalItems=$scope.localVars.totalCount;
                                    
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLink',value:[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.presentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.presentationId+"Completed",$scope.localVars.gridOptionsResourceData.data);
                                   
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
          
         }
        };


 $scope.localVars.pageChanged = function(){
        $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,$scope.localVars.currentPage,$scope.localVars.pageSize);
         };
   //**********************************************************
	//           GET RESOURCE COLUMNS
	//**********************************************************      
  $scope.localVars.getResourceColumns = function (getData) {
     var lang = $rootScope.globals.currentUser.lang;
     if ($scope.localVars.selectedResource==undefined) $scope.localVars.selectedResource = $scope.fd.related;
     
     if ($scope.localVars.selectedResource!=undefined && $scope.localVars.selectedResource!=null)
     {
     ResourcesDataService.getResourceColumns($scope.localVars.selectedResource,$scope.taskId,lang).then(function(data) {
                                
                $scope.localVars.gridOptionsResourceData.columnDefs = data.data;
                
                if (getData) $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,1,$scope.localVars.pageSize);               
                
         //                            MessagingService.addMessage(data.msg,'success');
         
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
               
     }
  };
     
     $scope.localVars.getResourceDataFiltered = function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

           //scope.activity.activityMessage = ProcessingService.setActivityMsg('Retriving data '+scope.selectedResource);
          //$scope.filter = ResourcesDataService.prepareResourceWidgetData($scope.dashboard.widgets,-1);
         
          if ($scope.localVars.selectedResource==null) {
            $scope.localVars.selectedResource = $scope.fd.related;
          }
	//		ResourcesDataService.getResourceDataFiltered($scope.fd.related,lang,$scope.filter).then(function(data) {//MIGOR
         ResourcesDataService.getResourceDataFiltered($scope.localVars.selectedResource,$scope.taskId,lang,$scope.localVars.dashboard.data).then(function(data) {//MIGOR
                            
                                  $scope.localVars.gridOptionsResourceData.data = data.data;
                                  $scope.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                                  
                                    $timeout(function(){
                                      $scope.localVars.resourceDataDownloaded = true;
                                      $scope.localVars.restoreGridState();
                                    });
                                 
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });
        };
		
       $scope.localVars.setField = function(form,name,value,msg)
        {
     				TaskDataService.setField(form,name,value);
            if (msg) MessagingService.addMessage(msg,'info');
        };
        
          $scope.localVars.showHideSearchForm = function()
        {
     				if ($scope.fd.showSearchForm) $scope.localVars.getFormResource();
        };
       
        
    
  if ($scope.fd.related!=null &&  !$scope.formDefinition && !$scope.localVars.searchFormDownloaded)
  {
    $scope.localVars.getFormResource();
    
  }else if ($scope.fd.related!=null && ($scope.localVars.showForm || $scope.fd.autoSearch))
  {
     $scope.localVars.getFormResource();
     $scope.localVars.getDatasets();
      $scope.localVars.getResourceColumns(false);
    
  }else if(!$scope.localVars.showForm)
  {
     $scope.localVars.showHideSearchForm();
     $scope.localVars.getResourceColumns(false);
     $scope.localVars.getDatasets();
    
  }
  
    
    
		 $scope.localVars.saveGridState= function() {
       var state = $scope.localVars.gridApi.saveState.save();
       $scope.fd.gridState = state;
       MessagingService.addMessage("To make changes stick please save form",'info');
             
    };
  
   $scope.localVars.restoreGridState= function() {
    if ($scope.fd.gridState!=null)
    {
       if ($scope.localVars.gridApi!=undefined)
       {
      
        $scope.localVars.gridApi.saveState.restore($scope,$scope.fd.gridState);
        
       }else
       {
        $timeout(function(){
            $scope.localVars.restoreGridState();
        });
       }
    }
  };
        
}]; //controler_END

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktNotificationList.html',
            controller: controller,
            link: function (scope, elem, attrs) {

            
            
               $rootScope.$broadcast("Widget"+scope.fd.PresentationId+"Ready",null);
                          
            
             }
             
             
  } 
        
};