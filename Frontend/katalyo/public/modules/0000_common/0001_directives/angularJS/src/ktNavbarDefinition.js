'use strict';

export function ktNavbarDefinition() {
  
  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktNavbarDefinition.html',
            scope:
            {
                navbarItem:'=',
                localVars:'='
            },
            controller: function($scope,$rootScope,$timeout,MenuService,CodesService,MessagingService,ResourcesDataService){
            
              $scope.CTRL="ktNavbarDefinition"
              //$scope.selectedUsers = [] //needed for selectUsers widget
             // $scope.selectedGroups =[] //needed for selectUsers widget
               
              let lang = $rootScope.globals.currentUser.lang;
              var curUser=$rootScope.globals.currentUser
              //debugger;
              if ($scope.localVars==undefined) $scope.localVars={};
              $scope.menu_service=MenuService;
               
              if ($scope.navbarItem==undefined) {      
                    $scope.navbarItem=$scope.menu_service.getSelectedNavbar();  
              }
              
              if ($scope.navbarItem.Users == undefined) $scope.navbarItem.Users=[]
              if ($scope.navbarItem.Groups == undefined) $scope.navbarItem.Groups=[]
              
              
              $scope.selectedUsers = $scope.navbarItem.Users
              $scope.selectedGroups = $scope.navbarItem.Groups
                    
                    
              $scope.headerLabel="Grant access to navigation";
              $scope.usersLabel="Users";
              $scope.groupsLabel="Groups";
              
              $scope.getUsersGroups= function(){
                  debugger
                var menuId = $scope.navbarItem.katalyonode_menus_id;
                if (menuId!=null)
                {
                  MenuService.getUsersGroupsForMenu(menuId,lang).then(
                        function(data) {
                          $scope.navbarItem.Users = data.Users;
                          $scope.navbarItem.Groups = data.Groups;
                          
                           $scope.selectedUsers = $scope.navbarItem.Users  //needed for selectUsers widget
                           $scope.selectedGroups = $scope.navbarItem.Groups  //needed for selectGroups widget
                          
                          $scope.navbarItem.UsersGroupsDownloaded = true;
                         
                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                  });
                }else $scope.navbarItem.UsersGroupsDownloaded = true;
              }
              
              
              $scope.localVars.setPage = function() {
                 var pos = $scope.localVars.pageList.map(function(e) { return e.id; }).indexOf($scope.navbarItem.katalyonode_menus_Target_id);
              
                if (pos>=0){
                  $scope.navbarItem.Target = $scope.localVars.pageList[pos];
                }
              }
              $scope.localVars.getPages = function() {
                  ResourcesDataService.getResourceDefListByType(lang,'page').then(
                        function(data) {
                          $scope.localVars.pageList = data.data;
                          $scope.localVars.pagesLoaded=true;
                          $scope.localVars.setPage();
                         
                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                        });
              }
              
            //TODO - vidjeti jel ovo dole treba
             if ($scope.selectedUsers==undefined || $scope.selectedGroups==undefined) 
                        $scope.getUsersGroups(); 
             
               
              $scope.$watch('menu_service.selectedNavbar',function(newValue,oldValue){
                  if (newValue!=oldValue) {
                        $scope.navbarItem=$scope.menu_service.getSelectedNavbar();
                  }
               });
               
               $scope.$watch('selectedUsers',function(newValue,oldValue){
                  let a= newValue
                 // debugger
               });
               
                $scope.$watch('navbarItem',function(newValue,oldValue){
                  let a= newValue
                  debugger
               });
            
            $scope.Cancel = function() {
               $scope.menu_service.navbarSelected = false;   
            };              
                                    
            $scope.saveNavbar = function(form) {
                //debugger; //saveNavbar
                if (form.$valid) $scope.menu_service.saveMenu($scope.menu_service.selectedNavbar,lang).then(function(data) {
                                 
                         if ($scope.menu_service.selectedNavbar.katalyonode_menus_id == 0 || $scope.menu_service.selectedNavbar.katalyonode_menus_id == null || $scope.menu_service.selectedNavbar.katalyonode_menus_id==undefined) 
                         {
                                $scope.menu_service.selectedNavbar = data.data;
                                $scope.currentNavbarId = $scope.menu_service.selectedNavbar.katalyonode_menus_id;
                                $scope.menu_service.selectedNavbar.SideMenus = [];
                                $scope.menu_service.navbarItems.push(angular.copy($scope.menu_service.selectedNavbar));
                                $scope.menu_service.setCurrentNavbarId($scope.currentNavbarId,lang);  
                        }
                        else
                           {
                             $scope.menu_service.selectedNavbar = data.data;
                             $scope.currentNavbarId = $scope.menu_service.selectedNavbar.katalyonode_menus_id; 
                           }
                             MessagingService.addMessage(data.msg,'success');
                         },function(error){    
                         MessagingService.addMessage(error.msg,'error');
                     
                      });
            };
            
              $scope.localVars.navbarLoaded = true;
            },//controller
            
            link: function (scope, elem, attrs) {
              

           
      }
    }
  
};