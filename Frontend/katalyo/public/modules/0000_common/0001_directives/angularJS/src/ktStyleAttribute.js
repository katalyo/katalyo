'use strict';

export function ktStyleAttribute () {

        return {
            restrict: 'A',
            
            scope:{
              fd: '=',
              styleType:'=',
            },
         
            link: function (scope, elem, attrs) {
                
                if (scope.fd.Parameters==undefined) scope.fd.Parameters={};
                
                scope.$watch(function () { return elem[0].style.width},  widthChangedCallBack, true);
                scope.$watch(function () { return elem[0].style.height},  heightChangedCallBack, true);

                function widthChangedCallBack(newValue, oldValue) {
                    if (newValue !== oldValue && scope.fd.Parameters!=undefined && scope.fd.Parameters?.CustomSize) {
                    // do something interesting here
                    if (scope.styleType=="label")
                    {
                        if (scope.fd.Parameters.LabelStyle==undefined) scope.fd.Parameters.LabelStyle = {};
                        scope.fd.Parameters.LabelStyle.Width = newValue;
                    }
                    if (scope.styleType=="element")
                    {
                        if (scope.fd.Parameters.ElementStyle==undefined) scope.fd.Parameters.ElementStyle = {};
                        scope.fd.Parameters.ElementStyle.Width = newValue;
                    }
                  }
                }
              

                 function heightChangedCallBack(newValue, oldValue) {
                    if (newValue !== oldValue && scope.fd.Parameters!=undefined && scope.fd.Parameters?.CustomSize) {
                    // do something interesting here
                    if (scope.styleType=="label")
                    {
                        if (scope.fd.Parameters.LabelStyle==undefined) scope.fd.Parameters.LabelStyle = {};
                        scope.fd.Parameters.LabelStyle.Height = newValue;
                    }
                    if (scope.styleType=="element")
                    {
                        if (scope.fd.Parameters.ElementStyle==undefined) scope.fd.Parameters.ElementStyle = {};
                        scope.fd.Parameters.ElementStyle.Height = newValue;
                    }
                  }
                }

                if (scope.fd.colLength!==undefined)
                {
                    scope.$watch('fd.LabelLength',function(newValue,oldValue){
                    if (newValue!=oldValue)
                    {
                        scope.fd.gridClassLabel=scope.fd.colLength[scope.fd.LabelLength-1];
                    }
                    });


                    scope.$watch('fd.ValueLength',function(newValue,oldValue){
                    if (newValue!=oldValue)
                    {
                        scope.fd.gridClassElement=scope.fd.colLength[scope.fd.ValueLength-1];
                    }
                    });
                }
               
             
                scope.fd.process_size_style = function(newValue)
                {

                            let customStyleLabel = scope.fd.Parameters.customCssLabel+";text-align:"+scope.fd.Parameters.LabelAlign+" !important;font-size:"+scope.fd.Parameters.FontSize+scope.fd.Parameters.FontUnits+";color:"+scope.fd.Parameters.FontColor+";";

                            let customStyleElement = scope.fd.Parameters.customCssElement+";font-size:"+scope.fd.Parameters.ValueFontSize+scope.fd.Parameters.ValueFontUnits+";color:"+scope.fd.Parameters.ValueFontColor+";";
                            
                            scope.fd.labelAlign = "text-align:"+scope.fd.Parameters.LabelAlign+" !important";
                            if  (scope.fd.Parameters.FontBold) customStyleLabel = customStyleLabel+ "font-weight: bold;"

                            if  (scope.fd.Parameters.ValueFontBold) customStyleElement = customStyleElement+ "font-weight: bold;"

                            if (newValue?.CustomSize)
                            {
                                scope.fd.customStyleLabel= customStyleLabel+";width:"+scope.fd.Parameters.LabelStyle?.Width+";height:"+scope.fd.Parameters.LabelStyle?.Height;
                                scope.fd.customStyleElement = customStyleElement+"resize:both;width:"+scope.fd.Parameters.ElementStyle?.Width+";height:"+scope.fd.Parameters.ElementStyle?.Height;
                                scope.fd.gridClassLabel="";
                                scope.fd.gridClassElement="";
                                
                            }else
                            {
                                scope.fd.customStyleLabel=customStyleLabel;
                                scope.fd.customStyleElement=customStyleElement;
                                if (scope.fd.colLength!==undefined)
                                {
                                    scope.fd.gridClassLabel=scope.fd.colLength[scope.fd.LabelLength-1];
                                    scope.fd.gridClassElement=scope.fd.colLength[scope.fd.ValueLength-1];
                                }
                            }
                }
                
                  scope.$watch('fd.Parameters',function(newValue,oldValue){
                  if (newValue!=oldValue && scope.fd.process_size_style)
                  {
                    scope.fd.process_size_style(newValue);
                  }
                 },true);
                /* scope.$watch('fd.Parameters.ElementStyle',function(newValue,oldValue){
                  if (newValue!=oldValue)
                  {
                    scope.fd.process_size_style(newValue);
                  }
                 },true);
                  
                scope.$watch('fd.Parameters.CustomSize',function(newValue,oldValue){
                   if (newValue!=oldValue)
                    {
                    scope.fd.process_size_style(newValue);
                    }
                });
               */
               scope.fd.process_size_style(scope.fd.Parameters);

                 
            }
        }
  
};