'use strict';

export function ktPageMenuExe () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktPageMenuExe.html',
            scope:{
                sideMenus:'=',
            },
            controller: ['$scope','$rootScope','MenuService',
          
            function($scope,$rootScope,MenuService){
               
                $scope.menu_service = MenuService;     

                 let currentUser=$rootScope.globals.currentUser;
              
            }],
            link: function (scope, elem, attrs) {

                 scope.setSideMenu = function(menuId) {     
                    scope.menu_service.setSelectedSidemenu(menuId) ;
                };
              
            
            }
        }
  
};