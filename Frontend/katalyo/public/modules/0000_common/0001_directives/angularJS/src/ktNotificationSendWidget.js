'use strict';

export function ktNotificationSendWidget (TaskDataService) {
   
   var controller = ['$scope', '$uibModal', '$state','$stateParams','$element','dragularService','$timeout','CodesService','ResourcesDataService','$rootScope','ProcessingService','MessagingService','GridsterService','WidgetDataExchangeService','CODES_CONST',
                     function ($scope,$uibModal,$state,$stateParams,$element,dragularService,$timeout,CodesService,ResourcesDataService,$rootScope,ProcessingService,MessagingService,GridsterService,WidgetDataExchangeService,CODES_CONST) {
      
      $scope.fd.localVars = {};
      $scope.fd.localVars.searchFormDownloaded = false;
      $scope.fd.localVars.formLoaded=false;
      $scope.fd.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_;
      $scope.fd.localVars.animationsEnabled = true;
      $scope.fd.localVars.widgetType = 1;
      $scope.fd.localVars.hidePopulateMethod = true;
      
      if ($scope.fd!=undefined)
      {
           $scope.fd.populateMethod = "M";
      }else
      {
           $scope.fd={populateMethod : "M"};
      }
      
      $scope.gsrvc = GridsterService;
      $scope.fd.localVars.gridsterType="resource";
      $scope.fd.localVars.resourceType="notification";
     
          
     if ($scope.localVars == undefined) $scope.localVars={};
                
      $scope.localVars.showMenu = false
      
      $scope.localVars.toggleDropdown = function(){
                                       $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
      }
    
        
      $scope.fd.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: GridsterService.getEmptyDatasetForm()
				} ;
        
     
  
        //$scope.taskId=$stateParams.id;
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        $scope.fd.localVars.taskId=$scope.taskId;
        $scope.fd.parentDatasetDefId=$scope.fd.related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
        $scope.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        $scope.formDefinition = $scope.resourceDefinition.form_definition;
        
        if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
        $scope.fd.localVars.resourceRecordId=0;
        if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        
     //register widget 
     var out_fields = ['resourceId']
     var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'viewResourceWidget',widgetId: $scope.fd.presentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.fd.localVars.selectedResource, output:out_fields, };
      $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
       $scope.fd.localVars.localId=$scope.fd.localVars.localWidgetId;  		
         
        $scope.fd.localVars.showProperties=function(datasetMapperType,showPropertiesRight)
      {
            
            if ($scope.fd.localVars.datasetMapperType==undefined)
            {
               $scope.fd.localVars.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                    $scope.fd.localVars.datasetMapperType = datasetMapperType;
               else 
                   $scope.fd.localVars.datasetMapperType=0;
            }
            if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);
            
      };
        
        $scope.fd.localVars.getFormResourceOriginal = function(){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.presentationId==null)
              {
                $scope.fd.presentationId = 0;
                
              }
              ResourcesDataService.getResourceForm($scope.fd.related,"i",lang).then(function(data) {
                            
                                $scope.fd.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                $scope.fd.localVars.dashboard.widgets.layout=data.data.widgets;
                                if ($scope.fd.localVars.selectedResourceItem!=undefined)
                                 { 
                                  $scope.fd.localVars.selectedResourceItem.name=$scope.fd.localVars.resourceWidgetDefinition.name;
                                  $scope.fd.localVars.selectedResourceItem.relatedId=$scope.fd.related;
                                  $scope.fd.localVars.selectedResourceItem.presentationId=$scope.fd.presentationId;
                                  $scope.fd.localVars.selectedResourceItem.src_type_id=$scope.fd.populateType;
                                  $scope.fd.localVars.selectedResourceItem.src_type_text=$scope.fd.localVars.src_type_text[$scope.fd.populateType];
                                  $scope.fd.localVars.selectedResourceItem.parentElementId=$scope.fd.parentElementId;
                                 }
                                else{
                                  $scope.fd.localVars.selectedResourceItem={'name':$scope.fd.localVars.resourceWidgetDefinition.name,'relatedId':$scope.fd.related, 'presentationId':$scope.fd.presentationId  ,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.fd.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
 
                                }
                                $scope.fd.wItemValue=data.data.widgets;
                                
                                var container = $element.children().eq(0);
                                GridsterService.setupDragular($scope.fd.wItemValue,container,'rows-'+$scope.fd.presentationId,'row-'+$scope.fd.localVars.gridsterType+'-hadle',true);
                                
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.presentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.fd.localVars.resourceRecordId}]}]});
						
                                  $scope.fd.localVars.searchFormDownloaded = true;
                                  $scope.fd.localVars.formLoaded=true;
                                     
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
        
        $scope.fd.localVars.getFormResource = function(){
              var lang=$rootScope.globals.currentUser.lang;
               if ($scope.fd.presentationId==null)
               {
                    $scope.fd.presentationId=0;
            
               }
               $scope.fd.localVars.formLoading=true;
                         
              ResourcesDataService.getResourceFormExtended($scope.fd.related,$scope.taskId,$scope.fd.localVars.formType,$scope.fd.presentationId,"w",lang).then(function(data) {
                               
                                  $scope.fd.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                  $scope.fd.localVars.dashboard.widgets.layout =  data.data.widgets;
                                 if ($scope.fd.localVars.selectedResourceItem!=undefined)
                                 { 
                                  $scope.fd.localVars.selectedResourceItem.name=$scope.fd.localVars.resourceWidgetDefinition.name;
                                  $scope.fd.localVars.selectedResourceItem.relatedId=$scope.fd.related;
                                  $scope.fd.localVars.selectedResourceItem.presentationId=$scope.fd.presentationId;
                                  $scope.fd.localVars.selectedResourceItem.src_type_id=$scope.fd.populateType;
                                  $scope.fd.localVars.selectedResourceItem.src_type_text=$scope.fd.localVars.src_type_text[$scope.fd.populateType];
                                  $scope.fd.localVars.selectedResourceItem.parentElementId=$scope.fd.parentElementId;
                                 }
                                else{
                                  $scope.fd.localVars.selectedResourceItem={'name':$scope.fd.localVars.resourceWidgetDefinition.name,'relatedId':$scope.fd.related, 'presentationId':$scope.fd.presentationId  ,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.fd.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
 
                                }
                              
                                $scope.fd.wItemValue=data.data.widgets;
                                $scope.fd.localVars.searchFormDownloaded = true;
                                  
                                  $scope.fd.localVars.formLoaded=true;
                                  $scope.fd.localVars.formLoading=false;
                                   
                                  var container = $element.children().eq(0).children().eq(1).children().eq(0);
                                  GridsterService.setupDragular($scope.fd.wItemValue,container[0],'rows-'+$scope.fd.presentationId,'row-'+$scope.fd.localVars.gridsterType+'-handle',true);
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 $scope.fd.localVars.formLoading=false;
                                 
                                
                             });
                              
      };
      
              
        $scope.fd.localVars.getFormResourceWithData = function(){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.presentationId==null || $scope.fd.presentationId==undefined)
              {
                $scope.fd.presentationId = 0;
                
              }
              
              if ($scope.fd.parentElementId==null || $scope.fd.parentElementId==undefined)
              {
                $scope.fd.parentElementId = 0;
                
              }

              
              ResourcesDataService.getResourceFormExtendedWithData($scope.fd.related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.populateType,$scope.fd.parentElementId,$scope.fd.presentationId,$scope.prevTaskExeId,$scope.fd.localVars.formType,$scope.fd.localVars.widgetType,$scope.fd.localVars.resourceRecordId,"w",lang).then(function(data) {

                                  $scope.formData[$scope.fd.presentationId] =  data.data.form_data;
                                  $scope.fd.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                  $scope.fd.localVars.dashboard.widgets.layout=data.data.widgets;
                                  $scope.fd.localVars.resourceRecordId = data.data.resourceRecordId;
                                  $scope.fd.localVars.datasetRecordId=$scope.fd.localVars.resourceRecordId;
                                  if ($scope.fd.localVars.selectedResourceItem!=undefined)
                                 { 
                                  $scope.fd.localVars.selectedResourceItem.name=$scope.fd.localVars.resourceWidgetDefinition.name;
                                  $scope.fd.localVars.selectedResourceItem.relatedId=$scope.fd.related;
                                  $scope.fd.localVars.selectedResourceItem.presentationId=$scope.fd.presentationId;
                                  $scope.fd.localVars.selectedResourceItem.src_type_id=$scope.fd.populateType;
                                  $scope.fd.localVars.selectedResourceItem.src_type_text=$scope.fd.localVars.src_type_text[$scope.fd.populateType];
                                  $scope.fd.localVars.selectedResourceItem.parentElementId=$scope.fd.parentElementId;
                                 }
                                else{
                                  $scope.fd.localVars.selectedResourceItem={'name':$scope.fd.localVars.resourceWidgetDefinition.name,'relatedId':$scope.fd.related, 'presentationId':$scope.fd.presentationId  ,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.fd.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
 
                                }
                                //$scope.fd.wItemValue=data.data.widgets;
                               
                                 //  $timeout(function() {
                                     $scope.fd.localVars.formLoaded=true;
                                     $scope.fd.localVars.formLoading=false;
                                     $scope.fd.localVars.searchFormDownloaded = true;
                                      
                                 //   });   
                                                                          
                                   WidgetDataExchangeService.setData({widgetId:$scope.fd.presentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.fd.localVars.resourceRecordId}]}]});
	                                 
                                   var container = $element.children().eq(0).children().eq(1).children().eq(0);
                                   GridsterService.setupDragular($scope.fd.wItemValue,container[0],'rows-'+$scope.fd.presentationId,'row-'+$scope.fd.localVars.gridsterType+'-handle',true);
                                   
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      
      
      
        $scope.fd.localVars.setField = function(form,name,value,msg)
        {
     				TaskDataService.setField(form,name,value);
            if (msg) MessagingService.addMessage(msg,'info');
        };
        
           $scope.fd.localVars.setSelectedDataset = function(id)  {
              
              if ($scope.fd.localVars.selectedResourceItem==undefined) $scope.fd.localVars.selectedResourceItem={};
              var pos = $scope.fd.localVars.datasetList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.fd.localVars.selectedResourceItem.id = $scope.fd.localVars.datasetList[pos].id;
                $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.datasetList[pos].name;
                $scope.fd.localVars.selectedDataset = $scope.fd.localVars.datasetList[pos];
                $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                $scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem;
                }
					}
          
                  
          // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
					$scope.fd.localVars.getDatasets = function()  {
						
            if ($scope.fd.localVars.datasetList==undefined) $scope.fd.localVars.datasetList=[];
          
            var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'notification').then(
						function(data) {
										
											  $scope.fd.localVars.datasetList = data.data;
                       if ($scope.fd.related!=null)
                       {
                        $scope.fd.localVars.setSelectedDataset($scope.fd.related);
                
                        }     
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					}
          
  if ($scope.fd.related!=null &&  !$scope.formDefinition && !$scope.fd.localVars.searchFormDownloaded)
  {
     $scope.fd.localVars.getFormResourceWithData();
    // $scope.fd.localVars.getDatasets();
    // $scope.fd.localVars.setSelectedDataset($scope.fd.related);
    
  }else if ($scope.fd.related!=null)
  {
     $scope.fd.localVars.getFormResource();
     $scope.fd.localVars.getDatasets();
     //$scope.fd.localVars.setSelectedDataset($scope.fd.related);
    
  }
  
  
      }];

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktNotificationSendWidget.html',
            controller: controller,
            link: function (scope, elem, attrs) {

           
            
           // scope.resourceData = {};
  
            scope.fd.localVars.prepareResourceData = function(resourceRecordId){

             scope.fd.itemValue =  ResourcesDataService.prepareResourceWidgetData(scope.fd.localVars.dashboard.widgets.layout,resourceRecordId);
            }
          
           scope.fd.localVars.processWidgetDependencies = function() { //-----  ktUploader  -----
              
           if (scope.fd.tItemValue!=undefined) 
            for (var i=0;i< scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<scope.fd.tItemValue[i].DEI.length;j++) {
                        if ((scope.fd.tItemValue[i].DEI[j].name==linkedItem) && (scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("view "+scope.fd.presentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                               // console.log("view "+ scope.fd.presentationId+" dobio event " + event.name);
                                for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    scope.fd.localVars.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    scope.resourceRecordId = scope.fd.localVars.linkedResourceId;
                                  }
                                  if (data[k].name=='resourceId') scope.fd.localVars.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                }
                          
                              //get selectedRecord
                              //scope.getFormResourceWithData();
                                
                                    //	data: {resource_def_id:scope.fd.linkedResourceDefId,resource_id:scope.fd.linkedResourceId}
                                    
                              }) //on_END
                        }//if
                    }//for j
                }//if

             // else {  scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget

          if (scope.fd.presentationId==undefined || scope.fd.presentationId==null) scope.fd.presentationId=0;
          
          scope.fd.localVars.formType=scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies(scope.taskId,scope.fd.presentationId,scope.fd.localVars.formType).then(function(data) {
                               
                                scope.fd.tItemValue=data.data;
                                scope.fd.localVars.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
          
            scope.$on('InitiateTaskPostProcess',function(event, data){
              
            if (data!=undefined){
              for(var i=0;i<data.length;i++) {
                if (scope.fd.presentationId==data[i].widgetId) WidgetDataExchangeService.setData(data[i]); 
              }
              //set all elements read-only
            }
              scope.fd.localVars.setField(scope.fd.localVars.dashboard.widgets.layout,'readOnly',true);
          });
  
      }     
    }
        
};