'use strict';

export function ktActionSendNotification () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktActionSendNotification.html',
             
             controller: function($scope,$rootScope,ResourcesDataService,KatalyoStateManager,MessagingService){
              
              $scope.notificationSelected = function(item,model)
                        {
                            $scope.act.actionParams.notificationId = item.id;
                            
                        };
                        
              $scope.ksm = KatalyoStateManager
                var lang = $rootScope.globals.currentUser.lang;
               
                let dependent_field_pid = $scope.fd.LocalId;
                let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
                let dependent_field_pid2
                let update_presentation_id2 = function(data) {
                    let presentation_id = angular.copy($scope.fd.PresentationId)
                    if (data[presentation_id]!==undefined)
                    {
                        $scope.fd.PresentationId = data[presentation_id];
                    //unsubscribe here later after implementing unsubscribe - ToDo
                    dependent_field_pid2 = $scope.fd.PresentationId
                    let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                    }
                }
                let update_presentation_id = function(data) {
                    if (data[$scope.fd.LocalId]>0 && $scope.fd.PresentationId==0 && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
                  
                    let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                    //unsubscribe here later after implementing unsubscribe - ToDo
                }
                
                
                if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0)
                {
                    let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
                } else{
                    dependent_field_pid2 = $scope.fd.PresentationId
                    let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                }

               $scope.setNotification = function(){
                if ($scope.act!=null && $scope.act!=undefined)
                {
                if ($scope.act.actionParams.notificationId!=null) {
                                  var pos = $scope.notificationList.map(function(e) { return e.id; }).indexOf($scope.act.actionParams.notificationId);
                                  if (pos>-1) {
                                     $scope.act.actionParams.notification = $scope.notificationList[pos];
                                         
                                   }
                                  
                  }
                }
             }; 
             $scope.getNotificationList = function()
             {
                if ($scope.notificationList==undefined)
                {
                   ResourcesDataService.getResourceDefListByType(lang,'notification').then(function(data) {
                                   
                                     $scope.notificationList = data.data;
                                     $scope.act.notificationsLoaded = true;
                                     $scope.setNotification();
                                  
                                    
                                  },function(error){
                                   
                                     MessagingService.addMessage(error.msg,'error');
                               });
                }else $scope.setNotification();
                
              }; 
                
                /*
                 $scope.$watch('act.notificationId', function(oldValue,newValue){
                     
                     $scope.getNotificationList();
                   });
               */
            
             
             $scope.getNotificationList(); 
             
             },
             link: function (scope, elem, attrs) {
            
     
        }
    }
};

//export {ktActionInitiateTask};