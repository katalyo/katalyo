'use strict';

export function ktUser () {

        return {
            restrict: 'A',
            controller: function ($scope, $element,UserService,ResourcesDataService,$rootScope,ProcessingService,GridsterService,CodesService,$templateCache,$compile){
                $scope.gsrvc = GridsterService;
                $scope.localVars={};
                var lang=$rootScope.globals.currentUser.lang;
                $scope[$scope.fd.id]  = {'fd': $scope.fd}

                if  ($scope.formData==undefined ||  !Array.isArray($scope.formData))
                {
                    $scope.formData=[{}]
                }

                CodesService.getCodesByName('usertags',lang).then(function (data) {
                    //debugger
                        $scope.fd.tagList = data.data;
                        
                         var pos = $scope.fd.tagList.map(function(e) { return e.id; }).indexOf($scope.fd.UniqueId);

                           if (pos>=0){
                               $scope.fd.UniqueIdObj=$scope.fd.tagList[pos];
                              
                           }
                     });
                     
                      $scope.fd.searchList=[
                            {id:1,filter:'onlist',code_name:'onlist',name:"On list",icon:"fa fa-check"},
                            {id:2,filter:'notonlist',code_name:'notonlist',name:"Not on list",icon:"fa fa-times"},
                        ];
                        
                        // get search filter in correct language
                        $scope.fd.processFilters = function(filterList)
                        {
                            for (let i=0;i<$scope.fd.searchList.length;i++)
                            {
                                let pos = filterList.map(function(e) { return e.id; }).indexOf($scope.fd.searchList[i].code_name);
                                if (pos>=0) $scope.fd.searchList[i].name = filterList[pos].name
                            }
                           
                            
                            if ($scope.fd.Search===undefined || $scope.fd.Search===null)   $scope.fd.Search=$scope.fd.searchList[0];//initialize to onlist
                            if ($scope.fd.Search)  $scope.fd.Search.Value=$scope.formData[0][$scope.fd.FieldName]; 

                            $scope.fd.filterReady=true
                        }
                      if ($scope.showSearchFilters || $scope.resourceDefinition.form_definition)
                        {
                            CodesService.getCodesByName('search_filters',lang).then(function (data) {
                                        //debugger;
                                        try {
                                        //$scope.fd.processFilters(data.data)
                                        $scope[$scope.fd.id]['fd'].processFilters(data.data) 
                                        } catch (err) {
                                                console.log(err)
                                        }
                                        
                            });
                        }

                $scope.fd.applyListFilters = function(index) {
                       
                      $scope.fd.Search=$scope.fd.searchList[index];
                      if( typeof $scope.formData[0][$scope.fd.FieldName] === 'string' ) {
                                //$scope.fd.search.value = [ $scope.fd.sItemValue ];
                                $scope.fd.Search.value = [ {'id':$scope.formData[0][$scope.fd.FieldName]} ];
                      }
                    
                };
                    
                    
                $scope.showProperties = function(event) {
                      $scope.gsrvc.setFieldProperties($scope.fd,event);
                }; 
                
                $scope.fd.colLength=[];
                for (var i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));
                
                if ($scope.fd.PopulateType==null) {$scope.fd.PopulateType=1;}    
                $scope.selectedUsr={};
                $scope.userItem = {selectedUser : null};
              
                  
                $scope.setUser = function (user_id) { 
                   
                   if  ($scope.formData==undefined ||  !Array.isArray($scope.formData))
                   {
                    $scope.formData=[{}]
                   }
                   // UserService.getUserObjectById($rootScope.globals.currentUser.userid).then(function(data){
                    let pos = $scope.userList.map(function(e) { return e.id; }).indexOf(user_id);

                           if (pos>=0){
                              $scope.formData[0][$scope.fd.FieldName]=$scope.userList[pos]
                              $scope.fd.Search.Value = [ $scope.formData[0][$scope.fd.FieldName] ];
                              
                           }
                    
                      
                };   

             

                  
                $scope.getUsers = function () {

                  UserService.getUsersGroups('users').then(function(data){
                      
                      $scope.userList = data.data;
                      $scope.localVars.userList = data.data;
                      $scope.usersLoaded=true;
                      
                        if ($scope.fd.PopulateType.toString()=='2') {
                                $scope.setUser($rootScope.globals.currentUser.userid);
                        }else if ($scope.formData!=undefined && $scope.formData.length>0) {
                        
                            if ($scope.formData[0][$scope.fd.FieldName]!=null && $scope.formData[0][$scope.fd.FieldName]!=undefined && Number($scope.formData[0][$scope.fd.FieldName])!=NaN) {
                               
                                  $scope.setUser($scope.formData[0][$scope.fd.FieldName])
                               
                            }
                            else if ($scope.formData[0][$scope.fd.FieldName]!=null && $scope.formData[0][$scope.fd.FieldName]!=undefined && Array.isArray($scope.formData[0][$scope.fd.FieldName])) {

                                if ($scope.formData[0][$scope.fd.FieldName].length>0)
                                  {
                                    
                                   $scope.setUser($scope.formData[0][$scope.fd.FieldName][0])
                                     
                                  
                                }
                            }
                      }
                 });

                }//GetUsers
          
                $scope.clear = function(event,select) 
                {
                  $scope.formData[0][$scope.fd.FieldName] = null;
                  $scope.fd.Search.value = null;
                  $scope.userItem.selectedUser = null;
                }
                
                $scope.$watch('userItem.selectedUser', function(newValue,oldValue){
                    //debugger
                    if ($scope.formData && newValue)  $scope.formData[0][$scope.fd.FieldName]=newValue.id;
                });
                 let template;
                if ($scope.resourceDefinition.form_definition)  template = $templateCache.get('ktdirectivetemplates/ktUser.html');
                else template = $templateCache.get('ktdirectivetemplates/ktUserExe.html');
                $element.append($compile(template)($scope));  
        
  				      $scope.getUsers();
          

            },
       link: function (scope, elem, attrs) {
          

           }
        }
  
};