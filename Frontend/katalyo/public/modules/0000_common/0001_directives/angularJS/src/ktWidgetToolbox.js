'use strict';

export function ktWidgetToolbox () {

        return {
                restrict: 'A',
                templateUrl: 'ktdirectivetemplates/ktWidgetToolbox.html',
               
                controller: ['$scope','$rootScope','GridsterService','TaskDefinitionService','KatalyoStateManager','$timeout', function($scope,$rootScope,GridsterService,TaskDefinitionService,KatalyoStateManager,$timeout){
              
                        //$scope.CTRL='ktWidgetToolbox';
                        $scope.gsrvc = GridsterService;
                        $scope.toolboxItems = [];
                        $scope.tds = TaskDefinitionService;
                        $scope.collapsed={};
                        $scope.reloadingToolbox = true;
                        $scope.dragula_bag='bag-'+$scope.gridsterType
                        $scope.gsrvc.destroyDragula($scope.dragula_bag);
                        
                        var widgetHandle = $scope.gridsterType+'-element-drag-handle';
        
                        var setupDragulaResult;
                        
                        $scope.gsrvc.setupDragula($scope.dragula_bag,$scope.gridsterType+'-toolbox',widgetHandle);
                        $scope.widgetToolboxItems= $scope.gsrvc.getWidgetToolboxItems($scope.gridsterType);
                        $scope.not_toolbox_group_class='item '+$scope.gridsterType+'-element-drag-handle'
                        //register to state manager
                        $scope.ksm = KatalyoStateManager;

                        let toolbox_loaded = function(data)
                        {
                            $scope.widgetToolboxLoaded(data.state);
                        };
                        
                         let toolbox_reload = function(data)
                        {
                          $scope.widgetToolboxLoaded(data);
                        };
                        
                        let toolboxState= $scope.ksm.addSubject($scope.gridsterType+"ToolBox",null);
                        let toolboxStateName = toolboxState.subject.name;

                        let action_toolbox_loaded = $scope.ksm.addAction(toolboxStateName,"toolboxLoaded",toolbox_loaded);
                        
                        let action_toolbox_reload = $scope.ksm.addAction(toolboxStateName,"toolboxReload",toolbox_reload);
                        
                         $scope.setCollapse=function(value,element_type){
                            if (element_type=="toolboxgroup")
                            {
                                if ($scope.collapsed[value]===undefined) $scope.collapsed[value] = true
                                else
                                {
                                    if ($scope.collapsed[value]) $scope.collapsed[value] = false
                                    else $scope.collapsed[value] = true;
                                }
                            }
                        }

                        $scope.widgetToolboxLoaded=function(value){
                                
                               
                                if (!value)
                                {
                                    $scope.gsrvc.destroyDragula('bag-'+$scope.gridsterType);
                                  
                                }
                                else
                                {
                                    $scope.widgetToolboxItems= $scope.gsrvc.getWidgetToolboxItems($scope.gridsterType);
                                }
               
                        };
                        
                        $scope.toolboxStatus=function(value){
        
                         $scope.gsrvc.SetWidgetToolboxLoaded(value,$scope.gridsterType);
                        };
             
             }],
             link: function (scope, elem, attrs) {
              
                       
              }
      }
};