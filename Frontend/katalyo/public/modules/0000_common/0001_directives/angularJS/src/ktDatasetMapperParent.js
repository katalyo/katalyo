'use strict';



export function ktDatasetMapperParent () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktDatasetMapperParent.html',
              controller: function($scope,CodesService,ResourcesDataService,GridsterService,$rootScope,ProcessingService,dragularService,$timeout,CODES_CONST,MessagingService){
               
                 $scope.fd.formPriorityParent='e';
              $scope.fd.ParentSearchPriority='d';
              $scope.src_type_text_parent=CODES_CONST._POPULATE_TYPE_NEW_;
              $scope.fd.PopulateTypeParentLocal=1;
              
              $scope.gsrvc = GridsterService

              if ($scope.fd.PopulateTypeParent!=null)
              {
                var rpType = $scope.fd.PopulateTypeParent;
                var formPriorityParent = rpType.toString().substr(1,1);
                var parentSearchPriority = rpType.toString().substr(2,1);
                $scope.fd.PopulateTypeParentLocal=rpType.toString().substr(0,1);
              
                if (formPriorityParent=="i" || formPriorityParent=="e")
                {
                  
                  $scope.fd.formPriorityParent = formPriorityParent;
                }
                
                if (parentSearchPriority=="i" || parentSearchPriority=="d")
                {
                  
                  $scope.fd.ParentSearchPriority = parentSearchPriority;
                }
              }

              $scope.fd.selectedResourceItemParent={}
              $scope.fd.selectedResourceItemParent.src_type_text_parent = $scope.src_type_text_parent[$scope.fd.PopulateTypeParent];
              $scope.fd.selectedResourceParent=[];
              if ($scope.fd.selectedResourceItemParent != undefined)
              {
                 $scope.fd.selectedResourceParent.push($scope.fd.selectedResourceItemParent);
                
              }

            $scope.fd.datasetListParent=[];
            
            $scope.fd.SetSelectedDatasetParent = function(id)  {
              
              
              var pos = $scope.fd.datasetListParent.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.fd.selectedResourceItemParent.id = $scope.fd.datasetListParent[pos].id;
                $scope.fd.selectedResourceItemParent.name = $scope.fd.datasetListParent[pos].name;
                $scope.fd.selectedDatasetParent = $scope.fd.datasetListParent[pos];
                $scope.fd.selectedResourceItemParent.name_confirmed = $scope.fd.selectedResourceItemParent.name +' ('+$scope.fd.selectedResourceItemParent.id +')'
                $scope.fd.selectedResourceItemParent_c=$scope.fd.selectedResourceItemParent;
                $scope.fd.ParentDatasetId = $scope.fd.selectedDatasetParent.id
                $scope.fd.datasetListParentDownloaded = true
                
                $scope.fd.SetPopulateTypeParent($scope.fd.PopulateTypeParentLocal)
                }
					}
          
           $scope.fd.localVars.setSelectedSourceParent = function(source_id){
             
                  if (source_id==-1)
                  {
                    
                    $scope.fd.PopulateTypeParentLocal = '3';
                    $scope.fd.formPriorityParent = 'e';
                    
                  }else
                  {
                   
                    $scope.fd.PopulateTypeParentLocal = '2';
                    $scope.fd.formPriorityParent = source_id.form_type;
                    $scope.fd.SrcParentWidgetId =  source_id.parentElementId;
                    if (source_id.type=='rlist') $scope.fd.PopulateTypeParentLocal = '4';
                    
                  }
             }; 
                  
          // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST) - get only potential parents
					$scope.fd.getDatasetsParent = function()  {
						var lang=$rootScope.globals.currentUser.lang;
                        $scope.fd.datasetListParentDownloaded = false
            ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
						function(data) {
										
						$scope.fd.datasetListParent = data.data;

                       if ($scope.fd.ParentDatasetId!=null && $scope.fd.ParentDatasetId!=0)
                       {
                        $scope.fd.SetSelectedDatasetParent($scope.fd.ParentDatasetId);
                
                        }else{

                            $scope.fd.datasetListParentDownloaded = true

                        }     
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					}
				  
          
          
				  $scope.fd.getDatasetsParent();
          
           $scope.fd.changePriorityParent= function(id){
             
             $scope.fd.formPriorityParent = id;
            
            };
           $scope.fd.changeParentSearchPriority= function(id){
             
              $scope.fd.ParentSearchPriority = id;
            };     
         $scope.fd.confirmParent= function(){
               
          
               if  ($scope.fd.selectedResourceItemParent_c!=undefined){
                
                      if ($scope.fd.ParentDatasetId!=$scope.fd.selectedResourceItemParent_c.id && $scope.fd.PopulateTypeParent!=null && $scope.fd.PopulateTypeParent!=undefined)
                      {
                          $scope.fd.ParentDatasetId=$scope.fd.selectedResourceItemParent_c.id;
                       
                      }
                      $scope.fd.selectedResourceItemParent.name_confirmed = $scope.fd.selectedResourceItemParent_c.name +' ('+$scope.fd.selectedResourceItemParent_c.id +')'

                 }
               
               $scope.fd.selectedResourceItemParent.src_type_text = $scope.src_type_text_parent[$scope.fd.PopulateTypeParentLocal];
               
               $scope.fd.PopulateTypeParent=$scope.fd.PopulateTypeParentLocal+$scope.fd.formPriorityParent+$scope.fd.ParentSearchPriority;
               
            };

               // FUNCTION ONSELECT DATASET CALLBACK
              $scope.fd.onSelectDatasetParentCallback = function (item, model){
                  	var eventResultId=item.id;
                  	var eventResultName=item.name;
                    var eventResultParentElementId =item.parentElementId;
                    if (item.id!=$scope.fd.selectedResourceItemParent_c?.id){
                      $scope.fd.localVars.getFormWidgetsParent(item.id,$scope.fd.PopulateTypeParentLocal);
                      
                    }
              
                	  $scope.fd.selectedResourceItemParent_c = {id:eventResultId, name : eventResultName,parentElementId:eventResultParentElementId};
                    
                    
              }//ONSELECT DATASET CALLBACK
            
             $scope.fd.localVars.getFormWidgetsParent = function(resource_id,type){
          

                  var lang=$rootScope.globals.currentUser.lang;
                    let formVersion = $scope.gsrvc.getTaskFormVersion($scope.fd.localVars.taskId+$scope.fd.localVars.formType)

                 if (formVersion!=undefined)
                  {
                  ResourcesDataService.getFormWidgets(resource_id,$scope.fd.localVars.taskId,$scope.fd.UuidRef,1,lang,formVersion).then(function(data) {
                                 $scope.fd.localVars.currentTaskInitiateDatasetsParent = data.data.initiate_widgets;
                                 $scope.fd.localVars.currentTaskExecuteDatasetsParent = data.data.execute_widgets;
                                 $scope.fd.localVars.currentDownloadedParent = true;
                                  
                                  
                    },function(error){
                                 MessagingService.addMessage(error.msg,'error');
                      });

              }else MessagingService.addMessage('Error - Form version is undefined in dataset mapper directive (getFormWidgetsParent function)','error');
             };
           
           
              
                  $scope.fd.SetPopulateTypeParent= function(populateType){
                        
                     $scope.fd.PopulateTypeParentLocal=populateType;
                       
                     if ($scope.fd.selectedResourceItemParent_c!=undefined)
                     {
                            $scope.fd.localVars.getFormWidgetsParent($scope.fd.selectedResourceItemParent_c.id,$scope.fd.PopulateTypeParentLocal);
                     }
                         
                        
                  }
                       
              $scope.fd.getDatasetSources= function(){
                        
                        var radio = $scope.fd.PopulateTypeParentLocal;
                        
                       }
                       
             },
             link: function (scope, elem, attrs) {
             
            
             
            
             //$scope.getDatasetSources();
          
             }
        }
};