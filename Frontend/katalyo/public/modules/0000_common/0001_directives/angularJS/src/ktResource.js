'use strict';

export function ktResource () {

        return {
            restrict: 'A', 
			controller: function ($scope, $rootScope,$element,$compile,$templateCache,ResourcesDataService,ProcessingService,$timeout,$q,MessagingService,CODES_CONST,GridsterService,WidgetDataExchangeService,KatalyoStateManager) {

	
        
        $scope.gsrvc = GridsterService;
      
        
        $scope.fd.localVars={};   
        $scope[$scope.fd.id]  = {'fd': $scope.fd, 'localvars':$scope.fd.localVars}        
        
        $scope.fd.localVars = {dashboard:{widgets:[]}}
        $scope.fd.localVars.datasetlistlinked = false
        $scope.fd.localVars.gridsterType = 'dataset'
        if ($scope.formData == undefined) $scope.formData=[{}];
        if ($scope.formData.length == 0) $scope.formData.push({});
        if ($scope.formData[0][$scope.fd.FieldName] === undefined) $scope.formData[0][$scope.fd.FieldName] =[{}]
        let newValueSingle,newValueMultiple;
        
        $scope.fd.localVars.TablePrefix = 'resource'+$scope.fd.Related+'_'+$rootScope.globals.currentUser.organisation.id
        
        $scope.fd.localVars.formParams = {formViewType:1,showGridNavigation:true}

        $scope.ctrl = this;
        $scope.CTRLNAME='ktResource'
        
        if ($scope.fd.Parameters===undefined || $scope.fd.Parameters===null) $scope.fd.Parameters={}
        if ($scope.fd.Parameters.ResourceRelationshipType===undefined) $scope.fd.Parameters.ResourceRelationshipType='new'

        if ($scope.fd.MaxLength===undefined) $scope.fd.MaxLength=1
             
       
        var widgetDefinition = {DEI: [{'name':'resourceLinkDataset','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'datasetListWidget',widgetId: $scope.fd.PresentationId,taskId:  $scope.lTaskId,
                                resourceDefId : $scope.fd.Related, //resourceDefId : $scope.selectedResource
                                resourceRecordId:$scope.localVars.resourceRecordId,task_initiate_id:$scope.resourceDefinition.task_initiate_id,task_execute_id:$scope.resourceDefinition.task_execute_id, output:null };
        $scope.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
        
        if (!$scope.fd.Saved) $scope.fd.ItemId = $scope.localVars.localWidgetId

        $scope.ksm = KatalyoStateManager;
        
        $scope.populateValue = function(newValue)
        {
                if ($scope.fd.MaxLength>0)
                {
                 newValueMultiple = newValue;
                 newValueSingle="";
                }
                else
                {
                 newValueMultiple = [];
                 newValueSingle=newValue;
                }
               
                $scope.itemValue.val = newValue;
                $scope.formData[0][$scope.fd.FieldName] = $scope.itemValue.val;
                $scope.fd.Search.value = $scope.formData[0][$scope.fd.FieldName];
                $scope.ksm.executeAction(valueStateName,"valueChanged", newValue);
               
                WidgetDataExchangeService.setData({
                    DEI: [{name:'resourceLinkDataset',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:newValueSingle}
                        ,{name:'resourceRecordIds',value:newValueMultiple},{name:'resourceRecordsAll',value:$scope.fd.datasetItems}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});   
                
        }
      
         /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
       
        if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
        $scope.itemValue = {val:null};
        
        let valueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+"-"+$scope.fd.ElementType+'ValueStateName'+$scope.fd.PresentationId,$scope.itemValue.val);
        let valueStateName = valueState.subject.name;
        
        let value_changed = function(data)
        {
            valueState.subject.setStateChanges = data;
            valueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
        
        let apply_value = function(data)
        {
            let local_data;        
            if ($scope.itemValue.val!=data)
            {
                local_data = data;
                if ($scope.fd.Parameters.DependentFieldFilter!=undefined)
                {
                        if ($scope.fd.Parameters.DependentFieldFilter!="" || $scope.fd.Parameters.DependentFieldElement!="")
                        {
                                if (Array.isArray(data))
                                {
                                        if (data.length>0) local_data = data[0];
                                        else local_data = null;
                                        
                                }
                                else
                                {
                                     local_data = data;
                                }
                                let dItems ;
                                if ($scope.fd.ShowField && !$scope.fd.ReadOnly) dItems = $scope.fd.datasetItems;
                                else dItems = $scope.fd.datasetItemsHidden;
                                if (dItems==undefined)
                                {
                                        $scope.fd.refreshDatasetDataList("",true,local_data);
                                        local_data=null;
                                }
                                else
                                {
                                        let element1,element2;
                                        if ($scope.fd.Parameters.DependentFieldFilter!="")
                                        {
                                                element1 = $scope.fd.Parameters.DependentFieldFilter;
                                                element2 = "id";
                                        }
                                        else
                                        {
                                                element1 = "id";
                                                element2 = $scope.fd.Parameters.DependentFieldElement;
                                        }
                                        let pos = dItems.map(function(e) { return e[element1]; }).indexOf(local_data[element2]);

                                        if (pos>=0) local_data = dItems[pos];
                                        else local_data = null;
                                }
                        }
                }
                $scope.populateValue(local_data);
            }
        }; 
       
        if ($scope.fd.Parameters.DependentFieldId!=undefined && $scope.fd.Parameters.DependentFieldId!=null)
        
        {
            if ($scope.fd.Parameters.DependentFieldId!="")
            {
                let dependent_field = "Field"+$scope.fd.PresentationId;
                let subscribedSubject = $scope.resourceDefinition.page_state_id+"-"+$scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value,true);
                let val = $scope.ksm.getState(subscribedSubject);
                $scope.populateValue(val.state);
            }
        }
        
          /************ Get selected records for dropdown **************/
        $scope.fd.getDatasetList = function()
        {
                if ($scope.formData[0][$scope.fd.FieldName+'l']!=undefined && $scope.formData[0][$scope.fd.FieldName+'l'].length>0 && $scope.fd.PresentationId!=null && $scope.fd.PresentationId!=undefined)
                {
                  ResourcesDataService.getResourceDataList($scope.fd.Related,$scope.fd.PresentationId,lang,{'id__in': $scope.formData[0][$scope.fd.FieldName+'l']}).then(function(data) {
                             
                             if ($scope.formData===undefined) $scope.formData = [{}];
                             if ($scope.fd.MaxLength==1) $scope.formData[0][$scope.fd.FieldName] = data.data[0];
                             else $scope.formData[0][$scope.fd.FieldName] = data.data;
                             $scope.populateValue($scope.formData[0][$scope.fd.FieldName]);
                             WidgetDataExchangeService.setSearch($scope.fd.PresentationId,$scope.taskExecuteId);
                             //$scope.ksm.executeAction(valueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                             //$scope.itemValue.val = $scope.formData[0][$scope.fd.FieldName];
                      },function(error){    
                      MessagingService.addMessage(error.msg,'error');
                  });
                }
        }
        
    $scope.fd.localVars.gridDataRetrieved =false;
    $scope.fd.localVars.results = {} 
    $scope.fd.localVars.gridData=[]
    
    /**********************************************************/
     /************ Populate ktResource smart table **************/
     /**********************************************************/
    $scope.fd.getDatasetDataForSmartTable = function(tableState, pageNumber,pageSize)
    {
         let deferred = $q.defer(); 
         if ($scope.fd.Related>0)
            {
                if (!$scope.resourceDefinition.form_definition)
                {
                
                    let presentation_id = $scope.fd.PresentationId
                    if (presentation_id==null || presentation_id===undefined)  presentation_id=0
                    var lang=$rootScope.globals.currentUser.lang;
                    if ($scope.fd.localVars.gridData===undefined) $scope.fd.localVars.gridData = []
                    
                    //debugger //getDatasetDataForSmartTable //==ktRefreshData
                    
                    if (pageNumber==undefined) pageNumber = 1;
                    if (pageSize==undefined) pageSize = 5;
                   
                   let items = $scope.formData[0][$scope.fd.FieldName] 
                   if (items==undefined) items = []
                   if ($scope.fd.localVars.gridDataRetrieved === true || items.length===0)
                   {
                         
                           $scope.fd.localVars.results.columns=$scope.fd.localVars.columnDefs
                           $scope.fd.localVars.results.pageSize = pageSize;
                           $scope.fd.localVars.results.count =    $scope.fd.localVars.gridData.length
                           $scope.fd.localVars.results.numberOfPages = Math.ceil($scope.fd.localVars.results.count/pageSize)
                           $scope.fd.localVars.results.results = $scope.fd.localVars.gridData.slice( (pageNumber -1) * pageSize, pageNumber * pageSize)

                           deferred.resolve({data: $scope.fd.localVars.results});
                   }
                   else
                   {
                        if (items!=[{}])
                        {
                           ResourcesDataService.getResourceDataDirectFilter($scope.fd.Related,presentation_id,"i",lang,{id__in: items}).then(function(data) {
                                
                                $scope.fd.localVars.gridDataRetrieved = true;                        
                                if (presentation_id>0) $scope.fd.localVars.results.columns=data.columnsDef
                                else 
                                    {
                                        $scope.fd.localVars.results.columns = $scope.fd.GridState['columnsDef']
                                        //$scope.fd.localVars.restoreGridState()
                                    }
                                $scope.fd.localVars.results.pageSize = pageSize;
                                $scope.fd.localVars.gridData = data.data
                                $scope.fd.localVars.results.count =    $scope.fd.localVars.gridData.length
                                $scope.fd.localVars.results.numberOfPages = Math.ceil($scope.fd.localVars.results.count/pageSize)
                                if ($scope.fd.localVars.gridData) 
                                    $scope.fd.localVars.results.results = $scope.fd.localVars.gridData.slice( (pageNumber -1) * pageSize, pageNumber * pageSize)
                                else 
                                    $scope.fd.localVars.results = {}
                                
                                deferred.resolve({data: $scope.fd.localVars.results});
                                                   
                                },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                            });
                        }
                    }
                }
           
        }else

        {

            MessagingService.addMessage('Dataset is not selected. Please select dataset','info');
            deferred.resolve({data: $scope.fd.localVars.results});
        }
        
         return deferred.promise;
    }
      /*
        $scope.$watch('itemValue.val',function(newValue,oldValue){
                if (newValue!=oldValue && newValue!=undefined)
                {
                        $scope.populateValue(newValue);    
                
                }
        });
        */
        
        $scope.search=[
                            {id:1,filter:'onlist',name:"On list:",icon:"fa fa-chevron-right"},
                            {id:2,filter:'notonlist',name:"Not on list:",icon:"fa fa-expand"},
                      ];
				
        if ($scope.fd!=undefined) {
                  if ($scope.fd.Search==undefined)
                  {
                    $scope.fd.Search=$scope.search[0]; //MIGOR TODO - greska kod incijacije za multiple , mozda treba kod promjene na multiple ovo jos jednom okinuti
                  }
        } 

			  $scope.applySearchFilters = function(index) {
                   
				  $scope.fd.Search=$scope.search[index];
                            $scope.fd.Search.value = [ $scope.formData[0][$scope.fd.FieldName] ];
                            $scope.fd.isOpen=false;		
			  };
       
       
        $scope.src_type_text=CODES_CONST._POPULATE_TYPE_  
      
        $scope.fd.inputParamsDatasets = {placeholder:'Select dataset...',label:'Dataset',name:'datasets',columnSizeLabel:'col-24',
                hideLabel:false,paddingLeft:true,dropdownSize:'col-24',labelTextSize:'label-xs text-muted',sizeXs:true};
       
       
        $scope.fd.inputParamsSearchForm = {placeholder:'Select task...',label:'Search form task',name:'tasks',columnSizeLabel:'col-24',
                hideLabel:false,paddingLeft:true,dropdownSize:'col-24',labelTextSize:'label-xs text-muted',sizeXs:true};
                
       
        var lang = $rootScope.globals.currentUser.lang;
        
        $scope.localVars.isResourceSelected = false;
 
				$scope.localVars.selectedDataRecord=false;
 
				$scope.taskId=0;
       
        $scope.localVars.showForm = false;
        
        $scope.fd.localVars.gridOptions ={}
	
					   
        $scope.fd.RelationshipType = $scope.fd.MaxLength;
        
						
         
           $scope.fd.colLength=[];
           for (var i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));
          
        // $scope.fd.showSearchForm = false;
        
        $scope.fd.ParentDatasetDefId=$scope.fd.Related;
        if ($scope.resourceDefinition!=undefined)
        {
          $scope.taskId=$scope.resourceDefinition.ResourceDefId;
          $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
          $scope.formType=$scope.resourceDefinition.form_type;
          $scope.source_type=$scope.resourceDefinition.source_type;
          $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
          $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
          $scope.formDefinition = $scope.resourceDefinition.form_definition;
          
         
        
        }
        
         $scope.fd.gridsterType = "resource";
          
          $scope.localVars.resourceDashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: [],
          data:{}
				} ;
        if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
        $scope.fd.resourceRecordId=0;
        if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        if ($scope.fd.PresentationType==null)
        {
              $scope.fd.PresentationType = 1;
        }
            
          $scope.fd.tItemValue = $scope.fd.FormDef;
          if (typeof $scope.fd.tItemValue=="string") $scope.fd.tItemValue=[]; 
          $scope.fd.resourcesList=[];
          $scope.fd.resourceListLength=0;
          $scope.fd.selectedDataset = 0;
          //if ($scope.fd.Related!=null) $scope.fd.selectedDataset = $scope.fd.Related;
		


        //*********************************************************************************************
        //************************************ GET DATASET LIST ***************************************
		//*********************************************************************************************
       $scope.fd.localVars.getResources = function(search)  {
			const lang=$rootScope.globals.currentUser.lang
            $scope.fd.RelatedLoaded=false

              ResourcesDataService.getResourceDefListByTypeFilter(lang,'dataset',search,$scope.fd.selectedDataset).then(
			function(data) {
										
       			$scope.fd.resourcesList = data.data;
                            
                            if (!Array.isArray($scope.fd.resourcesList))   $scope.fd.resourcesList=[];            
                        
                            if ($scope.fd.Related!=null)
                            {
                                   $scope.fd.resourceId = $scope.fd.Related;
                                   $scope.fd.resourceListLength = $scope.fd.resourcesList.length;
                                   //select dataset from dropdown
                                    var pos = $scope.fd.resourcesList.map(function(e) { return e.id; }).indexOf($scope.fd.resourceId);

                                   if (pos>=0){
                                       $scope.fd.selectedResource=$scope.fd.resourcesList[pos];
                                      
                                       $scope.fd.isResourceSelected=true;
                                   }
                         
                            }

                            if ($scope.fd.Parameters.PublishedResourceId!=undefined) $scope.fd.localVars.getPublishedResources()
                            
                            $scope.fd.RelatedLoaded=true;
                											 
                     },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
       };	  

        /*********************************************************************************************/
        /************************ GET DATASET LIST FOR EXISTING RELATIONSHIP *************************/
        /*********************************************************************************************/
       $scope.fd.localVars.getExistingResources = function()  {

             $scope.fd.resourcesList.length=0
             $scope.fd.RelatedLoaded=false
             
             let resource_def_id = $scope.resourceDefinition.ResourceDefId_id
             if (resource_def_id===undefined) resource_def_id = $scope.resourceDefinition.ResourceDefId
             
             if (resource_def_id===undefined) resource_def_id = $scope.resourceDefinition.ParentDatasetDefId

            if (resource_def_id>0)
            {
              ResourcesDataService.getExistingResourceRelationships(resource_def_id).then(function(data) {
                                        
                $scope.fd.resourcesList = data.data;
                            
                            if (!Array.isArray($scope.fd.resourcesList))   $scope.fd.resourcesList=[];            
                        
                            if ($scope.fd.Related!=null)
                            {
                                   $scope.fd.resourceId = $scope.fd.Related;
                                   $scope.fd.resourceListLength = $scope.fd.resourcesList.length;
                                   //select dataset from dropdown
                                    var pos = $scope.fd.resourcesList.map(function(e) { return e.id; }).indexOf($scope.fd.resourceId);

                                   if (pos>=0){
                                       $scope.fd.selectedResource=$scope.fd.resourcesList[pos];
                                      
                                       $scope.fd.isResourceSelected=true;
                                   }
                         
                            }

                            if ($scope.fd.Parameters.PublishedResourceId!=undefined) $scope.fd.localVars.getPublishedResources()
                            
                            $scope.fd.RelatedLoaded=true;
               
                     },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                         });
          }else  MessagingService.addMessage('Dataset id error (directive fn getExistingResources)','error');
       };     
          
          
          
				if ($scope.resourceDefinition.form_definition)
                {
                 if ($scope.fd.Parameters.ResourceRelationshipType==='new' || $scope.fd.Parameters.ResourceRelationshipType==='view') $scope.fd.localVars.getResources();
                 else $scope.fd.localVars.getExistingResources();
                }
				  $scope.fd.resourceDataRecordSelected=false;

         $scope.fd.localVars.ShowSearchForm = function(id) 
 
        {   let show_search_form
            if ($scope.fd.ShowSearchForm) show_search_form = false
            else show_search_form = true
            $scope.fd.localVars.getPublishedResourceById(id,show_search_form)
            
        }
		/*********************************************************************************************/
        /******************************* GET PUBLISHED DATASET DETAILS *******************************/
        /*********************************************************************************************/	

        $scope.fd.localVars.getPublishedResourceById = function(id,show_search_form) 
        {
            if (show_search_form)  {
                let pos
                let old_publish_id = $scope.fd.Parameters.PublishedResourceId
                if ($scope.fd.FormDef!==undefined && $scope.fd.FormDef!==null && $scope.fd.FormDef.length>0) { 
                        pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                        $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                        $scope.fd.localVars.formLoaded=true;
                  } else 
                  {
                      pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                      $scope.fd.FormDef={}
                      if (pos>=0){
                        ResourcesDataService.GetPublishedResourceById(id).then(function (data) {
                                $scope.fd.FormDef = data.data.Form  
                                $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                                $scope.fd.localVars.formLoaded=true;
                            },function(error){    
                              MessagingService.addMessage(error.msg,'error');
                        });

                    }
                }
             
            }
        }    


         //*********************************************************************************************
          //************************************ GET TASK LIST ***************************************
					//*********************************************************************************************
					$scope.fd.getTasks = function(search)  {
						var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
										
											  $scope.fd.taskList = data.data;
                        
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
          }
					
        //  $scope.fd.getTasks('');
          
				    // when confirm selection clicked
            /*
              $scope.fd.confirmSelection = function ()
						{
							
							$scope.fd.SearchFormDownloaded=false;
							$scope.fd.resourceDashboard.widgets.length=0;
					 }
            	$scope.fd.cancel = function ()
						{
              $scope.fd.SearchFormDownloaded=false;
              $scope.fd.resourceDashboard.widgets.length=0;

            }
            */
	
        
         $scope.fd.rowSelectionChanged = function(row){
                             
                                $scope.formData[0][$scope.fd.FieldName]=row;
                                
                                $scope.fd.localVars.selectedDatasetRecordId = row[$scope.fd.localVars.TablePrefix+'_id'];                                 
                                $scope.fd.localVars.isRecordSelected=row.isSelected;
                                // count selected
                                $scope.fd.resourceDataRecordSelected=$scope.formData[0][$scope.fd.FieldName].length; 
                                
                                WidgetDataExchangeService.setData({
                                DEI: [{name:'resourceLinkDataset',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId},
                                {name:'resourceRecordIds',value:row},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptions.data}]}],
                                widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});

        };
        
        /* delete selected child record*/
        $scope.fd.localVars.deleteChildRecord = function()
        {
              var lang = $rootScope.globals.currentUser.lang;
              if ($scope.resourceRecordId>0 && $scope.fd.MaxLength==0 && $scope.fd.localVars.isRecordSelected)
              {
               
              ResourcesDataService.DeleteChildRelationship($scope.fd.PresentationId,$scope.parentResourceDefId,$scope.resourceRecordId,$scope.fd.localVars.selectedDatasetRecordId).then(function(data) {
                      MessagingService.addMessage(data.data,'success');
                       $scope.fd.localVars.gridData.length=0
                      $scope.fd.localVars.isRecordSelected = false
                      $scope.fd.localVars.GetResourceDataForParent();

                },function(error){    
                    MessagingService.addMessage(error.data,'error');
                                    });
            }       
        }
            
        $scope.fd.getResourceDataRefresh = function (retValue) 
            {
              var lang = $rootScope.globals.currentUser.lang;
              //ToDo ovo prebaciti sve u jedan API poziv
              if ($scope.resourceRecordId>0 && $scope.fd.MaxLength==0)
              {
              ResourcesDataService.getResourceDataM2m($scope.fd.PresentationId,$scope.parentResourceDefId,$scope.fd.Related,$scope.resourceRecordId).then(function(data) {
                            $scope.formData[0][$scope.fd.FieldName] = data; 
                            $scope.setupGrid(data);                
                },function(error){    
                    MessagingService.addMessage(error,'error');
									});
            }       
        }
            

        $scope.fd.localVars.GetResourceDataForParent = function () 
        {
              var lang = $rootScope.globals.currentUser.lang;
              if ($scope.resourceRecordId>0 && $scope.fd.MaxLength==0)
              {
                ResourcesDataService.getResourceDataM2m($scope.fd.PresentationId,$scope.parentResourceDefId,$scope.fd.Related,$scope.resourceRecordId,$scope.taskId,$scope.fd.ItemId,lang).then(function(data) {
                            $scope.formData[0][$scope.fd.FieldName] = data; 
                           // $scope.fd.localVars.setupGridNew(data);  BUG TODO!
                            $scope[$scope.fd.id]['fd'].localVars.setupGridNew(data);  
                            
                },function(error){    
                    MessagingService.addMessage(error.msg,'error');
                                    });
            }              
        }

            $scope.linkedWidgetId = 0;
            
            if ($scope.fd.Search!=undefined) $scope.linkedWidgetId = $scope.fd.Search.WidgetId;
            
            var refresh_list_from_callback=function(data)
            {
                $scope.fd.refreshDatasetDataList("",true,$scope.lData);
                
            }
              
       
            $scope.fd.datasetItems= []
            $scope.fd.refreshDatasetDataList = function(search,apply,val)
            {
              var lang = $rootScope.globals.currentUser.lang;
              //ToDo ovo prebaciti sve u jedan API poziv
              $scope.lData = val;
               if ($scope.fd.PresentationId!=null && $scope.fd.PresentationId!=undefined)
                {
                        var ctrl = this;
                        this.l_apply = apply;
                        this.l_val = val;
                        let fromCache=true;
                   //ResourcesDataService.getResourceDataList($scope.fd.Related,$scope.fd.PresentationId,lang,search).then(function(data) {
                   ResourcesDataService.getResourceDataListAll($scope.fd.Related,$scope.fd.PresentationId,lang,fromCache,refresh_list_from_callback).then(function(data) {
                           
                           if ($scope.itemValue.val==undefined || $scope.itemValue.val==null)
                           {
                                if (Array.isArray($scope.formData))
                                {
                                   if (Array.isArray($scope.formData[0][$scope.fd.FieldName+'l']))
                                        for (let i=0;i<$scope.formData[0][$scope.fd.FieldName+'l'].length;i++)
                                        {
                                                if ($scope.fd.MaxLength==1) $scope.itemValue.val={'id':$scope.formData[0][$scope.fd.FieldName+'l'][i]};
                                                else
                                                
                                                {
                                                        $scope.itemValue.val = [];
                                                        $scope.itemValue.val.push ({'id':$scope.formData[0][$scope.fd.FieldName+'l'][i]});
                                                        
                                                }
                                        }
                                }
                                
                           }
                           
                           if ($scope.fd.ShowField && !$scope.fd.ReadOnly)
                           {
                                $scope.fd.datasetItems = data.data;
                                if ($scope.fd.datasetItems.length>0)
                                {
                                    if (!$scope.fd.Required && $scope.fd.datasetItems[0].id!=null) $scope.fd.datasetItems.unshift({id:null});   
                                }
                                
                           }
                           else
                           {
                                $scope.fd.datasetItemsHidden = data.data;
                                if ($scope.fd.datasetItemsHidden.length>0)
                                {
                                    if (!$scope.fd.Required && $scope.fd.datasetItemsHidden[0].id!=null) $scope.fd.datasetItemsHidden.unshift({id:null});   
                                }
                           }
                           $timeout(function(){
                                //if (!$scope.fd.ReadOnly) $scope.loadDropdown($scope.fd.datasetItems);
                                if (ctrl.l_apply) apply_value(ctrl.l_val);
                           });
                    },function(error){    
                    MessagingService.addMessage(error.msg,'error');
									});
              
                }
            };
           
         
          if ($scope.taskExecuteId != 0 || $scope.taskInitiateId != 0) // MIGOR  TODO - DONT REFRESH DATALIST IF IN BUILD MODE ! (long live OR)
            if ($scope.fd.PresentationType==1 && $scope.fd.ShowField && !$scope.fd.ReadOnly) $scope.fd.refreshDatasetDataList(""); //This should be connected to search form
          /************ Call task when magnifier is clicked or autoSearch=true **************/
          
          $scope.fd.SelectDatasetRecords = function(taskId,autoSearch){
            
           // $scope.PreInitiateTask(taskId,0);
            $scope.fd.autoSearchLocal=autoSearch;
            $scope.localVars.showForm = true;
           if (!$scope.fd.searchFormDownloaded)
            
            {   if  (taskId==undefined || taskId==null || taskId==0) MessagingService.addMessage("Error in definition!! Search form is not assigned to dataset.","error");
               else 
               {
               $scope.localVars.StartTask(taskId,0,0);
               //$rootScope.$broadcast("ProcessTask"+$scope.fd.presentationId,{'taskId':taskId,'task_execute_id':$scope.taskExecuteId});
               //if (autoSearch) $scope.fd.ConfirmSelection();
          
                $scope.fd.searchFormDownloaded=true;
                
               }
            }
          };
     
    
          
          
          /*
           var localWidgetId="";  
            $scope.$on("Event"+localWidgetId+"resourceLinkDataset",function(event,data){
             
            for (var i=0;i<data[1].value.length;i++)
            {
             $scope.fd.gridOptions.data.push(data[1].value[i]);
            }
          });
          */
          
          
          /************ Confirm selection - populate with selected items from search form **************/
          
          
          $scope.fd.autoSearchLocal = true;
          var counter=0;
          
          $scope.fd.ConfirmSelection = function(){
            
            var items=[];
            if ($scope.fd.localVars.gridOptions.data!=undefined)
            {
              for (var i=0;i<$scope.fd.localVars.gridOptions.data.length;i++)
              {
              
             
               items.push($scope.fd.localVars.gridOptions.data[i].id);
              
              }
            }
            //Get also selected from search form
            var linkedItem = 'resourceLinkDataset';
            if ($scope.linkedWidgetId!=undefined) //linkedWidgetId = 2325;
            {
             var data = WidgetDataExchangeService.getDataByWidgetIdOnly($scope.linkedWidgetId,linkedItem);
             //var data = WidgetDataExchangeService.getDataByLocalTaskWidgetId(localTaskId,WidgetId,linkedItem);
             var fromWidget=[];
             for (var k=0;k<data.length;k++) {
                if ($scope.fd.AutoSearch && $scope.fd.autoSearchLocal)
                {
                   if (data[k].name=='resourceRecordsAll') fromWidget = data[k].value; 
                }
                else{
                     if (data[k].name=='resourceRecordIds') fromWidget = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                  }
              }
              
            
             if (fromWidget!=null)                         
             {
               for (var j=0;j<fromWidget.length;j++)
               {
               
                   var pos = items.map(function(e) { return e; }).indexOf(fromWidget[j].id);
               
                   if (pos<0) items.push(fromWidget[j].id);
                }
              
              }
            }
            if ($scope.formData!=undefined)
            {
              if ($scope.formData.length>0) $scope.formData[0][$scope.fd.FieldName+'l']=items;
            }
            
            if ($scope.fd.PresentationType==0)
            {
              if ($scope.fd.localVars.gridOptions.data!=undefined)
              {
                 if (items.length>0 && $scope.fd.localVars.gridOptions.data.length<items.length)
                 {
                   if ($scope.fd.Related>0 && items!=[{}]) $scope.fd.getDatasetDirect($scope.fd.Related,items);
                 
                 }
              }
            }
            
            if ($scope.fd.PresentationType==1 && items.length>0) $scope.fd.getDatasetList();
            
            //$scope.fd.searchFormDownloaded=false;
            
            
            $scope.localVars.showForm = false;
          };
              /************ Cancel selection - cancel selection from search form **************/
          
          $scope.fd.CancelSelection = function(){
             $scope.localVars.showForm = false;
            
          }
       
      
         //**********************************************************
         //           GET FORM FIELDS
         //**********************************************************      
            
          $scope.fd.getFormFields = function(){
              
              
              var lang=$rootScope.globals.currentUser.lang;
           
                         
              ResourcesDataService.getFormFields($scope.fd.Related,lang).then(function(data) {
                               
                                  $scope.fd.presentationElements= data.data;
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
                              
          };
      
      
       //**********************************************************
         //          Clear saerch task dropdown
         //**********************************************************      
            
           $scope.localVars.clearSearchTask = function(event,select){
            $scope.fd.Search.taskId = null;
           };
         //**********************************************************
         //          Remove selected records from grid
         //**********************************************************      
          
            
            
              $scope.fd.localVars.setupGridNew= function(data)
              {
                $scope.fd.localVars.gridData = data.data;
                $scope.fd.localVars.columnDefs = data.columnsDef;
                $scope.fd.localVars.gridDataRetrieved = true
              
              }
              
              
          
               
              
              
           $scope.fd.localVars.saveGridState= function(show_message) {
               //debugger //ktTaskDatasetList smarttable
               let columnsDef = $scope.fd.localVars.columnDefs

               let columnsQuery = $scope.fd.localVars.columnsForQuery
               $scope.fd['GridState']={'columnsDef':columnsDef,'columnsQuery': columnsQuery}
                                
              if (show_message)
               MessagingService.addMessage("To make changes stick please save form",'info');            
    };

    $scope.fd.localVars.restoreGridState= function() {
        //debugger
        if ($scope.fd['GridState']!==undefined && $scope.fd['GridState']!==null)
        {
          
            if ($scope.fd['GridState']['columnsDef']!==undefined) $scope.fd.localVars.columnDefs =  $scope.fd['GridState']['columnsDef']
            if ($scope.fd['GridState']['columnsQuery']!==undefined) $scope.fd.localVars.columnsForQuery =  $scope.fd['GridState']['columnsQuery']

        }
    };   
            
    if ($scope.fd['GridState']!==undefined && $scope.fd['GridState']!==null) $scope.fd.localVars.restoreGridState();

    $scope.fd.localVars.getResourceDataRelated= function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

           //scope.activity.activityMessage = ProcessingService.setActivityMsg('Retriving data '+scope.selectedResource);
          //$scope.filter = ResourcesDataService.prepareResourceWidgetData($scope.dashboard.widgets,-1);
         
          if ($scope.fd.selectedResource==null) {
            $scope.fd.selectedResourceId = $scope.fd.Related;
          }else
          {
             $scope.fd.selectedResourceId = $scope.fd.selectedResource.id;
            
          }
    //      ResourcesDataService.getResourceDataFiltered($scope.fd.Related,lang,$scope.filter).then(function(data) {//MIGOR
         ResourcesDataService.getResourceDataFiltered($scope.fd.Related,$scope.fd.ResourceId).then(function(data) {//MIGOR
                            
                            $scope.fd.setupGrid(data);
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });
        }

            
		 $scope.fd.getResourceDataFiltered = function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

           //scope.activity.activityMessage = ProcessingService.setActivityMsg('Retriving data '+scope.selectedResource);
          //$scope.filter = ResourcesDataService.prepareResourceWidgetData($scope.dashboard.widgets,-1);
         
          if ($scope.fd.selectedResource==null) {
            $scope.fd.selectedResourceId = $scope.fd.Related;
          }else
          {
             $scope.fd.selectedResourceId = $scope.fd.selectedResource.id;
            
          }
	//		ResourcesDataService.getResourceDataFiltered($scope.fd.Related,lang,$scope.filter).then(function(data) {//MIGOR
         ResourcesDataService.getResourceDataFiltered($scope.fd.selectedResourceId,$scope.taskId,lang,$scope.fd.resourceDashboard.widgets).then(function(data) {//MIGOR
                            
                            $scope.fd.setupGrid(data);
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });
        }
		
						
             
        $scope.fd.getResourceData = function () 
	{
                // $scope.activity.activityMessage = ProcessingService.setActivityMsg('Downloading resource data for '+$scope.resourceId);
                 var lang = $rootScope.globals.currentUser.lang;
                 ResourcesDataService.getResourceData($scope.resourceId,$scope.taskId,lang).then(function(data) {
                                                 
                                                                $scope.fd.localVars.setupGridNew(data);
        
                },function(error){    
                        MessagingService.addMessage(error.msg,'error');
                });
	};
       
        if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
    var lang=$rootScope.globals.currentUser.lang;

     
      $scope.fd.localVars.getPublishedResources=function()
      {
       $scope.fd.localVars.publishedLoaded=false        
      ResourcesDataService.GetPublishedResourcesByTypeId($scope.fd.Related,'form').then(
        function (data) {       
           if ($scope.fd.localVars===undefined) $scope.fd.localVars={}
           $scope.fd.localVars.publishedResourceList = data.data;
           $scope.fd.localVars.publishedLoaded=true
           if ($scope.formDefinition && $scope.fd.GridState?.columnsDef!=undefined) 

            {
                $scope.fd.localVars.gridDataRetrieved = true
                $scope.fd.localVars.resourceDataDownloaded = true
                
            }
           if ($scope.fd.Parameters.PublishedResourceId!=undefined) {

                $scope.fd.localVars.getPublishedResourceById($scope.fd.Parameters.PublishedResourceId,$scope.fd.ShowSearchForm)
            }
           },function(error){    
                          MessagingService.addMessage(error.msg,'error');
        });

    }

     //**********************************************************
     //           GET DATASET DIRECT FILTER
     //**********************************************************      
      $scope.fd.getDatasetDirect = function (relatedDatasetId,items) {

        ResourcesDataService.getResourceDataDirectFilter(relatedDatasetId,$scope.fd.PresentationId,"i",lang,{id__in: items}).then(function(data) {
                                
                                    $scope.fd.localVars.setupGridNew(data);
                               
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
                
    $scope.fd.localVars.getColumnsDef = function () {

        
            ResourcesDataService. getResourceColumns($scope.fd.Parameters.PublishedResourceId,'dataset',lang,$scope.fd.Parameters.PublishedResourceVersion).then(function(data) {//MIGOR
                            
                                  $scope.fd.localVars.columnDefs = data.data.columnsDef
                                  $scope.fd.localVars.results.columns =  $scope.fd.localVars.columnDefs
                                  $scope.fd.localVars.resourceDataDownloaded = true;
                                
                                  $scope.fd.localVars.saveGridState(false)
                                 
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });

    }

            // Resource is selected
              $scope.fd.onSelectCallback = function (item, model){
                var eventResultId=item.id;
                $scope.fd.eventResultName=item.name;
                
                $scope.fd.resourceId=eventResultId;
                $scope.fd.isResourceSelected=true;
				
                if ($scope.fd.Parameters.ResourceRelationshipType==='existing')
                {
                    $scope.fd.MaxLength=item.max_length
                    $scope.fd.Dbtype=item.db_type
                    $scope.fd.Parameters.FieldNameRelatedParent=item.field_name

                    if ($scope.fd.Parameters.FieldNameRelatedParent==undefined) $scope.fd.Parameters.FieldNameRelatedParent = 'Field'+item.rmdId
                    
                }
               	$scope.fd.Related=$scope.fd.resourceId;
                $scope.fd.localVars.getPublishedResources()
                if ($scope.fd.Related!=undefined && $scope.fd.Related!=null) $scope.fd.getFormFields();
                   
              }
              
            // Resource is selected
              $scope.fd.onSelectPublished = function (item, model){
                let pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(item.id);
                let old_publish_id = $scope.fd.Parameters.PublishedResourceId
                if (pos>=0){
                    $scope.fd.Parameters.PublishedResourceId = $scope.fd.localVars.publishedResourceList[pos].id;
                
                    $scope.fd.Parameters.PublishedResourceVersion = $scope.fd.localVars.publishedResourceList[pos].Version;
                    if ($scope.fd.GridState?.columnsDef===undefined || $scope.fd.Parameters.PublishedResourceId !=old_publish_id) $scope.fd.localVars.getColumnsDef()
                    else $scope.fd.localVars.resourceDataDownloaded =true
                }


                  
              }

              $scope.fd.onSelectSearchForm= function (item, model){
               
               if ($scope.fd.Search==undefined) $scope.fd['search']={taskId:item};
               else $scope.fd.Search.taskId = item;
                //$scope.fd.Search.widgetId=0; //ovo treba na serveru dohvatiti widgetId
                
                
              } 
        //ako ima odabranih dohvati ih i setupiraj
        if ($scope.formData!=undefined && $scope.formData.length>0 && !$scope.fd.Parameters.NoGetData)
        {
            if ($scope.formData[0][$scope.fd.FieldName]!=undefined){
                if ($scope.fd.PresentationType==0 && $scope.fd.Related>0 && !$scope.resourceDefinition.form_definition)
                {
                    //$scope.fd.localVars.resourceDataDownloaded =true
                    if ($scope.formData[0][$scope.fd.FieldName]!=[{}]) $scope.fd.getDatasetDirect($scope.fd.Related,[$scope.formData[0][$scope.fd.FieldName]]);
                }else if ($scope.fd.Related===null)
                {
                    MessagingService.addMessage('Dataset is not selected. Please select dataset','info');
                }else
                {
                    if  ($scope.fd.OverrideType!="f") $scope.fd.getDatasetList();
                }
            } else if ($scope.fd.PresentationType==0 && $scope.formParams.formViewType==1 && $scope.formParamsParent.formViewType!=2 && !$scope.resourceDefinition.form_definition)
            {
                if ($scope.fd.Related>0) $scope.fd.getDatasetDirect($scope.fd.Related,[]);
                else MessagingService.addMessage('Dataset is not selected. Please select dataset','info');
            }
        }   
        
        if ($scope.fd.MaxLength==0 && !$scope.formDefinition && !$scope.fd.Parameters.NoGetData) $scope.fd.localVars.GetResourceDataForParent()
        

        if ($scope.fd.Parameters.PublishedResourceId!=undefined) {

            if ($scope.formDefinition) {
                $scope.fd.localVars.getPublishedResources() 
                $scope.fd.getFormFields()
            }
            else $scope.fd.localVars.getPublishedResourceById($scope.fd.Parameters.PublishedResourceId,$scope.fd.ShowSearchForm)
            
        }
        
        let template;
        if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktResource.html');
        else template = $templateCache.get('ktdirectivetemplates/ktResourceExe.html');

          
        $element.append($compile(template)($scope));  
          
	},//...controller
				
           
            link: function (scope, elem, attrs) {
             		scope.fd.localVars.datasetlistlinked  = true
            }
    
    }
};