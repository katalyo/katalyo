'use strict';

export function ktTaskProcDirective () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskProcDirective.html',
            scope: {
                fd: '=',
                localVars: '=',
                taskDefinition:'=',
            },
 
     controller: function($scope,MessagingService,TaskDataService,PublicTaskDataService,MenuService,$timeout,$rootScope,GridsterService,$stateParams,WidgetDataExchangeService,dragulaService,KatalyoStateManager)
     {
        $scope.CTRL = "ktTaskProcDirective"
        $scope.gsrvc = GridsterService;
        if ($scope.localVars==undefined) $scope.localVars={};
        if ($scope.fd==undefined) $scope.fd={};
        $scope.localVars.gridsterType = "task";
        $scope.gsrvc.showFooter=true;
        if ($scope.taskDefinition!=undefined && $scope.localVars.previousTask==undefined) $scope.localVars.previousTask= $scope.taskDefinition.task_execute_id;
        else if ($scope.localVars.previousTask==undefined)  $scope.localVars.previousTask=0;
        //if ($scope.taskDefinition==undefined) $scope.taskDefinition =  {};
        $scope.localVars.potentialOwners = {};
        $scope.localVars.potentialOwners.potentialOwnersUsers = [];
        $scope.localVars.potentialOwners.potentialOwnersGroups = [];
           
        $scope.localVars.backCloseIcon = "fa fa-close";
        $scope.localVars.cancelBackBtnTxt = "Cancel";
        $scope.localVars.inlineTaskDefinition={};
        $scope.localVars.inlineTask={};
                   
        $scope.localVars.inlineTask = {
                id: '1',
                name: 'Home',
                widgets: $scope.gsrvc.getEmptyColumn(),
                widgetsOriginal: $scope.gsrvc.getEmptyColumn(),
                data:{}
            };
            
       
      if ($scope.localVars.selectedTasks==undefined) $scope.localVars.selectedTasks = [];
       
      $scope.localVars.selectedTaskIndex=-1;
      if ($scope.fd.type!=undefined)
      {
          if ($scope.fd.type=='itask')
          {
            if ($scope.fd.wItemValue!=null) {
               $scope.localVars.selectedTasks = $scope.fd.wItemValue;
            }else
            {
              $scope.fd.wItemValue = [];
              
            }
          }
          else
          {
              if ($scope.fd.tItemValue!=null) {
               $scope.localVars.selectedTasks = $scope.fd.tItemValue;
              }else
              {
              $scope.fd.tItemValue = [];
              
              } 
              
          }
      }
       
       $scope.ProcessDataFromStartTask = function(data)
       {
        
                if (data.taskdef!=undefined)  $scope.localVars.inlineTaskDefinition = data.taskdef;
                if ($scope.taskDefinition!=undefined)
                {
                    if ($scope.taskDefinition.task_execute_id==undefined) $scope.taskDefinition.task_execute_id = $scope.localVars.previousTask;
                    $scope.localVars.inlineTaskDefinition.prev_task_id = $scope.taskDefinition.task_execute_id;
                    $scope.localVars.inlineTaskDefinition.prev_task_transaction_id = $scope.taskDefinition.transaction_id;
                    $scope.localVars.inlineTaskDefinition.prev_task_object = $scope.taskDefinition;

                }else{
                   
                    $scope.localVars.inlineTaskDefinition.prev_task_id = $scope.localVars.previousTask;
                }
                $scope.localVars.inlineTaskDefinition.proc_type = data.procType;
                $scope.localVars.inlineTaskDefinition.form_type = data.formType;
                $scope.localVars.inlineTaskDefinition.transaction_id = data.transactionId;
                $scope.localVars.inlineTaskDefinition.taskExecuteInstance = data.taskExecuteInstance;
                $scope.localVars.inlineTaskDefinition.taskInitiateInstance = data.taskInitiateInstance;
                $scope.localVars.inlineTaskDefinition.inline_task=$scope.localVars.isInlineTask;
                $scope.localVars.inlineTaskDefinition.source_type = $stateParams.src_type;
                $scope.localVars.inlineTaskDefinition.taskFormLoaded = false;
                $scope.localVars.inlineTaskDefinition.inlineInitiateFormLoaded = true; 
                if ($scope.localVars.inlineTaskDefinition.taskExecuteInstance!=undefined && $scope.localVars.inlineTaskDefinition.taskExecuteInstance.length>0) $scope.localVars.inlineTaskDefinition.task_execute_id = $scope.localVars.inlineTaskDefinition.taskExecuteInstance[0].id;
                if ($scope.localVars.inlineTaskDefinition.taskInitiateInstance!=undefined && $scope.localVars.inlineTaskDefinition.taskInitiateInstance.length>0) $scope.localVars.inlineTaskDefinition.task_initiate_id = $scope.localVars.inlineTaskDefinition.taskInitiateInstance[0].id;
                $scope.localVars.inlineTask.resourceDefinition=data.resourceDefinition;  
            
                $scope.localVars.inlineTask.data = data.form_data;
               
                if ($scope.localVars.inlineTask.data==undefined) $scope.localVars.inlineTask.data = {};
                $scope.localVars.inlineTask.widgets.layout= data.widgets;
                if ($scope.localVars.inlineTask.resourceDefinition==undefined) $scope.localVars.inlineTask.resourceDefinition = {};
                $scope.localVars.inlineTask.resourceDefinition.formParams = {formViewType:2,formLocked : true};
          
                $scope.gsrvc.setFormViewType($scope.localVars.inlineTask.resourceDefinition.formParams.formViewType)
                                        
                $scope.gsrvc.setTaskFormVersion($scope.localVars.inlineTask.resourceDefinition.id+$scope.localVars.inlineTaskDefinition.form_type,$scope.localVars.inlineTask.resourceDefinition.FormVersion)

                $scope.ksm = KatalyoStateManager;
                //define task_closed action from parent task if parent exists
                if ($scope.taskDefinition!=undefined)
                {
                    $scope.localVars.inlineTask.resourceDefinition.page_state_id = $scope.taskDefinition.page_state_id
                    
                    let taskParentState= $scope.ksm.getSubject("task"+$scope.taskDefinition.transaction_id);
                    let task_closed = function(data)
                    {
                        //textValueState.subject.setStateChanges = data;
                        taskParentState.notifyObservers("task_closed");
                    };
                    
                    let action_task_closed;
                    if (taskParentState!=undefined) action_task_closed = $scope.ksm.addAction(taskParentState.name,"task_closed",task_closed);        
                
                    //Register task with state manager
                    let taskState= $scope.ksm.addSubject('task'+$scope.localVars.inlineTaskDefinition.transaction_id,$scope.localVars.inlineTaskDefinition);
                    let taskStateName = taskState.subject.name;
                            
                    let task_started = function(data)
                    {
                        //textValueState.subject.setStateChanges = data;
                        taskState.subject.notifyObservers("task_started");
                    };
                            
                    let action_task_started = $scope.ksm.addAction(taskStateName,"task_started",task_started);
                    
                    $scope.ksm.executeAction(taskStateName,"task_started");
                    
                    let task_initiated = function(data)
                    {
                        //textValueState.subject.setStateChanges = data;
                        taskState.subject.notifyObservers("task_initiated");
                    };
                            
                    let action_task_initiated = $scope.ksm.addAction(taskStateName,"task_initiated",task_initiated);
                    
                    $scope.$on('RefreshTask'+$scope.localVars.inlineTaskDefinition.transaction_id,function(event,data){

                          $scope.localVars.StartNewTask();
                    });
                    
                    $timeout(function(){
                        $scope.localVars.initiateFormLoaded=true;
                        $scope.localVars.taskInitiated=false;
                        $scope.localVars.isCollapsed3=true;
                    },500);      
        
        
        
            }

        };
        $scope.localVars.ProcessMenuClick= function(menuId){
            var lang=$rootScope.globals.currentUser.lang;
          
          
           if (menuId!=0)
           {
              $scope.localVars.initiateFormLoaded=false;
              MenuService.ProcessMenuClick(menuId).then(function(data) {
                                
                                $scope.ProcessDataFromStartTask(data);                            
                                 
     
                              },function(error){
                                
                                  if (error.msg!=undefined) MessagingService.addMessage(error.msg,'error');
                                  else MessagingService.addMessage(error,'error');
                                  $scope.localVars.inlineTask.taskProcResponse = {status : error.status,msg:error.msg};
                                  
                            });
           }
        };
       
       $scope.localVars.GetExecuteTaskByExeId= function(task_execute_id,task_initiate_id){
            var lang=$rootScope.globals.currentUser.lang;
          
          
           if (task_execute_id>0)
           {
              
              TaskDataService.preExecuteTask(task_initiate_id,task_execute_id,lang).then(function(data) {
                                
                                $scope.ProcessDataFromStartTask(data.data);                            
                                 
     
                              },function(error){
                                
                                  if (error.msg!=undefined) MessagingService.addMessage(error.msg,'error');
                                  else MessagingService.addMessage(error,'error');
                                  $scope.localVars.inlineTask.taskProcResponse = {status : error.status,msg:error.msg};
                                  
                            });
           }
        };
         
       if ($scope.localVars.processMenuClicked) $scope.localVars.ProcessMenuClick($scope.localVars.menuId);
       
       
         $scope.localVars.StartTask = function(taskId,outcomeId,previousTaskId){
                var lang=$rootScope.globals.currentUser.lang;
                if (outcomeId==undefined || outcomeId=="") outcomeId=0;
                if (previousTaskId==undefined || previousTaskId=="") previousTaskId=0;
               
                if (taskId!=0)
                {
                    $scope.localVars.initiateFormLoaded=false;
                    
                    TaskDataService.StartTask(taskId,outcomeId,previousTaskId).then(function(data) {
                                
                                  
                                  if (Object.keys(data.data).length==0 || data.data==null || data.data==undefined)
                                  {
                                    MessagingService.addMessage('Task processing error. Task data is empty! --> Check task definition --> Task id = '+taskId,'error');
                                  }
                                  else
                                  {
                                    
                                    $scope.ProcessDataFromStartTask(data.data);
                                                                     }
                              },function(error){
                                
                            
                                  MessagingService.addMessage(error.msg.msg,'error');
                                  $scope.localVars.inlineTask.taskProcResponse = {status : error.status,msg:error.msg};
                                  
                            });
                }
          };
        
         $scope.localVars.StartPublicTask = function(organisationId,taskName,taskId,outcomeId,previousTaskId){
                if (organisationId==undefined || organisationId=="") organisationId=0;
                if (outcomeId==undefined || outcomeId=="") outcomeId=0;
                if (previousTaskId==undefined || previousTaskId=="") previousTaskId=0;
                if (taskId==undefined || taskId=="") taskId=0;
                if (taskName==undefined) taskName="";
                if ((taskId!=0 || taskName!="") && organisationId>0)
                {
                  PublicTaskDataService.StartTask(organisationId,taskName,taskId,outcomeId,previousTaskId).then(function(data) {
                                
                                   if (Object.keys(data.data).length==0 || data.data==null || data.data==undefined)
                                  {
                                    MessagingService.addMessage('Task processing error. Task data is empty! --> Check task definition --> Task id = '+taskId,'error');
                                  }
                                  else
                                  {
                                    
                                    $scope.ProcessDataFromStartTask(data.data);
                                  }

                                /*
                                 MessagingService.addMessage(error.msg,'error');
                                  $scope.localVars.inlineTask.taskProcResponse = {status : error.status,msg:error.msg};

                                  $scope.localVars.inlineTaskDefinition = data.data.taskdef[0];
                                  if ($scope.taskDefinition!=undefined) $scope.localVars.inlineTaskDefinition.prev_task_id = $scope.taskDefinition.task_execute_id;
                                  $scope.localVars.inlineTaskDefinition.proc_type = data.data.procType;
                                  $scope.localVars.inlineTaskDefinition.form_type = data.data.formType;
                                  $scope.localVars.inlineTaskDefinition.transaction_id = data.data.transactionId;
                                  $scope.localVars.inlineTaskDefinition.inline_task=$scope.localVars.isInlineTask;
                                  $scope.localVars.inlineTaskDefinition.source_type = $stateParams.src_type;
                                  $scope.localVars.inlineTaskDefinition.taskFormLoaded = false;
                                  $scope.localVars.inlineTaskDefinition.taskExecuteInstance = data.data.taskExecuteInstance;
                                  $scope.localVars.inlineTaskDefinition.taskInitiateInstance = data.data.taskInitiateInstance;
                                  $scope.localVars.inlineTaskDefinition.inlineInitiateFormLoaded = true;  
                                  
                                  if ($scope.localVars.inlineTaskDefinition.taskExecuteInstance!=undefined && $scope.localVars.inlineTaskDefinition.taskExecuteInstance.length>0) $scope.localVars.inlineTaskDefinition.task_execute_id = $scope.localVars.inlineTaskDefinition.taskExecuteInstance[0].id;
                                  if ($scope.localVars.inlineTaskDefinition.taskInitiateInstance!=undefined && $scope.localVars.inlineTaskDefinition.taskInitiateInstance.length>0) $scope.localVars.inlineTaskDefinition.task_initiate_id = $scope.localVars.inlineTaskDefinition.taskInitiateInstance[0].id;
                                  
                                  $scope.localVars.inlineTask.resourceDefinition=data.data.resourceDefinition;
                                  $scope.localVars.inlineTask.data = data.data.form_data;
                                  if ($scope.localVars.inlineTask.data==undefined) $scope.localVars.inlineTask.data = {};
                                  $scope.localVars.inlineTask.widgets.layout= data.data.widgets;
            
                                  $scope.localVars.inlineTask.resourceDefinition.formParams = {formViewType:2,formLocked : true};
                                  $scope.localVars.taskInitiated = false;                 
                                  
                                    $timeout(function(){
                                      $scope.localVars.initiateFormLoaded=true;
                                      $scope.localVars.isCollapsed3=true;
                                    },1);   

                                */

                                },function(error){
                            
                                  MessagingService.addMessage(error.msg,'error');
                                  $scope.localVars.inlineTask.taskProcResponse = {status : error.status,msg:error.msg};
                                  
                            });
                }
          };
        
          
        if ($scope.localVars.loadTaskDirect) $scope.localVars.StartTask($scope.localVars.taskId,$scope.localVars.outcomeId,$scope.localVars.previousTask);
          
        if ($scope.localVars.processStartTask || $scope.localVars.autoSearch) $scope.localVars.StartTask($scope.localVars.menuId,$scope.localVars.outcomeId,$scope.localVars.previousTask);
        
        if ($scope.localVars.publicTask) $scope.localVars.StartPublicTask($scope.localVars.organisationId,$scope.localVars.publicTaskName,$scope.localVars.publicTaskId,$scope.localVars.outcomeId,$scope.localVars.previousTask);
              
    },
            
             link: function (scope, elem, attrs) {
       
       
       
      
        }
      }
};