'use strict';

export function ktGrid (GridsterService,$timeout,$rootScope,dragulaService) {
     //  console.log("u direktivi kt-textbox ");
        return {
            restrict: 'A',
          
			scope:{
                   allColumns: "=",
                 /*  inputArray: "=",
                   active: '=',*/
                   parent:'=',
                   gridType: '=',
                   formParams:'=',
                   formParamsParent:'=',
                   showSearchFilters:'=',
                   resourceDefinition: '=',
                   parentResourceDefId: '=',
                   resourceRecordId: '=',
                   resourceFormLoaded : '=',
                   formData : '=',  
                   hideProcessingAnimation : '=?',            
      			   
            },
            controller: ['$scope','dragulaService','$rootScope','$element','$compile','$templateCache',
          
            function($scope,dragulaService,$rootScope,$element,$compile,$templateCache){
            
                $scope.gsrvc = GridsterService;
                $scope.CTRL="ktGrid"
                var widgetHandle = $scope.gridType+'-row-drag-handle';
            
                var bag = dragulaService.find($scope, 'bag-rows-'+$scope.gridType+'-'+$scope.$id);

                if (bag) {
                     dragulaService.destroy($scope, 'bag-rows-'+$scope.gridType+'-'+$scope.$id);   
                }    
                dragulaService.options($scope, 'bag-rows-'+$scope.gridType+'-'+$scope.$id, {
                  revertOnSpill: true,
                  removeOnSpill: false,
                  
                  //direction:'vertical',
                  accept:function (el,target, source) {
                      return false;
                  },
                  moves: function (el, container, handle) {
                            return handle.classList.contains(widgetHandle);
                  }
                });
                
                
                let template;
                if ($scope.resourceDefinition===undefined) $scope.resourceDefinition={form_definition:false}
                if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktGrid.html');
                else template = $templateCache.get('ktdirectivetemplates/ktGridExe.html');
          
                $element.append($compile(template)($scope));                
                
            
            }],
            link: function (scope, elem, attrs) {

            
                scope.ShowFormView = function(viewId)
                {
                               
                    scope.formParams.formViewType=viewId;
                    scope.gsrvc.setFormViewType(scope.formParams.formViewType);
                   
                };
      
                scope.getClass = function(size)
                    {
                    
                        return "col-lg-"+size;
                    
                    };
          
                
            
                scope.$watch('resourceFormLoaded',function(newValue,oldValue){
                    
                    if (newValue!=oldValue || newValue)
                    {
                     scope.formLoaded = scope.resourceFormLoaded;
                    }
                });
                
                scope.$watch('formParams.showGridNavigation',function(newValue,oldValue){
                    
                    if (newValue!=oldValue || newValue )
                    {
                        if (scope.formParams!==undefined)  scope.showGridNavigation = scope.formParams.showGridNavigation;

                    }
                });
            
            /*
                scope.$on('LastRepeaterElement',function(event, data){
        
                    $timeout(function(){
              
                        scope.formLoaded=true;
                      
                    },750); 
      
      
                });
            */
      
               
                
             } 
        }
    };