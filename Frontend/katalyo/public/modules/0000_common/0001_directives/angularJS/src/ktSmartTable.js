'use strict';

export function ktSmartTable ($timeout, ResourcesDataService,MessagingService) {
        return {
            restrict: 'EA',
             controller: function ($scope, $rootScope, $filter, $timeout, ResourcesDataService,MessagingService) {
                
                    $scope.displayTableData = [];
                    $scope.priority= { min:1, max:3 }
                    $scope.isSortChanged = true
                    $scope.Math = window.Math;
                    $scope.local= { stItemsOnPage:$scope.itemsOnPage}
                    $scope.showTableConfigMenu=false
                    $scope.pageItemsList = [5,10,25,50,100,250,500,1000];
                    $scope.myFormat={}
                    $scope.langCode = $rootScope.globals.currentUser.langObj.Code
                    $scope.leftCss="left: 0px;"              
                    if ($scope.fontSize==undefined ) $scope.fontSize = "1rem"                                
                    $scope.CTRL='ktSmartTable'
                    if ($scope.clientSide==undefined) $scope.clientSide=false;
                    if ($scope.usePagination==undefined) $scope.usePagination=false;
                    if ($scope.tableData!=undefined) $scope.displayTableData = $scope.tableData;
                    if ($scope.parameters===undefined) $scope.parameters={}
                    if ($scope.parameters.PagerLabel1===undefined) $scope.parameters.PagerLabel1 ='items per page | Page:'
                    if ($scope.parameters.PagerLabel2===undefined) $scope.parameters.PagerLabel2 ='of'
                    if ($scope.parameters.PagerLabel3===undefined) $scope.parameters.PagerLabel3 ='items'
                    
                    
                    Array.prototype.sortBy = function (propertyName, sortDirection) { // should be used instead GETSORTMEHTOD !!
                        // sortDirection :  true=ascending, false=descending
                        var sortArguments = arguments;
                        this.sort(function (objA, objB) {

                            var result = 0;
                            for (var argIndex = 0; argIndex < sortArguments.length && result === 0; argIndex += 2) {

                                var propertyName = sortArguments[argIndex];
                                result = (objA[propertyName] < objB[propertyName]) ? -1 : (objA[propertyName] > objB[propertyName]) ? 1 : 0;

                                //Reverse if sort order is false (DESC)
                                result *= !sortArguments[argIndex + 1] ? 1 : -1;
                            }
                            return result;
                        });

                    }
                    
                    
                    $scope.getSortMethod = function(){     //usage: array.sort( getSortMethod('-price', '+priority', '+name'));
                       
                            var _args = arguments[0];
                            return function(a, b){
                                for(var x of _args){
                                     try {    
                                     
                                        var ax,bx, cx ;
                                        ax = a[x.substring(1)]      
                                        bx = b[x.substring(1)];

                                        if (ax==null || ax == undefined) {
                                            return 1
                                        }
                                        
                                        if (bx==null || bx == undefined) {
                                            return -1
                                        }
                                        
                                        if (ax == bx) continue          
                                       
                                    }   catch (error) {
                                         console.error(error);
                                         //debugger
                                    }
                                    
                                   if(x.substring(0,1) == "d") {cx = ax; ax = bx; bx = cx;} //switch when 'descending' sort
                                          
                                  if (typeof ax == "string") {
                                       let res= ax.localeCompare(bx, $scope.langCode || 'en-GB', {sensitivity: 'base'})
                                       return res     
                                   }                                       
                                   else return  ax < bx ? -1 : 1;
  
                                }
                            } 

                       
                     }

                        
                    $scope.sortFunction = {
                        'ascending': function (a,b) {
                                    if (a > b)    
                                        return 1
                                    else return -1
                        },
                        'descending': function (a,b)  { 
                                    if (a < b)    
                                            return 1
                                     else return -1
                                 }
                    }
                    
                    
                    $scope.sortGridData = function ()
                    {
                         $scope.isSortChanged = false                     
                                let sortArguments = []
                                var allTableData = $scope.gridData()
                                
                                if (allTableData!=undefined && allTableData.length >1 ) {
                                        for (const col of  $scope.columnDefs) {     
                                            if (col.sort =='ascending' || col.sort =='descending') {
                                                sortArguments.push(col.sort[0] + col.name)        
                                            }        
                                        }
                                    
                                        allTableData.sort ($scope.getSortMethod(sortArguments));      
                                }
                      
                    }
                    
                    
                     const numFormat = new Intl.NumberFormat("de-DE", {
                          maximumFractionDigits: 2 ,
                          minimumFractionDigits: 2
                    });
                    
                    
    
    
                    $scope.getItem = function (column,item)
                    {    
                        let val;
                        if (item.resourcedeflang_set) {
                             val = get(item,column.name, null)        // For RESOURCE LIST table
                        }
                        else {
                            val = item[column.name]      //for user data
                        }
                        
                        if (val===null || val===undefined) return "∅";          
                        //debugger //smarttable //getItem
                        
                        if (column.cellFilter) { //DATE FORMAT
                            if (column.cellFilter.substring(0,4).trim()=="date") 
                                    val = $filter(column.cellFilter.substring(0,4))(val,column.cellFilter.substring(6,column.cellFilter.length).trim());
                        }
  
                        // number.toString().replace(/\./g, ',') # replace dot with comma
                        if (column.ElementType == "number")   val =  $scope.myFormat[column.id].format(val)
        
                        return val;
                    };
                                    
                    
                    $scope.rowSelected = function(row) 
                    { 
                         $scope.rowSelectedCount = row.isSelected;
                         $scope.leftCss="left: "+$scope.cursorPosition+"px;"
                         $scope.rowSelectedChange({item:row}); 
                    };
                      
                    $scope.setLeftPosition = function(event,row) 
                    { 
                        $scope.cursorPosition = event.clientX;
                    };     
                    
                   $scope.copycolumnDefs = function() 
                    {
                        //debugger //copycolumnDefs
                        for (let item of $scope.columnDefs) {
                                 if (item.sort == "true" || item.sort == "reverse")  {
                                          $timeout(function() {
                                             if ($scope.sortColumnDefs)  $scope.sortColumnDefs.push(item)  })
                                  }
                        }   
                    }
                    
                    
                    $scope.dropSuccess = function() { // NOT USED - DEMONSTRATION OF "ACTION ON DROP"
                        for (let src of $scope.sortColumnDefs) {
                             for (let dst of $scope.columnDefs) {
                                    if (src == dst)  {
                                       //debugger
                                    }
                             }   
                        }
                    }
                     
                    
                    $scope.sortFilter = function (item) { 
                        return item.sort === 'true' || item.sort === 'reverse'; 
                    };
                    
                    
                    $scope.toggleTableConfigMenu = function()
                    {
                       $scope.showTableConfigMenu = !$scope.showTableConfigMenu
                    }
                    
                    $scope.toggleSortMenu = function()
                    {
                       $scope.showSortMenu = !$scope.showSortMenu
                    }
                    
                    
                    $scope.selectPage = function (page) { 
                        $scope.$broadcast ('selectPage', page);//send to smart-table controller
                    };
        
                      
                    $scope.printTable = function()
                    {                  
                        let columns=[]
                        var output="<div class='container'>"
                        output = output+ "<div id='printb' class='noprint' style='padding:10px'> <button  onClick=' var e = document.getElementById(&quot;printb&quot;); e.remove(); window.print()'>Print</button></div>" 
                        var data= $scope.gridData()
                       

                       // PRINT HEADER
                        output = output+ "<div class='red'>" 
                        var columnStyles=''
                        for (let i=0; i< $scope.columnDefs.length;i++) {
                              let col = $scope.columnDefs[i]
                              if (col.visible == true) {
                                  let colClass="cell col" + i 
                                  if  ( col.ElementType == "number" ) 
                                       output = output + "<div class=' " + colClass + " ar' >" + col.displayName + "</div>";
                                  else 
                                      output = output + "<div class=' " + colClass +  " ' >" + col.displayName + "</div>";
                                  columnStyles+= ' .col' + i + '  {  margin-left: 4px ; width:' + col.width + ' }' 
                              }
                        }
                        output = output+ "</div>" 

                        // PRINT DATA
                        for(let row of data) {
                             
                            output = output+ "<div class='red'>"
                            for (let i=0; i< $scope.columnDefs.length; i++)  {
                                 let col = $scope.columnDefs[i]
                                 if (col.visible == true) {
                                    let colClass="cell col" + i
                                    if  ( col.ElementType == "number" ) {
                                        let formatted = row[col.name].toLocaleString(  $scope.langCode || 'en-GB', { maximumFractionDigits: 2, minimumFractionDigits: 2 });
                                        output=output + '<div class="' + colClass + ' ar">'+ formatted +  '</div>'
                                    }
                                    else {
                                        output=output + '<div class="' + colClass+ '">'+ row[col.name] +  '</div>'
                                    }
                                 }
                            }     
                            output = output+ "</div>"        
                        }
                        
                        /*
                        var doc = document.implementation.createHTMLDocument('My title');  
                        doc.head.innerHTML+='  <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/master.css" /> '                   
                        doc.head.innerHTML+=' <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/main.css" />'
                        doc.body.innerHTML+='<div id="content">TEST</div>'
                        */
                        
                        var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
                        var win = window.open("", "_blank", strWindowFeatures);       
                        
                        win.document.body.innerHTML+=
                       // ' <style type="text/css" media="print">   @page { size: landscape;  }    body {   writing-mode: tb-rl; } </style>'  
                      //  ' <style type="text/css" >   @page { size: landscape;  }    body {  } </style>'  
                        ' <style type="text/css">'
                        +' @page {  size: landscape;  margin: 0;}  html, body {   width: 297mm;   height: 210mm;  }'
                       // +' @page {  size: A4;  margin: 0;}  html, body {   width: 2000px;   height: 1000px;  }'
                        +' .red {  font-weight: bold; max-width: 100%;  font-size: 10px; display: table-row; } '
                        +' .cell  {  width: 100px; float: left;   display: table-column; }'
                        +' .ar {  text-align: right  }'
                        +' .container { display: table;  width: 100%; } '
                        +  columnStyles
                        +' </style> '
                       
                        win.document.body.innerHTML += output;

                    }
             
            $scope.export2excel = function() {
              
                var wb = new ExcelJS.Workbook();
                       
                ResourcesDataService.downloadFile($scope.parameters.excelExportTemplateFileId , $scope.parameters.excelExportTemplateFileType.id)
                .then(function(data) {
                            MessagingService.addMessage('File downloaded','info');
                            $scope.template = data.data;
                        },function(error){    
                            MessagingService.addMessage(error.msg,'error');  
                      })
                .then( function(data) {               
                    wb.xlsx.load($scope.template).
                        then(workbook => {
                              workbook.eachSheet((sheet, id) => {
                                   if ($scope.$parent.formData) {
                                        let party_name = $scope.$parent.formData[Object.keys($scope.$parent.formData)[0]][0]["party_name"]
                                        let contract_number = $scope.$parent.formData[Object.keys($scope.$parent.formData)[0]][0]["contract_number"]
                                         sheet.getCell('A2').value = party_name;
                                         sheet.getCell('B3').value = contract_number;     
                                   }
                         
                                  let temp=[]
                                  let header=[]
     
                                  $scope.columnDefs.forEach(  (col, i) =>     
                                  {
                                      if (col.visible) {
                                          header.push(col.displayName)
                                          let colWidth = col.width.replace("%","").replace("px","") // for Excel we dont want % or px
                                          temp.push({header: col.displayName, key:col.name, width: colWidth } ) 
                                      }                                          
                                  });
                                  
                                sheet.columns = temp
                               
                                /*sheet.insertRow(6, header,'i')  TODO - make header dynamicaly created
                                sheet.spliceRows(5, 1); */
                                var data = $scope.gridData()
                                sheet.insertRows(7, data,'i')                          
                                sheet.spliceRows(6, 1);
                                //sheet.spliceRows(1, 1);
                                sheet.getRow(1).hidden = true   
                                                             
                                return workbook                                
                              })
                       
                
                          workbook.xlsx.writeBuffer().then((data) => {
                                  const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
                                  saveAs(blob, 'export.xlsx');
                                }); 
                            
                        })
                    })
            }
             
             
  
                   $scope.exportTable = function()
                    {   
                        const workbook = new ExcelJS.Workbook();
                        const worksheet = workbook.addWorksheet('New Sheet');          
                        let temp=[]
                        let isDate={}
                        let isList={}
                        let isColumnVisible=[]
                        
                        for (let col of $scope.columnDefs) {
                                if (col.visible) {
                                    isColumnVisible.push(col.name)
                                    temp.push({header: col.displayName, key:col.name})
                                    if (col.ElementType== 'list')  isList[col.name] = true 
                                    if (col.ElementType== 'date')  isDate[col.name] = true     
                                }
                        }
                        
                        worksheet.columns = temp
                        //worksheet.columns = $scope.columnDefs.map(col => {header: col.displayName, key:col.name}); //TODO mozda ovo ubaciti umjesto gore

                        var allTableData=$scope.gridData()
                        var allExportData = []
                        // creating  data for export
                        for (let row of allTableData) {
                            let obj = {}
                            for (let col of isColumnVisible) {                          
                                 obj[col] = row[col]
                                 if (row[col] && isList[col])   {  //when list
                                         obj[col] = row[col]
                                     }  
                                 if (row[col] && isDate[col])   {  //when date
                                          obj[col] = new Date(row[col])
                                     }   
                              }
                            allExportData.push(obj)
                        }
                          
                        worksheet.addRows(allExportData)
                        
                        workbook.xlsx.writeBuffer().then((data) => {
                          const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
                          saveAs(blob, 'export.xlsx');
                        });  
                    }
                    
                    
                    $scope.refreshDataExt = function(pageNumber,pageSize)  
                    { 
                          $scope.tableState.pagination.number = pageSize;
                          $scope.tableState.pagination.start = (pageNumber-1)*pageSize;
                          $scope.refreshData($scope.tableState);
                    }
                    
                    
                    $scope.flipSort = function(tc) {
                        if (tc.sort == undefined || tc.sort=="")
                            tc.sort="ascending"
                        else  if (tc.sort=="ascending") 
                            tc.sort="descending" 
                        else if (tc.sort=="descending") 
                            tc.sort=""
                        
                        $scope.isSortChanged = true

            
                    }
                    
                    // REFRESH FUNCTION - CALLED FROM SMART-TABLE !!
                    $scope.refreshData = function(tableState) 
                    {  
                       if (tableState.pagination.number==undefined || tableState.pagination.number==NaN)   
                            tableState.pagination.number=10;
                        
                        //debugger
                        if ( $scope.isSortChanged)  {
                            $scope.isSortChanged = false
                            $scope.sortGridData()     
                        }
                       
                       //actually function GETDATASMARTTABLE in ktTaskDatasetList widget 
                       $scope.ktRefreshData({tableState: tableState, pageNumber:(tableState.pagination.start/tableState.pagination.number)+1, pageSize:tableState.pagination.number})
                            .then(function (result) {
                                //debugger //smarttable //get formated displayTabeData 
        
                                $scope.displayTableData = result.data.results;
                                $scope.columnDefs = result.data.columns;
                                $scope.totalMatched = result.data.count;

                                
                                for (const col of $scope.columnDefs) {
                                    var newFormat;
                                    let useGrouping = col.disableGrouping ? false : true
                                    
                                    if (col.decimalPlaces)     
                                            newFormat = new Intl.NumberFormat("de-DE", { useGrouping: useGrouping,  maximumFractionDigits: col.decimalPlaces ,  minimumFractionDigits: col.decimalPlaces  });
                                     else 
                                            newFormat = new Intl.NumberFormat("de-DE", { useGrouping: useGrouping });
                                    $scope.myFormat[col.id] = newFormat
                               }
                                
                                 
                                 
                                
                                if (result.data.results!=undefined) {
                                       for (let item of result.data.results){  
                                         item.getItem= {}
                                         for (let col of result.data.columns) {
                                                item.getItem[col.name] = $scope.getItem(col ,item)
                                            }                                         
                                    }   
                                }
                                
                                
                                if (tableState!=undefined)  {
                                    tableState.pagination.numberOfPages = result.data.numberOfPages;
                                    tableState.pagination.totalItemCount = result.data.count;
                                }
                                $scope.tableState = tableState;
                                
                             
                            });  
                    };
                    
                    
                     $scope.getActionLabel = function(index) 
                    {  
                        return $scope.actionDefinition[index].Label
                    };

                  
                    let stringToPath = function (path) {
                        // If the path isn't a string, return it
                        if (typeof path !== 'string') return path;
                        var output = []; 
                        path.split('.').forEach(function (item, index) {
                            // Split to an array with bracket notation
                            item.split(/\[([^}]+)\]/g).forEach(function (key) {
                                if (key.length > 0)    output.push(key);// Push to the new array
                            });
                        });
                        return output;
                    };
                        
                    
                  
                    let get = function (obj, path, def) {
                       
                          
                        path = stringToPath(path);// Get the path as an array
                        var current = obj;// Cache the current object
                        // For each item in the path, dig into the object
                        if (path!==null && path!==undefined)
                        {
                            for (var i = 0; i < path.length; i++) {
                            // If the item isn't found, return the default (or null)
                            if ( current[path[i]] === undefined || current[path[i]] === null ) return def;
                            current = current[path[i]]; // Otherwise, update the current  value
                            }
                        } else return def
                        return current;
                    };
         
       
        $scope.$watch('columnDefs', function (newValue) { //SORTIRANJE KOLONA - NE ZNAM DA LI UOPČE OVO IMA IKAKVOG SMISLA..MISLIM DA NE - TODO OBRISATI

            if ($scope.sortColumnDefs == undefined) $scope.sortColumnDefs =  []      
            if ($scope.columnDefs == undefined) $scope.columnDefs =  []  
             
            for (let item of $scope.columnDefs) {
                  if (item.sort == "ascending" || item.sort == "descending") {
                       //if new column already exists in sort array do nothing otherwise push into new array
                       if ( ! $scope.sortColumnDefs.includes(item) )   
                                        $scope.sortColumnDefs.push(item)   
                                
                  }                      
            } 

           //  //sort kolona 
           // $scope.sortColumnDefs.sort(  (a,b) => (a.sortOrder > b.sortOrder) ? 1 : ((b.sortOrder > a.sortOrder) ? -1 : 0))                               
            
        },true);
        
      /* 
        $scope.$watch('sortColumnDefs', function (newValue,oldValue) {
            
            for (let i=0; i < $scope.sortColumnDefs.length;i++) {
                       if ($scope.sortColumnDefs[i].sort == "")  {
                            $scope.sortColumnDefs.splice(i, 1);
                       } else    
                           $scope.sortColumnDefs[i]['sortOrder'] = i
         
            }
                
        }, true);*/
  

        
                },  //CTRL
               scope:{
                    tableData:'=?',
                    parameters:'=?',
                    fontSize:'=?',
                    cellPadding:'=?',
                    columnDefs:'=?',
                    usePagination:'=?',
                    clientSide:'=?',
                    itemsOnPage:'=',
                    allowSort:'=',
                    rowSelectedChange:'&',
                    ktRefreshData:'&ktRefreshData',
                    gridData:'&gridData',
                    setFnRefreshData: '&',
                    showActionButtons:'=?',
                    processAction:'&ktProcessAction',
                    initialPage: '=?',
                    actionDefinition: '=?',
            },
            templateUrl: 'ktdirectivetemplates/ktSmartTable.html',
            link: function (scope, elem, attrs) {
                 
                scope.setFnRefreshData({theDirFn: scope.refreshDataExt});
                if (scope.actionDefinition===undefined) scope.actionDefinition=[{actionId:1,Label:'Edit'}]
                 
             }
    }
             
      
};