'use strict';

export function ktOutcomeActions () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktOutcomeActions.html',
              controller: function($scope,dragularService){
                
               $scope.getSeqNoForNewAction = function(){
                  var seqNo=0;
                  for (var i=0;i<$scope.outcomes.length;i++) {
                     
                     seqNo = seqNo+$scope.outcomes[i].actions.length;
                     
                  }
                  
                  seqNo++;        
                  return seqNo;
      
                  }
                  
              $scope.actions = [
                {id1:1,type:1,name1:'Start task',taskId:null,actionParams:{}},
                {id1:2,type:2,name1:'Send notification',taskId:null,actionParams:{}}
                
              ];
                      
                      
                  $scope.setupDragular = function(){
                            
                                      setTimeout(function(){
                                        // var container = elem.children().eq(0).children().eq(0).children().eq(0).children().eq(2).children().eq(0).children().eq(0).children().eq(1).children().eq(0).children();
                                         var container = document.getElementsByClassName('outcomeActionsDragular');
                                        // var widgets =[$$scope.pTaskInitiateFormResources,$$scope.pTaskExecuteFormResources,$$scope.initiateFormResources,$$scope.selectedResource,$$scope.executeFormResources];
                                                     
                                         dragularService(container,
                                           {
                                          // nameSpace: namespace ,
                                           copy: true,
                                           classes: {
                                           //mirror: 'gu-mirror-empty'
                                               },
                                           revertOnSpill: true,
                                           containersModel: $scope.actions,
                                           //direction:'vertical',
                                         // accepts: accepts,
                                           
                                            moves: function (el, container, handle) {
                                               return handle.classList.contains("element-drag-handle");
                                             }
                                         
                                            }
                                            );
                                          }
                                        );          
                                  
                            }
                 
                 $scope.setupDragular();
                },
             link: function (scope, elem, attrs) {
              
             
              
              /*          
              scope.outcomeDefActionsOptions =
              
              {
                itemMoved: function (event) {
                    event.dest.sortableScope.modelValue[event.dest.index].id = scope.getSeqNoForNewAction();
                },
                clone:true,
              }
              */
             
             
               
        }
    }
};