'use strict';

export function ktList ($rootScope,CodesService,GridsterService,KatalyoStateManager,$timeout) {

        return {
            restrict: 'A',
            controller: function ($scope,CodesService,$rootScope,MessagingService,GridsterService,KatalyoStateManager,$compile,$element,$templateCache,$timeout){
                $scope.CTRL='ktList'
                $scope.fd.localVars = {}
                $scope[$scope.fd.id]  = {'fd': $scope.fd}
                 
                $scope.fd.colLength=[];
                for (let i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));
        
                let template;
                if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktList.html');
                else template = $templateCache.get('ktdirectivetemplates/ktListExe.html');
          
                $element.append($compile(template)($scope)); 
                 },
            
            link: function (scope, elem, attrs) {

                if (scope.fd.Parameters===undefined) scope.fd.Parameters = {}
                var lang = $rootScope.globals.currentUser.lang; 

                if (scope.fd.Parameters.ValueFontColor===undefined) scope.fd.Parameters.ValueFontColor = '#000'
                scope.gsrvc = GridsterService;
                scope.codesService = CodesService;
                scope.fd.codesLoaded = false
                scope.codesLoaded = false
                 let field_name = scope.fd.FieldName;
                 let elementObject = {};
                 elementObject[field_name]=null;
                 if (scope.formData==undefined)
                 {
                    scope.formData=[];
                    scope.formData.push(elementObject);
                 }
                  if (scope.formData.length == 0) scope.formData.push({});
                 // else if (scope.formData[0][field_name]==undefined) scope.formData[0][field_name] =null;
                scope.itemValue = {val:scope.formData[0][field_name]};
                
                scope.ksm = KatalyoStateManager;
                if (scope.fd.Parameters==undefined) scope.fd.Parameters={};
               
                
                let listValueState= scope.ksm.addSubject('listValueStateName'+scope.fd.ItemId,scope.itemValue.val);
                let listValueStateName = listValueState.subject.name;
                
                let value_changed = function(data)
                {
                    listValueState.subject.setStateChanges = data;
                    listValueState.subject.notifyObservers("valueChanged");
                };
                
                let action_value_changed = scope.ksm.addAction(listValueStateName,"valueChanged",value_changed);
               
                
                let apply_value = function(data)
                {
                    if (scope.formData[0][field_name]!=data) scope.formData[0][field_name] = data;
                }; 
               
                if (scope.fd.Parameters.DependentFieldId!=undefined && scope.fd.Parameters.DependentFieldId!=null)
                
                {
                    if (scope.fd.Parameters.DependentFieldId!="")
                    {
                        let dependent_field = scope.fd.Parameters.DependentFieldId;
                        let subscribedSubject = scope.fd.Parameters.DependentFieldType+"ValueStateName"+scope.fd.Parameters.DependentFieldId;
                        let observer_undo = scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value,true);
                        
                    }
                }
        
                
                         scope.fd.searchList=[
                            {id:1,filter:'onlist',code_name:'onlist',name:"On list",icon:"fa fa-check"},
                            {id:2,filter:'notonlist',code_name:'notonlist',name:"Not on list",icon:"fa fa-times"},
                        ];
                        
                        // get search filter in correct language
                        scope.fd.localVars.processFilters = function(filterList)
                        {
                            for (let i=0;i<scope.fd.searchList.length;i++)
                            {
                                let pos = filterList.map(function(e) { return e.id; }).indexOf(scope.fd.searchList[i].code_name);
                                if (pos>=0) scope.fd.searchList[i].name = filterList[pos].name
                            }
                           
                            
                            if (scope.fd.Search===undefined || scope.fd.Search===null)   scope.fd.Search=scope.fd.searchList[0];//initialize to equals
                            if (scope.fd.Search)  scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 

                            scope.fd.filterReady=true
                        }

                        if (scope.showSearchFilters || scope.resourceDefinition.form_definition)
                        {
                            CodesService.getCodesByName('search_filters',lang).then(function (data) {
                                        //debugger;
                                        try {
                                        //scope.fd.processFilters(data.data)
                                        scope[scope.fd.id]['fd'].localVars.processFilters(data.data)
 
                                        } catch (err) {
                                                debugger
                                        }
                                        
                            });
                        }
                
                scope.setDropdownValue=function(theSelectItemFn)
                {
                    scope.setItemValueDropdown = theSelectItemFn;
                
                }
                
                scope.setClearDropdown = function(directiveFn) {
                        scope.clearDropdown = directiveFn;
                }
                
                
            scope.fd.selection_changed = function () {
                    
                    scope.fd.Search.value = scope.formData[0][field_name];
                    scope.fd.DefaultValue = {value:scope.formData[0][field_name]};
                    scope.ksm.executeAction(listValueStateName,"valueChanged", scope.formData[0][field_name]);
                    if (scope.formData[0][field_name]?.id==null) delete scope.formData[0][field_name]
                }
            scope.removeItem = function(index)
                {
                    scope.formData[0][field_name].splice(index,1);
                    //scope.ngModel.$setViewValue(scope.model.selectedItems); 
                
                }
                
             scope.selectedItemId =  scope.gsrvc.getSelectedItemId(); 
            
            scope.showProperties = function(event)
                {
                  scope.fd.loadingProperties=true
                  scope.gsrvc.setFieldProperties(scope.fd,event);

                  $timeout(function(){scope.fd.loadingProperties=false})
       
                };
                
                
               
                        scope.fd.code={selectedCode:null};
                
                if (scope.resourceDefinition.form_definition)
                {
                        CodesService.CodesList().then(function(data) {
                
                           scope.fd.codesDefinitionList = data.data
                           

                           if (scope.fd.CodeId!=undefined)
                           {
                                var posCodesDef = scope.fd.codesDefinitionList.map(function(e) { return e.id.toString(); }).indexOf(scope.fd.CodeId.toString());
              
                                if (posCodesDef>=0){
             
                                    scope.fd.code.selectedCode=scope.fd.codesDefinitionList[posCodesDef];
                                  
                                }
                           }
                           scope.fd.codeListDownloaded=true

                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
                }
                


                
                scope.applyListFilters = function(index) {
               
                    scope.fd.Search=scope.fd.searchList[index];
               
                    if( typeof scope.formData[0][scope.fd.FieldName] === 'string' ) {
                        scope.fd.Search.value = [ {'id':scope.formData[0][scope.fd.FieldName]} ];
                    }
                
                    scope.isOpen=false;     
                };

                if (scope.fd.Multiline && scope.formData[0][scope.fd.FieldName]===undefined) scope.formData[0][scope.fd.FieldName]=[]

        
       
        scope.fd.getCodes = function (codeId) {
           

            scope.fd.CodeId = codeId;
            let searchCode="";
            scope.codesService.getCodes(codeId,lang).then(function(data){
                
                        scope.fd.codeListFull = data.data;
                        scope.fd.codeList = [];
                        //filter data here
                        if (scope.fd.Parameters.Condition!=undefined)
                        {
                            if (scope.fd.Parameters.Condition!="")
                            {
                                let conditions =  scope.fd.Parameters.Condition.split(";");
                                
                                for (let i=0;i<conditions.length;i++)
                                {
                                   let idx = scope.fd.codeListFull.map(function(e) { return e.value; }).indexOf(conditions[i]);
                                   if (idx>=0) scope.fd.codeList.push(scope.fd.codeListFull[idx]);
                                }
                            } else scope.fd.codeList= scope.fd.codeListFull;
                        }else scope.fd.codeList= scope.fd.codeListFull;
                        
                        
                        if (scope.CTRL !=='ktList') { //angular scope mix debug
                             console.log('ktlist - nije moj scope !  CTRL= ' +  scope.CTRL)
                                   
                        }
                        
                        if (scope.fd.localVars===undefined) {//angular scope mix debug
                             console.log('ktlist - nemam localvars!')
                                   
                        }
                        


                        
                        scope.fd.codesLoaded = true
                        scope.codesLoaded = true
                        //this should come from server however for also on client
                        if (scope.fd.UpdateWithDefault && !scope.fd.NoUpdate) scope.formData[0][scope.fd.FieldName] = scope.fd.DefaultValue?.value

                        if (scope.fd.Parameters.UseUserProperty) scope.formData[0][scope.fd.FieldName] = $rootScope.globals.currentUser.uep.AdvancedProperties[scope.fd.Parameters.UserPropertyId]


                        if (scope.formData!=undefined && !scope.fd.Multiline)
                        {
                            if (scope.formData[0][scope.fd.FieldName]!=null && scope.formData[0][scope.fd.FieldName]!=undefined)
                            {   
                            
                                if (typeof(scope.formData[0][scope.fd.FieldName])=='string' || typeof(scope.formData[0][scope.fd.FieldName])=='number') searchCode = scope.formData[0][scope.fd.FieldName].toString()
                                else searchCode = scope.formData[0][scope.fd.FieldName].id.toString()
                                    
                                    let pos = scope.fd.codeList.map(function(e) {
                                    if (e.id!=null) return e.id.toString();
                                    else return e.id;
                                    }).indexOf(searchCode);
                     
                                        if (pos>=0) {
                                    
                                           $timeout(function(){ 
                                            scope.formData[0][scope.fd.FieldName]=scope.fd.codeList[pos]
                                            
                                            })
                                        
                                        }

    
                            }//else searchCode = scope.formData[0][scope.fd.FieldName].id.toString();
                            
                        }
                
            });
          
        }
        
        if (!scope.fd.ReadOnly && scope.fd.CodeId>0) scope.fd.getCodes(scope.fd.CodeId);
            

      }

    }
};


export function ktListParser () {
        return {
                restrict: 'A',
                require:'ngModel',
       
        link: function (scope, elem, attrs,ngModelController) {


                //For model -> DOM validation (model -> view)
                //
                function formatFormatter(value){

                    let valid = true
                    //if (value===undefined)  value={id:null,name:scope.fd.SearchText}
                    if (scope.fd.Required && !scope.fd.Multiline) {
                        if (value?.id==null)    {

                            valid = false
                            
                        }
                       ngModelController.$setValidity('required', valid);
                    }
                    
                    return valid? value : undefined;
                    //return value

                }
                //For DOM -> model validation (view -> model)
                function formatParser(value){

                        let valid = true
                         if (scope.fd.Required && !scope.fd.Multiline) {
                            if (value?.id==null)    {

                            valid = false
                            
                        }
                        ngModelController.$setValidity('required', valid);
                    }
                         
                         //return value
                         return valid? value : undefined;

                }

                ngModelController.$formatters.push(formatFormatter)

                ngModelController.$parsers.push(formatParser)


        }

   }


}