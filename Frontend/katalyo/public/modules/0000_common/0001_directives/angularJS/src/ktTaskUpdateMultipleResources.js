'use strict';

export function ktTaskUpdateMultipleResources () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService) {
      
    $scope.searchFormDownloaded = false;
    $scope.src_type_text=CODES_CONST._POPULATE_TYPE_;   
     
    $scope.widgetType = 2;
    $scope.animationsEnabled = true;
     var lang=$rootScope.globals.currentUser.lang;
          $scope.gridsterType="resource";
           $scope.gridsterOpts = GridsterService.getGridsterOptions();
          
  
  
            var lang = $rootScope.globals.currentUser.lang;
  
  
  $scope.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: GridsterService.getEmptyDatasetForm()
				} ;
      
      
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        
        $scope.parentDatasetDefId=$scope.fd.related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.localVars.formType=$scope.resourceDefinition.form_type;
        $scope.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        
        //register widget
       var out_fields = ['resourceRecordId']
       var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:''}]},{'name':'parentDatasetLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'updateMultipleResourceWidget',widgetId: $scope.fd.presentationId,taskId:  $scope.lTaskId,
      
       resourceDefId : $scope.fd.related, //resourceDefId : $scope.selectedResource
       resourceRecordId:$scope.resourceRecordId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,prev_task_id:$scope.prevTaskExeId,  output:out_fields };
       
       $scope.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
       
       $scope.fd.localId=$scope.localWidgetId;  
 
      
        
        if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        
        
        
        if ($scope.source_type=="fromOutcome")
        {
           $scope.resourceRecordId = WidgetDataExchangeService.getDataByWidgetId($scope.prevTaskExeId,$scope.resourceDefinition.task_initiate_id,$scope.fd.parentElementId,'resourceRecordId');
        }
         
        if ($scope.resourceRecordId==undefined)
         {
          $scope.resourceRecordId = 0; 
         }
          
         $scope.getFormResourceOriginal = function(){
              
              if ($scope.fd.presentationId==null)
              {
                $scope.fd.presentationId = 0;
                
              }
              
              ResourcesDataService.getResourceForm($scope.fd.related,"i",lang).then(function(data) {
                            
                                $scope.resourceWidgetDefinition= data.data.resourceDefinition;
                                $scope.dashboard.widgetsFilter.layout=angular.copy(data.data.widgets);
                                if ( $scope.taskInitiateId==0 && $scope.taskExecuteId==0)
                                {
                                 $scope.dashboard.widgets.layout=[data.data.widgets];
                                }
                                 $scope.selectedResourceItem = {'name':$scope.resourceWidgetDefinition.name,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                            
                            
                                $scope.fd.fItemValue= $scope.dashboard.widgetsFilter.layout;
                                $scope.fd.wItemValue= $scope.dashboard.widgets.layout;
                                
                                //MIGOR BITNO ZA BROADCAST - ktTaskUpdateResource 
                                //WidgetDataExchangeService.setData($scope.widgetId,'resourceRecordId',{'resourceRecordId':$scope.fd.wItemValue.resourceRecordId});
                                 //ovo je da trenutno radi
                                WidgetDataExchangeService.setData({DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.resourceRecordId}]}],widgetId: $scope.fd.presentationId});
								
                                
                                  $scope.searchFormDownloaded = true;
                                   $timeout(function() {
                                    $scope.formLoaded=true;
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      
      
  
      $scope.SearchFormProcessed=false;
      
      $scope.ProcessSearchForm=function (form){
            
            if (form!=undefined){
     					for(var i=0;i<form.length;i++) {
                
                    if ('type' in form[i])
                    {
                      if (form[i].type == 'resource' && form[i].autoSearch)
                      {
                        var pos = $scope.childWidgets.map(function(e) { return e.widgetId; }).indexOf(form[i].presentationId);
                        if (pos<0) $scope.childWidgets.push({widgetId:form[i].presentationId,processed:false})
                        
                      }
                     }
                      if ('layout' in form[i])
                      {
                           
                       $scope.ProcessSearchForm(form[i].layout);
                      }
              }
          } 
           
      };
            
            
      $scope.GetDatasetData=function(data)
      {

            fromWidget = data;
            var allProcessed=true;
            var pos = $scope.childWidgets.map(function(e) { return e.widgetId; }).indexOf(fromWidget);
            if (pos<0) $scope.childWidgets.push({widgetId:fromWidget,processed:true});
            for (var i=0;i<$scope.childWidgets.length;i++)
              {
                
                if ($scope.childWidgets[i].widgetId == fromWidget) $scope.childWidgets[i].processed = true;
                if (!$scope.childWidgets[i].processed) allProcessed = $scope.childWidgets[i].processed;
              }
             // if (!$scope.SearchFormProcessed) $timeout(function() {$scope.GetDatasetData(fromWidget);},1000);
              if($scope.fd.autoSearch && allProcessed && $scope.SearchFormProcessed) {
                  $scope.selectedResource=$scope.fd.related;
                  $scope.getResourceDataFiltered();
                                      
                }
           
      	
      };
       
       
      $scope.childWidgets = [];
     // $scope.uncaughtWidgets = [];
      var fromWidget=0;
   
       $scope.$on('SearchFormReady'+$scope.taskExecuteId,function(event, data){
      
           $scope.GetDatasetData(data); 
            
      });     
            
            
        $scope.getFormResource = function(){
              var lang=$rootScope.globals.currentUser.lang;
             
              if ($scope.fd.presentationId==null) {
                $scope.fd.presentationId=0;
              }
              ResourcesDataService.getResourceFormExtendedMultiple($scope.fd.related,$scope.taskId,$scope.formType,$scope.fd.presentationId,["w","f"],lang).then(function(data) {
                               
                          $scope.resourceWidgetDefinition= data.data.resourceDefinition;
                          //$scope.dashboard.widgetsTemplate = angular.copy(data.data.widgets[0]);
                          $scope.dashboard.widgetsFilter.layout= data.data.widgets.widgetsf;
                           if ( $scope.taskInitiateId==0 && $scope.taskExecuteId==0)
                                {
                                $scope.dashboard.widgets.layout = [data.data.widgets.widgetsw];
                                }
                          $scope.selectedResourceItem = {'name':$scope.resourceWidgetDefinition.name,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
               
                          $scope.fd.wItemValue=$scope.dashboard.widgets.layout;
                          $scope.fd.fItemValue= $scope.dashboard.widgetsFilter.layout;
                          $scope.searchFormDownloaded = true;
                          
                           $timeout(function() {
                                     $scope.searchFormDownloaded = true;
                                    $scope.setRequired($scope.fd.fItemValue,false);
                                    $scope.formLoaded=true;
                                   //prepare search form
                                    $scope.ProcessSearchForm($scope.fd.fItemValue);
                                    $scope.SearchFormProcessed = true;
                                    //$scope.GetDatasetData(0);                                 
                                   });
                           
                      
                  },function(error){
                  MessagingService.addMessage(error.msg,'error');
                                 
                                 
                                
              });
                             
      };
        
       
        $scope.selectedDatasetRecords=[];
        $scope.getFormResourceWithData = function(){
              var lang=$rootScope.globals.currentUser.lang;
              $scope.updateFormLoaded=false;
              if ($scope.fd.presentationId==null)
              {
                $scope.fd.presentationId = 0;
                
              }
              
              if ($scope.fd.parentElementId==null)
              {
                $scope.fd.parentElementId = 0;
                
              }
               if ($scope.fd.linkedResourceModelDefinitionId==null || $scope.fd.linkedResourceModelDefinitionId==undefined)
              {
                $scope.fd.linkedResourceModelDefinitionId = 0;
                
              }
              
              if ($scope.gridApi!=undefined) $scope.selectedDatasetRecords = $scope.gridApi.selection.getSelectedRows();
              
               if ($scope.selectedDatasetRecords.length>0 || $scope.fd.linkedResourceModelDefinitionId>0)
               {
                $scope.filter = $scope.selectedDatasetRecords;
               var record_id = $scope.selectedDatasetRecords.length;
               
               if ($scope.fd.linkedResourceModelDefinitionId>0) record_id = $scope.fd.linkedResourceId;
               
               if ($scope.fd.linkedResourceDefId==undefined) $scope.fd.linkedResourceDefId=0;
               if (record_id == null || record_id==undefined || record_id=="") record_id=0;
                
              
              ResourcesDataService.getResourceFormExtendedWithDataMultiple($scope.fd.related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.populateType,$scope.fd.parentElementId,$scope.fd.presentationId,$scope.prevTaskExeId,$scope.formType,$scope.widgetType,record_id,"w",$scope.filter,$scope.fd.linkedResourceDefId,$scope.fd.linkedResourceModelDefinitionId,lang).then(function(data) {
                                
                                  $scope.resourceWidgetDefinition= data.data.resourceDefinition;
                                  $scope.dashboard.widgets.layout=data.data.widgets;
                                  
                                 
                                  $scope.resourceRecordIds = data.data.resourceRecordId;
                                  if ($scope.resourceRecordIds.length>0)
                                  {
                                    $scope.resourceRecordId=$scope.resourceRecordId[0];
                                  }
                                  $scope.selectedResourceItem = {'name':$scope.resourceWidgetDefinition.name,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                                
                                  $scope.fd.wItemValue=$scope.dashboard.widgets.layout;//data.data.widgets;
                                //  $scope.fd.fItemValue=data.data.widgetsf;
                                
                                  $scope.searchFormDownloaded = true;
                                //  $timeout(function() {
                                    $scope.formLoaded=true;
                                    $scope.updateFormLoaded=true;
                                     
                                 // });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
               }else
               {
                MessagingService.addMessage("Please select record(s) to update","info");
               }
                              
               
      };
      
     $scope.gridOptionsResourceData = {
        columnDefs: [],
        data:[],
        enableGridMenu: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: true,
        enableHorizontalScrollbar : 1,
        enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: true,
        enableColumnResizing: true,
        enableColumnReordering: true,
      //exporterCsvFilename: 'myFile.csv',
     /*
        paginationPageSizes: [5, 10, 20, 50, 100],
         paginationPageSize: 5,
       enableSelectAll: false,
       enableCellEdit: false*/

   }
      
      
      $scope.gridOptionsResourceData.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
           
          //gridApi.selection.on.rowSelectionChanged($scope,function(row){

         
                  
              
         // });
     }
     
      
 

    
          
       $scope.getResourceDataFiltered = function () {

            $scope.filter = {};
            //$scope.gridOptionsResourceData.columnDefs.length=0;
            //$scope.gridOptionsResourceData.data.length=0;
            $scope.dataDownloading=true;
          if ($scope.selectedResource==null) {
            $scope.selectedResource = $scope.fd.related;
          }
          
         ResourcesDataService.getResourceDataFiltered($scope.selectedResource,$scope.taskId,lang,$scope.dashboard.widgetsFilter.layout).then(function(data) { 
                            
                                  $scope.gridOptionsResourceData.data = data.data;
                                  $scope.gridOptionsResourceData.columnDefs = data.columnsDef;
                                  
                                    $timeout(function(){
                                      $scope.resourceDataDownloaded = true;
                                      $scope.restoreGridState();
                                    });
                                 
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });
              $scope.dataDownloading=false;
        }
    
    
    $scope.saveDatasetRecordsMultiple = function(){
              //var lang=$rootScope.globals.currentUser.lang;
              //$scope.updateFormLoaded=false;
               
              ResourcesDataService.saveDatasetRecordsMultiple($scope.fd.related,$scope.fd.wItemValue).then(function(data) {
                             
                                
                                   MessagingService.addMessage(data.msg,'success');
                                   $scope.dashboard.widgets.layout.length=0;
                                   $scope.getResourceDataFiltered();                               
                                   $timeout(function() {
                                    //$scope.formLoaded=true;
                                    //$scope.updateFormLoaded=true;
                                     
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
              
                              
               
      };
    
    
    $scope.saveGridState= function() {
       var state = $scope.gridApi.saveState.save();
       $scope.fd.gridState = state;
       MessagingService.addMessage("To make changes stick please save form",'info');
                 
    }
  
   $scope.restoreGridState= function() {
      if ($scope.fd.gridState!=null)
      {
         if ($scope.gridApi!=undefined)
         {
      
          $scope.gridApi.saveState.restore($scope,$scope.fd.gridState);
        
         }else
         {
        $timeout(function(){
            $scope.restoreGridState();
          });
         }
      }
    };
    
        //mupdate
          $scope.openWidgetConnector  = function(){
             $scope.fd.showWidgetConnector= !$scope.fd.showWidgetConnector;
             if ($scope.fd.showWidgetConnector) {
                           
                 //POPUNI LISTU DATASETOVA            
                  ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
                                function(data) {
                                                  $scope.datasetList = data.data;
                                                  //dodaj ime dataseta u konektore
                                                  if ($scope.fd.listOfConnectors!=undefined) 
                                                             for (var i=0;i< $scope.fd.listOfConnectors.length;i++) {
                                                                   //var p = $scope.datasetList.map(function(e) { return e.id; }).indexOf($scope.fd.listOfConnectors[i].parentDatasetDefId);
                                                                   var p = $scope.datasetList.map(function(e) { return e.id; }).indexOf($scope.fd.listOfConnectors[i].parentDatasetDefId);
                                                                   if (p>=0) $scope.fd.listOfConnectors[i].parentDatasetName = $scope.datasetList[p].name;  
                                                             }
                                                  
                                                  },function(error){    
                                        MessagingService.addMessage(error.msg,'error');
                                                 });
                                                 
                                                 
 							if ($scope.fd.tItemValue==undefined || $scope.fd.tItemValue[0]==null) $scope.fd.tItemValue = []; 
							// ovo je  Kt-UPLOADER
							  $scope.fd.listOfConnectors= WidgetDataExchangeService.getRegisteredWidgets();//MIGOR  - uzmi sve registrirane widgete
							  
							  //Brisi sve koji se vec nalaze u tItemValue
							   if ($scope.fd.listOfConnectors!=undefined) 
								  for (var i=0;i< $scope.fd.tItemValue.length ;i++) {
										 for (var j=0;j< $scope.fd.listOfConnectors.length;j++) {
                                              if ($scope.fd.tItemValue[i]!=null) {
                                                 if ($scope.fd.listOfConnectors[j].widgetId==$scope.fd.tItemValue[i].widgetId)  {
                                                     $scope.fd.listOfConnectors.splice(j,1);//brisi lokalni				   					    		
                                                 }
                                              }
                                         }
							    }
                                //dodaj sa servera
                                for (var i=0;i< $scope.fd.tItemValue.length ;i++) 
                                              if ($scope.fd.tItemValue[i]!=null) 
                                                  $scope.fd.listOfConnectors.push($scope.fd.tItemValue[i]);		
                                

							  //Ostavi u listi konektora samo WIDGETE koji imaju "ResourceID" jer su to data widgeti
							  var newlist=[];
                              
							    if ($scope.fd.listOfConnectors!=undefined){
                 
                                 for (var i=0;i< $scope.fd.listOfConnectors.length;i++) {
                                                     if ($scope.fd.listOfConnectors[i].DEI!=undefined)  
                                                         for (var j=0;j< $scope.fd.listOfConnectors[i].DEI.length;j++) {
                                                                 //ako DEI nije true/false - resetiraj na false
                                                                 if ( ($scope.fd.listOfConnectors[i].DEI[j].value!=false) && ($scope.fd.listOfConnectors[i].DEI[j].value!=true) ) $scope.fd.listOfConnectors[i].DEI[j].value=false;
                                                                
                                                                  if ($scope.fd.listOfConnectors[i].DEI[j].params!=undefined && $scope.fd.listOfConnectors[i].DEI[j].params!=null)
                                                                  
                                                                  {
                                                                    if ($scope.fd.listOfConnectors[i].DEI[j].params.type=="parent")
                                                                    
                                                                    {
                                                                      var related_id = 0;
                                                                      
                                                                      if ($scope.fd.listOfConnectors[i].DEI[j].params.value!=undefined || $scope.fd.listOfConnectors[i].DEI[j].params.value!=null)
                                                                        related_id = $scope.fd.listOfConnectors[i].DEI[j].params.value;
                                                                      
                                                                      $scope.getFormFields($scope.fd.related,related_id,'resource',i,j);
                                                                     
                                                                      
                                                                    }
                                                                    
                                                                  }
                                                                 
                                                                 //dodaj konektor u novu listu 
                                                                 
                                                             };
                                                             newlist.push($scope.fd.listOfConnectors[i]);
                                               }
                                }//if
							  $scope.fd.listOfConnectors=newlist; //konacna lista NOVIH dataset konektora 
 
			
	
            } //if showfileconnetor  
		}; // open FILECONNECTOR - VIEW
    

            $scope.okWidgetConnector = function () {
                $timeout(function() {
                $scope.fd.tItemValue=$scope.fd.listOfConnectors;
                $scope.fd.showWidgetConnector=false;  
                
                });
            };
    
    $scope.cancelWidgetConnector = function () {
                $timeout(function() {
                
                $scope.fd.showWidgetConnector=false;  
                
                });
            };
    
    
    
      $scope.setField = function(form,name,value)
        {
     				TaskDataService.setField(form,name,value);
        }  
    
       $scope.setReadOnly = function(form,readOnly)
        {
          if (form!=undefined){
     					for(var i=0;i<form.length;i++) {
                
                    if ('readOnly' in form[i])
                    {
                      form[i].readOnly = readOnly;
                     }
                      if ('layout' in form[i])
                      {
                           
                         $scope.setReadOnly(form[i].layout,readOnly);
                      }
              }
          }
        };
        
    $scope.setRequired = function(form,required)
        {
          if (form!=undefined){
     					for(var i=0;i<form.length;i++) {
                
                    if ('required' in form[i])
                    {
                      form[i].required = required;
                     }
                      if ('layout' in form[i])
                      {
                           
                         $scope.setRequired(form[i].layout,required);
                      }
              }
          }
        }
  
  //$scope.getFormResource();
   
  if ($scope.fd.related!=null &&  !$scope.searchFormDownloaded && $scope.taskInitiateId==0 && $scope.taskExecuteId==0 && $scope.prevTaskExeId==0)
  {
     $scope.getFormResource();
    
  }
  
   if ($scope.fd.related!=null &&  !$scope.searchFormDownloaded && ($scope.taskInitiateId!=0 || $scope.taskExecuteId!=0 || $scope.prevTaskExeId!=0))
  {
     $scope.getFormResource();
    
  }
  
  
      }];

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskUpdateMultipleResources.html',
            controller: controller,
            link: function (scope, elem, attrs) {

            scope.resourceData = {};
  
            scope.prepareResourceData = function(resourceRecordId){
           
     
                scope.fd.itemValue =  ResourcesDataService.prepareResourceWidgetData(scope.dashboard.widgets.layout,resourceRecordId);
  
          }
          
          scope.ProcessEvent = function(data)
          {
             for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    scope.fd.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    scope.resourceRecordId = scope.fd.linkedResourceId;
                                   
                                  }
                                  if (data[k].name=='resourceId') scope.fd.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                  
                                  //scope.fd.linkedResourceModelDefinitionId = scope.fd.tItemValue[i].DEI[j].params.selectedItem.id;
                             
                                
                                }
                          
                              //get selectedRecord
                              scope.getFormResourceWithData();
            
          }
           scope.processWidgetDependencies = function() { //-----  ktUploader  -----
              
           if (scope.fd.tItemValue!=undefined) 
            for (var i=0;i< scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<scope.fd.tItemValue[i].DEI.length;j++) {
                        if (scope.fd.tItemValue[i].DEI[j].value==true) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("view "+scope.fd.presentationId+" se veze na serverski " + linkedWidgetId); 
                             scope.fd.linkedResourceModelDefinitionId = scope.fd.tItemValue[i].DEI[j].params.selectedItem.id;
                                          
                             scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                                console.log("view "+ scope.fd.presentationId+" dobio event " + event.name);
                                scope.ProcessEvent(data);
                                    
                              }) //on_END
                               var data = WidgetDataExchangeService.getDataByWidgetId(scope.resourceDefinition.task_execute_id,scope.resourceDefinition.task_initiate_id,scope.resourceDefinition.prev_task_id,linkedWidgetId,linkedItem);
                               if (data!=undefined && data!=null && data!=0)  scope.ProcessEvent(data);
                        }//if
                    }//for j
                }//if

             // else {  scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget
      
          if (scope.fd.presentationId==undefined || scope.fd.presentationId==null) scope.fd.presentationId=0;
          
          scope.formType=scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies(scope.taskId,scope.fd.presentationId,scope.formType).then(function(data) {
                               
                                scope.fd.tItemValue=data.data;
                                scope.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
          
          
          
           //**********************************************************
         //           GET FORM FIELDS
         //**********************************************************      
            
          scope.getFormFields = function(resource_id,related_id,element_type,i,j){
              
              
              var lang=$rootScope.globals.currentUser.lang;
           
                         
              ResourcesDataService.getFormFieldsByType(resource_id,related_id,element_type,lang).then(function(data) {
                               
                                  scope.fd.listOfConnectors[i].DEI[j].params.list= data.data;
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
                              
          };
          
         
          scope.$on('InitiateTaskPostProcess',function(event, data){
              
              //find data
            //  var item = data;
              //send data to widget exchange service
              //WidgetDataExchangeService.setData(item);
              scope.setReadOnly(scope.dashboard.widgets.layout,true);
          });
  
          }
    } 
        
};