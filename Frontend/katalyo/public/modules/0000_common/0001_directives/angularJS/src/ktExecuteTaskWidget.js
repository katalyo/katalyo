'use strict';

export function ktExecuteTaskWidget () {

        return {
            restrict: 'A',
          controller:function($scope,UserService,ResourcesDataService,$rootScope,ProcessingService,$timeout,$stateParams,$cookies,
                              WidgetDataExchangeService,$state,TaskDataService,MessagingService,GridsterService,$templateCache,$compile,$element){
          
           if ($scope.localVars==undefined) $scope.localVars = {};
           $scope.localVars.showMenu = false
      

  
                $scope.localVars.executeTaskDataLoaded=false;
                if ($scope.localVarsExecuteTask==undefined) $scope.localVarsExecuteTask = {};
                var lang=$rootScope.globals.currentUser.lang;
                //$scope.localVarsExecuteTask = angular.copy($scope.localVars);
                if ($scope.localVarsExecuteTask.taskDefinition==undefined) $scope.localVarsExecuteTask.taskDefinition = {};
      	
                //WidgetDataExchangeService.deRegisterAllWidgets();//MIGOR TODO - dodao ovo ali gdje je registerTASK i da li treba ovdje????
                $scope.gsrvc = GridsterService;
                
                $scope.localVarsExecuteTask.fromStateName=$stateParams.fromStateName;
                $scope.localVarsExecuteTask.fromStateParams=$stateParams.fromStateParams;
                $scope.localVarsExecuteTask.cookieFromStateName=$cookies.getObject('executeTaskFromStateName');
                $scope.localVarsExecuteTask.cookieFromStateParams=$cookies.getObject('executeTaskFromStateParams');
                $scope.localVarsExecuteTask.noBackbutton = false;
                $scope.localVarsExecuteTask.backBtnFromCookie=false;
                $scope.localVarsExecuteTask.initiateFormLoaded = false;
                $scope.localVarsExecuteTask.taskInitiateInstance=[];
                $scope.localVarsExecuteTask.taskExecuteInstance=[];
                $scope.localVarsExecuteTask.dashboard = {widgets : [],data:{}};

      
                if ($scope.localVarsExecuteTask.fromStateName==undefined || $scope.localVarsExecuteTask.fromStateName=="")
                {
                    if ($scope.localVarsExecuteTask.cookieFromStateName==undefined || $scope.localVarsExecuteTask.cookieFromStateName=="")
                    {
                      $scope.localVarsExecuteTask.noBackButton = true;
                    }else
                    {
                      $scope.localVarsExecuteTask.fromStateName =  $scope.localVarsExecuteTask.cookieFromStateName;
                      $scope.localVarsExecuteTask.backBtnFromCookie = true;
                    }
                    
                  
                }else{
                  $cookies.putObject('executeTaskFromStateName',$scope.localVarsExecuteTask.fromStateName);
                  
                }
      
                if ($scope.localVarsExecuteTask.fromStateParams==undefined)
                {
                    if ($scope.localVarsExecuteTask.cookieFromStateParams!=undefined)
                    {
                        if ($scope.localVarsExecuteTask.backBtnFromCookie)
                        {
                          $scope.localVarsExecuteTask.fromStateParams =  $scope.localVarsExecuteTask.cookieFromStateParams;
                        }
                    }
                    
                }else if ($scope.localVarsExecuteTask.cookieFromStateParams==undefined)
                {
                  $cookies.putObject('executeTaskFromStateParams',$scope.localVarsExecuteTask.fromStateParams);
                  
                }else
                {
                    if ($scope.localVarsExecuteTask.backBtnFromCookie)
                    {
                  
                      $scope.localVarsExecuteTask.fromStateParams =  $scope.localVarsExecuteTask.cookieFromStateParams;
                  
                    }else
                    {
                        $cookies.putObject('executeTaskFromStateParams',$scope.localVarsExecuteTask.fromStateParams);    
                    }
                }
      
                $scope.localVarsExecuteTask.gridsterType="task";
              //  $scope.localVarsExecuteTask.gridsterExeFormOpts = GridsterService.getGridsterOptions();
                $scope.localVarsExecuteTask.formType="e";
               
                $scope.localVarsExecuteTask.isCollapsedTaskDetails=true;
                  $scope.localVarsExecuteTask.FormLockUnlock = function()
                {
                  $scope.localVarsExecuteTask.resourceDefinition.formParams.formLocked=!$scope.localVarsExecuteTask.resourceDefinition.formParams.formLocked;
                };
                
                $scope.localVarsExecuteTask.goBack = function(){
                                    $state.go($scope.localVarsExecuteTask.fromStateName,$scope.localVarsExecuteTask.fromStateParams);
                             
                };
                
                
                $scope.localVars.toggleDropdown = function(){
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
                }

                
                $scope.localVars.setTaskExecuteData = function(inputData){
                                              
                                             $scope.localVarsExecuteTask.resourceDefinition= inputData.data.resourceDefinition;
                                             $scope.localVarsExecuteTask.dashboard.widgets.layout = inputData.data.widgets;
                                             $scope.localVarsExecuteTask.dashboard.data = inputData.data.form_data;
                                             $scope.localVarsExecuteTask.taskDefinition = inputData.data.taskdef[0];
                                             $scope.localVarsExecuteTask.taskDefinition.transaction_id = inputData.data.transactionId;
                                             $scope.localVarsExecuteTask.taskDefinition.fd = $scope.fd;
                                             $scope.localVarsExecuteTask.taskId = $scope.localVarsExecuteTask.taskDefinition.ResourceDefId_id;
                                             if ($scope.inlineTaskDefinition!=undefined) $scope.localVarsExecuteTask.taskDefinition.prev_task_id = $scope.inlineTaskDefinition.prev_task_id;
                                             
                                             $scope.localVarsExecuteTask.taskInitiateInstance = inputData.data.taskInitiateInstance;
                                             $scope.localVarsExecuteTask.taskExecuteInstance = inputData.data.taskExecuteInstance;
                                             if (!$scope.localVarsExecuteTask.OpenTaskInNewPage) $scope.localVarsExecuteTask.ShowCloseBtn=true;
                                      
                                             $scope.localVarsExecuteTask.initiateFormLoaded = true;
                                             $scope.localVarsExecuteTask.isCollapsed3 =false;
                                             $scope.localVarsExecuteTask.resourceDefinition.formParams={formLocked:true,formViewType:2};
                                             
                                                
                                             if ($scope.localVarsExecuteTask.taskExecuteInstance.length>0)
                                             {
                                                $scope.localVarsExecuteTask.taskDefinition.task_execute_id = $scope.localVarsExecuteTask.taskExecuteInstance[0].id;
                                                $scope.localVarsExecuteTask.taskDefinition.taskExecuteInstance=$scope.localVarsExecuteTask.taskExecuteInstance[0];
                                             
                                             }
                                             else
                                             {
                                              
                                              $scope.localVarsExecuteTask.taskDefinition.task_execute_id = 0;
                                             }
                                             
                                             if ($scope.localVarsExecuteTask.taskInitiateInstance.length>0)
                                             {
                                                $scope.localVarsExecuteTask.taskDefinition.task_initiate_id = $scope.localVarsExecuteTask.taskInitiateInstance[0].id;
                                                $scope.localVarsExecuteTask.taskDefinition.taskInitiateInstance=$scope.localVarsExecuteTask.taskInitiateInstance[0];
                                             
                                             }
                                             else
                                             {
                                              $scope.localVarsExecuteTask.taskDefinition.task_initiate_id = 0;
                                              
                                             }
                                             $scope.localVarsExecuteTask.taskDefinition.form_type="e";
                                            
                                            if ($scope.localVarsExecuteTask.taskDefinition.TaskOutcomeHeadId_id==undefined || $scope.localVarsExecuteTask.taskDefinition.TaskOutcomeHeadId_id==null)
                                            {
                                              $scope.localVarsExecuteTask.taskDefinition.TaskOutcomeHeadId_id = 0;
                                              
                                            }
                                            
                                             $scope.$on('ExecuteTaskPostProcess'+ $scope.localVarsExecuteTask.taskDefinition.task_execute_id,function(event,data1){
                                                $scope.localVars.getTaskExecuteData($scope.localVarsExecuteTask.taskDefinition.task_initiate_id,$scope.localVarsExecuteTask.taskDefinition.task_execute_id);
                                            });
                                            $scope.$on('InlineTaskClosed',function(event,data){
                                                if (data==$scope.localVarsExecuteTask.taskId) $scope.localVars.executeTaskDataLoaded=false;
                                            });
                  
                   
                                            $scope.$on('RefreshData'+ $scope.localVarsExecuteTask.taskDefinition.task_execute_id,function(event,data2){
                                              
                                               $scope.localVars.getTaskExecuteData($scope.localVarsExecuteTask.taskDefinition.task_initiate_id,$scope.localVarsExecuteTask.taskDefinition.task_execute_id,data2);
                                            });
                                            
                                            /*
                                            $scope.$on('ExecuteTask'+$scope.localVarsExecuteTask.task_execute_id,function(event,data){
                                                $scope.localVars.ExecuteTask(data.form,data.outcomeId);
                                            });
                                              */                            
                                            TaskDataService.GetTaskOutcomes($scope.localVarsExecuteTask.taskDefinition.ResourceDefId_id,$scope.localVarsExecuteTask.taskDefinition.TaskOutcomeHeadId_id,lang).then(function(data3) {            
                                              
                                                $scope.localVarsExecuteTask.taskOutcomes = data3.data;
                                                    
                                                   
                                                $scope.localVars.executeTaskDataLoaded=true;
                                              
                                             
                                            
                                              },function(error){
                                          
                                                  MessagingService.addMessage(error.msg,'error');
                                              });
                }
      
                $scope.localVars.getTaskExecuteData = function(initiate_id,execute_id,inData){
                      var lang=$rootScope.globals.currentUser.lang;
                     
                        $scope.localVarsExecuteTask.task_initiate_id = initiate_id;
                      
                        
                        $scope.localVarsExecuteTask.task_execute_id = execute_id;
                        $scope.localVars.executeTaskDataLoaded=false;                       
                      TaskDataService.preExecuteTask(initiate_id,execute_id,lang).then(function(data) {
                                             
                                                                        
                                      $scope.localVars.setTaskExecuteData(data);       
                          
                                        },function(error){
                                          
                                      
                                            MessagingService.addMessage(error.msg,'error');
                                      });
                   
                 };
                $scope.localVars.ExecuteTaskAction = function(action){
                     
                     
                        var lang=$rootScope.globals.currentUser.lang;
                      
                       TaskDataService.ExecuteTaskAction($scope.localVarsExecuteTask.task_initiate_id,$scope.localVarsExecuteTask.task_execute_id,action,lang).then(function(data) {
                 
                                             
                                              $scope.localVarsExecuteTask.taskInitiateInstance =data.data.data.taskInitiateInstance;
                                              $scope.localVarsExecuteTask.taskExecuteInstance =data.data.data.taskExecuteInstance;
                                              if ($scope.localVarsExecuteTask.taskExecuteInstance!=undefined)
                                                {
                                                $scope.localVarsExecuteTask.taskDefinition.task_execute_id = $scope.localVarsExecuteTask.taskExecuteInstance.id;
                                             
                                                }
                                                
                                              if (action=="claim")
                                              
                                              {
                                                $scope.localVars.pageChanged();
                                                $scope.localVarsExecuteTask.enableActions=true;
                                                $rootScope.$broadcast('RefreshData0',data.data.ret_value);
                                                
                                              }
                                              
                                              MessagingService.addMessage(data.data.msg,'success');
                                             
               
                                        },function(error){
                                          
                                      
                                            MessagingService.addMessage(error.msg,'error');
                                      });
                }
                
                
      
                $scope.localVars.ExecuteTask = function(form,outcomeId){
                        
                        
                      var lang=$rootScope.globals.currentUser.lang;
                      
                      if (form.$submitted) $scope.localVars.setSubmitted(form);
                      if (form.$valid) {
                      
                      
                          TaskDataService.executeTask($scope.localVarsExecuteTask.task_execute_id,$scope.localVarsExecuteTask.dashboard.data,outcomeId,lang).then(function(data) {
                                      
                                  MessagingService.addMessage(data.data.msg,'success');
                                  $rootScope.$broadcast('ExecuteTaskPostProcess'+$scope.localVarsExecuteTask.task_execute_id,data.data.ret_value);
                          },function(error){
                                  
                                  MessagingService.addMessage(error.msg,'error');           
                                            
                          });
                          
                    } // if (isValid)
                    else
                    {
                      MessagingService.addMessage("Please enter required data",'warning');
                      
                    }
                }; 
     
            $scope.localVars.setSubmitted = function (form) {
               form.$setSubmitted();
               angular.forEach(form, function(item) {
               if(item && item.$$parentForm === form && item.$setSubmitted) {
                   $scope.localVars.setSubmitted(item);
               }
           });
       }; 
         
          let template;
                        if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktExecuteTaskWidget.html');
                        else template = $templateCache.get('ktdirectivetemplates/ktExecuteTaskWidgetExe.html');
                      
                        $element.append($compile(template)($scope));  
          
          
          },
             link: function (scope, elem, attrs) {
                 
                 
               
 
    
    
                  
              }
        }             
};