'use strict';

export function ktActionInitiateTask () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktActionInitiateTask.html',
             
             controller: function($scope,$rootScope,ResourcesDataService,MessagingService){
              
              $scope.taskListParameters = {placeholder:'Select or search task...',label:'Initiate task',name:'task',
              paddingLeft:false,selectedItem:null,hideLabel:false,columnSizeLabel:'col-auto',dropdownSize:'col',selectedItem:null};
              
              $scope.taskListParameters.label = $scope.act.name1;
                        
               
              $scope.setInitiateTaskAction = function(item,model)
                        {
                      //  $scope.taskListParameters.label = $scope.$parent.act.name;
                        //$scope.$parent.act.taskId = item.id;
                        
                        $scope.act.taskId = item.id;
                        $scope.taskListParameters.selectedItem = item;
                        
 
                        };
                        
              
                var lang = $rootScope.globals.currentUser.lang;
               
               $scope.setTask = function(){
                if ($scope.act!=null && $scope.act!=undefined)
                {
                if ($scope.act.taskId!=null) {
                                  var pos = $scope.taskList.map(function(e) { return e.id; }).indexOf($scope.$parent.act.taskId);
                                  if (pos>-1) {
                                     $scope.taskListParameters.selectedItem = $scope.taskList[pos];
                                         
                                   }
                                  
                  }
                }
             };
             
             $scope.getTaskList = function()
             {
                if ($scope.taskList==undefined)
                {
                   ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
                                   
                                     $scope.taskList = data.data;
                                     $scope.setTask();
                                  
                                    
                                  },function(error){
                                   
                                     MessagingService.addMessage(error.msg,'error');
                               });
                }else  $scope.setTask();
                
              }; 
                
                 $scope.$watch('act.taskId', function(oldValue,newValue){
                     
                     $scope.getTaskList();
                   });
               
            
             
             $scope.getTaskList(); 
             
             },
             link: function (scope, elem, attrs) {
            
     
        }
    }
};

//export {ktActionInitiateTask};