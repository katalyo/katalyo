'use strict';

export function ktInitiateTaskWidget ($timeout) {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktInitiateTaskWidget.html',
             /*scope: {
                inputParameters: '=',
                //selectedItem: '=',
                inputArray: '=',
            },*/

             controller: function($scope,$rootScope,UserService,ResourcesDataService,TaskDataService,MessagingService,KatalyoStateManager,GridsterService,$timeout,$state,$stateParams) {
              
                $scope.gsrvc= GridsterService;
                $scope.CTRL='ktInitiateTaskWidget'
                if (!Array.isArray($scope.fd.FormDef)) $scope.fd.FormDef=[];
                if ($scope.resourceDefinition!=undefined)
                {
                  $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                }
                
                 if (!$scope.hasOwnProperty('localVars')) $scope.localVars={};//migor TODO - provjeriti da li ovim nesto breakamo
                if ($scope.fd.localVars == undefined) $scope.fd.localVars={};
            
                //$scope.localVars.showInlineTaskLocal = false;
                //$scope.fd.InlineTask=true; // ToDo change later when properties become available
                //$scope.localVars.isInlineTask=$scope.fd.InlineTask;
                $scope.fd.localVars.loadedTasks = [];
                $scope.localVars.resourceItem = null;
                $scope.localVars.selectedResource = {};
                $scope.localVars.showMenu = false
      
                $scope.localVars.toggleDropdown = function(){
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
                }
  
                

                if ($scope.fd.ElementType=="itask") $scope.localVars.datasetMapperType=0;                
                
                
                
                /*********** SETUP KATALYO STATE MANAGER *************************/
                $scope.ksm = KatalyoStateManager;
                let initiatedTaskId=null;
                let initiateTaskState= $scope.ksm.addSubject('initiateTaskStateName'+$scope.fd.PresentationId,initiatedTaskId);
                let initiateTaskStateName = initiateTaskState.subject.name;
                
                let task_initiated = function(data)
                {
                    initiateTaskState.subject.setStateChanges = data;
                    initiateTaskState.subject.notifyObservers("taskInitiated");
                    
                };
                
                let action_task_initiated = $scope.ksm.addAction(initiateTaskStateName,"taskInitiated",task_initiated);
               
                
                               
                $scope.$on('InlineTaskClosed',function(event,data){
                    //var index=data;
                    //$scope.fd.localVars.loadedTasks.splice(index,1);
                    var taskId=data;
                    var pos = $scope.fd.localVars.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (pos>=0)
                    {
                      $scope.fd.localVars.loadedTasks.splice(pos,1);  
                    }
                });
               
                //var taskDefId = $stateParams.id;
                var lang=$rootScope.globals.currentUser.lang;
                $scope.localVars.taskListParameters = {placeholder:'Select or search task...',label:$scope.fd.placeholder,name:'task',
                paddingLeft:true,selectedItem:null,hideLabel:true,columnSizeLabel:'col-lg-24',dropdownSize:'col-lg-24',onSelectFunction:''};
                
                if ($scope.fd.FormDef==undefined) $scope.fd.FormDef = [];
                $scope.localVars.selectedResourceIndex=-1;
            
              
                $scope.localVars.LoadTask=function(taskId,outcomeId,previousTask,openTaskType)
                  {
                    
                    var localVars = {'isInlineTask':$scope.localVars.isInlineTask,'OpenTaskType':openTaskType,'taskIndex':$scope.fd.localVars.loadedTasks.length,'loadTaskDirect' : true,'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,};
                    
                    if (openTaskType!="newPage") localVars.ShowCloseBtn = true;
                    else localVars.ShowReturnBtn = true;
                    var pos = $scope.fd.localVars.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (pos<0 && openTaskType=="current")
                    {
                        $scope.fd.localVars.loadedTasks.splice(0,0,{'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                        
                    }else if (openTaskType=="inline")
                    {
                        $scope.fd.localVars.loadedTasks.length=0;
                        $scope.fd.localVars.loadedTasks.push({'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                    }else if (openTaskType=="container")
                    {
                        localVars = {'isInlineTask':$scope.localVars.isInlineTask,'OpenTaskType':'current','taskIndex':0,'loadTaskDirect' : true,'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,};
                    
                         $scope.ksm.executeAction(initiateTaskStateName,"taskInitiated", {'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                                
                    }
                    
                    if (openTaskType=="newPage") {
                        $scope.gsrvc.setTaskDataInNewPage({'localVars':localVars,'taskDefinition':$scope.resourceDefinition});
                    }
                    $scope.gsrvc.setOpenTaskType(openTaskType);
                    
                  }
                  
                    $scope.localVars.OpenPage = function(button)
                  
                    {
                        $state.go(button.page_def.ResourceExtended.Target,{pageid: button.id,parameters:{prev_task:$scope.resourceDefinition,page_state_id:$stateParams.parameters?.prev_page_state_id,prev_page_state_id:$scope.resourceDefinition.page_state_id,use_state:false}});
                    }
                  
                  $scope.localVars.taskIsOpen = function(taskId)
                  {
                    var isOpenPos = $scope.fd.localVars.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
                    if (isOpenPos>=0) return true 
                    return false
                  }
                  
                  
                  
                  $scope.localVars.ShowResourceDefinition = function (resourceId){
                      $scope.localVars.showResourceDetails=true
                      
                      if ($scope.fd.FormDef.length>0) {
                        for (var i=0;i<$scope.fd.FormDef.length;i++) {
                            if ($scope.fd.FormDef[i].id==resourceId) {
                                $scope.localVars.selectedResourceIndex=i;
                                 $scope.localVars.selectedResourceType = $scope.fd.FormDef[i].ResourceType;
                                $timeout(function() { $scope.localVars.resourceItem = $scope.fd.FormDef[i] });// resourceItem is shown on screen
                                
                                break;
                              }
                        } 
                      }
                        else {
                            //this is the first resource to be selectedItem - just show
                            if ($scope.localVars.resourceItem) {    
                                 $scope.localVars.selectedResourceIndex=0;    
                                 $scope.localVars.selectedResourceType=$scope.localVars.resourceItem.ResourceType
                                 $timeout(function() { });
                            }
                            
                        }   
                        
                   }
                  
                    
                  $scope.localVars.ShowResourceDefinition22 = function (i){
                      
                      $scope.localVars.showResourceDetails=true
                      
                      if ($scope.fd.FormDef.length>0) {
                          $scope.localVars.selectedResourceIndex=i;
                          $scope.localVars.selectedResourceType = $scope.fd.FormDef[i].ResourceType;
                          $scope.localVars.resourceItem = $scope.fd.FormDef[i] 
                          $scope.localVars.selectedResource = $scope.fd.FormDef[i] 
                          
                          $timeout(function() { 
                                $scope.localVars.resourceItem = $scope.fd.FormDef[i] 
                          });// resourceItem is shown on screen
                          
                       /* for (var i=0;i<$scope.fd.FormDef.length;i++) {
                            if ($scope.fd.FormDef[i].id==resourceId) {
                                $scope.localVars.selectedResourceIndex=i;
                                 $scope.localVars.selectedResourceType = $scope.fd.FormDef[i].ResourceType;
                                $timeout(function() { $scope.localVars.resourceItem = $scope.fd.FormDef[i] });// resourceItem is shown on screen
                                
                                break;
                              }
                        } */
                      }
                        else {
                            //this is the first resource to be selectedItem - just show
                            if ($scope.localVars.resourceItem) {    
                                 $scope.localVars.selectedResourceIndex=0;    
                                 $scope.localVars.selectedResourceType=$scope.localVars.resourceItem.ResourceType
                                 $timeout(function() { });
                            }
                            

                        }   
                        
                   }
                   
                  
                   $scope.localVars.AddResource = function (){
                       //debugger
                      // $scope.localVars.selectedResourceIndex=-1
                       $scope.localVars.showResourceDetails=false
                       if ( $scope.localVars.selectedResourceIndex == -1) 
                                       $scope.fd.FormDef.push(angular.copy($scope.localVars.selectedResource)); 
                   }
                  
                  $scope.localVars.ShowResourceDetails = function (){
                 
                      $scope.localVars.selectedResource.ResourceType = $scope.localVars.selectedResourceType;
                      $scope.localVars.selectedResource.id = $scope.localVars.resourceItem.id;
                      $scope.localVars.selectedResource.name = $scope.localVars.resourceItem.name;
                      $scope.localVars.selectedResource.btnLabel = $scope.localVars.selectedResource.name;
                      $scope.localVars.selectedResource.btnClass = "btn-primary";
                      
                        if ($scope.localVars.selectedResource.ResourceType=='page') {
                            //get page
                            ResourcesDataService.getResourceDefAll($scope.localVars.selectedResource.id)
                             .then(function(data) {
                                    $scope.localVars.selectedResource.page_def = data.data;
                                   // if ( $scope.localVars.selectedResourceIndex == -1) 
                                   //         $scope.fd.FormDef.push(angular.copy($scope.localVars.selectedResource)); 
                                   // $scope.localVars.selectedResourceIndex = $scope.fd.FormDef.length-1                                    
                             },function(error){
                                MessagingService.addMessage(error.msg,'error');  
                            });  
                        } else {
                            var pos = $scope.fd.FormDef.map(function(e) { if (e!=undefined) return e.id; else return null;}).indexOf($scope.localVars.selectedResource.id);
                          //  if (pos<0)   $scope.fd.FormDef.push(angular.copy($scope.localVars.selectedResource)); 
                        }
                       
                        if ($scope.fd.ElementType=='itask') {
                                $scope.fd.wItemValue = $scope.fd.FormDef;
                        } else  {
                                $scope.fd.tItemValue = $scope.fd.FormDef;
                        }

                    $scope.localVars.showResourceDetails = true
                    $scope.localVars.showAddResource = false

                 };
                  
                $scope.localVars.RemoveResourceFromList = function (index){
                 
                     
                        if (index>=0){
                  
                                  $scope.fd.FormDef.splice(index,1);
                                  $scope.localVars.selectedResourceIndex=-1;
                                  $scope.localVars.selectedResourceType=''
                                  $scope.localVars.showResourceDetails=false;
                                       
                        }
                }
          
          
                 $scope.localVars.saveTaskToResource = function(tdid,tei,datasetDefId,datasetRecordId){
                          TaskDataService.saveTaskToResource(tei,datasetDefId,datasetRecordId,$scope.fd.presentationId)
                          .then(function(data) {
                                                    
                              $state.go('app.menu.tasks.initiate',{id: tdid,fromStateName:$state.current.name,fromStateParams:$state.params,outcome_id:0,prev_task_execute_id:tei});
                                                               
                           },function(error){
                  
                                MessagingService.addMessage(error.msg,'error');
                            });
                 }
                 
                 
                 
                 $scope.localVars.setResourceType = function (resource_type){
                 
                  $scope.localVars.selectedResourceType=resource_type;
                  $scope.localVars.resourceLoaded = false;
                    
                    if   ($scope.gsrvc.getFormViewType()==1 || $scope.fd.ElementType=='itask')
                    {
                            ResourcesDataService.getResourceDefListByType(lang,resource_type).then(function(data) {
                                                    
                                  $timeout(function() {
                                        $scope.localVars.resourceListForDropdown = data.data;
                                        $scope.localVars.resourceLoaded = true;         
                                  });
                                   
      
                               },function(error){

                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
                }    
                  
                    
                 }

        },

        link: function (scope, elem, attrs) {
                
               
        }       
              
    }
  
};