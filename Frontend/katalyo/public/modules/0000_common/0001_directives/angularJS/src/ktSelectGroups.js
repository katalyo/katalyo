'use strict';

export function ktSelectGroups () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktSelectGroups.html',
            /*  scope:{
                selectedItems:'=',
             },*/
            controller: function($scope,UserService){
                
                
                
                $scope.groupList=[];
                
                $scope.getGroups = function () {
                      UserService.getUsersGroups('groups').then(function(data){
                      $scope.groupList = data.data;
                      $scope.groupsDownloaded=true;
                            });
            
                 }
                
                $scope.getGroups();
                            
            },
            link: function (scope, elem, attrs) {
            
            
           
                    
                   
                
            
            }

    }
};