'use strict';

export function ktTaskViewResource () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','KatalyoStateManager','UtilityService','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,KatalyoStateManager,UtilityService,$templateCache,$compile,$element) {
     

      var lang=$rootScope.globals.currentUser.lang;
      $scope.CTRL="ktTaskViewResource"
    

      $scope.fd.localVars = {
          showMenu: false,
          searchFormDownloaded: false,
          formLoaded: true,
          src_type_text: CODES_CONST._POPULATE_TYPE_,
          animationsEnabled: true,
          formParams: {formViewType:1, showGridNavigation:false},
          widgetType: 3,
          gridsterType: "dataset",
          resourceType:"dataset",
          datasetMapperType:0,
          searchFilter:[{}],
          resourceTypesList:[{type:'dataset',name:'Dataset'},{type:'file',name:'File'}]
      }
      
      $scope.fd.localVars.TablePrefix = 'resource'+$scope.fd.Related+'_'+$rootScope.globals.currentUser.organisation.id
       $scope.fd.localVars.toggleDropdown = function(){
           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
       }

      $scope.gsrvc = GridsterService;
      
     
    if ($scope.formData===undefined)
    {

        $scope.formData={}

        $scope.formData[$scope.fd.PresentationId]=[{}]

    }else
    {
        if ($scope.formData[$scope.fd.PresentationId]===undefined || $scope.formData[$scope.fd.PresentationId]===null) $scope.formData[$scope.fd.PresentationId]=[{}]
    }

      if ($scope.fd.PopulateType==undefined) $scope.fd.PopulateType="";
      
        
      $scope.fd.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: {},
                searchForm:{}
      } ;
      if ($scope.fd.ParametersLang===undefined || $scope.fd.ParametersLang===null) $scope.fd.ParametersLang={}

      if ($scope.fd.ParametersLang?.CreatedByTemplate===undefined)  $scope.fd.ParametersLang.CreatedByTemplate='Created by <<user>> on <<date>> at <<time>>'
      if ($scope.fd.ParametersLang?.ChangedByTemplate===undefined)  $scope.fd.ParametersLang.ChangedByTemplate='Last changed by <<user>> on <<date>> at <<time>>'

      $scope.taskId=$scope.resourceDefinition.ResourceDefId;
      if ($scope.taskId==undefined) {
            $scope.taskId = $scope.resourceDefinition.ResourceDefId_id;
      }
      if ($scope.taskId==undefined) $scope.taskId=0;
      $scope.fd.localVars.taskId=$scope.taskId;
    
      $scope.fd.ParentResourceDefId=$scope.fd.Related;
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
      $scope.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.formDefinition = $scope.resourceDefinition.form_definition;
        
      if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
      $scope.fd.localVars.resourceRecordId=0;
      if ($scope.taskInitiateId==undefined)
            $scope.taskInitiateId =0;
      if ($scope.taskExecuteId==undefined)
            $scope.taskExecuteId =0;  
      if ($scope.prevTaskExeId==undefined)
            $scope.prevTaskExeId =0;

      //register widget 
      var out_fields = ['resourceId'];
      var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'viewResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.fd.Related, output:out_fields, };
      $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
      $scope.fd.localVars.LocalId=$scope.fd.localVars.localWidgetId;
      $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
      $scope.ksm = KatalyoStateManager;
      $scope.fd.localVars.widgetState = null;
       
      var valueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+"-"+$scope.fd.UuidRef,$scope.fd.localVars.widgetState);
      var valueStateName = valueState.subject.name;
        
      //*********************** get state if exists ********************************************
      
      if (valueState.subject.currentStateIndex>=0)  {   
          $scope.fd.localVars.widgetState = valueState.subject.getState;
          $scope.fd.localVars.resourceRecordId = $scope.fd.localVars.widgetState.resourceRecordId;
          $scope.fd.localVars.loadFromState=true;
      } else {
        $scope.fd.localVars.widgetState={'resourceId':$scope.fd.Related,'resourceRecordId':0};
      }
      
      let value_changed = function(data){
          valueState.subject.setStateChanges = data;
          valueState.subject.notifyObservers("valueChanged");
      };
      
      let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
       
       let apply_value = function(data)
        {
            let do_refresh=false;
            let state=data.state
            if (state!=undefined)
            {
               
                  if (typeof state ==='number') $scope.fd.localVars.resourceRecordId= state;
                  else if (Array.isArray(state))
                  {
                    if (state.length>0)
                    {
                        if ($scope.fd.localVars.TablePrefix+'_id' in state)
                        {
                            $scope.fd.localVars.resourceRecordId= state[0][$scope.fd.localVars.TablePrefix+'_id']
                        }else $scope.fd.localVars.resourceRecordId= state[0].id

                     }
                }else
                    {
                    if ($scope.fd.localVars.TablePrefix+'_id' in state)
                    {
                        $scope.fd.localVars.resourceRecordId= state[$scope.fd.localVars.TablePrefix+'_id']
                    }else $scope.fd.localVars.resourceRecordId= state.id
                     
                  }
                  do_refresh = true;
               
            }
            //get selectedRecord
            if ($scope.fd.localVars.resourceRecordId == null) $scope.fd.localVars.resourceRecordId=0;
            
            if ($scope.fd.localVars.resourceRecordId>0 && do_refresh) $scope.fd.localVars.GetRecordForView($scope.fd.ParentDatasetDefId,$scope.fd.localVars.resourceRecordId)
            
            
        };
        
      
      if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
      
      if ($scope.fd.Parameters.RefreshOnIdChange)
      {
         
         //find 
         //subscribe observer
           
         let dependent_field = "Field"+$scope.fd.UuidRef;
         let subscribedSubject = "resourceidValueStateName"+$scope.fd.Parameters.ResourceIdItemId+$scope.resourceDefinition.transaction_id;
         let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
         let val = $scope.ksm.getState(subscribedSubject);
         if ($scope.fd.localVars.TablePrefix+'_id' in val)
        {
            $scope.fd.localVars.resourceRecordId= val[$scope.fd.localVars.TablePrefix+'_id']
        }else $scope.fd.localVars.resourceRecordId= val.id
            
            //get selectedRecord
         if ($scope.fd.localVars.resourceRecordId == null) $scope.fd.localVars.resourceRecordId=0;
            
         if ($scope.fd.localVars.resourceRecordId>0) $scope.fd.localVars.GetRecordForView($scope.fd.ParentDatasetDefId,$scope.fd.localVars.resourceRecordId);
      }
      
      //subscribe observer
      if ($scope.fd.PopulateType.toString().substr(0,1)=="4")
      {
           /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
       
        if ($scope.fd.SrcWidgetId!=undefined && $scope.fd.SrcWidgetId!=null)
        
        {
           
                let dependent_field = "Field"+$scope.fd.SrcWidgetId;
               // let subscribedSubject = $scope.fd.Parameters.SrcWidgetType+"ValueStateName"+$scope.fd.SrcWidgetId;
                let subscribedSubject = "rlistValueStateName"+$scope.fd.SrcWidgetId+$scope.resourceDefinition.transaction_id;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            
        }  
         
      }
      
      let refresh_widget_data = function()
      
      {
         $scope.fd.localVars.GetRecordForView($scope.fd.ParentDatasetDefId,$scope.fd.localVars.resourceRecordId)    
         
      }
      if ($scope.fd.Parameters.EventName!=undefined)
      {
         if ($scope.fd.Parameters.EventName!="")
         {
            let dependent_field2 = "Field"+$scope.fd.PresentationId;
            let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
            let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,refresh_widget_data);
         }
      }
      
      
      $scope.fd.localVars.showProperties=function(datasetMapperType,showPropertiesRight)
      {
            
            if ($scope.fd.localVars.datasetMapperType==undefined)
            {
               $scope.fd.localVars.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                    $scope.fd.localVars.datasetMapperType = datasetMapperType;
               else 
                   $scope.fd.localVars.datasetMapperType=0;
            }
            if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);
            
      };
   
      $scope.fd.openModal = function(modalId)
      {
        $(modalId).modal();
      }
      

      $scope.fd.localVars.setField = function(form,name,value,msg)
      {
         TaskDataService.setField(form,name,value);
         if (msg) MessagingService.addMessage(msg,'info');
      };  
      
      $scope.fd.localVars.getFields = function(form,requiredFields,msg)
      {
         let resultProperties =  TaskDataService.getFields(form, requiredFields);
         if (msg) MessagingService.addMessage(msg,'info');
         return resultProperties
      };  
      
    
        

     
      
         $scope.fd.localVars.GetRecord = function(){

         ResourcesDataService.getDatasetData($scope.fd.Parameters.PublishedResourceId,$scope.fd.localVars.searchFilter[0]).then(function(data) {
             //ResourcesDataService.getTaskViewResource($scope.fd.PresentationId,$scope.taskId,$scope.fd.localVars.formType,$scope.fd.Related,).then(function(data) {

                                  let dataset_record
                                  if (Array.isArray(data.data))
                                    {
                                      if  (data.data.length>0) dataset_record  = data.data[0]
                                      else dataset_record={}
                                    }
                                  else dataset_record  = data.data
                                  $scope.formData[$scope.fd.PresentationId][0] =  dataset_record
                                $scope.fd.localVars.dataDownloaded = true
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
      }


     
       $scope.fd.localVars.setSelectedDataset = function(id,saved)  {
              //debugger //setSelectedDataset
              if ($scope.fd.localVars.selectedResourceItem==undefined) 
                                $scope.fd.localVars.selectedResourceItem={};
              $scope.fd.localVars.getPublishedResourceById(id,saved);

              
    }


    $scope.fd.localVars.ProcessAuditInfo = function(record)
    {

        $scope.fd.localVars.CreatedByLabel = $scope.fd.ParametersLang.CreatedByTemplate.replace('<<user>>',record[$scope.fd.localVars.TablePrefix+'_CreatedBy.name'])
        $scope.fd.localVars.CreatedByLabel = $scope.fd.localVars.CreatedByLabel.replace('<<date>>',new Date(record[$scope.fd.localVars.TablePrefix+'_CreatedDateTime']).toLocaleDateString())
        $scope.fd.localVars.CreatedByLabel = $scope.fd.localVars.CreatedByLabel.replace('<<time>>',new Date(record[$scope.fd.localVars.TablePrefix+'_CreatedDateTime']).toLocaleTimeString())
        if (record[$scope.fd.localVars.TablePrefix+'_ChangedBy.name']!==null)
        {
            $scope.fd.localVars.ChangedByLabel = $scope.fd.ParametersLang.ChangedByTemplate.replace('<<user>>',record[$scope.fd.localVars.TablePrefix+'_ChangedBy.name'])
            $scope.fd.localVars.ChangedByLabel = $scope.fd.localVars.ChangedByLabel.replace('<<date>>',new Date(record[$scope.fd.localVars.TablePrefix+'_ChangedDateTime']).toLocaleDateString())
            $scope.fd.localVars.ChangedByLabel = $scope.fd.localVars.ChangedByLabel.replace('<<time>>',new Date(record[$scope.fd.localVars.TablePrefix+'_ChangedDateTime']).toLocaleTimeString())
            $scope.fd.localVars.HasChangedBy=true
        } 
    }

    $scope.fd.localVars.ExtractIdFromRecord=function(record){

        let record_id

        if (record!==undefined)
        {   
            if (Array.isArray(record) && record.length>0)
            {
                if ($scope.fd.localVars.TablePrefix+'_id' in record[0])
                {
                    record_id=record[0][$scope.fd.localVars.TablePrefix+'_id']
                 
                }else record_id = record[0].id

            }else{
                if ($scope.fd.localVars.TablePrefix+'_id' in record)
                {
                    record_id = record[$scope.fd.localVars.TablePrefix+'_id']
                 
                }else record_id = record.id

            }
        }else record_id = null

        return record_id   
    }
          
     
      $scope.fd.localVars.GetRecordForView = function(datasetDefId,datasetRecordId){
         
         $scope.fd.localVars.resourceRecordId = datasetRecordId
         $scope.fd.ParentDatasetDefId = datasetDefId

          TaskDataService.GetTaskViewResource($scope.fd.PresentationId,$scope.taskId,$scope.taskExecuteId,$scope.fd.localVars.formType,datasetDefId,datasetRecordId).then(function(data) {

                                  let dataset_record
                                  if (Array.isArray(data.data.data))
                                    {
                                      if  (data.data.length>0) dataset_record  = data.data.data[0]
                                      else dataset_record={}
                                    }
                                  else dataset_record  = data.data.data
                                  $scope.formData[$scope.fd.PresentationId][0] =  dataset_record
                                    $scope.fd.localVars.resourceRecordId = $scope.fd.localVars.ExtractIdFromRecord(dataset_record)
                                   WidgetDataExchangeService.setData({DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:datasetDefId},{name:'resourceRecordId',value:$scope.fd.localVars.resourceRecordId}]}],
                                    actions: [{name:'PrepareData',status:false}],

                                    name: 'viewResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,

                                    taskId:  $scope.lTaskId,resourceDefId : $scope.fd.Related, output:out_fields})

                                    $scope.ksm.executeAction(valueStateName,"valueChanged", {datasetDefId:datasetDefId,recordId:$scope.fd.localVars.resourceRecordId,record:$scope.formData[$scope.fd.PresentationId][0]});
                                    
                                    if ($scope.fd.Parameters.ShowAuditInfo) $scope.fd.localVars.ProcessAuditInfo(dataset_record)

                                  $scope.fd.localVars.dataDownloaded = true
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
      }  

$scope.fd.localVars.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData;
     

      let pageState =  $scope.ksm.getState($scope.resourceDefinition.page_state_id)
      let prev_task_execute_id = pageState.state.previousTask?.task_execute_id
      let pTaskDataRet
      if (prev_task_execute_id!==undefined) 
        {
            pTaskDataRet = WidgetDataExchangeService.getPrevTaskData(prev_task_execute_id);

              if (Array.isArray(pTaskDataRet) && pTaskDataRet.length>0)
              {      
                    var datasetId=0,datasetRecordId=0;
                    for (let k=0;k<pTaskDataRet.length;k++)
                    {
                       pTaskData = pTaskDataRet[k];
                       if (pTaskData!=undefined)
                       {
                          if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
                          {
                          for (var i=0;i<pTaskData.DEI.length;i++)
                          {
                            if ((pTaskData.DEI[i].name==linkType) || linkType==undefined)
                            {
                              if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                              {
                                
                                for (let j=0;j<pTaskData.DEI[i].value.length;j++)
                                {

                                 if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined && pTaskData.DEI[i].value[j].value===$scope.fd.Related) 

                                {   
                                    datasetId=pTaskData.DEI[i].value[j].value;
                                    for (let k=0;k<pTaskData.DEI[i].value.length;k++)
                                    {
                                        if (pTaskData.DEI[i].value[k].name=="resourceRecordId" && pTaskData.DEI[i].value[k].value!=undefined && datasetId>0 && pTaskData.DEI[i].value[k].value>0) datasetRecordId=pTaskData.DEI[i].value[k].value;
                                    }
                                }
                                 
                                  
                                }
                                   
                                  
                              }
                                
                            }
                              
                            }
                          }
                       }
                    }
              }
            return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
        }
    };


   
 if (!$scope.resourceDefinition.form_definition && $scope.fd.PopulateMethod=='A' && $scope.fd.PopulateType.toString().substr(0,1)=='3') 

    {
        $scope.fd.localVars.previousTaskData=$scope.fd.localVars.getPreviousTaskWidget();

        if ($scope.fd.localVars.previousTaskData===undefined)
        {
           $scope.fd.localVars.previousTaskData = {datasetRecordId:0,datasetId:$scope.fd.Related} 
        }

    }

$scope.fd.localVars.getPublishedResourceById = function(id,saved) 
 
 {
    
            let pos
            if (saved && $scope.fd.FormDef!==undefined && $scope.fd.FormDef!==null)
              { 
                    pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                    if (pos>=0){
                        $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.publishedResourceList[pos].name;
                        $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos]
                    }
                    $scope.fd.localVars.selectedResourceItem.id = id;
                    $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                    $scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem;
                    $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                    $scope.fd.localVars.dashboard.searchForm.layout=angular.copy($scope.fd.FormDef)
                    $scope.fd.localVars.formLoaded=true;

              }else{
                  pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                  if (pos>=0){
                     $scope.fd.localVars.selectedResourceItem = $scope.fd.localVars.publishedResourceList[pos]
                    ResourcesDataService.GetPublishedResourceById(id).then(function (data) {

                            $scope.fd.localVars.selectedResourceItem.Form = data.data.Form
                            $scope.fd.localVars.dashboard.widgets.layout = data.data.Form
                            $scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem
                            $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                            if ($scope.fd.localVars.selectedResourceItem_c!==undefined)
                            {
                                $scope.fd.Parameters.PublishedResourceId=$scope.fd.localVars.selectedResourceItem_c.id;
                                $scope.fd.Parameters.PublishedResourceVersion=$scope.fd.localVars.selectedResourceItem_c.Version;
                                $scope.fd.Related=$scope.fd.localVars.selectedResourceItem_c.ResourceDefId; 
                                $scope.fd.localVars.dashboard.searchForm.layout=angular.copy($scope.fd.localVars.selectedResourceItem_c.Form)
                                $scope.fd.localVars.freshForm=angular.copy($scope.fd.localVars.selectedResourceItem_c.Form)
                                $scope.fd.Related = $scope.fd.localVars.publishedResourceList[pos].ResourceDefId

                            }else MessagingService.addMessage('Error - published dataset not selected','error')
                            
                         
                            $scope.fd.localVars.setField($scope.fd.localVars.dashboard.widgets.layout,'ReadOnly',true);     
                            $scope.fd.localVars.setField($scope.fd.localVars.dashboard.widgets.layout,'Required',false);     
                            $scope.fd.localVars.setField($scope.fd.localVars.dashboard.searchForm.layout,'Required',false); 
                            $scope.fd.FormDef = $scope.fd.localVars.dashboard.widgets.layout
                            $scope.fd.localVars.formLoaded=true;

                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                    });

                }
            }
             
       
}

  if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
  var lang=$rootScope.globals.currentUser.lang;
  
           
  ResourcesDataService.GetPublishedResourcesByType('dataset','form').then(
        function (data) {       
           $scope.fd.localVars.publishedResourceList = data.data;
           //debugger  //GetPublishedResourcesByType   
           //$scope.fd.localVars.dashboard.widgets.layout = JSON.parse(data.data[0].Form)
                        
           if ($scope.fd.Parameters.PublishedResourceId!=null) 
                                        $scope.fd.localVars.setSelectedDataset($scope.fd.Parameters.PublishedResourceId,true);
             
           },function(error){    
                          MessagingService.addMessage(error.msg,'error');
    });
    
   //$scope.fd.localVars.getResources();
   
  if ($scope.fd.Related!=null &&  !$scope.resourceDefinition.form_definition && !$scope.fd.localVars.searchFormDownloaded)
  { 
     if ($scope.fd.PopulateMethod=='A') $scope.fd.localVars.GetRecordForView($scope.fd.Related,$scope.fd.localVars.previousTaskData.datasetRecordId)
   
  }
      
       
        $scope.fd.localVars.processWidgetDependencies = function() { //-----                
           if ($scope.fd.tItemValue!=undefined) 
            for (var i=0;i< $scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=$scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = $scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<$scope.fd.tItemValue[i].DEI.length;j++) {
                        if (($scope.fd.tItemValue[i].DEI[j].name==linkedItem) && ($scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = $scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("view "+$scope.fd.PresentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             $scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj $scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                              //  console.log("view "+ $scope.fd.PresentationId+" dobio event " + event.name);
                                for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    $scope.fd.localVars.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    $scope.fd.localVars.resourceRecordId = $scope.fd.localVars.linkedResourceId;
                                  }
                                  if (data[k].name=='resourceId') $scope.fd.localVars.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                }
                          
                              //get selectedRecord
                              $scope.fd.localVars.GetRecordForView($scope.fd.ParentDatasetDefId,$scope.fd.localVars.resourceRecordId)

                                    //	data: {resource_def_id:$scope.fd.linkedResourceDefId,resource_id:$scope.fd.linkedResourceId}
                                    
                              }) //on_END
                        }//if
                    }//for j
                }//if

             // else {  $scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget

          if ($scope.fd.PresentationId==undefined || $scope.fd.PresentationId==null) $scope.fd.PresentationId=0;
          
          $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies($scope.taskId,$scope.fd.PresentationId,$scope.fd.localVars.formType).then(function(data) {
                               
                                $scope.fd.tItemValue=data.data;
                                $scope.fd.localVars.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
                              
            
            $scope.fd.verify_blockchain_data = function () {
               
                
               ResourcesDataService.getBlockchainDatasetRecords($scope.fd.Related,'dataset','kataly1.code',1,$scope.formData[$scope.fd.PresentationId][0].BChainRecId,lang).then(function(data) {
                               
                                $scope.formData[$scope.fd.PresentationId][0].formDataBlockchain= JSON.parse(data.data.data.rows[0].datasetData);
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
                
            };
            $scope.fd.get_transaction = function () {
               
                
               ResourcesDataService.getBlockchainTransaction($scope.formData[$scope.fd.PresentationId][0].TrxId,$scope.formData[$scope.fd.PresentationId][0].BlockId).then(function(data) {
                               
                               // let ret_val = {'block_num':data.data.block_num,'block_time':data.data.block_num,
                               // 'id':data.data.id,'traces':data.data.traces,'trx':data.data.trx};
                                let ret_val = data.data;
                                $scope.fd.BlockChainTransaction = JSON.stringify(ret_val,null,2);
                                $scope.fd.showTransaction = true;
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
                
            };

    
            let template;
            if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskViewResource.html');
            else template = $templateCache.get('ktdirectivetemplates/ktTaskViewResourceExe.html');
          
            $element.append($compile(template)($scope)); 

  
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

              

        }     
  }
  
};