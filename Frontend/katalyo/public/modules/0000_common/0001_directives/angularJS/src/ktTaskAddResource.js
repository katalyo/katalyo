'use strict';

export function ktTaskAddResource ($timeout) {
   
   var controller = ['$scope', '$uibModal', '$state','$stateParams','dragularService','$timeout','CodesService','ResourcesDataService','$rootScope','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','CODES_CONST','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$state,$stateParams,dragularService,$timeout,CodesService,ResourcesDataService,$rootScope,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,CODES_CONST,$templateCache,$compile,$element) {
      
      $scope.fd.localVars = {};
      $scope.fd.localVars.searchFormDownloaded = false;
      //$scope.fd.localVars.formLoaded=true;
      $scope.fd.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_;
      $scope.fd.localVars.animationsEnabled = true;
      $scope.fd.localVars.widgetType = 1;
      $scope.fd.localVars.hidePopulateMethod = true;
      $scope.fd.localVars.datasetMapperType=0;      
      $scope.fd.localVars.formParams={formViewType:1,showGridNavigation:false};
      $scope.fd.localVars.showMenu = false
     
      if ($scope.fd.Parameters===null || $scope.fd.Parameters===undefined) $scope.fd.Parameters = {}
      if ($scope.formData===undefined)  $scope.formData={}
      $scope.formData[$scope.fd.PresentationId]= [{}]
      $scope.fd.Parameters.SelectedResourceType='dataset'

      $scope.fd.localVars.toggleDropdown = function(){
           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
       }
       
      //if ($scope.formParams!==undefined) $scope.fd.localVars.formParams = angular.copy($scope.formParams);
      
      $scope.formParamsParent=$scope.formParams;
      
      if ($scope.fd!=undefined)
      {
           $scope.fd.PopulateMethod = "M";
      }else
      {
           $scope.fd={PopulateMethod : "M"};
      }
      if (typeof($scope.fd.DatasetMapping)=='string') $scope.fd.DatasetMapping=[];
      $scope.gsrvc = GridsterService;
      
      
      /*
      if ($scope.fd.hasParentDataset) {
        
        $scope.fd.ParentDatasetId = $stateParams.presentation_id;
      
      }
      
      $scope.fd.localVars.presentationIdLocal = $scope.fd.PresentationId;
      
      if ($scope.fd.localVars.parentDatasetIdLocal == undefined || $scope.fd.localVars.parentDatasetIdLocal=="")
      {
        $scope.fd.localVars.parentDatasetIdLocal = 0;
        
      }else
      {
        
         $scope.fd.ParentDatasetId = $scope.fd.localVars.parentDatasetIdLocal;
      }
      */

      $scope.fd.localVars.openModalSelectResource = function(){
        $scope.fd.localVars.selectDatasetType  = 1;
        $scope.fd.localVars.openModal('lg','selectResourceModal.html','SelectResourceModalCtrl',$scope.fd.localVars.selectDatasetType);
        
      };
     
      $scope.fd.localVars.openModalSelectRelatedResource = function(){
        $scope.fd.localVars.selectDatasetType  = 2;
        $scope.fd.localVars.openModal('lg','selectDatasetModal.html','SelectDatasetModalCtrl',$scope.fd.localVars.selectDatasetType);
      };
      
     
          $scope.fd.localVars.gridsterType="dataset";
          $scope.fd.localVars.resourceType="dataset";
          // $scope.gridsterOpts = GridsterService.getGridsterOptions();
    
    
        
  $scope.fd.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
       
     
  
        //$scope.taskId=$stateParams.id;
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        $scope.fd.localVars.taskId=$scope.taskId;
        $scope.fd.ParentDatasetDefId=$scope.fd.Related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
        $scope.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        $scope.formDefinition = $scope.resourceDefinition.form_definition;
        $scope.fd.localVars.resourceRecordId=0;
        
        $scope.formDefinition = $scope.formDefinition || false
        $scope.taskInitiateId = $scope.taskInitiateId || 0
        $scope.taskExecuteId = $scope.taskExecuteId || 0
        $scope.prevTaskExeId = $scope.prevTaskExeId || 0
      
        
      //register widget 
      var out_fields = ['resourceId']
      var widgetDefinition = {DEI: [{'name':'resourceLink','displayName':'Dataset record id','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],displayName: 'Add dataset record',name: 'addResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.fd.localVars.selectedResource, output:out_fields, };
      $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
      $scope.fd.localVars.localId=$scope.fd.localVars.localWidgetId;
      $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;  		  
      
      
      $scope.fd.localVars.showProperties=function(datasetMapperType,showPropertiesRight)
      {
            
            if ($scope.fd.localVars.datasetMapperType==undefined)
            {
               $scope.fd.localVars.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                    $scope.fd.localVars.datasetMapperType = datasetMapperType;
               else 
                   $scope.fd.localVars.datasetMapperType=0;
            }
            if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);
            
      };
      
        
       
      
        $scope.fd.localVars.setField = function(form,name,value,msg)
        {
     				TaskDataService.setField(form,name,value);
            if (msg) MessagingService.addMessage(msg,'info');
        };
        
        



        $scope.fd.localVars.setSelectedDataset = function(id,saved)  {

              if ($scope.fd.localVars.selectedResourceItem==undefined) $scope.fd.localVars.selectedResourceItem={}
              $scope.fd.localVars.getPublishedResourceById(id,saved);
		}
          
$scope.fd.localVars.getPublishedResourceById = function(id,saved) 
 
 {
            let pos
            if (saved && $scope.fd.FormDef!==undefined && $scope.fd.FormDef!==null)
              { 
                    pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                    if (pos>=0){
                        $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.publishedResourceList[pos].name;
                        $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos]
                    }
                    $scope.fd.localVars.selectedResourceItem.id = id;
                    $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                    $scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem;
                    $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                    $scope.fd.localVars.formLoaded=true;

              }else{
                  pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                  if (pos>=0){
                    ResourcesDataService.GetPublishedResourceById(id).then(function (data) {

                            $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos];
                            $scope.fd.localVars.dashboard.widgets.layout = data.data.Form
                            $scope.fd.FormDef = $scope.fd.localVars.dashboard.widgets.layout
                            if ($scope.fd.localVars.selectedResourceItem_c!==undefined)
                            {
                                $scope.fd.Parameters.PublishedResourceId=$scope.fd.localVars.selectedResourceItem_c.id;
                                $scope.fd.Parameters.PublishedResourceVersion=$scope.fd.localVars.selectedResourceItem_c.Version;
                                $scope.fd.Related=$scope.fd.localVars.selectedResourceItem_c.ResourceDefId; 
                                $scope.fd.localVars.freshForm=angular.copy($scope.fd.localVars.selectedResourceItem_c.Form)
                                $scope.fd.Related = $scope.fd.localVars.publishedResourceList[pos].ResourceDefId

                            }else MessagingService.addMessage('Error - published dataset not selected ktTAskAdd','error')
          
                            $scope.fd.localVars.formLoaded=true;

                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                    });

                }
            }
             
       
}
 

    if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
    var lang=$rootScope.globals.currentUser.lang;

              
      ResourcesDataService.GetPublishedResourcesByType('dataset','form').then(function (data) {       
           
          if  ($scope.fd.localVars==undefined)  $scope.fd.localVars = {}
           $scope.fd.localVars.publishedResourceList = data.data;
                        
           if ($scope.fd.Parameters.PublishedResourceId!=null) $scope.fd.localVars.setSelectedDataset($scope.fd.Parameters.PublishedResourceId,true);
           else {

              $scope.fd.localVars.formLoaded=true;
              $scope.fd.localVars.NoPublishedResourceMsg="Definition error --> Published dataset not defined!"
            }
             
           },function(error){    
                          MessagingService.addMessage(error.msg,'error');
    });

                  
  
  
  
              $scope.fd.localVars.prepareResourceData = function(resourceRecordId){

             $scope.fd.itemValue =  ResourcesDataService.prepareResourceWidgetData($scope.fd.localVars.dashboard.widgets.layout,resourceRecordId);
            }
          
           $scope.fd.localVars.processWidgetDependencies = function() { //-----  ktUploader  -----
              
           if ($scope.fd.tItemValue!=undefined) 
            for (var i=0;i< $scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=$scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = $scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<$scope.fd.tItemValue[i].DEI.length;j++) {
                        if (($scope.fd.tItemValue[i].DEI[j].name==linkedItem) && ($scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = $scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("view "+$scope.fd.PresentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             $scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj $scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                               // console.log("view "+ $scope.fd.PresentationId+" dobio event " + event.name);
                                for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    $scope.fd.localVars.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    $scope.resourceRecordId = $scope.fd.localVars.linkedResourceId;
                                  }
                                  if (data[k].name=='resourceId') $scope.fd.localVars.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                }
                          
                              //get selectedRecord
                              //$scope.getFormResourceWithData();
                                
                                    //	data: {resource_def_id:$scope.fd.linkedResourceDefId,resource_id:$scope.fd.linkedResourceId}
                                    
                              }) //on_END
                        }//if
                    }//for j
                }//if

             // else {  $scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget

          if ($scope.fd.PresentationId==undefined || $scope.fd.PresentationId==null) $scope.fd.PresentationId=0;
          
          $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies($scope.taskId,$scope.fd.PresentationId,$scope.fd.localVars.formType).then(function(data) {
                               
                                $scope.fd.tItemValue=data.data;
                                $scope.fd.localVars.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                         });
        

        $scope.fd.localVars.processTaskReturn = function(data)
        {

             if (data!=undefined){
                let rv
                if (Array.isArray(data))
                {
                 rv  = data
                }else rv =[data]

                  for(var i=0;i<rv.length;i++) {
                    
                    if ($scope.fd.PresentationId==rv[i].widgetId)
                    {
                      WidgetDataExchangeService.setData(rv[i]);
                      $scope.fd.localVars.setField($scope.fd.localVars.dashboard.widgets.layout,'ReadOnly',true); //set all elements read-only
                    }
                  }
            }
            
        }


         $scope.$on('InitiateTaskPostProcess'+$scope.resourceDefinition.transaction_id,function(event, data){
            $scope.fd.localVars.processTaskReturn(data)
              
         });
            
        $scope.$on('ExecuteTaskPostProcess'+$scope.resourceDefinition.task_execute_id,function(event, data){
              
            $scope.fd.localVars.processTaskReturn(data)
              
          });


                  let template;
                if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskAddResource.html');
                else template = $templateCache.get('ktdirectivetemplates/ktTaskAddResourceExe.html');
          
                $element.append($compile(template)($scope)); 
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

           
            
                let widgetState= scope.ksm.addSubject('taskWidget'+scope.fd.PresentationId,scope.fd.PresentationId);
                let widgetStateName = widgetState.subject.name;
                
                let clear_form = function()
                {
                  //scope.fd.localVars.dashboard.widgets.layout=[]
                  scope.fd.localVars.formLoaded=false
                  
                  $timeout(function(){
                    scope.fd.localVars.formLoaded=true;
                    scope.fd.localVars.dashboard.widgets.layout = angular.copy(scope.fd.localVars.freshForm);
                    scope.formData[scope.fd.PresentationId]= [{}]
                    widgetState.subject.setStateChanges = scope.fd.PresentationId
                    widgetState.subject.notifyObservers("clearform");
                  })  
                
                
                 
               
                } 
                let action_form_cleared = scope.ksm.addAction(widgetStateName,"clearform",clear_form);
            }     
        }
  }
