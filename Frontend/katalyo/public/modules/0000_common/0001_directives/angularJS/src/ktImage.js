'use strict';

export function ktImage () {
        return {
            restrict: 'A',
	  controller: function ($scope,$rootScope,GridsterService,ResourcesDataService,KatalyoStateManager,WidgetDataExchangeService) {
                
                if ($scope.fd.localVars==undefined) $scope.fd.localVars={};
                $scope.fd.localVars.showMenu = false
      
                $scope.fd.localVars.toggleDropdown = function(){
                           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                }

                $scope.taskId=$scope.resourceDefinition.ResourceDefId;
     
                  $scope.fd.localVars.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
                  $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
                  $scope.fd.localVars.source_type=$scope.resourceDefinition.source_type;
                  $scope.fd.localVars.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
                  $scope.fd.localVars.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                  $scope.fd.localVars.formDefinition = $scope.resourceDefinition.form_definition;
                  $scope.fd.localVars.resourceRecordId = 0;
                  $scope.fd.ParentElementId = 0;
                  if ($scope.fd.localVars.formDefinition==undefined) $scope.fd.localVars.formDefinition = false;
                  if ($scope.fd.localVars.taskInitiateId==undefined) $scope.fd.localVars.taskInitiateId =0;
                  if ($scope.fd.localVars.taskExecuteId==undefined) $scope.fd.localVars.taskExecuteId =0;
                  if ($scope.fd.localVars.prevTaskExeId==undefined) $scope.fd.localVars.prevTaskExeId =0;
                $scope.fd.localVars.colLength=[];
                for (let i=0;i<24;i++) $scope.fd.localVars.colLength.push('col-'+(i+1));
                
                var widgetDefinition = {name: 'ktImage',widgetId: $scope.fd.PresentationId,taskId:  $scope.taskId,
                  task_initiate_id:$scope.fd.localVars.taskInitiateId,task_execute_id:$scope.fd.localVars.taskExecuteId};
                  $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
                  $scope.fd.localVars.LocalId=$scope.fd.localVars.localWidgetId;
                  $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
                  
                  
                  $scope.fd.localVars.showMenu = false
                  
                   $scope.fd.localVars.toggleDropdown = function(){
                       $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                   }
                   
                   
                    $scope.fd.localVars.setField = function(form,name,value,msg)
                    {
                                TaskDataService.setField(form,name,value);
                        if (msg) MessagingService.addMessage(msg,'info');
                    };
                    
                    $scope.ksm=KatalyoStateManager
                    
                     /**********************************************************************
                     ****************** Sync data with State manager **********************
                     ***********************************************************************/
                    let dependent_field_pid = $scope.fd.LocalId;
                    let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
                    let dependent_field_pid2
                    let update_presentation_id2 = function(data) {
                        let presentation_id = angular.copy($scope.fd.PresentationId)
                        if (data[presentation_id]!==undefined)
                        {
                            $scope.fd.PresentationId = data[presentation_id];
                        //unsubscribe here later after implementing unsubscribe - ToDo
                        dependent_field_pid2 = $scope.fd.PresentationId
                        let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                        }
                    }
                    let update_presentation_id = function(data) {
                        if (data[$scope.fd.LocalId]>0 && ($scope.fd.PresentationId==0 ||  $scope.fd.PresentationId==null)  && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
                      
                        let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                        //unsubscribe here later after implementing unsubscribe - ToDo
                    }
                    
                    
                    if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0)
                    {
                        let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
                    } else{
                        dependent_field_pid2 = $scope.fd.PresentationId
                        let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                    }
        

                $scope.gsrvc= GridsterService;

                ResourcesDataService.getResourceDefListByType($rootScope.globals.currentUser.lang,'file').then(
                        function(data) {            
                            $scope.fd.localVars.listOfFileTypes = data.data;
                         }
                        ,function(error){   MessagingService.addMessage(error.msg,'error')  }
                );

                $scope.fd.downloadImage = function(fileDefId,fileId){
                ResourcesDataService.downloadFile(fileDefId,fileId).then(function(data) {
                               
                                   
                                    var url = URL.createObjectURL(new Blob([data.data]));
                                    $scope.fd.ImageUrl=url
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// dowload image END

      if ($scope.fd.Parameters.FileDefId?.id>0 && $scope.fd.Parameters.FileId>0) $scope.fd.downloadImage($scope.fd.Parameters.FileDefId?.id,$scope.fd.Parameters.FileId);
               },
             templateUrl: 'ktdirectivetemplates/ktImage.html',
             link: function (scope, elem, attrs) {
              
             
               
            }
        };
};