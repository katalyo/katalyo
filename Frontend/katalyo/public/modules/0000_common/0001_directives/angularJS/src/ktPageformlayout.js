'use strict';

export function ktPageformlayout () {
        return {
            restrict: 'A',
            
			controller: function ($scope,dragulaService,$rootScope,WidgetDataExchangeService,GridsterService,$compile,$element,$templateCache) {
                    
                $scope.CTRL="ktPageformlayout"
                
                let deletedWidgetsState= $scope.ksm.addSubject('deletedWidgetsState'+$scope.resourceDefinition.id+$scope.resourceDefinition.form_type,null);
                let deletedWidgetsStateName = deletedWidgetsState.subject.name;
                
                let add_deleted = function(data){

                    let deleted = deletedWidgetsState.subject.getState
                    if (deleted===undefined || deleted===null) deleted=[]            
                    deleted.push(data)
                    deletedWidgetsState.subject.setStateChanges = deleted;
                }            

                let action_value_changed = $scope.ksm.addAction(deletedWidgetsStateName,"addDeleted",add_deleted);
				//let action_value_changed = $scope.ksm.addAction(deletedWidgetsState,"addDeleted",add_deleted);
                
                let process_deleted_widgets=function(){

                    let deletedw = deletedWidgetsState.subject.getState
                    if (deletedw!==null)
                    {
                        for (let i=0;i<deletedw.length;i++)
                        {
                           for (let j=0;j<$scope.inputArray.layout.length;j++)
                           {
                             if ($scope.inputArray.layout[j].Status=="Deleted" && $scope.inputArray.layout[j].PresentationId===deletedw[i].PresentationId) $scope.inputArray.layout.splice(j,1)  
                           }
                           
                        }
                    }
                }
                let subscribedSubject = "formDefinitionState"+$scope.resourceDefinition.id+$scope.resourceDefinition.form_type;
                let dependent_field = $scope.inputArray.PresentationId;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"formSaved",process_deleted_widgets);

                

                $scope.removeField = function (index){
						// console.log("REMOVE FIELD "+index);
						if (index > -1) {
                        
                            WidgetDataExchangeService.deRegisterWidget($scope.inputArray.layout[index].LocalId);
                            if ($scope.inputArray.layout[index].Saved==true && $scope.inputArray.layout[index].PresentationId!=null && $scope.inputArray.layout[index].PresentationId!=undefined) {
								
                                 $scope.inputArray.layout[index].Status="Deleted";
                                 //subscribe to save form event

                                 $scope.ksm.executeAction(deletedWidgetsStateName,"addDeleted",{PresentationId:$scope.inputArray.layout[index].PresentationId,idx:index})
							 }else
                             {
                                $scope.inputArray.layout.splice(index,1);
                                
                             }
                             
						}

				};

                $scope.undoDelete = function (index){
                        // console.log("REMOVE FIELD "+index);
                        if (index > -1) {
                        
                                 $scope.inputArray.layout[index].Status="Active";
                        }

                };
                
       
                let template;
               if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktPageFormLayout.html');
               else template = $templateCache.get('ktdirectivetemplates/ktPageFormLayoutExe.html');

                //template = $templateCache.get('ktdirectivetemplates/ktPageFormLayout.html');
          
               $element.append($compile(template)($scope));

			},
			 link: function (scope, elem, attrs) {

                if (scope.inputArray.layout==undefined || scope.inputArray.layout==null) scope.inputArray.layout = [];
          
            }
        };
};