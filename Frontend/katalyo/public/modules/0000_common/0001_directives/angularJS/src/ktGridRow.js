'use strict';

export function ktGridRow () {
        return {
            restrict: 'A',
              scope:{
                   row : '=',
                   allRows: "=",
                   parent: "=",
                   gridType: '=',
                   formType: '=',
                   resourceDefinition: '=', 
                   formParams:'=',
                   formParamsParent:'=',
                   showSearchFilters:'=',
                   parentResourceDefId: '=',
                   resourceRecordId: '=',
                   resourceFormLoaded : '=',
                   formData : '=',  
      			},
             controller: ['$scope','GridsterService','WidgetDataExchangeService','$timeout','dragulaService','$rootScope','KatalyoStateManager','$compile','$element','$templateCache',
                          function($scope,GridsterService,WidgetDataExchangeService,$timeout,dragulaService,$rootScope,KatalyoStateManager,$compile,$element,$templateCache)
                {
                $scope.CTRL = 'ktGridRow';
                $scope.gsrvc = GridsterService;
                $scope.ksm = KatalyoStateManager;
                let originalRow = angular.copy($scope.row); // TODO - Zasto je ovo tu ako se ne koristi??? MIGOR
                let rowId = $scope.gridType+'_'+$scope.resourceDefinition.ResourceDefId_id+'_'+$scope.row.ElementType+'_'+$scope.row.ContainerId+'_'+$scope.row.id;
                let formChangesSubject = 'datasetFormState'+$scope.resourceDefinition.ResourceDefId_id;
                
                var widgetDefinition = {name: 'row',widgetId: $scope.row.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId};
                $scope.row.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
                $scope.row.LocalId=$scope.row.localWidgetId;
             
               
                /**********************************************************************
                 ****************** Sync data with State manager **********************
                 ***********************************************************************/
                let dependent_field_pid = $scope.row.LocalId;
                let resource_id = $scope.resourceDefinition.ResourceDefId
                let subscribedSubjectPid = "formDefinitionState"+resource_id+$scope.resourceDefinition.form_type;
                let dependent_field_pid2
                let update_presentation_id2 = function(data) {
                    let presentation_id = angular.copy($scope.row.PresentationId)
                    if (data[presentation_id]!==undefined)
                    {
                        $scope.row.PresentationId = data[presentation_id];
                    //unsubscribe here later after implementing unsubscribe - ToDo
                    dependent_field_pid2 = $scope.row.PresentationId
                    let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                    }
                }
                let update_presentation_id = function(data) {
                    if (data[$scope.row.LocalId]>0 && $scope.row.PresentationId==0 && data[$scope.row.LocalId]!==undefined) $scope.row.PresentationId = data[$scope.row.LocalId];
                  
                    let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                    //unsubscribe here later after implementing unsubscribe - ToDo
                }
                
                
                if ($scope.row.PresentationId===null || $scope.row.PresentationId===0)
                {
                    let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
                } else{
                    dependent_field_pid2 = $scope.row.PresentationId
                    let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                }
        

                $scope.isFormViewTypeEqual = function(what) {
                        let gsrvcType= $scope.gsrvc.getFormViewType()  
                        if ( gsrvcType == what) return true;
                        return false;
                        
                }
                
                
                
                  $scope.oldprintRow = function(pid) {
                                  
                        let x=document.getElementById('row'+pid)
                        x.classList.add("printme");
                        
                        var doc = document.implementation.createHTMLDocument('My title');  
                        doc.head.innerHTML+='  <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/master.css" /> '                   
                        doc.head.innerHTML+=' <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/main.css" />'
                        doc.body.innerHTML+='<div id="content">TEST</div>'

                        var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
                       // var win = window.open("", "_blank", strWindowFeatures);
                        var win = window.open("", "_blank", strWindowFeatures);
                        
                        //win.document.open();
                        //win.document.write('<!DOCTYPE html> <html><head> <style> @page { size: auto !important;} </style> <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/master.css" />   <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/main.css" />   </head><body><div id="content"></div></body>  </html> ');
                        //win.document.close();
                       
                        var loadingscreen = document.getElementById('loadingscreen')
                        if (loadingscreen) loadingscreen.parentNode.removeChild(loadingscreen); 
                        
                        var gridloading= document.getElementById('gridloading')   
                        if (gridloading) gridloading.parentNode.removeChild(gridloading);   
                         
                        var printContents = document.getElementById('row'+pid)
                        //var com = $compile(printContents)($scope);
                        
                        htmlTree(printContents)  
                                             
                        win.document.body.innerHTML = printContents.innerHTML;

                        $timeout(function() {    
//                        win.print();     
  }); 
                      
                }

                $scope.printRow = function(pid) {
                                  
                        let x=document.getElementById('row'+pid)
                        x.classList.add("printme");
                        
                        var doc = document.implementation.createHTMLDocument('My title');  
                        doc.head.innerHTML+='  <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/master.css" /> '                   
                        doc.head.innerHTML+=' <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/main.css" />'
                        doc.body.innerHTML+='<div id="content">TEST</div>'

                        //var URL = "https://localhost:8080;url=" + location.href;
                        var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
                        //var win = window.open("", "_blank", strWindowFeatures);
                        
                       // win.document.open();
                        //win.document.write('<!DOCTYPE html> <html><head> <style> @page { size: auto !important;} </style> <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/master.css" />   <link rel="stylesheet" type="text/css" href="/static/modules/styles/build/css/main.css" />   </head><body><div id="content"></div></body>  </html> ');
                        //win.document.close();
                       
                        var loadingscreen = document.getElementById('loadingscreen')
                        if (loadingscreen) loadingscreen.parentNode.removeChild(loadingscreen); 
                        
                        var gridloading= document.getElementById('gridloading')   
                        if (gridloading) gridloading.parentNode.removeChild(gridloading);   
                         
                        //var printContents = document.getElementById('row'+pid)
                           
                        //var com = $compile(printContents)($scope);
                        //win.document.body.innerHTML = com[0].innerHTML
                        
                        //win.document.body.innerHTML = printContents.innerHTML;

                        $timeout(function() {        window.print();            }); 
                            
                            
                       // win.document = document.getElementById('row'+pid).cloneNode(true);
        
                }
                
                //define fn to sync to manager 
                let process_undo = function(data,action)  {
                    if (data!==undefined) {
                        let rowData = data.oldData;
                        if (data.id===rowId && action==="undoDatasetFormChange")    {
                            $scope.row = rowData;
                            $scope.ksm.executeAction(formChangesSubject,"undoDatasetFormChangeDone");    
                            $scope.syncFromObserver=true;
                        }
                    }
                };
                
                //define fn to sync to manager 
                let process_redo = function(data,action) {
                    if (data!==undefined)  {
                        let rowData = data.newData;
                        if (data.id===rowId && action==="redoDatasetFormChange") {
                                $scope.row = rowData;
                                $scope.syncFromObserver=true;
                        }
                    }
                };
                
                 //create observer if change comes from someplace else (e.g. undo or from server)
                let observer_undo = $scope.ksm.subscribe(formChangesSubject,rowId,"undoDatasetFormChange",process_undo);
                 //create observer if change comes from someplace else (e.g. undo or from server)
                let observer_redo = $scope.ksm.subscribe(formChangesSubject,rowId,"redoDatasetFormChange",process_redo);
                
                $scope.addColumn = function (row){
                    row.layout.push($scope.gsrvc.getEmptyColumnWithId($scope.parent.layout));  
                };
                $scope.addRowAbove = function (row){
                    $scope.parent.layout.splice($scope.parent.layout.indexOf(row),0, GridsterService.getEmptyRowWithId($scope.allRows));
                };
              
                $scope.addRowBelow = function (row){
                    $scope.parent.layout.splice($scope.parent.layout.indexOf(row)+1,0, GridsterService.getEmptyRowWithId($scope.allRows));
                };
                $scope.getClass = function(size) {     
                    return "col-lg-"+size;     
                };
              
                $scope.getHandle = function(){
                    return "row-"+$scope.gridType+"-handle";     
                };
              
                $scope.setHandle = function (type){
                        return type+"-handle";
                };
                if ($scope.row.Parameters==undefined) $scope.row.Parameters={};
                if ($scope.row.Parameters.BackgroundOpacity==null || $scope.row.Parameters.BackgroundOpacity==undefined ) $scope.row.Parameters.BackgroundOpacity=1;
                
                $scope.row.BackgroundColorChanged = function (){
                        
                        if ($scope.row.Parameters.BackgroundColor.length>0) $scope.row.Parameters.BackgroundColorA = "rgba(" + parseInt($scope.row.Parameters.BackgroundColor.slice(-6, -4), 16) + "," + parseInt($scope.row.Parameters.BackgroundColor.slice(-4, -2), 16) + "," + parseInt($scope.row.Parameters.BackgroundColor.slice(-2), 16) + "," + $scope.row.Parameters.BackgroundOpacity + ")";
       
                };
                
                $scope.row.BackRepeatChanged = function (){       
                        if ($scope.row.Parameters.RepeatBackImg) $scope.row.Parameters.RepeatBackText = 'repeat';
                        else $scope.row.Parameters.RepeatBackText = 'no-repeat';
                };
                 
                $scope.$watch('row',function(newValue,oldValue){
               
                    if (newValue!==oldValue && !$scope.syncFromObserver)   {
                         $scope.ksm.executeAction(formChangesSubject,"addDatasetFormChange",{'id':rowId,'data':angular.copy(oldValue)});
                    } else $scope.syncFromObserver=false;              
              
                });
                
            //}
            
            var widgetHandle = $scope.setHandle($scope.gridType+'-column-drag');
            
            var bag = dragulaService.find($scope, 'bag-columns-'+$scope.gridType);
             
            if (bag) {
                dragulaService.destroy($scope, 'bag-columns-'+$scope.gridType);
            }
            dragulaService.options($scope, 'bag-columns-'+$scope.gridType, {
                    revertOnSpill: true,
                    removeOnSpill: false,
                    direction:'vertical',
                    accept:function (el,target, source) {
                              return false;
                    },
                    moves: function (el, container, handle) {
                              return handle.classList.contains(widgetHandle);
                    }
             });

             $scope.row.downloadImage = function(fileDefId,fileId){
                ResourcesDataService.downloadFile(fileDefId,fileId).then(function(data) {
                               
                                   
                                    var url = URL.createObjectURL(new Blob([data.data]));
                                    $scope.row.Parameters.BackgroundImageUrl=url
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
            };// dowload image END

            if ($scope.row.Parameters.FileDefId?.id>0 && $scope.row.Parameters.FileId>0) $scope.row.downloadImage($scope.row.Parameters.FileDefId?.id,$scope.row.Parameters.FileId);

                
            let template;
            if ($scope.resourceDefinition.form_definition)    {
                template = $templateCache.get('ktdirectivetemplates/ktGridRow.html');
            }
                else template = $templateCache.get('ktdirectivetemplates/ktGridRowExe.html');
          
            $element.append($compile(template)($scope));  
                 
            }],
             
             link: function (scope, elem, attrs) {
              

          //  if (scope.formParams.formViewType!==2 && (scope.formParamsParent.formViewType!==2 || scope.formParamsParent===undefined))           
          //  {
          
          /*
                scope.$watch('row.layout',function(newValue,oldValue){
               
                  //process object change
                  if (newValue.length!==oldValue.length && !scope.syncFromObserver)
                  {
                    scope.ksm.executeAction(formChangesSubject,"addDatasetFormChange",{'id':rowId,'data':angular.copy(oldValue)});
                  } else scope.syncFromObserver=false;              
              
                },true);
            */
             
            }
        };
    };
    
    
    
export   function    htmlTree (obj){
        var obj = obj || document.getElementsByTagName('body')[0];
        var str=""
        
        
        if (obj.tagName == 'INPUT')  {
            if (obj.value) obj.setAttribute("value", obj.value); 
           // output.push(obj);
            }
        if (obj.hasChildNodes()) {
          var child = obj.firstChild;
          while (child) {
            if (child.nodeType === 1 && child.nodeName != 'SCRIPT'){
              str += htmlTree(child) + ".";
            }
            child = child.nextSibling;
          }
        }
        
        if (obj.classList.contains('noprint' )) 
        {  
        obj.remove()
        }
        return "";
      }