'use strict';

export function ktTaskViewPdf () {
   
    var controller = ['$scope', '$uibModal','$stateParams','$timeout','CodesService','ResourcesDataService','$rootScope','ProcessingService','WidgetDataExchangeService','GridsterService','MessagingService','CODES_CONST','TaskDataService','KatalyoStateManager',
                      function ($scope,$uibModal,$stateParams,$timeout,CodesService,ResourcesDataService,$rootScope,ProcessingService,WidgetDataExchangeService,GridsterService,MessagingService,CODES_CONST,TaskDataService,KatalyoStateManager) {
      
      
    
    $scope.localVars={};
    $scope.gsrvc=GridsterService;
    $scope.localVars.datasetMapperType=0;
    $scope.fd.initialLoad=true;
    $scope.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_; 
    $scope.localVars.searchFormDownloaded = false;
    $scope.localVars.showSearchFilters=true;
    $scope.localVars.resourceDataDownloaded = false;
    $scope.localVars.selectedItems=[];
    $scope.taskId=$scope.resourceDefinition.ResourceDefId;
    if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
    if ($scope.taskId==undefined) $scope.taskId=0
    $scope.localVars.taskId=$scope.taskId;
    $scope.localVars.widgetType = 4;
    $scope.localVars.formParams={formViewType:1,showGridNavigation:true};
    $scope.localVars.cardLayoutParams={formViewType:1,showGridNavigation:true};
    //$scope.gsrvc.setFormViewType($scope.localVars.formParams={formViewType));
    $scope.localVars.parentDatasetDefId=$scope.fd.Related;
    $scope.localVars.animationsEnabled = true;
    if ($scope.fd.Parameters==undefined) $scope.fd.Parameters = {PresentationType:'table'};
    if ($scope.fd.Parameters.PresentationType==undefined || $scope.fd.Parameters.PresentationType==null) $scope.fd.Parameters.PresentationType='table';
    $scope.localVars.gridsterType="resource";
    $scope.itemValue = {val:null};
    $scope.localVars.isCollapsed = $scope.fd.Parameters.SearchFormCollapsed;
    if ($scope.localVars.isCollapsed==undefined) $scope.localVars.isCollapsed =true;
    if ($scope.fd.AutoSearch) $scope.localVars.resourceDataDownloading = true;

     $scope.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
        
         $scope.localVars.dashboardCard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
       $scope.localVars.gridOptionsResourceData = {
           //  columnDefs: colDefs,
        //enableGridMenu: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: $scope.fd.Multiline,
      //  enableHorizontalScrollbar : 1,
       // enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: false,
        enableColumnResizing: true,
        enableColumnReordering: true,
        //useExternalPagination: false,
        paginationPageSizes: [10, 25, 50,100,200,500,1000,5000,10000],
       // enablePaginationControls: true,
      };

  var paginationOptions = {
    pageNumber: 1,
    pageSize: 10,
    sort: null
    };
    
    
       $scope.localVars.gridOptionsRelated = {
      //  columnDefs: colDefs,
       enableGridMenu: true,
       showGridFooter: true,
       showColumnFooter: true,
       enableRowSelection: false,
       multiSelect: false,
       enableHorizontalScrollbar : 2, 
       nableVerticalScrollbar : 2,
       enableFiltering: true,
       enableRowHeaderSelection: true,
       enableColumnResizing: true,
       minRowsToShow: 2,
      
   }
    
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        
        //$scope.fd.PopulateType=1;
        $scope.fd.ParentDatasetDefId=$scope.fd.Related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.localVars.formType=$scope.resourceDefinition.form_type;
        $scope.localVars.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        $scope.formDefinition = $scope.resourceDefinition.form_definition;
        if  ($scope.formDefinition==undefined)
        {
          
          $scope.formDefinition = false;
        }
        
        
        
        
          if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        
        $scope.localVars.resourceRecordId = 0;
        $scope.fd.ParentElementId = 0;
      
      var out_fields = ['resourceRecordId'];
      //resourceLink
      var widgetDefinition = {
            DEI: [{'name':'resourceLinkDatasetList','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],
            actions: [{name:'PrepareData',status:false}],
            name: 'datasetListWidget',
            widgetId: $scope.fd.PresentationId,
            taskId:  $scope.lTaskId,
            resourceDefId : $scope.fd.Related, //resourceDefId : $scope.selectedResource
            resourceRecordId:$scope.localVars.resourceRecordId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId, output:out_fields 
      };
      $scope.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
      $scope.localVars.LocalId=$scope.localVars.localWidgetId;
      $scope.fd.LocalId=$scope.localVars.localWidgetId;
          
       
         /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
        
        let dependent_field_pid = $scope.fd.LocalId;
        let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
        let dependent_field_pid2
        let update_presentation_id2 = function(data) {
            let presentation_id = angular.copy($scope.fd.PresentationId)
            if (data[presentation_id]!==undefined)
            {
                $scope.fd.PresentationId = data[presentation_id];
            //unsubscribe here later after implementing unsubscribe - ToDo
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            }
        }
        let update_presentation_id = function(data) {
            if (data[$scope.fd.LocalId]>0 && $scope.fd.PresentationId==0 && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
          
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            //unsubscribe here later after implementing unsubscribe - ToDo
        }
        
        
        if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0)
        {
            let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
        } else{
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
        }
        
        if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
       
        
        let valueState= $scope.ksm.addSubject($scope.fd.ElementType+"ValueStateName"+$scope.fd.PresentationId+$scope.resourceDefinition.transaction_id,$scope.itemValue.val);
        let valueStateName = valueState.subject.name;
        
        let value_changed = function(data)
        {
            valueState.subject.setStateChanges = data;
            valueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
        let apply_value = function(data)
        {
            $scope.itemValue.val = data;
        }; 
       
        if ( $scope.fd.Parameters.DependentFieldId!=undefined     && $scope.fd.Parameters.DependentFieldId!=null     && $scope.fd.Parameters.DependentFieldId!="" )
            {
                let dependent_field = "Field"+$scope.fd.Parameters.DependentFieldId;
                let subscribedSubject = $scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            }
 
        
           $scope.localVars.showProperties = function(datasetMapperType)  {
            
           // if ($scope.localVars.datasetMapperType==undefined)
           // {
           //    $scope.localVars.datasetMapperType = datasetMapperType;
           // }else
           // {
           //    if ($scope.localVars.datasetMapperType==0 || $scope.localVars.datasetMapperType!=datasetMapperType) $scope.localVars.datasetMapperType = datasetMapperType;
           //    else $scope.localVars.datasetMapperType=0;
           // } 
           
                $scope.localVars.datasetMapperType = (datasetMapperType == $scope.localVars.datasetMapperType) ? 0: datasetMapperType;
            
           };
           
      $scope.localVars.gridOptionsResourceData.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.localVars.gridApi = gridApi;
         if ($scope.restoreGridState) $scope.localVars.gridApi.saveState.restore($scope,$scope.fd.GridState);
          gridApi.selection.on.rowSelectionChanged($scope,function(row){

            $scope.localVars.datasetRecordSelected=row.isSelected;
            if ($scope.localVars.datasetRecordSelected)
              {
                if (row.entity.id!=$scope.localVars.selectedDatasetRecordId)
                {
                  $scope.localVars.selectedDatasetRecordId = row.entity.id;
                }
              }else
              {
                $scope.localVars.selectedDatasetRecordId = 0;
                
              }
              $scope.localVars.gridOptionsRelated.columnDefs=[];
              $scope.localVars.gridOptionsRelated.data=[];
              $scope.localVars.showGrid=false;
              $scope.localVars.prevTaskPackage ={resourceDefId:$scope.fd.Related,records:gridApi.selection.getSelectedRows()}
              
              
              $scope.localVars.selectedItems = gridApi.selection.getSelectedRows();
                
              //$rootScope.$broadcast('EventresourceLink'+$scope.fd.PresentationId,$scope.selectedDatasetRecordId);
            WidgetDataExchangeService.setData({
                  DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                        ,{name:'resourceRecordIds',value:gridApi.selection.getSelectedRows()},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
              
              
            });
          
            //external pagination
        /*
          gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
            paginationOptions.pageNumber = newPage;
            paginationOptions.pageSize = pageSize;
            $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,newPage,pageSize);
          });
      */
           gridApi.selection.on.rowSelectionChangedBatch($scope,function(row){

              $scope.localVars.prevTaskPackage ={resourceDefId:$scope.fd.Related,records:gridApi.selection.getSelectedRows()}
              
          });
     }
     
        
    let process_form_change = function(data)
    { 
        $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
        if ($scope.fd.Parameters.PresentationType=='table') $scope.localVars.getResourceColumns(true);
        else if ($scope.fd.Parameters.PresentationType=='dropdown') $scope.localVars.getDatasetDataList($scope.fd.Parameters.ClientSideFilter,true);
        else $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,1,$scope.localVars.pageSize);               
                      
    };
    
    
    //subscribe to events - initially only refresh data
   
    let dependent_field2 = "Field"+$scope.fd.PresentationId;
    let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
    let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,process_form_change);

        
     $scope.localVars.ProcessSearchForm=function (form){
            
           if (form!=undefined){
            for(var i=0;i<form.length;i++) {
                
                    if ('ElementType' in form[i])
                    {
                      if (form[i].AutoSearch)
                      {
                        if (form[i].ElementType == 'resource')
                        {
                        var pos = $scope.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(form[i].PresentationId);
                        if (pos<0) $scope.localVars.childWidgets.push({widgetId:form[i].PresentationId,processed:false})
                        }
                        //subscribe to change
                        if (form[i].Parameters!=undefined)
                        {
                            if (form[i].Parameters.DependentFieldId!=undefined && form[i].Parameters.DependentFieldId!=null)
        
                            {
                                if (form[i].Parameters.DependentFieldId!="")
                                {
                                    let dependent_field = "Field"+$scope.fd.PresentationId;
                                    let subscribedSubject = form[i].ElementType+"ValueStateName"+form[i].ItemId+$scope.resourceDefinition.transaction_id;
                                    let subject = $scope.ksm.addSubject(subscribedSubject,null);
                                    let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",process_form_change);
                                }
                            }
                        }
                        
                      }
                      
                     }
                      if ('layout' in form[i])
                      {   
                       $scope.localVars.ProcessSearchForm(form[i].layout);
                      }
              }
          } 
           
      };
      
      $scope.localVars.SearchFormProcessed=false;


       $scope.localVars.setSelectedDataset = function(id)  {
              
              if ($scope.localVars.selectedResourceItem==undefined) $scope.localVars.selectedResourceItem={};
              var pos = $scope.localVars.datasetList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.localVars.selectedResourceItem.id = $scope.localVars.datasetList[pos].id;
                $scope.localVars.selectedResourceItem.name = $scope.localVars.datasetList[pos].name;
                $scope.localVars.selectedDataset = $scope.localVars.datasetList[pos];
                $scope.localVars.selectedResourceItem.name_confirmed = $scope.localVars.selectedResourceItem.name +' ('+$scope.localVars.selectedResourceItem.id +')'
                $scope.localVars.selectedResourceItem_c=$scope.localVars.selectedResourceItem;
                }
					};
          
       // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
           $scope.localVars.getDatasets = function()  {
						
                       if ($scope.localVars.datasetList==undefined) $scope.localVars.datasetList=[];
          
                       var lang=$rootScope.globals.currentUser.lang;
                       ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
                       
                      function(data) {
										
                                 $scope.localVars.datasetList = data.data;
                                  if ($scope.fd.Related!=null)
                                  {
                                   $scope.localVars.setSelectedDataset($scope.fd.Related);
                                   }     		 
                      },function(error){    
                       MessagingService.addMessage(error.msg,'error');
                      });
	};
            
      $scope.localVars.GetDatasetData=function(data)

      {
            fromWidget = data.widgetId;
            var allProcessed=true;
            var pos = $scope.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(fromWidget);

            for (var i=0;i<$scope.localVars.childWidgets.length;i++)
              {
                
                if ($scope.localVars.childWidgets[i].widgetId == fromWidget) $scope.localVars.childWidgets[i].processed = true;
                if (!$scope.localVars.childWidgets[i].processed) allProcessed = $scope.localVars.childWidgets[i].processed;
              }

              if($scope.fd.AutoSearch && allProcessed && $scope.localVars.SearchFormProcessed) {
                    $scope.localVars.selectedResource=$scope.fd.Related;
                    if ($scope.fd.Parameters!=null && $scope.fd.Parameters!=undefined)
                    {
                      if ($scope.fd.Parameters.PresentationType=='table') $scope.localVars.getResourceColumns(true);
                      else if ($scope.fd.Parameters.PresentationType=='dropdown') $scope.localVars.getDatasetDataList($scope.fd.Parameters.ClientSideFilter,false);
                      else $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,1,$scope.localVars.pageSize);               
                         
                    }                   
                }
      };
    
      if ($scope.localVars!=undefined) $scope.localVars.childWidgets = [];
      else $scope.localVars = {childWidgets : []};
      var fromWidget=0;

      let search_form_ready = function(data)
        {
            $scope.localVars.GetDatasetData(data); 
        };
        
        let action_search_form_ready = $scope.ksm.addAction(valueStateName,"SearchFormReady"+$scope.taskExecuteId+$scope.resourceDefinition.transaction_id,search_form_ready);
       
       
   
   //**********************************************************
	//           GET DATASET RELATED RECORDS
	//**********************************************************      
        $scope.localVars.getDatasetRelatedRecords = function (relatedDatasetId,recordId,modelId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataRelated($scope.fd.Related,recordId,relatedDatasetId,0,modelId,lang).then(
                function(data) {       
                                     $scope.localVars.gridOptionsRelated.data = data.data;
                                     $scope.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                                     $scope.localVars.showGrid=true;
                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
        
        
    //**********************************************************
	//           GET DATASET SINGLE RECORD
	//**********************************************************      
      $scope.localVars.getDatasetSingleRecord = function (relatedDatasetId,recordId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataDirectFilter(relatedDatasetId,0,lang,{id: recordId}).then(
         
                    function(data) {        
                                     $scope.localVars.gridOptionsRelated.data = data.data;
                                     $scope.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                               
                    },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
    
        $scope.$on('SearchFormReady'+$scope.taskExecuteId+$scope.fd.PresentationId,function(event, data1){
      
                                        $scope.localVars.GetDatasetData(data1); 
            
        });
        
    $scope.localVars.getFormResource = function(){   
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null) {
                $scope.fd.PresentationId = 0;
              }
               
               
              ResourcesDataService.getResourceFormExtendedWithData($scope.fd.Related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.PopulateType,$scope.fd.ParentElementId,$scope.fd.PresentationId,$scope.prevTaskExeId,$scope.localVars.formType,$scope.localVars.widgetType,$scope.localVars.resourceRecordId,"f",lang).then(function(data) {
                 
                                    $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                
                                    $scope.localVars.selectedResourceItem_c = {'name':$scope.localVars.resourceWidgetDefinition.name,'id':$scope.fd.Related,'relatedId':$scope.fd.Related,'presentationId':$scope.fd.PresentationId,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
                              
                                    $scope.fd.fItemValue=data.data.widgets;

                                    $scope.localVars.searchFormDownloaded = true;
                                    $scope.localVars.setField($scope.fd.fItemValue,'Required',false);
                                    $scope.localVars.formLoaded=true;
                                   //prepare search form
                                    $scope.localVars.ProcessSearchForm($scope.fd.fItemValue);
                                    $scope.localVars.SearchFormProcessed = true;
                                    if ($scope.localVars.childWidgets.length>0)
                                    {
                                      WidgetDataExchangeService.registerSearch({widgetId:$scope.fd.PresentationId,taskExecuteId:$scope.taskExecuteId,prevTaskExeId:$scope.prevTaskExeId,childWidgets:$scope.localVars.childWidgets});
                                    }
                                    else
                                    {
                                       $scope.ksm.executeAction(valueStateName,"SearchFormReady"+$scope.taskExecuteId+$scope.resourceDefinition.transaction_id, {widgetId:$scope.fd.PresentationId});
                                    }
                                    $scope.localVars.dashboard.widgets.layout =  data.data.widgets;
                                    if ($scope.formData == undefined) $scope.formData={};
                                    $scope.formData[$scope.fd.PresentationId] =  data.data.form_data;
                                    $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
                                    if ($scope.fd.Search==undefined) $scope.fd.Search={};
               
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                             });
                              
      };//getFormResource_END
      
    $scope.localVars.executeDatasetList = function(){   
            var lang=$rootScope.globals.currentUser.lang;
            if ($scope.fd.PresentationId==null) {
                $scope.fd.PresentationId = 0;
            }
            if ($scope.localVars.selectedResource==undefined) $scope.localVars.selectedResource = $scope.fd.Related;
            if ($scope.localVars.pageSize==undefined) $scope.localVars.pageSize = paginationOptions.pageSize;
                   
            TaskDataService.ktTaskDatasetList($scope.fd.Related,$scope.fd.PresentationId,$scope.localVars.taskId,$scope.localVars.formType,lang).then(
        
                function(data) {
                 
                                    $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                 
                                    $scope.localVars.gridOptionsResourceData.columnDefs = data.data.columns;
                                    $scope.fd.fItemValue=data.data.widgets;
                                    if (data.data.widgetsCard!=null)
                                    {
                                        $scope.fd.wItemValue=data.data.widgets;
                                        $scope.localVars.dashboardCard.widgets.layout =  data.data.widgetsCard;
                                    }
                                    $scope.localVars.searchFormDownloaded = true;
                                    $scope.localVars.setField($scope.fd.fItemValue,'Required',false);
                                    $scope.localVars.formLoaded=true;
                                   
                                    $scope.localVars.dashboard.widgets.layout =  data.data.widgets;
                                    if ($scope.formData == undefined) $scope.formData={};
                                    $scope.formData[$scope.fd.PresentationId] =  data.data.formData;
                                    $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
                                    if ($scope.fd.Search==undefined) $scope.fd.Search={};
                                    
                                    //setup data grid or card
                                    $scope.localVars.gridOptionsResourceData.data = data.data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')
                                    $scope.localVars.gridOptionsResourceData.data = $scope.TransformDataForCard(data.data.data);
                                    $scope.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.localVars.resourceDataDownloaded = true;
                                    $scope.localVars.resourceDataDownloading = false;
                                    $scope.localVars.nextPage = data.data.data.next;
                                    $scope.localVars.previousPage = data.data.data.previous;
                                    $scope.localVars.totalCount=data.data.data.count;
                                    $scope.localVars.currentPage=1;
                                    if ($scope.localVars.totalCount>0 && $scope.localVars.currentPage==0) $scope.localVars.currentPage=1;
                                    $scope.localVars.numPages = Math.ceil($scope.localVars.totalCount/$scope.localVars.pageSize);
                                    
                                    $scope.localVars.gridOptionsResourceData.totalItems=$scope.localVars.totalCount;
               
                 },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                             });
                              
      };//getFormResource_END

      
    
  $scope.localVars.getFormResourceOriginal = function(){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null)
              {
                $scope.fd.PresentationId = 0;
                
              }
              ResourcesDataService.getResourceForm($scope.fd.Related,"i",lang).then(
               
                function(data) {
                            
                                $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                $scope.localVars.dashboard.widgets.layout=data.data.widgets;
                                 $scope.localVars.selectedResourceItem_c = {'name':$scope.localVars.resourceWidgetDefinition.name,'id':$scope.fd.Related,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
                            
                             
                                $scope.fd.fItemValue=data.data.widgets;
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.resourceRecordId}]}]});
						
                                 $scope.localVars.searchFormDownloaded = true;
                                 $timeout(function() {
                                            $scope.localVars.formLoaded=true;
                                 });
           
                                 if ($scope.formData == undefined) $scope.formData={};
                                 $scope.formData[$scope.fd.PresentationId] = [{}];
                                 $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
                            },
                 function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
   
     $scope.localVars.getDatasetCardLayoutOriginal = function(){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null)
              {
                $scope.fd.PresentationId = 0;
                
              }
              ResourcesDataService.getResourceForm($scope.fd.Related,"i",lang).then(function(data) {
                            
                     $scope.localVars.resourceDefinitionCard= data.data.resourceDefinition;
                     $scope.localVars.dashboardCard.widgets.layout=data.data.widgets;
                     
                     $scope.fd.wItemValue=data.data.widgets;
                      
                      $timeout(function() {
                                $scope.localVars.cardLayoutLoaded = true;
                          
                      });
           
                      if ($scope.formDataCard == undefined) $scope.formDataCard={};
                         
               }, function(error){
                        MessagingService.addMessage(error.msg,'error');
                  });
                              
      };
      
      
     $scope.localVars.getCardLayout = function(){   
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null) {
                $scope.fd.PresentationId = 0;
              }
                
              ResourcesDataService.getResourceFormExtendedWithData($scope.fd.Related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.PopulateType,$scope.fd.ParentElementId,$scope.fd.PresentationId,$scope.prevTaskExeId,$scope.localVars.formType,$scope.localVars.widgetType,$scope.localVars.resourceRecordId,"w",lang).then(function(data) {
                 
                                    $scope.localVars.resourceDefinitionCard= data.data.resourceDefinition;
                               
                                    $scope.fd.wItemValue=data.data.widgets;
                                    $scope.localVars.dashboardCard.widgets.layout =  data.data.widgets;
                                    
                                    $scope.formDataCard =  data.data.form_data;
                                    if ($scope.formDataCard == undefined) $scope.formDataCard={};
                                   
                                    $timeout(function() {
                                           $scope.localVars.cardLayoutLoaded = true;
                                     
                                    });
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                             });
                              
      };//getCardlayout_END

     $scope.localVars.selectCardItem = function(index){
        let value;
        if ($scope.localVars.selectedCardIndex!=index)
        {
            $scope.localVars.selectedCardIndex=index;
            value=$scope.localVars.gridOptionsResourceData.data[$scope.localVars.selectedCardIndex][0];
        }
        else
        {
            $scope.localVars.selectedCardIndex=null;
            value = null;
        }
        //send data to package
         WidgetDataExchangeService.setData({
                  DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                        ,{name:'resourceRecordIds',value:value},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
     };
      
   $scope.localVars.datasetRecordSelected = false;
   $scope.localVars.selectedDatasetRecordId = 0;
 
    $scope.localVars.restoreGridState= function() {
        if ($scope.fd.GridState!=null)
        {
           if ($scope.localVars.gridApi!=undefined)
           {
            $scope.localVars.gridApi.saveState.restore($scope,$scope.fd.GridState);
           }
           else
           {
            $scope.restoreGridState=true;
           }
        }
    };
      $scope.localVars.getResourceData = function () {

        var lang = $rootScope.globals.currentUser.lang;
        $scope.localVars.resourceDataDownloading = true;
        if ($scope.localVars.pageSize==undefined) $scope.localVars.pageSize = paginationOptions.pageSize;
        ResourcesDataService.getResourceData($scope.localVars.selectedResource,$scope.taskId,$scope.localVars.formType,lang).then(function(data) {
                               
                                     $scope.localVars.gridOptionsResourceData.data = data.data;
                                     $scope.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                                     $scope.localVars.restoreGridState();
                                     $scope.fd.initialLoad = false;
                                     $scope.localVars.resourceDataDownloaded = true;
                                     $scope.localVars.resourceDataDownloading = false;
                                    
                                 },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                   $scope.localVars.resourceDataDownloading = false;
                             });
        };

    //**********************************************************
    //           GET FORM FIELDS
    //**********************************************************      
            
          $scope.localVars.getFormFields = function(){
              
              var lang=$rootScope.globals.currentUser.lang;
                      
              ResourcesDataService.getFormFields($scope.fd.Related,lang).then(function(data) {
                               
                                  $scope.localVars.PresentationElements= data.data;
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 
                             });
                              
          };
          
          
          $scope.$watch('itemValue.val',function(newValue,oldValue){
                  if (newValue!=oldValue)
                  {
                   //send data to package
                   let newValueSingle,newValueMultiple;
                   if ($scope.fd.Multiline)
                   {
                    newValueMultiple = newValue;
                    newValueSingle="";
                   }
                   else
                   {
                    newValueMultiple = {};
                    newValueSingle=newValue;
                   }
                   
                   $scope.ksm.executeAction(valueStateName,"valueChanged", newValue);
                   
                    WidgetDataExchangeService.setData({
                    DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:newValueSingle}
                        ,{name:'resourceRecordIds',value:newValueMultiple},{name:'resourceRecordsAll',value:$scope.localVars.datasetItems}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
           
                  }
      });
        
       
       //**********************************************************
      //           GET DATASET DATA FOR DROPDOWN
      //**********************************************************
      var refresh_list_from_callback=function(data)
            {
                $scope.localVars.getDatasetDataList(true);  
            }
        $scope.filterOnClient = function(items)
        {
            let temp=[];
            let search;
            if (Array.isArray($scope.fd.Search) && $scope.fd.Search.length>0) search = $scope.fd.Search[0];
            else search = $scope.fd.Search;
            for (let i=0;i<items.length;i++)
            {
                for (let key in search) {
                         
                    if (search[key]!=undefined)
                    {
                        if (key in items[i])
                        {
                            if (items[i][key]==search[key].id || search[key]==null || search[key]==undefined || items[i][key]==undefined)  temp.push(items[i]);
                        }else if (key in items[i])
                        {
                            if (items[i][key]==search[key] || search[key]==null || search[key]==undefined || items[i][key]==undefined)  temp.push(items[i]);
                        }else temp.push(items[i]);
                    }else temp.push(items[i]);
                }
                
                
            }
            return temp;
            
        }
        $scope.localVars.getDatasetDataList = function(fromCache,clientSideFilter)
        {
                    if (fromCache==undefined) fromCache=true;
                    if (clientSideFilter==undefined) clientSideFilter=false;
                    var lang = $rootScope.globals.currentUser.lang;
                    $scope.localVars.datasetItems = null;
                    ResourcesDataService.getResourceDataListAll($scope.fd.Related,$scope.fd.PresentationId,lang,fromCache,refresh_list_from_callback).then(function(data) {
                   
                        if (clientSideFilter) $scope.localVars.datasetItems = $scope.filterOnClient(data.data);
                        else $scope.localVars.datasetItems = data.data;
                        $timeout(function(){
                            
                            if ($scope.itemValue.val!=undefined)
                            {
                            let pos = $scope.localVars.datasetItems.map(function(e) { return e.id; }).indexOf($scope.itemValue.val.id);
                  
                                if (pos<0){
                                    $scope.itemValue.val = null;
                                }
                            }else
                            
                            {
                                $scope.itemValue.val = null;
                            }
                        });
                        
                    },function(error){    
                        MessagingService.addMessage(error.msg,'error');
                    });
              
            
        };
        
        $scope.localVars.GetDatasetDataClick = function()
        {
            if ($scope.fd.Parameters.PresentationType=='dropdown')
            {
               $scope.localVars.getDatasetDataList(); 
            }else
            {
                $scope.localVars.selectedResource=$scope.fd.Related;
                $scope.localVars.getResourceColumns(true);
            }
        };    
           
	$scope.TransformDataForCard = function(data)
    {
        let newData = [];
        let newElement = [];
        for (let i=0;i<data.length;i++)
        {
        
        newElement.push(data[i]);
        newData.push(newElement);
        
        newElement = [];
        }
        return newData; 
    };
      //**********************************************************
      //           GET RESOURCE DATA PAGINATED
      //**********************************************************      
    $scope.localVars.getResourceDataPaginated = function (urlNext,urlPrev,newPage,pageSize) {
        
        
        $scope.localVars.resourceDataDownloaded = false;        
        if (Math.ceil($scope.localVars.totalCount/pageSize)<newPage)
        {
          newPage = Math.ceil($scope.localVars.totalCount/pageSize);
        }
         
         var lang = $rootScope.globals.currentUser.lang;
          
         if (pageSize==undefined)
         
         {
          $scope.localVars.pageSize = paginationOptions.pageSize;
          pageSize= $scope.localVars.pageSize;
         }
         $scope.localVars.selectedItems.length=0;
         if ($scope.fd.PopulateMethod=='M')
         {
           $scope.localVars.resourceDataDownloading = true;
          
          ResourcesDataService.getResourceDataPaginatedFilter($scope.localVars.selectedResource,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.Search, $scope.localVars.dashboard.widgets.layout).then(function(data) {
                                    
                                    
                                    $scope.localVars.gridOptionsResourceData.data = data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')
                                    $scope.localVars.gridOptionsResourceData.data = $scope.TransformDataForCard(data.data);
                                    $scope.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.localVars.resourceDataDownloaded = true;
                                    $scope.localVars.resourceDataDownloading = false;
                                    $scope.localVars.nextPage = data.data.next;
                                    $scope.localVars.previousPage = data.data.previous;
                                    $scope.localVars.totalCount=data.data.count;
                                    $scope.localVars.currentPage=newPage;
                                    if ($scope.localVars.totalCount>0 && $scope.localVars.currentPage==0) $scope.localVars.currentPage=1;
                                    $scope.localVars.numPages = Math.ceil($scope.localVars.totalCount/pageSize);
                                    $scope.localVars.pageSize = pageSize;
                                    $scope.localVars.gridOptionsResourceData.totalItems=$scope.localVars.totalCount;
                                    
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.PresentationId+"Completed",$scope.localVars.gridOptionsResourceData.data);
                                   
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.localVars.resourceDataDownloading = false;
                             });
         }
         else if ($scope.fd.PopulateMethod=='A')
         {
          var task_instance=0;  
          if ($scope.localVars.formType=='i') task_instance = $scope.taskInitiateId;
          else task_instance = $scope.taskExecuteId;
           $scope.localVars.resourceDataDownloading = true;
          ResourcesDataService.getResourceDataPaginatedAuto($scope.localVars.selectedResource,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.PresentationId,task_instance,$scope.localVars.formType).then(function(data) {
                                      
                                    $scope.localVars.gridOptionsResourceData.data = data.data;
                                    $scope.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.localVars.resourceDataDownloaded = true;
                                    $scope.localVars.resourceDataDownloading = false;
                                    $scope.localVars.nextPage = data.data.next;
                                    $scope.localVars.previousPage = data.data.previous;
                                    $scope.localVars.totalCount=data.data.count;
                                    $scope.localVars.currentPage=newPage;
                                    $scope.localVars.numPages = Math.ceil($scope.localVars.totalCount/pageSize);
                                    $scope.localVars.pageSize = pageSize;
                                    $scope.localVars.gridOptionsResourceData.totalItems=$scope.localVars.totalCount;
                                    
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLink',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.PresentationId+"Completed",$scope.localVars.gridOptionsResourceData.data);
                                   
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.localVars.resourceDataDownloading = false;
                             });
          
         }
        };


    $scope.localVars.pageChanged = function(){ 
        $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,$scope.localVars.currentPage,$scope.localVars.pageSize);
         };
         
   //**********************************************************
	//           GET RESOURCE COLUMNS
	//**********************************************************      
  $scope.localVars.getResourceColumns = function (getData) {
     var lang = $rootScope.globals.currentUser.lang;
     if ($scope.localVars.selectedResource==undefined) $scope.localVars.selectedResource = $scope.fd.Related;
     
     if ($scope.localVars.selectedResource!=undefined && $scope.localVars.selectedResource!=null)
     {
           taskId = $scope.taskId;
           if ($scope.fd.PresentationId==undefined || $scope.fd.PresentationId==null || $scope.fd.PresentationId==0)
           {
              var taskId = 0;        
           }
      $scope.localVars.resourceDataDownloading = true;

     ResourcesDataService.getResourceColumns($scope.localVars.selectedResource,taskId,$scope.localVars.formType,lang).then(function(data) {
                                
                $scope.localVars.gridOptionsResourceData.columnDefs = data.data;
                if ($scope.localVars.currentPage==undefined) $scope.localVars.currentPage=1;
                if (getData && !$scope.formDefinition) $scope.localVars.getResourceDataPaginated($scope.localVars.nextPage,$scope.localVars.previousPage,1,$scope.localVars.pageSize);               
                else
                {
                      $scope.localVars.restoreGridState();
                      $scope.fd.initialLoad = false;
                      $scope.localVars.resourceDataDownloaded = true;
                      $scope.localVars.resourceDataDownloading = false;
                }
                 },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.localVars.resourceDataDownloading = false;
                             });
               
     }
  };
     
     $scope.localVars.getResourceDataFiltered = function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

         
          if ($scope.localVars.selectedResource==null) {
            $scope.localVars.selectedResource = $scope.fd.Related;
          }
          $scope.localVars.resourceDataDownloading = true;
          
         ResourcesDataService.getResourceDataFiltered($scope.localVars.selectedResource,$scope.taskId,$scope.localVars.formType,lang,$scope.localVars.dashboard.data).then(function(data) {//MIGOR
                            
                         $scope.localVars.gridOptionsResourceData.data = data.data;
                         $scope.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                         
                           $timeout(function(){
                             $scope.fd.initialLoad = false;
                             $scope.localVars.resourceDataDownloaded = true;
                             $scope.localVars.resourceDataDownloading = false;
                             $scope.localVars.restoreGridState();
                           });
                        
                            
                         },function(error){
                       MessagingService.addMessage(error.msg,'error');
                       $scope.localVars.resourceDataDownloading = false;
                    });
        };
		
       $scope.localVars.setField = function(form,name,value,msg)
        {
     				TaskDataService.setField(form,name,value);
            if (msg) MessagingService.addMessage(msg,'info');
        };
        
          $scope.localVars.showHideSearchForm = function()
        {
     				if ($scope.fd.ShowSearchForm) $scope.localVars.getFormResource();
        };
       
       
       
    $scope.localVars.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData = WidgetDataExchangeService.getPrevTaskData($scope.prevTaskExeId);
            var datasetId=0,datasetRecordId=0;
            let action;
            for (let k=0;k<pTaskData.length;k++)
            {
                if (pTaskData[k].DEI!=undefined && pTaskData[k].DEI!=null)
                {
                for (var i=0;i<pTaskData[k].DEI.length;i++)
                {
                  if (pTaskData[k].DEI[i].name==linkType)
                  {
                    if (pTaskData[k].DEI[i].value!=undefined && pTaskData[k].DEI[i].value!=null)
                    {
                      //populate selected item
                     
                      for (var j=0;j<pTaskData[k].DEI[i].value.length;j++)
                      {
                       if (pTaskData[k].DEI[i].value[j].name=="resourceId" && pTaskData[k].DEI[i].value[j].value!=undefined) datasetId=pTaskData[k].DEI[i].value[j].value;
                       if (pTaskData[k].DEI[i].value[j].name=="resourceRecordId" && pTaskData[k].DEI[i].value[j].value!=undefined)
                       {
                            datasetRecordId=pTaskData[k].DEI[i].value[j].value;
                            if ($scope.fd.Parameters.PresentationType == 'dropdown' && datasetId==$scope.fd.Related && !$scope.fd.Multiline)
                            {
                        
                                $scope.itemValue.val = pTaskData[k].DEI[i].value[j].value;
                                action = $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.itemValue.val);
                            }
                       }
                       if (pTaskData[k].DEI[i].value[j].name=="resourceRecordIds" && pTaskData[k].DEI[i].value[j].value!=undefined)
                       {
                            datasetRecordId=pTaskData[k].DEI[i].value[j].value;
                            if ($scope.fd.Parameters.PresentationType == 'dropdown' && datasetId==$scope.fd.Related && $scope.fd.Multiline)
                            {
                        
                                $scope.itemValue.val = pTaskData[k].DEI[i].value[j].value;
                                action = $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.itemValue.val);
                            }
                       }
                        
                      }
                         
                        
                    }
                      
                  }
                    
                  }
                }
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };


   
  $scope.localVars.previousTaskData=$scope.localVars.getPreviousTaskWidget("resourceLinkDatasetList");
 
        
       $scope.localVars.saveGridState= function() {
       var state = $scope.localVars.gridApi.saveState.save();
       $scope.fd.GridState = state;
       MessagingService.addMessage("To make changes stick please save form",'info');
             
    };
  
                if ($scope.fd.Related!=null &&  !$scope.formDefinition && !$scope.localVars.searchFormDownloaded && !$scope.fd.Parameters.DownloadAllRecords)
                {
                  
                  $scope.localVars.executeDatasetList();
                  
                }else if ($scope.fd.Related!=null &&  !$scope.formDefinition && !$scope.localVars.searchFormDownloaded && $scope.fd.Parameters.DownloadAllRecords)
                {
                  if ($scope.fd.Parameters.PresentationType=='table') $scope.localVars.getResourceColumns(true);
                  if ($scope.fd.Parameters.PresentationType=='dropdown')
                  {
                    $scope.localVars.getDatasetDataList(true,$scope.fd.Parameters.ClientSideFilter);
                    $scope.localVars.getFormResource();
                  }
                  if ($scope.fd.Parameters.PresentationType=='card') $scope.localVars.getCardLayout();
                
                }
                else if ($scope.fd.Related!=null && ($scope.localVars.datasetMapperType==2 || $scope.fd.AutoSearch))
                {
                   $scope.localVars.getFormResource();
                   $scope.localVars.getDatasets();
                   if ($scope.fd.Parameters.PresentationType=='table') $scope.localVars.getResourceColumns(false);
                   else if ($scope.fd.Parameters.PresentationType=='card') $scope.localVars.getCardLayout();
                  
                }else if($scope.localVars.datasetMapperType!=5)
                {
                   $scope.localVars.showHideSearchForm();
                   $scope.localVars.getResourceColumns(false);
                   $scope.localVars.getDatasets();
                  
                }
                  
                  if ($scope.fd.Parameters!=null && $scope.fd.Parameters!=undefined)
                   {
                     if ($scope.fd.Parameters.PresentationType=="dropdown") $scope.localVars.getFormFields(); 
                   }
  
   $rootScope.$broadcast("Widget"+$scope.fd.PresentationId+"Ready",null);
        
}]; //controler_END

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskViewPdf.html',
            controller: controller,
            link: function (scope, elem, attrs) {
             }
             
             
  } 
        
};