'use strict';

export function ktMenuDefinition() {
  
   return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktMenuDefinition.html',
             scope:
             {
            menuItem:'=',
            localVars:'='
            },
            controller: function($scope,$rootScope,$timeout,MenuService,MessagingService,ResourcesDataService){
              let lang = $rootScope.globals.currentUser.lang;
              if ($scope.localVars==undefined) $scope.localVars={};
              $scope.menu_service = MenuService;
              $scope.CTRL='ktMenuDefinition'
              
              if ($scope.menuItem==undefined) $scope.menuItem=$scope.menu_service.getSelectedSidemenu();
              
              $scope.headerLabel="Grant access to side menu";
              $scope.usersLabel="Users";
              $scope.groupsLabel="Groups";  
             
                $scope.localVars.setPage = function() {
                 var pos = $scope.localVars.pageList.map(function(e) { return e.id; }).indexOf($scope.menuItem.katalyonode_menus_Target_id);
              
                if (pos>=0){
                  $scope.menuItem.Target = $scope.localVars.pageList[pos];
                }
              }
              $scope.getUsersGroups= function(){
                var menuId = $scope.menuItem.katalyonode_menus_id;
                if (menuId!=null)
                {
                  MenuService.getUsersGroupsForMenu(menuId,lang).then(
                        function(data) {
                          $scope.menuItem.Users = data.Users;
                          $scope.menuItem.Groups = data.Groups;
                          $scope.menuItem.UsersGroupsDownloaded = true;
                         
                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                        });
                }else $scope.menuItem.UsersGroupsDownloaded = true;
                
              }
              
              $scope.localVars.getPages = function()
              {
              ResourcesDataService.getResourceDefListByType(lang,'page').then(
                function(data) {
                  $scope.localVars.pageList = data.data;
                  $scope.localVars.pagesLoaded=true;
                  $scope.localVars.setPage();
                  $scope.getUsersGroups();
                },function(error){    
                  MessagingService.addMessage(error.msg,'error');
                });
          
              }
              $scope.localVars.getPages();
              
              
               
               
               $scope.$watch('menu_service.selectedSideMenu',function(newValue,oldValue){
                  if (newValue!=oldValue)
                  {
                  $scope.menuItem=$scope.menu_service.getSelectedSidemenu();
                  }
               });
               
                $scope.saveSideMenu = function()
            
                {
              
                
                    $scope.menu_service.saveMenu($scope.menu_service.selectedSideMenu,lang).then(function(data) {
                                
                                 
                                 if ($scope.menu_service.selectedSideMenu.katalyonode_menus_id == null || $scope.menu_service.selectedSideMenu.katalyonode_menus_id==undefined)
                                  {
                                    $scope.menu_service.setSelectedSidemenu(data.data,lang);
                                    $scope.menu_service.addNewSideMenuToNavbar($scope.menu_service.selectedSideMenu);
                                  
                                  }
                                  else
                                  {
                                    $scope.menu_service.selectedSideMenu = data.data;
                                    
                                  }
                                  $scope.menu_service.updateNavbarSelectedSideMenu();
                                  
                                    MessagingService.addMessage(data.msg,'success');
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                          
                             });
              };
               
               
               
               
               $scope.localVars.menuLoaded = true;
               },
             link: function (scope, elem, attrs) {
            

             
           
           
      }
    }
  
};