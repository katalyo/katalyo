'use strict';

export function ktSelectGeneric () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktSelectGeneric.html',
             scope: {
                inputParameters: '=',
                selectedItem: '=',
                inputArray: '=',
                requiredField: '=?',
                onSelectFn: '&',
                refreshFn: '&',
            },
            controller: function(UserService,ResourcesDataService,$rootScope,ProcessingService){},
             link: function (scope, elem, attrs) {
                
                 scope.onSelectItem = function(item,model) {
                   scope.onSelectFn(item,model);
                }
                
                 scope.refreshResults = function(filter) {
                   scope.refreshFn(filter);
                }
                     
              }
        }
  
};