'use strict';

export function ktResourceid () {
      
        return {
        restrict: 'A',
	templateUrl: 'ktdirectivetemplates/ktResourceid.html',
        controller: function ($scope,GridsterService,KatalyoStateManager)
        {
                $scope.gsrvc = GridsterService;
                
                $scope.ksm = KatalyoStateManager;
                if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
                let resourceIdValue = null;
                if ($scope.formData!=undefined)
                {
                        if ($scope.formData.length==0) $scope.formData=[{}];
                        resourceIdValue=$scope.formData[0]['id']
                        
                }else $scope.formData=[{}];
                
                let valueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+'-resourceidValueStateName'+$scope.fd.PresentationId,resourceIdValue);
                let valueStateName = valueState.subject.name;
                //valueState.subject.notifyObservers("valueChanged"); 
                 
                let value_changed = function(data)
                {
                    valueState.subject.setStateChanges = data;
                    valueState.subject.notifyObservers("valueChanged");
                };
                
                let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
                                                  
                               
                $scope.on_blur_action=function()
                {
                        
                        $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.formData[0]["id"]);
                        
                };
               
                
                
                let apply_value = function(data)
                {
                    if (Array.isArray(data)) $scope.formData[0]["id"] = data[0].id;
                    else if (Object.prototype.toString.call(data) === '[object Object]') $scope.formData[0]["id"] = data.id;

                    $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.formData[0]["id"]);
                }; 
               // let observer_undo = $scope.ksm.subscribe(formChangesSubject,"Field"+$scope.fd.ItemId,"valueChanged",apply_value);
                if ($scope.fd.Parameters.DependentFieldId!=undefined && $scope.fd.Parameters.DependentFieldId!=null)
                
                {
                    if ($scope.fd.Parameters.DependentFieldId!="")
                    {
                        let dependent_field = "Field"+$scope.fd.PresentationId;
                        let subscribedSubject = $scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId+$scope.resourceDefinition.transaction_id;
                        let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
                        let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
                        let val = $scope.ksm.getState(subscribedSubject);
                        if (val.state!=null) apply_value(val.state);
                    }
                }

                $scope.fd.colLength=[];
                
                for (var i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));

                 if (resourceIdValue!=undefined) $scope.ksm.executeAction(valueStateName,"valueChanged", resourceIdValue);
                
                },
        link: function (scope, elem, attrs) {
				
        
        
                scope.search=[
                {id:1,filter:'gt',name:"Greater than",icon:"fa fa-chevron-right"},
                {id:2,filter:'between',name:"Between",icon:"fa fa-expand"},
                {id:3,filter:'lt',name:"Lower than",icon:"fa fa-chevron-left"},
                {id:4,filter:'eq',name:"Equals",icon:"fa fa-bars"} ];
				
                scope.refreshSearchValue = function() {
                        if (scope.fd.Search) scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName];      
                } 
                
                if (scope.showSearchFilters)  {
                    scope.fd.Search=scope.search[3];//initialize to equals
                    if (scope.fd.Search && scope.formData!=undefined && scope.formData.length>0) scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 
                }
              
                scope.showProperties = function(event)
                {
                        scope.gsrvc.setFieldProperties(scope.fd,event);
       
                };
                
                scope.applyFilters = function(index) {
                        scope.fd.Search=scope.search[index];
                              
                        if (scope.formData[0][scope.fd.FieldName]) scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName];      
                         
                }
        
            }
        };
};