'use strict';

export function ktAdjustContentToNavbar(){
   
   var directive = {
		restrict 	: 'EA',
        link: function (scope, element, attrs) {
            
                
        },
        controller: function ($scope,$element,GridsterService)
        {
            $scope.gsrvc = GridsterService;
            $scope.$watch(function(){
               if ($element[0].offsetHeight>0)
               {
                $scope.gsrvc.setNavbarHeight($element[0].offsetHeight);
               }
            }); 
            
        }
	};
	return directive;

};