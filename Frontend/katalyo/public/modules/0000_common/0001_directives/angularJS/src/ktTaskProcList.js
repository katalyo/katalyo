'use strict';

export function ktTaskProcList () {
             var controller = ['$scope', '$stateParams','$state','UserService','ResourcesDataService','$rootScope','ProcessingService','$timeout','$cookies','CodesService','WidgetDataExchangeService','TaskDataService','MessagingService','GridsterService',
                               function ($scope,$stateParams,$state,UserService,ResourcesDataService,$rootScope,ProcessingService,$timeout,$cookies,CodesService,WidgetDataExchangeService,TaskDataService,MessagingService,GridsterService) {
          
                  
          
          }];
          
          
        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskProcList.html',
            controller:controller,
             link: function (scope, elem, attrs) {
                 
             var lang=$rootScope.globals.currentUser.lang;
                
                    scope.localVars = {};                      
                    scope.stateName=$state.current.name;
                    scope.stateParams = $state.current.params;
                    scope.task_initiate_id=$stateParams.task_initiate_id;
                    scope.task_execute_id=$stateParams.task_execute_id;
                                    
                     scope.is_superuser =  $rootScope.globals.currentUser.is_superuser;
                   
                    scope.gridOptions = {
                      data:[],
                    enableGridMenu: true,
                    //showGridFooter: true,
                    //showColumnFooter: true,
                    enableRowSelection: true,
                    multiSelect: false,
                    enableHorizontalScrollbar : 0, 
                    enableVerticalScrollbar : 2,
                    //enableFiltering: true,
                    enableRowHeaderSelection: true,
                    enableColumnResizing: true,
                    useExternalPagination: true,
                    paginationPageSizes: [10, 25, 50,100,200,500,1000],
                    enablePaginationControls: false,
                    //paginationPageSize: 10,
                    //useExternalSorting : true,
                    //useExternalFiltering: true,
                   // minRowsToShow : 3
                   };
                    
                  var paginationOptions = {
                    pageNumber: 1,
                    pageSize: 10,
                    sort: null
                    };
                    
                    var columnDefsInitiate = [
                        { name: 'TaskInitiationId.id', displayName: 'ID', type: 'number', width:'5%',enableHiding: false, visible: true },
                        { name: 'TaskInitiationId.InitiatorId.name', displayName: 'Started by'},
                        { name: 'TaskInitiationId.Status.CodesDetailLang[0].Value', displayName: 'Task status' },
                        { name: 'TaskInitiationId.Resourcedef.resourcedeflang_set[0].Name', displayName: 'Task name' },
                        { name: 'TaskInitiationId.Resourcedef.resourcedeflang_set[0].Description', displayName: 'Task description' },
                        { name: 'TaskInitiationId.Resourcedef.id', displayName: 'Task def id',enableHiding: false,visible: false },
                        { name: 'TaskInitiationId.CreatedDateTime', displayName: 'Date created',enableHiding: false,type: 'date', cellFilter: 'date:\'dd.MM.yyyy HH:mm:ss:sss\'' },
                        { name: 'TaskInitiationId.CreatedBy.name', displayName: 'Created by'},
                        { name: 'ForGroup.name', displayName: 'For group'},
                        ];
                    
                   var columnDefsExecute = [
                         { name: 'id', displayName: 'Task Execute ID', type: 'number', width:'10%',enableHiding: false, visible: true },
                         { name: 'resourcedef.resourcedeflang_set[0].Name', displayName: 'Task name' },
                         { name: 'resourcedef.resourcedeflang_set[0].Description', displayName: 'Task description' },
                         { name: 'actualOwner.name', displayName: 'Task Owner'},
                         { name: 'status.CodesDetailLang[0].Value', displayName: 'Task status' },
                         { name: 'outcome.TaskOutcomesDetailLang_taskoutcomedetailid[0].DisplayName', displayName: 'Task outcome' },
                         { name: 'resourcedef.id', displayName: 'Task def id',enableHiding: false,visible: false },
                         { name: 'CreatedDateTime', displayName: 'Date created',enableHiding: false,type: 'date', cellFilter: 'date:\'dd.MM.yyyy HH:mm:ss:sss\'' },
                         { name: 'createdBy.name', displayName: 'Created by'},
                         ];   
                   
                    scope.task_execute_id = 0;
                   scope.task_initiate_id = 0;
                    
                    if (scope.fd===undefined) scope.fd={};
                    if (scope.fd.search===undefined || scope.fd.search===null) scope.fd.search = {};
                   //ovo treba prilagoditi
                   if (scope.fd.search.taskListType==undefined) scope.fd.search.taskListType="claim";
                      
                      
                    
                     scope.gridOptions.onRegisterApi = function (gridApi) {
                        scope.gridApi = gridApi;
                        gridApi.selection.on.rowSelectionChanged(scope,function(row){
                         scope.taskSelected=row.isSelected
                       
                         if(row.isSelected){
                           
                               if (scope.fd.search.taskListType=="execute")
                               {
                                scope.selectedTask=row.entity.id
                                scope.task_execute_id = scope.selectedTask;
                                scope.selectedTaskDefId=row.entity.resourcedef.id
                                scope.selectedTaskObject = row.entity;  
                               }
                               else
                               {
                                scope.selectedTask=row.entity.TaskInitiationId.id
                                scope.task_initiate_id = scope.selectedTask;
                                scope.selectedTaskDefId=row.entity.TaskInitiationId.Resourcedef.id
                                scope.selectedTaskObject = row.entity.TaskInitiationId;  
                               }
                               
                            }
                        });
                        
                        //external pagination
                      gridApi.pagination.on.paginationChanged(scope, function (newPage, pageSize) {
                          paginationOptions.pageNumber = newPage;
                          paginationOptions.pageSize = pageSize;
                          scope.ExecuteAllListPaginatedFilter(scope.fd.search.taskListType,1,false,"",scope.localVars.nextPage,scope.localVars.previousPage,newPage,pageSize,$scope.fd.search);
                      });
                      
                       gridApi.core.on.filterChanged(scope, function() {
                           // Set some scope variable that both lists use for filtering
                          // scope.someFilterValueBothListsLookAt = ......
                       });
                
                      
                     }
                      
                    scope.removeField = function (index){
						
                        if (index > -1) {
						 
                           scope.inputArray.layout.splice(index, 1);
						 
                        }

                  }
                
                    scope.ClaimSelected = function(){
                              
                              
                            var lang=$rootScope.globals.currentUser.lang;
                             TaskDataService.ExecuteTaskAction(scope.selectedTask,0,'claim',lang).then(function(data) {
                       
                                                   
                                                    
                                                    //refresh liste task-ova za claim
                                                    scope.ExecuteAllList(scope.fd.search.taskListType,1,true,data.data.msg);
                                                   
                                              },function(error){
                                                
                                            
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                      } 
                    
                    scope.ExecuteAllList = function(type,filter,afterClaim,msgAfter){
                      
                      var lang=$rootScope.globals.currentUser.lang;
                      
                      TaskDataService.getInitiatedTasks(type,filter).then(function(data) {
                                                
                                                  scope.gridOptions.data = data.data.results;
                                                 if (afterClaim)
                                                 {
                                                   MessagingService.addMessage(msgAfter,'success');
                                                  
                                                 }
                                               },function(error){
                                                
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                    }
                    
                    
                  
                  scope.ExecuteAllListPaginated = function(type,filter,afterClaim,msgAfter,urlNext,urlPrev,newPage,pageSize){
                      
                      var lang=$rootScope.globals.currentUser.lang;
                      
                      if (Math.ceil(scope.totalCount/pageSize)<newPage)
                      {
                        newPage = Math.ceil(scope.totalCount/pageSize);
                      }
                      TaskDataService.getInitiatedTasksPaginated(type,filter,urlNext,urlPrev,newPage,pageSize).then(function(data) {
                                                
                                                  scope.gridOptions.data = data.data.results;
                                                  scope.nextPage = data.data.next;
                                                  scope.previousPage = data.data.previous;
                                                  scope.totalCount=data.data.count;
                                                  scope.currentPage=newPage;
                                                  scope.numPages = Math.ceil(scope.totalCount/pageSize);
                                                  scope.pageSize = pageSize;
                                                  scope.gridOptions.totalItems=scope.totalCount;
                                                 if (afterClaim)
                                                 {
                                                   MessagingService.addMessage(msgAfter,'success');
                                                  
                                                 }
                                               },function(error){
                                                
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                    }
                    
                    
                    scope.ExecuteAllListPaginatedFilter = function(type,filter,afterClaim,msgAfter,urlNext,urlPrev,newPage,pageSize,f_filter){
                      
                      var lang=$rootScope.globals.currentUser.lang;
                      
                      
                      if (scope.fd.search.taskListType=="claim")
                      {
                        scope.gridOptions.columnDefs = columnDefsInitiate;
                      }
                    
                         if (scope.fd.search.taskListType=="execute")
                      {
                        scope.gridOptions.columnDefs = columnDefsExecute;
                      }
                    
                      
                      
                      if (Math.ceil(scope.localVars.totalCount/pageSize)<newPage)
                      {
                        newPage = Math.ceil(scope.localVars.totalCount/pageSize);
                      }
                      scope.gridOptions.data.length=0;
                      TaskDataService.getInitiatedTasksPaginatedFiltered(type,filter,urlNext,urlPrev,newPage,pageSize,f_filter).then(function(data) {
                                                
                                                  scope.gridOptions.data = data.data.results;
                                                  scope.localVars.nextPage = data.data.next;
                                                  scope.localVars.previousPage = data.data.previous;
                                                  scope.localVars.totalCount=data.data.count;
                                                  scope.localVars.currentPage=newPage;
                                                  scope.localVars.numPages = Math.ceil(scope.localVars.totalCount/pageSize);
                                                  scope.localVars.pageSize = pageSize;
                                                  scope.gridOptions.totalItems=scope.localVars.totalCount;
                                                  if (scope.localVars.totalCount==1)
                                                  {
                                                    if (scope.fd.search.taskListType=="execute")
                                                      {
                                                       scope.task_execute_id = scope.gridOptions.data[0].id;
                                                         
                                                      }
                                                      else
                                                      {
                                                       scope.task_initiate_id = scope.gridOptions.data[0].TaskInitiationId.id;
                                                       
                                                      }
                                                      scope.localVars.getTaskExecuteData(scope.task_initiate_id,scope.task_execute_id);
                                                  }
                                                  $timeout(function(){
                                                      $rootScope.$broadcast('PageLoaded');
                                                  });
                                                 if (afterClaim)
                                                 {
                                                   MessagingService.addMessage(msgAfter,'success');
                                                  
                                                 }
                                               },function(error){
                                                
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                    }
                     scope.localVars.pageChanged = function(){
                        scope.ExecuteAllListPaginatedFilter(scope.fd.search.taskListType,1,false,"",scope.localVars.nextPage,scope.localVars.previousPage,scope.localVars.currentPage,scope.localVars.pageSize,scope.fd.search);
                         }
                         
                         
                    //napuni grid s definicijama resursa
                     //scope.currentPage =  paginationOptions.pageNumber;
                     //scope.pageSize = paginationOptions.pageSize;
                      
                     scope.ExecuteAllListPaginatedFilter(scope.fd.search.taskListType,1,false,"","","",paginationOptions.pageNumber,paginationOptions.pageSize,scope.fd.search);
                     
                     scope.ExecuteListPrePaginated = function(listFilter){
                            scope.ExecuteAllListPaginated(scope.fd.search.taskListType,listFilter,false,"","","",paginationOptions.pageNumber,paginationOptions.pageSize);
                         }
                         
                          ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
                                                
                                                  scope.taskDefList = data.data;
                                                  
                                                
                                                 
                                               },function(error){
                                                
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                      //
                      scope.fd.search.headerLabel = "Select users or groups";
                      scope.fd.search.usersLabel ="Users";
                      scope.fd.search.groupsLabel = "Groups";
                      
                      //get task statuses
                      
                       CodesService.getCodesByName('taskstatuses',lang).then(function (data) {
                                scope.taskStatuses = data.data;
                             });
              
              }
        }             
};