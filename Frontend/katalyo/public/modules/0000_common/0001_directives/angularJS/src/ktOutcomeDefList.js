'use strict';

export function ktOutcomeDefList () {

    return {
                restrict: 'A',
                templateUrl: 'ktdirectivetemplates/ktOutcomeDefList.html',
                scope:{
                        task: '=',
                        localVars: '=',
                        fd: '=',
                },
                controller: function($scope,TaskDefinitionService,CodesService,MessagingService,TaskDataService,$rootScope,$timeout,dragulaService,GridsterService){
                        
                        $scope.parameters = {placeholder:'Select template to load',label:'Outcomes',name:'outcomes',selectedItem:$scope.task.TaskOutcomeHeadId,
                        paddingLeft:true,hideLabel:true,dropdownSize:'col-lg-24'};
                        $scope.showLoadFromTemplate ={};
                        $scope.tds=TaskDefinitionService;
                        $scope.outcomesDownloaded = false;
                        $scope.localVars.outcomes=[];
                        var lang=$rootScope.globals.currentUser.lang;
                        
                        $scope.gsrvc=GridsterService;
                        
                         $scope.setupDragula = function()
                         {
                              var widgetHandleResource = 'element-drag-handle';
                              
                              var bag = dragulaService.find($rootScope, 'bag-outcomes');
                              if (!bag)
                              {
                                 $scope.gsrvc.setupDragula('bag-outcomes','outcomesDragula',widgetHandleResource);    
                              }
                              else
                              {
                                   $scope.gsrvc.destroyDragula('bag-outcomes');
                                   $scope.gsrvc.setupDragula('bag-outcomes','outcomesDragula',widgetHandleResource); 
                                    
                               }
                         };  
                        
                        
                        TaskDataService.GetTaskOutcomesDef(lang).then(function(data) {
                       
                                    $scope.outcomeCodes = data.data;
                                    if ($scope.task.TaskOutcomeHeadId!=null)
                                    {
                                         
                                         var pos = $scope.outcomeCodes.map(function(e) { return e.id; }).indexOf($scope.task.TaskOutcomeHeadId);
                                         if (pos>-1) {
                                           $scope.parameters.selectedItem = $scope.outcomeCodes[pos];
                                           $scope.selectedOutcomeId=$scope.parameters.selectedItem.id;
                             
                                           
                                         }
                                         $scope.GetOutcomes(); 
         
         
                                    }   
                                  
                               },function(error){
                                 
                                   MessagingService.addMessage(error.msg,'error');
                               }
                         );
                
                 
                 $scope.showoutcomedetails= function(outcome,index,showLoadFromTemplate){
                                 //transfer child $scope parameters to parents 
                                 $scope.showLoadFromTemplate=showLoadFromTemplate;
                                 $scope.clickedOutcome = outcome;
                                 $scope.selectedOutcomeIndex=index
                                 $scope.showConfirmRemoveOutcome = false;
                                 $scope.setupDragula();
                 };
                 
                 $scope.selectOutcome= function(outcome){
                                 $scope.localVars.selectedButton.outcome=outcome;
                               
                 };
                 
                               
                 $scope.selectedOutcome = function(item,model){
                               $scope.parameters.selectedItem = item;
                               $scope.selectedOutcomeId=item.id;
                               }                     
                        $scope.removeAction = function(index){
                             //$scope.task.TaskOutcomeCodeId = null;
                              $scope.clickedOutcome.actions.splice(index,1);
                        }
                        
                        $scope.removeOutcome = function(index){
                             //$scope.task.TaskOutcomeCodeId = null;
                              $scope.localVars.outcomes.splice(index,1);
                        }
            
                            
                    
                          $scope.isSelectedOutcome = function(index) {
                             // if ($scope.selectedIndex==index) return true;
                              //return false;
                              if ($scope.selectedOutcomeIndex==index ) return true;
                              return false;
                              
                          };
                                          
                                          
                 $scope.localVars.promjenioOutcome = function (index,isSelected) {
                    //debugger   //promjenioOutcome              

                   isSelected = isSelected[$scope.localVars.selectedButtonIndex]
                   
                   if (isSelected[index] == true) {
                       for (let key in isSelected) {
                            isSelected[key]=false
                       } 

                       isSelected[index] = true
                       
                       $scope.localVars.selectedOutcomeIndex = index
                   } else 
                        $scope.localVars.selectedOutcomeIndex = null
                 }
                                          
                                                                         
                   
                  $scope.addOutcomeItem = function(newItem){
                                                   
                         if (newItem!=undefined && newItem!=null) {
                           $scope.localVars.outcomes.push(newItem);
                         }else {
                            // $timeout(function(){ //MIGOR -može se maknuti timeout
                                 $scope.localVars.outcomes.push({id1:null,name1:null,outcomeName:null,icon:null,btnName:'*New Outcome*',btnClass:null,actions:[]});
                                 $scope.selectedOutcomeIndex= $scope.localVars.outcomes.length-1;
                                 $scope.clickedOutcome=$scope.localVars.outcomes[$scope.selectedOutcomeIndex];
                                 $scope.showLoadFromTemplate[$scope.selectedOutcomeIndex]=true;
                            //});
                          };
                               
                            //$timeout(function(){ //MIGOR -može se maknuti timeout
                                   $scope.showoutcomedetails( $scope.localVars.outcomes[$scope.localVars.outcomes.length-1], $scope.localVars.outcomes.length-1, $scope.showLoadFromTemplate);
                            // });
                             
                            
                             // vidjeti da li ovo dole ima funkciju                   
                      //   if ($scope.parameters.selectedItem) delete $scope.parameters.selectedItem; //reset selected item
                     }
                          
                     $scope.GetOutcomes = function(){
                                
                               var lang=$rootScope.globals.currentUser.lang;               
                               var outcomeId = 0                /*MIGOR provjeriti jos*/  
                              // if ($scope.parameters.selectedItem!=undefined) { /*MIGOR provjeriti jos*/ 
                               //     outcomeId = $scope.parameters.selectedItem.id;  
                              // }
                              if ($scope.task.TaskOutcomeHeadId !=undefined) outcomeId = $scope.task.TaskOutcomeHeadId;
                              
                               
                               $scope.loadingActions=true; 
                              if (!$scope.outcomesDownloaded)
                               {   
                                    TaskDataService.GetTaskOutcomes( $scope.task.ResourceDefId,outcomeId,lang).then(function(data) {            
                                               
                                              $scope.localVars.outcomes = data.data;
                                              //$scope.task.TaskOutcomeHeadId = $scope.parameters.selectedItem.id;
                                              //$scope.selectedOutcomeId=$scope.parameters.selectedItem.id;
                                              $scope.outcomesDownloaded = true;
                                              $scope.loadingActions=false; 
                                             // if ($scope.localVars.outcomes.length>0)  $scope.showoutcomedetails($scope.localVars.outcomes[0],0,$scope.showLoadFromTemplate); //show first available outcome (if any)
                                              
                                    },function(error){
                                             $scope.loadingActions=false; 
                                             MessagingService.addMessage(error.msg,'error');
                                    });
                               }
                               else
                               {
                                $scope.loadingActions=false;      
                               }
                 
                     }//GetOutcomes
         
                        //**********************************************************
                        //           SAVE TASK OUTCOMES
                        //**********************************************************    
                        $scope.localVars.saveTaskOutcomes = function(){
               
                                    //check for errors
                                    var saveTaskOutcomesErrors=[];
                                    if (!$scope.task.InfiniteTask)
                                    {
                                       for(var i=0;i<$scope.localVars.outcomes.length;i++){
                                            if ($scope.localVars.outcomes[i].outcomeName==null) 
                                                saveTaskOutcomesErrors.push('Outcome name must be defined!');
                                       }
                                       if ($scope.localVars.outcomes.length==0)
                                           saveTaskOutcomesErrors.push("Outcome must be defined!");
                                    }
                                    
                                    if (saveTaskOutcomesErrors.length>0) 
                                             MessagingService.addMessage(saveTaskOutcomesErrors,'error');
                                    else 
                                    { // NO ERRORS
                         
                                                var lang=$rootScope.globals.currentUser.lang;
                                 
                                                //ako ne postoji outcomeHead a ima podataka onda kreiramo novi outcomeHead 
                                                if (($scope.task.TaskOutcomeHeadId == null || $scope.task.TaskOutcomeHeadId==undefined || $scope.task.TaskOutcomeHeadId==0))
                                                {
                                                            if ($scope.localVars.outcomes.length>0) {
                                    
                                                                        $scope.OutcomeHead = {Active:true,id:null};
                                                                        $scope.OutcomeHead.Lang = lang;
                                                                        $scope.OutcomeHead.OutcomeName="OutcomeTask"+$scope.task.ResourceDefId;
                                                                        $scope.OutcomeHead.Name="OutcomeTask"+$scope.task.ResourceDefId;
                    
                                                                        TaskDataService.SaveOutcomeDefinition($scope.OutcomeHead).then(function(data) {
                                                           
                                                                                    $scope.task.TaskOutcomeHeadId=data.data.data.id;
                                                                                    MessagingService.addMessage(data.data.msg,'success');     
                                                                                    $scope.saveTaskOutcomesFinalize();
                                                        
                                                                        },function(error){                   
                                                                                    MessagingService.addMessage(error.msg,'error');
                                                                        }); 
                                                            }
         
                                                }  //ako već postoji outcomehead onda samo snimamo outcome 
                                                else  $scope.saveTaskOutcomesFinalize();
                                    }
                        };
                   

                        $scope.saveTaskOutcomesFinalize = function(){
                                
                                 var outcomes = {outcomeId: $scope.task.TaskOutcomeHeadId, defaultOutcomeId:$scope.task.DefaultOutcomeId, outcomes:$scope.localVars.outcomes};
                                //TODO - nekad je ResourceDefId_id a nekad ResourceDefId !!
                                 TaskDataService.SaveTaskOutcomes($scope.task.ResourceDefId_id || $scope.task.ResourceDefId,!$scope.task.InfiniteTask,outcomes,lang).then(function(data) {
                                                                 
                                           $scope.localVars.outcomes.outcomeCodesDetails = data.data;
                                           $scope.localVars.outcomes.selectedOutcomeIndex=-1;
                                           MessagingService.addMessage(data.msg,'success');
                                
                                         
                                     },function(error){
                                         
                                           MessagingService.addMessage(error.msg,'error');
                                       });
                                                                   
                        }
                    
                            
                        $scope.GetAllOutcomes = function(){
                           
                               var lang=$rootScope.globals.currentUser.lang;                
                               TaskDataService.GetTaskOutcomes(0,0,lang).then(function(data) {            
                                                                                         
                                   for(var i=0;i<data.data.length;i++) { 
                                    data.data[i].id=data.data[i].id1;
                                    data.data[i].name=data.data[i].name1;
                                   }
                                   $scope.allOutcomes = data.data;
                                   
                               },function(error){
                                           MessagingService.addMessage(error.msg,'error');
                               });
                 
                        };//GetAllOutcomes
                           
                        // $scope.GetAllOutcomes();        
                        
            },
            link: function (scope, elem, attrs) {
              
               
            }
    }
};