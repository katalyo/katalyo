'use strict';

export function ktTaskDatasetList () {
   
    var controller = ['$scope', '$uibModal','$stateParams','$timeout','CodesService','ResourcesDataService','$rootScope','ProcessingService','WidgetDataExchangeService','GridsterService','MessagingService','CODES_CONST','TaskDataService','KatalyoStateManager','$q','$filter',
                      function ($scope,$uibModal,$stateParams,$timeout,CodesService,ResourcesDataService,$rootScope,ProcessingService,WidgetDataExchangeService,GridsterService,MessagingService,CODES_CONST,TaskDataService,KatalyoStateManager,$q,$filter) {
      
    if ($scope.fd.localVars===undefined) $scope.fd.localVars={};

  
    $scope.fd.localVars.datasetlistlinked= false
    $scope.CTRL='ktTaskDatasetList'
    $scope.gsrvc=GridsterService;
    $scope.fd.localVars.datasetMapperType=0;
    $scope.fd.localVars.InitialLoad=true;
    
      
    $scope.fd.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_; 
    $scope.fd.localVars.searchFormDownloaded = false;
    $scope.fd.localVars.showSearchFilters=true;
    $scope.fd.localVars.resourceDataDownloaded = false;

    $scope.fd.localVars.resourceTypesList=[{type:'dataset',name:'Dataset'},{type:'file',name:'File'}]
    $scope.fd.localVars.widgetState={};
    $scope.fd.localVars.widgetState.selectedItems=[];
    $scope.taskId=$scope.resourceDefinition.ResourceDefId;
    $scope.fd.localVars.taskId=$scope.resourceDefinition.ResourceDefId;
    if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
    if ($scope.taskId==undefined) $scope.taskId=0
    $scope.fd.localVars.taskId=$scope.taskId;
    $scope.fd.localVars.widgetType = 4;
    $scope.fd.localVars.results = {}
    $scope.fd.localVars.formParams={formViewType:1,showGridNavigation:false};
    $scope.fd.localVars.cardLayoutParams={formViewType:1,showGridNavigation:false};
    //$scope.gsrvc.setFormViewType($scope.fd.localVars.formParams={formViewType));
    $scope.fd.localVars.TablePrefix = 'resource'+$scope.fd.Related+'_'+$rootScope.globals.currentUser.organisation.id
               
    $scope.fd.localVars.parentDatasetDefId=$scope.fd.Related;
    $scope.fd.localVars.animationsEnabled = true;
    if ($scope.fd.Parameters==undefined)  $scope.fd.Parameters = {}
    if ($scope.fd.Parameters.table==undefined) $scope.fd.Parameters.table={} 
    if ($scope.fd.Parameters.Analytics==undefined)  $scope.fd.Parameters.Analytics=[]
    if ($scope.fd.Parameters.SelectedResourceType==undefined)   $scope.fd.Parameters.SelectedResourceType='dataset'

    if ($scope.fd.ParametersLang==undefined)  $scope.fd.ParametersLang = {}
    if ($scope.fd.ParametersLang.PagerLabel1==undefined)   $scope.fd.ParametersLang.PagerLabel1='item per page | Page:'
    if ($scope.fd.ParametersLang.PagerLabel2==undefined)   $scope.fd.ParametersLang.PagerLabel2='of'
    if ($scope.fd.ParametersLang.PagerLabel3==undefined)   $scope.fd.ParametersLang.PagerLabel3='items'
    $scope.fd.Parameters.PagerLabel1 = $scope.fd.ParametersLang.PagerLabel1
    $scope.fd.Parameters.PagerLabel2 = $scope.fd.ParametersLang.PagerLabel2
    $scope.fd.Parameters.PagerLabel3 = $scope.fd.ParametersLang.PagerLabel3

    if ($scope.fd.Parameters.PresentationType==undefined || $scope.fd.Parameters.PresentationType==null) $scope.fd.Parameters.PresentationType='table';
    $scope.fd.localVars.gridsterType=$scope.fd.Parameters.SelectedResourceType;
    $scope.fd.localVars.widgetState.itemValue = {val:null};
    $scope.fd.localVars.isCollapsed = $scope.fd.Parameters.SearchFormCollapsed;
    if ($scope.fd.localVars.isCollapsed==undefined) $scope.fd.localVars.isCollapsed =true;

    $scope.fd.localVars.resourceDefinition = $scope.resourceDefinition;
    $scope.fd.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
        
    $scope.fd.localVars.dashboardCard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
    $scope.fd.localVars.gridOptionsResourceData = {
        //  columnDefs: colDefs,
        //enableGridMenu: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: $scope.fd.Multiline,
      //  enableHorizontalScrollbar : 1,
       // enableVerticalScrollbar : 2,
        enableFiltering: true,
        fontSize: 12,
        cellPadding:30,
        enableRowHeaderSelection: false,
        enableColumnResizing: true,
        enableColumnReordering: true,
        //useExternalPagination: false,
        paginationPageSizes: [10, 25, 50,100,200,500,1000,5000,10000],
       // enablePaginationControls: true,
    };

    if ($scope.fd.Parameters.printable == true) {
        $scope.fd.localVars.gridOptionsResourceData.showColumnFooter = false;
        $scope.fd.localVars.gridOptionsResourceData.showColumnFooter = false;
        $scope.fd.localVars.gridOptionsResourceData.showGridFooter = false;
        $scope.fd.localVars.gridOptionsResourceData.enablePaginationControls = false; 
        $scope.fd.localVars.gridOptionsResourceData.enableVerticalScrollbar = 0;  
        $scope.fd.localVars.gridOptionsResourceData.enableHorizontalScrollbar = 0;  
        $scope.fd.localVars.gridOptionsResourceData.enableFiltering = false;  
        
    }
      
    var paginationOptions = {
        pageNumber: 1,
        pageSize: 10,
        sort: null
      };
      
       $scope.paginationOptions = {
        pageNumber: 1,
        pageSize: 10,
        sort: null
    };
    
      $scope.fd.localVars.gridOptionsRelated = {
         //  columnDefs: colDefs,
          enableGridMenu: true,
          showGridFooter: true,
          showColumnFooter: true,
          enableRowSelection: false,
          multiSelect: false,
          enableHorizontalScrollbar : 2, 
          nableVerticalScrollbar : 2,
          enableFiltering: true,
          enableRowHeaderSelection: true,
          enableColumnResizing: true,
          minRowsToShow: 2,
      }
      
      //MIGORCHART
          $scope.fd.localVars.calculateFunction= [ {id:1,name:'sum'},{id:2,name:'mean'},{id:3,name:'min'},{id:4,name:'max'},{id:5,name:'count'} ]
          $scope.fd.localVars.chartOptions= {
                                scales: {
                                    y: {
                                        beginAtZero: true
                                    },
                                    x: {
                                        beginAtZero: true
                                    },
                                }
                            },
           $scope.fd.localVars.chartTypes = [{id:1,name:'line'},{id:2,name:'bar'},{id:3,name:'radar'},{id:4,name:'doughnut'},{id:5,name:'pie'},{id:6,name:'polarArea'},{id:7,name:'bubble'}]
            
            
            
    

      $scope.taskId=$scope.resourceDefinition.ResourceDefId;

      if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
      if ($scope.taskId==undefined) $scope.taskId=0;
      
      //$scope.fd.PopulateType=1;
      $scope.fd.ParentDatasetDefId=$scope.fd.Related;
      $scope.resourceDefinition.ParentDatasetDefId = $scope.fd.ParentDatasetDefId
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
      $scope.fd.localVars.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.fd.localVars.formDefinition = $scope.resourceDefinition.form_definition;
      $scope.fd.localVars.resourceRecordId = 0;
      $scope.fd.ParentElementId = 0;
      if ($scope.fd.localVars.formDefinition==undefined) $scope.fd.localVars.formDefinition = false;
      if ($scope.taskInitiateId==undefined) $scope.taskInitiateId =0;
      if ($scope.taskExecuteId==undefined) $scope.taskExecuteId =0;
      if ($scope.prevTaskExeId==undefined) $scope.prevTaskExeId =0;
    
        $scope.fd.localVars.widgetState.datasetRecordSelected = false;
        $scope.fd.localVars.widgetState.selectedDatasetRecordId = 0;      
      var out_fields = ['resourceRecordId'];
      //resourceLink
      var widgetDefinition = {DEI: [{'name':'resourceLinkDatasetList','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'datasetListWidget',widgetId: $scope.fd.PresentationId,taskId:  $scope.lTaskId,
      resourceDefId : $scope.fd.Related, //resourceDefId : $scope.selectedResource
      resourceRecordId:$scope.fd.localVars.resourceRecordId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId, output:out_fields };
      $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
      $scope.fd.localVars.LocalId=$scope.fd.localVars.localWidgetId;
      $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
      
      if ($scope.fd.localVars.formDefinition && $scope.fd.localVars.formParams.formViewType==1) $scope.fd.Parameters.BuildMode = true
      else $scope.fd.Parameters.BuildMode = false

      $scope.fd.localVars.showMenu = false
      
       $scope.fd.localVars.toggleDropdown = function(){
           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
       }
       
       
        $scope.fd.localVars.setField = function(form,name,value,msg)
        {
     				TaskDataService.setField(form,name,value);
            if (msg) MessagingService.addMessage(msg,'info');
        };
        
        
        
        
        if ($scope.fd.Parameters==undefined) 
            $scope.fd.Parameters= {
                table: {}, 
                PresentationType:'table', 
                Analytics:[]
         };
        
        const subjectName= $scope.resourceDefinition.page_state_id+"-"+$scope.fd.UuidRef

        let valueState= $scope.ksm.addSubject(subjectName,$scope.fd.localVars.widgetState);
        const valueStateName = valueState.subject.name;
        
        
        
        //*********************** get state if exists********************************************
        //***************************************************************************************
        if (valueState.subject.currentStateIndex>=0 && !$scope.fd.AutoSearch)
        {
            $scope.fd.localVars.loadFromState=true;
            $scope.fd.localVars.widgetState = valueState.subject.getState;
            $scope.fd.localVars.dashboard.widgets.layout=$scope.fd.localVars.widgetState.widgets;
            $scope.fd.fItemValue = $scope.fd.localVars.dashboard.widgets.layout;
            $scope.fd.localVars.searchFormDownloaded = true;
            //$scope.fd.localVars.setField($scope.fd.fItemValue,'Required',false);
            $scope.fd.localVars.formLoaded=true;
            //$scope.fd.Search = $scope.fd.localVars.widgetState.formData;
            //$scope.formData[$scope.fd.PresentationId]=$scope.fd.Search;
        }else
        
        {
          $scope.fd.localVars.widgetState.formData=[{}];  
        }
        
        let value_changed = function(data){
            valueState.subject.setStateChanges = data;
            valueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
        let apply_value = function(data) {
            $scope.fd.localVars.widgetState.itemValue.val = data.state;
        }; 
       
        if ($scope.fd.Parameters.DependentFieldId!=undefined && $scope.fd.Parameters.DependentFieldId!=null) {
            if ($scope.fd.Parameters.DependentFieldId!="") {
                let dependent_field = $scope.fd.Parameters.DependentFieldId;
                
                let subscribedSubject = $scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            }
        }
        
      /*  $scope.fd.localVars.showProperties = function(datasetMapperType){  
                if ($scope.fd.localVars.datasetMapperType==undefined) {
                   $scope.fd.localVars.datasetMapperType = datasetMapperType;
                }else {
                   if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) $scope.fd.localVars.datasetMapperType = datasetMapperType;
                   else $scope.fd.localVars.datasetMapperType=0;
                } 
           };*/
           
         $scope.fd.localVars.showProperties=function(datasetMapperType,showPropertiesRight)
          {
            
            if ($scope.fd.localVars.datasetMapperType==undefined)
            {
               $scope.fd.localVars.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                    $scope.fd.localVars.datasetMapperType = datasetMapperType;
               else 
                   $scope.fd.localVars.datasetMapperType=0;
            }
            if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);
            
        };
          


    // ****  MAIN smart table function for preparation and pagination ****
    
    
      $scope.fd.localVars.getDataSmartTable = function(tableState, pageNumber,pageSize){
       
            //debugger  //debug getDataSmartTable (zapravo ktRefresh u smarttable widgetu)
            let deferred = $q.defer();    
            $scope.resourceLoading=true;
            var lang=$rootScope.globals.currentUser.lang;
            var newPage
           
            
            if (pageNumber==undefined) pageNumber = $scope.paginationOptions.pageNumber;
            if (pageSize==undefined) pageSize = $scope.paginationOptions.pageSize;
            
            if ($scope.fd.localVars.gridOptionsResourceData.data) {
                    $scope.totalCount= $scope.fd.localVars.gridOptionsResourceData.data.length
                    $scope.paginationOptions.totalItemCount=$scope.fd.localVars.gridOptionsResourceData.data.length
                    $scope.paginationOptions.numberOfPages = Math.ceil($scope.paginationOptions.totalItemCount/pageSize);
            }

            if ($scope.totalCount>0){
                if (Math.ceil($scope.totalCount/pageSize)<pageNumber)
                    newPage = Math.ceil($scope.totalCount/pageSize);
            }
            
             if ($scope.fd.localVars.gridOptionsResourceData.data) 
                    $scope.fd.localVars.results = {results : $scope.fd.localVars.gridOptionsResourceData.data.slice( (pageNumber -1) * pageSize, pageNumber * pageSize) }
             else 
                    $scope.fd.localVars.results = {}     
             $scope.fd.localVars.results.columns=$scope.fd.localVars.gridOptionsResourceData.columnDefs
             //$scope.nextPage = data.data.next;
             //$scope.previousPage = data.data.previous;
             $scope.fd.localVars.currentPage=pageNumber;
             $scope.fd.localVars.results.pageSize = pageSize;
             $scope.fd.localVars.results.numberOfPages = $scope.paginationOptions.numberOfPages;
             $scope.fd.localVars.results.count =   $scope.totalCount
             
             $scope.fd.localVars.widgetState.currentPage = $scope.fd.localVars.currentPage;
             $scope.fd.localVars.widgetState.pageSize = $scope.fd.localVars.results.pageSize;
             
             //$scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState); //TODO - provjeriti jel ovo treba otkomentirati
             if (!$scope.fd.localVars.loadFromState) $scope.fd.localVars.resetSelection();
             deferred.resolve({data: $scope.fd.localVars.results});
            
            return deferred.promise;
      };

          //MIGOR TODO
        $scope.fd.localVars.rowSelectionChanged = function (row) {

                                 
                   $scope.fd.localVars.widgetState.selectedDatasetRecordId = 0
                  
                  // find "selectedDatasetRecordId"
                let extDataSourceIdName, newselectedId
                extDataSourceIdName='id';
                 // find ID for clicked row
                  if ($scope.fd.localVars.widgetState?.resourceWidgetDefinition?.Parameters?.extDataSourceLink != undefined) 
                        extDataSourceIdName = $scope.fd.localVars.widgetState.resourceWidgetDefinition.Parameters.extDataSourceLink + "_id"
                  if (  row.hasOwnProperty(extDataSourceIdName) ) {
                       newselectedId = row[extDataSourceIdName]
                       if (newselectedId==null) 
                       {
                        newselectedId = row[$scope.fd.localVars.TablePrefix+'_'+extDataSourceIdName]

                       }
                  }else{

                    newselectedId = row[$scope.fd.localVars.TablePrefix+'_'+extDataSourceIdName]
                  }
                    
                  //debugger //ktTaskDatasetList
                 //already selected
                 if ( row.isSelected == false)    {
                       //unselect and remove from selection 
                       if ( newselectedId > 0  && $scope.fd.Multiline) {        
                                let pos =  $scope.fd.localVars.widgetState.selectedItems.indexOf(  newselectedId )
                                if (pos != -1)  $scope.fd.localVars.widgetState.selectedItems.splice(pos,1)
                       }else if (!$scope.fd.Multiline)  $scope.fd.localVars.widgetState.selectedItems.length=0 
                 } else 
                     if (newselectedId) {
                        $scope.fd.localVars.widgetState.selectedDatasetRecordId = newselectedId;
                        if ($scope.fd.Multiline == true) $scope.fd.localVars.widgetState.selectedItems.push(newselectedId)
                            else $scope.fd.localVars.widgetState.selectedItems =[newselectedId]
                 }
                 
                  $scope.fd.localVars.widgetState.datasetRecordSelected = row.isSelected;
                  $scope.fd.localVars.widgetState.selectedResourceRecord = row
                  $scope.fd.localVars.prevTaskPackage ={resourceDefId:$scope.fd.Related,records: $scope.fd.localVars.widgetState.selectedItems}
                  
                  WidgetDataExchangeService.setData({
                      DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.widgetState.selectedDatasetRecordId}
                        //    ,{name:'resourceRecordIds',value:gridApi.selection.getSelectedRows()},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],
                          ,{name:'resourceRecordIds', value: $scope.fd.localVars.widgetState.selectedItems},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],
                        
                            widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
                
                  $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
       };

    
    
    $scope.fd.localVars.resetSelection = function()
    {
         $scope.fd.localVars.widgetState.selectedItems.length=0;
         $scope.fd.localVars.widgetState.selectedDatasetRecordId = 0;
         $scope.fd.localVars.widgetState.datasetRecordSelected = false;
        
    }
        
    let process_form_change = function(data)
    { 
        if (data.state!=undefined)
        {
            $scope.fd.Search = $scope.fd.localVars.widgetState.formData;
            if ($scope.fd.Parameters.PresentationType=='dropdown') $scope.fd.localVars.getDatasetDataList($scope.fd.Parameters.ClientSideFilter,true);
            else $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,1,$scope.fd.localVars.widgetState.pageSize);               
        }             
    };
        
    //subscribe to events - initially only refresh data
   
    let dependent_field2 = "Field"+$scope.fd.PresentationId;
    let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
    let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,process_form_change);

        
     $scope.fd.localVars.ProcessSearchForm=function (form){
            
           if (form!=undefined){
            for(var i=0;i<form.length;i++) {
                
                    if ('ElementType' in form[i])
                    {
                     if (form[i].Parameters!=undefined)
                        {
                            if (form[i].AutoSearch)
                            {
                              if (form[i].ElementType == 'resource')
                              {
                                var pos = $scope.fd.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(form[i].UuidRef);
                                if (pos<0) $scope.fd.localVars.childWidgets.push({widgetId:form[i].UuidRef,processed:false})
                              }
                              //subscribe to change
                             
                                  if (form[i].Parameters.DependentFieldId!=undefined && form[i].Parameters.DependentFieldId!=null)
              
                                  {
                                      if (form[i].Parameters.DependentFieldId!="")
                                      {
                                          let dependent_field = valueStateName
                                          let subscribedSubject = $scope.resourceDefinition.page_state_id+"-"+form[i].UuidRef;
                                          let subject = $scope.ksm.addSubject(subscribedSubject,null);
                                          let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",process_form_change);
                                          let val = $scope.ksm.getState(subscribedSubject);
                                          if (val.state!=null && val.state!=undefined) process_form_change(val.state);
                                      }
                                  }
                              }
                              
                        }
                      
                     }
                      if ('layout' in form[i])
                      {
                           
                       $scope.fd.localVars.ProcessSearchForm(form[i].layout);
                      }
              }
          } 
           
      };
      
      $scope.fd.localVars.SearchFormProcessed=false;
      
      

        $scope.fd.localVars.setSelectedDataset = function(id,saved)  {

              if ($scope.fd.localVars.selectedResourceItem==undefined) $scope.fd.localVars.selectedResourceItem={};
              $scope.fd.localVars.getPublishedResourceById(id,saved);

		}
     
        $scope.fd.localVars.getPublishedResourceById = function(id,saved) 
 
 {
            let pos
            let old_publish_id = $scope.fd.Parameters.PublishedResourceId
           
                   
            if (saved && $scope.fd.FormDefSearch?.SearchForm!==undefined && $scope.fd.FormDefSearch?.SearchForm!==null && $scope.fd.FormDefSearch?.SearchForm.length>0)
              { 
                    pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                    if (pos>=0){
                        $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.publishedResourceList[pos].name;
                        $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos];

                        $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                    }
                    
                    $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDefSearch?.SearchForm
                    $scope.fd.localVars.formLoaded=true
                    if ($scope.fd.Parameters.PresentationType=='card') 
                        {
                            $scope.fd.localVars.dashboardCard.widgets.layout = $scope.fd.FormDefSearch?.CardForm
                            $scope.fd.localVars.cardLayoutLoaded = true
                        }

                    if ($scope.fd.localVars.formDefinition) 
                    {
                        $scope.fd.localVars.resourceDataDownloaded =true
                        if ($scope.fd.GridState?.columnsDef===undefined || old_publish_id!=id) $scope.fd.localVars.getDefaultColumnsDef()
                        else $scope.fd.localVars.restoreGridState()
                    }
                    
                    //FOR ANAYLTICS
                       $scope.fd.localVars.fieldsList = $scope.fd.GridState.columnsDef.map( 
                            x =>  { 
                                if ( x.name && x.name.includes('.name') )   
                                    return  {'id':x.id, 'FieldName':x.name.slice(0, -5), 'name':x.displayName}  
                                else 
                                    return {'id':x.id, 'FieldName':x.name, 'name':x.displayName} 
                        })
                     // $scope.fd.localVars.getFieldNamesFromObject($scope.fd.localVars.dashboard, $scope.fd.localVars.fieldsList)

              }else{
                  pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  $scope.fd.FormDefSearch={}
                  if (pos>=0){
                    ResourcesDataService.GetPublishedResourceById(id).then(function (data) {

                            //$scope.fd.localVars.selectedResourceItem.id = $scope.fd.localVars.publishedResourceList[pos].id;
                            //$scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.publishedResourceList[pos].name;
                            $scope.fd.localVars.selectedResourceItem.Form = data.data.Form
                            $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos];
                            //$scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem;
                            
                            $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                            if ($scope.fd.localVars.selectedResourceItem_c!==undefined)
                            {
                                $scope.fd.Parameters.PublishedResourceId=$scope.fd.localVars.selectedResourceItem_c.id;
                                $scope.fd.Parameters.PublishedResourceVersion=$scope.fd.localVars.selectedResourceItem_c.Version;
                                $scope.fd.Related=$scope.fd.localVars.selectedResourceItem_c.ResourceDefId; 
                                $scope.fd.localVars.freshForm=angular.copy($scope.fd.localVars.selectedResourceItem_c.Form)
                                $scope.fd.Related = $scope.fd.localVars.publishedResourceList[pos].ResourceDefId
                            }
                            $scope.fd.localVars.dashboard.widgets.layout = data.data.Form

                            
                            if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.dashboardCard.widgets.layout = data.data.Form
                          
                            $scope.fd.FormDefSearch['SearchForm'] = $scope.fd.localVars.dashboard.widgets.layout
                            if ($scope.fd.Parameters.PresentationType=='card') 
                                {
                                    $scope.fd.FormDefSearch['CardForm'] = $scope.fd.localVars.dashboardCard.widgets.layout
                                    $scope.fd.localVars.cardLayoutLoaded = true

                                }
                            if ($scope.fd.GridState?.columnsDef===undefined || old_publish_id!=id) 
                                $scope.fd.localVars.getDefaultColumnsDef()
                            else $scope.fd.localVars.resourceDataDownloaded =true
                           
                            $scope.fd.localVars.formLoaded=true;
                            
                            //MIGORCHART
                            
                             $scope.fd.localVars.fieldsList=[]
                             $scope.fd.localVars.getFieldNamesFromObject($scope.fd.localVars.dashboard, $scope.fd.localVars.fieldsList)//MIGORCHART
                             
                             
                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                    });

                }
            }
             
       
}    
      
      //needed for ANAYLTICS
       $scope.fd.localVars.getFieldNamesFromObject = function (obj, fieldsList, parent,path)   {      // find FieldNames in given Dashboard object recursively . fieldsList  is return variable 
            var o = {};
            Object.keys(obj).forEach(function(key) {        
                if ( (typeof(obj[key]) === 'object') && obj[key]   && key != 'FormDefSearch' && key != 'NameField' && key != 'Parameters' && key != 'localVars' && key != 'FormDef' && key != 'publishedDataset')  { //Do NOT parse 'NameField'  (we need it as it is)
                    $scope.fd.localVars.getFieldNamesFromObject( obj[key], fieldsList, parent, path + '/' + key)
                } 
                else { 
                        if (key == 'DirectiveName') { parent = obj['Related'] } 
                        
                        if (key == 'name') { 
                            if ( obj[key] && obj[key].includes('.name') ) 
                                o['FieldName'] = obj[key].slice(0, -5);
                            else o['FieldName'] = obj[key];         
                        }  
          
                        if (key == 'displayName') {  o['name'] = obj[key]  }                            
                        if (key == 'ElementType' || key == 'id'  ) { 
                            o[key]= obj[key];
                        }   
                    }
            }); 
     
             o['parent'] = parent 
             //o['path'] = path
            // if (path.includes('Gridstate') && path.includes('columnsDef')) {
              if (  o['ElementType'] && o['name'] && Object.keys(o).length > 0 ) {
                     // delete  o['ElementType']
                      //  delete  o['FieldName']
                      //   delete  o['parent']
                           
                  fieldsList.push(o)   
              }
        
    }; 

  
  
      $scope.fd.localVars.GetDatasetData=function(data)
      {
            fromWidget = data.widgetId;
            var allProcessed=true;
            var pos = $scope.fd.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(fromWidget);
            //if (pos<0) $scope.fd.childWidgets.push({widgetId:fromWidget,processed:true});
            for (var i=0;i<$scope.fd.localVars.childWidgets.length;i++)
              {
                
                if ($scope.fd.localVars.childWidgets[i].widgetId == fromWidget) $scope.fd.localVars.childWidgets[i].processed = true;
                if (!$scope.fd.localVars.childWidgets[i].processed) allProcessed = $scope.fd.localVars.childWidgets[i].processed;
              }
              // if (!$scope.SearchFormProcessed) $timeout(function() {$scope.GetDatasetData(fromWidget);},1000);
              if(($scope.fd.AutoSearchClient || $scope.fd.localVars.loadFromState) && allProcessed && $scope.fd.localVars.SearchFormProcessed) {
                    $scope.fd.localVars.selectedResourceId=$scope.fd.Related;
                    if ($scope.fd.Parameters!=null && $scope.fd.Parameters!=undefined)
                    {
                      if ($scope.fd.Parameters.PresentationType=='dropdown') $scope.fd.localVars.getDatasetDataList($scope.fd.Parameters.ClientSideFilter,false);
                      else $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);               
                      
                    }                   
                }
      };
    
      if ($scope.fd.localVars!=undefined) $scope.fd.localVars.childWidgets = [];
      else $scope.fd.localVars = {childWidgets : []};
     // $scope.uncaughtWidgets = [];
      var fromWidget=0;
   
       let search_form_ready = function(data)
        {
            $scope.fd.localVars.GetDatasetData(data); 
        };
        
        let action_search_form_ready = $scope.ksm.addAction(valueStateName,"SearchFormReady"+$scope.taskExecuteId+$scope.resourceDefinition.transaction_id,search_form_ready);
      /*   
      $scope.$on('SearchFormReady'+$scope.taskExecuteId+$scope.resourceDefinition.transaction_id,function(event, data){   
      });     
     */
   
   //**********************************************************
	//           GET DATASET RELATED RECORDS
	//**********************************************************      
        $scope.fd.localVars.getDatasetRelatedRecords = function (relatedDatasetId,recordId,modelId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataRelated($scope.fd.Related,recordId,relatedDatasetId,0,modelId,lang).then(function(data) {
                                //debugger //getDatasetRelatedRecords
                                     $scope.fd.localVars.gridOptionsRelated.data = data.data;
                                     $scope.fd.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                                     $scope.fd.localVars.showGrid=true;
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
    
     //**********************************************************
    //           SELECT CARD ITEM
    //**********************************************************  

    $scope.fd.localVars.selectCardItem = function(index){
        let value
        $scope.fd.localVars.selectedDatasetRecordId=0
        if ($scope.fd.localVars.widgetState.selectedCardIndex!=index)
        {
            $scope.fd.localVars.widgetState.selectedCardIndex=index;
            value=$scope.fd.localVars.gridOptionsResourceData.data[$scope.fd.localVars.widgetState.selectedCardIndex][0];
            $scope.fd.localVars.selectedDatasetRecordId = $scope.fd.localVars.gridOptionsResourceData.data[0][index].id
        }
        else
        {
            $scope.fd.localVars.widgetState.selectedCardIndex=null;
            value = null;
        }
        //send data to package
         WidgetDataExchangeService.setData({
                  DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.selectedDatasetRecordId}
                        ,{name:'resourceRecordIds',value:value},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
     };
       
 //**********************************************************
	//           GET DATASET SINGLE RECORD
	//**********************************************************      
      $scope.fd.localVars.getDatasetSingleRecord = function (relatedDatasetId,recordId) {
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataDirectFilter(relatedDatasetId,0,lang,{id: recordId}).then(function(data) {
                                
                                     $scope.fd.localVars.gridOptionsRelated.data = data.data;
                                     $scope.fd.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                               
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
    
        $scope.$on('SearchFormReady'+$scope.taskExecuteId+$scope.fd.PresentationId,function(event, data1){
      
                                        $scope.fd.localVars.GetDatasetData(data1); 
            
        });
        
    $scope.fd.localVars.TransformDataForCard = function(data)
    {
        let newData = [];
        let newElement = [];
        for (let i=0;i<data.length;i++)
        {
        
        newElement.push(data[i]);
        newData.push(newElement);
        
        newElement = [];
        }
        return newData; 
    };


        let stringToPath = function (path) {
                        // If the path isn't a string, return it
                        if (typeof path !== 'string') return path;
                        var output = []; 
                        path.split('.').forEach(function (item, index) {
                            // Split to an array with bracket notation
                            item.split(/\[([^}]+)\]/g).forEach(function (key) {
                                if (key.length > 0)    output.push(key);// Push to the new array
                            });
                        });
                        return output;
         };
         
         
      let get = function (obj, path, def) {
                        /**
                         * If the path is a string, convert it to an array
                         * @param  {String|Array} path The path
                         * @return {Array}             The path array           */
                          
                        path = stringToPath(path);// Get the path as an array
                        var current = obj;// Cache the current object
                        // For each item in the path, dig into the object
                        for (var i = 0; i < path.length; i++) {
                            // If the item isn't found, return the default (or null)
                            if ( current[path[i]] === undefined || current[path[i]] === null ) return def;
                            current = current[path[i]]; // Otherwise, update the current  value
                        }
                        return current;
         };
                    
      let getItem = function (column,item)
      {  
            
            let def = null
            let val = get(item,column.name,def) ;
            if (val===null) return "∅";
            //debugger //smarttable //getItem
            
            if (column.cellFilter) {
                if (column.cellFilter.substring(0,4).trim()=="date") 
                        val = $filter(column.cellFilter.substring(0,4))(val,column.cellFilter.substring(6,column.cellFilter.length).trim());
            }
            
            if (column.NoOfDecimals){
                    val=  (val).toLocaleString(    navigator.language,          { minimumFractionDigits: column.NoOfDecimals }  ); 
            }
            
           
            return val;
      };
                    
      
    $scope.fd.localVars.executeDatasetList = function(){   
        var lang=$rootScope.globals.currentUser.lang;
        if ($scope.fd.PresentationId==null) {
            $scope.fd.PresentationId = 0;
        }
        if ($scope.fd.localVars.selectedResourceId==undefined) $scope.fd.localVars.selectedResourceId = $scope.fd.Related;
        if ($scope.fd.localVars.widgetState.pageSize==undefined) $scope.fd.localVars.widgetState.pageSize = paginationOptions.pageSize;
        
        $scope.fd.localVars.resourceDataDownloading = true

        TaskDataService.ktTaskDatasetList($scope.fd.Related,$scope.fd.PresentationId,$scope.fd.localVars.taskId).then(function(data) {
                 
                                    $scope.fd.localVars.widgetState.resourceWidgetDefinition= data.data.resourceDefinition;
                           
                                    $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columns;
                                    //debugger //ktTaskDatasetList
                                    
                                    
                                  /* for (let col of data.data.columns) {
                                        for (let item of data.data.data){
                                                item[col.name] = getItem(col ,item)
                                        }    
                                    }*/
                                    
                                   
                                    if ($scope.fd.GridState?.columnsDef?.length > 0)  
                                            $scope.fd.localVars.gridOptionsResourceData.columnDefs = $scope.fd.GridState.columnsDef
                                    // MIGOR TODO    KAD SE MIJENJA RESURS NA DATASET LISTI TREBA NULIRATI GRIDSTATE !
                                    
                                    //debugger  //prepare columnDefs //smarttable
                                    
                                    $scope.fd.fItemValue=data.data.widgets;
                                    if (data.data.widgetsCard!=null)  {
                                        $scope.fd.localVars.dashboardCard.widgets.layout =  data.data.widgetsCard;
                                    }
         
                                    $scope.fd.localVars.dashboard.widgets.layout =  data.data.widgets;
                                    $scope.fd.localVars.widgetState.widgets = $scope.fd.localVars.dashboard.widgets.layout;
                                   
                                    $scope.fd.localVars.widgetState.formData =  data.data.formData;
                                    $scope.fd.Search = $scope.fd.localVars.widgetState.formData;
                                    if ($scope.fd.Search==undefined) $scope.fd.Search={};

                                    $scope.fd.localVars.widgetState.sharedData = data.data
                                    $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
                                    
                                    //setup data grid or card
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')
                                    $scope.fd.localVars.gridOptionsResourceData.data = $scope.fd.localVars.TransformDataForCard(data.data.data);
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.localVars.nextPage = data.data.data.next;
                                    $scope.fd.localVars.previousPage = data.data.data.previous;
                                    $scope.fd.localVars.totalCount=data.data.data.count;
                                    $scope.fd.localVars.widgetState.currentPage=1;
                                    if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.widgetState.currentPage==0) $scope.fd.localVars.widgetState.currentPage=1;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/$scope.fd.localVars.widgetState.pageSize);
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                    $scope.fd.localVars.InitialLoad = false;
                                    $scope.fd.localVars.searchFormDownloaded = true;
                                    $scope.fd.localVars.formLoaded=true;
                                    
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                                   $scope.fd.localVars.resourceDataDownloading = false
                             });
                              
      };//getFormResource_END

      
  
    $scope.fd.localVars.restoreGridState= function() {
        //debugger
        if ($scope.fd['GridState']!==undefined && $scope.fd['GridState']!==null)
        {
          

            if ($scope.fd['GridState']['columnsDef']!==undefined) $scope.fd.localVars.gridOptionsResourceData.columnDefs =  $scope.fd['GridState']['columnsDef']
            if ($scope.fd['GridState']['columnsQuery']!==undefined) $scope.fd.localVars.columnsForQuery =  $scope.fd['GridState']['columnsQuery']
            if ($scope.fd.localVars.columnsForQuery===undefined) $scope.fd.localVars.columnsForQuery=[]
        }
    };
    
      $scope.fd.localVars.getResourceData = function () {

        // scope.activity.activityMessage = ProcessingService.setActivityMsg('Downloading resource data for '+scope.selectedResource);
        var lang = $rootScope.globals.currentUser.lang;
        $scope.fd.localVars.resourceDataDownloading = true;
        if ($scope.fd.localVars.widgetState.pageSize==undefined) $scope.fd.localVars.widgetState.pageSize = paginationOptions.pageSize;
        ResourcesDataService.getResourceData($scope.fd.localVars.selectedResourceId,$scope.taskId,$scope.fd.localVars.formType,lang).then(function(data) {
                               
                                     $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                                     $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                                     //debugger //getResourceData() //smarttable
                                     $scope.fd.localVars.restoreGridState();
                                     $scope.fd.localVars.InitialLoad = false;
                                     $scope.fd.localVars.resourceDataDownloaded = true;
                                     $scope.fd.localVars.resourceDataDownloading = false;
                                    
                                 },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                   $scope.fd.localVars.resourceDataDownloading = false;
                             });
        };

    //**********************************************************
    //           GET FORM FIELDS
    //**********************************************************      
            
          $scope.fd.localVars.getFormFields = function(){
                 
              var lang=$rootScope.globals.currentUser.lang;
                       
              ResourcesDataService.getFormFields($scope.fd.Related,lang).then(function(data) {
                                  $scope.fd.localVars.PresentationElements= data.data;
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                             });         
          };
          
          
          
          $scope.$watch('localVars.widgetState.itemValue.val',function(newValue,oldValue){
                  if (newValue!=oldValue)
                  {
                   //send data to package
                   let newValueSingle,newValueMultiple;
                   if ($scope.fd.Multiline)
                   {
                    newValueMultiple = newValue;
                    newValueSingle="";
                   }
                   else
                   {
                    newValueMultiple = {};
                    newValueSingle=newValue;
                   }
                   
                   $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
                   
                    WidgetDataExchangeService.setData({
                    DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:newValueSingle}
                        ,{name:'resourceRecordIds',value:newValueMultiple},{name:'resourceRecordsAll',value:$scope.fd.localVars.datasetItems}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
           
                  }
      });
        
       
       //**********************************************************
      //           GET DATASET DATA FOR DROPDOWN
      //**********************************************************
      var refresh_list_from_callback=function(data)
            {
                $scope.fd.localVars.getDatasetDataList(true);
                
            }
        $scope.filterOnClient = function(items)
        {
            let temp=[];
            let search;
            if (Array.isArray($scope.fd.Search) && $scope.fd.Search.length>0) search = $scope.fd.Search[0];
            else search = $scope.fd.Search;
            for (let i=0;i<items.length;i++)
            {
  
                for (let key in search) {
                    
                    
                    if (search[key]!=undefined)
                    {
                        if (key in items[i])
                        {
                            if (items[i][key]==search[key].id || search[key]==null || search[key]==undefined || items[i][key]==undefined)  temp.push(items[i]);
                        }else if (key in items[i])
                        {
                            if (items[i][key]==search[key] || search[key]==null || search[key]==undefined || items[i][key]==undefined)  temp.push(items[i]);
                        }else temp.push(items[i]);
                    }else temp.push(items[i]);
                }
                
                
            }
            return temp;
            
        }
        $scope.fd.localVars.getDatasetDataList = function(fromCache,clientSideFilter)
        {
                    if (fromCache==undefined) fromCache=true;
                    if (clientSideFilter==undefined) clientSideFilter=false;
                    var lang = $rootScope.globals.currentUser.lang;
                    $scope.fd.localVars.datasetItems = null;
                    
                    ResourcesDataService.getResourceDataListAll($scope.fd.Related,$scope.fd.PresentationId,lang,fromCache,refresh_list_from_callback).then(function(data) {
                   
                        if (clientSideFilter) $scope.fd.localVars.datasetItems = $scope.filterOnClient(data.data);
                        else $scope.fd.localVars.datasetItems = data.data;
                        $timeout(function(){
                            
                            //$scope.loadDropdown($scope.fd.localVars.datasetItems);
                        
                            if ($scope.fd.localVars.widgetState.itemValue.val!=undefined)
                            {
                            let pos = $scope.fd.localVars.datasetItems.map(function(e) { return e.id; }).indexOf($scope.fd.localVars.widgetState.itemValue.val.id);
                  
                                if (pos<0){
                                    //$scope.clearDropdown();
                                    $scope.fd.localVars.widgetState.itemValue.val = null;
                                }
                            }else
                            
                            {
                                //$scope.clearDropdown();
                                $scope.fd.localVars.widgetState.itemValue.val = null;
                            }
                        });
                        
                    },function(error){    
                        MessagingService.addMessage(error.msg,'error');
                    });
              
            
        };
        
        
        $scope.fd.localVars.GetDatasetDataClick = function()
        {
            if ($scope.fd.Parameters.PresentationType=='dropdown')
            {
               $scope.fd.localVars.getDatasetDataList(); 
            }else
            {
                $scope.fd.localVars.selectedResourceId=$scope.fd.Related;
                if ($scope.fd.Parameters.AutoSearchClient && $scope.fd.HasParentDataset && $scope.fd.PopulateParentMethod=='A')
                { 
                    
                    $scope.fd.localVars.GetResourceDataForParent()
                    
                }else $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,1,$scope.fd.localVars.widgetState.pageSize); 
            }
        };    
  

      //**********************************************************
      //           GET RESOURCE DATA PAGINATED
      //**********************************************************      
    $scope.fd.localVars.getResourceDataPaginated = function (urlNext,urlPrev,newPage,pageSize) {
        
        if ($scope.fd.localVars.gridOptionsResourceData.data==undefined) $scope.fd.localVars.gridOptionsResourceData.data = [];
        
      
        
        if (Math.ceil($scope.fd.localVars.totalCount/pageSize)<newPage)
        {
          newPage = Math.ceil($scope.fd.localVars.totalCount/pageSize);
        }
         
         var lang = $rootScope.globals.currentUser.lang;
          
         if (pageSize==undefined)
         
         {
          $scope.fd.localVars.widgetState.pageSize = paginationOptions.pageSize;
          pageSize= $scope.fd.localVars.widgetState.pageSize;
         }
         if ($scope.fd.PopulateMethod=='M')
         {
          
           
           $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
           $scope["datasetListSearchForm" + $scope.fd.PresentationId]?.$setSubmitted()
           let is_valid = $scope["datasetListSearchForm" + $scope.fd.PresentationId]?.$valid
           //let is_valid = $scope.datasetListSearchForm?.$valid
           
           if (is_valid)
           {
                 $scope.fd.localVars.resourceDataDownloading = true;
                 $scope.fd.localVars.gridOptionsResourceData.data.length=0;
                
                TaskDataService.ktTaskDatasetListFilter($scope.fd.Related,$scope.fd.PresentationId,$scope.fd.localVars.taskId,$scope.fd.localVars.widgetState.formData, $scope.fd.localVars.dashboard.widgets.layout,$scope.fd.localVars.columnsForQuery).then(function(data) {
                 
                           
                                    $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columns;
                                    //debugger //ktTaskDatasetList
                                   
                                   
                                    if ($scope.fd.GridState?.columnsDef?.length > 0)  
                                            $scope.fd.localVars.gridOptionsResourceData.columnDefs = $scope.fd.GridState.columnsDef
                 
                                    $scope.fd.localVars.widgetState.sharedData = data.data
                                    $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')
                                    $scope.fd.localVars.gridOptionsResourceData.data = $scope.fd.localVars.TransformDataForCard(data.data.data);
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.localVars.nextPage = data.data.data.next;
                                    $scope.fd.localVars.previousPage = data.data.data.previous;
                                    $scope.fd.localVars.totalCount=data.data.data.count;
                                    $scope.fd.localVars.widgetState.currentPage=1;
                                    if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.widgetState.currentPage==0) $scope.fd.localVars.widgetState.currentPage=1;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/$scope.fd.localVars.widgetState.pageSize);
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                    $scope.fd.localVars.InitialLoad = false
                                        
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                                   $scope.fd.localVars.resourceDataDownloading = false
                             });
                }else{
                 MessagingService.addMessage($scope.fd.ErrorMsg,'warning');
                 $timeout(function(){$scope.fd.localVars.resourceDataDownloading = false});
            }
         }
         else if ($scope.fd.PopulateMethod=='A')
         {
          var task_instance=0;  
          if ($scope.fd.localVars.formType=='i') task_instance = $scope.taskInitiateId;
          else task_instance = $scope.taskExecuteId;
           $scope.fd.localVars.resourceDataDownloading = true;
          ResourcesDataService.getResourceDataPaginatedAuto($scope.fd.localVars.selectedResourceId,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.PresentationId,task_instance,$scope.fd.localVars.formType).then(function(data) {
                                      
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                                    $scope.fd.localVars.restoreGridState();
                                  
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                    $scope.fd.localVars.nextPage = data.data.next;
                                    $scope.fd.localVars.previousPage = data.data.previous;
                                    $scope.fd.localVars.totalCount=data.data.count;
                                    $scope.fd.localVars.widgetState.currentPage=newPage;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/pageSize);
                                    $scope.fd.localVars.widgetState.pageSize = pageSize;
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    $scope.fd.localVars.InitialLoad = false
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLink',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.widgetState.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.PresentationId+"Completed",$scope.fd.localVars.gridOptionsResourceData.data);
                          
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.fd.localVars.resourceDataDownloading = false;
                             });
          
         }
        };


    $scope.fd.localVars.pageChanged = function(){
        
        $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);
    };
      
     
     
       $scope.fd.localVars.gridData = function() {

           return $scope.fd.localVars.gridOptionsResourceData.data
        }
      
      
      $scope.fd.localVars.getDefaultColumnsDef = function () {

        
        ResourcesDataService.getResourceColumns($scope.fd.Parameters.PublishedResourceId,$scope.fd.Parameters.SelectedResourceType,lang,$scope.fd.Parameters.PublishedResourceVersion).then(function(data) {//MIGOR
                            
                                $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columnsDef
                                $scope.fd.localVars.results.columns =  $scope.fd.localVars.gridOptionsResourceData.columnDefs
                                $scope.fd.localVars.resourceDataDownloaded = true;
                                
                                $scope.fd.localVars.saveGridState(false)
                                
                                     
                        },function(error){
                                MessagingService.addMessage(error.msg,'error');
        });

      }
      
     
     $scope.fd.localVars.getResourceDataFiltered = function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

           //scope.activity.activityMessage = ProcessingService.setActivityMsg('Retriving data '+scope.selectedResource);
          //$scope.filter = ResourcesDataService.prepareResourceWidgetData($scope.dashboard.widgets,-1);
         
          if ($scope.fd.localVars.selectedResourceId==null) {
            $scope.fd.localVars.selectedResourceId = $scope.fd.Related;
          }
          $scope.fd.localVars.resourceDataDownloading = true;
            //	ResourcesDataService.getResourceDataFiltered($scope.fd.Related,lang,$scope.filter).then(function(data) {//MIGOR
         ResourcesDataService.getResourceDataFiltered($scope.fd.localVars.selectedResourceId,$scope.taskId,$scope.fd.localVars.formType,lang,$scope.fd.localVars.dashboard.data).then(function(data) {//MIGOR
                            
                                $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                                $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                             
                                $scope.fd.localVars.resourceDataDownloaded = true;
                                $scope.fd.localVars.resourceDataDownloading = false;
                                $scope.fd.localVars.restoreGridState();
                                $scope.fd.localVars.InitialLoad = false
                                 
                                 
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                                $scope.fd.localVars.resourceDataDownloading = false;
                             });
        };
		
              
          $scope.fd.localVars.showHideSearchForm = function()
        {
     				//if ($scope.fd.ShowSearchForm) $scope.fd.localVars.getFormResource();
        };
       
    // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
	/*$scope.fd.localVars.getResources = function()  {

            if ($scope.fd.localVars.resourceList==undefined) $scope.fd.localVars.resourceList=[];
          
            var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
						function(data) {
										
											  $scope.fd.localVars.resourceList = data.data;
                                              $scope.fd.localVars.resourceList = data.data;
                       if ($scope.fd.Related!=null)
                       {
                        $scope.fd.localVars.setSelectedDataset($scope.fd.Related);
                
                        }     
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
	}*/

     //**********************************************************
     //           GET DATASET DIRECT FILTER
     //**********************************************************      
      $scope.fd.localVars.getDatasetDirect = function (item) {

        let children = [item[$scope.fd.Parameters.ParentDatasetFieldName]]
        $scope.fd.localVars.resourceDataDownloading = true 
        ResourcesDataService.getResourceDataDirectFilter($scope.fd.Related,$scope.fd.PresentationId,"i",lang,{id__in: children}).then(function(data) {
                                    
                                    $timeout(function(){
                                        $scope.fd.localVars.gridOptionsResourceData.data = data.data
                                        $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef
                                        $scope.fd.localVars.resourceDataDownloaded = true
                                        $scope.fd.localVars.resourceDataDownloading = false 
                                        $scope.fd.localVars.InitialLoad = false
                                    })
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');     
                                    $scope.fd.localVars.resourceDataDownloading = false           
                             });
        };


    
    
      /* Get children for parent*/
        $scope.fd.localVars.GetResourceDataForParent = function () 
        {   
              $scope.fd.localVars.resourceDataDownloading = true
              var lang = $rootScope.globals.currentUser.lang;
               $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDefSearch?.SearchForm
             
                ResourcesDataService.getResourceDataM2mFilter($scope.fd.PresentationId,$scope.fd.ParentDatasetDefId,$scope.fd.Related,$scope.fd.localVars.ParentRecordId,$scope.fd.localVars.widgetState.formData, $scope.fd.localVars.dashboard.widgets.layout,$scope.taskId,0,lang).then(function(data) {
                     
                                $scope.fd.localVars.gridOptionsResourceData.data = data.data
                                $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef
                                $scope.fd.localVars.resourceDataDownloaded = true
                                $scope.fd.localVars.resourceDataDownloading = false 
                                $scope.fd.localVars.InitialLoad = false
                       
                                           
                },function(error){    
                    MessagingService.addMessage(error.msg,'error');
                     $scope.fd.localVars.resourceDataDownloading = false 
                    
                                    });              
        }

        let refresh_data=function(data)
        {
            let state = data.state
            
            if (state!==undefined)
            {
                $scope.fd.ParentDatasetDefId = state.datasetDefId
                $scope.fd.localVars.ParentRecordId = state.recordId
                if ($scope.fd.Parameters.ParentRelationshipType==0) $scope.fd.localVars.GetResourceDataForParent()
                else $scope.fd.localVars.getDatasetDirect(state.record)
            }

        }
    if ($scope.fd.Parameters.AutoSearchClient && $scope.fd.HasParentDataset && $scope.fd.PopulateParentMethod=='A')
    {
        

        //setup here state manager
        let dependent_field = valueStateName
        let subscribedSubject = $scope.resourceDefinition.page_state_id+'-'+$scope.fd.SrcParentWidgetId;
        let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
        let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",refresh_data);
        let val = $scope.ksm.getState(subscribedSubject);
        if (val.state!==undefined && val.state!==null)
        {   
            let parent_item  = val.state
            $scope.fd.ParentDatasetDefId = parent_item.datasetDefId
            $scope.fd.localVars.ParentRecordId = parent_item.recordId
            if ($scope.fd.Parameters.ParentRelationshipType==0) $scope.fd.localVars.GetResourceDataForParent()
            else $scope.fd.localVars.getDatasetDirect(parent_item.record)
    
        }
        
    }

    $scope.fd.localVars.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData = WidgetDataExchangeService.getPrevTaskData($scope.prevTaskExeId);
            var datasetId=0,datasetRecordId=0;
            let action;
            for (let k=0;k<pTaskData.length;k++)
            {
                if (pTaskData[k].DEI!=undefined && pTaskData[k].DEI!=null)
                {
                for (var i=0;i<pTaskData[k].DEI.length;i++)
                {
                  if (pTaskData[k].DEI[i].name==linkType)
                  {
                    if (pTaskData[k].DEI[i].value!=undefined && pTaskData[k].DEI[i].value!=null)
                    {
                      //populate selected item
                     
                      for (var j=0;j<pTaskData[k].DEI[i].value.length;j++)
                      {
                       if (pTaskData[k].DEI[i].value[j].name=="resourceId" && pTaskData[k].DEI[i].value[j].value!=undefined) datasetId=pTaskData[k].DEI[i].value[j].value;
                       if (pTaskData[k].DEI[i].value[j].name=="resourceRecordId" && pTaskData[k].DEI[i].value[j].value!=undefined)
                       {
                            datasetRecordId=pTaskData[k].DEI[i].value[j].value;
                            if ($scope.fd.Parameters.PresentationType == 'dropdown' && datasetId==$scope.fd.Related && !$scope.fd.Multiline)
                            {
                        
                                $scope.fd.localVars.widgetState.itemValue.val = pTaskData[k].DEI[i].value[j].value;
                                action = $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
                            }
                       }
                       if (pTaskData[k].DEI[i].value[j].name=="resourceRecordIds" && pTaskData[k].DEI[i].value[j].value!=undefined)
                       {
                            datasetRecordId=pTaskData[k].DEI[i].value[j].value;
                            if ($scope.fd.Parameters.PresentationType == 'dropdown' && datasetId==$scope.fd.Related && $scope.fd.Multiline)
                            {
                        
                                $scope.fd.localVars.widgetState.itemValue.val = pTaskData[k].DEI[i].value[j].value;
                                action = $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
                            }
                       }
                        
                      }
                         
                        
                    }
                      
                  }
                    
                  }
                }
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };


  $scope.fd.localVars.previousTaskData=$scope.fd.localVars.getPreviousTaskWidget("resourceLinkDatasetList");
 
 
  $scope.fd.localVars.patchDashboard = function (obj, destId, patchData)     
     { // sample function for recursively patching dashboard object
        var o = {};
        Object.keys(obj).forEach(function(key) {
          
            if ( (typeof(obj[key]) === 'object') && obj[key]   )
            {       
              $scope.fd.localVars.patchDashboard(obj[key], destId, patchData)
            } 
            else {     
                    if (key == 'AngularDirectiveName' || key == 'ContainerId' || key == 'ElementType' || key == 'FieldName' || key == 'Name'  || key == 'OriginalPresentationElementId') {  
                        o[key]= obj[key];
                        }
                    if ( key == 'NameField' ) {
                       //debugger
                        if (Array.isArray(obj[key]) ) o[key]= obj[key][0];
                        else o[key]= obj[key];
                    }   
                    if (key == 'PresentationId' ) {
                        o['id']= obj[key];
                    }    
                    
                }
            }) ; 
        
        //if (Object.keys(o).length > 0) ..do somoething with o   
    }; 


  $scope.fd.localVars.saveGridState= function(show_message) {
               //debugger //ktTaskDatasetList smarttable
               let columnsDef = $scope.fd.localVars.gridOptionsResourceData.columnDefs

               let columnsQuery = $scope.fd.localVars.columnsForQuery
               $scope.fd['GridState']={'columnsDef':columnsDef,'columnsQuery': columnsQuery}
                                
               //var destId=$scope.fd.LocalId
                
                //find corresponding columns in Presentation elements
                /*
                for (let i=0; i < $scope.fd.fItemValue[0].layout[0].layout.length;i++) {
                    for (let j=0; j< state.length;j++) {
                        if (state[j]['id'] == $scope.fd.fItemValue[0].layout[0].layout[i]['id']) {
                                $scope.fd.fItemValue[0].layout[0].layout[i]['Parameters']['GridState'] = state[j]
                        }
                        
                    }
   
                }
              */
               //var res = $scope.fd.localVars.patchDashboard (s, $scope.fd.LocalId, state)
              if (show_message)
               MessagingService.addMessage("To make changes stick please save form",'info');            
    };
    
    /*********************************** preview and dowlload files functions *************************/
    /*********************************** preview and dowlload files functions *************************/
    /*********************************** preview and dowlload files functions *************************/
     $scope.fd.localVars.downloadFile = function(fileId,fileName){
                ResourcesDataService.downloadFile($scope.fd.Related,fileId).then(function(data) {
                               
                                    MessagingService.addMessage('File '+fileName+' downloaded','info');
                                    var url = URL.createObjectURL(new Blob([data.data]));
                                    var a = document.createElement('a');
                                    a.href = url;
                                    a.download = fileName;
                                    a.target = '_blank';
                                    a.click();
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// dowload file END
      
      
       $scope.fd.localVars.previewFile = function(fileId,fileName,fileType){
           
            ResourcesDataService.downloadFile($scope.fd.Related,fileId).then(function(data) {
                               
                                    var url = URL.createObjectURL(new Blob([data.data],{type:fileType}));
                                    let new_doc = window.open(url,"_blank");
                                  
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// preview file END
     /*********************************** preview and dowlload files functions *************************/
      /*********************************** preview and dowlload files functions *************************/
       /*********************************** preview and dowlload files functions *************************/


     if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
    var lang=$rootScope.globals.currentUser.lang;

              
      ResourcesDataService.GetPublishedResourcesByType($scope.fd.Parameters.SelectedResourceType,'form').then(
        function (data) {  
            if ($scope.fd.localVars===undefined) $scope.fd.localVars={}     
           $scope.fd.localVars.publishedResourceList = data.data;
           //debugger  //GetPublishedResourcesByType   
                        
           if ($scope.fd.Parameters.PublishedResourceId!=null) 
                                        $scope.fd.localVars.setSelectedDataset($scope.fd.Parameters.PublishedResourceId,true);   
           },function(error){    
                          MessagingService.addMessage(error.msg,'error');
    });
    
    
    
    
          ResourcesDataService.getResourceDefListByType($rootScope.globals.currentUser.lang,'file').then(
                        function(data) {			
							$scope.fd.localVars.listOfFileTypes = data.data;
                         }
                        ,function(error){   MessagingService.addMessage(error.msg,'error')	}
            );
    

    if ($scope.fd.Related!=null &&  !$scope.fd.localVars.formDefinition && !$scope.fd.Parameters.DownloadAllRecords && !$scope.fd.HasParentDataset) {
                  
                  //$scope.fd.localVars.getFormResource();
                  if (!$scope.fd.localVars.loadFromState && !$scope.fd.Parameters.AutoSearchClient && $scope.fd.AutoSearch) $scope.fd.localVars.executeDatasetList();
                  else if  ($scope.fd.localVars.loadFromState || ($scope.fd.localVars.searchFormDownloaded && $scope.fd.Parameters.AutoSearchClient))
                  {
                    if ($scope.fd.Parameters.PresentationType=='table') $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);
                    if ($scope.fd.Parameters.PresentationType=='dropdown')
                    {
                        $scope.fd.localVars.getDatasetDataList(true,$scope.fd.Parameters.ClientSideFilter);
                    }
                    if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
                  
                  }
                  else if  (!$scope.fd.localVars.searchFormDownloaded && $scope.fd.Parameters.AutoSearchClient)
                  {
                   // $scope.fd.localVars.getFormResource();
                    $scope.fd.localVars.showHideSearchForm();
                   
                  }
                  //if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
                  
        }else if ($scope.fd.Related!=null  &&  !$scope.fd.localVars.formDefinition && !$scope.fd.localVars.searchFormDownloaded && $scope.fd.Parameters.DownloadAllRecords  && !$scope.fd.Parameters.AutoSearchClient)
                {
                  //$scope.fd.localVars.getFormResource();
                  if ($scope.fd.Parameters.PresentationType=='table') $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);
                  if ($scope.fd.Parameters.PresentationType=='dropdown')
                  {
                    $scope.fd.localVars.getDatasetDataList(true,$scope.fd.Parameters.ClientSideFilter);
                   // $scope.fd.localVars.getFormResource();
                  }
                  if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
                
                }
        else if ($scope.fd.Related!=null  && ($scope.fd.localVars.datasetMapperType!=5 || $scope.fd.Parameters.AutoSearchClient))
        {
           //$scope.fd.localVars.getFormResource();
           if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
           $scope.fd.localVars.restoreGridState();
           $scope.fd.localVars.showHideSearchForm();

          
        }
                  
                  
        if ($scope.fd.localVars.formDefinition && $scope.fd.Related>0) $scope.fd.localVars.getFormFields(); 
        
   $rootScope.$broadcast("Widget"+$scope.fd.PresentationId+"Ready",null);
   
   
   $scope.fd.localVars.addAnalytics = function()
   {
        if ( $scope.fd.Parameters.Analytics == undefined)   {
                $scope.fd.Parameters.Analytics =   []
        }
        $scope.fd.Parameters.Analytics.push( { CalculateFunction:'', AggregateField:'', CalculateField:'', Show:true })
       
   }
   
   
        
}]; //controler_END

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskDatasetList.html',
            controller: controller,
            link: function (scope, elem, attrs) {
                scope.fd.localVars.datasetlistlinked = true
     
            
             }
             
             
  } 
        
};
             
                                       
              
            