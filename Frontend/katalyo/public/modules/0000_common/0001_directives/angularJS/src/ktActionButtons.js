'use strict';


export function ktActionButtons () {

        return {
            restrict: 'A',
            
            controller: function($scope,UserService,ResourcesDataService,TaskDataService,MessagingService,WidgetDataExchangeService,$rootScope,GridsterService,CODES_CONST,$stateParams,$state,KatalyoStateManager,$templateCache,$compile,$element,$timeout)
            {
                $scope.gsrvc= GridsterService;
                $scope.CTRLNAME="ktActionButtons"
                if ($scope.fd.localVars===undefined) $scope.fd.localVars = {}
                $scope.fd.localVars.actionTypes = [
                 {id:0,name: "Execute task", value: "executetask"},
                  {id:1,name: "Start task", value: "runtask"},
                  {id:2,name: "Show/Hide elements", value: "elemvis"},
                  {id:3,name: "Refresh task",  value: "refreshtask"},
                  {id:4, name: "Complete task", value: "completetask"},
                  {id:5,name: "Widget function",  value: "widgetfn"},
                  {id:6, name: "Close form", value: "closeform"},
                  {id:7, name: "Clear form", value: "clearform"},
                  {id:8, name: "Open Page", value: "openpage"},
                  {id:9, name: "Initiate task", value: "initiatetask"}
                ]
                
                $scope.fd.localVars.actionButtonClass=['btn-xs','btn-sm','','btn-lg']
                $scope.fd.localVars.actionButtonSize=['Extra Small','Small','Normal','Large']
                        
                
                
                
                
                $scope.fd.localVars.showMenu = false
                $scope.taskId=$scope.resourceDefinition.ResourceDefId

                if ($scope.fd.FormDef===undefined || $scope.fd.FormDef===null || $scope.fd.FormDef==="") $scope.fd.FormDef=[];
      
                
                  $scope.fd.localVars.toggleDropdown = function(){
                    $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                    }
                
                 if ($scope.resourceDefinition!=undefined)
                {
                  $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
                  $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                  $scope.fd.localVars.resourceDefinition = $scope.resourceDefinition;
                  $scope.fd.localVars.previousTask = $scope.taskExecuteId;
                  if ($scope.fd.localVars.previousTask==undefined) $scope.fd.localVars.previousTask = $scope.taskInitiateId;
                }else $scope.resourceDefinition = $scope.fd.localVars.resourceDefinition;

                
                let widgetDefinition = {name: 'actionBtns',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.fd.localVars.resourceDefinition?.ResourceDefId };
                $scope.fd.localVars.LocalId=WidgetDataExchangeService.registerWidget(widgetDefinition);
                
                /*********** SETUP KATALYO STATE MANAGER *************************/
                $scope.ksm = KatalyoStateManager;
                let initiatedTaskId=null;
                let initiateTaskState= $scope.ksm.addSubject("initiateTaskState"+$scope.fd.UuidRef,initiatedTaskId);
                let initiateTaskStateName = initiateTaskState.subject.name;
                
                let task_initiated = function(data)
                {
                    initiateTaskState.subject.setStateChanges = data;
                    initiateTaskState.subject.notifyObservers("task_initiated");
                    
                };
                
                let action_task_initiated = $scope.ksm.addAction(initiateTaskStateName,"task_initiated",task_initiated);
               
      
             
                
                 $scope.fd.localVars.onSelectFn = function(item,model) {
                    //debugger  
                     $scope.fd.localVars.selectedButton.action = item
                 }
    
                
                $scope.GetOutcomes = function(){
                                
                              var lang=$rootScope.globals.currentUser.lang;               
                              var outcomeId = 0                /*MIGOR provjeriti jos*/  
                             
                              if ($scope.fd.localVars.resourceDefinition?.TaskOutcomeHeadId !=undefined) outcomeId = $scope.fd.localVars.resourceDefinition.TaskOutcomeHeadId;
                              

                              if (!$scope.outcomesDownloaded && outcomeId>0)
                               {   
                                    TaskDataService.GetTaskOutcomes( $scope.fd.localVars.resourceDefinition?.ResourceDefId,outcomeId,lang).then(function(data) {            
                                               
                                            $scope.fd.localVars.outcomes = data.data;
                                            $scope.outcomesDownloaded = true;
                                             
                                             
                                    },function(error){
                                             
                                             MessagingService.addMessage(error.msg,'error');
                                    });
                               }
                               else
                               {
                                $scope.loadingActions=false;      
                               }
                 
                }//GetOutcomes
                
                if ($scope.fd.localVars.outcomes==undefined)  {
                    $scope.GetOutcomes();   
                }
                
                $scope.fd.localVars.loadedTasks = [];
                if ($scope.fd.localVars.actionButtonProps==undefined)
                {
                    $scope.fd.localVars.actionButtonProps={};
                }
                
                $scope.fd.localVars.actionButtonProps[$scope.fd.PresentationId]=0;  

                $scope.fd.localVars.formType=$scope.resourceDefinition?.form_type;
                //var taskDefId = $stateParams.id;
                var lang=$rootScope.globals.currentUser.lang;
                $scope.fd.localVars.taskListParameters = {placeholder:'Select or search task...',label:$scope.fd.placeholder,name:'task',
                  paddingLeft:true,selectedItem:null,hideLabel:true,columnSizeLabel:'col-lg-24',dropdownSize:'col-lg-24',onSelectFunction:''};
              
                $scope.fd.localVars.selectedTaskIndex=-1;
                
                
                let process_event_task_action = function(data)
                {
                    if ($scope.fd.localVars.clickedButton!==undefined && $scope.fd.FormDef.length>$scope.fd.localVars.clickedButton){

                        for (let j=0;j<$scope.fd.FormDef[$scope.fd.localVars.clickedButton].visibilityAfterAction.length;j++)
                        {
                            $scope.fd.FormDef[j].hideButton = !$scope.fd.FormDef[$scope.fd.localVars.clickedButton].visibilityAfterAction[j].visible;
                        }
                     }
                }
                  
                let dependent_field1 = "Field"+$scope.fd.PresentationId;
                let subscribedSubject1 = "task"+$scope.resourceDefinition?.transaction_id
                let observer1 = $scope.ksm.subscribe(initiateTaskStateName,dependent_field1,"task_initiated",process_event_task_action);
                let subscribedSubject2 = "executeTaskStateName"+$scope.resourceDefinition?.transaction_id
                let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field1,"task_executed",process_event_task_action);

                
                                            
                  $scope.$on('InlineTaskClosed',function(event,data){
                    //var index=data;
                    //$scope.fd.loadedTasks.splice(index,1);
                    var taskId=data;
                    var pos = $scope.fd.localVars.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (pos>=0)
                    {
                      $scope.fd.localVars.loadedTasks.splice(pos,1);  
                    }
                });
                  
                  
                $scope.fd.localVars.LoadTask=function(taskId,outcomeId,previousTask,openTaskType,button)
                {
                    
                    var localVars = {'isInlineTask':$scope.fd.isInlineTask,'OpenTaskType':openTaskType,'taskIndex':$scope.fd.localVars.loadedTasks.length,'loadTaskDirect' : true,'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,};
                    
                    localVars.ShowCloseBtn = button.ShowCloseTaskBtn;
                    var pos = $scope.fd.localVars.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (pos<0 && openTaskType=="current")
                    {
                        $scope.fd.localVars.loadedTasks.splice(0,0,{'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                        $scope.ksm.executeAction(initiateTaskStateName,"task_initiated", {'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                         
                        
                    }else if (openTaskType=="inline")
                    {
                        $scope.fd.localVars.loadedTasks.length=0;
                        $scope.fd.localVars.loadedTasks.push({'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                        $scope.ksm.executeAction(initiateTaskStateName,"task_initiated", {'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                                
               
                    }else if (openTaskType=="container")
                    {
                        localVars = {'isInlineTask':$scope.fd.isInlineTask,'OpenTaskType':'current','taskIndex':0,'loadTaskDirect' : true,'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,};
                    
                         $scope.ksm.executeAction(initiateTaskStateName,"task_initiated", {'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                                
                    }
                    $scope.gsrvc.setOpenTaskType(openTaskType);
                    
                }
                  
                
                $scope.fd.localVars.copyToClipboard = function(text){
                    $scope.fd.localVars.copyUuid = true;

                    navigator.clipboard.writeText(text).then( 
                        () => {
                            $timeout(function(){$scope.fd.localVars.copyUuid = false;},1000);
                        },
                        () => {    /* clipboard write failed */
                            MessagingService.addMessage('Copy to clipboard failed','error');
                            $timeout(function(){$scope.fd.localVars.copyUuid = false;},1000);
                    });
                                    
                }

                $scope.fd.localVars.ProcessOutcome = function (outcome,form){
                
                    for (var i=0;i<outcome.actions.length;i++) {
                      if (outcome.actions[i].type==1 || outcome.actions[i].type==2)
                      {
                         $scope.fd.localVars.InitiateTaskFromOutcome(outcome.actions[i].taskId,outcome.id1,outcome.actions[i].type,outcome.actions[i].actionParams,$scope.formData,outcome.actions[i].actionParams.OpenTaskType);
                             
                      }
                     
                    }
                    if (outcome.actions.length==0)
                    {
                       $scope.$emit("ExecuteTask"+$scope.resourceDefinition?.task_execute_id,{form:form,outcomeId:outcome.id1}); 
                      
                    }
                     
                };
                 
                 
                 
              $scope.fd.localVars.InitiateTaskFromOutcome = function (taskDefId,outcomeId,type,actionParams,exeForm,openInNewPage){
                      //save task instance form for later processing
                     
                      if (openInNewPage==undefined) openInNewPage='inline';
                      if ($scope.resourceDefinition?.task_execute_id!=undefined)
                      {
                      TaskDataService.saveTaskFormData($scope.resourceDefinition?.id,exeForm).then(function(data) {
                               if (type==2)
                               {
                                       $state.go('app.menu.tasks.proc',{id: taskDefId,fromStateName:$state.current.name,
                                      fromStateParams:$state.params,prev_task_execute_id:$scope.resourceDefinition?.task_execute_id,outcome_id:outcomeId,presentation_id:0});
                               }
                               if (type==1)
                               {
                
                                
                                  $scope.fd.localVars.fromOutcome=true;
                                  if (taskDefId!=undefined &&  taskDefId>0) $scope.fd.localVars.LoadTask(taskDefId,outcomeId,$scope.resourceDefinition?.task_execute_id,openInNewPage);          
                               }
                                 },function(error){
                                   MessagingService.addMessage(error.msg,'error');

                             });

                      }else
                      {
                         MessagingService.addMessage("Invalid task data --> Task execution object is undefined",'error');
                      }
                };
                 
                
             
               
                $scope.fd.localVars.ProcessButtonAction=function(button,event,previousTask,index)
                {
                    
                    let old_clicked_btn = $scope.fd.localVars.clickedButton
                    $scope.fd.localVars.clickedButton = index
                   

                    if (button.action.value=='completetask')
                    {
                     
                      $scope.fd.localVars.ProcessOutcome(button.outcome,event.target.form);
                     // $scope.$emit("ExecuteTask"+$scope.resourceDefinition.task_execute_id,{form:event.target.form,outcomeId:button.outcome.id1}); 
                    }
                    else if (button.action.value=='runtask')
                    {
                       if (old_clicked_btn!==$scope.fd.localVars.clickedButton) $scope.fd.localVars.LoadTask(button.task.id,0,previousTask,button.OpenTaskType,button);
                    }
                    else if (button.action.value=='closeform')
                    {
                       
                         
                       $scope.ksm.executeAction(taskStateName,"task_closed");
                       $scope.$emit("CloseTaskForm"+$scope.resourceDefinition?.transaction_id,$scope.resourceDefinition?.ResourceDefId);
                       
                        
                    }
                    else if (button.action.value=='refreshtask')
                    {
                       $scope.fd.localVars.taskInitiated=false;
                       $scope.$emit("RefreshTask"+$scope.resourceDefinition?.ResourceDefId,$scope.resourceDefinition?.ResourceDefId); 
                    }
                    else if (button.action.value=='executetask')
                    {
                       let taskStateName = "task"+$scope.resourceDefinition?.prev_task_transaction_id
                       $scope.fd.localVars.taskInitiated=false;
                       $scope.$emit("ExecuteTask"+$scope.resourceDefinition?.task_execute_id,{form:event.target.form,outcomeId:null}); 
                    }
                    else if (button.action.value=='taskstatus')
                    {
                        
                    }
                     else if (button.action.value=='clearform')
                    {
                       
                       
                       let widgetStateName = "taskWidget"+button.widgetId;
                       let observer3 = $scope.ksm.subscribe(widgetStateName,dependent_field,button.action.value,process_event_task_action);
                       $scope.ksm.executeAction(widgetStateName,button.action.value);
                       

                       
                        
                    }
                    else if (button.action.value=='openpage')
                    {
                        $state.go(button.page_def.ResourceExtended.Target,{pageid: button.page.id,parameters:{prev_task:$scope.resourceDefinition,use_state:button.use_state}});
                    }
                    else if (button.action.value=='backbutton')
                    {
                        $window.history.back();
                    }
                
                  
                };
                
                
              
                $scope.fd.localVars.onSelectPageCallback=function(item,model)
                {
                    
                    if (item?.id!=undefined && $scope.fd.localVars.selectedButton!=undefined)
                    {
                        ResourcesDataService.getResourceDefAll(item.id).then(function(data) { 
                                    $scope.fd.localVars.selectedButton.page_def = data.data;  
                                  
                        })
                    }
                }

                 $scope.fd.localVars.getNewActionButton = function()
                 {
                    var newButton = {
                        action: $scope.fd.localVars.actionTypes[0],
                        btnLabel: "New button",
                        btnClass: "btn-outline-primary",
                        size:"Normal",
                        textAlign:"left",
                        visibilityAfterAction:$scope.fd.localVars.populateButtons(),
                    }
                    
                    return newButton
                    
                 
                 };

                 $scope.fd.localVars.populateButtons = function()
                {
                        let btns = []
                        for (let i=0;i<$scope.fd.FormDef.length;i++)
                        {
                            btns.push({'index':i,'visible':true})
                        }
                        return btns
                }
                    

                $scope.fd.localVars.addShowHideVars = function(index)
                {
                        for (let i=0;i<$scope.fd.FormDef.length;i++)
                        {
                        $scope.fd.FormDef[i].visibilityAfterAction.push({'index':index,'visible':true})
                        }
                }
                  
                  
                  $scope.fd.localVars.taskSelected = function(item,model)
                  {
                    $scope.fd.localVars.taskListParameters.selectedItem = item;
                    $scope.fd.localVars.selectedTask={};
                  };
                  
                   $scope.fd.localVars.taskIsOpen = function(taskId)
                  {
                    var isOpenPos = $scope.fd.localVars.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (isOpenPos>=0) return true;
                    else return false;
                  };
                 
                   $scope.fd.localVars.showButtonDefinition = function (index){
                    
                        //debugger  //showButtonDefinition
                   
                        $scope.fd.localVars.selectedButtonIndex=index;
                        
                        $scope.fd.localVars.selectedButton = $scope.fd.FormDef[index];
                        for (let i =0;i<$scope.fd.localVars.actionButtonClass.length;i++)
                        {
                            if($scope.fd.localVars.actionButtonClass[i]===$scope.fd.localVars.selectedButton.size) $scope.fd.localVars.selectedButtonSize = i
                        }
                        
                        $scope.fd.localVars.actionButtonProps[$scope.fd.PresentationId] = 1

                   };
                   
                    $scope.fd.localVars.toggleButtonDefinition = function (index){
                    
                         
                            $scope.fd.localVars.selectedButtonIndex=index;
                            $scope.fd.localVars.selectedButton = $scope.fd.FormDef[index];
                            if ($scope.fd.localVars.actionButtonProps[$scope.fd.PresentationId] !=1)
                                $scope.fd.localVars.actionButtonProps[$scope.fd.PresentationId] = 1
                            else    
                                $scope.fd.localVars.actionButtonProps[$scope.fd.PresentationId] = 0 //toggle
                   };
                   
                  
                  $scope.fd.localVars.addButtonToList = function (){
                 
                     
                            $scope.fd.FormDef.push( $scope.fd.localVars.getNewActionButton()) 
                            $scope.fd.localVars.addShowHideVars($scope.fd.FormDef.length-1)
                            $scope.fd.localVars.showButtonDefinition($scope.fd.FormDef.length-1);
               
                     
                 };
                  
                  $scope.fd.localVars.clearVisibilityParams=function(index)

                  {
                    for (let i =0;i<$scope.fd.FormDef.length;i++)
                    {
                            let temp = []
                            for (let j =0;j<$scope.fd.FormDef[i].visibilityAfterAction.length;j++)
                            {

                            if($scope.fd.FormDef[i].visibilityAfterAction[j].index!==index) temp.push($scope.fd.FormDef[i].visibilityAfterAction[j])

                            }
                        $scope.fd.FormDef[i].visibilityAfterAction=temp
                    }
                        

                  }
                     
                   $scope.fd.localVars.RemoveButtonFromList = function (index)
                   { 
                 
                      if (index>=0)
                           $scope.fd.FormDef.splice(index,1);
                                                
                      $scope.fd.localVars.clearVisibilityParams(index)
                      if ($scope.fd.FormDef.length > 0) 
                            $scope.fd.localVars.showButtonDefinition($scope.fd.FormDef.length - 1)

                      
                    }
          
          
                 $scope.fd.localVars.saveTaskToResource = function(tdid,tei,datasetDefId,datasetRecordId){
                          TaskDataService.saveTaskToResource(tei,datasetDefId,datasetRecordId,$scope.fd.presentationId).then(function(data) {
                                                    
                              $state.go('app.menu.tasks.initiate',{id: tdid,fromStateName:$state.current.name,fromStateParams:$state.params,outcome_id:0,prev_task_execute_id:tei});
                                                             
                                
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
             }
             
             if   ($scope.gsrvc.getFormViewType()==1 || $scope.fd.ElementType=='itask')
             {
            
                ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
                                                    
                                $scope.fd.localVars.taskList = data.data;
                                $scope.fd.localVars.tasksLoaded=true;
                                
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
                 }

              ResourcesDataService.getResourceDefListByType(lang,'page').then(function(data) {
                                                    
                                  $scope.fd.localVars.pageList = data.data;
                                                               
                                $scope.fd.localVars.pagesLoaded=true;
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
              
            let template;
            if ($scope.resourceDefinition?.form_definition) template = $templateCache.get('ktdirectivetemplates/ktActionButtons.html');
            else template = $templateCache.get('ktdirectivetemplates/ktActionButtonsExe.html');
          
            $element.append($compile(template)($scope));  

                },
            link: function (scope, elem, attrs) {
                
        }
    }
              
};