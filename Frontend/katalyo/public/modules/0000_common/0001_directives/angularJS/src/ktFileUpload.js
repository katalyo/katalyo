'use strict';

export function ktFileUpload($timeout,WidgetDataExchangeService,ResourcesDataService,TaskDataService) {
        return {
        restrict: 'A', 
             templateUrl: 'ktdirectivetemplates/ktFileUpload.html',
             controller: function ($scope, $element,$rootScope,$uibModal,MessagingService,ProcessingService,$log,GridsterService,$timeout,$cookies,$stateParams) {

                //console.log("U kontroleru");
                $scope.gsrvc = GridsterService;
                $scope.fd.ReadOnly=false;
                $scope.animationsEnabled = true;
                $scope.showFileMetaData=false;
                $scope.taskId=$scope.resourceDefinition.ResourceDefId;
                if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
                if ($scope.taskId==undefined) $scope.taskId=0;  
                if ($scope.localVars==undefined)  $scope.localVars = {datasetMapperType:0};
                $scope.fd.files = [];              
                $scope.localVars.showProperties=function(datasetMapperType)
                {
                 
                 if ($scope.localVars.datasetMapperType==undefined)
                 {
                    $scope.localVars.datasetMapperType = datasetMapperType;
                 }else
                 {
                    if ($scope.localVars.datasetMapperType==0 || $scope.localVars.datasetMapperType!=datasetMapperType) $scope.localVars.datasetMapperType = datasetMapperType;
                    else $scope.localVars.datasetMapperType=0;
                 }
                 
                };
            
            
             $scope.localVars.okFileConnector = function () {
                $timeout(function() {
                $scope.fd.tItemValue=$scope.fd.listOfConnectors;
                $scope.localVars.showProperties(0);
                
                });
            };
            
             $scope.localVars.cancelFileConnector = function () {
                $timeout(function() {
                $scope.fd.listOfConnectors=angular.copy($scope.fd.tItemValue);
                $scope.localVars.showProperties(0);
                
                });
            };

            
		  $scope.okModal = function (windex,index) {
			$timeout(function() {$scope.showFileMetaData=false; });
		  };

		  $scope.cancelModal = function () {
			$timeout(function() {$scope.showFileMetaData=false; });
		  };
		  
       
        
		 $scope.showMetadata = function(fileIndex,fileId){ 
		 
				   var lang=$rootScope.globals.currentUser.lang;
				   var file_resource_type = "file";
				    $scope.taskId=$stateParams.id;
				   
				    //ResourcesDataService.getResourceFormextended(id resursa ,task-id, i, $scope.fd.presentationId,lang)
					
				   ResourcesDataService.getResourceForm(fileId,"i",lang).then(function(data) {
                               
                                 var sc=$scope;
                                  //$scope.gridOptions.data = data.data;
								  //$scope.fd.files[fileIndex].dashboard=[];
								  $scope.fd.files[fileIndex].dashboard=data.data;
								  //files[$index].dashboard.widgets
								  
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
						});
                            
		

			  };
     
           
			// OPEN MODAL filehelper
			 $scope.openModal = function (size,fileIndex) {
                  var modalInstance = $uibModal.open({
                  animation: $scope.animationsEnabled,
                  templateUrl: 'filehelper.html',
                  controller: 'FileModalInstanceCtrl',
                  size: size,
                  resolve: {
                    retValue: function () {
                      return {resourceId:$scope.resourceId,fileIndex:fileIndex,files:$scope.fd.files,headline:'Enter file metadata'};
                    }
                  }
            });
			
			
            modalInstance.result.then(function (retValue,windex,index) {
             // $scope.getResourceForm ();
             
         
          }, function () {
          //$log.info('Modal dismissed at: ' + new Date());
          });
       
		};
        
      
      //novi FILE CONNECTOR - UPLOADER
      $scope.localVars.openFileConnector  = function(){
             //$scope.fd.showFileConnector= !$scope.fd.showFileConnector;
            $scope.localVars.showProperties(1);
             if ($scope.localVars.datasetMapperType==1) {
                           
                 //POPUNI LISTU DATASETOVA            
                  ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
                                function(data) {
                                                  $scope.datasetList = data.data;
                                                  //dodaj ime dataseta u konektore
                                                  if ($scope.fd.listOfConnectors!=undefined) 
                                                             for (var i=0;i< $scope.fd.listOfConnectors.length;i++) {
                                                                   //var p = $scope.datasetList.map(function(e) { return e.id; }).indexOf($scope.fd.listOfConnectors[i].parentDatasetDefId);
                                                                   var p = $scope.datasetList.map(function(e) { return e.id; }).indexOf($scope.fd.listOfConnectors[i].parentDatasetDefId);
                                                                   if (p>=0) $scope.fd.listOfConnectors[i].parentDatasetName = $scope.datasetList[p].name;  
                                                             }
                                                  
                                                  },function(error){    
                                        MessagingService.addMessage(error.msg,'error');
                                                 });
                                                 
                                                 
 							if ($scope.fd.tItemValue==undefined || $scope.fd.tItemValue[0]==null) $scope.fd.tItemValue = []; 
							// ovo je  Kt-UPLOADER
							  $scope.fd.listOfConnectors= WidgetDataExchangeService.getRegisteredWidgets();//MIGOR  - uzmi sve registrirane widgete
							  
							  //Brisi sve koji se vec nalaze u tItemValue
							   if ($scope.fd.listOfConnectors!=undefined) 
								  for (var i=0;i< $scope.fd.tItemValue.length ;i++) {
										 for (var j=0;j< $scope.fd.listOfConnectors.length;j++) {
                                              if ($scope.fd.tItemValue[i]!=null) {
                                                 if ($scope.fd.listOfConnectors[j].widgetId==$scope.fd.tItemValue[i].widgetId)  {
                                                     $scope.fd.listOfConnectors.splice(j,1);//brisi lokalni				   					    		
                                                 }
                                              }
                                         }
							    }
                                //dodaj sa servera
                                for (var i=0;i< $scope.fd.tItemValue.length ;i++) 
                                              if ($scope.fd.tItemValue[i]!=null) 
                                                  $scope.fd.listOfConnectors.push($scope.fd.tItemValue[i]);		
                                

							  //Ostavi u listi konektora samo WIDGETE koji imaju "ResourceID" jer su to data widgeti
							  var newlist=[];
                              
							    if ($scope.fd.listOfConnectors!=undefined){
                 
                                 for (var i=0;i< $scope.fd.listOfConnectors.length;i++) {
                                                     if ($scope.fd.listOfConnectors[i].DEI!=undefined)  
                                                         for (var j=0;j< $scope.fd.listOfConnectors[i].DEI.length;j++) {
                                                             if ($scope.fd.listOfConnectors[i].DEI[j].name=='resourceLink') {
                                                                 //ako DEI nije true/false - resetiraj na false
                                                                 if ( ($scope.fd.listOfConnectors[i].DEI[j].value!=false) && ($scope.fd.listOfConnectors[i].DEI[j].value!=true) ) $scope.fd.listOfConnectors[i].DEI[j].value=false;
                                                                 //dodaj konektor u novu listu 
                                                                 newlist.push($scope.fd.listOfConnectors[i]);
                                                             };						 						  						   					      
                                                    }
                                               }
                                }//if
							  $scope.fd.listOfConnectors=newlist; //konacna lista NOVIH dataset konektora 
 
							  //return {selectedItemFromServer:$scope.fd.tItemValue,listOfConnectors: $scope.fd.listOfConnectors,headline:'Choose required interfaces',selectedFileConnector:$scope.selectedFileConnector};
						
				  
				/* interfaceModal.result.then(function (selectedItem) { 
							//$scope.fd.tItemValue=selectedItem; // //MIGOR TODO vidjet da li treba angular.copy(selectedItem);
                            $scope.fd.tItemValue=angular.copy(selectedItem); // MIGOR TODO - TEST SA ANGULAR COPY
                            
                            
							// MIGOR  TODO -
							//selektirani widget ima "resourceID" interface" -  registriramo se na njegov "event"
							
							$scope.$on('resourceId:'+selectedItem,
								function(event, data){
									console.log('resourceID uhvacen u ktUploader! data:' +  data);
							})
							
							//$scope.fd.listOfConnectors= WidgetDataExchangeService.getRegisteredInterfaces(selectedItem); 
							
							
				}*/
	
            } //if showfileconnetor  
		}; // open FILECONNECTOR - UPLOADER
                    
      
					
				 	// FUNCTION ONSELECT CALLBACK II
					$scope.onSelectCallback2 = function (item, model,fileIndex){
						var eventResultId=item.id;
						$scope.eventResultName=item.Name;
						
						 //console.log("onSelectCallback II:" +  $scope.eventResultName+ ' ' + eventResultId);

						  $scope.resourceId=eventResultId;
						  $scope.isResourceSelected=true;
						 	
								$scope.fd.filetype=$scope.resourceId; 						
					}//onSelectCallback II
				 
				 
				 
				 
					// FUNCTION - ON_SELECT_FILETYPE 
					// NAKON TO SE ZA POJEDINAÈNI FILE ODABERE FILE-TYPE  --> $scope.resourceId
					
					$scope.onSelectFileType = function (item,fileIndex){
						var selectedFileTypeId=item.id;
						$scope.resourceId=selectedFileTypeId;
						 $scope.isResourceSelected=true;
			             if ($scope.fd.files[fileIndex].fileResourceID!=selectedFileTypeId) {
			              $scope.fd.files[fileIndex].fileResourceID = selectedFileTypeId;
			              $scope.fd.files[fileIndex].formLoaded = false;
			             }
					
					
						 
						console.log("onSelectFileType :" + selectedFileTypeId);
		
						$scope.fileIndex=fileIndex;
						
								//if ($scope.fd.files[fileIndex].fileResourceID== undefined) {
								//	$scope.getResourceForm ();
								//	}
									
								//if ($scope.resourceId != undefined) {	
									//$scope.getResourceForm ();
								//}
						 		
						//$scope.fd.related=$scope.resourceId; // ovo viska??? MIGOR
						
						$scope.showMetadata(fileIndex,$scope.fd.files[fileIndex].fileResourceID );
						
						var err=$scope.testRules();
				
					}//onSelectFileType
				 
				 
				    
				var lang = $rootScope.globals.currentUser.lang;
				var type="file"; // PROMJENITI - ZASAD JE HARD-CODED !!!!! MIGOR
					 
					 
					// poziva se iz ktUploader.html
					$scope.remove = function (index) {
						
						$scope.fd.MetaData.splice(index,1);
						$scope.isOpen=true;
						var err=$scope.testRules();	
					}
					 
					 
			// POPUNI LISTU  lIST OF FILE TYPES
            ResourcesDataService.getResourceDefByType(lang,type).then(
						function(data) {
											  $scope.listOfFileTypes = data.data;
											 
							},function(error){
											 MessagingService.addMessage(error.msg,'error');
							
						});
                	            
            
	
      
     
			 },// controler_ktUploader_END
			

			//  ***************************************************  KT- UPLOADER   LINK FUNKCIJA *****************************
			require: '^form', // MIGOR  - NOVO DODAO 
			link: function (scope, elem, attrs,formCtrl) { //MIGOR NOVO
                        
                        
                                
                                //console.log("U post-link");
		  
                                //var element = elem[0].getElementsByClassName('fileElement');
                                 //scope.fd.uiUploader = uiUploader;
                                 //scope.fd.uiUploader.createUploader(scope.fd.PresentationId);
                                
                                				
                                //MIGOR TODO - prilagoditi widgetdefinition za uploader da salje i povezani related dataset
                                var out_fields = ['resourceId'];
                                var widgetDefinition = {DEI: [{'name':'resourceLinkR','value':[{name:'resourceId',value:scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'ktUploader',localWidgetId:scope.fd.LocalId,widgetId: scope.fd.PresentationId,taskId:  scope.lTaskId,resourceDefId : scope.selectedResource, output:out_fields, };
                                //var widgetDefinition = {DEI: [{'name':'resourceLinkR','value':false}],actions: [{name:'PrepareData',status:false}],name: 'ktUploader',localWidgetId:scope.fd.localId,widgetId: scope.fd.presentationId,taskId:  scope.lTaskId,resourceDefId : scope.selectedResource, output:out_fields, };
		  
                                scope.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition); //zamijeniti gdje se sve pojavljujue i staviti LOCAL TODO
                                scope.fd.LocalId=scope.localWidgetId;         
      
                               // scope.fd.uiUploader.removeAll(scope.fd.PresentationId); //MIGOR TEMP- TREBA nekako obrisati inicijalne fajlove kod reigstracija ali ovdje ne radi !!!
                                //console.log(element.files);

                      /*
                                var element = elem[0].getElementsByClassName('fileElement');
            
                                element[0].addEventListener('change', function(e) {
                
                                        $timeout(function() {
                                                var files = e.target.files;
                                                scope.fd.uiUploader.addFiles(files,scope.fd.PresentationId);				
                                                scope.fd.files =scope.fd.uiUploader.getFiles(scope.fd.PresentationId);  
                          
                                                var err=scope.testRules();
                                        });
                                });    
    	
                                */
        var fileCounter = 0;
			//scope.rules=[];
			if (scope.fd.MetaData==undefined)  scope.fd.MetaData=[];        
			scope.rules =  scope.fd.MetaData;
				
				// ADD RULE DEFINITION - poziva se iz ktUploader.html
				scope.addRuleDefinition=function(name,min,max,minTotal){
					
					if (min==undefined) min=0;			
					
					for (var i=1, l=scope.rules.length; i<l; i++) 
								if (scope.rules[i].name==name) name='';
							
					if (name!=undefined && name != '') {
						scope.rules.push({'name':name,'min':min,'max':max});
						//scope.rules.minTotal=minTotal;	
					}
				}
            
                scope.btn_remove = function(index,file) {
                                
                        //$log.info('deleting=' + file);
                        //var s=scope;
                        //var f=formCtrl;
                       // scope.fd.uiUploader.removeFile(file,scope.fd.PresentationId);
                        scope.fd.files.splice(index,1);
                        var err=scope.testRules();
                                
                };      
			

                scope.btn_clean = function() {
                   //scope.fd.uiUploader.removeAll(scope.fd.PresentationId);
                   scope.fd.files.length = 0;

                };


        scope.testRules = function(){
					
                var errMsg=null;
                for (var i=1; i < scope.rules.length; i++) { //search rules
                        var filesFound=0;
                        for(var j = 0; j < scope.fd.files.length; j++ ) {//search files
                                if (scope.fd.files[j].fileResourceID == scope.rules[i].name) filesFound=filesFound+1;
                        }
                        
                        if (filesFound < scope.rules[i].min) {formCtrl.uploaderForm.$setValidity('Not enough file types', false);
                        }	 else formCtrl.uploaderForm.$setValidity('Not enough file types', true);
                        
                        if (filesFound > scope.rules[i].max) {formCtrl.uploaderForm.$setValidity('Too many file types', false);
                                }	 else formCtrl.uploaderForm.$setValidity('Too many file types', true);
                 
                }
					
		if (scope.rules.length>0) {
                        if (scope.fd.files.length < scope.rules[0].min) formCtrl.uploaderForm.$setValidity('Minimum files needed', false); 
                        else formCtrl.uploaderForm.$setValidity('Minimum files needed', true); }
  
					//errMsg=null; // samo za test !!!     MAKNUTI KASNIJEEE !!!!     <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                return errMsg;
					
         }//test_RULES
               
	scope.checkForChanges = function(files) {
                //scope.fd.files = files;
                scope.files = files;
        };
     	//*******************************************************************************************************
		// 											 kt-UPLOADER BTN_UPLOAD
		//*******************************************************************************************************
        scope.btn_upload = function() {
            				
        // CHECK THE RULES FIRST
		var errMsg=scope.testRules();
                errMsg=null; // samo za test !!!     MAKNUTI KASNIJEEEE !!!!     <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
					
		if (errMsg==null) {
	
        		var csrf_token = $cookies.get('csrftoken'); //'miro';//$cookieStore.get('csrftoken');
			
                        Upload.upload({
                            url: '../api/file-upload/',
                            headers: {'X-CSRFToken' : csrf_token },
                            file: scope.fd.files,
                            data: {resource_def_id:scope.fd.linkedResourceDefId,resource_id:scope.fd.linkedResourceId},
                        }).then(function (resp) {
                                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                                file.uploadSuccess=true;
                        }, function (resp) {
                                console.log('Error status: ' + resp.status);
                                file.uploadError=true;
                                file.status=resp;
                        }, function (evt) {
                                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
                                file.status='Uploading...';
                        });
                        
                        /*
        		// ZA svaki FAJL pojedinaèno !
        		scope.fd.uiUploader.startUpload(scope.fd.PresentationId,{  
				url: '../api/file-upload/', 
				headers: {'X-CSRFToken' : csrf_token },
					concurrency: 3,
					// u data napuniti podatke vezane za fajl - fileType , ID resursa
					data: {resource_def_id:scope.fd.linkedResourceDefId,resource_id:scope.fd.linkedResourceId},
					onProgress: function(file) {
						$log.info(file.name + '=' + file.humanSize);
                                                $timeout(function() { file.status='Uploading...'; });
                                        },
					onCompleted: function(file, response,status) {

                                                //$log.info(file + 'response' + response);
                                                response=JSON.parse(response); //convertiram string u JSON objekt
								
						if (status==201) {// NAKON SMO USPJESNO UPLOADALI FILE , POTREBAN METADATA UPLOAD

                                                        // response.... ID fajla
                                                        // file.fileResourceID ... je file_type 
                                                        // scope.resourceData .. spremljeni meta podaci
								
                                                        $timeout(function() { file.uploadSuccess=true;  });
									

                        				//OUTPUT: data.savedResourceId .. ID meta-resursa koji je pridruen fajlu (unutar 11-ice ili 12-ice)
									
                                        		 file.resourceData={};
									
                                                        //MessagingService.addMessage("File upload successful! "+response,'success');
									
                                                        //PRIPREMA scope.resourceData  ZA UPLOAD 
                                                        if (file.dashboard!=undefined)
                                                        {
                                                                //code
                                                                var fieldId="";
                                                                var sc=scope;
                                                                          
                                                                for (var i=0;i<file.dashboard.widgets.length;i++) {//idem po gridROW
                                                                        for (var j=0;j<file.dashboard.widgets[i].layout.length;j++)
                                                                                for (var k=0;k<file.dashboard.widgets[i].layout[j].layout.length;k++)	
                                                                                {
                                                                                        fieldId='Field'+file.dashboard.widgets[i].layout[j].layout[k].itemId;
                                                                                        if (file.dashboard.widgets[i].layout[j].layout[k].dbtype=='id') {
                                                                                                fieldId=fieldId+'_id'//code
                                                                                                fieldId='id'//code
                                                                                        }
                                                                                        file.resourceData[fieldId] = file.dashboard.widgets[i].layout[j].layout[k].sItemValue	
                                                                                }
                                                                }//for
                                                                
                                                                                                                 
                                                                //  ********************************* METAUPLOAD ************************
                                        
                                                                if (file.resourceData)
                                                                {
                                                                        // ovdje je sve i update modela i insert meta pddataka - napravljena posebna DRF funkcija
                                                                        file.metaStatus='Saving metadata...'
                                                                        //	data: {resource_def_id:scope.fd.linkedResourceDefId,resource_id:scope.fd.linkedResourceId},
                                                                        //ResourcesDataService.fileMetaUpload(response,file.fileResourceID,scope.fd.linkedResourceId,scope.fd.linkedResourceDefId, file.resourceData).then(
                                                                        ResourcesDataService.fileMetaUpload(response,file.fileResourceID,scope.fd.linkedResourceId,scope.fd.linkedResourceDefId, file.dashboard.widgets).then(
                                                                                                
                                                                        // service.fileMetaUpload= function (fileId,fileMetaId,resourceId,resourceDefId,fileResourceData) {
                                                                                function(data) {
                                                                                                                                                        
                                                                                // MessagingService.addMessage(data.msg,'success');
                                                                                                file.metaSuccess=true;
                                                                                                scope.showFileMetaData=false;
                                                                    
                                                                                                //$log.info('data savedresourceid:' + data.savedResourceId);
                                                                                                                                 
                                                                                },function(error){
                                                                                        //MessagingService.addMessage(error.msg,'error');
                                                                                        file.metaError=true;
                                                                                        file.metaStatus = error.msg;
                                                                                                                                                        
                                                                                                                                                                         
                                                                                });
                                                                } //IF
                                                        }
                                                        else
                                                        {
                                                                $timeout(function() {
                                                                        file.noMeta = true;
                                                                        //file.metaSuccess=true;
                                                                });
                                                        }
                                                                                                         // });
                                                        fileCounter++;
                                                        if (scope.fd.files.length == fileCounter){ //MIGOR -Ovaj se okida a onaj drugi na 123 izgleda na
                                                           $rootScope.$broadcast('InitiateTaskFilesUploaded',{'resource_id':scope.fd.linkedResourceId,'resource_def_id': scope.fd.linkedResourceDefId})
                                                        }
                                                
                                                }        
                                                else { 
                                                        //if file upload error
                                                        $timeout(function() {
                                                                file.uploadError=true;
                                                                file.status=response;
                                                        });
                                                                                                                 
                                                }
                                                                   
                                                        
                                        }//on_completed
                                }); //startUpload
                                */
		} else {
                        //scope.ac.alerts = 'Min Total!!';
			//$log.info(errMsg);
		}
        }; // FUNCTION b.n_up1oad

       
        scope.processWidgetDependencies = function() { //-----  ktUploader  -----
              
           if (scope.fd.tItemValue!=undefined) 
            for (var i=0;i< scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<scope.fd.tItemValue[i].DEI.length;j++) {
                        if ((scope.fd.tItemValue[i].DEI[j].name=='resourceLink') && (scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("uploader "+scope.fd.PresentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                                //console.log("uploader "+ scope.fd.PresentationId+" dobio event " + event.name);
                                scope.ProcessEvent(data);
                               
                                    //	data: {resource_def_id:scope.fd.linkedResourceDefId,resource_id:scope.fd.linkedResourceId}
                                    
                              }) //on_END
                             var data = WidgetDataExchangeService.getDataByWidgetId(scope.resourceDefinition.task_execute_id,scope.resourceDefinition.task_initiate_id,scope.resourceDefinition.prev_task_id,linkedWidgetId,linkedItem);
                             scope.ProcessEvent(data);
                        }//if
                    }//for j
                }//if

             // else {  scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktUploader
      scope.ProcessEvent=function(data)
      {
         for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId') scope.fd.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                  if (data[k].name=='resourceId') scope.fd.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                  if (scope.fd.linkedResourceDefId>0 && scope.fd.linkedResourceId>0) scope.fd.uploadReady = true;
                                }
                          
         
         if (scope.fd.files.length>0 && scope.fd.uploadReady) scope.btn_upload();  // poziva UPLOAD - MIGOR TODO - upload treba samo ako ima fajl !!!
                                
        
      };
            
        
        	
      			
	
     	  // ne znam da li ce u ovoj link funkciji widget vec imati resource_id
          //ILJU treba dohvatit sa servera tItemValue
          
          scope.formType=scope.resourceDefinition.form_type;
          if (scope.fd.PresentationId!=undefined && scope.fd.PresentationId!=null)
          {
          TaskDataService.getWidgetDependencies(scope.taskId,scope.fd.PresentationId,scope.formType).then(function(data) {
                               
                                scope.fd.tItemValue=data.data;

                                  //ovo je da trenutno radi
                               // WidgetDataExchangeService.setData(scope.fd.tItemValue);
                                scope.processWidgetDependencies();
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
                              
    
          }
          //MIGOR - poèisti fajlove kad dobije InitateTask event
		  scope.$on('ProcessTaskWidgets',function(event, data){
           //scope.btn_clean(); // MIGOR - PRIVREMENO MAKNUO JER CE BITI VISAK KAD PRORADI OVO GORE
           if (scope.fd.files.length>0 && scope.fd.uploadReady) scope.btn_upload();
          });

		  
		  
		  //MIGOR NOVO TODO
		  scope.interfaceHandler= function (event,data) {
							scope.test=0;
				
			 // scope.fd.linkedResourceId = data.resource_id;
			   //scope.fd.linkedResourceDefId = data.resource_def_id;
			  
			   //scope.btn_upload();  
		  }
		  
                        
        
                               
       
     } //link directive 'ktUploader'
    };
};