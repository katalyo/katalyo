'use strict';

export function ktTaskExecuteOutcomes () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktTaskExecuteOutcomes.html',
              scope:{
                outcomes: '=',
                taskDefinition: '=',
                taskExe: '=',
                exeForm: '=',
                form: '=',
                formParams: '=',
                taskInitiateInstance: '=',
                localVars:'=',
                },
                controller: function($scope,CodesService,MessagingService,TaskDataService,$rootScope,$state)
                {
                   
                },
             link: function (scope, elem, attrs) {
              
              if (scope.localVars==undefined) scope.localVars={};
              
              scope.localVars.showInlineTask=false;
              scope.localVars.isInlineTask = false;
              
             if (scope.taskExe!=undefined) scope.taskExe.showInlineTask=false;
             else scope.taskExe = {showInlineTask:false};   
              
              scope.localVars.ProcessOutcome = function (outcome){
                
                for (var i=0;i<outcome.actions.length;i++) {
                  if (outcome.actions[i].type==1 || outcome.actions[i].type==2)
                  {
                     scope.localVars.InitiateTaskFromOutcome(outcome.actions[i].taskId,outcome.id1,outcome.actions[i].type,outcome.actions[i].actionParams);
                         
                  }
                 
                }
                if (outcome.actions.length==0)
                {
                    $rootScope.$broadcast('ExecuteTask'+scope.taskExe.id,{form:scope.form,outcomeId:outcome.id1});
                  
                }
                     
                 };
                 
                 
                 
              scope.localVars.InitiateTaskFromOutcome = function (taskDefId,outcomeId,type,actionParams){
                      //save task instance form for later processing
                      if (scope.taskExe!=undefined)
                      {
                      if (scope.taskExe.id!=undefined)
                      {
                      TaskDataService.saveTaskFormData(scope.taskExe.id,scope.exeForm).then(function(data) {
                               if (type==1)
                               {
                                
                                 scope.localVars.showInlineTask=true;
                                  scope.taskExe.showInlineTask=true;
                
                                  scope.localVars.isInlineTask=true;
                                  scope.localVars.fromOutcome=true;
                                  //scope.localVars.taskDefId=taskDefId;
                                  //scope.localVars.outcomeId=outcomeId;
                                  //scope.localVars.prevTaskId=scope.taskExe.id;
                                  if (taskDefId!=undefined &&  taskDefId>0) scope.localVars.StartTask(taskDefId,outcomeId,scope.taskExe.id);             
                              
                              
                                }
                                if (type==2)
                                {
                                      $state.go('app.menu.tasks.proc',{id: taskDefId,fromStateName:$state.current.name,
                                      fromStateParams:$state.params,prev_task_execute_id:scope.taskExe.id,outcome_id:outcomeId,presentation_id:0});
                            
                                 }
                        },function(error){
                                   MessagingService.addMessage(error.msg,'error');

                        });

                      }
                        else{
                        
                        MessagingService.addMessage("Invalid task data --> Task execution id is undefined",'error');
                        }
                      }else
                      {
                         MessagingService.addMessage("Invalid task data --> Task execution object is undefined",'error');
                      }
                 }
                 
                
                
        }
    }
};