export function ktGenericPropertyGenerator () {
    return {
        restrict: 'A',
        replace: true,
        template: '<div ng-include="getTemplateUrl()" ></div>',
          scope: {
            fd: '=',
            wTemplate:'=',
          },
        controller: function($scope, $rootScope){
            $scope.CTRL="ktGenericPropertyGenerator"
            
              },
        link: function (scope, element, attr) {
          
          scope.getTemplateUrl = function() {
                
                var template ="";
               
                template = 'ktdirectivetemplates/'+scope.wTemplate
               
                return template;
           
            };
        }
    }
};