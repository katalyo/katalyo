'use strict';

export function ktPageMenu () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktPageMenu.html',
            scope:{
            localVars:'=',
            navbarItemsLeft:'=',
            navbarItemsRight:'=',
            pageMenu:'=?',
            resource:'=',
            },
            controller: ['$scope','$timeout','dragulaService','$rootScope','MenuService','ResourcesDataService','MessagingService','GridsterService','$state',
          
            function($scope,$timeout,dragulaService,$rootScope,MenuService,ResourcesDataService,MessagingService,GridsterService,$state){
               
                //if (target.id==="navbarLeft") $scope.localVars.getNavbarDetails($scope.navbarItemsLeft,$scope.lastIndex); 
                // if (target.id==="navbarRight")  $scope.localVars.getNavbarDetails($scope.navbarItemsRight,$scope.lastIndex);
               $scope.localVars.selectedNavbar = {layout : []}
               $scope.localVars.pageMenuItems= []
                if ($scope.pageMenu) 
                        $scope.localVars.pageMenuItems= $scope.pageMenu
                else 
                    $scope.localVars.pageMenuItems=[]
               
               $scope.localVars.lrdrop= []
                  //$scope.localVars.container= [{}]
              
                if ($scope.resource.ResourceType.ResourceCode=='pagemenu') {
                    

                  $scope.localVars.showNavbarDetails = true;
                  $scope.localVars.showMenuDetails = false;
                 // if (navbar.length===index && navbar.length>0) index--;
                 // $scope.localVars.selectedNavbar  = navbar[index];
 
            
                     // $scope.localVars.selectedNavbar.index=index;
                      
                      //$scope.localVars.selectedMenu = null;
                     // if ($scope.localVars.selectedNavbar.Target==undefined) $scope.localVars.selectedNavbar.Target = {id:navbar[index].id,name:navbar[index].name};
                     // if ($scope.localVars.selectedNavbar.katalyonode_menus_Name == undefined) $scope.localVars.selectedNavbar.katalyonode_menus_Name = navbar[index].name;
                      
                      //if ($scope.localVars.selectedNavbar.layout==undefined) $scope.localVars.selectedNavbar.layout = [];
                    
                        $timeout(function(){
                            $scope.localVars.showNavbarDetails = true;
                            $scope.localVars.showNavbarDetailsFirst = true;
                        })
                    
                }
                    
                  
               
               $scope.whenSuccesfullDrop = function(itemIndex) {
                    //debugger
                    //let item = $scope.localVars.pageMenuItems[itemIndex]
                    $scope.localVars.getMenuDetails($scope.localVars.pageMenuItems,itemIndex)
                    $scope.localVars.selectedPageMenuItem = $scope.localVars.pageMenuItems[itemIndex]
                     $scope.localVars.showMenuDetails = true
                    
               }
                    
              /*  $scope.$watch('localVars.selectedMenu', function(newValue, oldValue) {
                      if (newValue!=undefined) {                            debugger                             }
                      },true);
                      
                      $scope.$watch('localVars.selectedNavbar', function(newValue, oldValue) {
                      if (newValue!=undefined) {                            debugger                           }
                      },true);
                      */
                                
                
                $scope.gsrvc = GridsterService;     

                 let currentUser=$rootScope.globals.currentUser;

                 if ($scope.localVars == undefined) $scope.localVars={};
                 $scope.localVars.pagesLoaded=false;    
                 let lang = $rootScope.globals.currentUser.lang;
              
            
                
                $scope.localVars.getMenuDetails = function(menu,index)
                {
                  if (menu.length===index) index--;
                  $scope.localVars.selectedMenu  = menu[index];
                  $scope.localVars.selectedMenu.index=index;
                  $scope.localVars.showMenuDetails = false;
                  $scope.localVars.showNavbarDetails = false;
                  if ($scope.localVars.selectedMenu.Target==undefined) $scope.localVars.selectedMenu.Target = {id:menu[index].id,name:menu[index].name};
                  if ($scope.localVars.selectedMenu.katalyonode_menus_Name==undefined) $scope.localVars.selectedMenu.katalyonode_menus_Name = menu[index].name;
                  
                  
                   $timeout(function(){
                       
                        $scope.localVars.showMenuDetails = true;
                    })
                
                    
                };
                
                  
                  
                 $scope.localVars.deletePageMenu = function(menu,index) {
                    if (menu.katalyonode_menus_id!==null && menu.katalyonode_menus_id!==undefined)
                    
                    {
                       if (menu.deleteItem) menu.deleteItem = false;
                       else menu.deleteItem = true;
                    }
                    else
                    {
                        $scope.localVars.pageMenuItems.splice(index,1);
                        $scope.localVars.selectedMenu = null;
                        $scope.localVars.showMenuDetails = false;
                    }
                  };
                  
                  
                $scope.buildDataset = function(e)
                {
                   // e.target.tooltip("dispose");
                    $state.go("app.menu.resources.new",{'resource_type':'dataset'});
                };
                
                 $scope.buildPage = function(e)
                {
                   // e.target.tooltip("dispose");
                    $state.go("app.menu.resources.new",{'resource_type':'page'});
                };
                
                $scope.savePageMenu = function()
                {
                    let navbarLeft,navbarRight
                    let sideMenuItems = angular.copy($scope.localVars.pageMenuItems);
                    
                    $scope.localVars.showMenuDetails = false;
                    
                    MenuService.saveFullAppMenu($scope.resource.id, navbarLeft, navbarRight,sideMenuItems ).then(function(data) {
                        
                        MessagingService.addMessage(data.msg,'success');

                        $scope.localVars.pageMenuItems=data.pageMenu
                        
                    },
                    function(error){    
                        MessagingService.addMessage(error.msg,'error');
                    });
                };
                
                //get pages
                ResourcesDataService.getResourceDefListByType(lang,'page').then(function(data) {
										
					$scope.localVars.pageList = data.data;
                    /*
                    for (let i=0;i<$scope.localVars.pageList.length;i++)
                    {
                       $scope.localVars.pageList[i].katalyonode_menus_PopulateType="navbar"; 
                    }
                    */
                    $scope.localVars.pagesLoaded=true;    
											 
                    },function(error){    
                    MessagingService.addMessage(error.msg,'error');
				});
                
                
                 
                
            
            }],
            link: function (scope, elem, attrs) {
              
             
            }
        }
  
};