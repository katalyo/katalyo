'use strict';


export function ktCodesDetailEdit () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktCodesDetailEdit.html',
              //scope:{
              //  codeChanged : '&',
              //  },
                controller:function($stateParams,$state, $scope,TaskDataService,$rootScope,dragularService,uiGridConstants,ProcessingService,MessagingService,CodesService,ResourcesDataService, $q, $timeout) 
                
                {
    
    $scope.CTRLNAME='codesDetailEdit';
    $scope.isCollapsed  = false;
    $scope.pageState={} //MIGOR - samo da proradi GETRESOUCEDEF
    $scope.paginationOptions={}//MIGOR - samo da proradi GETRESOUCEDEF
    $scope.showTableActions=true;// Needed by SmartTable
    $scope.codeId=0;
    $scope.searchInitiated = true
    var lang=$rootScope.globals.currentUser.lang;
    
   
    $scope.returnState = $stateParams.returnState
    if ($scope.returnState===undefined || $scope.returnState==='') {
       $scope.returnState={state:{name:'app.menu.codes.list'},Label:'Return to Codes list',params:{}} 
    }
    
    
    $scope.codesDetails = []
     
    $scope.columnDefs = [// Needed by SmartTable
         {name: 'id', displayName: 'ID', type: 'number'},
         { name: "act", displayName: ''}, //special actions column
        {name: 'CodesHeadLang[0].CodeName', displayName: 'Code name', type: 'string'},
         {name: 'CodeHeadName', displayName: 'Code unique identifier',  type: 'string'},
         {name: 'CodesHeadLang[0].Description', displayName: 'Code description',  type: 'string'},
         {name: 'Status.name', displayName: 'Status', type: 'string'},
         {name: 'CodeType.name', displayName: 'Code Type',  type: 'string'},
         {name: 'Version', displayName: 'Version', type: 'string'},
         {name: 'CodesHeadLang[0].Lang.name', displayName: 'Language',  type: 'string'}]
 
    $scope.ResourceSearch = function(page){
            $scope.searchInitiated=false;
            $scope.paginationOptions.totalItemCount=0;
            if (page!=undefined) $scope.currentPage=page;
            $timeout(function(){
                $scope.searchInitiated=true;
            });
     };
     
     
     $scope.returnToPreviousPage = function()
        {
                let page = $scope.returnState.state.name
                $state.go(page,$scope.returnState.params);
             
        }
  
    
     $scope.processTableAction = function(row,actionId)
         {   // Needed by SmartTable - on EDIT ROW selection
            
            $scope.codeId = row.id
            $scope.codeSelected=true;
            $scope.selectedCode= row.id
            $scope.CodeHead = {}
            $scope.codesList = []
            $scope.codesDetails = []
             
            $scope.codeSelected=row.isSelected      
            $scope.selectedCode = row.id;
                  
            $scope.selectedCodeLang = row.CodesHeadLang[0].Lang;     
            $scope.CodeHeadLang = {CodeName: row.CodesHeadLang[0].CodeName,CodesHeadId: row.id,Description:row.CodesHeadLang[0].Description,Lang:row.CodesHeadLang[0].Lang};
            
            $scope.getCodesDetails($scope.selectedCode, $scope.CodeHeadLang.CodeName)
                  
            CodesService.CodesGetById($scope.selectedCode,lang)
                        .then(function(data) {          
                                    $scope.CodeHead = data.data[0];
                         },function(error){    
                                    MessagingService.addMessage(error.msg,'error');
                    });      
        };
    

       
     $scope.getCodesFiltered = function(pageNumber,pageSize,filter, lang) {// Needed by SmartTable
          let deferred = $q.defer();
         
         // if ($scope.data1 == undefined) 
        //  {
                CodesService.CodesGet(lang).then(function(data) {
                                   
                                   if (filter?.Name?.length>0)  //filter by Search query
                                       $scope.data1  = data.data.filter(item =>  item?.CodesHeadLang?.[0]?.CodeName.includes(filter.Name))
                                   else
                                       $scope.data1 = data.data;
                                   
                                   $scope.totalCount = $scope.data1.length
                                   $scope.paginationOptions.totalItemCount  =$scope.data1.length
                                   let startAt = (pageNumber - 1) *  pageSize 
                                   let newData =  $scope.data1.slice(startAt , Math.min( pageNumber * pageSize,  $scope.totalCount) )
                                  //debugger
                                   deferred.resolve( {status: 200, data:newData, totalCount: $scope.totalCount});
                              },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                            })
         // } else deferred.resolve( $scope.data1);

        return deferred.promise;       
         
     }
    
     
    $scope.getResourceDef = function (pageNumber,pageSize) {// Called by SmartTable
       
        let deferred = $q.defer();    
        $scope.resourceLoading=true;
        var filter=$scope.resource;
        //debugger //getResourceDef
        
        if (pageNumber==undefined) pageNumber = $scope.paginationOptions.pageNumber;
        if (pageSize==undefined) pageSize = $scope.paginationOptions.pageSize;
        if ($scope.totalCount>0){
            if (Math.ceil($scope.totalCount/pageSize) < pageSize) {
              let newPage = Math.ceil($scope.totalCount/pageSize);
            }
         }
         $scope.pageState.resource = $scope.resource;
         
         if ($scope.firstQuery) {
          $scope.pageState.currentPage = pageNumber;
          $scope.pageState.pageSize = pageSize;           
         } else  {
                if ($scope.pageState.rememberSearch) {
                        if ($scope.currentPage == 1)  {
                                $scope.pageState.currentPage = $scope.currentPage;
                        }
                        pageNumber = $scope.pageState.currentPage;
                        pageSize = $scope.pageState.pageSize;
                }
         }
        
         // $scope.ksm.executeAction(resourceListStateName,"resourceListSearch",$scope.pageState);
        
            $scope.getCodesFiltered (pageNumber,pageSize,filter,lang).then( 
                function(response) {
                     
                   //debugger
                    
                    $scope.firstQuery=true;
                    $scope.resourceTableData = response.data;
                    let newData = {}
                    newData.results = response.data;
                    $scope.paginationOptions.totalItemCount = response.totalCount
                    
                    
                    //$scope.nextPage = data.data.next;
                    //$scope.previousPage = data.data.previous;
                 
                    $scope.currentPage=pageNumber;
                    $scope.paginationOptions.numberOfPages = Math.ceil($scope.paginationOptions.totalItemCount/pageSize);
                     
                    newData.pageSize = pageSize;
                    newData.numberOfPages = $scope.paginationOptions.numberOfPages;
                    newData.columns = $scope.columnDefs;
                    newData.count = response.totalCount
                    deferred.resolve({data: newData });
                   
                    $scope.resourceLoading=false;
                }
                , function (error) {
                 deferred.resolve( {data: newData });
                }
            ) 
                                 
                            
     return deferred.promise;
    }; // ---getResourceDEf ---
      
   
    CodesService.getCodesByName('languages',lang)
		.then( (data) =>  {
            //debugger
            $scope.codeLanguageList = data.data; });
    CodesService.getCodesByName('_CODE_TYPE_LIST_',lang)
		.then( (data) =>{ $scope.codeTypeList = data.data; });
     CodesService.getCodesByName('codestatus',lang)
		.then( (data) => {  $scope.codeStatusList = data.data; });
                               
           
     
    $scope.saveCode = function (isValid) {
        //debugger
          if (isValid) {
                $scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving code '+ $scope.CodeHead.CodesHeadLang[0].CodeName);
                CodesService.CodesPost($scope.CodeHead)
                        .then(function(data) {
                                   MessagingService.addMessage(data.data,'success');
                              },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                            });                             
          }
     }; 

   
    $scope.setDirectiveFn = function(directiveFn) { // directiveFn == getCodesDetails()
        //debugger //setDirectiveFn 
        $scope.directiveFn = directiveFn;
    };

     $scope.isCollapsedEditItem = false;
     
     
    $scope.editCodeItem = function(){
        
        //populate from grid
        $scope.codeItemHeader={Text: 'Edit code item',Type:'Edit'};
         if ($scope.CodesDetailLang.Lang==undefined) {
          //code
           $scope.CodesDetailLang.Lang=$scope.CodeHeadLang.Lang;
        }
         $scope.CodesDetailLang.CreatedBy=null;
         $scope.CodesDetailLang.ChangedBy=null;
        
        $scope.isCollapsedEditItem = !$scope.isCollapsedEditItem;
            
    }
    
  $scope.CodesDetail = {codesdetaillang_set: []};
  $scope.CodesDetailLang ={CodesDetailId:{}}; 
  
  
      $scope.newCodeItem = function(){
        
            $scope.codeItemHeader={Text: 'New code item',Type:'New'};
            //populate from grid
            $scope.isCollapsedEditItem = !$scope.isCollapsedEditItem;
            $scope.CodesDetailLang.id=undefined;
            $scope.CodesDetailLang.Lang=$scope.CodeHeadLang.Lang;
            $scope.CodesDetailLang.CodesDetailId.CodesHeadId=$scope.CodeHeadLang.CodesHeadId;
        }
     
    $scope.editCodeLang = function(){
        //populate from grid
        $scope.isCollapsedEditLang = !$scope.isCollapsedEditLang 
    }
     
    $scope.saveCodeLang = function(id) {
        //debugger
            $scope.isCollapsedEditLang = !$scope.isCollapsedEditLang
       
            CodesService.CodesPost($scope.CodeHead).then(function(data) {    
                         $scope.data1 = data.data;
                          //$scope.gridOptions1.data = $scope.data1;
                       
                    },function(error){
                      
                        MessagingService.addMessage(error.msg,'error');
                  });
      
    }
     
    $scope.getListOfCodes = function(){

       var lang=$rootScope.globals.currentUser.lang;
       CodesService.CodesGet(lang).then(function(data) {
                                
                                   $scope.data1 = data.data;
                                  //  $scope.gridOptions1.data = $scope.data1;
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });    
    }
    
 
    $scope.getListOfCodes();
    
    
     
      $scope.saveCodeItem = function (isValid) {
           //debugger
           if (isValid) {
              //var codesStatus = CODES_LIST._CODE_STATUS_LIST_;
            $scope.CodesDetailLang.CodesDetailId.Status = 'Active';
        
                CodesService.addUpdateCodesDetails($scope.codeItemHeader.Type,$scope.CodesDetailLang).then(function(data) {
                                
                                  MessagingService.addMessage(data.msg,'success');
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
          }
        };
      
   
    
        
                
                
                
                
                $scope.showEditCodes = false
                  $scope.showListCodes = false
                  

                  $scope.pokaziModal = function() {
                           $('#exampleModalC').modal('show');         
                  }
                  
                   $scope.$watch('codeId', function (newValue, oldValue, scope) {
                        if (newValue) $scope.pokaziModal();
                        //debugger      
                        
                    });
                    
                  $scope.toggleEditCodes = function() {                      
                        $scope.showEditCodes = !$scope.showEditCodes   
                  }

                  $scope.toggleListCodes = function() {                      
                        $scope.showListCodes = !$scope.showListCodes   
                  }                  
                  
                  
                  $scope.removeCode = function(index){
                          console.log("text")
                          //debugger
                      $scope.codesDetails.splice(index,1);
                  }
                  
                  $scope.unselectRows = function() {
                       
                      //$scope.gridApi1.selection.clearSelectedRows()
                      $scope.codeId=0; 
                      $scope.codeSelected=false;
                      $scope.selectedCode=0
                          
                  }
                  

                 $scope.getCodesDetails = function(codeId,codeName){
                  
                    var lang=$rootScope.globals.currentUser.lang;
                    
                     $scope.codeId = codeId;
                     $scope.codeName = codeName;
                     $('#exampleModalC').modal('show');    
                     
                       
                    if ($scope.codeId>0)  {
                      
                    CodesService.getCodesDetails($scope.codeId,lang).then(function(data) {         
                                   $scope.codesDetails = data.data;
                                   $scope.codesLoaded = true;   
                                    $('#exampleModalC').modal('show');                                       
                              },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                            });
                    } else {
                      
                      $scope.codesDetails.length = 0;
                      $scope.codesLoaded = false;
                    }
                }
                
                
                   $scope.saveCodesDetails = function(){
                  
                        var lang=$rootScope.globals.currentUser.lang;
                        //debugger
                    
                        if ($scope.CodeHead.id>0) {
                            CodesService.saveCodesDetails($scope.CodeHead.id,$scope.codesDetails,lang).then(
                                function(data) {
                                           MessagingService.addMessage(data.msg,'success');
                                           $scope.codesDetails = data.data;  
                              },function(error){ 
                                          MessagingService.addMessage(error.msg,'error');
                             });
                        } else {  
                          $scope.codesDetails.length = 0;
                        }
                    }
                
                
                $scope.addEmptyCodeItem = function(){
                  
                  
                  var lang=$rootScope.globals.currentUser.lang;
                   $scope.codesDetails.push({id:null,Name:null,Value:null,Active:true,CodesDetailLang:[{Value:null,Lang:lang}]});
                }
                
                //$scope.codeChanged({theDirFn: $scope.getCodesDetails});
                
               if ($scope.codeId>0)
                  {
                    $scope.getCodesDetails();
                  
                    if ($scope.codeId==0) $scope.codesLoaded = false;
                  } else $scope.codesLoaded = false;
                  
          
         
          
          
          
          
          
          
          },//controller
         
         link: function (scope, elem, attrs) {
              
              
             
            }   
        }
    };