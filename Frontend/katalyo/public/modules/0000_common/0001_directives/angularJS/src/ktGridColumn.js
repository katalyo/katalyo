'use strict';

export function ktGridColumn () {
     //  console.log("u direktivi kt-textbox ");
        return {
            restrict: 'A',
             
             scope:{
                   inputArray : '=',
                   allColumns: "=",
                   parent: "=",
                   gridType: '=',
                   formType: '=',
                   resourceDefinition: '=', 
                   formParams:'=',
                   formParamsParent:'=',
                   showSearchFilters:'=',
                   parentResourceDefId: '=',
                   resourceRecordId: '=',
                   resourceFormLoaded : '=',
                   formData : '=',
                   index : '=',
      			},
                 controller: ['$scope', '$element','$rootScope','GridsterService','WidgetDataExchangeService','$timeout','dragulaService','KatalyoStateManager','$compile','$templateCache',
                              function ($scope, $element,$rootScope,GridsterService,WidgetDataExchangeService,$timeout,dragulaService,KatalyoStateManager,$compile,$templateCache) {
                        
                        if ($scope.localVars == undefined || $scope.localVars == null) $scope.localVars={};
                        
                        $scope.ksm = KatalyoStateManager;
                        $scope.gsrvc = GridsterService;
                        $scope.localVars.gsrvc = GridsterService;
                        
                        if ($scope.inputArray.Parameters == undefined || $scope.inputArray.Parameters == null) $scope.inputArray.Parameters={};
                        
                       if ($scope.inputArray.Parameters.border == undefined || $scope.inputArray.Parameters.border == null )
                       {
                       
                        $scope.inputArray.Parameters.border = {
                            left: {
                                name: 'Left',
                                style:'solid',
                                width:0,
                                color:"#000000"
                            },
                            right: {
                                name: 'Right',
                                style:'solid',
                                width:0,
                                 color:"#000000"
                            } ,
                            top:{
                                name: 'Top',
                                style:'solid',
                                width:0,
                                color:"#000000"
                            },
                            bottom:{
                                name: 'Bottom',
                                style:'solid',
                                width:0,
                                color:"#000000"
                            }
                        };
                       }
                         
                           
                        if ($scope.inputArray.Parameters.margin ==  undefined ||  $scope.inputArray.Parameters.margin ==  null) {
                                $scope.inputArray.Parameters.margin= {
                                     left: {  
                                        name: 'Left Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },
                                     right: {  
                                        name: 'Right Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },
                                     top: {  
                                        name: 'Top Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },
                                     bottom: {  
                                        name: 'Bottom Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },        
                                }
                        }
                        if ($scope.inputArray.Parameters.padding ==  undefined ||  $scope.inputArray.Parameters.padding ==  null){
                                $scope.inputArray.Parameters.padding= {
                                 left: {  
                                    name: 'Left Padding',
                                    value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                 right: {  
                                    name: 'Right Padding',
                                     value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                 top: {  
                                    name: 'Top Padding',
                                   value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                 bottom: {  
                                    name: 'Bottom Padding',
                                    value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                     
                                };
                        }
                    

                
                 var widgetDefinition = {name: 'column',widgetId: $scope.inputArray.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId};
                $scope.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
                $scope.localVars.LocalId=$scope.localVars.localWidgetId;
        
                $scope.inputArray.LocalId=$scope.localVars.localWidgetId;
             
                let update_presentation_id = function(data)
                {
                   if (data[$scope.inputArray.LocalId]>0 && ($scope.inputArray.PresentationId==null || $scope.inputArray.PresentationId==0)) $scope.inputArray.PresentationId = data[$scope.inputArray.LocalId];
                  //unsubscribe here later after implementing unsubscribe - ToDo
                  
                }
                if ($scope.inputArray.PresentationId==0 || $scope.inputArray.PresentationId==null)
                {
                 
                    let resource_id = $scope.resourceDefinition.ResourceDefId;
                    if (resource_id==undefined) resource_id = $scope.resourceDefinition.ResourceDefId_id;
                    let dependent_field = $scope.inputArray.LocalId;
                    let subscribedSubject = "formDefinitionState"+resource_id+$scope.resourceDefinition.form_type;
                    let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"formSaved",update_presentation_id);
          
                }

    
                let columnId = $scope.gridType+'_'+$scope.resourceDefinition.ResourceDefId_id+'_'+$scope.inputArray.ElementType+'_'+$scope.parent.id+'_'+$scope.inputArray.id;
                let formChangesSubject = 'datasetFormState'+$scope.resourceDefinition.ResourceDefId_id;
                let originalColumn = angular.copy($scope.inputArray);    
                //Add Subject to katalyo state manager (name must be unique)
                //let columnSubject = $scope.ksm.addSubject(columnId,angular.copy($scope.inputArray));
                
                //define fn for applyChange action 
                /*
                let changed_item= function(data)
                {
                
                    //scope.inputArray = data; 
                    if (!$scope.syncFromObserver)
                    {
                        columnSubject.subject.setState = angular.copy($scope.inputArray);
                        columnSubject.subject.notifyObservers();
                    }
         
                };
    
                //add action to subject rowSubject
                let action_change_column = $scope.ksm.addAction(columnSubject.subject.name,"applyChangeToColumn",changed_item);    
                */
                
        
                
                
                
                //define fn to sync to manager 
                let process_undo = function(data,action)
                {
                    if (data!==undefined)
                    {
                        let columnData = data.oldData;
                        if (data.id===columnId && action==="undoDatasetFormChange")
                        
                        {
                            $scope.parent.layout[$scope.index] = columnData;
                            $scope.ksm.executeAction(formChangesSubject,"undoDatasetFormChangeDone");    
                            $scope.syncFromObserver=true;
                        }
                    }
                };
                
                //define fn to sync to manager 
                let process_redo = function(data,action)
                {
                    if (data!==undefined)
                    {
                        let columnData = data.newData;
                        if (data.id===columnId && action==="redoDatasetFormChange")
                        
                        {
                          
                                $scope.parent.layout[$scope.index] = columnData;
                                $scope.syncFromObserver=true;
                         
                        }
                    }
                };
                
                 //create observer if change comes from someplace else (e.g. undo or from server)
                let observer_undo = $scope.ksm.subscribe(formChangesSubject,columnId,"undoDatasetFormChange",process_undo);
                 //create observer if change comes from someplace else (e.g. undo or from server)
                let observer_redo = $scope.ksm.subscribe(formChangesSubject,columnId,"redoDatasetFormChange",process_redo);
            
          //  if ($scope.formParams.formViewType!==2 && ($scope.formParamsParent.formViewType!==2 || $scope.formParamsParent===undefined))      
           // {
           
        /****************** remove bloody watchers that kill the application START ****************************************
              $scope.$watch('inputArray.layout',function(newValue,oldValue){
               
                //process object change
                if (newValue.length!==oldValue.length && !$scope.syncFromObserver)
                {
                  //$scope.ksm.executeAction(columnSubject.subject.name,'applyChangeToColumn',angular.copy(newValue));
                   $scope.ksm.executeAction(formChangesSubject,"datasetFormChanged",{'id':columnId,'source':columnId,'oldData':angular.copy(oldValue),'newData':angular.copy(newValue)});
                } else $scope.syncFromObserver=false;              
              
                });
            
                $scope.$watch('inputArray',function(newValue,oldValue){
               
                //process object change
                if (newValue.SizeX!==oldValue.SizeX && !$scope.syncFromObserver)
                {
                    //$scope.ksm.executeAction(columnSubject.subject.name,'applyChangeToColumn',angular.copy(newValue));
                    $scope.ksm.executeAction(formChangesSubject,"datasetFormChanged",{'id':columnId,'source':columnId,'oldData':angular.copy(oldValue),'newData':angular.copy(newValue)});
                } else $scope.syncFromObserver=false;              
              
                });
             
            //}
           ***************** remove bloody watchers that kill the application END ****************************************/
        
            $scope.addColumn = function (row,type){
          
                  let lindex = $scope.index+type;
                  let size = $scope.inputArray.SizeX;
                  //let newSize = Math.ceil(size/2);
                  
                 // $scope.inputArray.SizeX = newSize;
                  
                  let newColumn = GridsterService.getEmptyColumnWithId($scope.allColumns);
                  
                  newColumn.SizeX = size;
                  let oldRow = angular.copy(row);
                  row.layout.splice(lindex,0, newColumn);
                  let source = $scope.gridType+'_'+$scope.resourceDefinition.ResourceDefId_id+'_'+row.ElementType+'_'+row.ContainerId+'_'+row.id;
                  $scope.ksm.executeAction(formChangesSubject,"datasetFormChanged",{'id':source,'source':source,'oldData':angular.copy(oldRow),'newData':angular.copy(row)});
                  //$scope.gsrvc.selectedFieldParent.countActive++;
            };
              
                    
                    
                    
                       if ($scope.gridType=="" || $scope.gridType==undefined) {
                  
                    $scope.gridType="resource";
                }
                 
                  $scope.getClass = function(size)
                  {
                    
                    return "col-lg-"+size;
                    
                  };
                  
                  $scope.setpOpen = function(value)
                  {
                    $scope.pOpen=value;
                  };
                  
                  $scope.setHandle = function (type){
                        return "column-"+type+"-handle";
                 };
                 
                 
                 $scope.addRow = function (){
      
                        $scope.inputArray.layout.push(GridsterService.getEmptyRowWithId($scope.inputArray));
       
                 };
                 
                  $scope.inputArray.BackgroundColorChanged = function (){
      
                        $scope.inputArray.Parameters.BackgroundColorA = "rgba(" + parseInt($scope.inputArray.Parameters.BackgroundColor.slice(-6, -4), 16) + "," + parseInt($scope.inputArray.Parameters.BackgroundColor.slice(-4, -2), 16) + "," + parseInt($scope.inputArray.Parameters.BackgroundColor.slice(-2), 16) + "," + $scope.inputArray.Parameters.BackgroundOpacity + ")";
       
                 };
              
                  
                $scope.showHideProperties = function(value)
                      {
                        $scope.gsrvc.setFieldPropertiesOpened(value);
                        $scope.showFieldProperties=$scope.gsrvc.getFieldPropertiesOpened();
             
                      };
 
               $scope.splitColumn = function (column){
                
                var splitColumn = GridsterService.getNewRowForSplitColumn($scope.allRows,column);
      
                  $scope.parent.layout.splice($scope.parent.layout.indexOf(column),1,splitColumn);
       
              };
                  
                  
                  $scope.remove = function (item){
						
                       if (item.layout.length==0)
                       {
                          if ($scope.parent==undefined){
                            $scope.allColumns.splice($scope.allColumns.indexOf(item), 1);
                          }else
                          {
                          $scope.parent.layout.splice($scope.parent.layout.indexOf(item), 1); 
                            
                          }
                       }
                       
                };
                
                $scope.deleteElement = function (index) // delete Column
                {
                    $scope.localVars.gsrvc.deleteElement($scope.inputArray)
                }
                
                
                
                 $scope.inputArray.select = function(selected){
                        $scope.inputArray.selected=selected;
                    }
                    
                    $scope.inputArray.selectBorder = function(selected){
                        $scope.inputArray.selectedBorder=selected;
                    }
                let template;
                if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktGridColumn.html');
                else template = $templateCache.get('ktdirectivetemplates/ktGridColumnExe.html');
          
                $element.append($compile(template)($scope)); 
                         
                }],
                
                
             link: function (scope, elem, attrs) {
            
               
                    
               

        
               
             
        }
    }
};