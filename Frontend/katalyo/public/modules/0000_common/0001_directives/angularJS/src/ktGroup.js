'use strict';

export function ktGroup () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktGroup.html',
             controller:function($scope,UserService,$rootScope,GridsterService){
                 
                 $scope.gsrvc = GridsterService;
           if ($scope.localVars==undefined) $scope.localVars={};
              //MIGOR START
              $scope.search=[
                      {id:1,filter:'onlist',name:"On list:",icon:"fa fa-chevron-right"},
                      {id:2,filter:'notonlist',name:"Not on list:",icon:"fa fa-expand"},
                ];
				
               if ($scope.fd!=undefined) {
                  if ($scope.fd.search==undefined)
                  {
                    $scope.fd.search=$scope.search[0]; //MIGOR TODO - greska kod incijacije za multiple , mozda treba kod promjene na multiple ovo jos jednom okinuti
                  }
               } 

			  $scope.applyListFilters = function(index) {
                   
				   $scope.fd.search=$scope.search[index];
                   if( typeof $scope.formData[0][$scope.fd.FieldName] === 'string' ) {
                        //$scope.fd.search.value = [ $scope.fd.sItemValue ];
                        $scope.fd.search.value = [ {'id':$scope.formData[0][$scope.fd.FieldName]} ];
                    }
                   //if ($scope.fd.search.name=="Equals") $scope.fd.search=null;
				   $scope.isOpen=false;		
			  	};
                
                //MIGOR END
                
             $scope.showProperties = function(event)
            {
                  $scope.gsrvc.setFieldProperties($scope.fd,event);
       
            };    
             $scope.fd.colLength=[];
            for (var i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));
            $scope.groupItem = {selectedGroup : null};
          
             $scope.populateGroup = function (item,model) {
               
           
                  $scope.selectedGroup = item;
                  $scope.fd.codeId = item.id;
                  $scope.formData[0][$scope.fd.FieldName]=item.id;
             
               $scope.fd.search.value = [ {'id':$scope.formData[0][$scope.fd.FieldName]} ]; //MIGOR TODO - vidjeti da li ovjde siTemValue ( provjeriti i ostale sluèajeve u onselect funkciji)

    				};

              
            $scope.getGroups = function () {
              UserService.getUsersGroups('groups').then(function(data){
              
              $scope.groupList = data.data;
              
              if ($scope.formData!=undefined && $scope.formData.length>0)
              {
                    if ($scope.formData[0][$scope.fd.FieldName]!=null) {
                         UserService.getGroupObjectById($scope.formData[0][$scope.fd.FieldName]).then(function(data){
                            $scope.selectedGroup = data;
                            $scope.groupItem.selectedGroup=$scope.selectedGroup;
                           
                          });
                       
                         
                      }
                     else if ($scope.formData[0][$scope.fd.FieldName]!=null) {
                     
                      if ($scope.formData[0][$scope.fd.FieldName].length>0)
                      {
                        if ($scope.formData[0][$scope.fd.FieldName][0]!=undefined)
                        {
                         UserService.getGroupObjectById($scope.formData[0][$scope.fd.FieldName][0]).then(function(data){
                            $scope.selectedGroup = data;
                            $scope.groupItem.selectedGroup=$scope.selectedGroup;
                            $scope.formData[0][$scope.fd.FieldName]=$scope.selectedGroup.id;
                          });
                      }
                         
                      }
                     }
              }
    				});
          
          }
          
          
          $scope.$watch('groupItem.selectedGroup', function(newValue,oldValue){
                    //debugger
                    if ($scope.formData && newValue)  $scope.formData[0][$scope.fd.FieldName]=newValue.id;
                });
           $scope.clear = function(event,select)
        {
          $scope.formData[0][$scope.fd.FieldName] = null;
          $scope.fd.search.value = null;
          $scope.groupItem.selectedGroup = null;
        }
        
  				$scope.getGroups();
          
         
                
                },
             link: function (scope, elem, attrs) {
          
          
          
          
        
          
              }
        }
  
};