'use strict';

export function ktFormlayout () {
    
        return {
            restrict: 'A',
           
          controller: ['$scope','dragulaService','$rootScope','GridsterService','$element','$compile','$templateCache',
              function($scope,dragulaService,$rootScope,GridsterService,$element,$compile,$templateCache){
                  
                $scope.gsrvc = GridsterService;    
                $scope.CTRL = "ktFormlayout";                 
                var widgetHandleResource = 'element-drag-handle';
                let template = $templateCache.get('ktdirectivetemplates/ktFormLayoutExe.html');
                
                if ($scope.resourceDefinition.form_definition)    template = $templateCache.get('ktdirectivetemplates/ktFormLayout.html');
                
                $scope.massUpdateFieldList=[
                    {id:1, Field:'ReadOnly',Label:'Read only',Component:'checkbox'},
                    {id:2, Field:'ShowField',Label:'Show field',Component:'checkbox'},
                    {id:3, Field:'Required',Label:'Required',Component:'checkbox'},
                ];
                
                var bag = dragulaService.find($rootScope, 'bag-dataset');
                if (!bag)  
                     $scope.gsrvc.setupDragula('bag-dataset','dataset-toolbox',widgetHandleResource);    
                else {
                    if ($scope.gsrvc.formViewType == 4){
                       if ($scope.gsrvc.formViewTypeCounterDs==0) $scope.gsrvc.destroyDragula('bag-dataset');
                       $scope.gsrvc.setupDragula('bag-dataset','dataset-toolbox',widgetHandleResource); 
                       $scope.gsrvc.addFormViewTypeCounterDs(1);
                    }                      
                 }

                $element.append($compile(template)($scope));  
            }],
          
            link: function (scope, elem, attrs) {
            
            
            scope.hideAnimation=false;
            
            scope.inputArray.countActive=scope.inputArray.length;
            
             scope.countActiveFn = function (){
                scope.countActive=0;
                for (var i=0;i<scope.inputArray.layout.length;i++)
                {
                   if (scope.inputArray.layout[i].Status=='Active') scope.countActive++;
                   
                   if (scope.countActive>0) break;
                    
                }
                return scope.countActive;
             };
              
            scope.removeField = function (index){
						
                if (index > -1) {
							// if (scope.inputArray.layout[index].saved==false) {
                     scope.inputArray.layout[index].Status='Deleted'; //splice(index, 1);
                     scope.inputArray.countActive--;
							// }
				}

            };       
          }
            
        };
    };