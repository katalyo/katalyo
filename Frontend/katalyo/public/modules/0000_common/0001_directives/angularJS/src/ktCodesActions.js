'use strict';

export function ktCodesActions () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktCodesActions.html',
              controller: function(dragulaService)
                {}, 
             link: function (scope, elem, attrs) {
             
             scope.actions = [
                {id1:1,type:1,name1:'Initiate task',taskId:null,actionParams:{}},
                {id1:2,type:2,name1:'Send notification',notificationId:null,actionParams:{}},                
              ];
              
              
               scope.getSeqNoForNewAction = function(){
                  var seqNo=0;
                  var actLen=0;
                  for (var i=0;i<scope.codesActions.length;i++) {
                    
                    
                    if (scope.codesActions[i].actions!= undefined)
                    {
                                          
                      actLen = scope.codesActions[i].actions.length;
                      
                    }
                    
                    seqNo = seqNo+actLen;
                    
                  }
                  
                  return seqNo;
        
     
              
             
               }   
               
            }
    }
};