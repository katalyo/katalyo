'use strict';

export function ktWriteSmartContract () {
   
   var controller = ['$scope', '$uibModal','$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','KatalyoStateManager','UtilityService','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,KatalyoStateManager,UtilityService,$templateCache,$compile,$element) {
     
      $scope.localVarsWidget = {};
      //$scope.fd = {};
      $scope.localVarsWidget.searchFormDownloaded = false;
      $scope.localVarsWidget.formLoaded=false;
      $scope.localVarsWidget.src_type_text=CODES_CONST._POPULATE_TYPE_;
      $scope.localVarsWidget.animationsEnabled = true;
      $scope.localVarsWidget.widgetType = 3;
      $scope.gsrvc = GridsterService;
      $scope.localVarsWidget.formParams={formViewType:1,showGridNavigation:true};
      $scope.fd.contractDatasets = true;
      if ($scope.formData==undefined) $scope.formData={};
      if ($scope.formData[$scope.fd.PresentationId]==undefined) $scope.formData[$scope.fd.PresentationId]=[];
      $scope.fd.msgs={};
      $scope.localVarsWidget.datasetMapperType=0;
      $scope.fd.ShowTab=1;
      if ($scope.fd.PopulateType==undefined) $scope.fd.PopulateType="";
      $scope.fd.token1Contract="0x24E3794605C84E580EEA4972738D633E8a7127c8";
      $scope.fd.token1ContractAbi=[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}];;
      $scope.fd.token2Contract="0x1DE5e000C41C8d35b9f1f4985C23988f05831057";
      $scope.fd.token2ContractAbi=[{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}];;
      $scope.fd.stakingContract="0x8C05bA21557936E3e0713dCE4F9C3bfc974f4C8E";
      $scope.fd.stakingContractAbi=[{"inputs":[{"internalType":"address","name":"_token1","type":"address"},{"internalType":"address","name":"_token2","type":"address"},{"internalType":"uint256","name":"_apy","type":"uint256"},{"internalType":"uint256","name":"_duration","type":"uint256"},{"internalType":"uint256","name":"_tokenRatio","type":"uint256"},{"internalType":"uint256","name":"_maxStakeAmt1","type":"uint256"},{"internalType":"uint256","name":"_rewardAmt1","type":"uint256"},{"internalType":"address","name":"_owner","type":"address"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"sentDate","type":"uint256"}],"name":"AddedToBlackList","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"token1","type":"address"},{"indexed":false,"internalType":"address","name":"token2","type":"address"},{"indexed":false,"internalType":"uint256","name":"apy","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"duration","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"maxStakeAmt1","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"maxStakeAmt2","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"rewardAmt1","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"rewardAmt2","type":"uint256"},{"indexed":false,"internalType":"address","name":"owner","type":"address"},{"indexed":false,"internalType":"uint256","name":"createdAt","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"interestRate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"tokenRatio","type":"uint256"}],"name":"CreatedContract","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"redeemedDate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"EarlyRedeemed","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"redeemedDate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount2","type":"uint256"}],"name":"EarlyRedeemedAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Received","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"redeemedDate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount2","type":"uint256"}],"name":"Redeemed","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"redeemedDate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount1","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount2","type":"uint256"}],"name":"RedeemedAll","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"sentDate","type":"uint256"}],"name":"RemovedFromBlackList","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Reverted","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"address","name":"tokenCtr","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"Staked","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"startDate","type":"uint256"}],"name":"StartStaking","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"stopDate","type":"uint256"}],"name":"StopStaking","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"sentDate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"TransferBackReward","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"sentDate","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"}],"name":"TransferReward","type":"event"},{"stateMutability":"payable","type":"fallback"},{"inputs":[],"name":"activate","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"apy","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address[]","name":"addressList","type":"address[]"},{"internalType":"bool","name":"blStatus","type":"bool"}],"name":"blackList","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"createdAt","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"deActivate","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"duration","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"wallet","type":"address"}],"name":"getBlackListedStatus","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getMyInfo","outputs":[{"components":[{"internalType":"address","name":"wallet","type":"address"},{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"createdAt","type":"uint256"},{"internalType":"bool","name":"redeemed","type":"bool"},{"internalType":"uint256","name":"rewardAmt1","type":"uint256"},{"internalType":"uint256","name":"rewardAmt2","type":"uint256"},{"internalType":"uint256","name":"redeemedAt","type":"uint256"},{"internalType":"uint256","name":"stakeEnd","type":"uint256"}],"internalType":"struct KtlyoStaking.Transaction[]","name":"","type":"tuple[]"},{"components":[{"internalType":"uint256","name":"limit1","type":"uint256"},{"internalType":"uint256","name":"limit2","type":"uint256"}],"internalType":"struct KtlyoStaking.MaxLimit","name":"","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getMyLimits","outputs":[{"components":[{"internalType":"uint256","name":"limit1","type":"uint256"},{"internalType":"uint256","name":"limit2","type":"uint256"}],"internalType":"struct KtlyoStaking.MaxLimit","name":"","type":"tuple"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getMyStakings","outputs":[{"components":[{"internalType":"address","name":"wallet","type":"address"},{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"createdAt","type":"uint256"},{"internalType":"bool","name":"redeemed","type":"bool"},{"internalType":"uint256","name":"rewardAmt1","type":"uint256"},{"internalType":"uint256","name":"rewardAmt2","type":"uint256"},{"internalType":"uint256","name":"redeemedAt","type":"uint256"},{"internalType":"uint256","name":"stakeEnd","type":"uint256"}],"internalType":"struct KtlyoStaking.Transaction[]","name":"","type":"tuple[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getRewardsInfo","outputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getStakeRewardAmounts","outputs":[{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"wallet","type":"address"}],"name":"getStakings","outputs":[{"components":[{"internalType":"address","name":"wallet","type":"address"},{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"createdAt","type":"uint256"},{"internalType":"bool","name":"redeemed","type":"bool"},{"internalType":"uint256","name":"rewardAmt1","type":"uint256"},{"internalType":"uint256","name":"rewardAmt2","type":"uint256"},{"internalType":"uint256","name":"redeemedAt","type":"uint256"},{"internalType":"uint256","name":"stakeEnd","type":"uint256"}],"internalType":"struct KtlyoStaking.Transaction[]","name":"","type":"tuple[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"info","outputs":[{"internalType":"address","name":"","type":"address"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"},{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"maxStakeAmt1","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"maxStakeAmt2","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"indexId","type":"uint256"}],"name":"redeem","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"redeemAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"indexId","type":"uint256"}],"name":"redeemEarly","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"redeemEarlyAll","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"rewardAmt1","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"rewardAmt2","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"tokenContract","type":"address"},{"internalType":"uint256","name":"amt","type":"uint256"}],"name":"stake","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"token1","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"token2","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"reward_amount","type":"uint256"}],"name":"transferBackReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"token","type":"address"},{"internalType":"uint256","name":"reward_amount","type":"uint256"}],"name":"transferReward","outputs":[],"stateMutability":"nonpayable","type":"function"},{"stateMutability":"payable","type":"receive"}];
     
     if ($scope.localVars == undefined)  $scope.localVars= {} 
      $scope.localVars.showMenu = false
      
       $scope.localVars.toggleDropdown = function(){
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
       }
    
     
      $scope.localVarsWidget.gridsterType="resource";
          // $scope.gridsterOpts = GridsterService.getGridsterOptions();
    
    
        
      $scope.localVarsWidget.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: []
      } ;
        
    
  
      //$scope.taskId=$stateParams.id;
      $scope.taskId=$scope.resourceDefinition.ResourceDefId;
      if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
      if ($scope.taskId==undefined) $scope.taskId=0;
      $scope.localVarsWidget.taskId=$scope.taskId;
      $scope.fd.ParentDatasetDefId=$scope.fd.Related;
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.localVarsWidget.formType=$scope.resourceDefinition.form_type;
      $scope.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.formDefinition = $scope.resourceDefinition.form_definition;
        
      if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
      $scope.localVarsWidget.resourceRecordId=0;
      if ($scope.taskInitiateId==undefined)
      {
        $scope.taskInitiateId =0;
      
      }
      if ($scope.taskExecuteId==undefined)
      {
         $scope.taskExecuteId =0;  
      }
        if ($scope.prevTaskExeId==undefined)
        {
            $scope.prevTaskExeId =0;
        }
        
      //register widget 
      var out_fields = ['resourceId'];
      //if ($scope.fd.LocalId==undefined || $scope.fd.LocalId==null || $scope.fd.LocalId==0)
      //{
         var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'viewResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.localVarsWidget.selectedResource, output:out_fields, };
         $scope.localVarsWidget.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
         $scope.localVarsWidget.LocalId=$scope.localVarsWidget.localWidgetId;
         $scope.fd.LocalId=$scope.localVarsWidget.localWidgetId;
         //$scope.fd.LocalId=UtilityService.uuidNow();
         
      //}
      $scope.ksm = KatalyoStateManager;
      
       let dependent_field_pid = $scope.fd.LocalId;
        let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
        let dependent_field_pid2
        let update_presentation_id2 = function(data) {
            let presentation_id = angular.copy($scope.fd.PresentationId)
            if (data[presentation_id]!==undefined)
            {
                $scope.fd.PresentationId = data[presentation_id];
            //unsubscribe here later after implementing unsubscribe - ToDo
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            }
        }
        let update_presentation_id = function(data) {
            if (data[$scope.fd.LocalId]>0 && $scope.fd.PresentationId==0 && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
          
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            //unsubscribe here later after implementing unsubscribe - ToDo
        }
        
        
        if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0)
        {
            let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
        } else{
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
        }
      
       let apply_value = function(data)
        {
            let do_refresh=false;
            if (data!=undefined)
            {
               if (data != $scope.localVarsWidget.resourceRecordId)
               {
                  if (typeof data ==='number') $scope.localVarsWidget.resourceRecordId= data;
                  else if (Array.isArray(data))
                  {
                     if (data.length>0)
                     {
                      $scope.localVarsWidget.resourceRecordId= data[0].id  
                     }
                  }else
                  {
                     $scope.localVarsWidget.resourceRecordId = data.id;
                  }
                  do_refresh = true;
               }
            }
            //get selectedRecord
            if ($scope.localVarsWidget.resourceRecordId == null) $scope.localVarsWidget.resourceRecordId=0;
            
            if ($scope.localVarsWidget.resourceRecordId>0 && do_refresh) $scope.localVarsWidget.getFormResourceWithData();
            
            
        };
        
      
      if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
      
      if ($scope.fd.Parameters.RefreshOnIdChange)
      {
         
         //find 
         //subscribe observer
           
         let dependent_field = "Field"+$scope.fd.PresentationId;
         let subscribedSubject = "resourceidValueStateName"+$scope.fd.Parameters.ResourceIdItemId+$scope.resourceDefinition.transaction_id;
         let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
         let val = $scope.ksm.getState(subscribedSubject);
         $scope.localVarsWidget.resourceRecordId= val.id;
            
            //get selectedRecord
         if ($scope.localVarsWidget.resourceRecordId == null) $scope.localVarsWidget.resourceRecordId=0;
            
         if ($scope.localVarsWidget.resourceRecordId>0) $scope.localVarsWidget.getFormResourceWithData();
      }
      
      //subscribe observer
      if ($scope.fd.PopulateType.toString().substr(0,1)=="4")
      {
           /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
        
       
        if ($scope.fd.SrcWidgetId!=undefined && $scope.fd.SrcWidgetId!=null)
        
        {
           
                let dependent_field = "Field"+$scope.fd.SrcWidgetId;
               // let subscribedSubject = $scope.fd.Parameters.SrcWidgetType+"ValueStateName"+$scope.fd.SrcWidgetId;
                let subscribedSubject = "rlistValueStateName"+$scope.fd.SrcWidgetId+$scope.resourceDefinition.transaction_id;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            
        }  
         
      }
      
      let refresh_widget_data = function()
      
      {
         $scope.localVarsWidget.getFormResourceWithData();    
         
      }
      if ($scope.fd.Parameters.EventName!=undefined)
      {
         if ($scope.fd.Parameters.EventName!="")
         {
            let dependent_field2 = "Field"+$scope.fd.PresentationId;
            let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
            let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,refresh_widget_data);
         }
      }
      
      $scope.localVarsWidget.showProperties=function(datasetMapperType)
      {
            
            if ($scope.localVarsWidget.datasetMapperType==undefined)
            {
               $scope.localVarsWidget.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.localVarsWidget.datasetMapperType==0 || $scope.localVarsWidget.datasetMapperType!=datasetMapperType) $scope.localVarsWidget.datasetMapperType = datasetMapperType;
               else $scope.localVarsWidget.datasetMapperType=0;
            }
            
      };
           
      $scope.localVarsWidget.setField = function(form,name,value,msg)
      {
         TaskDataService.setField(form,name,value);
         if (msg) MessagingService.addMessage(msg,'info');
      };  
      
      $scope.localVarsWidget.getResourceAll = function () {
          
            ResourcesDataService.getResourceDefAll($scope.fd.Related).then(function(data) {
                               
                             // MessagingService.addMessage(data.msg,'success');
                              $scope.fd.resource = data.data;
                              $scope.localVarsWidget.resourceLoaded = true;
                              
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };
      //$scope.localVarsWidget.getResourceAll();
      
        
      $scope.localVarsWidget.getFormResourceOriginal = function(datasetId){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null)
              {
                $scope.fd.PresentationId = 0;
                
              }
              $scope.localVarsWidget.formLoaded=false;
              ResourcesDataService.getResourceForm(datasetId,"i",lang).then(function(data) {
                            
                                $scope.localVarsWidget.resourceWidgetDefinition= data.data.resourceDefinition;
                                if (Array.isArray(data.data.widgets)) $scope.localVarsWidget.dashboard.widgets.layout =  data.data.widgets;
                                
                                if ($scope.localVarsWidget.selectedResourceItem!=undefined)
                                 { 
                                  $scope.localVarsWidget.selectedResourceItem.name=$scope.localVarsWidget.resourceWidgetDefinition.name;
                                  $scope.localVarsWidget.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.localVarsWidget.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_text=$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType];
                                  $scope.localVarsWidget.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.localVarsWidget.selectedResourceItem={'name':$scope.localVarsWidget.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                                $scope.fd.wItemValue=data.data.widgets;
                                
                           /*
                                var container = $element.children().eq(0);
                                GridsterService.setupDragular($scope.fd.wItemValue,container,'rows-'+$scope.fd.PresentationId,'row-'+$scope.localVarsWidget.gridsterType+'-hadle',true);
                             */   
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.ResourceRecordId}]}]});
						
                                  $scope.localVarsWidget.searchFormDownloaded = true;
                                  //$timeout(function(){$scope.localVarsWidget.formLoaded=true});
                                  $scope.localVarsWidget.formLoaded=true;
                                
                               // $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'Required',false);     
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      
      $scope.fd.openStakeScreen=function(token){
         
         $scope.fd.ShowTab=2;
         if (token=='BNF') $scope.fd.ShowTab=3;
         $scope.fd.currentTokenTicker=token;
         $scope.fd.confirmation1 = null;
         $scope.fd.transactionHash1 = null;
         $scope.fd.errorMessage1 = null;
         $scope.fd.confirmation2 = null;
         $scope.fd.transactionHash2 = null;
         $scope.fd.errorMessage2 = null;
         
         if ($scope.fd.eth_address==undefined)
         {
            web3.eth.getAccounts().then(function (result) {
                      $timeout(function(){
                           $scope.fd.eth_address = result[0];
                            $scope.fd.getAllowance($scope.fd.eth_address,token);
                           $scope.fd.getMyLimits(token);
                      });
            });
         
         } else
         {
           $scope.fd.getAllowance($scope.fd.eth_address,token);
         $scope.fd.getMyLimits(token); 
            
         }
        
       
      }
      
      $scope.fd.getAllowance = function(wallet,token){
                  
                   var tokenAddress;
                   var tokenABI;
                   var contractAddress = $scope.fd.stakingContract;
                   if (token=="KTLYO")
                   {
                     tokenAddress= $scope.fd.token1Contract;
                     tokenABI = $scope.fd.token1ContractAbi;
                   }else
                   {
                     tokenAddress= $scope.fd.token2Contract;
                     tokenABI = $scope.fd.token2ContractAbi;
                     
                     
                   }
                  
            let web3;       
            if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to web3 wallet!",'error');
            }
                     
               if (web3!=null)
               {
                                var tokenInst = new web3.eth.Contract(tokenABI,tokenAddress);
                                
        
                                tokenInst.methods.allowance(wallet,contractAddress).call().then(function (allowance_raw) {
                                var bal = web3.utils.fromWei(allowance_raw, 'ether');
                                
                                $timeout(function(){$scope.fd.allowance = bal;});
                                });
                  }    
                 
      }
      
         $scope.fd.setAllowance = function(){
                  
                  $scope.fd.confirmation1 = null;
                  $scope.fd.transactionHash1 = null;
                  $scope.fd.errorMessage1 = null;
                   let web3;
                   var tokenAddress,tokenABI;
                   var spender = $scope.fd.stakingContract;
                   var owner = $scope.fd.eth_address;
                   var token = $scope.fd.currentTokenTicker;
                   if (token=="KTLYO")
                   {
                     tokenAddress= $scope.fd.token1Contract;
                     tokenABI = $scope.fd.token1ContractAbi;
                   }else
                   {
                     tokenAddress= $scope.fd.token2Contract;
                     tokenABI = $scope.fd.token2ContractAbi;
                     
                     
                   }
                   if (window.ethereum)
                     {
                      web3= new Web3(window.ethereum);  
                     }else if (window.web3)
                     {
                        web3= new Web3(window.web3); 
                     }else
                     {
                        web3 = null;
                        MessagingService.addMessage("Error connecting to wallet!",'error');
                     }
                              
                        if (web3!=null)
                        {
                                var tokenInst = new web3.eth.Contract(tokenABI,tokenAddress);
                                
                                tokenInst.options.from = owner;
                                let amount_wei = web3.utils.toWei($scope.fd.ApproveAmount.toString(), 'ether');
                                
                                tokenInst.methods.approve(spender,amount_wei).send()
                                .on('transactionHash', function(hash){
                                 
                                    $timeout(function(){$scope.fd.transactionHash1=hash;
                                              $scope.fd.pending1=true;});
                                    
                                 })
                                 .on('confirmation', function(confirmationNumber, receipt){
                                    if (confirmationNumber>0)
                                    {
                                    $timeout(function(){$scope.fd.confirmation1=confirmationNumber;
                                              $scope.fd.pending1=false;
                                             
                                              });
                                     $scope.fd.getAllowance($scope.fd.eth_address,$scope.fd.currentTokenTicker);
                                    }
                                 })
                                 .on('receipt', function(receipt){
                                $timeout(function(){$scope.fd.trxReceipt1=receipt;
                                         $scope.fd.status1=receipt.status;  
                                         });
                                  
                                  //console.log(receipt);

                                 })
                              .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                                  $timeout(function(){$scope.fd.errorMessage1=error.message;
                                 if (receipt!=undefined)  $scope.fd.status1=receipt.status;  
                                  });
                              });
                                
                               
                  } else MessagingService.addMessage("Error connecting to wallet!",'error');     
                 
      }
      
       $scope.fd.stake = function(){
                  
                   var tokenAddress,tokenABI;
                   var contractAddress = $scope.fd.stakingContract;
                   var contractABI = $scope.fd.stakingContractAbi;
                   var owner = $scope.fd.eth_address;
                   var token = $scope.fd.currentTokenTicker;
                   $scope.fd.confirmation2 = null;
                  $scope.fd.transactionHash2 = null;
                  $scope.fd.errorMessage2 = null;
                  let web3;
                   if (token=="KTLYO")
                   {
                     tokenAddress= $scope.fd.token1Contract;
                     tokenABI = $scope.fd.token1ContractAbi;
                   }else
                   {
                     tokenAddress= $scope.fd.token2Contract;
                     tokenABI = $scope.fd.token2ContractAbi;
                     
                     
                   }
                  if (window.ethereum)
                  {
                   web3= new Web3(window.ethereum);  
                  }else if (window.web3)
                  {
                     web3= new Web3(window.web3); 
                  }else
                  {
                     web3 = null;
                     MessagingService.addMessage("Error connecting to web3 wallet!",'error');
                  }
                     
               if (web3!=null)
               {
                  
                               
                                var contractInst = new web3.eth.Contract(contractABI,contractAddress);
                                
                                contractInst.options.from = owner;
                                let amount_wei = web3.utils.toWei($scope.fd.StakeAmount.toString(), 'ether');
                                contractInst.methods.stake(tokenAddress,amount_wei).send()
                                .on('transactionHash', function(hash){
                                 
                                    $timeout(function(){$scope.fd.transactionHash2=hash;
                                              $scope.fd.pending2=true;});
                                    
                                 })
                                 .on('confirmation', function(confirmationNumber, receipt){
                                    if (confirmationNumber>0)
                                    {
                                     $timeout(function(){$scope.fd.confirmation2=confirmationNumber;
                                               $scope.fd.pending2=false;
                                               });
                                    }
                                 })
                                 .on('receipt', function(receipt){
                                 $timeout(function(){$scope.fd.trxReceipt2=receipt;
                                          $scope.fd.status2=receipt.status;  
                                          });
                                  $scope.fd.getAllowance($scope.fd.eth_address,token);
                                  //console.log(receipt);

                                 })
                              .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                                 $timeout(function(){$scope.fd.errorMessage2=error.message;
                                  $scope.fd.pending2=false;
                                  if (receipt!=undefined) $scope.fd.status2=receipt.status;
                                  });
                              });

                              /*
                                  contractInst.methods.stake(tokenAddress,amount_wei).send().then(function (trx) {
                                
                                    MessagingService.addMessage(trx,'success');
                                
                                },function(error){
                                 MessagingService.addMessage(error.message,'error');     
                                 });
                                 */
                  } else MessagingService.addMessage("Error connecting to wallet!",'error');     
                 
      }
      
      $scope.fd.Redeem = function(early,index){
            
            var web3;
            
            if ($scope.fd.msgs[index]==undefined) $scope.fd.msgs[index] = {confirmation : null,transactionHash : null,errorMessage : null,pending:null,trxReceipt:null,status : null};
            else
            {
               $scope.fd.msgs[index].confirmation=null;
               $scope.fd.msgs[index].transactionHash = null;
               $scope.fd.msgs[index].errorMessage = null;
               $scope.fd.msgs[index].trxReceipt = null;
               $scope.fd.msgs[index].pending = null;
               $scope.fd.msgs[index].status = null;
            
            }
            
            $scope.fd.currectIndex=index;      
            
            if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to web3 wallet!",'error');
            }
                     
               if (web3!=null)
               {
                        var contractAddress = $scope.fd.stakingContract;
                        var contractABI = $scope.fd.stakingContractAbi;
                        
                        var contractInst = new web3.eth.Contract(contractABI,contractAddress);
                         
                        web3.eth.getAccounts().then(function (result) { 
                              $scope.fd.eth_address = result[0];
                              
                              contractInst.options.from = $scope.fd.eth_address;
                               
                           if (early)
                           {
                              contractInst.methods.redeemEarly(index).send()
                               .on('transactionHash', function(hash){
                           
                                    $timeout(function(){$scope.fd.msgs[index].transactionHash=hash;
                                       $scope.fd.msgs[index].pending=true;
                                       });
                              
                              })
                              .on('confirmation', function(confirmationNumber, receipt){
                                 if (confirmationNumber>0)
                                    {
                                       $timeout(function(){$scope.fd.msgs[index].confirmation=confirmationNumber;
                                          $scope.fd.msgs[index].pending=false;
                                          
                                          });
                                    }
                              })
                              .on('receipt', function(receipt){
                                 $timeout(function(){
                                       $scope.fd.msgs[index].trxReceipt=receipt;
                                       $scope.fd.msgs[index].status=receipt.status;  
                                        
                                           
                                 });
                               
                                 $scope.fd.getMyStakings();
                                 

                              })
                              .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                                  $timeout(function(){$scope.fd.msgs[index].errorMessage=error.message;
                                          if (receipt!=undefined) $scope.fd.msgs[index].status=receipt.status;
                                           $scope.fd.msgs[index].pending=false;});
                              });
                             
                           }
                           else{
                                 contractInst.methods.redeem(index).send()
                                 .on('transactionHash', function(hash){
                           
                                    $timeout(function(){$scope.fd.msgs[index].transactionHash=hash;
                                             $scope.fd.msgs[index].pending=true;});
                                    
                                 })
                                 .on('confirmation', function(confirmationNumber, receipt){
                                    if (confirmationNumber>0)
                                    {
                                    $timeout(function(){
                                       $scope.fd.msgs[index].confirmation=confirmationNumber;
                                             $scope.fd.msgs[index].pending=false;});
                                    }
                                 })
                                 .on('receipt', function(receipt){
                                    
                                $timeout(function(){
                                 
                                         $scope.fd.msgs[index].trxReceipt=receipt;
                                         $scope.fd.msgs[index].status=receipt.status; 
                                         
                                         
                                         });
                                  $scope.fd.getMyStakings();
                                 

                                 })
                              .on('error', function(error, receipt) { // If the transaction was rejected by the network with a receipt, the second parameter will be the receipt.
                                  $timeout(function(){
                                          $scope.fd.msgs[index].errorMessage=error.message;
                                    
                                          if (receipt!=undefined)  $scope.fd.msgs[index].status=receipt.status;
                                           $scope.fd.msgs[index].pending=false;});
                                 
                              });
                        
                           }
               });
      
            } 
                 
      };
      
      $scope.fd.getBalance = function(token){
                  
                  let web3;
                   var tokenAddress,tokenABI;
                   
                   if (token=="KTLYO")
                   {
                     tokenAddress= $scope.fd.token1Contract;
                     tokenABI = $scope.fd.token1ContractAbi;
                   }else
                   {
                     tokenAddress= $scope.fd.token2Contract;
                     tokenABI = $scope.fd.token2ContractAbi;
                     
                     
                   }
           if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to web3 wallet!",'error');
            }
                     
               if (web3!=null)
               {
                                var tokenInst = new web3.eth.Contract(tokenABI,tokenAddress);
                                          
        
                                tokenInst.methods.balanceOf($scope.fd.eth_address).call().then(function (bal_raw) {
                                var bal = web3.utils.fromWei(bal_raw, 'ether');
                                if (bal<=100)
                                {
                                        $scope.ktlyo_balance_ok = false;
                                        $scope.ktlyo_balance_msg = "Insufficient balance";
                                }else $scope.ktlyo_balance_ok = true;
                                $timeout(function(){$scope.balance_ktlyo = bal+ " KTLYO";});
                                });
                    }   
               };
        
        
         
          $scope.fd.formatNumber = function(input_string){
            let number;
            
           if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to web3 wallet!",'error');
            }
                     
               if (web3!=null)
               {
                  
              
               number = Number(web3.utils.fromWei(input_string, 'ether')).toLocaleString(undefined, { maximumFractionDigits: 18 });
                               
               }
            
            /*
           let decimals = ("000000000000000000" + input_string.substr(input_string.length - 18)).slice(-18);
           if (input_string.length<19) number= "0."+decimals;
           else number = input_string.substring(0,input_string.length - 18)+"."+decimals;
           */
           return number;
          }
          
          
          $scope.fd.getAmount = function(token,forToken,amount,amountReward){
            let number,numberReward;
            
            let tokenTicker = $scope.fd.getTokenTicker(token);
            if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to wallet!",'error');
            }
                     
               if (web3!=null)
               {
               
               number = Number(web3.utils.fromWei(amount, 'ether'));
               numberReward = Number(web3.utils.fromWei(amountReward, 'ether'));
               if (tokenTicker==forToken) number = number+numberReward;
               else number = numberReward;
               number = number.toLocaleString(undefined, { maximumFractionDigits: 4 });
                               
            }
           
           return number;
          }
          
          $scope.fd.isReleased = function(stake){
            
              if (stake.stakeEnd>=$scope.fd.currentDate) return false;
              else return true;
           }
          
         
          
          
          $scope.fd.formatDate = function(input_timestamp){
            let date_var;
            let options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric',hour: '2-digit', minute:'2-digit',second:'2-digit',hour12:false };
            date_var = new Date(input_timestamp*1000).toLocaleDateString(undefined,options);
          
           return date_var;
          }
        
        $scope.fd.getTokenTicker = function(tokenAddress){
        
         let tokenTicker;
         if (tokenAddress.toLowerCase()== $scope.fd.token1Contract.toLowerCase())
         {
            tokenTicker="KTLYO";
         }else
         {
            tokenTicker="BNF";
                     
         }
         return tokenTicker;
        }
        
        $scope.fd.setMax = function(){
         
         if ($scope.fd.remainStakeLimit > $scope.fd.allowance) $scope.fd.StakeAmount = $scope.fd.allowance;
         else $scope.fd.StakeAmount = $scope.fd.remainStakeLimit;
         
         
        };
        $scope.fd.setMaxAllowance = function(){
         
         $scope.fd.ApproveAmount = $scope.fd.remainStakeLimit;
         
        };
        $scope.fd.getMyLimits = function(token){
              var lang=$rootScope.globals.currentUser.lang;
             
             if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to wallet!",'error');
            }
                     
               if (web3!=null)
               {                 
                  web3.eth.getAccounts().then(function (result) {
                      $timeout(function(){
                           $scope.fd.eth_address = result[0];
                           if ($scope.fd.eth_address!=null)
                           {
                              
                              //get address and abi
                              var contractAddress = $scope.fd.stakingContract;
                              var contractABI = $scope.fd.stakingContractAbi;
                               
                              var contractInst = new web3.eth.Contract(contractABI,contractAddress);
                                
                                contractInst.options.from = $scope.fd.eth_address;
        
                                contractInst.methods.getMyLimits().call().then(function (data) {
                                    
                                    if (token=="KTLYO") $scope.fd.spentStakeLimit=web3.utils.fromWei(data.limit1,'ether');
                                    else $scope.fd.spentStakeLimit=web3.utils.fromWei(data.limit2,'ether');
                                    
                                    
                                     contractInst.methods.getStakeRewardAmounts().call().then(function (data) {
                                    
                                    $timeout(function(){
                                       if (token=="KTLYO") $scope.fd.maxStakeLimit=web3.utils.fromWei(data[4],'ether');
                                       else $scope.fd.maxStakeLimit=web3.utils.fromWei(data[5],'ether');
                                       $scope.fd.maxStakeLimitPrint = Number($scope.fd.maxStakeLimit).toLocaleString(undefined, { maximumFractionDigits: 18 });
                                       $scope.fd.remainStakeLimit = $scope.fd.maxStakeLimit - $scope.fd.spentStakeLimit;
                                       $scope.fd.remainStakeLimitPrint = Number($scope.fd.remainStakeLimit).toLocaleString(undefined, { maximumFractionDigits: 18 });
                                    })
                                 },
                                function(error){
                                    MessagingService.addMessage(error.message,'error')
                                 });
                                
                                },
                                function(error){
                                    MessagingService.addMessage(error.message,'error')
                                 });
                                
                                contractInst.methods.getStakeRewardAmounts().call().then(function (data) {
                                    
                                    if (token=="KTLYO") $scope.fd.maxStakeLimit=data.maxStakeAmt1;  
                                    else $scope.fd.maxStakeLimit=data.maxStakeAmt2;
                                
                                },
                                function(error){
                                    MessagingService.addMessage(error.message,'error')
                                 });
                             
                              
                           
                           }else MessagingService.addMessage("Error connecting to wallet!",'error') ;
                      });
                  },function(error){
                        $timeout(function(){
                          
                           $scope.fd.error_msg = error.message;
                           
                        });
                  });
                          
                 
                }

        }
         
   
          
          
        $scope.fd.getMyStakings = function(){
              var lang=$rootScope.globals.currentUser.lang;
              $scope.fd.afterQuery=false;
              //$scope.fd.myStakes=[];
            
            if (window.ethereum)
            {
             web3= new Web3(window.ethereum);  
            }else if (window.web3)
            {
               web3= new Web3(window.web3); 
            }else
            {
               web3 = null;
               MessagingService.addMessage("Error connecting to wallet!",'error');
            }
                     
               if (web3!=null)
               {
                  

                                    
                  web3.eth.getAccounts().then(function (result) {
                      
                           $scope.fd.eth_address = result[0];
                           if ($scope.fd.eth_address!=null)
                           {
                              
                              //get address and abi
                              var contractAddress = $scope.fd.stakingContract;
                              var contractABI = $scope.fd.stakingContractAbi;
                               
                              var contractInst = new web3.eth.Contract(contractABI,contractAddress);
                                
                                contractInst.options.from = $scope.fd.eth_address;
        
                                contractInst.methods.getMyStakings().call().then(function (data) {
                                    
                                    $scope.fd.currentDate = Math.floor(Date.now() / 1000);
                                
                                    $timeout(function(){ $scope.fd.myStakes = data;$scope.fd.afterQuery=true;});
                                
                                },
                                function(error){
                                    MessagingService.addMessage(error.message,'error')
                                 });
                             
                              
                           
                           }else MessagingService.addMessage("Error connecting to wallet!",'error') ;
                     
                  },function(error){
                        $timeout(function(){
                          
                           $scope.fd.error_msg = error.message;
                           
                        });
                  });
                          
                 
                }

        }
        
        
        
        //$scope.fd.getMyStakings();
        
        $scope.localVarsWidget.getFormResource = function(){
              var lang=$rootScope.globals.currentUser.lang;
               $scope.localVarsWidget.formLoaded=false;
               if ($scope.fd.PresentationId==null)
               {
                    $scope.fd.PresentationId=0;
            
               }
               $scope.localVarsWidget.formLoading=true;
                         
              ResourcesDataService.getResourceFormExtended($scope.fd.Related,$scope.taskId,$scope.localVarsWidget.formType,$scope.fd.PresentationId,"w",lang).then(function(data) {
                               
                                  $scope.localVarsWidget.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.localVarsWidget.dashboard.widgets.layout =  data.data.widgets;
                                  
                                 if ($scope.localVarsWidget.selectedResourceItem!=undefined)
                                 { 
                                  $scope.localVarsWidget.selectedResourceItem.name=$scope.localVarsWidget.resourceWidgetDefinition.name;
                                  $scope.localVarsWidget.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.localVarsWidget.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_text=$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType];
                                  $scope.localVarsWidget.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.localVarsWidget.selectedResourceItem={'name':$scope.localVarsWidget.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                              
                                $scope.fd.wItemValue=data.data.widgets;
                               
                                
                                  
                                $scope.localVarsWidget.searchFormDownloaded = true;
                                
                                $timeout(function(){    
                                   $scope.localVarsWidget.formLoaded=true;
                                   $scope.localVarsWidget.formLoading=false;
                                });
                                  
                               
                              //  $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'Required',false);
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 $scope.localVarsWidget.formLoading=false;
                                 
                                
                             });
                              
      };
      
              
        $scope.localVarsWidget.getFormResourceWithData = function(){
              var lang=$rootScope.globals.currentUser.lang;
              $scope.localVarsWidget.formLoaded=false;
              if ($scope.fd.PresentationId==null || $scope.fd.PresentationId==undefined)
              {
                $scope.fd.PresentationId = 0;
                
              }
              
              if ($scope.fd.ParentElementId==null || $scope.fd.ParentElementId==undefined)
              {
                $scope.fd.ParentElementId = 0;
                
              }
              
              
              
              
               if ($scope.localVarsWidget.resourceRecordId==undefined || $scope.localVarsWidget.resourceRecordId==0)
               {
                
                  if ($scope.localVarsWidget.previousTaskData['datasetRecordId']!=undefined && $scope.localVarsWidget.previousTaskData['datasetRecordId']!="" && $scope.localVarsWidget.previousTaskData['datasetId']==$scope.fd.Related) $scope.localVarsWidget.resourceRecordId = $scope.localVarsWidget.previousTaskData['datasetRecordId'];
                
               }
               if (typeof $scope.localVarsWidget.resourceRecordId !=='number' && typeof $scope.localVarsWidget.resourceRecordId !=='string')
               {
                  
                  if (Array.isArray($scope.localVarsWidget.resourceRecordId))
                  {
                     if ($scope.localVarsWidget.resourceRecordId.length>0)
                     {
                        $scope.localVarsWidget.resourceRecordId= $scope.localVarsWidget.resourceRecordId[0].id  
                     
                     }
                  }
                  else $scope.localVarsWidget.resourceRecordId = $scope.localVarsWidget.resourceRecordId.id;                 
               }
               
              ResourcesDataService.getResourceFormExtendedWithData($scope.fd.Related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.PopulateType,$scope.fd.ParentElementId,$scope.fd.PresentationId,$scope.prevTaskExeId,$scope.localVarsWidget.formType,$scope.localVarsWidget.widgetType,$scope.localVarsWidget.resourceRecordId,"w",lang).then(function(data) {

                                  $scope.formData[$scope.fd.PresentationId] =  data.data.form_data;
                                  if ($scope.formData[$scope.fd.PresentationId]==undefined) $scope.formData[$scope.fd.PresentationId]=[];
                                  $scope.localVarsWidget.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.localVarsWidget.dashboard.widgets.layout =  data.data.widgets;
                                 
                                  if ($scope.formData[$scope.fd.PresentationId].length>0)
                                  {
                                    $scope.localVarsWidget.resourceRecordId = $scope.formData[$scope.fd.PresentationId][0]['id'];
                                  }
                                  $scope.localVarsWidget.datasetRecordId=$scope.localVarsWidget.resourceRecordId;
                                  if ($scope.localVarsWidget.selectedResourceItem!=undefined)
                                 { 
                                  $scope.localVarsWidget.selectedResourceItem.name=$scope.localVarsWidget.resourceWidgetDefinition.name;
                                  $scope.localVarsWidget.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.localVarsWidget.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_text=$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType];
                                  $scope.localVarsWidget.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.localVarsWidget.selectedResourceItem={'name':$scope.localVarsWidget.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                                    //$scope.fd.wItemValue=data.data.widgets;
                               
                                  // $timeout(function() {
                                     $scope.localVarsWidget.formLoaded=true;
                                    // $scope.localVarsWidget.initiateFormLoaded=true;
                                     $scope.localVarsWidget.formLoading=false;
                                     $scope.localVarsWidget.searchFormDownloaded = true;
                                      
                                  //  });   
                                  
                                   
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - VIEW_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVarsWidget.resourceRecordId}]}]});
                                
                               // $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'Required',false);   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      
      
           $scope.localVarsWidget.setSelectedDataset = function(id)  {
              
              if ($scope.localVarsWidget.selectedResourceItem==undefined) $scope.localVarsWidget.selectedResourceItem={};
              var pos = $scope.localVarsWidget.resourceList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.localVarsWidget.selectedResourceItem.id = $scope.localVarsWidget.resourceList[pos].id;
                $scope.localVarsWidget.selectedResourceItem.name = $scope.localVarsWidget.resourceList[pos].name;
                $scope.localVarsWidget.selectedDataset = $scope.localVarsWidget.resourceList[pos];
                $scope.localVarsWidget.selectedResourceItem.name_confirmed = $scope.localVarsWidget.selectedResourceItem.name +' ('+$scope.localVarsWidget.selectedResourceItem.id +')'
                $scope.localVarsWidget.selectedResourceItem_c=$scope.localVarsWidget.selectedResourceItem;
                }
					}
          
                  
          // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
	$scope.localVarsWidget.getResources = function()  {
						
            if ($scope.localVarsWidget.resourceList==undefined) $scope.localVarsWidget.resourceList=[];
          
            var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'smartcontract').then(
						function(data) {
										
											  $scope.localVarsWidget.resourceList = data.data;
                       if ($scope.fd.Related!=null)
                       {
                        $scope.localVarsWidget.setSelectedDataset($scope.fd.Related);
                
                        }     
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					}
   
         
  if ($scope.fd.Related!=null &&  !$scope.formDefinition && !$scope.localVarsWidget.searchFormDownloaded)
  {
    
    // $scope.localVarsWidget.getFormResourceWithData();
    // $scope.localVarsWidget.getDatasets();
    // $scope.localVarsWidget.setSelectedDataset($scope.fd.Related);
    
  }else if ($scope.fd.Related!=null)
  {
     //$scope.localVarsWidget.getFormResource();
     //$scope.localVarsWidget.getResources();
     //$scope.localVarsWidget.setSelectedDataset($scope.fd.Related);
    
  } 

    


       
     
                              
    
            let template;
            //if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskViewResource.html');
            //else template = $templateCache.get('ktdirectivetemplates/ktTaskViewResourceExe.html');
            template = $templateCache.get('ktdirectivetemplates/ktWriteSmartContract.html');
            $element.append($compile(template)($scope)); 

  
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

            }     
  }
  
};