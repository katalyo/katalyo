'use strict';

export function ktMassUpdateProperties (KatalyoStateManager) {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktMassUpdateProperties.html',
             controller: function($scope){    
                },
             link: function (scope, elem, attrs) {
              
              //
            scope.ksm = KatalyoStateManager;

            
            let massUpdateState= scope.ksm.addSubject('massUpdateState',null);
            let massUpdateStateName = massUpdateState.subject.name;

            let activate_mass_update = function(data)
            {
                    scope.fd.massUpdate = data;
            }
            
            let dependent_field = "Field"+scope.fd.PresentationId;
            let observer = scope.ksm.subscribe(massUpdateStateName,dependent_field,"massUpdateChanged",activate_mass_update);
            let state = scope.ksm.getState(massUpdateStateName);
            if (state.status=="ok" && state.state!==null) scope.fd.massUpdate = state.state;
            }
      }
  
};