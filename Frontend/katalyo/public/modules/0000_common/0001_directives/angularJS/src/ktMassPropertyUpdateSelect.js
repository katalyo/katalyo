'use strict';



export function ktMassPropertyUpdateSelect (KatalyoStateManager, TaskDataService,MessagingService) {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktMassPropertyUpdateSelect.html',
              controller: function($scope){
          
             },
             link: function (scope, elem, attrs) {
                   
              //To do - get this from server (db table)
            scope.fd.massUpdateFieldList=[
                {id:1, Field:'ReadOnly',Label:'Read only',Component:'checkbox'},
                {id:2, Field:'ShowField',Label:'Show field',Component:'checkbox'},
                {id:3, Field:'Required',Label:'Required',Component:'checkbox'},
                {id:4, Field:'Parameters[ValueFontBold]',Label:'Value-Font-Bold',Component:'checkbox'}
            ];
              
            scope.ksm = KatalyoStateManager;
            scope.taskDataService = TaskDataService;
            scope.messagingService  = MessagingService

              let massUpdateState= scope.ksm.addSubject('massUpdateState',null);
              let massUpdateStateName = massUpdateState.subject.name;

              let activate_mass_update = function(data)
              {
                   massUpdateState.subject.setStateChanges = data;
                   massUpdateState.subject.notifyObservers("massUpdateChanged");
              }
            
             let action_mass_update_changed = scope.ksm.addAction(massUpdateStateName,"massUpdateChanged",activate_mass_update);
             
             scope.activateMassUpdateFn = function(item)
              {
                  scope.ksm.executeAction(massUpdateStateName,"massUpdateChanged", {Active:true,Item:item});
              }
              
              scope.deActivateMassUpdateFn = function(index)
              {
                  scope.ksm.executeAction(massUpdateStateName,"massUpdateChanged", {Active:false,Item:null});
                  $('#massPropertyUpdate'+ scope.fd.PresentationId).modal('hide')
              }
              
              
               scope.hideMassUpdateFn = function()
              { 
                 $('#massPropertyUpdate'+ scope.fd.PresentationId).modal('hide') 

                 if (scope.localVars.turnAllPropertiesOn) {        
                    scope.taskDataService.setField(scope.inputArray.layout, scope.selected.Field ,  true );  
                    scope.messagingService.addMessage('All fields are ' + scope.selected.Label ,'info');
                 }
                 
                 if (scope.localVars.turnAllPropertiesOff) {
                    scope.taskDataService.setField(scope.inputArray.layout, scope.selected.Field ,  false );  
                    scope.messagingService.addMessage('All fields are not ' + scope.selected.Label ,'info'); 
                 }
                 scope.localVars.turnAllPropertiesOn = false
                 scope.localVars.turnAllPropertiesOff = false    
              }

             }
        }
};