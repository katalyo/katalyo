'use strict';

export function ktReadSmartContract () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','KatalyoStateManager','UtilityService','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,KatalyoStateManager,UtilityService,$templateCache,$compile,$element) {
     
     
      //$scope.fd = {};
      if ($scope.fd.formData==undefined)  $scope.fd.formData=[{}];
      $scope.fd.searchFormDownloaded = false;
      $scope.fd.formLoaded=false;
      $scope.fd.src_type_text=CODES_CONST._POPULATE_TYPE_;
      $scope.fd.animationsEnabled = true;
      $scope.fd.widgetType = 3;
      $scope.gsrvc = GridsterService;
      $scope.fd.formParams={formViewType:1,showGridNavigation:true};
      $scope.fd.contractDatasets = true;


      if ($scope.localVars == undefined)  $scope.localVars= {} 
      $scope.localVars.showMenu = false
      
      $scope.localVars.toggleDropdown = function(){
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
      }
       
       
      $scope.fd.datasetMapperType=0;
      
      if ($scope.fd.PopulateType==undefined) $scope.fd.PopulateType="";
     
      $scope.fd.gridsterType="resource";
          // $scope.gridsterOpts = GridsterService.getGridsterOptions();
        
      $scope.fd.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: []
      } ;
        
    
  
      //$scope.taskId=$stateParams.id;
      $scope.taskId=$scope.resourceDefinition.ResourceDefId;
      if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
      if ($scope.taskId==undefined) $scope.taskId=0;
      $scope.fd.taskId=$scope.taskId;
      $scope.fd.ParentDatasetDefId=$scope.fd.Related;
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.fd.formType=$scope.resourceDefinition.form_type;
      $scope.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.formDefinition = $scope.resourceDefinition.form_definition;
        
      if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
      $scope.fd.resourceRecordId=0;
      if ($scope.taskInitiateId==undefined)
      {
        $scope.taskInitiateId =0;
      
      }
      if ($scope.taskExecuteId==undefined)
      {
         $scope.taskExecuteId =0;  
      }
        if ($scope.prevTaskExeId==undefined)
        {
            $scope.prevTaskExeId =0;
        }
        
      //register widget 
      var out_fields = ['resourceId'];
      //if ($scope.fd.LocalId==undefined || $scope.fd.LocalId==null || $scope.fd.LocalId==0)
      //{
         var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'viewResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.fd.selectedResource, output:out_fields, };
         $scope.fd.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
         $scope.fd.LocalId=$scope.fd.localWidgetId;
         $scope.fd.LocalId=$scope.fd.localWidgetId;
         //$scope.fd.LocalId=UtilityService.uuidNow();
         
      //}
      $scope.ksm = KatalyoStateManager;
      
      let update_presentation_id = function(data)
      {
         if (data[$scope.fd.LocalId]>0 && ($scope.fd.PresentationId==null || $scope.fd.PresentationId==0)) $scope.fd.PresentationId = data[$scope.fd.LocalId];
          //unsubscribe here later after implementing unsubscribe - ToDo
          
      }
      if ($scope.fd.PresentationId==0 || $scope.fd.PresentationId==null)
      {
         
         let dependent_field = $scope.fd.LocalId;
         let subscribedSubject = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"formSaved",update_presentation_id);
  
      }        
      
       let apply_value = function(data)
        {
            let do_refresh=false;
            if (data!=undefined)
            {
               if (data != $scope.fd.resourceRecordId)
               {
                  if (typeof data ==='number') $scope.fd.resourceRecordId= data;
                  else if (Array.isArray(data))
                  {
                     if (data.length>0)
                     {
                      $scope.fd.resourceRecordId= data[0].id  
                     }
                  }else
                  {
                     $scope.fd.resourceRecordId = data.id;
                  }
                  do_refresh = true;
               }
            }
            //get selectedRecord
            if ($scope.fd.resourceRecordId == null) $scope.fd.resourceRecordId=0;
            
           // if ($scope.fd.resourceRecordId>0 && do_refresh) $scope.fd.getFormResourceWithData();
            
            
        };
        
      
      if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
      
      if ($scope.fd.Parameters.RefreshOnIdChange)
      {
         
         //find 
         //subscribe observer
           
         let dependent_field = "Field"+$scope.fd.PresentationId;
         let subscribedSubject = "resourceidValueStateName"+$scope.fd.Parameters.ResourceIdItemId+$scope.resourceDefinition.transaction_id;
         let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
         let val = $scope.ksm.getState(subscribedSubject);
         $scope.fd.resourceRecordId= val.id;
            
            //get selectedRecord
         if ($scope.fd.resourceRecordId == null) $scope.fd.resourceRecordId=0;
            
         //if ($scope.fd.resourceRecordId>0) $scope.fd.getFormResourceWithData();
      }
      
      //subscribe observer
      if ($scope.fd.PopulateType.toString().substr(0,1)=="4")
      {
         
        if ($scope.fd.SrcWidgetId!=undefined && $scope.fd.SrcWidgetId!=null)
        
        {
           
                let dependent_field = "Field"+$scope.fd.SrcWidgetId;
               // let subscribedSubject = $scope.fd.Parameters.SrcWidgetType+"ValueStateName"+$scope.fd.SrcWidgetId;
                let subscribedSubject = "rlistValueStateName"+$scope.fd.SrcWidgetId+$scope.resourceDefinition.transaction_id;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            
        }  
         
      }
      
      let refresh_widget_data = function()
      
      {
        // $scope.fd.getFormResourceWithData();    
         
      }
      if ($scope.fd.Parameters.EventName!=undefined)
      {
         if ($scope.fd.Parameters.EventName!="")
         {
            let dependent_field2 = "Field"+$scope.fd.PresentationId;
            let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
            let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,refresh_widget_data);
         }
      }
      
      $scope.fd.showProperties=function(datasetMapperType)
      {
            
            if ($scope.fd.datasetMapperType==undefined)
            {
               $scope.fd.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.fd.datasetMapperType==0 || $scope.fd.datasetMapperType!=datasetMapperType) $scope.fd.datasetMapperType = datasetMapperType;
               else $scope.fd.datasetMapperType=0;
            }
            
      };
           
      $scope.fd.setField = function(form,name,value,msg)
      {
         TaskDataService.setField(form,name,value);
         if (msg) MessagingService.addMessage(msg,'info');
      };  
      
      $scope.fd.getResourceAll = function () {
          
            ResourcesDataService.getResourceDefAll($scope.fd.Related).then(function(data) {
                               
                             // MessagingService.addMessage(data.msg,'success');
                              $scope.fd.resource = data.data;
                              $scope.fd.resourceLoaded = true;
                              if ( $scope.fd.Parameters.AutoLoad) $scope.fd.readBlockchainVars();
                              
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };
              
         $scope.fd.readBlockchainVars = function ()
        {
            var lang=$rootScope.globals.currentUser.lang;
              //let provider=$rootScope.globals.currentUser.web3Provider;
              //$rootScope.globals.currentUser.web3Infura = new Web3(new Web3.providers.WebsocketProvider('wss://ropsten.infura.io/ws/v3/d5619a41905a41f88c5e742e4180ef3c'))
              $rootScope.globals.currentUser.web3Infura = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/v3/d5619a41905a41f88c5e742e4180ef3c'))
              
              ResourcesDataService.readBlockchainVars($scope.fd.resource.Parameters.ContractId,$scope.fd.resource.Parameters.ContractAbi).then(function(data) {
                  
                  $scope.fd.formData[0]=data.data.vars;
                  $scope.fd.getFormResourceOriginal( $scope.fd.resource.Parameters.PublicVarsDatasetId.id);

               },function(error){
                                   MessagingService.addMessage(error.msg,'error');   
         
               });
        }
      $scope.fd.getFormResourceOriginal = function(datasetId){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null)
              {
                $scope.fd.PresentationId = 0;
                
              }
              $scope.fd.formLoaded=false;
              ResourcesDataService.getResourceForm(datasetId,"i",lang).then(function(data) {
                            
                                $scope.fd.resourceWidgetDefinition= data.data.resourceDefinition;
                                if (Array.isArray(data.data.widgets)) $scope.fd.dashboard.widgets.layout =  data.data.widgets;
                                
                                if ($scope.fd.selectedResourceItem!=undefined)
                                 { 
                                  $scope.fd.selectedResourceItem.name=$scope.fd.resourceWidgetDefinition.name;
                                  $scope.fd.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.fd.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.fd.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.fd.selectedResourceItem.src_type_text=$scope.fd.src_type_text[$scope.fd.PopulateType];
                                  $scope.fd.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.fd.selectedResourceItem={'name':$scope.fd.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.fd.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                                $scope.fd.wItemValue=data.data.widgets;
                                
                           /*
                                var container = $element.children().eq(0);
                                GridsterService.setupDragular($scope.fd.wItemValue,container,'rows-'+$scope.fd.PresentationId,'row-'+$scope.fd.gridsterType+'-hadle',true);
                             */   
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.ResourceRecordId}]}]});
						
                                  $scope.fd.searchFormDownloaded = true;
                                  //$timeout(function(){$scope.fd.formLoaded=true});
                                  $scope.fd.formLoaded=true;
                                
                               // $scope.fd.setField($scope.fd.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.fd.setField($scope.fd.dashboard.widgets.layout,'Required',false);
                                //$scope.fd.readBlockchainVars();
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      $scope.fd.gridOptions = {
         
         showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: false,
      //  enableHorizontalScrollbar : 1,
       // enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: false,
        enableColumnResizing: true,
        enableColumnReordering: true,
        //useExternalPagination: false,
        paginationPageSizes: [10, 25, 50,100,200,500,1000,5000,10000],
      }
       
        $scope.fd.getFormResource = function(){
              var lang=$rootScope.globals.currentUser.lang;
               $scope.fd.formLoaded=false;
               if ($scope.fd.PresentationId==null)
               {
                    $scope.fd.PresentationId=0;
            
               }
               $scope.fd.formLoading=true;
                         
              ResourcesDataService.getResourceFormExtended($scope.fd.resource.Parameters,$scope.taskId,$scope.fd.formType,$scope.fd.PresentationId,"w",lang).then(function(data) {
                               
                                  $scope.fd.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.fd.dashboard.widgets.layout =  data.data.widgets;
                                  
                                 if ($scope.fd.selectedResourceItem!=undefined)
                                 { 
                                  $scope.fd.selectedResourceItem.name=$scope.fd.resourceWidgetDefinition.name;
                                  $scope.fd.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.fd.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.fd.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.fd.selectedResourceItem.src_type_text=$scope.fd.src_type_text[$scope.fd.PopulateType];
                                  $scope.fd.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.fd.selectedResourceItem={'name':$scope.fd.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.fd.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                              
                                $scope.fd.wItemValue=data.data.widgets;
                               
                                
                                  
                                $scope.fd.searchFormDownloaded = true;
                                
                                $timeout(function(){    
                                   $scope.fd.formLoaded=true;
                                   $scope.fd.formLoading=false;
                                });
                                  
                               
                              //  $scope.fd.setField($scope.fd.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.fd.setField($scope.fd.dashboard.widgets.layout,'Required',false);
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 $scope.fd.formLoading=false;
                                 
                                
                             });
                              
      };
      
              
       
           $scope.fd.setSelectedDataset = function(id)  {
              
              if ($scope.fd.selectedResourceItem==undefined) $scope.fd.selectedResourceItem={};
              var pos = $scope.fd.resourceList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.fd.selectedResourceItem.id = $scope.fd.resourceList[pos].id;
                $scope.fd.selectedResourceItem.name = $scope.fd.resourceList[pos].name;
                $scope.fd.selectedDataset = $scope.fd.resourceList[pos];
                $scope.fd.selectedResourceItem.name_confirmed = $scope.fd.selectedResourceItem.name +' ('+$scope.fd.selectedResourceItem.id +')'
                $scope.fd.selectedResourceItem_c=$scope.fd.selectedResourceItem;
                }
					}
          
                  
          // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
	$scope.fd.getResources = function()  {
						
            if ($scope.fd.resourceList==undefined) $scope.fd.resourceList=[];
          
            var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'smartcontract').then(
						function(data) {
										
											  $scope.fd.resourceList = data.data;
                       if ($scope.fd.Related!=null)
                       {
                        $scope.fd.setSelectedDataset($scope.fd.Related);
                
                        }     
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					}
   

$scope.fd.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData;
      var pTaskDataRet = WidgetDataExchangeService.getPrevTaskData($scope.prevTaskExeId);
      if (Array.isArray(pTaskDataRet) && pTaskDataRet.length>0) pTaskData = pTaskDataRet[0];
            var datasetId=0,datasetRecordId=0;
            if (pTaskData!=undefined)
            {
               if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
               {
               for (var i=0;i<pTaskData.DEI.length;i++)
               {
                 if (pTaskData.DEI[i].name==linkType)
                 {
                   if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                   {
                     for (var j=0;j<pTaskData.DEI[i].value.length;j++)
                     {
                      if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined) datasetId=pTaskData.DEI[i].value[j].value;
                      if (pTaskData.DEI[i].value[j].name=="resourceRecordId" && pTaskData.DEI[i].value[j].value!=undefined) datasetRecordId=pTaskData.DEI[i].value[j].value;
                      
                       
                     }
                        
                       
                   }
                     
                 }
                   
                 }
               }
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };


   
   
  $scope.fd.previousTaskData=$scope.fd.getPreviousTaskWidget("resourceLinkDatasetList");
 
          
  if ($scope.fd.Related!=null &&  !$scope.formDefinition && !$scope.fd.searchFormDownloaded)
  {
     $scope.fd.getResourceAll();
    
  }
  else if ($scope.fd.Related!=null)
  {
     //$scope.fd.getFormResource();
     $scope.fd.getResources();
     $scope.fd.getResourceAll();
     //$scope.fd.setSelectedDataset($scope.fd.Related);
    
  } 

        $scope.fd.processWidgetDependencies = function() { //-----                
           if ($scope.fd.tItemValue!=undefined) 
            for (var i=0;i< $scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=$scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = $scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<$scope.fd.tItemValue[i].DEI.length;j++) {
                        if (($scope.fd.tItemValue[i].DEI[j].name==linkedItem) && ($scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = $scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("view "+$scope.fd.PresentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             $scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj $scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                              //  console.log("view "+ $scope.fd.PresentationId+" dobio event " + event.name);
                                for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    $scope.fd.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    $scope.fd.resourceRecordId = $scope.fd.linkedResourceId;
                                  }
                                  if (data[k].name=='resourceId') $scope.fd.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                }
                          
                              //get selectedRecord
                              //$scope.fd.getFormResourceWithData();
                                
                                    //	data: {resource_def_id:$scope.fd.linkedResourceDefId,resource_id:$scope.fd.linkedResourceId}
                                    
                              }) //on_END
                        }//if
                    }//for j
                }//if

             // else {  $scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget

          if ($scope.fd.PresentationId==undefined || $scope.fd.PresentationId==null) $scope.fd.PresentationId=0;
          
          $scope.fd.formType=$scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies($scope.taskId,$scope.fd.PresentationId,$scope.fd.formType).then(function(data) {
                               
                                $scope.fd.tItemValue=data.data;
                                $scope.fd.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
                              
    
            let template;
            //if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskViewResource.html');
            //else template = $templateCache.get('ktdirectivetemplates/ktTaskViewResourceExe.html');
            template = $templateCache.get('ktdirectivetemplates/ktReadSmartContract.html');
            $element.append($compile(template)($scope)); 

   
  
  
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

            }     
  }
  
};