'use strict';

export function ktCodesDefActions () {
        
           var controller = ['$scope', '$stateParams','$state','CodesService','MessagingService','TaskDataService','$rootScope','$timeout','dragulaService','GridsterService',
                             function ($scope,$stateParams,$state,CodesService,MessagingService,TaskDataService,$rootScope,$timeout,dragulaService,GridsterService) {
          
        
        $scope.firstTime=true;
                 
        CodesService.CodesList().then(function(data) {
            
                            $scope.codesList = data.data;
                  
                      },function(error){
                        
                          MessagingService.addMessage(error.msg,'error');
               });

          $scope.codesActions.codesDownloaded=false;
          
          $scope.GetCodesByName = function()
          {
                     var lang=$rootScope.globals.currentUser.lang;
                    // $scope.loadingActions=true; 
                     CodesService.getCodesByName($scope.defaultCode,lang).then(function(data) {
                             $scope.codes = data.data;
                      //      $scope.loadingActions=false;
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
       
          };
    
         
                $scope.gsrvc= GridsterService;
                
                $scope.setupDragula = function()
                {
                     var widgetHandleResource = 'element-drag-handle';
                     
                     var bag = dragulaService.find($rootScope, 'bag-actions');
                     if (!bag)
                     {
                        $scope.gsrvc.setupDragula('bag-actions','codeActionsDragula',widgetHandleResource);    
                     }
                     else
                     {
                          $scope.gsrvc.destroyDragula('bag-actions');
                          $scope.gsrvc.setupDragula('bag-actions','codeActionsDragula',widgetHandleResource); 
                           
                      }
                };  
                    
                 

                    $scope.fillTransitionCounters=function(codeActions) {
                        codeActions.transitionList={};
                        var transition='';
                        
                        for (var i=0;i<codeActions.codesActionsList.length;i++)
                        {
                            transition=codeActions.codesActionsList[i].value1+codeActions.codesActionsList[i].value2;
                            if (codeActions.transitionList[transition] === undefined )
                            {
                                 codeActions.transitionList[transition]=1; 
                            }
                                 else codeActions.transitionList[transition]++;
                        }
                            

                    };
                   
                  $scope.removeCodeItem = function(pindex){
                    //scope.task.TaskOutcomeCodeId = null;
                     $scope.codesActions.codesActionsList.splice(pindex,1);
                      };
                      
       
                  $scope.removeAction = function(pindex,index){
                    //scope.task.TaskOutcomeCodeId = null;
                     $scope.codesActions.codesActionsList[pindex].actions.splice(index,1);
                      };
                      
                  $scope.GetCodesClick = function(){
                    //scope.task.TaskOutcomeCodeId = null;
                    scope.GetCodes();
                      };
                  $scope.GetCodesByNameClick = function(){
                    //scope.task.TaskOutcomeCodeId = null;
                    scope.GetCodesByName();
                      };
                                    
                  $scope.AddAction = function(){
                    
                      var found=false;
                      //dodati kontrole da se ne može dodati dupli
                      for (var i=0;i<$scope.codesActions.codesActionsList.length;i++)
                      {
                        if ($scope.codesActions.codesActionsList[i].id1==$scope.statusFrom.id && $scope.codesActions.codesActionsList[i].id2==$scope.statusTo.id) found=true;
                        
                      }
                      if (!found)
                      {
                        
                        $scope.codesActions.codesActionsList.push({id1:$scope.statusFrom.id,name1:$scope.statusFrom.name,value1:$scope.statusFrom.name,id2:$scope.statusTo.id,name2:$scope.statusTo.name,value2:$scope.statusTo.name,actions:[]});        
                        $scope.codesActions.selectedActionIndex=$scope.codesActions.codesActionsList.length-1;
                 
                      }
                    
                  };
                      
                $scope.GetCodes = function(){
                   // var lang=$rootScope.globals.currentUser.lang;
                  $scope.loadingActions=true; 
                    var lang=$rootScope.globals.currentUser.lang;
                    //$scope.codesActions.codesActionsList.length = 0;
                   
                      if (!$scope.codesActions.codesDownloaded)
                      {                      
                         TaskDataService.GetTaskCodesActionsByName($scope.taskId,lang,$scope.defaultCode).then(function(data) {            
                                    
                                   $scope.codesActions.codesActionsList = data.data;
                                   $scope.codesActions.codesDownloaded=true;
                                   $scope.firstTime=false;
                                   $scope.fillTransitionCounters($scope.codesActions);  //MIGOR
                                   $scope.findSelectedTransition();
                                   $scope.loadingActions=false;
                                   
                              },function(error){
                                
                                  $scope.loadingActions=false;
                                  MessagingService.addMessage(error.msg,'error');
                            });
                      
                      }else
                      {
                       $scope.findSelectedTransition();
                       $scope.loadingActions=false;      
                      }
                      
                     
                };
              
                
                
                 //find selected transition
                  $scope.findSelectedTransition= function(){
                     var found=false;
                      for (var i=0;i<$scope.codesActions.codesActionsList.length;i++)
                      {
                            if ($scope.codesActions.codesActionsList[i].value1 == $scope.statusFrom.name && $scope.codesActions.codesActionsList[i].value2 == $scope.statusTo.name && !found)
                        {
                                    //  $scope.codesActions.codesActionsList.push(angular.copy($scope.codesActions.codesActionsListOriginal[i]));
                                   $scope.codesActions.selectedActionIndex=i;
                                   found=true;
         
                        }  
                      }
                      if (!found) $scope.AddAction();
                      $scope.setupDragula();
                  };
                $scope.$watch('codesActions.selectedTransition', function(newValue,oldValue){
                  
                  
                  if (newValue!=oldValue || $scope.firstTime)
                  {
                    
                   if (!!$scope.defaultCode)
                   {
                       $scope.GetCodesByName();
                   }
               
                    $scope.GetCodes();
                  
                  
                  // $timeout(function(){$scope.loadingActions.loading=false;});
                  }
                  /*else{
                    $scope.loadingActions.loading=false;
                    
                  }*/
                 
                });
                
                /*               
               if (!!$scope.defaultCode)
               {
                       $scope.GetCodesByName();
               }
               else{  
                   $scope.GetCodes();
               }
                */
                
          }];
          
          
        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktCodesDefActions.html',
              scope:{
                codes: '=',
                selectCode: '=',
                defaultCode: "@",
                task: "=",
                taskId: "=?",
                actionsList: "@",
                codesActions: "=",
                statusFrom: "=",
                statusTo: "=",
                },
                controller:controller,
             link: function (scope, elem, attrs) {
              
               
            }
        }
        
};