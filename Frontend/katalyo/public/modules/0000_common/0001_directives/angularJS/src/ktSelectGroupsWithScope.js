'use strict';

export function ktSelectGroupsWithScope () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktSelectGroupsWithScope.html',
              scope:{
                selectedItems:'=',
                size:'=?',
                selectId:'=',
             },
             controller: function($scope,UserService){
                 $scope.selected={groups:$scope.selectedItems};
         
         
           $scope.$watch('selected.groups',function(newValue,oldValue){
              
              if (newValue!=oldValue && newValue!==undefined)
              {
               $scope.selectedItems=newValue;
              }
            });
          
           
             $scope.groupList=[];
        //if ($scope.selectedItems==undefined) $scope.selectedItems=[]

        $scope.getGroups = function () {
          UserService.getUsersGroups('groups').then(function(data){
          $scope.groupList = data.data;
          $scope.groupsDownloaded = true;
				});

        }
				$scope.getGroups();
                },
             link: function (scope, elem, attrs) {
            
          
       
        
        
      
    

            }

    }
};