'use strict';

export function ktTaskContainer () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktTaskContainer.html',
             /*scope: {
                inputParameters: '=',
                //selectedItem: '=',
                inputArray: '=',
            },*/
             controller: function($scope,$rootScope,$timeout,UserService,ResourcesDataService,TaskDataService,MessagingService,GridsterService) {
                
                 $scope.gsrvc= GridsterService;
                 if (!Array.isArray($scope.fd.selectedTasks)) $scope.fd.selectedTasks=[];
                if ($scope.resourceDefinition!=undefined)
                {
                  $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
                }
                if ($scope.localVars == undefined) $scope.localVars={};
                $scope.localObj={loadedTasks : []};
                $scope.localVars.taskItem = null;
                $scope.localVars.selectedTask = {};
                $scope.localVars.showMenu = false
      
                $scope.localVars.toggleDropdown = function(){
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
                }
                
                               
                $scope.$on('InlineTaskClosed',function(event,data){
                    //var index=data;
                    //$scope.localObj.loadedTasks.splice(index,1);
                    var taskId=data;
                    var pos = $scope.localObj.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (pos>=0)
                    {
                      $scope.localObj.loadedTasks.splice(pos,1);  
                    }
                });
               
                if ($scope.fd.selectedTasks===undefined || $scope.fd.selectedTasks===null) $scope.fd.selectedTasks = [];
                
                if ($scope.fd.FormDef!=null && $scope.fd.FormDef!==undefined) {
                 $scope.fd.selectedTasks = $scope.fd.FormDef;
                }else
                {
                $scope.fd.FormDef = [];
                
                } 
                
                let initiate_task = function(data)
                {
                    let loadTask=false;
                    $scope.localVars.taskLoaded = false;
                    if ($scope.localObj.loadedTasks.length>0)
                    
                    {
                        if ($scope.localObj.loadedTasks[0].taskId.id != data.taskId)
                        {
                            $scope.localObj.loadedTasks.length=0;
                            loadTask=true;
                        }
                    }else loadTask=true;
                    
                    
                  //  if (loadTask)
                    //{
                        $scope.localObj.loadedTasks.splice(0,1);
                         
                        for (let j = 0;j<$scope.fd.selectedTasks.length;j++)
                        {
                        
                            if ($scope.fd.selectedTasks[j].taskId.id==data.taskId)
                            {
                            
                                $timeout(function(){
                                $scope.localObj.loadedTasks.push(data);
                                $scope.localVars.taskLoaded = true;
                                });
                            }
                        }
                    //}
                }
                
                for (let i = 0;i<$scope.fd.selectedTasks.length;i++)
                {
                    let subscribedSubject = 'initiateTaskStateName'+$scope.fd.selectedTasks[i].widgetId;
                    let dependent_field = 'taskContainer'+$scope.fd.UuidRef+$scope.resourceDefinition.transaction_id;
                    $scope.ksm.subscribe(subscribedSubject,dependent_field,"taskInitiated",initiate_task);  
                
                }
                
                /*
                  $scope.localVars.LoadTask=function(taskId,outcomeId,previousTask,openTaskType)
                  {
                    
                   
                        $scope.localObj.loadedTasks.splice(0,0,{'taskId':taskId,'outcomeId':outcomeId,'previousTask':previousTask,'localVars':localVars});
                        
                    
                    
                  }
                  
                 */
                let lang=$rootScope.globals.currentUser.lang;    
                ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
                                                    
                                $scope.fd.taskList = data.data;
                                $scope.fd.tasksLoaded=true;
                                
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });

                   $scope.localVars.taskIsOpen = function(taskId)
                  {
                    var isOpenPos = $scope.localObj.loadedTasks.map(function(e) { return e.taskId; }).indexOf(taskId);
   
                    if (isOpenPos>=0) return true;
                    else return false;
                  }
                 
                   $scope.fd.pasteFromClipboard = function(task){
                        
                        navigator.clipboard.readText().then( (data) => {
                            /* clipboard successfully read */
                            $timeout(function(){task.widgetId = data})
                        },
                        () => {
                            /* clipboard read failed */
                            MessagingService.addMessage('Paste from clipboard failed','error');
                        });
                       
                }

                  
                $scope.fd.AddTaskToList = function (){
  
                    $scope.fd.selectedTasks.push({widgetId:null,taskId:null});
     
                }
                  
                   $scope.fd.ConfirmTaskList = function (){
                     $scope.fd.FormDef = $scope.fd.selectedTasks;
                     $scope.fd.showConfirmed=true;
                     $timeout(function(){$scope.fd.showConfirmed=false},5000);
                   }
                   $scope.fd.RemoveTaskFromList = function (index){
                 
                  
                   if (index>=0){
             
                             $scope.fd.selectedTasks.splice(index,1);
                                  
                   }
                            
                    
                 }
          
          
                 $scope.localVars.saveTaskToResource = function(tdid,tei,datasetDefId,datasetRecordId){
                          TaskDataService.saveTaskToResource(tei,datasetDefId,datasetRecordId,$scope.fd.presentationId).then(function(data) {
                                                    
                              $state.go('app.menu.tasks.initiate',{id: tdid,fromStateName:$state.current.name,fromStateParams:$state.params,outcome_id:0,prev_task_execute_id:tei});
                                                             
                                
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
             }
             
                
                },
             link: function (scope, elem, attrs) {
                
               
        }       
              
    }
  
};