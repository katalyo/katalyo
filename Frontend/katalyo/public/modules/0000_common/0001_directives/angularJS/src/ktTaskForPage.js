'use strict';

export function ktTaskForPage () {
        var controller = ['$scope', '$stateParams','$state','$rootScope','GridsterService','WidgetDataExchangeService','ResourcesDataService','KatalyoStateManager','$templateCache','$compile','$element','TaskDefinitionService',
                               function ($scope,$stateParams,$state,$rootScope,GridsterService,WidgetDataExchangeService,ResourcesDataService,KatalyoStateManager,$templateCache,$compile,$element,TaskDefinitionService) {

        $scope.CTRL="ktTaskForPage"
        let lang = $rootScope.globals.currentUser.lang
        if ($scope.fd==undefined) $scope.fd={};
        if ($scope.fd.localVars==undefined) $scope.fd.localVars={};             
        if ($scope.resourceDefinition==undefined) $scope.resourceDefinition={};            
        if ($scope.fd.localVars.previousTask==undefined) $scope.fd.localVars.previousTask = 0;
        if ($scope.fd.localVars.outcomeId==undefined) $scope.fd.localVars.outcomeId = 0;
        $scope.fd.localVars.loadTaskDirect = true;
        $scope.fd.localVars.taskDefinition = {};
        $scope.fd.localVars.taskDefinition.page_state_id = $scope.resourceDefinition.page_state_id
        $scope.fd.localVars.taskId = $scope.fd.Parameters?.taskDefId?.id
        $scope.ksm = KatalyoStateManager
        $scope.tds = TaskDefinitionService
        $scope.gsrvc=GridsterService
        
        let widgetDefinition = {name: 'taskForPage',widgetId: $scope.fd.LocalId,taskId:  $scope.resourceDefinition.id };
        $scope.fd.LocalId=WidgetDataExchangeService.registerWidget(widgetDefinition);
        
        let dependent_field_pid = $scope.fd.LocalId;
        let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
        let dependent_field_pid2
             
        let update_presentation_id2 = function(data) {
            let presentation_id = angular.copy($scope.fd.PresentationId)
            if (data[presentation_id]!==undefined) {
                    $scope.fd.PresentationId = data[presentation_id];
                //unsubscribe here later after implementing unsubscribe - ToDo
                dependent_field_pid2 = $scope.fd.PresentationId
                let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
                }
        }
        
        let update_presentation_id = function(data) {
            if (data[$scope.fd.LocalId]>0 && $scope.fd.PresentationId==0 && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            //unsubscribe here later after implementing unsubscribe - ToDo
        }
        
                

        $scope.fd.localVars.toggleDropdown = function(){
                $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
        }

        $scope.fd.localVars.showProperties=function(datasetMapperType,showPropertiesRight) {
                    
             if ($scope.fd.localVars.datasetMapperType==undefined) {
                $scope.fd.localVars.datasetMapperType = datasetMapperType;
             }else  {
                if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                     $scope.fd.localVars.datasetMapperType = datasetMapperType;
                else 
                    $scope.fd.localVars.datasetMapperType=0;
             }
             if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);            
         }  


        // **************  EXECUTE ON INIT ****************
        
        if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0) {
            let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
        } else {
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
        }

 
    
         let template;
         template = $templateCache.get('ktdirectivetemplates/ktTaskForPage.html');
             
          
         $element.append($compile(template)($scope));

       }];
          
          
        return {
            restrict: 'A',
            controller:controller,
             link: function (scope, elem, attrs) {
                 

              }
        }             
};