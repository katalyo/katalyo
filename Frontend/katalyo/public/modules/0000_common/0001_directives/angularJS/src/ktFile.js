'use strict';

export function ktFile () {
        return {
            restrict: 'A', 
			controller: function ($scope, $rootScope,$element,ResourcesDataService,ProcessingService,$timeout,MessagingService,CODES_CONST,GridsterService,WidgetDataExchangeService,KatalyoStateManager,$q) {
	
        
        $scope.gsrvc = GridsterService;
        $scope.org_id = $rootScope.globals.currentUser.organisation.id;
        if ($scope.fd.localVars==undefined) $scope.fd.localVars={};        
        if ($scope.formData == undefined) $scope.formData=[{}];
        if ($scope.formData.length == 0) $scope.formData.push({});

        $scope.fd.localVars = {dashboard:{widgets:[]}}
        let newValueSingle,newValueMultiple;
        $scope.ctrl = this;
        
        $scope.fd.localVars.TablePrefix = 'resource'+$scope.fd.Related+'_'+$rootScope.globals.currentUser.organisation.id


        $scope.CTRLNAME='ktFile'

        if ($scope.fd.Parameters===undefined || $scope.fd.Parameters===null) $scope.fd.Parameters={}
        if ($scope.fd.Parameters.ResourceRelationshipType===undefined) $scope.fd.Parameters.ResourceRelationshipType='new'

        if ($scope.fd.MaxLength===undefined || $scope.fd.MaxLength===null) $scope.fd.MaxLength=1

        $scope.fd.localVars.formParams = {formViewType:1,showGridNavigation:true}

        if ($scope.fd.PresentationType==1)
        {
                $scope.setDropdownValue=function(theSelectItemFn)
                {
                        $scope.setItemValueDropdown = theSelectItemFn;
                
                }
                 $scope.setLoadDropdown = function(directiveFn) {
                        $scope.loadDropdown = directiveFn;
            
                };
                $scope.setClearDropdown = function(directiveFn) {
                        $scope.clearDropdown = directiveFn;
                }
        }
          $scope.fd.localVars.paginationOptions = {
                    pageNumber: 1,
                    pageSize: 10,
                    sort: null
                };
              
       
        var widgetDefinition = {DEI: [{'name':'resourceLinkFile','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'datasetListWidget',widgetId: $scope.fd.PresentationId,taskId:  $scope.lTaskId,
                                resourceDefId : $scope.fd.Related, //resourceDefId : $scope.selectedResource
                                resourceRecordId:$scope.fd.localVars.resourceRecordId,task_initiate_id:$scope.resourceDefinition.task_initiate_id,task_execute_id:$scope.resourceDefinition.task_execute_id, output:null };
        $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
        
        $scope.ksm = KatalyoStateManager;
        
        $scope.populateValue = function(newValue)
        {
                if ($scope.fd.MaxLength!=1)
                {
                 newValueMultiple = newValue;
                 newValueSingle="";
                }
                else
                {
                 newValueMultiple = [];
                 newValueSingle=newValue;
                }
               
                $scope.itemValue.val = newValue;
                if ($scope.fd.PresentationType==1 && $scope.setItemValueDropdown!=undefined) $scope.setItemValueDropdown(null,$scope.itemValue.val);
                $scope.formData[0][$scope.fd.FieldName] = $scope.itemValue.val;
                $scope.fd.Search.value = $scope.formData[0][$scope.fd.FieldName];
                $scope.ksm.executeAction(valueStateName,"valueChanged", newValue);
                if (newValue && $scope.setItemValueDropdown!=undefined) $scope.setItemValueDropdown(null,newValue);
                else if ($scope.clearDropdown!=undefined) $scope.clearDropdown();
                WidgetDataExchangeService.setData({
                    DEI: [{name:'resourceLinkFile',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:newValueSingle}
                        ,{name:'resourceRecordIds',value:newValueMultiple},{name:'resourceRecordsAll',value:$scope.fd.datasetItems}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});   
                
        }

     $scope.fd.localVars.filesData = []
    
    /**********************************************************************/
    /***************** Get files for smart table ktFile *******************/
    /**********************************************************************/
    $scope.fd.localVars.getFiles= function(tableState, pageNumber,pageSize){


        //taskExecuteId: 0   taskInitiateId: 0
        let deferred = $q.defer();    

        $timeout(function(){

            $scope.resourceLoading=true;
            var lang=$rootScope.globals.currentUser.lang;
            var newPage
           
            
            if (pageNumber==undefined) pageNumber = $scope.fd.localVars.paginationOptions.pageNumber;
            if (pageSize==undefined) pageSize = $scope.fd.localVars.paginationOptions.pageSize;
            
            if ($scope.fd.localVars.filesData!==undefined) {
                    $scope.fd.localVars.totalCount= $scope.fd.localVars.filesData?.length
                    $scope.fd.localVars.paginationOptions.totalItemCount=$scope.fd.localVars.filesData?.length
                    $scope.fd.localVars.paginationOptions.numberOfPages = Math.ceil($scope.fd.localVars.paginationOptions.totalItemCount/pageSize);
            }

            if ($scope.fd.localVars.totalCount>0){
                if (Math.ceil($scope.fd.localVars.totalCount/pageSize)<pageNumber)
                    newPage = Math.ceil($scope.fd.localVars.totalCount/pageSize);
            }
            
             if ($scope.fd.localVars.filesData) 
                    $scope.fd.localVars.results = {results : $scope.fd.localVars.filesData.slice( (pageNumber -1) * pageSize, pageNumber * pageSize) }
             else 
                    $scope.fd.localVars.results = {}     
             $scope.fd.localVars.results.columns=$scope.fd.localVars.filesColumns

             $scope.fd.localVars.currentPage=pageNumber;
             $scope.fd.localVars.results.pageSize = pageSize;
             $scope.fd.localVars.results.numberOfPages = $scope.fd.localVars.paginationOptions.numberOfPages;
             $scope.fd.localVars.results.count =   $scope.fd.localVars.totalCount
      
             
             deferred.resolve({data: $scope.fd.localVars.results});
         })

      return deferred.promise;
    
    }
         /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
       
        if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
        $scope.itemValue = {val:null};
        
        let valueState= $scope.ksm.addSubject($scope.fd.ElementType+'ValueStateName'+$scope.fd.ItemId+$scope.resourceDefinition.transaction_id,$scope.itemValue.val);
        let valueStateName = valueState.subject.name;
        
        let value_changed = function(data)
        {
            valueState.subject.setStateChanges = data;
            valueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
        
        let apply_value = function(data)
        {
            let local_data;
            let state=data.state        
            if ($scope.itemValue.val!=state)
            {
                local_data = state;
                if ($scope.fd.Parameters.DependentFieldFilter!=undefined)
                {
                        if ($scope.fd.Parameters.DependentFieldFilter!="" || $scope.fd.Parameters.DependentFieldElement!="")
                        {
                                if (Array.isArray(state))
                                {
                                        if (state.length>0) local_data = state[0];
                                        else local_data = null;
                                        
                                }
                                else
                                {
                                     local_data = state;
                                }
                                if ($scope.fd.datasetItems==undefined)
                                {
                                        $scope.fd.refreshDatasetDataList("",true,local_data);
                                        local_data=null;
                                }
                                else
                                {
                                        let element1,element2;
                                        if ($scope.fd.Parameters.DependentFieldFilter!="")
                                        {
                                                element1 = $scope.fd.Parameters.DependentFieldFilter;
                                                element2 = "id";
                                        }
                                        else
                                        {
                                                element1 = "id";
                                                element2 = $scope.fd.Parameters.DependentFieldElement;
                                        }
                                        let pos = $scope.fd.datasetItems.map(function(e) { return e[element1]; }).indexOf(local_data[element2]);

                                        if (pos>=0) local_data = $scope.fd.datasetItems[pos];
                                        else local_data = null;
                                }
                        }
                }
                $scope.populateValue(local_data);
            }
        }; 
       
        if ($scope.fd.Parameters.DependentFieldId!=undefined && $scope.fd.Parameters.DependentFieldId!=null)
        
        {
            if ($scope.fd.Parameters.DependentFieldId!="")
            {
                let dependent_field = $scope.fd.FieldName;
                let subscribedSubject = $scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId+$scope.resourceDefinition.transaction_id;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value,true);
                let val = $scope.ksm.getState(subscribedSubject);
                $scope.populateValue(val.state);
            }
        }
        

          //**********************************************************
         //           GET FORM FIELDS
         //**********************************************************      
            
          $scope.fd.getFormFields = function(){
              
              
              var lang=$rootScope.globals.currentUser.lang;
           
                         
              ResourcesDataService.getFormFields($scope.fd.Related,lang).then(function(data) {
                               
                                  $scope.fd.presentationElements= data.data;
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
                              
          };

          /************ Get selected records for dropdown **************/
        $scope.fd.getDatasetList = function()
        {
                if ($scope.formData[0][$scope.fd.FieldName+'l']!=undefined && $scope.formData[0][$scope.fd.FieldName+'l'].length>0 && $scope.fd.PresentationId!=null && $scope.fd.PresentationId!=undefined)
                {
                  ResourcesDataService.getResourceDataList($scope.fd.Related,$scope.fd.PresentationId,lang,{'id__in': $scope.formData[0][$scope.fd.FieldName+'l']}).then(function(data) {
                             
                             if ($scope.formData===undefined) $scope.formData = [{}];
                             if ($scope.fd.MaxLength==1) $scope.formData[0][$scope.fd.FieldName] = data.data[0];
                             else $scope.formData[0][$scope.fd.FieldName] = data.data;
                             $scope.populateValue($scope.formData[0][$scope.fd.FieldName]);
                             WidgetDataExchangeService.setSearch($scope.fd.PresentationId,$scope.taskExecuteId);
                             //$scope.ksm.executeAction(valueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                             //$scope.itemValue.val = $scope.formData[0][$scope.fd.FieldName];
                      },function(error){    
                      MessagingService.addMessage(error.msg,'error');
                  });
                }
        }
        
      
      
        $scope.$watch('itemValue.val',function(newValue,oldValue){
                if (newValue!=oldValue)
                {
                        $scope.populateValue(newValue);    
                
                }
        });
        
           
         $scope.fd.localVars.fileSelectionChanged = function(file){
                             
                                $scope.formData[0][$scope.fd.FieldName]=file;
                                
                                $scope.fd.localVars.fileSelected =file.isSelected
                                
                                $scope.fd.localVars.SelectedFile = file                               
                                // count selected
                                $scope.fd.localVars.resourceDataRecordSelected=$scope.formData[0][$scope.fd.FieldName].length; 
                                
                                WidgetDataExchangeService.setData({
                                DEI: [{name:'resourceLinkFile',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.SelectedFile.id},
                                {name:'resourceRecordIds',value:file},{name:'resourceRecordsAll',value:$scope.fd.localVars.filesData}]}],
                                widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});

        };

        $scope.search=[
                            {id:1,filter:'onlist',name:"On list:",icon:"fa fa-chevron-right"},
                            {id:2,filter:'notonlist',name:"Not on list:",icon:"fa fa-expand"},
                      ];
				
        if ($scope.fd!=undefined) {
                  if ($scope.fd.Search==undefined)
                  {
                    $scope.fd.Search=$scope.search[0]; //MIGOR TODO - greska kod incijacije za multiple , mozda treba kod promjene na multiple ovo jos jednom okinuti
                  }
        } 

			  $scope.applySearchFilters = function(index) {
                   
				  $scope.fd.Search=$scope.search[index];
                            $scope.fd.Search.value = [ $scope.formData[0][$scope.fd.FieldName] ];
                            $scope.fd.isOpen=false;		
			  };
       
       
        $scope.src_type_text=CODES_CONST._POPULATE_TYPE_  
      
        
        var lang = $rootScope.globals.currentUser.lang;
        
        $scope.fd.localVars.isResourceSelected = false;
 
				$scope.fd.localVars.selectedDataRecord=false;
 
				$scope.taskId=0;
       
        			   
					   
	$scope.fd.gridOptionsFile = {
                data:[],
                showGridFooter: false,
                showColumnFooter: false,
                enableRowSelection: true,
                multiSelect: false,
                enableHorizontalScrollbar : true, 
                enableVerticalScrollbar : true,
                enableRowHeaderSelection: false, //false
                showHeader:true,
                enableColumnResizing: true,
                autoResize:true,
                minRowsToShow: 4,
                displayselectionCheckbox:false,
                enableGridMenu: true,
        }
					   
        $scope.fd.RelationshipType = $scope.fd.MaxLength;
     
						
         
           $scope.fd.colLength=[];
           for (var i=0;i<24;i++) $scope.fd.colLength.push('col-'+(i+1));
          
        // $scope.fd.showSearchForm = false;
        
        if ($scope.resourceDefinition!=undefined)
        {
          $scope.taskId=$scope.resourceDefinition.ResourceDefId;
          $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
          $scope.formType=$scope.resourceDefinition.form_type;
          $scope.source_type=$scope.resourceDefinition.source_type;
          $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
          $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
          $scope.formDefinition = $scope.resourceDefinition.form_definition;
          
         
        
        }
        
         $scope.fd.gridsterType = "resource";
          
          $scope.fd.localVars.resourceDashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: [],
          data:{}
				} ;
        if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
        $scope.fd.resourceRecordId=0;
        if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        if ($scope.fd.PresentationType==null)
        {
              $scope.fd.PresentationType = 0;
        }
            
          $scope.fd.tItemValue = $scope.fd.FormDef;
          if (typeof $scope.fd.tItemValue=="string") $scope.fd.tItemValue=[]; 
          $scope.fd.resourcesList=[];
          $scope.fd.resourceListLength=0;
          $scope.fd.selectedFile = 0;
          //$scope.fd.selectedFileDropdown = 0;
          //if ($scope.fd.Related!=null) $scope.fd.selectedFile = $scope.fd.Related;
					
          
          
           $scope.setItemFn = function(directiveFn) {
            $scope.SetFileId = directiveFn;
            
        };
        
        $scope.fd.resourceDataRecordSelected=false;
				  
         //*********************************************************************************************
          //************************************ GET FILE LIST ***************************************
                    //*********************************************************************************************
       $scope.fd.localVars.getResources = function(search)  {
                        var lang=$rootScope.globals.currentUser.lang;
              ResourcesDataService.getResourceDefListByTypeFilter(lang,'file',search,$scope.fd.selectedFile).then(
            function(data) {
                                        
                $scope.fd.resourcesList = data.data;
                            
                            if (!Array.isArray($scope.fd.resourcesList))   $scope.fd.resourcesList=[];            
                        
                            if ($scope.fd.Related!=null)
                            {
                                   $scope.fd.resourceId = $scope.fd.Related;
                                   $scope.fd.resourceListLength = $scope.fd.resourcesList.length;
                                   //select dataset from dropdown
                                    var pos = $scope.fd.resourcesList.map(function(e) { return e.id; }).indexOf($scope.fd.resourceId);

                                   if (pos>=0){
                                       $scope.fd.selectedResource=$scope.fd.resourcesList[pos];
                                      
                                       $scope.fd.isResourceSelected=true;
                                       $scope.localVars.pickButtonLabel="Choose "+$scope.fd.selectedResource.name+" record...";

                                   }
                         
                            }
                            $scope.fd.RelatedLoaded=true;
                                             
                     },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                         });
       };     
          
      $scope.fd.localVars.downloadFile = function(fileId,fileName){
                ResourcesDataService.downloadFile($scope.fd.Related,fileId).then(function(data) {
                               
                                    MessagingService.addMessage('File '+fileName+' downloaded','info');
                                    var url = URL.createObjectURL(new Blob([data.data]));
                                    var a = document.createElement('a');
                                    a.href = url;
                                    a.download = fileName;
                                    a.target = '_blank';
                                    a.click();
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// dowload file END
      
      
       $scope.fd.localVars.previewFile = function(fileId,fileName,fileType){
           
            ResourcesDataService.downloadFile($scope.fd.Related,fileId).then(function(data) {
                               
                                    var url = URL.createObjectURL(new Blob([data.data],{type:fileType}));
                                    let new_doc = window.open(url,"_blank");
                                  
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// preview file END

           $scope.fd.getResourceDataRefresh = function (retValue) 
            {
              var lang = $rootScope.globals.currentUser.lang;
              //ToDo ovo prebaciti sve u jedan API poziv
               if ($scope.resourceRecordId>0 && $scope.fd.MaxLength==0)
              {
                ResourcesDataService.getResourceDataWithRelated($scope.parentResourceDefId,$scope.resourceId,$scope.resourceRecordId,$scope.taskId,$scope.fd.ItemId,lang).then(function(data) {
                            $scope.formData[0][$scope.fd.FieldName] = data; 
                            $scope.setupGrid(data);                
                },function(error){    
                    MessagingService.addMessage(error.msg,'error');
			 					});
                      
                }
            }
            


        /* delete selected child record*/
        $scope.fd.localVars.deleteChildRecord = function()
        {
              var lang = $rootScope.globals.currentUser.lang;
              if ($scope.resourceRecordId>0 && $scope.fd.MaxLength==0 && $scope.fd.localVars.fileSelected)
              {
               
              ResourcesDataService.DeleteChildRelationship($scope.fd.PresentationId,$scope.parentResourceDefId,$scope.resourceRecordId,$scope.fd.localVars.SelectedFile[$scope.fd.localVars.TablePrefix+'_id']).then(function(data) {
                      MessagingService.addMessage(data.data,'success');
                       $scope.fd.localVars.filesData.length = 0
                      $scope.fd.localVars.fileSelected = false
                      $scope.fd.localVars.GetResourceDataForParent()
                },function(error){    
                    MessagingService.addMessage(error.data,'error');
                                    });
            }       
        }

            $scope.linkedWidgetId = 0;
            
            if ($scope.fd.Search!=undefined) $scope.linkedWidgetId = $scope.fd.Search.WidgetId;
            
            var refresh_list_from_callback=function(data)
            {
                $scope.fd.refreshDatasetDataList("",true,$scope.lData);
                
            }
              
            $scope.fd.refreshDatasetDataList = function(search,apply,val)
            {
              var lang = $rootScope.globals.currentUser.lang;
              //ToDo ovo prebaciti sve u jedan API poziv
              $scope.lData = val;
               if ($scope.fd.PresentationId!=null && $scope.fd.PresentationId!=undefined)
                {
                        var ctrl = this;
                        this.l_apply = apply;
                        this.l_val = val;
                        let fromCache=true;
                   //ResourcesDataService.getResourceDataList($scope.fd.Related,$scope.fd.PresentationId,lang,search).then(function(data) {
                   ResourcesDataService.getResourceDataListAll($scope.fd.Related,$scope.fd.PresentationId,lang,fromCache,refresh_list_from_callback).then(function(data) {
                   
                           $scope.fd.datasetItems = data.data;
                           $timeout(function(){
                                //if (!$scope.fd.ReadOnly) $scope.loadDropdown($scope.fd.datasetItems);
                                if (ctrl.l_apply) apply_value(ctrl.l_val);
                           });
                    },function(error){    
                    MessagingService.addMessage(error.msg,'error');
									});
              
                }
            };
           
          
            $scope.fd.localVars.restoreGridState= function() {
            //debugger
            if ($scope.fd['GridState']!==undefined && $scope.fd['GridState']!==null)
            {
              
                if ($scope.fd['GridState']['columnsDef']!==undefined) $scope.fd.localVars.filesColumns =  $scope.fd['GridState']['columnsDef']
                if ($scope.fd['GridState']['columnsQuery']!==undefined) $scope.fd.columnsForQuery =  $scope.fd['GridState']['columnsQuery']

            }
        };              
      
          // Resource is selected
              $scope.fd.onSelectCallback = function (item, model){
                var eventResultId=item.id;
                $scope.fd.eventResultName=item.name;
                
                 $scope.fd.resourceId=eventResultId;
                 $scope.fd.isResourceSelected=true;
                    
                $scope.fd.Related=$scope.fd.resourceId;
                $scope.fd.localVars.getPublishedResources()
                if ($scope.fd.Related!=undefined && $scope.fd.Related!=null) $scope.fd.getFormFields();
                   
              }
              
            // Resource is selected
              $scope.fd.onSelectPublished = function (item, model){
                let pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(item.id);
                let old_publish_id = $scope.fd.Parameters.PublishedResourceId
                if (pos>=0){
                    $scope.fd.Parameters.PublishedResourceId = $scope.fd.localVars.publishedResourceList[pos].id;
                
                    $scope.fd.Parameters.PublishedResourceVersion = $scope.fd.localVars.publishedResourceList[pos].Version;
                    if ($scope.fd.GridState?.columnsDef===undefined || $scope.fd.Parameters.PublishedResourceId !=old_publish_id) $scope.fd.localVars.getColumnsDef()
                    else $scope.fd.localVars.fileListDownloaded =true
                }
                
                  
              }
         $scope.fd.localVars.getPublishedResources=function()
              {
               $scope.fd.localVars.publishedLoaded=false        
              ResourcesDataService.GetPublishedResourcesByTypeId($scope.fd.Related,'form').then(
                function (data) { 
                if ($scope.fd.localVars===undefined) $scope.fd.localVars={}      
                   $scope.fd.localVars.publishedResourceList = data.data;
                   $scope.fd.localVars.publishedLoaded=true

                  
                   },function(error){    
                                  MessagingService.addMessage(error.msg,'error');
                });
            }



        $scope.fd.localVars.ShowSearchForm = function(id) 
 
        {   let show_search_form
            if ($scope.fd.ShowSearchForm) show_search_form = false
            else show_search_form = true
            $scope.fd.localVars.getPublishedResourceById(id,show_search_form)
            
        }
        /*********************************************************************************************/
        /******************************* GET PUBLISHED DATASET DETAILS *******************************/
        /*********************************************************************************************/ 

        $scope.fd.localVars.getPublishedResourceById = function(id,show_search_form) 
        {
            if (show_search_form)  {
                let pos
                let old_publish_id = $scope.fd.Parameters.PublishedResourceId
                if ($scope.fd.FormDef!==undefined && $scope.fd.FormDef!==null && $scope.fd.FormDef.length>0) { 
                        pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                        $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                        $scope.fd.localVars.formLoaded=true;
                  } else 
                  {
                      pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                      $scope.fd.FormDef={}
                      if (pos>=0){
                        ResourcesDataService.GetPublishedResourceById(id).then(function (data) {
                                $scope.fd.FormDef = data.data.Form  
                                $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                                $scope.fd.localVars.formLoaded=true;
                            },function(error){    
                              MessagingService.addMessage(error.msg,'error');
                        });

                    }
                }
             
            }
        }    

        $scope.fd.localVars.getColumnsDef = function () {

                
                    ResourcesDataService. getResourceColumns($scope.fd.Parameters.PublishedResourceId,'file',lang,$scope.fd.Parameters.PublishedResourceVersion).then(function(data) {//MIGOR
                                    
                                          $scope.fd.localVars.filesColumns = data.data.columnsDef
                                          $scope.fd.localVars.results.columns =  $scope.fd.localVars.filesColumns
                                          $scope.fd.localVars.fileListDownloaded = true;
                                        
                                          $scope.fd.localVars.saveGridState(false)
                                         
                                             
                                          },function(error){
                                        MessagingService.addMessage(error.msg,'error');
                                     });

            }

         /*********************************************************************************************/
        /************************ GET RESOURCE LIST FOR EXISTING RELATIONSHIP *************************/
        /*********************************************************************************************/
       $scope.fd.localVars.getExistingResources = function()  {

             $scope.fd.resourcesList.length=0
             $scope.fd.RelatedLoaded=false
             
             let resource_def_id = $scope.resourceDefinition.ResourceDefId_id
             if (resource_def_id===undefined) resource_def_id = $scope.resourceDefinition.ResourceDefId
             
             if (resource_def_id===undefined) resource_def_id = $scope.resourceDefinition.ParentDatasetDefId

            if (resource_def_id>0)
            {
              ResourcesDataService.getExistingResourceRelationships(resource_def_id).then(function(data) {
                                        
                $scope.fd.resourcesList = data.data;
                            
                            if (!Array.isArray($scope.fd.resourcesList))   $scope.fd.resourcesList=[];            
                        
                            if ($scope.fd.Related!=null)
                            {
                                   $scope.fd.resourceId = $scope.fd.Related;
                                   $scope.fd.resourceListLength = $scope.fd.resourcesList.length;
                                   //select dataset from dropdown
                                    var pos = $scope.fd.resourcesList.map(function(e) { return e.id; }).indexOf($scope.fd.resourceId);

                                   if (pos>=0){
                                       $scope.fd.selectedResource=$scope.fd.resourcesList[pos];
                                      
                                       $scope.fd.isResourceSelected=true;
                                   }
                         
                            }

                            if ($scope.fd.Parameters.PublishedResourceId!=undefined) $scope.fd.localVars.getPublishedResources()
                            
                            $scope.fd.RelatedLoaded=true;
               
                     },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                         });
          }else  MessagingService.addMessage('Dataset id error (directive fn getExistingResources)','error');
       };     
          
          
          
                if ($scope.formDefinition)
                {
                 if ($scope.fd.Parameters.ResourceRelationshipType==='new' || $scope.fd.Parameters.ResourceRelationshipType==='view') $scope.fd.localVars.getResources();
                 else $scope.fd.localVars.getExistingResources();
                }
        $scope.fd.setupGrid= function(data)
        {
              $scope.fd.localVars.filesColumns = data.columnsDef;
              
              $timeout(function(){
                $scope.fd.localVars.restoreGridState();
               
               });
              
              
        }
            
              $scope.fd.localVars.setupGridNew= function(data)
              {
                $scope.fd.localVars.filesData = data.data;
                if ($scope.fd.GridState?.columnsDef===undefined) $scope.fd.localVars.filesColumns = data.columnsDef;
                $scope.fd.localVars.fileListDownloaded = true
              
              }
              
 
            $scope.fd.localVars.restoreGridState();
               
       
              
              
            
            //  Popuni listu gridOptions.data sa listom svih resursa odabranog tipa
						
            
        $scope.fd.getResourceDataFiltered = function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

           //scope.activity.activityMessage = ProcessingService.setActivityMsg('Retriving data '+scope.selectedResource);
          //$scope.filter = ResourcesDataService.prepareResourceWidgetData($scope.dashboard.widgets,-1);
         
          if ($scope.fd.selectedResource==null) {
            $scope.fd.selectedResourceId = $scope.fd.Related;
          }else
          {
             $scope.fd.selectedResourceId = $scope.fd.selectedResource.id;
            
          }
	//		ResourcesDataService.getResourceDataFiltered($scope.fd.Related,lang,$scope.filter).then(function(data) {//MIGOR
                ResourcesDataService.getResourceDataFiltered($scope.fd.selectedResourceId,$scope.taskId,lang,$scope.fd.resourceDashboard.widgets).then(function(data) {//MIGOR
                            
                            $scope.fd.setupGrid(data);
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });
        }
		
	

        $scope.fd.localVars.saveGridState= function(show_message) {
               //debugger //ktTaskDatasetList smarttable
               let columnsDef = $scope.fd.localVars.filesColumns

               let columnsQuery = $scope.fd.localVars.columnsForQuery
               $scope.fd['GridState']={'columnsDef':columnsDef,'columnsQuery': columnsQuery}
               
              if (show_message)
               MessagingService.addMessage("To make changes stick please save form",'info');            
    };		
             
        $scope.fd.getResourceData = function () 
	{
                // $scope.activity.activityMessage = ProcessingService.setActivityMsg('Downloading resource data for '+$scope.resourceId);
                 var lang = $rootScope.globals.currentUser.lang;
                 ResourcesDataService.getResourceData($scope.resourceId,$scope.taskId,lang).then(function(data) {
                                                 
                                                                $scope.fd.setupGridNew(data);
        
                },function(error){    
                        MessagingService.addMessage(error.msg,'error');
                });
	};
            $scope.fd.localVars.results = {}         


     //**********************************************************
     //           GET FILES DIRECT FILTER
     //**********************************************************      
      $scope.fd.localVars.getFilesDirect = function (relatedDatasetId,items) {
        
        $scope.fd.localVars.fileListDownloaded =false

        ResourcesDataService.getResourceDataDirectFilter(relatedDatasetId,$scope.fd.PresentationId,"i",lang,{id__in: items}).then(function(data) {
                                
                                    $scope.fd.localVars.setupGridNew(data);
                               
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    $scope.fd.localVars.fileListDownloaded = true
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };

        
        /* Get children for parent*/
        $scope.fd.localVars.GetResourceDataForParent = function () 
        {
              var lang = $rootScope.globals.currentUser.lang;
              if ($scope.resourceRecordId>0 && $scope.fd.MaxLength==0)
              {
                $scope.fd.localVars.fileListDownloaded =false
                ResourcesDataService.getResourceDataM2m($scope.fd.PresentationId,$scope.parentResourceDefId,$scope.fd.Related,$scope.resourceRecordId,$scope.taskId,$scope.fd.ItemId,lang).then(function(data) {
                            $scope.formData[0][$scope.fd.FieldName] = data; 
                            $scope.fd.localVars.setupGridNew(data);                
                },function(error){    
                    MessagingService.addMessage(error.msg,'error');
                    $scope.fd.localVars.fileListDownloaded = true
                                    });
            }              
        }

         //**********************************************************
         //           GET FILES DATA DIRECT FILTER
         //**********************************************************      
        $scope.fd.localVars.getFilesData = function (file_def_id,dataset_def_id) {
                    
                    $scope.filter = {};
                    var lang = $rootScope.globals.currentUser.lang;
                    //$scope.fd.linkedResourceId = resource_id; //MIGOR - mozda je visak (je)
                    //$scope.fd.linkedResourceDefId = resource_def_id; //MIGOR - mozda je visak (je)
               if (file_def_id!=undefined)
               {
                  
                        $scope.fd.localVars.filesData.data.length=0;
                        $scope.fd.fileSelected = false;
                        $scope.fd.localVars.resourceDataDownloading = true
                        ResourcesDataService.getFilesData($scope.fd.PresentationId,file_def_id,dataset_def_id,0).then(function(data) {
                                       
                             
                                 
                            $scope.fd.localVars.filesData = data.data
                            
                            $scope.lastResourceDefId = file_def_id;
                            $scope.lastResourceId = dataset_def_id;
                            $scope.fd.localVars.fileListDownloaded = true;
                            $scope.fd.localVars.resourceDataDownloading = false
                        
                        }           
                        ,function(error){    
                             MessagingService.addMessage(error.msg,'error');    
                             $scope.fd.localVars.fileListDownloaded = true;
                             $scope.fd.localVars.resourceDataDownloading = false
                        });
                }
              
        };//getFilsData END
              
        
              
              
              $scope.fd.onSelectSearchForm= function (item, model){
               
               if ($scope.fd.Search==undefined) $scope.fd['search']={taskId:item};
               else $scope.fd.Search.taskId = item;
                //$scope.fd.Search.widgetId=0; //ovo treba na serveru dohvatiti widgetId
                
                
              } 
            if ($scope.fd.Parameters.PublishedResourceId!=undefined && $scope.fd.Parameters.PublishedResourceId!=null)

            {
             $scope.fd.localVars.getPublishedResources()

             if ($scope.fd.GridState?.columnsDef===undefined) $scope.fd.localVars.getColumnsDef()
             else 

                {
                    $scope.fd.localVars.restoreGridState()
                    $scope.fd.localVars.fileListDownloaded =true
                }

               
                                        
                                          
                                         
            }

            //ako ima odabranih dohvati ih i setupiraj
            if ($scope.formData!=undefined && $scope.formData.length>0 && !$scope.fd.Parameters.NoGetData)
            {
                if ($scope.formData[0][$scope.fd.FieldName]!=undefined){
                   if ($scope.fd.PresentationType==0)
                  {
                    $scope.fd.localVars.getFilesDirect($scope.fd.Related,[$scope.formData[0][$scope.fd.FieldName]]);
                  }
                  else
                  {
                   if  ($scope.fd.OverrideType!="f") $scope.fd.getDatasetList();
                  }
                } else if (!$scope.formDefinition && $scope.fd.MaxLength==0) $scope.fd.localVars.GetResourceDataForParent();

            }else if ($scope.fd.PresentationType==0)
            
            {
                if (!$scope.formDefinition && $scope.fd.MaxLength==0 && !$scope.fd.Parameters.NoGetData)  $scope.fd.localVars.GetResourceDataForParent();
            }
            
            if ($scope.fd.Parameters.PublishedResourceId!=undefined) {

                if ($scope.formDefinition) {
                    $scope.fd.localVars.getPublishedResources() 
                    $scope.fd.getFormFields()
                }
                else $scope.fd.localVars.getPublishedResourceById($scope.fd.Parameters.PublishedResourceId,$scope.fd.ShowSearchForm)
            
            }
          
				},//...controller
				
            templateUrl: 'ktdirectivetemplates/ktFile.html',
            link: function (scope, elem, attrs) {
				  
                scope.fd.localVars.smartTableLinked=true
          
            }
    
    }
};