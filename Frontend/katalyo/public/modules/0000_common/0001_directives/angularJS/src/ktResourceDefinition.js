'use strict';

export function ktResourceDefinition () {
      
        return {
        restrict: 'A',
        scope:{
                    resource:'=',
                  
            },
	templateUrl: 'ktdirectivetemplates/ktResourceDefinition.html',
        controller: function ($scope,$rootScope,GridsterService,KatalyoStateManager,ResourcesDataService,MessagingService,CodesService)
        {
                $scope.gsrvc = GridsterService;
                
                $scope.ksm = KatalyoStateManager;
               
                $scope.EditResourceFn=function(val)
                {
                        
                     $scope.editResource=val;
                }
                let lang = $rootScope.globals.currentUser.lang;
                CodesService.getCodesByName('codestatus',lang).then(function (data) {
                                $scope.resourceStatusList = data.data
                                $scope.statusesDownloaded=true
                             });

                $scope.saveResourceDef = function (forma) {
          
                  if (forma.$valid)
                  {
                    ResourcesDataService.SaveResourceDef2($scope.resource).then(function(data) {
                                       
                                       if (data.status===200 || data.status==201)
                                        {
                                        MessagingService.addMessage("Resource saved successfuly",'success');  
                                        }
                                        },function(error){
                                      
                                      MessagingService.addMessage(error.msg,'error');  
                 
                                    });
                    
                  }else
                  {
                    
                    MessagingService.addMessage("Please enter required data",'warning'); 
                  }
                }; 
                
                },
        link: function (scope, elem, attrs) {
				
             
        
        }      
            
        }
};