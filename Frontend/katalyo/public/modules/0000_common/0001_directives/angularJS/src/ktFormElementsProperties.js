'use strict';

export function ktFormElementProperties () {
    return {
        restrict: 'A',
        replace: true,
        template: '<div ng-include="getTemplateUrl()" class="mt-0 mr-1"></div>',
        scope: {
            fd: '=',
            localVars: '=',
        },
        controller: function($scope, $rootScope, TaskDefinitionService){
            $scope.CTRL="ktFormElementProperties"
            $scope.tds =TaskDefinitionService
            $rootScope.massUpdateFieldList = [
                    {id:1, name: 'Required', value:'required'},
                    {id:2, name: 'Read Only', value:'readOnly'}
             ]
            
              },
        link: function (scope, element, attr) {
          
          scope.getTemplateUrl = function() {
                
                var template ="";
                if (scope.localVars==undefined) scope.localVars = {};
                if (scope.fd!==undefined)
                {
                    template = 'ktdirectivetemplates/'+scope.fd.DirectiveName+'Properties.html';// MIGOR TODO- Security check !!!
                }else
                {
                    template = 'ktdirectivetemplates/ErrorElementProperties.html';
                }
                return template;
           
            };
        }
    }
};