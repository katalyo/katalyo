'use strict';

export function ktLabel () {
        return {
            restrict: 'A',
		
             templateUrl: 'ktdirectivetemplates/ktLabel.html',
             controller: function($scope,GridsterService){
                
                $scope.gsrvc = GridsterService;
                
                $scope.fd.colLength=[];
                for (let i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));
                
                $scope.showProperties = function(event)
                {
                  $scope.gsrvc.setFieldProperties($scope.fd,event);
       
                };
                },
             link: function (scope, elem, attrs) {
              
              if (scope.fd) {
                  if (scope.fd.Parameters == null) scope.fd.Parameters = {} //null or undefined !
                  scope.fd.Parameters.HideElementProperties = true;
                  scope.fd.isLabelStyle = true;
              }
            
            
           
            }
        };
};