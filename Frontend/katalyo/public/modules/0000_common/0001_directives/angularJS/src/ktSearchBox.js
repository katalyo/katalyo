'use strict';

export function ktSearchBox () {
        return {
            restrict: 'A',
		controller: function ($scope,GridsterService,$templateCache,$compile,$element) {
                
                 $scope.gsrvc = GridsterService;
                 if ($scope.localVars==undefined) $scope.localVars =  {};
                 if ($scope.formData==undefined)  $scope.formData = [{}];
                 $scope.colLength=[];
                 for (let i=0;i<24;i++) $scope.colLength.push('col-'+(i+1));
                
                 if ($scope.fd.Parameters==undefined) $scope.fd.Parameters = {};
                 
                 $scope.fd.setStep = function() {
                  $scope.fd.stepValue =parseFloat(Math.pow(0.1,$scope.fd.Parameters.NoOfDecimals)).toFixed($scope.fd.Parameters.NoOfDecimals);
                 }; 
                
                if ($scope.fd.stepValue==null || $scope.fd.stepValue==undefined) $scope.fd.setStep();
               
               
                if ($scope.fd.showSearchForm)  
                {
                 if ($scope.fd.search==undefined)    $scope.fd.search=$scope.search[3];//initialize to equals
                 if ($scope.fd.search)  $scope.fd.search.value=$scope.fd.sItemValue; 
                }
                
                $scope.search=[
			  	{id:1,filter:'gt',name:"Greater than",icon:"fa fa-chevron-right"},
			  	{id:2,filter:'between',name:"Between",icon:"fa fa-expand"},
			  	{id:3,filter:'lt',name:"Lower than",icon:"fa fa-chevron-left"},
			  	{id:4,filter:'eq',name:"Equals",icon:"fa fa-bars"} ];
                 
                $scope.formatNumber = function()
                {
                  if ($scope.formData[0][$scope.fd.FieldName]!=undefined)
                  {
                    $scope.formatedNumber = $scope.formData[0][$scope.fd.FieldName].toLocaleString(navigator.language,{minimumFractionDigits:$scope.fd.Parameters.NoOfDecimals,maximumFractionDigits:$scope.fd.Parameters.NoOfDecimals});
                    
                  }else  $scope.formatedNumber = "";
                }
                
               
                
                 $scope.fd.formatNumberBlockchain = function(input)
                {
                  let formatedNumber;
                  let web3;
                  if ($scope.fd.Parameters.ConvertBlockchainFormat)
                  {
                        if (input!=undefined)
                        {
                               

                                if (window.ethereum)
                                {
                                  web3 = new Web3(window.ethereum);
                                }else if (window.web3)
                                {
                                  web3 = new Web3(window.web3);
                                   console.log(web3);
                                }
                                
                                else if ($rootScope.globals.currentUser.web3Infura)
                                {
                                   web3 = new Web3($rootScope.globals.currentUser.web3Infura);     
                                } else
                                {
                                   console.log('web3 provider not available');
                                   
                                   return input;
                                }
                                if ($scope.fd.Parameters.FromArray!=undefined) input = $scope.formData[0][$scope.fd.Parameters.SourceFieldName][$scope.fd.Parameters.FromArray];
                                
                                formatedNumber = web3.utils.fromWei(input.toLocaleString('fullwide', {useGrouping:false}), 'ether').toLocaleString(undefined,{minimumFractionDigits:$scope.fd.Parameters.NoOfDecimals,maximumFractionDigits:18});
                                
                  
                        }else  formatedNumber = input;
                  }else formatedNumber=input;
                  if ($scope.fd.Parameters.ConvertToDays) formatedNumber = formatedNumber/86400;                              
                  return formatedNumber;
                }
                
                let template;
                if ($scope.resourceDefinition.form_definition)  template = $templateCache.get('ktdirectivetemplates/ktSearchBox.html');
                else template = $templateCache.get('ktdirectivetemplates/ktSearchBoxExe.html');
                $element.append($compile(template)($scope)); 

            },
             
            link: function (scope, elem, attrs) {
  
				scope.isOpen=true;
            
                scope.refreshSearchValue = function() {
                         if (scope.fd.search) scope.fd.search.value=scope.fd.sItemValue;      
                }; 
                        
                if (scope.fd.showSearchForm)  {
                        if (scope.fd.search==undefined)   scope.fd.search=scope.search[3];//initialize to equals
                        if (scope.fd.search)  scope.fd.search.value=scope.fd.sItemValue; 
                }
               
                scope.showProperties = function(event) {
                 scope.gsrvc.setFieldProperties(scope.fd,event); 
                };
             
                scope.applyNumFilters = function(index) {
				   scope.fd.search=scope.search[index];
                   if (scope.fd.sItemValue) scope.fd.search.value=scope.fd.sItemValue;
				   scope.isOpen=false;		
                };
       
            }
            
        }
};
		
                
               