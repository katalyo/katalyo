'use strict';

export function ktProcessingAnimationTimeline () {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktProcessingAnimationTimeline.html',
            scope:{
                   message : "@",
                   showMsg: "=",
                   animationType: "=",
      			},
             link: function (scope, elem, attrs) {
                
                scope.formItems = [1,2,3,4];
                if (scope.animationType=="table") scope.formItems = [1,2,3,4,5,6,7,8];
              }
        }             
};