'use strict';

export function ktSeparator () {
        return {
            restrict: 'A',
			 controller: function ($scope,GridsterService) {
                
                $scope.gsrvc = GridsterService;
                $scope.showProperties = function(event)
                {
                        $scope.gsrvc.setFieldProperties($scope.fd,event);
       
                };
                $scope.colLength=[];
                for (let i=0;i<24;i++) $scope.colLength.push('col-'+(i+1));
                },
             templateUrl: 'ktdirectivetemplates/ktSeparator.html',
             link: function (scope, elem, attrs) {
              
            
         
            }
        };
};