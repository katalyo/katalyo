'use strict';

export function ktTaskViewChart () {
   
    var controller = ['$scope', '$uibModal','$stateParams','$timeout','CodesService','ResourcesDataService','$rootScope','ProcessingService','WidgetDataExchangeService','GridsterService','MessagingService','CODES_CONST','TaskDataService','KatalyoStateManager',
                      function ($scope,$uibModal,$stateParams,$timeout,CodesService,ResourcesDataService,$rootScope,ProcessingService,WidgetDataExchangeService,GridsterService,MessagingService,CODES_CONST,TaskDataService,KatalyoStateManager) {
      
      
    if ($scope.fd.localVars===undefined) $scope.fd.localVars={};

    $scope.fd.localVars.chartType="pie";       

          
    $scope.fd.localVars={
            datasetlistlinked: false,
              showMenu: false,
              showForm: false,
              searchFormDownloaded: false,
              src_type_text: CODES_CONST._POPULATE_TYPE_,
              results: {},
              animationsEnabled: true,
              formParams: {formViewType:1, showGridNavigation:false},
              widgetType: 3,
              gridsterType: "dataset",
              resourceType:"dataset",
              datasetMapperType:0,
              searchFilter:[{}],
                        data:[34,45,43],
                        labels:['label1','label2','label3'],
                        formLoaded:true,
                        calculateFunction: [
                            {id:1,name:'sum'},{id:2,name:'mean'},{id:3,name:'min'},{id:4,name:'max'},{id:5,name:'count'},
                        ],
                        chartOptions: {
                                 plugins: {     
                            legend: {
                                display: true,
                                labels: {
                                    color: 'rgb(255, 99, 132)'
                                }
                            } ,                        
                                      labels: {
                                        // render 'label', 'value', 'percentage', 'image' or custom function, default is 'percentage'
                                        render: 'label',
                                        // precision for percentage, default is 0
                                        precision: 0,
                                        // identifies whether or not labels of value 0 are displayed, default is false
                                        showZero: true,
                                        // font size, default is defaultFontSize
                                        fontSize: 14,
                                        // font color, can be color array for each data or function for dynamic color, default is defaultFontColor
                                        fontColor: '#fff',
                                        // font style, default is defaultFontStyle
                                        fontStyle: 'normal',
                                        // font family, default is defaultFontFamily
                                        fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
                                        // draw text shadows under labels, default is false
                                        textShadow: true,
                                      }
                        },
                                  scales: {
                              yAxes: [{
                                 ticks: {
                                     beginAtZero: true
                                 }
                             }]
                         }
                            },
                        chartTypes: [{id:1,name:'line'},{id:2,name:'bar'},{id:3,name:'radar'},{id:4,name:'doughnut'},{id:5,name:'pie'},{id:6,name:'polarArea'},{id:7,name:'bubble'},{id:99,name:'counter'} ]
            };
      
        $scope.fd.localVars.toggleDropdown = function(){
                                       $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
         }
     
    $scope.CTRL='ktTaskViewChart'
    $scope.fd.initialLoad=true;
    $scope.fd.localVars.showSearchFilters=true;
    $scope.fd.localVars.resourceDataDownloaded = false;
    
    $scope.gsrvc=GridsterService;
    $scope.fd.localVars.datasetMapperType=0;
    $scope.fd.initialLoad=true;
    $scope.fd.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_; 
    $scope.fd.localVars.searchFormDownloaded = false;
    $scope.fd.localVars.resourceDataDownloaded = false;
    $scope.fd.localVars.widgetState={};
    $scope.fd.localVars.widgetState.selectedItems=[];
    $scope.fd.localVars.selectedItems=[];
    $scope.taskId=$scope.resourceDefinition.ResourceDefId;
    if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
    if ($scope.taskId==undefined) $scope.taskId=0
    $scope.fd.localVars.taskId=$scope.taskId;
    $scope.fd.localVars.widgetType = 4;
    $scope.fd.localVars.formParams={formViewType:1,showGridNavigation:true};
    $scope.fd.localVars.cardLayoutParams={formViewType:1,showGridNavigation:true};
    //$scope.gsrvc.setFormViewType($scope.fd.localVars.formParams={formViewType));
    $scope.fd.localVars.parentDatasetDefId=$scope.fd.Related;
    $scope.fd.localVars.animationsEnabled = true;
    $scope.fd.localVars.isCollapsed = $scope.fd.Parameters.SearchFormCollapsed;
    if ($scope.fd.Parameters==undefined) $scope.fd.Parameters = {PresentationType:'table'};
    if ($scope.fd.Parameters.PresentationType==undefined || $scope.fd.Parameters.PresentationType==null) $scope.fd.Parameters.PresentationType='table';
    $scope.fd.localVars.gridsterType="resource";
    $scope.itemValue = {val:null};
     $scope.fd.localVars.resourceDefinition = $scope.resourceDefinition;
    $scope.fd.localVars.isCollapsed = $scope.fd.Parameters.SearchFormCollapsed;
    if ($scope.fd.localVars.isCollapsed==undefined) $scope.fd.localVars.isCollapsed =true;
    if ($scope.fd.AutoSearch) $scope.fd.localVars.resourceDataDownloading = true;
   
          

  $scope.fd.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
        
         $scope.fd.localVars.dashboardCard = {
  				id: '1',
  				name: 'Home',
  				widgets: {}
				} ;
                
                
                
       $scope.fd.localVars.gridOptionsResourceData = {
        //  columnDefs: colDefs,
        //enableGridMenu: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: $scope.fd.Multiline,
      //  enableHorizontalScrollbar : 1,
       // enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: false,
        enableColumnResizing: true,
        enableColumnReordering: true,
        //useExternalPagination: false,
        paginationPageSizes: [10, 25, 50,100,200,500,1000,5000,10000],
       // enablePaginationControls: true,
      };
      
      
      
       if ($scope.fd.Parameters.printable == true) {
        $scope.fd.localVars.gridOptionsResourceData.showColumnFooter = false;
        $scope.fd.localVars.gridOptionsResourceData.showColumnFooter = false;
        $scope.fd.localVars.gridOptionsResourceData.showGridFooter = false;
        $scope.fd.localVars.gridOptionsResourceData.enablePaginationControls = false; 
        $scope.fd.localVars.gridOptionsResourceData.enableVerticalScrollbar = 0;  
        $scope.fd.localVars.gridOptionsResourceData.enableHorizontalScrollbar = 0;  
        $scope.fd.localVars.gridOptionsResourceData.enableFiltering = false;  
        
    }
      
      
 
   
  var paginationOptions = {
    pageNumber: 1,
    pageSize: 10,
    sort: null
    };
    
    
       $scope.fd.localVars.gridOptionsRelated = {
      //  columnDefs: colDefs,
       enableGridMenu: true,
       showGridFooter: true,
       showColumnFooter: true,
       enableRowSelection: false,
       multiSelect: false,
       enableHorizontalScrollbar : 2, 
       nableVerticalScrollbar : 2,
       enableFiltering: true,
       enableRowHeaderSelection: true,
       enableColumnResizing: true,
       minRowsToShow: 2,
      
   }
    
    
    
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        $scope.fd.ParentDatasetDefId=$scope.fd.Related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
        $scope.fd.localVars.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        $scope.formDefinition = $scope.resourceDefinition.form_definition;
        if  ($scope.formDefinition==undefined) { 
          $scope.formDefinition = false;
        }
        
        
        
          if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        
        
      $scope.fd.localVars.executeDatasetList = function(){   
        var lang=$rootScope.globals.currentUser.lang;
        if ($scope.fd.PresentationId==null) {
            $scope.fd.PresentationId = 0;
        }
        if ($scope.fd.localVars.selectedResourceId==undefined) $scope.fd.localVars.selectedResourceId = $scope.fd.Related;
        if ($scope.fd.localVars.widgetState.pageSize==undefined) $scope.fd.localVars.widgetState.pageSize = paginationOptions.pageSize;
         
        TaskDataService.ktTaskViewChart($scope.fd.Related,$scope.fd.PresentationId,$scope.fd.localVars.taskId,$scope.fd.localVars.formType,lang).then(function(data) {
                                //debugger
                                    //PREPARE CHART
                                    $scope.fd.localVars.chartData = data.data.chart
                                    $scope.fd.localVars.data = Object.values(data.data.chart[$scope.fd.Parameters.CalculateFunction.name])
                                    $scope.fd.localVars.labels = Object.keys(data.data.chart[$scope.fd.Parameters.CalculateFunction.name])
                                    $scope.fd.localVars.chartType=  $scope.fd.Parameters.chartType.name
                                    
                                     //summing all values for counter
                                    $scope.fd.localVars.countAll = $scope.fd.localVars.data.reduce((partialSum, a) => partialSum + a, 0);
                                    
                                    
                                    $scope.fd.localVars.widgetState.resourceWidgetDefinition= data.data.resourceDefinition;
                           
                                    $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columns;
                                    //debugger //ktTaskDatasetList
   
                                    if ($scope.fd.GridState?.columnsDef?.length > 0)  
                                            $scope.fd.localVars.gridOptionsResourceData.columnDefs = $scope.fd.GridState.columnsDef
                                    // MIGOR TODO 
                                    // KAD SE MIJENJA RESURS NA DATASET LISTI TREBA NULIRATI GRIDSTATE !
                                    
                                    //debugger  //prepare columnDefs //smarttable
                                    for (var col of $scope.fd.localVars.gridOptionsResourceData.columnDefs){
                                        if (col.visible == undefined) col.visible=true
                                    }
                                    
                                    $scope.fd.fItemValue=data.data.widgets;
                                    if (data.data.widgetsCard!=null)
                                    {
                                        $scope.fd.localVars.dashboardCard.widgets.layout =  data.data.widgetsCard;
                                    }
                                    $scope.fd.localVars.searchFormDownloaded = true;
                                    //$scope.fd.localVars.setField($scope.fd.fItemValue,'Required',false);
                                    $scope.fd.localVars.formLoaded=true;
                                    
                                   
                                    $scope.fd.localVars.dashboard.widgets.layout =  data.data.widgets;
                                    $scope.fd.localVars.widgetState.widgets = $scope.fd.localVars.dashboard.widgets.layout;
                                   
                                    $scope.fd.localVars.widgetState.formData =  data.data.formData;
                                    $scope.fd.Search = $scope.fd.localVars.widgetState.formData;
                                    if ($scope.fd.Search==undefined) $scope.fd.Search={};
                                    $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
                                    
                                    //setup data grid or card
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')
                                    $scope.fd.localVars.gridOptionsResourceData.data = $scope.fd.localVars.TransformDataForCard(data.data.data);
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                    $scope.fd.localVars.nextPage = data.data.data.next;
                                    $scope.fd.localVars.previousPage = data.data.data.previous;
                                    $scope.fd.localVars.totalCount=data.data.data.count;
                                    $scope.fd.localVars.widgetState.currentPage=1;
                                    if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.widgetState.currentPage==0) $scope.fd.localVars.widgetState.currentPage=1;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/$scope.fd.localVars.widgetState.pageSize);
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
                                    //$scope.fd.localVars.prepareChartFromServer() 
               
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                             });
                              
      };//getFormResource_END


                     
        $scope.fd.localVars.setSelectedDataset = function(id,saved)  {
            //debugger
              if ($scope.fd.localVars.selectedResourceItem==undefined) $scope.fd.localVars.selectedResourceItem={}
              // delete residual variables and parameters
              //$scope.fd.Parameters={}
              //pick orher parameters from database
              $scope.fd.localVars.getPublishedResourceById(id,saved);
              
		}
        
        
            $scope.fd.localVars.getPublishedResourceById = function(id,saved) {
    
                let pos
                let old_publish_id = $scope.fd.Parameters.PublishedResourceId// TODO -  fd.Parameters = undefined
                //debugger //getPublishedResourceById
                  //saved=true  FormDefSearch=undefined  
                if (saved && $scope.fd.FormDefSearch?.SearchForm!==undefined && $scope.fd.FormDefSearch?.SearchForm!==null && $scope.fd.FormDefSearch?.SearchForm.length>0)
                  { 
                        pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                      
                        if (pos>=0){
                            $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.publishedResourceList[pos].name;
                            $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos];

                            $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                        }
                        
                        $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDefSearch?.SearchForm
                        if ($scope.fd.Parameters.PresentationType=='card') 
                            {
                                $scope.fd.localVars.dashboardCard.widgets.layout = $scope.fd.FormDefSearch?.CardForm
                                $scope.fd.localVars.cardLayoutLoaded = true
                            }

                        if ($scope.fd.localVars.formDefinition) $scope.fd.localVars.resourceDataDownloaded =true
                        $scope.fd.localVars.formLoaded=true;

                  }else{
                      pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                      $scope.fd.FormDefSearch={}
                      

                      if (pos>=0){
                        ResourcesDataService.GetPublishedResourceById(id).then(function (data) {
           
                                $scope.fd.localVars.selectedResourceItem.Form = data.data.Form
                                $scope.fd.localVars.selectedDataset = $scope.fd.localVars.publishedResourceList[pos];
                                //TODO - selectedResourceItem.name  i $scope.fd.localVars.selectedResourceItem.id  su undefined
                                // MIGOR START - TODO - NE RADI BEZ OVOG DOLJE
                                if  ($scope.fd.localVars.selectedResourceItem.name == undefined)      
                                            $scope.fd.localVars.selectedResourceItem.name = $scope.fd.localVars.selectedDataset.name;
                                if ($scope.fd.localVars.selectedResourceItem.id == undefined)  
                                        $scope.fd.localVars.selectedResourceItem.id =   $scope.fd.localVars.selectedDataset.id
                                //MIGOR END
                                   
                                $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                                 
                                //debugger
                                if ($scope.fd.localVars.selectedResourceItem_c!==undefined)
                                {
                                    $scope.fd.Parameters.PublishedResourceId=$scope.fd.localVars.selectedResourceItem_c.id;
                                    $scope.fd.Parameters.PublishedResourceVersion=$scope.fd.localVars.selectedResourceItem_c.Version;
                                    $scope.fd.Related=$scope.fd.localVars.selectedResourceItem_c.ResourceDefId; 
                                    $scope.fd.localVars.freshForm=angular.copy($scope.fd.localVars.selectedResourceItem_c.Form)
                                    $scope.fd.Related = $scope.fd.localVars.publishedResourceList[pos].ResourceDefId
                                }
                                $scope.fd.localVars.dashboard.widgets.layout = data.data.Form

                                
                                if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.dashboardCard.widgets.layout = data.data.Form
                              
                                $scope.fd.FormDefSearch['SearchForm'] = $scope.fd.localVars.dashboard.widgets.layout
                                if ($scope.fd.Parameters.PresentationType=='card') 
                                    {
                                        $scope.fd.FormDefSearch['CardForm'] = $scope.fd.localVars.dashboardCard.widgets.layout
                                        $scope.fd.localVars.cardLayoutLoaded = true

                                    }
                                //MIGOR -TODO - zakomentirao jer je "$scope.fd.Parameters.PublishedResourceId" UNDEFINED
                                //if ($scope.fd.GridState?.columnsDef===undefined || old_publish_id!=id) $scope.fd.localVars.getDefaultColumnsDef()
                                //else $scope.fd.localVars.resourceDataDownloaded =true
                            
                               $scope.fd.localVars.fieldsList=[]
                               
                                  //TODO - ovo se moze sigurno iz nekih postojecih varijabli izvuci
                                  //debugger
                                  $scope.fd.localVars.getFieldNamesFromObject($scope.fd.localVars.dashboard, $scope.fd.localVars.fieldsList)
                                  //debugger                              
                                      
                                        
                                //$scope.fd.localVars.getResourceData()
                                if ($scope.fd.PresentationId) 
                                        $scope.fd.localVars.executeGetResourceDataPandas()
                                 
                                $scope.fd.localVars.formLoaded=true;

                            },function(error){    
                              MessagingService.addMessage(error.msg,'error');
                        });

                    }
                }
                 
       
} 
 
 
 
        //novo start
      $scope.taskId=$scope.resourceDefinition.ResourceDefId;
      if ($scope.taskId==undefined) {
            $scope.taskId = $scope.resourceDefinition.ResourceDefId_id;
      }
      if ($scope.taskId==undefined) $scope.taskId=0;
      $scope.fd.localVars.taskId=$scope.taskId;
    
      $scope.fd.ParentResourceDefId=$scope.fd.Related;
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
      $scope.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.formDefinition = $scope.resourceDefinition.form_definition;
        
      if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
      $scope.fd.localVars.resourceRecordId=0;
      if ($scope.taskInitiateId==undefined)
            $scope.taskInitiateId =0;
      if ($scope.taskExecuteId==undefined)
            $scope.taskExecuteId =0;  
      if ($scope.prevTaskExeId==undefined)
            $scope.prevTaskExeId =0;
    //novo end
    
        
        
        
        $scope.fd.localVars.resourceRecordId = 0;
        $scope.fd.ParentElementId = 0;
      
      var out_fields = ['resourceRecordId'];
      
      
      //resourceLink
      var widgetDefinition = {
            DEI: [{'name':'resourceLinkDatasetList','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],
            actions: [{name:'PrepareData',status:false}],
            name: 'datasetListWidget',
            widgetId: $scope.fd.PresentationId,
            taskId:  $scope.lTaskId,
            resourceDefId : $scope.fd.Related, //resourceDefId : $scope.selectedResource
            resourceRecordId:$scope.fd.localVars.resourceRecordId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId, output:out_fields 
      };
      $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
      $scope.fd.localVars.LocalId=$scope.fd.localVars.localWidgetId;
      $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
          
       
         /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
        
         let dependent_field_pid = $scope.fd.LocalId;
        let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
        let dependent_field_pid2
        
        
        
        if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
       
        
        let valueState= $scope.ksm.addSubject($scope.fd.ElementType+"ValueStateName"+$scope.fd.PresentationId+$scope.resourceDefinition.transaction_id,$scope.itemValue.val);
        let valueStateName = valueState.subject.name;
        
        let value_changed = function(data)
        {
            valueState.subject.setStateChanges = data;
            valueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
        let apply_value = function(data)
        {
            $scope.itemValue.val = data;
        }; 
       
        if ( $scope.fd.Parameters.DependentFieldId!=undefined     && $scope.fd.Parameters.DependentFieldId!=null     && $scope.fd.Parameters.DependentFieldId!="" )
            {
                let dependent_field = "Field"+$scope.fd.Parameters.DependentFieldId;
                let subscribedSubject = $scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            }
 
           
           
      $scope.fd.localVars.showProperties = function(datasetMapperType,showPropertiesRight)
      {
           //debugger
            if ($scope.fd.localVars.datasetMapperType==undefined)
            {
               $scope.fd.localVars.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) 
                    $scope.fd.localVars.datasetMapperType = datasetMapperType;
               else 
                   $scope.fd.localVars.datasetMapperType=0;
            }
           
             
            if (showPropertiesRight) $scope.gsrvc.setFieldProperties($scope.fd);
       
            if ($scope.fd.localVars.dashboard != undefined)  {
                //debugger
                 $scope.fd.localVars.fieldsList = []
                 $scope.fd.localVars.getFieldNamesFromObject($scope.fd.localVars.dashboard, $scope.fd.localVars.fieldsList)
                
            }
      };
           
     
        
    let process_form_change = function(data)
    { 
        $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
        if ($scope.fd.Parameters.PresentationType=='table') $scope.fd.localVars.getResourceColumns(true);
        else if ($scope.fd.Parameters.PresentationType=='dropdown') $scope.fd.localVars.getDatasetDataList($scope.fd.Parameters.ClientSideFilter,true);
        else $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,1,$scope.fd.localVars.pageSize);               
                      
    };
    
    
    //subscribe to events - initially only refresh data
   
    let dependent_field2 = "Field"+$scope.fd.PresentationId;
    let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
    let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,process_form_change);

        
     $scope.fd.localVars.ProcessSearchForm=function (form){
            
           if (form!=undefined){
            for(var i=0;i<form.length;i++) {
                
                    if ('ElementType' in form[i])
                    {
                      if (form[i].AutoSearch)
                      {
                        if (form[i].ElementType == 'resource')
                        {
                        var pos = $scope.fd.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(form[i].PresentationId);
                        if (pos<0) $scope.fd.localVars.childWidgets.push({widgetId:form[i].PresentationId,processed:false})
                        }
                        //subscribe to change
                        if (form[i].Parameters!=undefined)
                        {
                            if (form[i].Parameters.DependentFieldId!=undefined && form[i].Parameters.DependentFieldId!=null)
        
                            {
                                if (form[i].Parameters.DependentFieldId!="")
                                {
                                    let dependent_field = "Field"+$scope.fd.PresentationId;
                                    let subscribedSubject = form[i].ElementType+"ValueStateName"+form[i].ItemId+$scope.resourceDefinition.transaction_id;
                                    let subject = $scope.ksm.addSubject(subscribedSubject,null);
                                    let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",process_form_change);
                                }
                            }
                        }
                        
                      }
                      
                     }
                      if ('layout' in form[i])
                      {   
                       $scope.fd.localVars.ProcessSearchForm(form[i].layout);
                      }
              }
          } 
           
      };
      
      $scope.fd.localVars.SearchFormProcessed=false;

          
       // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST) -- MOZDA NE TREBA CIJELA ILI SAMO SETSELECTEDDATASET
           $scope.fd.localVars.getDatasets = function()  {
						//debugger
                       if ($scope.fd.localVars.datasetList==undefined) $scope.fd.localVars.datasetList=[];
          
                       var lang=$rootScope.globals.currentUser.lang;
                       ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
                       
                      function(data) {
										
                                 $scope.fd.localVars.datasetList = data.data;
                                  if ($scope.fd.Related!=null)
                                  {
                                   $scope.fd.localVars.setSelectedDataset($scope.fd.Related);
                                   }     		 
                      },function(error){    
                       MessagingService.addMessage(error.msg,'error');
                      });
	};
            
      $scope.fd.localVars.GetDatasetData=function(data)

      {
            fromWidget = data.widgetId;
            var allProcessed=true;
            var pos = $scope.fd.localVars.childWidgets.map(function(e) { return e.widgetId; }).indexOf(fromWidget);

            for (var i=0;i<$scope.fd.localVars.childWidgets.length;i++)
              {
                
                if ($scope.fd.localVars.childWidgets[i].widgetId == fromWidget) $scope.fd.localVars.childWidgets[i].processed = true;
                if (!$scope.fd.localVars.childWidgets[i].processed) allProcessed = $scope.fd.localVars.childWidgets[i].processed;
              }

              if($scope.fd.AutoSearch && allProcessed && $scope.fd.localVars.SearchFormProcessed) {
                    $scope.fd.localVars.selectedResource=$scope.fd.Related;
                    if ($scope.fd.Parameters!=null && $scope.fd.Parameters!=undefined)
                    {
                      if ($scope.fd.Parameters.PresentationType=='table') $scope.fd.localVars.getResourceColumns(true);
                      else if ($scope.fd.Parameters.PresentationType=='dropdown') $scope.fd.localVars.getDatasetDataList($scope.fd.Parameters.ClientSideFilter,false);
                      else $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,1,$scope.fd.localVars.pageSize);               
                         
                    }                   
                }
      };
    
      if ($scope.localVars!=undefined) $scope.fd.localVars.childWidgets = [];
      else $scope.localVars = {childWidgets : []};
      var fromWidget=0;

      let search_form_ready = function(data)
        {
            $scope.fd.localVars.GetDatasetData(data); 
        };
        
        let action_search_form_ready = $scope.ksm.addAction(valueStateName,"SearchFormReady"+$scope.taskExecuteId+$scope.resourceDefinition.transaction_id,search_form_ready);
       
      
 
      
   //**********************************************************
	//           GET DATASET RELATED RECORDS
	//**********************************************************      
        $scope.fd.localVars.getDatasetRelatedRecords = function (relatedDatasetId,recordId,modelId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataRelated($scope.fd.Related,recordId,relatedDatasetId,0,modelId,lang).then(
                function(data) {       
                                     $scope.fd.localVars.gridOptionsRelated.data = data.data;
                                     $scope.fd.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                                     $scope.fd.localVars.showGrid=true;
                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
        
        
    //**********************************************************
	//           GET DATASET SINGLE RECORD
	//**********************************************************      
      $scope.fd.localVars.getDatasetSingleRecord = function (relatedDatasetId,recordId) {
  
  
         var lang = $rootScope.globals.currentUser.lang;
        
         ResourcesDataService.getResourceDataDirectFilter(relatedDatasetId,0,lang,{id: recordId}).then(
         
                    function(data) {        
                                     $scope.fd.localVars.gridOptionsRelated.data = data.data;
                                     $scope.fd.localVars.gridOptionsRelated.columnDefs = data.columnsDef;
                               
                    },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };
    
        $scope.$on('SearchFormReady'+$scope.taskExecuteId+$scope.fd.PresentationId,function(event, data1){
      
                                        $scope.fd.localVars.GetDatasetData(data1); 
            
        });
        
        
        
        
        $scope.fd.localVars.prepareChartFromServer = function() {
            
                                    //debugger //ktGetResourceDataPandas
                                    //$scope.fd.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                    $scope.fd.localVars.chartData = data.data
                                    $scope.fd.localVars.data = Object.values(data.data.data)
                                    $scope.fd.localVars.labels = Object.keys(data.data.data)
                                    $scope.fd.localVars.chartType=  data.data.parameters.chartType.name
                                    
                                    //summing all values for counter
                                    $scope.fd.localVars.countAll = $scope.fd.localVars.data.reduce((partialSum, a) => partialSum + a, 0);
                                    
                                    // TREBA KONVERTIARTI IZ FLOAT U INT ZA ITEM VRIJEDNOST !!!
                                    $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columns;
                                    $scope.fd.fItemValue=data.data.widgets;
                               
                                    $scope.fd.localVars.searchFormDownloaded = true;
                                    $scope.fd.localVars.setField($scope.fd.fItemValue,'Required',false);
                                    $scope.fd.localVars.formLoaded=true;
                                   
                                    $scope.fd.localVars.dashboard.widgets.layout =  data.data.widgets;
                                    if ($scope.formData == undefined) $scope.formData={};
                                    $scope.formData[$scope.fd.PresentationId] =  data.data.formData;
                                    $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
                                    if ($scope.fd.Search==undefined) $scope.fd.Search={};
                                    
                                    //setup data grid or card
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')     $scope.fd.localVars.gridOptionsResourceData.data = $scope.TransformDataForCard(data.data.data);
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                   // $scope.fd.localVars.nextPage = data.data.data.next;
                                    //$scope.fd.localVars.previousPage = data.data.data.previous;
                                   // $scope.fd.localVars.totalCount=data.data.data.count;
                                    $scope.fd.localVars.currentPage=1;
                                    if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.currentPage==0) $scope.fd.localVars.currentPage=1;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/$scope.fd.localVars.pageSize);
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
            
        }
        
    $scope.fd.localVars.executeGetResourceDataPandas = function(){   
            //debugger
    
            var lang=$rootScope.globals.currentUser.lang;

            if ($scope.fd.localVars.selectedResource==undefined) $scope.fd.localVars.selectedResource = $scope.fd.Related;
            if ($scope.fd.localVars.pageSize==undefined) $scope.fd.localVars.pageSize = paginationOptions.pageSize;
                   
            TaskDataService.ktGetResourceDataPandas($scope.fd.Related,$scope.fd.PresentationId,$scope.fd.localVars.taskId,$scope.fd.localVars.formType,lang).then(
        
                function(data) {
                 
                                    //debugger //ktGetResourceDataPandas
                                    $scope.fd.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                    $scope.fd.localVars.chartData = data.data
                                    $scope.fd.localVars.data = Object.values(data.data.data)
                                    $scope.fd.localVars.labels = Object.keys(data.data.data)
                                    $scope.fd.localVars.chartType=  data.data.parameters.chartType.name
                 
                                        
                                        // TREBA KONVERTIARTI IZ FLOAT U INT ZA ITEM VRIJEDNOST !!!
                                 
                                    $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columns;
                                    $scope.fd.fItemValue=data.data.widgets;
                                    if (data.data.widgetsCard!=null)
                                    {
                                        $scope.fd.wItemValue=data.data.widgets;
                                        $scope.fd.localVars.dashboardCard.widgets.layout =  data.data.widgetsCard;
                                    }
                                    $scope.fd.localVars.searchFormDownloaded = true;
                                    $scope.fd.localVars.setField($scope.fd.fItemValue,'Required',false);
                                    $scope.fd.localVars.formLoaded=true;
                                   
                                    $scope.fd.localVars.dashboard.widgets.layout =  data.data.widgets;
                                    if ($scope.formData == undefined) $scope.formData={};
                                    $scope.formData[$scope.fd.PresentationId] =  data.data.formData;
                                    $scope.fd.Search = $scope.formData[$scope.fd.PresentationId];
                                    if ($scope.fd.Search==undefined) $scope.fd.Search={};
                                    
                                    //setup data grid or card
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')     $scope.fd.localVars.gridOptionsResourceData.data = $scope.TransformDataForCard(data.data.data);
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                   // $scope.fd.localVars.nextPage = data.data.data.next;
                                    //$scope.fd.localVars.previousPage = data.data.data.previous;
                                   // $scope.fd.localVars.totalCount=data.data.data.count;
                                    $scope.fd.localVars.currentPage=1;
                                    if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.currentPage==0) $scope.fd.localVars.currentPage=1;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/$scope.fd.localVars.pageSize);
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
                             
                        
               
                 },function(error){
                                   MessagingService.addMessage(error.msg,'error'); 
                             });
                              
      };


     $scope.fd.localVars.getFormFields = function(){
                 
              var lang=$rootScope.globals.currentUser.lang;
               //debugger       
              ResourcesDataService.getFormFields($scope.fd.Related,lang).then(function(data) {
                                  $scope.fd.localVars.PresentationElements= data.data;
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                             });         
          };
          
          
    
    $scope.fd.localVars.selectCardItem = function(index){
        let value;
        if ($scope.fd.localVars.selectedCardIndex!=index)
        {
            $scope.fd.localVars.selectedCardIndex=index;
            value=$scope.fd.localVars.gridOptionsResourceData.data[$scope.fd.localVars.selectedCardIndex][0];
        }
        else
        {
            $scope.fd.localVars.selectedCardIndex=null;
            value = null;
        }
        //send data to package
         WidgetDataExchangeService.setData({
                  DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.selectedDatasetRecordId}
                        ,{name:'resourceRecordIds',value:value},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
     };
      
   $scope.fd.localVars.datasetRecordSelected = false;
   $scope.fd.localVars.selectedDatasetRecordId = 0;
 
    $scope.fd.localVars.restoreGridState= function() {
        if ($scope.fd.GridState!=null)  {
          
            $scope.restoreGridState=true;

        }
    };
    
    
      $scope.fd.localVars.getResourceData = function () {

        var lang = $rootScope.globals.currentUser.lang;
        $scope.fd.localVars.resourceDataDownloading = true;
        if ($scope.fd.localVars.pageSize==undefined) $scope.fd.localVars.pageSize = paginationOptions.pageSize;
        //debugger

        ResourcesDataService.getResourceData($scope.fd.localVars.selectedDataset.id,$scope.taskId,$scope.fd.localVars.formType,lang).then(function(data) {
                               
                                     $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                                     $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                                     $scope.fd.localVars.restoreGridState();
                                     $scope.fd.initialLoad = false;
                                     $scope.fd.localVars.resourceDataDownloaded = true;
                                     $scope.fd.localVars.resourceDataDownloading = false;
                                    
                                 },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                   $scope.fd.localVars.resourceDataDownloading = false;
                             });
        };


          
          
          $scope.$watch('itemValue.val',function(newValue,oldValue){
                  if (newValue!=oldValue)
                  {
                   //send data to package
                   let newValueSingle,newValueMultiple;
                   if ($scope.fd.Multiline)
                   {
                    newValueMultiple = newValue;
                    newValueSingle="";
                   }
                   else
                   {
                    newValueMultiple = {};
                    newValueSingle=newValue;
                   }
                   
                   $scope.ksm.executeAction(valueStateName,"valueChanged", newValue);
                   
                    WidgetDataExchangeService.setData({
                    DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:newValueSingle}
                        ,{name:'resourceRecordIds',value:newValueMultiple},{name:'resourceRecordsAll',value:$scope.fd.localVars.datasetItems}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
           
                  }
      });
        
       
       //**********************************************************
      //           GET DATASET DATA FOR DROPDOWN
      //**********************************************************
      var refresh_list_from_callback=function(data)
            {
                $scope.fd.localVars.getDatasetDataList(true);  
            }
        $scope.filterOnClient = function(items)
        {
            let temp=[];
            let search;
            if (Array.isArray($scope.fd.Search) && $scope.fd.Search.length>0) search = $scope.fd.Search[0];
            else search = $scope.fd.Search;
            for (let i=0;i<items.length;i++)
            {
                for (let key in search) {
                         
                    if (search[key]!=undefined)
                    {
                        if (key in items[i])
                        {
                            if (items[i][key]==search[key].id || search[key]==null || search[key]==undefined || items[i][key]==undefined)  temp.push(items[i]);
                        }else if (key in items[i])
                        {
                            if (items[i][key]==search[key] || search[key]==null || search[key]==undefined || items[i][key]==undefined)  temp.push(items[i]);
                        }else temp.push(items[i]);
                    }else temp.push(items[i]);
                }
                
                
            }
            return temp;
            
        }
        $scope.fd.localVars.getDatasetDataList = function(fromCache,clientSideFilter)
        {
                    if (fromCache==undefined) fromCache=true;
                    if (clientSideFilter==undefined) clientSideFilter=false;
                    var lang = $rootScope.globals.currentUser.lang;
                    $scope.fd.localVars.datasetItems = null;
                    ResourcesDataService.getResourceDataListAll($scope.fd.Related,$scope.fd.PresentationId,lang,fromCache,refresh_list_from_callback).then(function(data) {
                   
                        if (clientSideFilter) $scope.fd.localVars.datasetItems = $scope.filterOnClient(data.data);
                        else $scope.fd.localVars.datasetItems = data.data;
                        $timeout(function(){
                            
                            if ($scope.itemValue.val!=undefined)
                            {
                            let pos = $scope.fd.localVars.datasetItems.map(function(e) { return e.id; }).indexOf($scope.itemValue.val.id);
                  
                                if (pos<0){
                                    $scope.itemValue.val = null;
                                }
                            }else
                            
                            {
                                $scope.itemValue.val = null;
                            }
                        });
                        
                    },function(error){    
                        MessagingService.addMessage(error.msg,'error');
                    });
              
            
        };
        
        
        $scope.fd.localVars.GetDatasetDataClick = function()
        {
            if ($scope.fd.Parameters.PresentationType=='dropdown')
            {
               $scope.fd.localVars.getDatasetDataList(); 
            }else
            {
                $scope.fd.localVars.selectedResource=$scope.fd.Related;
                $scope.fd.localVars.getResourceColumns(true);
            }
        };    
           
	$scope.TransformDataForCard = function(data)
    {
        let newData = [];
        let newElement = [];
        for (let i=0;i<data.length;i++)
        {
        
        newElement.push(data[i]);
        newData.push(newElement);
        
        newElement = [];
        }
        return newData; 
    };
    
      //**********************************************************
      //           GET RESOURCE DATA PAGINATED
      //**********************************************************      
    $scope.fd.localVars.getResourceDataPaginated = function (urlNext,urlPrev,newPage,pageSize) {
        
        
        $scope.fd.localVars.resourceDataDownloaded = false;        
        if (Math.ceil($scope.fd.localVars.totalCount/pageSize)<newPage)
        {
          newPage = Math.ceil($scope.fd.localVars.totalCount/pageSize);
        }
         
         var lang = $rootScope.globals.currentUser.lang;
          
         if (pageSize==undefined)
         
         {
          $scope.fd.localVars.pageSize = paginationOptions.pageSize;
          pageSize= $scope.fd.localVars.pageSize;
         }
         $scope.fd.localVars.selectedItems.length=0;
         if ($scope.fd.PopulateMethod=='M')
         {
           $scope.fd.localVars.resourceDataDownloading = true;
          
          ResourcesDataService.getResourceDataPaginatedFilter($scope.fd.localVars.selectedResource,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.Search, $scope.fd.localVars.dashboard.widgets.layout).then(function(data) {
                                    
                                    
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card')
                                    $scope.fd.localVars.gridOptionsResourceData.data = $scope.TransformDataForCard(data.data);
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                    $scope.fd.localVars.nextPage = data.data.next;
                                    $scope.fd.localVars.previousPage = data.data.previous;
                                    $scope.fd.localVars.totalCount=data.data.count;
                                    $scope.fd.localVars.currentPage=newPage;
                                    if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.currentPage==0) $scope.fd.localVars.currentPage=1;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/pageSize);
                                    $scope.fd.localVars.pageSize = pageSize;
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLinkDatasetList',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.PresentationId+"Completed",$scope.fd.localVars.gridOptionsResourceData.data);
                                   
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.fd.localVars.resourceDataDownloading = false;
                             });
         }
         else if ($scope.fd.PopulateMethod=='A')
         {
          var task_instance=0;  
          if ($scope.fd.localVars.formType=='i') task_instance = $scope.taskInitiateId;
          else task_instance = $scope.taskExecuteId;
           $scope.fd.localVars.resourceDataDownloading = true;
          ResourcesDataService.getResourceDataPaginatedAuto($scope.fd.localVars.selectedResource,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.PresentationId,task_instance,$scope.fd.localVars.formType).then(function(data) {
                                      
                                    $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                                    $scope.fd.localVars.restoreGridState();
                                    $scope.fd.initialLoad = false;
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false;
                                    $scope.fd.localVars.nextPage = data.data.next;
                                    $scope.fd.localVars.previousPage = data.data.previous;
                                    $scope.fd.localVars.totalCount=data.data.count;
                                    $scope.fd.localVars.currentPage=newPage;
                                    $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/pageSize);
                                    $scope.fd.localVars.pageSize = pageSize;
                                    $scope.fd.localVars.gridOptionsResourceData.totalItems=$scope.fd.localVars.totalCount;
                                    
                                    WidgetDataExchangeService.setData({
                                    DEI: [{name:'resourceLink',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.selectedDatasetRecordId}
                                    ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.fd.localVars.gridOptionsResourceData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
            
                                    $rootScope.$broadcast("StartTask"+$scope.fd.PresentationId+"Completed",$scope.fd.localVars.gridOptionsResourceData.data);
                                   
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.fd.localVars.resourceDataDownloading = false;
                             });
          
         }
        };


    $scope.fd.localVars.pageChanged = function(){ 
        $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.currentPage,$scope.fd.localVars.pageSize);
         };
         
   //**********************************************************
	//           GET RESOURCE COLUMNS
	//**********************************************************   

   
  $scope.fd.localVars.getResourceColumns = function (getData) {
    
    return
    
     var lang = $rootScope.globals.currentUser.lang;
     if ($scope.fd.localVars.selectedResource==undefined) $scope.fd.localVars.selectedResource = $scope.fd.Related;
     
     if ($scope.fd.localVars.selectedResource!=undefined && $scope.fd.localVars.selectedResource!=null)
     {
           taskId = $scope.taskId;
           if ($scope.fd.PresentationId==undefined || $scope.fd.PresentationId==null || $scope.fd.PresentationId==0)
           {
              var taskId = 0;        
           }
      $scope.fd.localVars.resourceDataDownloading = true;

     ResourcesDataService.getResourceColumns($scope.fd.localVars.selectedResource,taskId,$scope.fd.localVars.formType,lang).then(function(data) {
                                
                $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data;
                if ($scope.fd.localVars.currentPage==undefined) $scope.fd.localVars.currentPage=1;
                if (getData && !$scope.formDefinition) $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,1,$scope.fd.localVars.pageSize);               
                else
                {
                      $scope.fd.localVars.restoreGridState();
                      $scope.fd.initialLoad = false;
                      $scope.fd.localVars.resourceDataDownloaded = true;
                      $scope.fd.localVars.resourceDataDownloading = false;
                }
                 },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.fd.localVars.resourceDataDownloading = false;
                             });
               
        }
    };  
     
     
        $scope.fd.localVars.getDefaultColumnsDef = function () {
        
            ResourcesDataService. getResourceColumns($scope.fd.Parameters.PublishedResourceId,'dataset', $rootScope.globals.currentUser.lang ,$scope.fd.Parameters.PublishedResourceVersion).then(function(data) {//MIGOR
                                
                                    $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.data.columnsDef
                                    $scope.fd.localVars.results.columns =  $scope.fd.localVars.gridOptionsResourceData.columnDefs
                                    $scope.fd.localVars.resourceDataDownloaded = true;
                                    
                                    $scope.fd.localVars.saveGridState(false)
                                    
                                         
                            },function(error){
                                    MessagingService.addMessage(error.msg,'error');
            });

      }
      
      
     $scope.fd.localVars.getResourceDataFiltered = function () {

            $scope.filter = {};
            var lang = $rootScope.globals.currentUser.lang;

         
          if ($scope.fd.localVars.selectedResource==null) {
            $scope.fd.localVars.selectedResource = $scope.fd.Related;
          }
          $scope.fd.localVars.resourceDataDownloading = true;
          
         ResourcesDataService.getResourceDataFiltered($scope.fd.localVars.selectedResource,$scope.taskId,$scope.fd.localVars.formType,lang,$scope.fd.localVars.dashboard.data).then(function(data) {//MIGOR
                            
                         $scope.fd.localVars.gridOptionsResourceData.data = data.data;
                         $scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef;
                         
                           $timeout(function(){
                             $scope.fd.initialLoad = false;
                             $scope.fd.localVars.resourceDataDownloaded = true;
                             $scope.fd.localVars.resourceDataDownloading = false;
                             $scope.fd.localVars.restoreGridState();
                           });
                        
                            
                         },function(error){
                       MessagingService.addMessage(error.msg,'error');
                       $scope.fd.localVars.resourceDataDownloading = false;
                    });
        };
		
       $scope.fd.localVars.setField = function(form,name,value,msg)
        {
     				TaskDataService.setField(form,name,value);
            if (msg) MessagingService.addMessage(msg,'info');
        };
        
          $scope.fd.localVars.showHideSearchForm = function()
        {
     				if ($scope.fd.ShowSearchForm) $scope.fd.localVars.getFormResource();
        };
       
       
     
     $scope.fd.localVars.getFieldNamesFromObject = function (obj, fieldsList)   // find FieldNames in given Dashboard object recursively 
     {// fieldsList  is return variable (used for multiple select)
        //debugger
        var o = {};
        Object.keys(obj).forEach(function(key) {
          
            if ( (typeof(obj[key]) === 'object') && obj[key]    && key != 'NameField')  { //Do NOT parse 'NameField'  (we need it as it is)
              $scope.fd.localVars.getFieldNamesFromObject(obj[key], fieldsList)
            } 
            else {          
                    if (key == 'FieldName'  ) {  
                        //debugger
                        o[key]= obj[key];
                        }
                  
                    if (key == 'PresentationId' ) {
                        o['id']= obj[key];
                    }    
                    if (key == 'Label' ) {
                        o['name']= obj[key];
                    }    
                }
            }) ; 
        
        if ( o['FieldName'] && Object.keys(o).length > 0 ) fieldsList.push(o)   
    }; 

  
    //TODO - uzeti iz dataset liste
    $scope.fd.localVars.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData = WidgetDataExchangeService.getPrevTaskData($scope.prevTaskExeId);
            var datasetId=0,datasetRecordId=0;
            let action;
            for (let k=0;k<pTaskData.length;k++)
            {
                if (pTaskData[k].DEI!=undefined && pTaskData[k].DEI!=null)
                {
                for (var i=0;i<pTaskData[k].DEI.length;i++)
                {
                  if (pTaskData[k].DEI[i].name==linkType)
                  {
                    if (pTaskData[k].DEI[i].value!=undefined && pTaskData[k].DEI[i].value!=null)
                    {
                      //populate selected item
                     
                      for (var j=0;j<pTaskData[k].DEI[i].value.length;j++)
                      {
                       if (pTaskData[k].DEI[i].value[j].name=="resourceId" && pTaskData[k].DEI[i].value[j].value!=undefined) datasetId=pTaskData[k].DEI[i].value[j].value;
                       if (pTaskData[k].DEI[i].value[j].name=="resourceRecordId" && pTaskData[k].DEI[i].value[j].value!=undefined)
                       {
                            datasetRecordId=pTaskData[k].DEI[i].value[j].value;
                            if ($scope.fd.Parameters.PresentationType == 'dropdown' && datasetId==$scope.fd.Related && !$scope.fd.Multiline)
                            {
                        
                                $scope.itemValue.val = pTaskData[k].DEI[i].value[j].value;
                                action = $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.itemValue.val);
                            }
                       }
                       if (pTaskData[k].DEI[i].value[j].name=="resourceRecordIds" && pTaskData[k].DEI[i].value[j].value!=undefined)
                       {
                            datasetRecordId=pTaskData[k].DEI[i].value[j].value;
                            if ($scope.fd.Parameters.PresentationType == 'dropdown' && datasetId==$scope.fd.Related && $scope.fd.Multiline)
                            {
                        
                                $scope.itemValue.val = pTaskData[k].DEI[i].value[j].value;
                                action = $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.itemValue.val);
                            }
                       }
                        
                      }
                         
                        
                    }
                      
                  }
                    
                  }
                }
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };


    $scope.fd.localVars.previousTaskData = $scope.fd.localVars.getPreviousTaskWidget();
       
       

  $scope.fd.localVars.saveGridState= function(show_message) {
               //debugger //ktTaskDatasetList smarttable
               let columnsDef = $scope.fd.localVars.gridOptionsResourceData.columnDefs

               let columnsQuery = $scope.fd.localVars.columnsForQuery
               $scope.fd['GridState']={'columnsDef':columnsDef,'columnsQuery': columnsQuery}
                                
              if (show_message)
               MessagingService.addMessage("To make changes stick please save form",'info');            
    };
  
  
         if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
        var lang=$rootScope.globals.currentUser.lang;
        
                  
        ResourcesDataService.GetPublishedResourcesByType('dataset','form').then(
            function (data) {  
                if ($scope.fd.localVars===undefined) $scope.fd.localVars={}     
               $scope.fd.localVars.publishedResourceList = data.data;
               //debugger  //GetPublishedResourcesByType   
                            
               if ($scope.fd.Parameters.PublishedResourceId!=null) 
                                            $scope.fd.localVars.setSelectedDataset($scope.fd.Parameters.PublishedResourceId,true);   
               },function(error){    
                              MessagingService.addMessage(error.msg,'error');
         });
    

      
       ResourcesDataService.getResourceDefListByType($rootScope.globals.currentUser.lang,'file').then(
                        function(data) {			
							$scope.fd.localVars.listOfFileTypes = data.data;
                         }
                        ,function(error){   MessagingService.addMessage(error.msg,'error')	}
            );
            
        //POCINJE

        if ($scope.fd.Related!=null &&  !$scope.fd.localVars.formDefinition && !$scope.fd.Parameters.DownloadAllRecords) {
                  
                  if (!$scope.fd.localVars.loadFromState && !$scope.fd.Parameters.AutoSearchClient && $scope.fd.AutoSearch) 
                        $scope.fd.localVars.executeDatasetList();
                    
                  else if  ($scope.fd.localVars.loadFromState || ($scope.fd.localVars.searchFormDownloaded && $scope.fd.Parameters.AutoSearchClient))
                  {
                    if ($scope.fd.Parameters.PresentationType=='table') $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);
                    if ($scope.fd.Parameters.PresentationType=='dropdown')
                    {
                        $scope.fd.localVars.getDatasetDataList(true,$scope.fd.Parameters.ClientSideFilter);
                    }
                    if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
                  
                  }
                  else if  (!$scope.fd.localVars.searchFormDownloaded && $scope.fd.Parameters.AutoSearchClient)
                  {
                    $scope.fd.localVars.showHideSearchForm();  
                  }
                  
        }else if ($scope.fd.Related!=null  &&  !$scope.fd.localVars.formDefinition && !$scope.fd.localVars.searchFormDownloaded && $scope.fd.Parameters.DownloadAllRecords  && !$scope.fd.Parameters.AutoSearchClient)
                {
                  if ($scope.fd.Parameters.PresentationType=='table') $scope.fd.localVars.getResourceDataPaginated($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);
                  if ($scope.fd.Parameters.PresentationType=='dropdown')
                  {
                    $scope.fd.localVars.getDatasetDataList(true,$scope.fd.Parameters.ClientSideFilter);
                  }
                  if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
                
                }
        else if ($scope.fd.Related!=null  && ($scope.fd.localVars.datasetMapperType==2 || $scope.fd.Parameters.AutoSearchClient))
        {
           if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.getCardLayout();
          
        }else if($scope.fd.Related!=null && $scope.fd.localVars.datasetMapperType!=5)
        {
           $scope.fd.localVars.restoreGridState();
           $scope.fd.localVars.showHideSearchForm();
    
        }
                  
      
  
         if ($scope.fd.localVars.formDefinition && $scope.fd.Related>0) $scope.fd.localVars.getFormFields(); 
        //ZAVRSAVA  
   // $scope.fd.localVars.getFormFields();
     // $scope.fd.FormDefSearch?.SearchForm
     
    // if ($scope.fd?.FormDefSearch?.SearchForm)  $scope.fd.localVars.getFieldNamesFromObject($scope.fd.FormDefSearch.SearchForm, $scope.fd.localVars.fieldsList = [])        
  
   $rootScope.$broadcast("Widget"+$scope.fd.PresentationId+"Ready",null);
        
}]; //controler_END

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskViewChart.html',
            controller: controller,
            link: function (scope, elem, attrs) {
             }
             
             
  } 
        
};