'use strict';

export function ktCodesDetailList () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktCodesDetailList.html',
              /*scope:{
                CodeHead : '=',
                },*/
                controller:function($scope,CodesService,MessagingService,TaskDataService,$rootScope,$timeout,dragularService)
                {
                  $scope.showEditCodes = false
                  $scope.showListCodes = false
                  

                  $scope.pokaziModal = function() {
                           $('#exampleModalC').modal('show');         
                  }
                  
                   $scope.$watch('codeId', function (newValue, oldValue, scope) {
                        if (newValue) $scope.pokaziModal();
                        //debugger      
                        
                    });
                    
                  $scope.toggleEditCodes = function() {                      
                        $scope.showEditCodes = !$scope.showEditCodes   
                  }

                  $scope.toggleListCodes = function() {                      
                        $scope.showListCodes = !$scope.showListCodes   
                  }                  
                  
                  
                  $scope.removeCode = function(index){
                          console.log("text")
                          //debugger
                      $scope.codesDetails.splice(index,1);
                  }
                  
                  $scope.unselectRows = function() {
                       
                      //$scope.gridApi1.selection.clearSelectedRows()
                      $scope.codeId=0; 
                      $scope.codeSelected=false;
                      $scope.selectedCode=0
                          
                  }
                  

                 $scope.getCodesDetails = function(codeId,codeName){
                  
                    var lang=$rootScope.globals.currentUser.lang;
                    
                     $scope.codeId = codeId;
                     $scope.codeName = codeName;
                     $('#exampleModalC').modal('show');    
                     
                       
                    if ($scope.codeId>0)  {
                      
                    CodesService.getCodesDetails($scope.codeId,lang).then(function(data) {         
                                   $scope.codesDetails = data.data;
                                   $scope.codesLoaded = true;   
                                    $('#exampleModalC').modal('show');                                       
                              },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                            });
                    } else {
                      
                      $scope.codesDetails.length = 0;
                      $scope.codesLoaded = false;
                    }
                }
                
                
                   $scope.saveCodesDetails = function(){
                  
                        var lang=$rootScope.globals.currentUser.lang;
                        //debugger
                    
                        if ($scope.codeId>0) {
                            CodesService.saveCodesDetails($scope.codeId,$scope.codesDetails,lang).then(
                                function(data) {
                                           MessagingService.addMessage(data.msg,'success');
                                           $scope.codesDetails = data.data;  
                              },function(error){ 
                                          MessagingService.addMessage(error.msg,'error');
                             });
                        } else {  
                          $scope.codesDetails.length = 0;
                        }
                    }
                
    $scope.saveCodeHead = function (isValid) {
        //debugger
          if (isValid) {
                //$scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving code '+ $scope.CodeHead.CodesHeadLang[0].CodeName);
                CodesService.CodesPost($scope.CodeHead)
                        .then(function(data) {
                                   MessagingService.addMessage(data.data.msg,'success');
                              },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                            });                             
          }
        }; 

   

                $scope.addEmptyCodeItem = function(){
                  
                  
                  var lang=$rootScope.globals.currentUser.lang;
                   $scope.codesDetails.push({id:null,Name:null,Value:null,Active:true,CodesDetailLang:[{Value:null,Lang:lang}]});
                }
                
                //$scope.codeChanged({theDirFn: $scope.getCodesDetails});
                
               if ($scope.codeId>0)
                  {
                    $scope.getCodesDetails();
                  
                    if ($scope.codeId==0) $scope.codesLoaded = false;
                  } else $scope.codesLoaded = false;
                  
          },
         
         link: function (scope, elem, attrs) {
              
              
             
            }   
        }
    };