'use strict';



export function ktMassPropertyUpdateSelect2 (KatalyoStateManager, TaskDataService, MessagingService) {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktMassPropertyUpdateSelect2.html',
             controller: function($scope) {     },
             link: function (scope, elem, attrs) 
                {
              
              //To do - get this from server (db table)
            scope.massUpdateFieldList=[
                {id:1, Field:'ReadOnly',Label:'Read only',Component:'checkbox'},
                {id:2, Field:'ShowField',Label:'Show field',Component:'checkbox'},
                {id:3, Field:'Required',Label:'Required',Component:'checkbox'},

            ];
              
            scope.ksm = KatalyoStateManager;
            scope.taskDataService = TaskDataService;
            scope.messagingService = MessagingService;
            scope.localVars.turnAllPropertiesOn = false
            scope.localVars.turnAllPropertiesOff = false
            
            

            let massUpdateState= scope.ksm.addSubject('massUpdateState',null);
            let massUpdateStateName = massUpdateState.subject.name;
            
            
             scope.localVars.setField = function(form,name,value,msg)
            {
                scope.taskDataService.setField(form,name,value);
                if (msg) scope.messagingService.addMessage(msg,'info');
            };

            let activate_mass_update = function(data)
            {
                   scope.active = data.Active
                   massUpdateState.subject.setStateChanges = data;
                   massUpdateState.subject.notifyObservers("massUpdateChanged");
            }
            
            let action_mass_update_changed = scope.ksm.addAction(massUpdateStateName,"massUpdateChanged",activate_mass_update);
             
            
            scope.activateMassUpdateFn = function(item)
              {
                  scope.ksm.executeAction(massUpdateStateName,"massUpdateChanged", {Active:true,Item:item}); 
              }
              
              scope.deActivateMassUpdateFn = function(index)
              {
                  scope.selected = null
                  scope.ksm.executeAction(massUpdateStateName,"massUpdateChanged", {Active:false,Item:null});
                  $('#massPropertyUpdate').modal('hide')
              }
              
              
              scope.hideMassUpdateFn = function()
              { 
                 //debugger
                 $('#massPropertyUpdate').modal('hide')
                 if (scope.localVars.turnAllPropertiesOn) {
                   scope.localVars.setField(scope.inputArray.layout, scope.selected.Field ,  true  , 'All fields are ' + scope.selected.Label   )
                 }
                 if (scope.localVars.turnAllPropertiesOff) {
                   scope.localVars.setField(scope.inputArray.layout, scope.selected.Field ,  false , 'All fields are not' + scope.selected.Label   )
                 }
                 scope.localVars.turnAllPropertiesOn = false
                 scope.localVars.turnAllPropertiesOff = false
                 
                 
              }
              

             }
        }
};