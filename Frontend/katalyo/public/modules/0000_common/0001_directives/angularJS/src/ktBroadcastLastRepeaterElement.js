'use strict';

export function ktBroadcastLastRepeaterElement () {
  
  return{
  
            priority: -1000, // a low number so this directive loads after all other directives have loaded. 
            restrict: "A", // attribute only
            controller: function($rootScope)
            {},
            link: function(scope, elem, attrs) {
              if (scope.$last){
    
                  setTimeout(function(){
                  $rootScope.$broadcast('LastRepeaterElement');
                });
    
      }
    }
  }
};