export function ktFieldBorderProperty () {
    return {
        restrict: 'A',
        templateUrl: 'ktdirectivetemplates/ktFieldBorderProperty.html',
          scope: {
            fd: '=',
        },
        controller: function($scope, $rootScope){
            $scope.CTRL="ktFieldBorderProperty"
             if ($scope.fd.Parameters == undefined || $scope.fd.Parameters == null) $scope.fd.Parameters={};
                        
                       if ($scope.fd.Parameters.border == undefined || $scope.fd.Parameters.border == null )
                       {
                       
                        $scope.fd.Parameters.border = {
                            left: {
                                name: 'Left',
                                style:'solid',
                                width:0,
                                color:"#000000"
                            },
                            right: {
                                name: 'Right',
                                style:'solid',
                                width:0,
                                 color:"#000000"
                            } ,
                            top:{
                                name: 'Top',
                                style:'solid',
                                width:0,
                                color:"#000000"
                            },
                            bottom:{
                                name: 'Bottom',
                                style:'solid',
                                width:0,
                                color:"#000000"
                            }
                        };
                       }
                         
                           
                        if ($scope.fd.Parameters.margin ==  undefined ||  $scope.fd.Parameters.margin ==  null) {
                                $scope.fd.Parameters.margin= {
                                     left: {  
                                        name: 'Left Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },
                                     right: {  
                                        name: 'Right Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },
                                     top: {  
                                        name: 'Top Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },
                                     bottom: {  
                                        name: 'Bottom Margin',
                                        value: 0,
                                        units:"px",
                                        auto: true
                                     },        
                                }
                        }
                        if ($scope.fd.Parameters.padding ==  undefined ||  $scope.fd.Parameters.padding ==  null){
                                $scope.fd.Parameters.padding= {
                                 left: {  
                                    name: 'Left Padding',
                                    value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                 right: {  
                                    name: 'Right Padding',
                                     value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                 top: {  
                                    name: 'Top Padding',
                                   value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                 bottom: {  
                                    name: 'Bottom Padding',
                                    value: 0,
                                    units:"px",
                                    auto: true
                                 },
                                     
                                };
                        }



                    $scope.fd.select = function(selected){
                        $scope.fd.selected=selected;
                    }
                    
                    $scope.fd.selectBorder = function(selected){
                        $scope.fd.selectedBorder=selected;
                    }
              },
        link: function (scope, element, attr) {
          
        
        }
    }
};