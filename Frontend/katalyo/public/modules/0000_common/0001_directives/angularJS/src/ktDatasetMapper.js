'use strict';



export function ktDatasetMapper  () {

        return {
             restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktDatasetMapper.html',
             scope:{
                       fd : "=",
             },
             controller: function($scope,CodesService,ResourcesDataService,GridsterService,$rootScope,ProcessingService,dragularService,$timeout,CODES_CONST,MessagingService)
             {
                $scope.fd.formPriority='e';
              $scope.fd.localVars.src_type_text=CODES_CONST._POPULATE_TYPE_NEW_;
              $scope.fd.RelatedPopulateType=1;
              $scope.gsrvc = GridsterService
              if ($scope.fd.Parameters==undefined) $scope.fd.Parameters ={};              
              if ($scope.fd.PopulateType!=null && $scope.fd.PopulateType!="")
              {
                var pType = $scope.fd.PopulateType;
                var formPriority = pType.toString().substr(1,1);
                $scope.fd.RelatedPopulateType=pType.toString().substr(0,1);
                if (formPriority=="i" || formPriority=="e")
                {
                  
                  $scope.fd.formPriority = formPriority;
                }
              }

              if ($scope.fd.localVars.selectedResourceItem!=undefined)
              {
                $scope.fd.localVars.selectedResourceItem.src_type_text = $scope.fd.localVars.src_type_text[$scope.fd.RelatedPopulateType];
              }else
              {
                $scope.fd.localVars.selectedResourceItem = {src_type_text:$scope.fd.localVars.src_type_text[$scope.fd.RelatedPopulateType]};
              }
              
              $scope.fd.localVars.selectedResource=[];
              if ($scope.fd.localVars.selectedResourceItem != undefined)
              {
                 $scope.fd.localVars.selectedResource.push($scope.fd.localVars.selectedResourceItem);
                
              }


        // MIGOR DODAO 2.6.2021- PROVJERITI -POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)

        $scope.fd.localVars.getResources = function(resource_type)  {	
                
                if (resource_type==undefined) resource_type = $scope.fd.Parameters.SelectedResourceType
                if ($scope.fd.localVars.resourceList==undefined) $scope.fd.localVars.resourceList=[];
                var lang=$rootScope.globals.currentUser.lang;
                ResourcesDataService.getResourceDefListByType(lang,resource_type).then(
                    function(data) {
                           $scope.fd.localVars.resourceList = data.data;
                            if ($scope.fd.Related!=null) {
                            $scope.fd.localVars.setSelectedDataset($scope.fd.Related);
                            }     
                    },function(error){    
                            MessagingService.addMessage(error.msg,'error');
                           });
					}
                              
				  
                  
                  
 
        $scope.fd.localVars.GetPublishedResourcesByType = function(resource_type)  {	
                
                if (resource_type==undefined) resource_type = $scope.fd.Parameters.SelectedResourceType
                if ($scope.fd.localVars.publishedResourceList==undefined) $scope.fd.localVars.publishedResourceList=[];
                var lang=$rootScope.globals.currentUser.lang;
                
                // we are in build mode, so we should invalidate cahce (useCache is false)  TODO - provjeriti da li treba korisiti cache
                ResourcesDataService.GetPublishedResourcesByType(resource_type,'form', false).then(function (data) { 
                        //debugger   //GetPublishedResourcesByType  
              
                        $timeout(function(){
                            $scope.fd.localVars.publishedResourceList.length = 0;  
                            angular.extend($scope.fd.localVars.publishedResourceList, data.data);
                            //$scope.fd.localVars.publishedResourceList = data.data;
                 
                  
                            if ($scope.fd.Parameters.PublishedResourceId!=null) {
                                $scope.fd.localVars.setSelectedDataset($scope.fd.Parameters.PublishedResourceId);
                            }  
                        
                         })
                 },function(error){    
                            MessagingService.addMessage(error.msg,'error');
                  });
               } 
    
    
     
    
        if ($scope.fd.localVars.publishedResourceList==undefined || $scope.fd.localVars.publishedResourceList.length==0) {
                $scope.fd.localVars.GetPublishedResourcesByType();
        }
             
       $scope.fd.localVars.changePriority= function(fp){ 
             $scope.fd.formPriority =fp; 
            };
         
          
         $scope.fd.localVars.confirm= function(){
               
               $timeout(function(){
                   $scope.fd.Parameters.Analytics = [];   // REMOVE OLD PARAMETERS  (Analytics....)
                   
                   $scope.fd.localVars.selectedResourceItem.src_type_text = $scope.fd.localVars.src_type_text[$scope.fd.RelatedPopulateType];
                   $scope.fd.PopulateType=$scope.fd.RelatedPopulateType+$scope.fd.formPriority;
                   $scope.fd.localVars.selectedResourceItem_c=$scope.fd.localVars.selectedResourceItem; 
                   $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem_c.name +' ('+$scope.fd.localVars.selectedResourceItem_c.id +')'
                   $scope.fd.localVars.gridOptionsResourceData = {} //dataset is changed and gridOptions should be reset?? 
                   //$scope.fd.localVars.isCollapsedMapper = !$scope.fd.localVars.isCollapsedMapper;
                   $scope.fd.localVars.datasetMapperType=0;
                   $scope.fd.GridState = {}
                   $scope.fd.localVars.setSelectedDataset($scope.fd.localVars.selectedResourceItem.id);
     
               });
               
             //  $scope.localVars.toggleProperties(0); //MIGOR dodao da se sakrije property kad stisne prozor , TODO - treba potvrditi da se nesto s time ne kvari
            };
				  
              
              
              $scope.fd.localVars.onSelectDatasetCallback = function (item, model){

                  	 var eventResultId=item.id;
                   	 var eventResultName=item.name;
                     var eventResultParentElementId =item.parentElementId;
                     if ( item.id!=$scope.fd.localVars.selectedResourceItem.id && $scope.fd.localVars.relatedPopulateType!=1 )
                     {
                            $scope.fd.localVars.getFormWidgets(item.id,$scope.fd.RelatedPopulateType);      
                      }
                    
                	  $scope.fd.localVars.selectedResourceItem = {id:eventResultId, name : eventResultName,parentElementId:eventResultParentElementId,Version:item.Version,ResourceDefId:item.ResourceDefId};
                    
                    
              }
            
             $scope.fd.localVars.setSelectedSource = function(source_id){
             
                  if (source_id==-1)
                  {
                    
                    $scope.fd.RelatedPopulateType = '3';
                    $scope.fd.formPriority = 'e';
                    
                  }else
                  {
                   
                    $scope.fd.RelatedPopulateType = '2';
                    $scope.fd.formPriority = source_id.form_type;
                    $scope.fd.SrcWidgetId = source_id.parentElementId;
                    if (source_id.type=='rlist') $scope.fd.RelatedPopulateType = '4';
                    
                  }
             };
              
             $scope.fd.localVars.getFormWidgets = function(resource_id){
          

                  var lang=$rootScope.globals.currentUser.lang;
                  if ($scope.fd.PresentationId == undefined || $scope.fd.PresentationId==null) $scope.fd.PresentationId=0;
                  let formVersion = $scope.gsrvc.getTaskFormVersion($scope.fd.localVars.taskId+$scope.fd.localVars.formType)
                  if (formVersion!=undefined)
                  {
                    ResourcesDataService.getFormWidgets(resource_id,$scope.fd.localVars.taskId,$scope.fd.UuidRef,1,lang,formVersion).then(function(data) {
                               
                             
                     
                        $scope.fd.localVars.currentTaskInitiateDatasets = data.data.initiate_widgets;
                        
                        $scope.fd.localVars.currentTaskExecuteDatasets = data.data.execute_widgets;
                      
                        $scope.fd.localVars.currentDownloaded = true;
                      
                      
                      
                        $scope.fd.localVars.previousTaskInitiateDatasets = data.data.initiate_widgets;
                        $scope.fd.localVars.previousTaskExecuteDatasets = data.data.execute_widgets;
                     
                        $scope.fd.localVars.previousDownloaded = true;
                      
                      
                      
                         },function(error){
                           MessagingService.addMessage(error.msg,'error');
                    });
                }else MessagingService.addMessage('Error - Form version is undefined in dataset mapper directive (getFormWidgets function)','error');
             };
           
           
              $scope.fd.localVars.setPopulateMethod= function(populateMethod){
                        
                       $scope.fd.PopulateMethod=populateMethod;
                       
                       if (populateMethod=='A')
                        {
                          if ($scope.fd.localVars.selectedResourceItem_c!=undefined)
                          {
                                 var id=$scope.fd.localVars.selectedResourceItem_c.id;
                                 if (id==undefined) id=$scope.fd.localVars.selectedResourceItem_c.relatedId;
                                 if (id!=undefined) $scope.fd.localVars.getFormWidgets($scope.fd.localVars.selectedResourceItem_c.id);
                          }
                          
                        }
                  };
                  
              $scope.fd.localVars.setPopulateMethod($scope.fd.PopulateMethod);
              
              
              $scope.fd.localVars.setPopulateType= function(populateType){
                        
                       $scope.fd.RelatedPopulateType=populateType;
                       
                          if ($scope.fd.localVars.selectedResourceItem_c!=undefined)
                          {
                            $scope.fd.localVars.getFormWidgets($scope.fd.localVars.selectedResourceItem_c.relatedId);
                          }
                          
                        
                  }
          
               },
             link: function (scope, elem, attrs) {
                       
             
             }
        }
};