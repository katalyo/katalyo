'use strict';

export function ktTaskIntegrationWidget () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','KatalyoStateManager','UtilityService','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,KatalyoStateManager,UtilityService,$templateCache,$compile,$element) {
     
      $scope.gsrvc = GridsterService;
      var lang=$rootScope.globals.currentUser.lang;
      $scope.localVars.widgetType = 5;
      $scope.fd.PopulateType = "4";
      $scope.localVars.formParams={formViewType:1,showGridNavigation:true};
      if ($scope.formData==undefined) $scope.formData={};
      if ($scope.formData[$scope.fd.PresentationId]==undefined) $scope.formData[$scope.fd.PresentationId]=[{}];
      
      $scope.localVars.datasetMapperType=0;
           
      $scope.localVars.gridsterType="resource";
          // $scope.gridsterOpts = GridsterService.getGridsterOptions();
    
    
        
      $scope.localVars.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: []
      } ;
      
       if ($scope.localVars == undefined) $scope.localVars={};
                
        $scope.localVars.showMenu = false
      
        $scope.localVars.toggleDropdown = function(){
                                       $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
         }
        
    
  
      //$scope.taskId=$stateParams.id;
      $scope.taskId=$scope.resourceDefinition.ResourceDefId;
      if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
      if ($scope.taskId==undefined) $scope.taskId=0;
      $scope.localVars.taskId=$scope.taskId;
      $scope.fd.ParentDatasetDefId=$scope.fd.Related;
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.localVars.formType=$scope.resourceDefinition.form_type;
      $scope.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.formDefinition = $scope.resourceDefinition.form_definition;
      
       if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
      $scope.localVars.resourceRecordId=0;
      if ($scope.taskInitiateId==undefined)
      {
        $scope.taskInitiateId =0;
      
      }
      if ($scope.taskExecuteId==undefined)
      {
         $scope.taskExecuteId =0;  
      }
        if ($scope.prevTaskExeId==undefined)
        {
            $scope.prevTaskExeId =0;
        }
        
      //register widget 
      var out_fields = ['resourceId'];
      //if ($scope.fd.LocalId==undefined || $scope.fd.LocalId==null || $scope.fd.LocalId==0)
      //{
         var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'viewResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.localVars.selectedResource, output:out_fields, };
         $scope.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
         $scope.localVars.LocalId=$scope.localVars.localWidgetId;
         $scope.fd.LocalId=$scope.localVars.localWidgetId;
         //$scope.fd.LocalId=UtilityService.uuidNow();
         
      //}
      $scope.ksm = KatalyoStateManager;
      
      let dependent_field_pid = $scope.fd.LocalId;
        let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
        let dependent_field_pid2
        let update_presentation_id2 = function(data) {
            let presentation_id = angular.copy($scope.fd.PresentationId)
            if (data[presentation_id]!==undefined)
            {
                $scope.fd.PresentationId = data[presentation_id];
            //unsubscribe here later after implementing unsubscribe - ToDo
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            }
        }
        let update_presentation_id = function(data) {
            if (data[$scope.fd.LocalId]>0 && $scope.fd.PresentationId==0 && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
          
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            //unsubscribe here later after implementing unsubscribe - ToDo
        }
        
        
        if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0)
        {
            let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
        } else{
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
        }
             
      
       let apply_value = function(data)
        {
            let do_refresh=false;
            if (data!=undefined)
            {
               if (data != $scope.localVars.resourceRecordId)
               {
                  if (typeof data ==='number') $scope.localVars.resourceRecordId= data;
                  else if (Array.isArray(data))
                  {
                     if (data.length>0)
                     {
                      $scope.localVars.resourceRecordId= data[0].id  
                     }
                  }else
                  {
                     $scope.localVars.resourceRecordId = data.id;
                  }
                  do_refresh = true;
               }
            }
            //get selectedRecord
            if ($scope.localVars.resourceRecordId == null) $scope.localVars.resourceRecordId=0;
            
            if ($scope.localVars.resourceRecordId>0 && do_refresh) $scope.localVars.getFormResourceWithData();
            
            
        };
        
      
      if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
      
      if ($scope.fd.Parameters.RefreshOnIdChange)
      {
         
         //find 
         //subscribe observer
           
         let dependent_field = "Field"+$scope.fd.PresentationId;
         let subscribedSubject = "integrationValueStateName"+$scope.fd.Parameters.ResourceIdItemId+$scope.resourceDefinition.transaction_id;
         let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
         let val = $scope.ksm.getState(subscribedSubject);
         $scope.localVars.resourceRecordId= val.id;
            
            //get selectedRecord
         if ($scope.localVars.resourceRecordId == null) $scope.localVars.resourceRecordId=0;
            
         if ($scope.localVars.resourceRecordId>0) $scope.localVars.getFormResourceWithData();
      }
      
      
      //subscribe observer
      if ($scope.fd?.PopulateType?.toString().substr(0,1)=="4")
      {
           /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
       
       
        if ($scope.fd.SrcWidgetId!=undefined && $scope.fd.SrcWidgetId!=null)
        
        {
           
                let dependent_field = "Field"+$scope.fd.SrcWidgetId;
               // let subscribedSubject = $scope.fd.Parameters.SrcWidgetType+"ValueStateName"+$scope.fd.SrcWidgetId;
                let subscribedSubject = "integrationValueStateName"+$scope.fd.SrcWidgetId+$scope.resourceDefinition.transaction_id;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            
        }  
         
      }
      
        // MIGOR DODAO 2.6.2021- PROVJERITI -POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
        $scope.fd.getResources = function()  {				
                if ($scope.localVars.resourceList==undefined) $scope.localVars.resourceList=[];
                var lang=$rootScope.globals.currentUser.lang;
                ResourcesDataService.getResourceDefListByType(lang,'integration').then(
                    function(data) {
                           $scope.localVars.resourceList = data.data;
                           $scope.localVars.resourcesLoaded = true;
                           $scope.localVars.setSelectedIntegration($scope.fd.Related);
                    },function(error){    
                            MessagingService.addMessage(error.msg,'error');
               });
			}
                                 
      $scope.fd.getResources();
      let refresh_widget_data = function()
      
      {
         $scope.localVars.getFormResourceWithData();    
         
      }
           
      $scope.localVars.showProperties=function(datasetMapperType)
      {
            
            if ($scope.localVars.datasetMapperType==undefined)
            {
               $scope.localVars.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.localVars.datasetMapperType==0 || $scope.localVars.datasetMapperType!=datasetMapperType) $scope.localVars.datasetMapperType = datasetMapperType;
               else $scope.localVars.datasetMapperType=0;
            }
            
      };
           
      $scope.localVars.setField = function(form,name,value,msg)
      {
         TaskDataService.setField(form,name,value);
         if (msg) MessagingService.addMessage(msg,'info');
      };  
      
       $scope.localVars.confirmIntegration = function()
      {
       //  $scope.localVars.selectedResourceItem.src_type_text = $scope.localVars.src_type_text[$scope.fd.RelatedPopulateType];
         $scope.localVars.name_confirmed = $scope.localVars.selectedResourceItem.name +' ('+$scope.localVars.selectedResourceItem.id +')'
         //$scope.fd.PopulateType=$scope.fd.RelatedPopulateType+$scope.fd.formPriority;
      
         $scope.fd.Related=$scope.localVars.selectedResourceItem.id;
         $scope.localVars.getFormResource();
         $scope.localVars.integrationConfirmed = true;
         $timeout(function(){
            $scope.localVars.integrationConfirmed = false;
         },2000);
      };  

      
      $scope.localVars.getFormResourceOriginal = function(){
              if ($scope.fd.PresentationId==null)
              {
                $scope.fd.PresentationId = 0;
                
              }
               let datasetId = $scope.localVars.datasetFormParams.Parameters.dataset.id;
              ResourcesDataService.getResourceForm(datasetId,"i",lang).then(function(data) {
                            
                                $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                if (Array.isArray(data.data.widgets)) $scope.localVars.dashboard.widgets.layout =  data.data.widgets;
                                
                                $scope.fd.wItemValue=data.data.widgets;
                                
                       
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.ResourceRecordId}]}]});
						
                                  $scope.localVars.searchFormDownloaded = true;
                                  $scope.localVars.formLoaded=true;
                                
                                
                               // $scope.localVars.setField($scope.localVars.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVars.setField($scope.localVars.dashboard.widgets.layout,'Required',false);     
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
        
        $scope.localVars.getFormResource = function(){
              var lang=$rootScope.globals.currentUser.lang;
               $scope.localVars.formLoaded=false;
               if ($scope.fd.PresentationId==null)
               {
                    $scope.fd.PresentationId=0;
            
               }
               $scope.localVars.formLoading=true;
                
                let datasetId;
                //if ($scope.fd.Parameters.FormSaved)
                datasetId = $scope.fd.Related;
                //else  datasetId= $scope.localVars.datasetFormParams.Parameters.dataset.id;
              ResourcesDataService.getResourceFormExtended(datasetId,$scope.taskId,$scope.localVars.formType,$scope.fd.PresentationId,"w",lang).then(function(data) {
                               
                                  $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.localVars.dashboard.widgets.layout =  data.data.widgets;
                               
                                $scope.fd.wItemValue=data.data.widgets;
                               
                                
                                  
                                $scope.localVars.searchFormDownloaded = true;
                                
                                $timeout(function(){    
                                   $scope.localVars.formLoaded=true;
                                   $scope.localVars.formLoading=false;
                                });
                                  
                               
                              //  $scope.localVars.setField($scope.localVars.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVars.setField($scope.localVars.dashboard.widgets.layout,'Required',false);
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 $scope.localVars.formLoading=false;
                                 
                                
                             });
                              
      };
      
              
        $scope.localVars.getFormResourceWithData = function(){
              var lang=$rootScope.globals.currentUser.lang;
              $scope.localVars.formLoaded=false;
              if ($scope.fd.PresentationId==null || $scope.fd.PresentationId==undefined)
              {
                $scope.fd.PresentationId = 0;
                
              }
              
              if ($scope.fd.ParentElementId==null || $scope.fd.ParentElementId==undefined)
              {
                $scope.fd.ParentElementId = 0;
                
              }
              
              
              
              
               if ($scope.localVars.resourceRecordId==undefined || $scope.localVars.resourceRecordId==0)
               {
                
                  if ($scope.localVars.previousTaskData['datasetRecordId']!=undefined && $scope.localVars.previousTaskData['datasetRecordId']!="" && $scope.localVars.previousTaskData['datasetId']==$scope.fd.Related) $scope.localVars.resourceRecordId = $scope.localVars.previousTaskData['datasetRecordId'];
                
               }
               if (typeof $scope.localVars.resourceRecordId !=='number' && typeof $scope.localVars.resourceRecordId !=='string')
               {
                  
                  if (Array.isArray($scope.localVars.resourceRecordId))
                  {
                     if ($scope.localVars.resourceRecordId.length>0)
                     {
                        $scope.localVars.resourceRecordId= $scope.localVars.resourceRecordId[0].id  
                     
                     }
                  }
                  else $scope.localVars.resourceRecordId = $scope.localVars.resourceRecordId.id;                 
               }
               //let datasetId = $scope.localVars.datasetFormParams.Parameters.dataset.id;
               let datasetId = $scope.fd.Related;
               
              ResourcesDataService.getResourceFormExtendedWithData(datasetId,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.PopulateType,$scope.fd.ParentElementId,$scope.fd.PresentationId,$scope.prevTaskExeId,$scope.localVars.formType,$scope.localVars.widgetType,$scope.localVars.resourceRecordId,"w",lang).then(function(data) {

                                  $scope.formData[$scope.fd.PresentationId] =  data.data.form_data;
                                  if ($scope.formData[$scope.fd.PresentationId]==undefined) $scope.formData[$scope.fd.PresentationId]=[];
                                  $scope.localVars.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.localVars.dashboard.widgets.layout =  data.data.widgets;
                                 
                                  if ($scope.formData[$scope.fd.PresentationId].length>0)
                                  {
                                    $scope.localVars.resourceRecordId = $scope.formData[$scope.fd.PresentationId][0]['id'];
                                  }
                                  $scope.localVars.datasetRecordId=$scope.localVars.resourceRecordId;
                                  
                                  $scope.localVars.formLoaded=true;
                                    
                                 $scope.localVars.formLoading=false;
                                 $scope.localVars.searchFormDownloaded = true;
                                      
                               
                                   
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - VIEW_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVars.resourceRecordId}]}]});
                                
                               // $scope.localVars.setField($scope.localVars.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVars.setField($scope.localVars.dashboard.widgets.layout,'Required',false);   
                                },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.localVars.formLoaded=true;
                                    
                                    $scope.localVars.formLoading=false;
                                    $scope.localVars.searchFormDownloaded = true;
                             });
                              
      };
      
       $scope.localVars.callApi = function () {
         let apiEndPoint = $scope.localVars.datasetFormParams.Parameters.APIEndpoint;
         let method = $scope.localVars.datasetFormParams.Parameters.APIMethod;
         let form_data = $scope.formData[$scope.fd.PresentationId][0];
         ResourcesDataService.CallApiGeneric(apiEndPoint,method,form_data).then(function(data){
               MessagingService.addMessage(data.data.msg,'success');
            
            },function(error){
                                   MessagingService.addMessage(error.data.msg,'error');
                                
            });
       }
      $scope.getDatasetParams = function () {
            
            
            ResourcesDataService.getResourceDefAll($scope.fd.Related).then(function(data) {
                               
                             // MessagingService.addMessage(data.msg,'success');
                              $scope.localVars.datasetFormParams = data.data;
                              
                              if (!$scope.formDefinition && !$scope.localVars.searchFormDownloaded)
                                 {
                                     
                                     $scope.localVars.getFormResourceWithData();
                                   
                                 }else
                                 {
                                    $scope.localVars.getFormResource();
                                   
                                 } 
                               
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };      
      
           $scope.localVars.setSelectedIntegration = function(id)  {
              
              if ($scope.localVars.selectedResourceItem==undefined) $scope.localVars.selectedResourceItem={};
              var pos = $scope.localVars.resourceList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.localVars.selectedResourceItem = $scope.localVars.resourceList[pos];

                $scope.localVars.name_confirmed = $scope.localVars.selectedResourceItem.name +' ('+$scope.localVars.selectedResourceItem.id +')';
                
                }
					}
  

$scope.localVars.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData;
      var pTaskDataRet = WidgetDataExchangeService.getPrevTaskData($scope.prevTaskExeId);
      if (Array.isArray(pTaskDataRet) && pTaskDataRet.length>0) pTaskData = pTaskDataRet[0];
            var datasetId=0,datasetRecordId=0;
            if (pTaskData!=undefined)
            {
               if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
               {
               for (var i=0;i<pTaskData.DEI.length;i++)
               {
                 if (pTaskData.DEI[i].name==linkType)
                 {
                   if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                   {
                     for (var j=0;j<pTaskData.DEI[i].value.length;j++)
                     {
                      if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined) datasetId=pTaskData.DEI[i].value[j].value;
                      if (pTaskData.DEI[i].value[j].name=="resourceRecordId" && pTaskData.DEI[i].value[j].value!=undefined) datasetRecordId=pTaskData.DEI[i].value[j].value;
                      
                       
                     }
                        
                       
                   }
                     
                 }
                   
                 }
               }
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };


   
   
  $scope.localVars.previousTaskData=$scope.localVars.getPreviousTaskWidget("resourceLinkDatasetList");
 
  if ($scope.fd.Related!=null) $scope.getDatasetParams();
 
            let template;
            if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskIntegrationWidget.html');
            else template = $templateCache.get('ktdirectivetemplates/ktTaskIntegrationWidget.html');
          
            $element.append($compile(template)($scope)); 

  
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

            }     
  }
  
};