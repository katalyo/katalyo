'use strict';

export function ktStartTaskWidget ($rootScope,$stateParams,GridsterService,ResourcesDataService,TaskDataService,MessagingService,WidgetDataExchangeService,$timeout,$cookies) {

        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktStartTaskWidget.html',
            controller: function(UserService,ResourcesDataService,$rootScope,ProcessingService,$timeout,$stateParams,$cookies,WidgetDataExchangeService,$state,TaskDataService,MessagingService,GridsterService){},
             link: function (scope, elem, attrs) {
                 
                var lang=$rootScope.globals.currentUser.lang;
                scope.taskListParameters = {placeholder:'Select or search task...',label:scope.fd.Label,name:'task',
                  selectedItem:null,hideLabel:false,columnSizeLabel:'col-lg-4',dropdownSize:'col-lg-8',onSelectFunction:''};
               
                scope.initiateFormLoaded=false;
                scope.isSaving=false;
                
                scope.previousTask=$stateParams.prev_task_execute_id;
                scope.previousTaskOutcomeId=$stateParams.outcome_id;
                scope.previousTaskPackage=$stateParams.prev_task_package; 
                scope.gridsterType="task";
                //scope.gridsterOpts = GridsterService.getGridsterOptions();
                scope.showSearchFilters=false;      
                scope.dashboard = {widgets : []};
                scope.taskDefId=scope.fd.Related;          
                
                scope.fromStateName=$stateParams.fromStateName;
                scope.fromStateParams=$stateParams.fromStateParams;
                scope.cookieFromStateName=$cookies.getObject('initiateTaskFromStateName');
                scope.cookieFromStateParams=$cookies.getObject('initiateTaskFromStateParams');
                scope.noBackbutton = false;
                scope.backBtnFromCookie=false;
      
      
  
                scope.removeField = function (index){
						
                  if (index > -1) {
						 
                 scope.inputArray.layout.splice(index, 1);
						 
                  }

                }   

 
 
    
          scope.InititateAllList = function(){
      
            
              ResourcesDataService.getResourceDefByType(lang,'task').then(function(data) {
                                
                                  scope.gridOptions.data = data.data;
                                   
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
          }
        
    
    //napuni grid s definicijama resursa
   // $scope.InititateAllList();
                 
     scope.InititateMyList = function(){
      
         
              ResourcesDataService.getResourceDefListByLangByTypeUser(lang,'task').then(function(data) {
                                
                                 scope.gridOptions.data = data.data;
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
    
          
    };   
    
      scope.TaskSelectionChanged = function(item,model){
      
       scope.taskListParameters.selectedItem=item;
       scope.fd.Related = scope.taskListParameters.selectedItem.id;
       scope.SelectAnyTask();
  
     };
     
     scope.SelectAnyTask = function(){
      
      if (scope.fd.Search.startAnyTask)
          {
            
            scope.taskDefId=scope.taskListParameters.selectedItem.id;   
          //  scope.InititateMyList(); 
          }
          else
          {
            scope.taskDefId = scope.fd.Related;
          }
          
     };
      
     
      if (scope.fromStateName==undefined || scope.fromStateName=="")
      {
          if (scope.cookieFromStateName==undefined || scope.cookieFromStateName=="")
          {
            scope.noBackButton = true;
          }else
          {
            scope.fromStateName =  scope.cookieFromStateName;
            scope.backBtnFromCookie = true;
          }
          
        
      }else{
        $cookies.putObject('initiateTaskFromStateName',scope.fromStateName);
        
      }
      
      if (scope.fromStateParams==undefined)
      {
          if (scope.cookieFromStateParams!=undefined)
          {
              if (scope.backBtnFromCookie)
              {
                scope.fromStateParams =  scope.cookieFromStateParams;
              }
          }
          
      }else if (scope.cookieFromStateParams==undefined)
      {
        $cookies.putObject('initiateTaskFromStateParams',scope.fromStateParams);
        
      }else
      {
          if (scope.backBtnFromCookie)
          {
        
            scope.fromStateParams =  scope.cookieFromStateParams;
        
          }else
          {
              $cookies.putObject('initiateTaskFromStateParams',scope.fromStateParams);    
          }
      }
      
      
       if (scope.previousTask==undefined || scope.previousTask=="")
          {
           // $state.go();
            
            scope.previousTask = 0;
            
          }
      scope.potentialOwners = {};
      

      
    //WidgetDataExchangeService.deRegisterAllWidgets();	//Migor - cistim taskwidgetRegistry 

      scope.localTaskId=WidgetDataExchangeService.registerTask(scope.taskDefId);
     
     if  (scope.fd.localVars===undefined)  scope.fd.localVars={} 
      scope.fd.localVars.taskDefinition ={};
      scope.potentialOwners.potentialOwnersUsers = [];
      scope.potentialOwners.potentialOwnersGroups = [];
      
         scope.FormLockUnlock = function()
      {
        scope.resourceDefinition.formParams.formLocked=!scope.resourceDefinition.formParams.formLocked;

     
      };
      
      scope.PreInitiateTask = function(){
           var lang=$rootScope.globals.currentUser.lang;
          scope.taskInitiateId=0;
          scope.taskExecuteId=0;
          
         
        TaskDataService.preInitiateTask(scope.taskDefId,scope.previousTask,lang).then(function(data) {
                                
                                   
                                   scope.fd.localVars.taskDefinition = data.data.taskdef[0];
                                   scope.fd.localVars.taskDefinition.prev_task_id = scope.previousTask;
                                   scope.fd.localVars.taskDefinition.form_type="i";
                                   scope.fd.localVars.taskDefinition.source_type = $stateParams.src_type;
                                   scope.fd.localVars.taskDefinition.taskFormLoaded = false;
                                   if (scope.fd.localVars.taskDefinition.AutoInitiate) {
                                  
                                       scope.AutoInitiateTask();
                                                                    
                                  }else
                                      {
                                       scope.resourceDefinition=data.data.resourceDefinition;
                                       scope.dashboard.widgets.layout = data.data.widgets;
                                       scope.resourceDefinition.formParams = {formLocked:true,formViewType:2};
                                      
                                      $timeout(function(){scope.initiateFormLoaded = true;});
                      
                                      }
                                     
     
                              },function(error){
                                
                            
                                  MessagingService.addMessage(error.msg,'error');
                            });
      };
      
      scope.CancelClicked = function(){
        
        $state.go( scope.fromStateName, scope.fromStateParams);
      
      };
      
       scope.AutoInitiateTask = function(){
        var lang=$rootScope.globals.currentUser.lang;
        //  if (isValid) {
        var parentDataset=scope.getPreviousTaskWidget();
        if (scope.previousTaskOutcomeId==undefined) scope.previousTaskOutcomeId=0;
          
          TaskDataService.initiateTask(scope.taskDefId,scope.previousTask,scope.previousTaskOutcomeId,[],scope.potentialOwners,parentDataset['datasetId'],parentDataset['datasetRecordId'],lang).then(function(data) {
                                
                                    
                                    scope.task_initiate_id = data.data.task_initiate_id;
                                    scope.task_execute_id = data.data.task_execute_id;
                                    //$state.go('app.menu.tasks.execute',{id: scope.taskDefId,task_initiate_id:scope.task_initiate_id,task_execute_id:scope.task_execute_id,fromStateName:scope.fromStateName,fromStateParams:scope.fromStateParams,prev_task_package:scope.previousTaskPackage,prev:scope.taskExecuteId});
                                   
                                  
                              },function(error){
                                    
                                  MessagingService.addMessage(error.msg,'error');
                            });
         // }
      }
      
       scope.InitiateTask = function(form){
        var f = scope.forma;
        if (form.$submitted) scope.setSubmitted(form);
        if (form.$valid) {
            
            //var initiateData={};
            $rootScope.$broadcast('ProcessTaskWidgets',{}); //svi widgeti slusaju ovaj event
            scope.isSaving = true;
            var lang=$rootScope.globals.currentUser.lang;
            if (scope.previousTaskOutcomeId==undefined) scope.previousTaskOutcomeId=0;
            //ako je prethodni task imao listu povuci WidgetData
            var parentDataset=scope.getPreviousTaskWidget();
                    
                TaskDataService.initiateTask(scope.taskDefId,scope.previousTask,scope.previousTaskOutcomeId,scope.dashboard.widgets.layout,scope.potentialOwners,parentDataset['datasetId'],parentDataset['datasetRecordId'],lang).then(function(data) {
                                
					
                                  MessagingService.addMessage(data.data.msg,'success');
                                  $rootScope.$broadcast('InitiateTaskPostProcess',data.data.ret_value);
                                  //ovo treba staviti u event InitiateTaskPostProcessFinished                        
                                  scope.fd.GridState.cancelBtnIcon = "fa fa-arrow-circle-left";
                                  scope.fd.GridState.cancelBtnTxt = "Go back";
                                  scope.taskInitiated=true;
                                 
     
                              },function(error){
                                 // $scope.isSaving = false;    
                                  MessagingService.addMessage(error.msg,'error');
                            });
                          
                          scope.isSaving = false;  
						
			
							
          } // if (isValid)
          else
          {
            MessagingService.addMessage("Please enter required data",'warning');
            
          }
      };

    scope.getPreviousTaskWidget  = function()
    {
      var pTaskData = WidgetDataExchangeService.getPrevTaskData(scope.fd.localVars.taskDefinition.prev_task_id);
            var datasetId=0,datasetRecordId=0;
            if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
            {
            for (var i=0;i<pTaskData.DEI.length;i++)
            {
              if (pTaskData.DEI[i].name=="resourceLink")
              {
                if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                {
                  for (var j=0;j<pTaskData.DEI[i].value.length;j++)
                  {
                   if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined) datasetId=pTaskData.DEI[i].value[j].value;
                   if (pTaskData.DEI[i].value[j].name=="resourceRecordId" && pTaskData.DEI[i].value[j].value!=undefined) datasetRecordId=pTaskData.DEI[i].value[j].value;
                   
                    
                  }
                     
                    
                }
                  
              }
                
              }
              if (datasetId==undefined || datasetId=="") datasetId=0;
              if (datasetRecordId==undefined || datasetRecordId=="") datasetRecordId=0;
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };
        scope.setSubmitted = function (form) {
          form.$setSubmitted();
          angular.forEach(form, function(item) {
            if(item && item.$$parentForm === form && item.$setSubmitted) {
              scope.setSubmitted(item);
            }
          });
        } 
      //get form   
    
     
                 
                ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
                                                    
                                  scope.taskList = data.data;
                                    var pos = scope.taskList.map(function(e) { return e.id; }).indexOf(scope.fd.Related);
              
                                   if (pos>=0){
                                        scope.taskListParameters.selectedItem =  scope.taskList[pos];
                                          }                             
                                
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
      
      if (scope.fd.Search===undefined || scope.fd.Search===null)         scope.fd.Search = {startAnyTask:false};  
      if (scope.taskDefId!=undefined && scope.taskDefId>0 && !scope.fd.Search.startAnyTask) scope.PreInitiateTask();
      else if (scope.fd.Search.startAnyTask)  $rootScope.$broadcast('PageLoaded',{});             
              }
        }             
};