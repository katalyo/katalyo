'use strict';

export function ktCodesDropdown  () {
          
          var controller = ['$scope', '$stateParams','$state','CodesService','MessagingService','TaskDataService','$rootScope','$timeout','dragularService',function ($scope,$stateParams,$state,CodesService,MessagingService,TaskDataService,$rootScope,$timeout,dragularService) {
          
                  $scope.GetCodesByName = function()
                  {
                     var lang=$rootScope.globals.currentUser.lang;
                  
                     CodesService.getCodesByName($scope.codeName,lang).then(function(data) {
                             $scope.codeList = data.data;
                           
                             var pos;
                             if ($scope.selectedCodeId!=undefined)
                             {
                              pos = $scope.codeList.map(function(e) { return e.id; }).indexOf($scope.selectedCodeId);
                             }
                             
                             /*
                              if (scope.selectedCodeValue!=undefined)
                             {
                              pos = scope.codeList.map(function(e) { return e.value; }).indexOf(scope.selectedCodeValue);
                             } */ 
                             if (pos>-1) {
                              
                              $scope.selectedCode = $scope.codeList[pos];
                                        
                              }
                                      
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
       
                  };
                  
                   $scope.GetCodes= function()
                  {
                     var lang=$rootScope.globals.currentUser.lang;
                  
                     CodesService.getCodesByName($scope.codeName,lang).then(function(data) {
                             $scope.codeList = data.data;
       
                                        
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
       
                  };
        
                  $scope.GetCodesClick = function(){
                    //scope.task.TaskOutcomeCodeId = null;
                    scope.GetCodes();
                      };
                  $scope.GetCodesByNameClick = function(){
                    //scope.task.TaskOutcomeCodeId = null;
                    $scope.GetCodesByName();
                      };
                 
                   if (!!$scope.codeName)
                    {
                      $scope.GetCodesByName();
                    }
                else{
            
                      $scope.GetCodes();
                    }
          
          
          }];
        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktCodesDropdown.html',
              scope:{
                //codeList: '=',
                selectedCodeId: '=',
                codeId: "=",
                codeName: "@",
                valueLength:"@",
                labelLength:"@",
                name:"@",
                label:"@",
                searchText:"@",
                required:"=",
                sizeXs: "=",
                //errorText:"@",
                
                
                },
             controller:controller,
             link: function (scope, elem, attrs) {
              
                  if (scope.selectedCode == undefined) scope.selectedCode=null;
                  
                 /*
                  scope.$watch('selectedCodeId',function(newValue, oldValue){
                    
                        if (scope.codeList!=undefined)
                        {
                             var pos = scope.codeList.map(function(e) { return e.id; }).indexOf(scope.selectedCodeId);
                             if (pos>-1) {
                              
                              scope.selectedCode = scope.codeList[pos];
                                        
                              }
                    
                        }
                    });
                    */
                  scope.clear = function(event,selected){
                    
                   scope.selectedCode = null;
                    
                    
                  };
                  
                   scope.onSelectUniqueCallback = function (item, model){
                      
                      if (item!=undefined)
                      {
                      scope.selectedCode = item;
                      scope.selectedCodeId = item.id;
                      
                      }else {
                        
                        scope.selectedCode = null;
                        scope.selectedCodeId=null;
                      }
                    };
                           
        }
      }
    };