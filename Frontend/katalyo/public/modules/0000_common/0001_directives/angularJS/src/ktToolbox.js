'use strict';

export function ktToolbox () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktToolbox.html',
             /* scope:{
                	 form : '=',
            },*/
             controller: ['$scope','dragulaService','$rootScope','GridsterService','KatalyoStateManager',
          
            function($scope,dragulaService,$rootScope,GridsterService,KatalyoStateManager){
               
                $scope.gsrvc = GridsterService;      
                var widgetHandle = 'element-drag-handle';
                $scope.collapsed={};
                var setupDragula = $scope.gsrvc.setupDragula('bag-dataset','dataset-toolbox',widgetHandle);
                $scope.toolboxItems= $scope.gsrvc.getToolboxItems();
                 //register to state manager
                        $scope.ksm = KatalyoStateManager;

                        let toolbox_loaded = function(data)
                        {
                            $scope.datasetToolboxLoaded(data.state);
                        };
                        
                         let toolbox_reload = function(data)
                        {
                          $scope.datasetToolboxLoaded(data);
                        };
                        
                        let toolboxState= $scope.ksm.addSubject("datasetToolBox",null);
                        let toolboxStateName = toolboxState.subject.name;

                        let action_toolbox_loaded = $scope.ksm.addAction(toolboxStateName,"toolboxLoaded",toolbox_loaded);
                        
                        let action_toolbox_reload = $scope.ksm.addAction(toolboxStateName,"toolboxReload",toolbox_reload);
                        
                        $scope.setCollapse=function(value,element_type){
                            if (element_type=="toolboxgroup")
                            {
                                if ($scope.collapsed[value]===undefined) $scope.collapsed[value] = true
                                else
                                {
                                    if ($scope.collapsed[value]) $scope.collapsed[value] = false
                                    else $scope.collapsed[value] = true;
                                }
                            }
                        }
                        
                        $scope.datasetToolboxLoaded=function(value){
                                
                                
                                if (!value)
                                {
                                    $scope.gsrvc.destroyDragula('bag-dataset');
                                   
                                }
                                else
                                {
                                    $scope.toolboxItems= $scope.gsrvc.getToolboxItems();
                                   
                                }
                            }
               
                        
            
            }],
            
             link: function (scope, elem, attrs) {

              
              
        }
    }
};