'use strict';

export function ktTextbox () {
        return {
            restrict: 'A',
            controller: function ($scope,$rootScope,GridsterService,KatalyoStateManager,CodesService,$templateCache,$compile,$element) {
                        
                if ($scope.localVars==undefined) $scope.localVars={};
                $scope.fd.localVars={};
                $scope[$scope.fd.id]  = {'fd': $scope.fd, 'localvars':$scope.fd.localVars}
                
                $scope.localVars.showBasic = true;
                $scope.localVars.showSize = true;
                $scope.localVars.showStyle = true;
                $scope.localVars.showGeneral = true;
                $scope.localVars.showAdvanced = true;
                 
                
                $scope.gsrvc = GridsterService;
                $scope.toggleMe = true;
                $scope.ksm = KatalyoStateManager;
                var lang=$rootScope.globals.currentUser.lang;
                CodesService.getCodesByName('usertags',lang).then(function (data) {
                       //debugger // CodesService.getCodesByName
                        $scope.fd.tagList = data.data;
                        
                         var pos = $scope.fd.tagList.map(function(e) { return e.id; }).indexOf($scope.fd.UniqueId);

                           if (pos>=0){
                               $scope.fd.UniqueIdObj=$scope.fd.tagList[pos];
                              
                           }
                     });
                
                $scope.searchFilter={};
                $scope.searchFilter[$scope.fd.PresentationId] = {};
                $scope.fd.localVars.search=[
                    {id:1,filter:'begins',code_name:'begins',name:"Begins with",icon:"fa fa-chevron-right"},
                    {id:2,filter:'contains',code_name:'contains',name:"Contains",icon:"fa fa-expand"},
                    {id:3,filter:'ends',code_name:'ends',name:"Ends with",icon:"fa fa-chevron-left"},
                    {id:4,filter:'eq',code_name:'eq',name:"Equals",icon:"fa fa-bars"} ];
                
                $scope.fd.localVars.processFilters = function(filterList)
                {
                    for (let i=0;i< $scope[$scope.fd.id]['fd'].localVars.search.length;i++)
                    {
                        let pos = filterList.map(function(e) { return e.id; }).indexOf( $scope[$scope.fd.id]['fd'].localVars.search[i].code_name);
                        if (pos>=0)  $scope[$scope.fd.id]['fd'].localVars.search[i].name = filterList[pos].name
                    }
                   
                    
                    if ($scope.fd.Search===undefined || $scope.fd.Search===null)   $scope.fd.Search= $scope[$scope.fd.id]['fd'].localVars.search[1];//initialize to equals
                    if ($scope.fd.Search)  $scope.fd.Search.Value=$scope.formData[0][$scope.fd.FieldName]; 

                    $scope[$scope.fd.id]['fd'].localVars.filterReady=true
                }

                if ($scope.showSearchFilters || $scope.resourceDefinition.form_definition)
                {
                    CodesService.getCodesByName('search_filters',lang).then(function (data) {
                                //debugger;
                                try {
                                    //$scope.fd.localVars.processFilters(data.data)
                                    $scope[$scope.fd.id]['fd'].localVars.processFilters(data.data) 
                                    //$scope.fd.localVars = $scope[$scope.fd.id]['fd'].localVars
                                } catch (err) { 
                                    console.log(err)
                                    debugger
                                }                                
                    });
                }
                if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
                let textValue = null;
                if ($scope.formData!=undefined)
                {
                        if ($scope.formData.length==0) $scope.formData=[{}];
                        textValue=$scope.formData[0][$scope.fd.FieldName]
                        
                }else $scope.formData=[{}];
                
                let textValueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+'-textboxValueStateName'+$scope.fd.PresentationId,textValue);
                let textValueStateName = textValueState.subject.name;
                
                
                let value_changed = function(data)
                {
                    textValueState.subject.setStateChanges = data;
                    textValueState.subject.notifyObservers("valueChanged");
                    $scope.localVars.refreshSearchValue();
                };
                
                let action_value_changed = $scope.ksm.addAction(textValueStateName,"valueChanged",value_changed);
               
                $scope.on_blur_action=function()
                {
                        
                        $scope.ksm.executeAction(textValueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                        //$scope.fd.DefaultValue = $scope.formData[0][$scope.fd.FieldName];
                };
               
                
                
                let apply_value = function(data) {
                        if (data!=undefined) $scope.formData[0][$scope.fd.FieldName] = data;
                };
                
                if ($scope.fd?.Parameters?.DependentFieldId!=undefined && $scope.fd?.Parameters?.DependentFieldId!=null)   
                {
                    if ($scope.fd.Parameters.DependentFieldId!="")
                    {
                        let dependent_field = "Field"+$scope.fd.PresentationId;
                        let subscribedSubject = $scope.resourceDefinition.page_state_id+'-'+$scope.fd.Parameters.DependentFieldType+"ValueStateName"+$scope.fd.Parameters.DependentFieldId;
                        let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
                        let state = $scope.ksm.getState(subscribedSubject);
                        if (state.status=="ok") $scope.formData[0][$scope.fd.FieldName] = state.state;
                    }
                }
                
                $scope.localVars.refreshSearchValue = function() {
                    if ($scope.fd.Search) $scope.fd.Search.Value=$scope.formData[0][$scope.fd.FieldName];      
                };
                
                $scope.fd.SetDefault = function()
                {
                    $scope.fd.DefaultValue = {value:$scope.formData[0][$scope.fd.FieldName]};
                }
             
                $scope.fd.onSelectUnique = function(item,model)
                {    
                    $scope.fd.UniqueId=item.id;
                }
               
                let template;
                if ($scope.resourceDefinition.form_definition)  template = $templateCache.get('ktdirectivetemplates/ktTextbox.html');
                else template = $templateCache.get('ktdirectivetemplates/ktTextboxExe.html');
              
                $element.append($compile(template)($scope));  
                
                if (textValueState.subject.currentStateIndex>=0 && $scope.fd.Parameters.LoadFromState)
                {
                       
                        $scope.formData[0][$scope.fd.FieldName] = textValueState.subject.getState;
            
                }   
                
                if ($scope.formData[0][$scope.fd.FieldName]!=undefined) $scope.ksm.executeAction(textValueStateName,"valueChanged", $scope.formData[0][$scope.fd.FieldName]);
                
            },
       
            link: function (scope, elem, attrs) {
				 
                scope.fd.colLength=[];
                for (var i=0;i<24;i++) scope.fd.colLength.push('col-lg-'+(i+1));
                
                                
                scope.applyTextFilters = function(index) {
                    scope.fd.Search=scope.fd.localVars.search[index];
                    if (scope.fd.Search && scope.formData!=undefined && scope.formData.length>0) {
                            scope.fd.Search.Value=scope.formData[0][scope.fd.FieldName]; 
                    }
                    if (scope.fd.Search.Value==undefined) scope.fd.Search.Value = "";
                };

                
             
                scope.showProperties = function(event) {
                    scope.gsrvc.setFieldProperties(scope.fd,event);
                };
                
               if (scope.fd.UpdateWithDefault && !scope.fd.NoUpdate) scope.formData[0][scope.fd.FieldName] = scope.fd.DefaultValue?.value

                
                
              
            }
        };
};