'use strict';

export function ktDatasetToDatasetMapper () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktDatasetToDatasetMapper.html',
             controller: function ($scope,CodesService,ResourcesDataService,GridsterService,$rootScope,ProcessingService,dragulaService,$timeout,CODES_CONST,MessagingService){
                 $scope.gsrvc = GridsterService; 
                  $scope.CTRL='ktDatasetToDatasetMapper'
                  $scope.fd.DatasetMapping = []; 
              
              //$scope.fd.selectedPackages=$scope.fd.DatasetMapping;
             
              $scope.dataset1Fields = [];
              $scope.dataset2Fields = [];
             
              //if ($scope.fd.selectedPackages==undefined) $scope.fd.selectedPackages=[];
             
            /*
            $scope.localVars.confirmDataset2Dataset= function(){
               
               $timeout(function(){
               
                //$scope.fd.selectedPackages =$scope.localVars.gridApi.selection.getSelectedRows();
                
                if ($scope.fd.selectedPackages!=undefined)
               {
                $scope.fd.DatasetMapping = [];
                  for (var i=0;i<$scope.fd.selectedPackages.length;i++)
                  {
                    $scope.fd.DatasetMapping.push($scope.fd.selectedPackages[i])
                      
                  
                  }
               }

               });
               
            };
            */
  
    //setup dragula
    $scope.setupDragula = function()
    {
         var widgetHandle = "element-drag-handle";
         var bag = dragulaService.find($rootScope, 'bag-dataset-mapping');
         if (bag) dragulaService.destroy($rootScope, 'bag-dataset-mapping');
         
         dragulaService.options($rootScope, 'bag-dataset-mapping', {
                       revertOnSpill: true,
                       removeOnSpill: false,
                       copy:function (el,source){
                        
                        return source.id=='dataset1-mapping';
                       
                        },
                       //direction:'vertical',
                       accepts:function (el,target, source) {
                          return target.id!='dataset1-mapping';
                       },
                       moves: function (el, container, handle) {
                                 return handle.classList.contains(widgetHandle);
                       }
                   });     
              
    };
        
        // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
		$scope.localVars.getDatasets = function()  {
			var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
						function(data) {			
							$scope.localVars.datasetList = data.data;
                            $scope.localVars.datasetLoaded=true;
                            //console.log($scope.datasetList);
				 
							},function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					};
     $scope.getDatasetMappings = function (datasetId) {
    
         var lang = $rootScope.globals.currentUser.lang;
         
         ResourcesDataService.getDatasetMappings(datasetId,lang).then(function(data) {
                               
                             $scope.dataset2Fields = data.data;
                             
                             $scope.setupDragula();
                             $scope.dataset1Loaded=true;
                             
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };   

     $scope.getResourceFields = function (item,type) {
    
         var lang = $rootScope.globals.currentUser.lang;
         
         ResourcesDataService.getResourceFields(item.Dataset1.id,lang).then(function(data) {
                               
                             
                              if (type==1){
                                  $scope.dataset1Fields = data.data;

                               }
         
                              else{
                                $scope.dataset2Fields = data.data;

                                }
                               
                                $scope.getDatasetMappings(item.id);
                                
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };
        
    $scope.localVars.showMappingDetailsFn = function (item) {
        $scope.dataset1Loaded=false;
         if ($scope.dataset1Fields!=undefined) $scope.dataset1Fields.length=0;
         if ($scope.dataset2Fields!=undefined) $scope.dataset2Fields.length=0;
        
        $scope.selectedDataset1 = {id:item.Dataset1.id,name: item.Dataset1.resourcedeflang_set[0].Name};
        $scope.selectedDataset2 = {id:item.Dataset2.id,name: item.Dataset2.resourcedeflang_set[0].Name};
        $scope.getResourceFields(item,1);
        
        $scope.tmpMappingFields={}
        $scope.tmpMappingFields.dataset1=$scope.selectedDataset1;
        $scope.tmpMappingFields.dataset2=$scope.selectedDataset2;
        $scope.tmpMappingFields.mappingType= {id:item.MappingType.id,name:item.MappingType.Value,value:item.MappingType.Name};  
        $scope.tmpMappingFields.mappingDisplayName=item.DatasetMappingHeadLang_datasetmappingheadid[0].Name
        $scope.tmpMappingFields.mappingName=item.MappingName;
       
        $scope.localVars.showMappingData(item);
    
    };
        $scope.localVars.showMappingData = function (selectedResource) {
         
        $scope.localVars.showMappingDetails=true;
        $scope.localVars.showMappingName=true;
          
         if (selectedResource==false || selectedResource==undefined) 
          {
             $scope.mappingFields={};
             $scope.saveDatasetMappingHeadStatus='createMapping';
          } else  
          {
              $scope.saveDatasetMappingHeadStatus='updateMapping';
              $scope.mappingFields= $scope.tmpMappingFields;
          }
          
         $scope.localVars.getDatasets();//puni datasetList
         var lang = $rootScope.globals.currentUser.lang;
         CodesService.getCodesByName('dmt',lang).then(function (data) {
                                $scope.localVars.codeList = data.data;
                                $scope.localVars.mappingTypeLoaded = true;
            }); 
        };
        
        $scope.removeSelectedMappings=  function()
        {
            let pos;
            if ($scope.fd?.DatasetMapping)
                for (let i=0;i<$scope.fd.DatasetMapping.length;i++)
                {
                    
                    pos  = $scope.mappingList.map(function(e) { return e.id; }).indexOf($scope.fd.DatasetMapping[i].id);
                    
                    $scope.mappingList.splice(pos,1);
                }
        }
        $scope.removeItem=  function(pindex,index)
        {
            $scope.dataset2Fields[pindex].fields.splice(index,1);
        };
        $scope.cancelMapping = function () {
            $scope.saveDatasetMappingHeadStatus='CancelOrInit';
            $scope.localVars.showMappingDetails=false;
            $scope.localVars.showMappingName=false;
        };
        $scope.localVars.getMappingPackages = function () {
    
            var lang = $rootScope.globals.currentUser.lang;
            var fromDs = 0;
            var toDs = $scope.localVars.selectedResourceItem?.id;
        
            ResourcesDataService.getMappingPackages(fromDs,toDs,lang).then(function(data) {
               
                $scope.mappingList = data.data;
                $scope.removeSelectedMappings();
                $scope.mappingsLoaded=true;
                
            },function(error){
                  
                MessagingService.addMessage(error.msg,'error');  
            });
        };
         $scope.localVars.getMappingPackages();   
    },

    link: function (scope, elem, attrs) {
             
    }
  }      
};