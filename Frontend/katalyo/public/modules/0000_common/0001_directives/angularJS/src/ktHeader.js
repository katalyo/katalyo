'use strict';

export function ktHeader () {
        return {
            restrict: 'A',
			 controller: function ($scope) {},
             templateUrl: 'ktdirectivetemplates/ktHeader.html',
             controller:function($scope,KatalyoStateManager,WidgetDataExchangeService)
             {  
                
                if ($scope.fd.localVars===undefined) $scope.fd.localVars={}
                var widgetDefinition = {name: 'header',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId};
                $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
                $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
                if ($scope.fd.localVars == undefined ) 
                $scope.fd.localVars = {showMenu: false }
                
                $scope.fd.localVars.PropertyElements=['ktFieldBorderProperty.html']
               

                 $scope.fd.localVars.taskId=$scope.resourceDefinition.ResourceDefId;
                if ($scope.fd.localVars.taskId==undefined)  $scope.fd.localVars.taskId=$scope.resourceDefinition.ResourceDefId_id;
                if ($scope.fd.localVars.taskId==undefined) $scope.fd.localVars.taskId=0;
        

                $scope.fd.localVars.toggleDropdown = function(){
                           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                }
        
        
                $scope.ksm = KatalyoStateManager;
                        let update_presentation_id = function(data)
                        {
                           if (data[$scope.fd.LocalId]>0 && ($scope.fd.PresentationId==null || $scope.fd.PresentationId==0)) $scope.fd.PresentationId = data[$scope.fd.LocalId];
                          //unsubscribe here later after implementing unsubscribe - ToDo
                          
                        }
                        if ($scope.fd.PresentationId==0 || $scope.fd.PresentationId==null)
                        {
                         
                            let dependent_field = $scope.fd.LocalId;
                            let subscribedSubject = "formDefinitionState"+$scope.fd.localVars.taskId+$scope.resourceDefinition.form_type;
                            let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"formSaved",update_presentation_id);
                  
                        }


                },
             link: function (scope, elem, attrs) {
              
                if (scope.fd==undefined) scope.fd={};
                scope.fd.colLength=[];
                for (let i=0;i<24;i++) scope.fd.colLength.push('col-lg-'+(i+1));

              
                if (scope.fd.Parameters==undefined) scope.fd.Parameters={};
                if (scope.fd.Parameters.BackgroundOpacity==null || scope.fd.Parameters.BackgroundOpacity==undefined ) scope.fd.Parameters.BackgroundOpacity=1;
                scope.fd.BackgroundColorChanged = function (){
                        
                if (scope.fd.Parameters.BackgroundColor.length>0) scope.fd.Parameters.BackgroundColorA = "rgba(" + parseInt(scope.fd.Parameters.BackgroundColor.slice(-6, -4), 16) + "," + parseInt(scope.fd.Parameters.BackgroundColor.slice(-4, -2), 16) + "," + parseInt(scope.fd.Parameters.BackgroundColor.slice(-2), 16) + "," + scope.fd.Parameters.BackgroundOpacity + ")";
       
                 };


            }
        };
};