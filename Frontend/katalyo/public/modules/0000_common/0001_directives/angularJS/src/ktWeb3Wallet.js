'use strict';

export function ktWeb3Wallet () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','KatalyoStateManager','UtilityService','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,KatalyoStateManager,UtilityService,$templateCache,$compile,$element) {
         
        if ($scope.localVars == undefined) $scope.localVars={};
                
        $scope.localVars.showMenu = false
      
        $scope.localVars.toggleDropdown = function(){
                                       $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
         }
         
         $scope.fd.copyToClipboard = function(text){
            var input = document.getElementById("web3"+$scope.fd.PresentationId);
            input.value = text;
            input.focus();
            input.select();
            document.execCommand('copy');
            $scope.fd.copyAddress = true;
            $timeout(function(){$scope.fd.copyAddress = false;},1000);
          }
         
         
         $scope.fd.linkWeb3Wallet = function()
         {
                $scope.fd.connectingToWallet = true;
                $scope.fd.web3Connected = false;
                $scope.fd.eth_address=null;
                if (window.ethereum) {
                  $scope.fd.web3Exists = true;
                  var web3 = new Web3(window.ethereum);
                                    
                  ethereum.send('eth_requestAccounts').then(function (result) {
                      $timeout(function(){
                           $scope.fd.eth_address = result.result[0];
                           $scope.fd.connectingToWallet = false;
                           if ($scope.fd.eth_address!=null)
                           {
                              $scope.fd.web3Connected = true;
                              web3.eth.net.getNetworkType().then(function(result){$scope.fd.connected_network=result});
                           /*
                              var wei;
                           
                              web3.eth.getBalance($scope.eth_address, function (error, wei) {
                                 if (!error) {
                                    var balance = web3.utils.fromWei(wei, 'ether');
                                    $scope.balance = balance + " ETH";
                                     $scope.fd.balanceChecked = true;
                                 }
                              });
                              */
                           
                           }else  $scope.fd.web3Connected = false;
                      });
                  },function(error){
                        $timeout(function(){
                           $scope.fd.connectingToWallet = false;
                           $scope.fd.web3Connected = false;
                           $scope.fd.error_msg = error.message;
                           
                        });
                  });
                          
                 
                }else                $scope.fd.web3Exists = false;
         }     
        
        // $scope.fd.linkWeb3Wallet();
         
         
      // Unpkg imports
      const Web3Modal = window.Web3Modal?.default;
      const WalletConnectProvider = window.WalletConnectProvider?.default;
      const EvmChains = window.evmChains;
      const Fortmatic = window.Fortmatic;
      
      // Web3modal instance
      var web3Modal;
      
      // Chosen wallet provider given by the dialog window
      var provider;
      
      /**
       * Setup the orchestra
       */
      $scope.fd.init = function () {
         
         
        console.log("WalletConnectProvider is", WalletConnectProvider);
        console.log("Fortmatic is", Fortmatic);
      
        // Tell Web3modal what providers we have available.
        // Built-in web browser provider (only one can exist as a time)
        // like MetaMask, Brave or Opera is added automatically by Web3modal
        const providerOptions = {
          walletconnect: {
            package: WalletConnectProvider,
            options: {
              // Ktlyo key
              infuraId: "d5619a41905a41f88c5e742e4180ef3c",
            }
          },
      
          /* add later ILJ
          fortmatic: {
            package: Fortmatic,
            options: {
              // Mikko's TESTNET api key
              key: "pk_test_391E26A3B43A3350"
            }
          }
          */
        };
        if (Web3Modal!==undefined)
        {
          web3Modal = new Web3Modal({
            cacheProvider: false, // optional
            providerOptions, // required
          });
        }else $scope.fd.error_msg='Error connecting to web3 provider'
      }


      /**
       * Kick in the UI action after Web3modal dialog has chosen a provider
       */
      $scope.fd.fetchAccountData= function () {
         
         $scope.fd.connectingToWallet = true;
         $scope.fd.web3Connected = false;
         $rootScope.globals.web3Connected=$scope.fd.web3Connected;
         $scope.fd.eth_address=null;
        // Get a Web3 instance for the wallet
        const web3 = new Web3(provider);
      
        console.log("Web3 instance is", web3);
         window.web3 = web3;
        // Get connected chain id from Ethereum node
        web3.eth.getChainId().then(function(data){
          $scope.fd.chainId = data;
           $scope.fd.chainData=EvmChains.getChain($scope.fd.chainId);//.then(function(data){
           $scope.fd.connected_network = $scope.fd.chainData.name;
           // $scope.fd.chainData = data;
           // });
         },function(error){
                  $scope.fd.connectingToWallet=false;
                  
            });
        // Load chain information over an HTTP API
       
        
        //set chain name
        //document.querySelector("#network-name").textContent = chainData.name;
      
        // Get list of accounts of the connected wallet
        web3.eth.getAccounts().then(function(data){
            let accounts = data;
            // MetaMask does not give you all accounts, only the selected account
            console.log("Got accounts", accounts);
            //select account here
            $timeout(function(){
               if (accounts.length>0)
               {
                  $scope.fd.eth_address = accounts[0];
                  $scope.fd.web3Connected=true;
                  $rootScope.globals.web3Connected=$scope.fd.web3Connected;
               }
               $scope.fd.connectingToWallet=false;
              
               },function(error){
                  $scope.fd.connectingToWallet=false;
                  
            });
         });
      
        
        
      
      
        // Because rendering account does its own RPC commucation
        // with Ethereum node, we do not want to display any results
        // until data for all accounts is loaded
        
        //don't really understand this
        //await Promise.all(rowResolvers);
      
      }



      /**
       * Fetch account data for UI when
       * - User switches accounts in wallet
       * - User switches networks in wallet
       * - User connects wallet initially
       */
      $scope.fd.refreshAccountData = function () {
       
        $scope.fd.fetchAccountData(provider);

      }


      /**
       * Connect wallet button pressed.
       */
      //async function onConnect() {
         
       $scope.fd.connectWeb3Wallet = function(){  
      
        console.log("Opening a dialog", web3Modal);
        try {
          web3Modal.connect().then(function(data){
            provider = data;
            $rootScope.globals.web3Provider=provider;
             // Subscribe to accounts change
            provider.on("accountsChanged", (accounts) => {
              $scope.fd.fetchAccountData();
            });
       
            // Subscribe to chainId change
            provider.on("chainChanged", (chainId) => {
               $scope.fd.chainId = chainId;
              $scope.fd.fetchAccountData();
            });
       
         // Subscribe to networkId change
         provider.on("networkChanged", (networkId) => {
            $scope.fd.networkId=networkId;
           $scope.fd.fetchAccountData();
         });
       
         $scope.fd.refreshAccountData();
       
            });
        } catch(e) {
          console.log("Could not get a wallet connection", e);
          return;
        }

       }

      /**
       * Disconnect wallet button pressed.
       */
      $scope.fd.disconnectWallet = function () {
      
        console.log("Killing the wallet connection", provider);
      
                  // TODO: Which providers have close method?
                  if(provider.close) {
                    provider.close().then(function(data){
                        
                        
                     });
                
                    // If the cached provider is not cleared,
                    // WalletConnect will default to the existing session
                    // and does not allow to re-scan the QR code with a new wallet.
                    // Depending on your use case you may want or want not his behavir.
                   
                  }
                  $scope.fd.web3Connected = false;
                  $rootScope.globals.web3Connected=$scope.fd.web3Connected;
                  $scope.fd.eth_address=null;
                  web3Modal.clearCachedProvider();
                  provider = null;
                  
                }

         $scope.fd.init();
         //$scope.fd.connectWeb3Wallet();  
         let template;
         //if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskViewResource.html');
         //else template = $templateCache.get('ktdirectivetemplates/ktTaskViewResourceExe.html');
         template = $templateCache.get('ktdirectivetemplates/ktWeb3Wallet.html');
         $element.append($compile(template)($scope)); 

   
  
  
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

            }     
  }
  
};