'use strict';

export function ktTaskListWidget () {
             var controller = ['$scope', '$stateParams','$state','UserService','ResourcesDataService','$rootScope','ProcessingService','$timeout','$cookies','CodesService','WidgetDataExchangeService','TaskDataService','MessagingService','GridsterService','KatalyoStateManager','$q','$templateCache','$compile','$element','TaskDefinitionService',
                               function ($scope,$stateParams,$state,UserService,ResourcesDataService,$rootScope,ProcessingService,$timeout,$cookies,CodesService,WidgetDataExchangeService,TaskDataService,MessagingService,GridsterService,KatalyoStateManager,$q,$templateCache,$compile,$element,TaskDefinitionService) {
          
             var lang=$rootScope.globals.currentUser.lang;
             $scope.CTRL = "ktTaskListWidget"   
             if ($scope.localVars==undefined) $scope.localVars = {};                      
             $scope.stateName=$state.current.name;
             $scope.stateParams = $state.current.params;
             $scope.task_initiate_id=$stateParams.task_initiate_id;
             $scope.task_execute_id=$stateParams.task_execute_id;
             $scope.tds=TaskDefinitionService
                             
              $scope.is_superuser =  $rootScope.globals.currentUser.is_superuser;
            
             $scope.gridOptions = {
               data:[],
             enableGridMenu: true,
             //showGridFooter: true,
             //showColumnFooter: true,
             enableRowSelection: true,
             multiSelect: false,
             enableHorizontalScrollbar : 0, 
             enableVerticalScrollbar : 2,
             //enableFiltering: true,
             enableRowHeaderSelection: false,
             enableColumnResizing: true,
             useExternalPagination: true,
             paginationPageSizes: [10, 25, 50,100,200,500,1000],
             enablePaginationControls: false,
             //paginationPageSize: 10,
             //useExternalSorting : true,
             //useExternalFiltering: true,
            // minRowsToShow : 3
            };
                    
                  var paginationOptions = {
                    pageNumber: 1,
                    pageSize: 10,
                    sort: null
                    };
                  
                  $timeout(function(){  
                           $scope.localVars.pageSize =  paginationOptions.pageSize;
                  });
                  $scope.localVars.firstTime = true;
                var columnDefsInitiate = [  
                  { name: 'TaskInitiationId.id', displayName: 'Task Start ID', type: 'number', width:'5%',enableHiding: false, visible: true },
                  { name: 'TaskInitiationId.InitiatorId.name', displayName: 'Started by'},
                  { name: 'TaskInitiationId.Status.CodesDetailLang[0].Value', displayName: 'Task status' },
                  { name: 'TaskInitiationId.Resourcedef.resourcedeflang_set[0].Name', displayName: 'Task name' },
                  { name: 'TaskInitiationId.Resourcedef.resourcedeflang_set[0].Description', displayName: 'Task description' },
                  { name: 'TaskInitiationId.Resourcedef.id', displayName: 'Task def id',enableHiding: false,visible: false },
                  { name: 'TaskInitiationId.CreatedDateTime', displayName: 'Date created',enableHiding: false,type: 'date', cellFilter: 'date:\'dd.MM.yyyy HH:mm:ss:sss\'' },
                  { name: 'TaskInitiationId.CreatedBy.name', displayName: 'Created by'},
                  { name: 'ForGroup.name', displayName: 'For group'},
                  ];
                    
                   var columnDefsExecute = [
                         { name: 'id', displayName: 'Task Execute ID', type: 'number', width:'10%',enableHiding: false, visible: true },
                         { name: 'resourcedef[0].Name', displayName: 'Task name' },
                         { name: 'resourcedef[0].Description', displayName: 'Task description' },
                         { name: 'actualOwner.name', displayName: 'Task Owner'},
                         { name: 'status[0].Value', displayName: 'Task status' },
                        // { name: 'outcome[0].Name', displayName: 'Task outcome' },
                         { name: 'resourcedef[0].id', displayName: 'Task def id',enableHiding: false,visible: false },
                         { name: 'CreatedDateTime', displayName: 'Date created',enableHiding: false,type: 'date', cellFilter: 'date:\'dd.MM.yyyy HH:mm:ss:sss\'' },
                         { name: 'createdBy.name', displayName: 'Created by'},
                         ];   
                   
                    if ($scope.task_execute_id==undefined) $scope.task_execute_id = 0;
                    if ($scope.task_initiate_id==undefined) $scope.task_initiate_id = 0;
                    $scope.localVars.detailsClicked=false;
                    
                                    
                    $scope.localVars.showMenu = false
      
                    $scope.localVars.toggleDropdown = function(){
                               $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
                    }

                    if ($scope.fd.Search===undefined || $scope.fd.Search===null) $scope.fd.Search = {};
             
             
                let widgetDefinition = {name: 'taskExecuteWidget',widgetId: $scope.fd.LocalId,task_initiate_id:$scope.task_initiate_id,task_execute_id:$scope.task_execute_id,taskId:  $scope.resourceDefinition.id };
                $scope.fd.LocalId=WidgetDataExchangeService.registerWidget(widgetDefinition);
                
                $scope.ksm = KatalyoStateManager;
      
                 let dependent_field_pid = $scope.fd.LocalId;
        let subscribedSubjectPid = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
        let dependent_field_pid2
        let update_presentation_id2 = function(data) {
            let presentation_id = angular.copy($scope.fd.PresentationId)
            if (data[presentation_id]!==undefined)
            {
                $scope.fd.PresentationId = data[presentation_id];
            //unsubscribe here later after implementing unsubscribe - ToDo
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            }
        }
        let update_presentation_id = function(data) {
            if (data[$scope.fd.LocalId]>0 && $scope.fd.PresentationId==0 && data[$scope.fd.LocalId]!==undefined) $scope.fd.PresentationId = data[$scope.fd.LocalId];
          
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
            //unsubscribe here later after implementing unsubscribe - ToDo
        }
        
        
        if ($scope.fd.PresentationId===null || $scope.fd.PresentationId===0)
        {
            let observerPid = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid,"formSaved",update_presentation_id);
        } else{
            dependent_field_pid2 = $scope.fd.PresentationId
            let observerPid2 = $scope.ksm.subscribe(subscribedSubjectPid,dependent_field_pid2,"formSaved",update_presentation_id2);
        }
                    $scope.filter=$scope.fd.Search;
                   //ovo treba prilagoditi
                   if ($scope.fd.Search.taskListType==undefined) $scope.fd.Search.taskListType="claim";
                      
                      
                    
                     $scope.gridOptions.onRegisterApi = function (gridApi) {
                        $scope.gridApi = gridApi;
                        gridApi.selection.on.rowSelectionChanged($scope,function(row){
                         $scope.localVars.taskSelected=row.isSelected
                       
                         if(row.isSelected){
                           
                             
                               if ($scope.fd.Search.taskListType=="execute")
                               {
                                $scope.selectedTask=row.entity.id
                                $scope.task_execute_id = $scope.selectedTask;
                                $scope.selectedTaskDefId=row.entity.resourcedef.id
                                $scope.selectedTaskObject = row.entity;  
                               }
                               else
                               {
                                $scope.selectedTask=row.entity.TaskInitiationId.id
                                $scope.task_initiate_id = $scope.selectedTask;
                                $scope.selectedTaskDefId=row.entity.TaskInitiationId.Resourcedef.id
                                $scope.selectedTaskObject = row.entity.TaskInitiationId;  
                               }
                               
                            }
                        });
                        
                        //external pagination
                      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
                          paginationOptions.pageNumber = newPage;
                          paginationOptions.pageSize = pageSize;
                          $scope.fd.ExecuteAllListPaginatedFilter($scope.fd.Search.taskListType,1,false,"",$scope.localVars.nextPage,$scope.localVars.previousPage,newPage,pageSize,$scope.filter);
                      });
                      
                       gridApi.core.on.filterChanged($scope, function() {
                           // Set some $scope variable that both lists use for filtering
                          // $$scope.someFilterValueBothListsLookAt = ......
                       });
                
                      
                     }
                   
                   
                  $scope.localVars.showProperties=function(datasetMapperType)
                  {
                   
                   if ($scope.localVars.datasetMapperType==undefined)
                   {
                      $scope.localVars.datasetMapperType = datasetMapperType;
                   }else
                   {
                      if ($scope.localVars.datasetMapperType==0 || $scope.localVars.datasetMapperType!=datasetMapperType) $scope.localVars.datasetMapperType = datasetMapperType;
                      else $scope.localVars.datasetMapperType=0;
                   }
                   
                  };
                      
              /*
                    $scope.removeField = function (index){
						
                        if (index > -1) {
						 
                           $scope.inputArray.layout.splice(index, 1);
						 
                        }

                  }
                */
                    $scope.ClaimSelected = function(){
                              
                              
                            var lang=$rootScope.globals.currentUser.lang;
                             TaskDataService.ExecuteTaskAction($scope.selectedTask,0,'claim',lang).then(function(data) {
                       
                                                   
                                                    
                                                    //refresh liste task-ova za claim
                                                    $scope.ExecuteAllList($scope.fd.Search.taskListType,1,true,data.data.msg);
                                                   
                                              },function(error){
                                                
                                            
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                      } 
                    
                    $scope.ExecuteAllList = function(type,filter,afterClaim,msgAfter){
                      
                      var lang=$rootScope.globals.currentUser.lang;
                      
                      TaskDataService.getInitiatedTasks(type,filter).then(function(data) {
                                                
                                                  $scope.gridOptions.data = data.data.results;
                                                 if (afterClaim)
                                                 {
                                                   MessagingService.addMessage(msgAfter,'success');
                                                  
                                                 }
                                               },function(error){
                                                
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                    }
                    
                    
                  
                  $scope.ExecuteAllListPaginated = function(type,filter,afterClaim,msgAfter,urlNext,urlPrev,newPage,pageSize){
                      
                      $scope.localVars.dataLoaded = false;
                      var lang=$rootScope.globals.currentUser.lang;
                       if ($scope.localVars.totalCount>0)
                      {
                           if (Math.ceil($scope.totalCount/pageSize)<newPage)
                           {
                             newPage = Math.ceil($scope.totalCount/pageSize);
                           }
                      }
                      TaskDataService.getInitiatedTasksPaginated(type,filter,urlNext,urlPrev,newPage,pageSize).then(function(data) {
                                                
                                                  $scope.gridOptions.data = data.data.results;
                                                  $scope.nextPage = data.data.next;
                                                  $scope.previousPage = data.data.previous;
                                                  $scope.totalCount=data.data.count;
                                                  $scope.currentPage=newPage;
                                                  $scope.numPages = Math.ceil($scope.totalCount/pageSize);
                                                  $scope.pageSize = pageSize;
                                                  $scope.gridOptions.totalItems=$scope.totalCount;
                                                  $scope.localVars.dataLoaded = true;
                                                 if (afterClaim)
                                                 {
                                                   MessagingService.addMessage(msgAfter,'success');
                                                  
                                                 }
                                               },function(error){
                                                
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                    }
                    
                    $scope.fd.rowSelectionChanged=function(item)
                    {
                        $scope.localVars.taskSelected=item.isSelected;

                        if(item.isSelected){
                           
                             
                               if ($scope.fd.Search.taskListType=="execute")
                               {
                                $scope.selectedTask=item.id
                                $scope.task_execute_id = $scope.selectedTask;
                                $scope.selectedTaskDefId=item.resourcedef[0].id
                                $scope.selectedTaskObject = item;  
                               }
                               else
                               {
                                $scope.selectedTask=item.TaskInitiationId.id
                                $scope.task_initiate_id = $scope.selectedTask;
                                $scope.selectedTaskDefId=item.TaskInitiationId.Resourcedef.id
                                $scope.selectedTaskObject = item.TaskInitiationId;  
                               }
                               
                            }   
                    }
                    
                    $scope.fd.ExecuteAllListPaginatedFilter = function(type,filter,afterClaim,msgAfter,urlNext,urlPrev,newPage,pageSize,f_filter){
                      
                      var lang=$rootScope.globals.currentUser.lang;
                      var deferred = $q.defer()
                      $scope.localVars.dataLoaded = false;
                      
                      if ($scope.fd.Search.taskListType=="claim")
                      {
                        $scope.results = {columns : columnDefsInitiate};
                      }
                    
                         if ($scope.fd.Search.taskListType=="execute")
                      {
                        $scope.results = {columns : columnDefsExecute};
                      }
                    
                      
                      if ($scope.localVars.totalCount>0)
                      {
                           if (Math.ceil($scope.localVars.totalCount/pageSize)<newPage)
                           {
                             newPage = Math.ceil($scope.localVars.totalCount/pageSize);
                           }
                      }
                     // $scope.gridOptions.data.length=0;
                      
                      TaskDataService.getInitiatedTasksPaginatedFiltered(type,filter,urlNext,urlPrev,newPage,pageSize,f_filter).then(function(data) {
                                                
                                                  $scope.results.results = data.data.results;
                                                  $scope.localVars.nextPage = data.data.next;
                                                  $scope.localVars.previousPage = data.data.previous;
                                                  $scope.localVars.totalCount=data.data.count;
                                                  $scope.localVars.currentPage=newPage;
                                                  $scope.localVars.numPages = Math.ceil($scope.localVars.totalCount/pageSize);
                                                  $scope.localVars.pageSize = pageSize;
                                                  $scope.results.totalItems=$scope.localVars.totalCount;
                                                  $scope.localVars.firstTime = false;
                                                  $scope.localVars.dataLoaded = true;
                                                  
                                                  
                               
                                                    $scope.results.pageSize = pageSize;
                                                    $scope.results.numberOfPages = $scope.localVars.numPages;
                                                    $scope.results.count = data.data.count;
                                                    
                                                    if (deferred!=undefined) deferred.resolve({data: $scope.results});
                                                  
                                                  
                                                  
                                               /*   if ($scope.localVars.totalCount==1)
                                                  {
                                                        if (!$scope.localVars.detailsClicked)
                                                        {
                                                                if ($scope.fd.Search.taskListType=="execute")
                                                                {
                                                                $scope.task_execute_id = $scope.gridOptions.data[0].id;
                                                                 
                                                                }
                                                                else
                                                                {
                                                                $scope.task_initiate_id = $scope.gridOptions.data[0].TaskInitiationId.id;
                                                               
                                                                }
                                                        }*/
                                                  
                                                  $timeout(function(){
                                                      $rootScope.$broadcast('PageLoaded');
                                                  });
                                                 if (afterClaim)
                                                 {
                                                   MessagingService.addMessage(msgAfter,'success');
                                                  
                                                 }
                                               },function(error){
                                                  deferred.reject({data: $scope.results});
                                                  MessagingService.addMessage(error.msg,'error');
                                            });
                          return deferred.promise;
                    }
             $scope.fd.pageChanged = function(){
                        
                        $scope.directiveFnSmartTableRefresh(1,$scope.localVars.pageSize);
                        //$scope.fd.getTaskList(1,$scope.localVars.pageSize);
                       
             }
             
             $scope.setDirectiveFnRefreshData = function(directiveFn) {
                          $scope.directiveFnSmartTableRefresh = directiveFn;
             };
    
             $scope.fd.getTaskList = function(pageNumber,pageSize){
                          var dfrd = $q.defer();
                          $scope.localVars.dataLoaded = false;
                          $scope.fd.ExecuteAllListPaginatedFilter($scope.fd.Search.taskListType,1,false,"",
                                                                  $scope.localVars.nextPage,$scope.localVars.previousPage,
                                                                  pageNumber,pageSize,$scope.filter).then(function(data)
                                                                                                                            
                                                                 {
                                                                              dfrd.resolve(data);
                                                                 });
                          return dfrd.promise;
             };
                         
                         
                     
                     $scope.ExecuteListPrePaginated = function(listFilter){
                            $scope.ExecuteAllListPaginated($scope.fd.Search.taskListType,listFilter,false,"","","",paginationOptions.pageNumber,paginationOptions.pageSize);
                         }
                         
                       
                      $scope.fd.Search.headerLabel = "Select users or groups";
                      $scope.fd.Search.usersLabel ="Users";
                      $scope.fd.Search.groupsLabel = "Groups";
                      
                  $scope.$watch('fd.Search.taskDefIds',function(newValue,oldValue){
                    
                    if (newValue!=oldValue && newValue!=undefined)
                    {
                     $scope.filter.taskDefIds=$scope.fd.Search.taskDefIds;
                     
                    }
                  });
                   $scope.$watch('fd.Search.taskStatus',function(newValue,oldValue){
                    
                    if (newValue!=oldValue && newValue!=undefined)
                    {
                     $scope.filter.taskStatus=$scope.fd.Search.taskStatus;
                     
                    }
                  });
                      
                      //get task statuses
                      
                       CodesService.getCodesByName('taskstatuses',lang).then(function (data) {
                                $scope.localVars.taskStatuses = data.data;
                             });
             let template;
             if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktTaskListWidget.html');
             else template = $templateCache.get('ktdirectivetemplates/ktTaskListWidgetExe.html');
          
             $element.append($compile(template)($scope));      
          
          }];
          
          
        return {
            restrict: 'A',
            controller:controller,
             link: function (scope, elem, attrs) {
                 
            
              }
        }             
};