'use strict';

export function ktDropdownMultiple ($timeout) {

        return {
            restrict: 'EA',
            require:['^form','ngModel'],
            templateUrl: 'ktdirectivetemplates/ktDropdownMultiple.html',
            controller: function($scope,$element,$timeout)
            {
                $scope.CTRL='ktDropdownMultiple';
                $scope.search={searchFilter:''};
                if ($scope.fieldElement===undefined) $scope.fieldElement = {}
                if ($scope.dropdownItems==undefined) $scope.dropdownItems=[];  
                $scope.localVars = {     
                        editMode: false                                           
                }
                
                $scope.$watch('selectedItems',function(newValue,oldValue){
                      if (newValue != undefined) {      
                            //debugger //migortest                      
                            for (let j=0;j<$scope.dropdownItems.length;j++) {
                                    $scope.dropdownItems[j].isSelected = false
                             }
                          
                          for (let i=0;i<$scope.selectedItems.length;i++) {
                             if ($scope.selectedItems[i].isSelected ==undefined)  $scope.selectedItems[i].isSelected = true//server does not send  isSelected !
                             for (let j=0;j<$scope.dropdownItems.length;j++) {
                                    if ($scope.dropdownItems[j].id ==$scope.selectedItems[i].id) {
                                                                        $scope.dropdownItems[j].isSelected = $scope.selectedItems[i].isSelected || true
                                    }
                             }
                          }  
                      }
                 }, true)
                      
       
                $scope.selectItem = function()
                {
                    //debugger //TODO - VIDJETI DA LI SE KORISTI
                        if (!$scope.isMultiple && $scope.selectedItems!=undefined && $scope.selectedItems!=null){
                          if ($scope.selectedItems.id!==undefined && $scope.selectedItems.id===null)
                            {
                                $scope.ngModel.$setViewValue(null);
                                let element = $element[0].getElementsByClassName('form-control');
                                $timeout(function(){
                                    element[0].selectedIndex =-1;
                                });
                            }
                          else $scope.ngModel.$setViewValue($scope.selectedItems);
                         
                        } else
                        {
                          if ($scope.selectedItems==null || $scope.selectedItems==undefined || !Array.isArray($scope.selectedItems)) $scope.model = {selectedItems:[]};
                          $scope.ngModel.$setViewValue($scope.selectedItems);   
                        }
                        $scope.onSelectFn({item:$scope.selectedItems,model:$scope.ngModel});      
                }
                
                
                $scope.setItem = function(item)
                {
                    //debugger //TODO - vidjeti da li se koristi
                    if (item.id!=undefined && item.id==null) $scope.selectedItems=item.id;
                    else $scope.selectedItems=item;         
                }
                
                
                $scope.clickItem = function(item,index)
                {
                    //debugger
                    if (item.isSelected===undefined) item.isSelected=false;
                    if ( $scope.selectedItems===undefined) $scope.selectedItems=[]// ovo mozda treba negdje u vanjsku direktivu
                    
                    if (item.isSelected)
                    {
                        let pos = $scope.selectedItems.map(function(e) { return e.id; }).indexOf($scope.dropdownItems[index].id);
                        if (pos>=0) $scope.selectedItems.splice(pos,1); 
                    }
                    else {
                        if ($scope.selectedItems == undefined || $scope.selectedItems == null) $scope.selectedItems = []
                        item.isSelected=true;
                        $scope.selectedItems.push(item);
                    }
                    
                   // $scope.ngModel.$setViewValue($scope.selectedItems); 
                
                }
                
                
                $scope.removeItem = function(ev,index)
                {
                    ev.stopPropagation();
                    //debugger
                    let pos = $scope.dropdownItems.map(function(e) { return e.id; }).indexOf($scope.selectedItems[index].id);
                    if (pos>=0) $scope.dropdownItems[pos].isSelected=false;
                    $scope.selectedItems.splice(index,1);
                    $scope.ngModel.$setViewValue($scope.selectedItems); 
                }
                
                
                $scope.clearOnly = function()
                {  
                        //$scope.selectedItems = null;
                        //set ngModel to null
                        $scope.ngModel.$setViewValue(null);  
                }
                
                
                $scope.loadItems = function(items)
                {
                    //debugger
                    if (items!=undefined)
                    {
                        $scope.dropdownItems = items;
                        if ($scope.search.searchFilter!=undefined && $scope.search.searchFilter!="") $scope.dropdownItemsFiltered = $scope.filterItems();
                        else $scope.dropdownItemsFiltered = angular.copy($scope.dropdownItems.slice(0,50));
                    }                      
                   
                }
                
                $scope.switchMode = function()
                {     
                    if ($scope.localVars.editMode) $scope.localVars.editMode = false;
                    else $scope.localVars.editMode = true;
                }
                
                
                $scope.acceptChanges = function()
                {     
                    $scope.localVars.editMode = false;
                    $scope.model.deletedItems.length=0;
                }
                
              
                $scope.selectItemsOnLoad = function()
                {
                    //debugger     // TODO - vidjeti da li se koristi
                    let pos;
                    if ($scope.selectedItems!=undefined) {
                           for(let j=0;j<$scope.selectedItems.length;j++){
                              pos = $scope.dropdownItems.map(function(e) { return e.id; }).indexOf($scope.selectedItems[j].id);   
                              //if found (almost) identical object - replace it WITH newer version
                              if (pos>=0 && $scope.dropdownItems[pos].id!=null){
                                  $scope.dropdownItems[pos] = $scope.selectedItems[j]                             
                              }   
                           }    
                    }                        
                };
                
            document.addEventListener('click', function(e){                 
                      if (!$element[0].contains(e.target) ){
                         $timeout(function() {  $scope.localVars.editMode=false; });  
                      }       
                    })
            },
            
            scope:{
            
            dropdownItems:'=',
            dropdownId:'=',
            parentFormId:'=',
            searchText:'=',
            selectedItems:'=?',
            requiredField:'=?',
            onSelectFn:'&',
            clearFn: '&',
            selectItemFn:'&',
            loadItemsFn:'&',
            setItemFn:'&',
            size:'=?',
            showId:'=?',
            modelType:'=?',
            customStyleItems:'=?',
            fieldElement:'=?'
            },
             
            link: function (scope, elem, attrs,ctrls) {
                   
                scope.form = ctrls[0];
                scope.ngModel= ctrls[1];
                scope.selectedItem = null; //{id:null,name:scope.searchText};
                //debugger
                
                    //For DOM -> model validation
                    scope.ngModel.$parsers.unshift(function(value) {
                         let valid = true
                         if (scope.requiredField) {
                            if (value?.length===0) valid = false
                            }
                            
                         scope.ngModel.$setValidity('required', valid);
                         return valid ? value : undefined;
                      });

                      //For model -> DOM validation
                      scope.ngModel.$formatters.unshift(function(value) {
                         let valid = true
                         if (scope.requiredField) {
                            if (value?.length===0) valid = false
                            }
                         scope.ngModel.$setValidity('required', valid);
                         return value;
                      });

                

                scope.clearFn({theDirFn: scope.clearOnly});
                scope.selectItemFn({theSelectItemFn: scope.selectItem});
                scope.loadItemsFn({theLoadItemsFn: scope.loadItems});
                scope.setItemFn({theSetItemFn: scope.setItem});
                
        
                 /* ONLY FOR TESTING
                 scope.$watch('dropdownItems',function(newValue,oldValue){
                        if (newValue!=undefined) {
                         let a=newValue
                        // debugger
                        }          
                 },true) */      
                     
            }
        }
  
};