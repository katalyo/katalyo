'use strict';

export function ktTaskProcInitiate () {
             var controller = ['$scope', '$stateParams','$state','UserService','ResourcesDataService','$rootScope','ProcessingService','$timeout','$cookies','CodesService','KatalyoStateManager',
              'WidgetDataExchangeService','TaskDataService','TaskProcService','MessagingService','GridsterService', '$window','$sce',
                               function ($scope,$stateParams,$state,UserService,ResourcesDataService,$rootScope,ProcessingService,$timeout,$cookies,CodesService,KatalyoStateManager,
              WidgetDataExchangeService,TaskDataService,TaskProcService,MessagingService,GridsterService, $window,$sce) {
            $scope.CTRL ="ktTaskProcInitiate"
                 if ($scope.localVars==undefined) $scope.localVars={};
          
          $scope.localVars.isCollapsed3=true;  
          //$scope.localVars = {dashboard:{}};
          $scope.localVars.autoInitiate=false;
          $scope.localVars.showTask = true;
          if ($scope.localVars.previousTask==undefined) $scope.localVars.previousTask = 0;
          var aParams = {};
          var pTaskPackage={};
          $scope.gsrvc = GridsterService;
          if ($scope.fd!=undefined)
          {
              $scope.$on('ProcessTask'+ $scope.fd.presentationId,function(event,data){
                  if ($scope.localVars.inlineTaskDefinition==undefined) $scope.localVars.inlineTaskDefinition={task_execute_id : data.task_execute_id};
                  else if ($scope.localVars.inlineTaskDefinition.task_execute_id==undefined) $scope.localVars.inlineTaskDefinition.task_execute_id = data.task_execute_id;
                  
                  $scope.localVars.StartTask(data.taskId,data.outcomeId);
              });
          }
          
         
              
                var lang=$rootScope.globals.currentUser.lang;
               
                $scope.localVars.initiateFormLoaded=false;
                $scope.isSaving=false;
                $scope.previousTask=$scope.taskDefinition.prev_task_id;
                $scope.previousTaskOutcomeId=$stateParams.outcome_id;
                $scope.previousTaskPackage=$stateParams.prev_task_package; 
                $scope.gridsterType="task";
                $scope.showSearchFilters=false;      
                //$scope.dashboard = {widgets : []};
                if ($scope.previousTaskOutcomeId=="") $scope.previousTaskOutcomeId = 0;
                
                $scope.fromStateName=$stateParams.fromStateName;
                $scope.fromStateParams=$stateParams.fromStateParams;
                $scope.cookieFromStateName=$cookies.getObject('initiateTaskFromStateName');
                $scope.cookieFromStateParams=$cookies.getObject('initiateTaskFromStateParams');
                $scope.noBackbutton = false;
                $scope.backBtnFromCookie=false;
      
      
  
                $scope.removeField = function (index){
						
                  if (index > -1) {
						 
                 $scope.inputArray.layout.splice(index, 1);
						 
                  }

                }   

 
 
    
          $scope.InititateAllList = function(){
      
            
              ResourcesDataService.getResourceDefByType(lang,'task').then(function(data) {
                                
                                  $scope.gridOptions.data = data.data;
                                   
                                 
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
          }
        
    
   
      $scope.InititateMyList = function(){
      
         
              ResourcesDataService.getResourceDefListByLangByTypeUser(lang,'task').then(function(data) {
                                
                                 $scope.gridOptions.data = data.data;
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
    
          
    };   
    
   
     
      
     
      if ($scope.fromStateName==undefined || $scope.fromStateName=="")
      {
          if ($scope.cookieFromStateName==undefined || $scope.cookieFromStateName=="")
          {
            $scope.noBackButton = true;
          }else
          {
            $scope.fromStateName =  $scope.cookieFromStateName;
            $scope.backBtnFromCookie = true;
          }
          
        
      }else{
        $cookies.putObject('initiateTaskFromStateName',$scope.fromStateName);
        
      }
      
      if ($scope.fromStateParams==undefined)
      {
          if ($scope.cookieFromStateParams!=undefined)
          {
              if ($scope.backBtnFromCookie)
              {
                $scope.fromStateParams =  $scope.cookieFromStateParams;
              }
          }
          
      }else if ($scope.cookieFromStateParams==undefined)
      {
        $cookies.putObject('initiateTaskFromStateParams',$scope.fromStateParams);
        
      }else
      {
          if ($scope.backBtnFromCookie)
          {
        
            $scope.fromStateParams =  $scope.cookieFromStateParams;
        
          }else
          {
              $cookies.putObject('initiateTaskFromStateParams',$scope.fromStateParams);    
          }
      }
      
      
       if ($scope.previousTask==undefined || $scope.previousTask=="")
          {
           // $state.go();
            
            $scope.previousTask = 0;
            
          }
      $scope.potentialOwners = {};
      

      
    //WidgetDataExchangeService.deRegisterAllWidgets();	//Migor - cistim taskwidgetRegistry 

      $scope.localTaskId=WidgetDataExchangeService.registerTask($scope.taskDefId);
      
     // $scope.taskDefinition ={};
      $scope.potentialOwners.potentialOwnersUsers = [];
      $scope.potentialOwners.potentialOwnersGroups = [];
      
         $scope.FormLockUnlock = function()
      {
        $scope.resourceDefinition.formParams.formLocked=!$scope.resourceDefinition.formParams.formLocked;

     
      };
     
      $scope.menuId = $stateParams.id;
      var lang=$rootScope.globals.currentUser.lang;
      
        $scope.localVars.NewTask = function(taskId)
        {
             $scope.localVars.inlineTask.widgets.layout=angular.copy($scope.localVars.inlineTask.widgetsOriginal);
             $scope.taskDefinition.inlineTaskInitiated=false;
        };
          
        $scope.localVars.StartNewTask = function()
        {
             $scope.localVars.StartTask($scope.taskDefinition.ResourceDefId_id,0,0);
             
        };
        
        $scope.returnToPreviousTask = function(){
             
             
             $scope.ksm=KatalyoStateManager;
             let taskStateName = "task"+$scope.taskDefinition.prev_task_transaction_id;
                         
             $scope.ksm.executeAction(taskStateName,"task_closed");
             
             $scope.gsrvc.setOpenTaskInNewPage(false);
                             
        };       
          $scope.localVars.CancelClicked = function()
          {
             if ($scope.localVars.fromOutcome  && !$scope.taskDefinition.inline_task)
             {
              $scope.localVars.executeTaskDataLoaded=false;
              if ($scope.localVars.pageChanged!=undefined) $scope.localVars.pageChanged();
             }
             $scope.taskDefinition.proc_type=0;
             $scope.taskDefinition.inlineInitiateFormLoaded=false;
             $scope.taskDefinition.showInlineTask = false;
             $scope.taskDefinition.inlineTaskInitiated = false;
            /*
             $scope.localVars.proc_type=0;
             $scope.localVars.inlineInitiateFormLoaded=false;
             $scope.localVars.showInlineTask = false;
             $scope.localVars.inlineTaskInitiated = false;
             */
             $scope.$emit('InlineTaskClosed',$scope.localVars.taskId);
             if (!$scope.taskDefinition.inline_task) $window.history.back();
          };
      
      
       $scope.localVars.AutoInitiateTask = function(){
        var lang=$rootScope.globals.currentUser.lang;
        //  if (isValid) {
        var parentDataset=$scope.localVars.getPreviousTaskWidget();
        if ($scope.previousTaskOutcomeId==undefined) $scope.previousTaskOutcomeId=0;
          
          TaskDataService.initiateTask($scope.taskDefinition.ResourceDefId_id,$scope.previousTask,$scope.previousTaskOutcomeId,[],$scope.potentialOwners,parentDataset['datasetId'],parentDataset['datasetRecordId'],lang).then(function(data) {
                                
                                    
                                    $scope.task_initiate_id = data.data.task_initiate_id;
                                    $scope.task_execute_id = data.data.task_execute_id;
                                    //$state.go('app.menu.tasks.execute',{id: $scope.taskDefId,task_initiate_id:$scope.task_initiate_id,task_execute_id:$scope.task_execute_id,fromStateName:$scope.fromStateName,fromStateParams:$scope.fromStateParams,prev_task_package:$scope.previousTaskPackage,prev:$scope.taskExecuteId});
                                   
                                  
                              },function(error){
                                    
                                  MessagingService.addMessage(error.msg,'error');
                            });
         // }
      };
      
      $scope.$on('CloseTaskForm'+$scope.taskDefinition.transaction_id,function(event,data){
                  
                  $scope.localVars.showTask = false;
                  $scope.$emit('InlineTaskClosed',$scope.localVars.taskId);
       });
      
       $scope.localVars.InitiateTask = function(form){
        var f = $scope.forma;
        
        if (form.$submitted) $scope.localVars.setSubmitted(form);
        if (form.$valid) {
            
            //var initiateData={};
            $rootScope.$broadcast('ProcessTaskWidgets',{}); //svi widgeti slusaju ovaj event
            $scope.isSaving = true;
            var lang=$rootScope.globals.currentUser.lang;
            if ($scope.previousTaskOutcomeId==undefined) $scope.previousTaskOutcomeId=0;
            //ako je prethodni task imao listu povuci WidgetData
            var parentDataset=$scope.localVars.getPreviousTaskWidget("resourceLinkDatasetList");
      
            if (parentDataset['datasetId']==undefined || parentDataset['datasetId']=="") parentDataset['datasetId'] = 0;
            if (parentDataset['datasetRecordId']==undefined || parentDataset['datasetRecordId']=="") parentDataset['datasetRecordId'] = 0;
            let resource_def_id = $scope.taskDefinition.ResourceDefId;
            if (resource_def_id==undefined) resource_def_id = $scope.taskDefinition.ResourceDefId_id;
                TaskDataService.InitiateTask(resource_def_id,$scope.taskDefinition.transaction_id,$scope.dashboard.data,$scope.potentialOwners,parentDataset['datasetId'],parentDataset['datasetRecordId'],lang).then(function(data) {
                                
					
                                  MessagingService.addMessage(data.data.msg,'success');
                                  //$rootScope.$broadcast('InitiateTaskPostProcess'+$scope.taskDefinition.transaction_id,data.data);
                                  //ovo treba staviti u event InitiateTaskPostProcessFinished
                                   if (data.data.taskInitiateInstance!=undefined)
                                    {
                                    if (data.data.taskInitiateInstance.length>0)
                                        $scope.taskDefinition.taskInitiateInstance = data.data.taskInitiateInstance[0];
                                        $scope.taskDefinition.task_initiate_id = $scope.taskDefinition.taskInitiateInstance.id;
                                    }
                                    let r_val;
                                    if (data.data.ret_value!=undefined)
                                    {
                                       if(data.data.ret_value.length>0)
                                       {
                                                    if (data.data.ret_value[0].eosResponse!=null && data.data.ret_value[0].eosResponse!=undefined) $scope.eosResponse = JSON.stringify(data.data.ret_value[0].eosResponse,null,2);
                                                    if (data.data.ret_value[0].eosResponse2!=null && data.data.ret_value[0].eosResponse2!=undefined) $scope.eosResponse2 = JSON.stringify(data.data.ret_value[0].eosResponse2,null,2);
                                                    r_val = data.data.ret_value;
                                       }else r_val = {task_initiate_id:$scope.taskDefinition.task_initiate_id};
                                    }
                                    
                                                                        
                                    $rootScope.$broadcast($scope.taskDefinition.page_state_id+'-'+"TaskPostProcess"+$scope.taskDefinition.transaction_id,r_val);
                                 
                                  //execute action task_initiated
                                  $scope.ksm = KatalyoStateManager;
                                  let taskStateName = "task"+$scope.taskDefinition.transaction_id;
                                  $scope.ksm.executeAction(taskStateName,"task_initiated");
                                  
                                  if (data.data.restart_task!=null && data.data.restart_task!=undefined)
                                  
                                  {
                                       $scope.taskDefinition.transaction_id = data.data.restart_task.data.transactionId;
                                       
                                       $scope.$on('CloseTaskForm'+$scope.taskDefinition.transaction_id,function(event,data){
                  
                                                    $scope.localVars.showTask = false;
                                                    $scope.$emit('InlineTaskClosed',$scope.localVars.taskId);
                                       });
                                  }
                                                                  
                                  
                                  $scope.$emit('RefreshData'+ $scope.taskDefinition.prev_task_id,$scope.taskDefinition.ResourceDefId_id); 
                                  $scope.localVars.taskInitiated=true;
                                  if (data.data.task_execute_id>0)
                                  {
                                       if (data.data.taskdef.Parameters!=null && data.data.taskdef.Parameters!=undefined)
                                       {     
                                            if (data.data.taskdef.Parameters.AutoLoadExeForm)
                                            {
                                                         $scope.localVars.GetExecuteTaskByExeId(data.data.task_execute_id,data.data.task_initiate_id);  
                                            }
                                       }
                                  }
     
                              },function(error){
                                 // $scope.isSaving = false;    
                                  MessagingService.addMessage(error.msg,'error');

                                $scope.errorMsg = $sce.trustAsHtml(error.msg)
                            });
                          
                          $scope.isSaving = false;  
						
			
							
          } // if (isValid)
          else
          {
            MessagingService.addMessage("Please correct invalid entries",'warning');
            
          }
      };

    $scope.localVars.getPreviousTaskWidget  = function(deiType)
    {
      var pTaskPkg = WidgetDataExchangeService.getPrevTaskData($scope.taskDefinition.prev_task_id);
             var datasetId=0,datasetRecordId=0,pTaskData,pTaskDataArray;
             if (!Array.isArray(pTaskPkg))
             {
                          pTaskDataArray = [pTaskPkg];
             }else
             {
                          pTaskDataArray = pTaskPkg;        
             }
             for (let l=0;l<pTaskDataArray.length;l++)
             {
                          pTaskData = pTaskDataArray[l];
                          if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
                          {
                          for (var i=0;i<pTaskData.DEI.length;i++)
                          {
                            if (pTaskData.DEI[i].name==deiType)
                            {
                              if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                              {
                                for (var j=0;j<pTaskData.DEI[i].value.length;j++)
                                {
                                 if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined) datasetId=pTaskData.DEI[i].value[j].value;
                                 if (pTaskData.DEI[i].value[j].name=="resourceRecordId" && pTaskData.DEI[i].value[j].value!=undefined) datasetRecordId=pTaskData.DEI[i].value[j].value;
                                 
                                  
                                }
                                   
                                  
                              }
                                
                            }
                              
                            }
                          }
             }
            
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };
        $scope.localVars.setSubmitted = function (form) {
          form.$setSubmitted();
          angular.forEach(form, function(item) {
            if(item && item.$$parentForm === form && item.$setSubmitted) {
              $scope.localVars.setSubmitted(item);
            }
          });
        }     
          
          }];
          
          
        return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskProcInitiate.html',
            scope:{
                   taskDefinition : "=",
                   dashboard: "=",
                   resourceDefinition:"=",
                   localVars: "=",
      			},
            controller:controller,
          
             link: function (scope, elem, attrs) {
                 
               
                    
       
             
    }
  }             
};