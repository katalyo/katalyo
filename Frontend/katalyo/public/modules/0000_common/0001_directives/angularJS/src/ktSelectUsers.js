'use strict';

export function ktSelectUsers (UserService,$timeout) {

    return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktSelectUsers.html',
            controller: function($scope, UserService){
               
                $scope.userList=[];
                $scope.getUsers = function () {
              
                UserService.getUsersGroups('users').then(function(data){
                         $scope.userList = data.data;
                         $scope.usersDownloaded=true;
                         //$scope.addCurrentUser();
                        });
                }
		
            $scope.getUsers();  
          
           },
           
           link: function (scope, elem, attrs) {
         
        }//LInk
    }
};