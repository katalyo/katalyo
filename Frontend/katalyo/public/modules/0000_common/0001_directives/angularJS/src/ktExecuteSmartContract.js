'use strict';

export function ktExecuteSmartContract () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService','KatalyoStateManager','UtilityService','$templateCache','$compile','$element',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService,KatalyoStateManager,UtilityService,$templateCache,$compile,$element) {
     
      $scope.localVarsWidget = {};
      //$scope.fd = {};
      $scope.localVarsWidget.searchFormDownloaded = false;
      $scope.localVarsWidget.formLoaded=false;
      $scope.localVarsWidget.src_type_text=CODES_CONST._POPULATE_TYPE_;
      $scope.localVarsWidget.animationsEnabled = true;
      $scope.localVarsWidget.widgetType = 3;
      $scope.gsrvc = GridsterService;
      $scope.localVarsWidget.formParams={formViewType:1,showGridNavigation:true};
      $scope.fd.contractDatasets = true;
      if ($scope.formData==undefined) $scope.formData={};
      if ($scope.formData[$scope.fd.PresentationId]==undefined) $scope.formData[$scope.fd.PresentationId]=[];
      
      $scope.localVarsWidget.datasetMapperType=0;
      
      if ($scope.fd.PopulateType==undefined) $scope.fd.PopulateType="";
      
      $scope.localVarsWidget.openModal = function (size,template,ctrl,selectedType) {
     
         var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: template,
            controller: ctrl,
            size: size,
            resolve: {
              selectedResourceItem: function () {
                return $scope.localVarsWidget.selectedResourceItem;
                },
              type: function () {
                return $scope.localVarsWidget.selectDatasetType;
                }
            }
         });
         modalInstance.result.then(function (selectedItem,windex,index) {
      
          if ($scope.localVarsWidget.selectDatasetType==1){
            $scope.localVarsWidget.selectedResourceItem = selectedItem;
            $scope.fd.Related =  $scope.selectedResourceItem.relatedId;
            $scope.fd.PopulateType=selectedItem.src_type_id;
            $scope.fd.ParentElementId = $scope.selectedResourceItem.parentElementId;
            $scope.fd.PresentationId = $scope.selectedResourceItem.presentationId;
            $scope.localVarsWidget.getFormResourceOriginal();
          
          }else if ($scope.localVarsWidget.selectDatasetType==2)
          {
            $scope.localVarsWidget.selectedRelatedResourceItem = selectedItem;
            $scope.fd.HasParentDataset =  $scope.selectedResourceItem.relatedId;
            $scope.fd.PopulateTypeParent=selectedItem.src_type_id;
            //$scope.fd.ParentElementId = $scope.selectedResourceItem.parentElementId;
            //$scope.fd.PresentationId = $scope.selectedResourceItem.presentationId;
          }
         
         }, function () {
        //$log.info('Modal dismissed at: ' + new Date());
         });
         
      };
     
      $scope.localVarsWidget.openModalSelectResource = function(){
        $scope.localVarsWidget.selectDatasetType  = 1;
        $scope.localVarsWidget.openModal('lg','selectResourceModal.html','SelectResourceModalCtrl',$scope.localVarsWidget.selectDatasetType);
        
      };
     
      $scope.localVarsWidget.openModalSelectRelatedResource = function(){
        $scope.localVarsWidget.selectDatasetType  = 2;
        $scope.localVarsWidget.openModal('lg','selectDatasetModal.html','SelectDatasetModalCtrl',$scope.localVarsWidget.selectDatasetType);
      };
      
     
      $scope.localVarsWidget.gridsterType="resource";
          // $scope.gridsterOpts = GridsterService.getGridsterOptions();
    
    
        
      $scope.localVarsWidget.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: []
      } ;
        
    
  
      //$scope.taskId=$stateParams.id;
      $scope.taskId=$scope.resourceDefinition.ResourceDefId;
      if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
      if ($scope.taskId==undefined) $scope.taskId=0;
      $scope.localVarsWidget.taskId=$scope.taskId;
      $scope.fd.ParentDatasetDefId=$scope.fd.Related;
      $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
      $scope.localVarsWidget.formType=$scope.resourceDefinition.form_type;
      $scope.source_type=$scope.resourceDefinition.source_type;
      $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
      $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
      $scope.formDefinition = $scope.resourceDefinition.form_definition;
      
      if ($scope.localVars == undefined)  $scope.localVars= {} 
      $scope.localVars.showMenu = false
      
       $scope.localVars.toggleDropdown = function(){
                           $scope.localVars.showMenu =  $scope.localVars.showMenu ? false: true 
       }
        
      if  ($scope.formDefinition==undefined) $scope.formDefinition=false;
        
      $scope.localVarsWidget.resourceRecordId=0;
      if ($scope.taskInitiateId==undefined)
      {
        $scope.taskInitiateId =0;
      
      }
      if ($scope.taskExecuteId==undefined)
      {
         $scope.taskExecuteId =0;  
      }
        if ($scope.prevTaskExeId==undefined)
        {
            $scope.prevTaskExeId =0;
        }
        
      //register widget 
      var out_fields = ['resourceId'];
      //if ($scope.fd.LocalId==undefined || $scope.fd.LocalId==null || $scope.fd.LocalId==0)
      //{
         var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'viewResourceWidget',widgetId: $scope.fd.PresentationId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId,taskId:  $scope.lTaskId,resourceDefId : $scope.localVarsWidget.selectedResource, output:out_fields, };
         $scope.localVarsWidget.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
         $scope.localVarsWidget.LocalId=$scope.localVarsWidget.localWidgetId;
         $scope.fd.LocalId=$scope.localVarsWidget.localWidgetId;
         //$scope.fd.LocalId=UtilityService.uuidNow();
         
      //}
      $scope.ksm = KatalyoStateManager;
      
      let update_presentation_id = function(data)
      {
         if (data[$scope.fd.LocalId]>0 && ($scope.fd.PresentationId==null || $scope.fd.PresentationId==0)) $scope.fd.PresentationId = data[$scope.fd.LocalId];
          //unsubscribe here later after implementing unsubscribe - ToDo
          
      }
      if ($scope.fd.PresentationId==0 || $scope.fd.PresentationId==null)
      {
         
         let dependent_field = $scope.fd.LocalId;
         let subscribedSubject = "formDefinitionState"+$scope.taskId+$scope.resourceDefinition.form_type;
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"formSaved",update_presentation_id);
  
      }        
      
       let apply_value = function(data)
        {
            let do_refresh=false;
            if (data!=undefined)
            {
               if (data != $scope.localVarsWidget.resourceRecordId)
               {
                  if (typeof data ==='number') $scope.localVarsWidget.resourceRecordId= data;
                  else if (Array.isArray(data))
                  {
                     if (data.length>0)
                     {
                      $scope.localVarsWidget.resourceRecordId= data[0].id  
                     }
                  }else
                  {
                     $scope.localVarsWidget.resourceRecordId = data.id;
                  }
                  do_refresh = true;
               }
            }
            //get selectedRecord
            if ($scope.localVarsWidget.resourceRecordId == null) $scope.localVarsWidget.resourceRecordId=0;
            
            if ($scope.localVarsWidget.resourceRecordId>0 && do_refresh) $scope.localVarsWidget.getFormResourceWithData();
            
            
        };
        
      
      if ($scope.fd.Parameters==undefined) $scope.fd.Parameters={};
      
      if ($scope.fd.Parameters.RefreshOnIdChange)
      {
         
         //find 
         //subscribe observer
           
         let dependent_field = "Field"+$scope.fd.PresentationId;
         let subscribedSubject = "resourceidValueStateName"+$scope.fd.Parameters.ResourceIdItemId+$scope.resourceDefinition.transaction_id;
         let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
         let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
         let val = $scope.ksm.getState(subscribedSubject);
         $scope.localVarsWidget.resourceRecordId= val.id;
            
            //get selectedRecord
         if ($scope.localVarsWidget.resourceRecordId == null) $scope.localVarsWidget.resourceRecordId=0;
            
         if ($scope.localVarsWidget.resourceRecordId>0) $scope.localVarsWidget.getFormResourceWithData();
      }
      
      //subscribe observer
      if ($scope.fd.PopulateType.toString().substr(0,1)=="4")
      {
           /**********************************************************************
         ****************** Sync data with State manager **********************
         ***********************************************************************/
        /*
        $scope.itemValue = {val:null};
        
        let valueState= $scope.ksm.addSubject('datasetValueStateName'+$scope.fd.ItemId,$scope.itemValue.val);
        let valueStateName = valueState.subject.name;
        
        let value_changed = function(data)
        {
            valueState.subject.setStateChanges = data;
            valueState.subject.notifyObservers("valueChanged");
        };
        
        let action_value_changed = $scope.ksm.addAction(valueStateName,"valueChanged",value_changed);
       
        */
       
       
        if ($scope.fd.SrcWidgetId!=undefined && $scope.fd.SrcWidgetId!=null)
        
        {
           
                let dependent_field = "Field"+$scope.fd.SrcWidgetId;
               // let subscribedSubject = $scope.fd.Parameters.SrcWidgetType+"ValueStateName"+$scope.fd.SrcWidgetId;
                let subscribedSubject = "rlistValueStateName"+$scope.fd.SrcWidgetId+$scope.resourceDefinition.transaction_id;
                let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",apply_value);
            
        }  
         
      }
      
      let refresh_widget_data = function()
      
      {
         $scope.localVarsWidget.getFormResourceWithData();    
         
      }
      if ($scope.fd.Parameters.EventName!=undefined)
      {
         if ($scope.fd.Parameters.EventName!="")
         {
            let dependent_field2 = "Field"+$scope.fd.PresentationId;
            let subscribedSubject2 = "task"+$scope.resourceDefinition.transaction_id;
            let observer2 = $scope.ksm.subscribe(subscribedSubject2,dependent_field2,$scope.fd.Parameters.EventName,refresh_widget_data);
         }
      }
      
      $scope.localVarsWidget.showProperties=function(datasetMapperType)
      {
            
            if ($scope.localVarsWidget.datasetMapperType==undefined)
            {
               $scope.localVarsWidget.datasetMapperType = datasetMapperType;
            }else
            {
               if ($scope.localVarsWidget.datasetMapperType==0 || $scope.localVarsWidget.datasetMapperType!=datasetMapperType) $scope.localVarsWidget.datasetMapperType = datasetMapperType;
               else $scope.localVarsWidget.datasetMapperType=0;
            }
            
      };
           
      $scope.localVarsWidget.setField = function(form,name,value,msg)
      {
         TaskDataService.setField(form,name,value);
         if (msg) MessagingService.addMessage(msg,'info');
      };  
      
      $scope.localVarsWidget.getResourceAll = function () {
          
            ResourcesDataService.getResourceDefAll($scope.fd.Related).then(function(data) {
                               
                             // MessagingService.addMessage(data.msg,'success');
                              $scope.fd.resource = data.data;
                              $scope.localVarsWidget.resourceLoaded = true;
                              
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };
      $scope.localVarsWidget.getResourceAll();
      
       $scope.fd.loadBlockchainDataset = function (dataset) {
           
           if (dataset.DatasetId==undefined) dataset.DatasetId = {id:0}; 
           $scope.fd.selectedDataset = dataset;
           if (dataset.DatasetId.id>0) $scope.localVarsWidget.getFormResourceOriginal(dataset.DatasetId.id);
            
        };
        $scope.fd.loadBlockchainFunction = function (func)
        {
           if (func.InputDatasetId==undefined) func.DatasetId = {id:0}; 
           $scope.fd.selectedFunction = func;
           if (func.InputDatasetId.id>0) $scope.localVarsWidget.getFormResourceOriginal(func.InputDatasetId.id);
        }
        
         $scope.fd.executeBlockchainFunction = function (func)
        {
            var lang=$rootScope.globals.currentUser.lang;
              
               //$scope.localVarsWidget.formLoading=true;
              let limit =  100;
              $scope.fd.datasetLoaded=false;
              $scope.fd.gridOptions.columnDefs=[];
              if ($scope.fd.BlockChainPrimaryKey==undefined) $scope.fd.BlockChainPrimaryKey="";
              ResourcesDataService.executeBlockchainFunction($scope.fd.resource.Parameters.ContractId,$scope.fd.selectedFunction.FunctionName,$scope.formData[$scope.fd.PresentationId][0]).then(function(data) {
                  
                  MessagingService.addMessage(data.data,'success'); 

               },function(error){
                                   MessagingService.addMessage(error.msg,'error');   
         
               });
        }
      $scope.localVarsWidget.getFormResourceOriginal = function(datasetId){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.PresentationId==null)
              {
                $scope.fd.PresentationId = 0;
                
              }
              $scope.localVarsWidget.formLoaded=false;
              ResourcesDataService.getResourceForm(datasetId,"i",lang).then(function(data) {
                            
                                $scope.localVarsWidget.resourceWidgetDefinition= data.data.resourceDefinition;
                                if (Array.isArray(data.data.widgets)) $scope.localVarsWidget.dashboard.widgets.layout =  data.data.widgets;
                                
                                if ($scope.localVarsWidget.selectedResourceItem!=undefined)
                                 { 
                                  $scope.localVarsWidget.selectedResourceItem.name=$scope.localVarsWidget.resourceWidgetDefinition.name;
                                  $scope.localVarsWidget.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.localVarsWidget.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_text=$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType];
                                  $scope.localVarsWidget.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.localVarsWidget.selectedResourceItem={'name':$scope.localVarsWidget.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                                $scope.fd.wItemValue=data.data.widgets;
                                
                           /*
                                var container = $element.children().eq(0);
                                GridsterService.setupDragular($scope.fd.wItemValue,container,'rows-'+$scope.fd.PresentationId,'row-'+$scope.localVarsWidget.gridsterType+'-hadle',true);
                             */   
                                
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - ADD_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.ResourceRecordId}]}]});
						
                                  $scope.localVarsWidget.searchFormDownloaded = true;
                                  //$timeout(function(){$scope.localVarsWidget.formLoaded=true});
                                  $scope.localVarsWidget.formLoaded=true;
                                
                               // $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'Required',false);     
                                   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      $scope.fd.gridOptions = {
         
         showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: false,
      //  enableHorizontalScrollbar : 1,
       // enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: false,
        enableColumnResizing: true,
        enableColumnReordering: true,
        //useExternalPagination: false,
        paginationPageSizes: [10, 25, 50,100,200,500,1000,5000,10000],
      }
        $scope.fd.GetBlockchainDatasetRecords = function(){
              var lang=$rootScope.globals.currentUser.lang;
              
               //$scope.localVarsWidget.formLoading=true;
              let limit =  100;
              $scope.fd.datasetLoaded=false;
              $scope.fd.gridOptions.columnDefs=[];
              if ($scope.fd.BlockChainPrimaryKey==undefined) $scope.fd.BlockChainPrimaryKey="";
              ResourcesDataService.getBlockchainDatasetRecords($scope.fd.selectedDataset.DatasetId.id,$scope.fd.selectedDataset.DatasetName,$scope.fd.resource.Parameters.ContractId,limit,$scope.fd.BlockChainPrimaryKey,lang).then(function(data) {
                  
                  $scope.fd.datasetLoaded=true;
                  
                  $scope.fd.gridOptions.data = data.data.data.rows;
                  
                  //$scope.fd.gridOptions.columnDefs = data.data.columns;
                  
                  //$scope.localVarsWidget.formLoading=false;
                  
               
               },function(error){
                                   MessagingService.addMessage(error.msg,'error');   
                             });
        }
        $scope.localVarsWidget.getFormResource = function(){
              var lang=$rootScope.globals.currentUser.lang;
               $scope.localVarsWidget.formLoaded=false;
               if ($scope.fd.PresentationId==null)
               {
                    $scope.fd.PresentationId=0;
            
               }
               $scope.localVarsWidget.formLoading=true;
                         
              ResourcesDataService.getResourceFormExtended($scope.fd.Related,$scope.taskId,$scope.localVarsWidget.formType,$scope.fd.PresentationId,"w",lang).then(function(data) {
                               
                                  $scope.localVarsWidget.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.localVarsWidget.dashboard.widgets.layout =  data.data.widgets;
                                  
                                 if ($scope.localVarsWidget.selectedResourceItem!=undefined)
                                 { 
                                  $scope.localVarsWidget.selectedResourceItem.name=$scope.localVarsWidget.resourceWidgetDefinition.name;
                                  $scope.localVarsWidget.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.localVarsWidget.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_text=$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType];
                                  $scope.localVarsWidget.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.localVarsWidget.selectedResourceItem={'name':$scope.localVarsWidget.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                              
                                $scope.fd.wItemValue=data.data.widgets;
                               
                                
                                  
                                $scope.localVarsWidget.searchFormDownloaded = true;
                                
                                $timeout(function(){    
                                   $scope.localVarsWidget.formLoaded=true;
                                   $scope.localVarsWidget.formLoading=false;
                                });
                                  
                               
                              //  $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'Required',false);
                                
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                 $scope.localVarsWidget.formLoading=false;
                                 
                                
                             });
                              
      };
      
              
        $scope.localVarsWidget.getFormResourceWithData = function(){
              var lang=$rootScope.globals.currentUser.lang;
              $scope.localVarsWidget.formLoaded=false;
              if ($scope.fd.PresentationId==null || $scope.fd.PresentationId==undefined)
              {
                $scope.fd.PresentationId = 0;
                
              }
              
              if ($scope.fd.ParentElementId==null || $scope.fd.ParentElementId==undefined)
              {
                $scope.fd.ParentElementId = 0;
                
              }
              
              
              
              
               if ($scope.localVarsWidget.resourceRecordId==undefined || $scope.localVarsWidget.resourceRecordId==0)
               {
                
                  if ($scope.localVarsWidget.previousTaskData['datasetRecordId']!=undefined && $scope.localVarsWidget.previousTaskData['datasetRecordId']!="" && $scope.localVarsWidget.previousTaskData['datasetId']==$scope.fd.Related) $scope.localVarsWidget.resourceRecordId = $scope.localVarsWidget.previousTaskData['datasetRecordId'];
                
               }
               if (typeof $scope.localVarsWidget.resourceRecordId !=='number' && typeof $scope.localVarsWidget.resourceRecordId !=='string')
               {
                  
                  if (Array.isArray($scope.localVarsWidget.resourceRecordId))
                  {
                     if ($scope.localVarsWidget.resourceRecordId.length>0)
                     {
                        $scope.localVarsWidget.resourceRecordId= $scope.localVarsWidget.resourceRecordId[0].id  
                     
                     }
                  }
                  else $scope.localVarsWidget.resourceRecordId = $scope.localVarsWidget.resourceRecordId.id;                 
               }
               
              ResourcesDataService.getResourceFormExtendedWithData($scope.fd.Related,$scope.taskInitiateId,$scope.taskExecuteId,$scope.fd.PopulateType,$scope.fd.ParentElementId,$scope.fd.PresentationId,$scope.prevTaskExeId,$scope.localVarsWidget.formType,$scope.localVarsWidget.widgetType,$scope.localVarsWidget.resourceRecordId,"w",lang).then(function(data) {

                                  $scope.formData[$scope.fd.PresentationId] =  data.data.form_data;
                                  if ($scope.formData[$scope.fd.PresentationId]==undefined) $scope.formData[$scope.fd.PresentationId]=[];
                                  $scope.localVarsWidget.resourceWidgetDefinition= data.data.resourceDefinition;
                                  if (Array.isArray(data.data.widgets)) $scope.localVarsWidget.dashboard.widgets.layout =  data.data.widgets;
                                 
                                  if ($scope.formData[$scope.fd.PresentationId].length>0)
                                  {
                                    $scope.localVarsWidget.resourceRecordId = $scope.formData[$scope.fd.PresentationId][0]['id'];
                                  }
                                  $scope.localVarsWidget.datasetRecordId=$scope.localVarsWidget.resourceRecordId;
                                  if ($scope.localVarsWidget.selectedResourceItem!=undefined)
                                 { 
                                  $scope.localVarsWidget.selectedResourceItem.name=$scope.localVarsWidget.resourceWidgetDefinition.name;
                                  $scope.localVarsWidget.selectedResourceItem.relatedId=$scope.fd.Related;
                                  $scope.localVarsWidget.selectedResourceItem.presentationId=$scope.fd.PresentationId;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_id=$scope.fd.PopulateType;
                                  $scope.localVarsWidget.selectedResourceItem.src_type_text=$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType];
                                  $scope.localVarsWidget.selectedResourceItem.parentElementId=$scope.fd.ParentElementId;
                                 }
                                else{
                                  $scope.localVarsWidget.selectedResourceItem={'name':$scope.localVarsWidget.resourceWidgetDefinition.name,'relatedId':$scope.fd.Related, 'presentationId':$scope.fd.PresentationId  ,'src_type_id':$scope.fd.PopulateType,'src_type_text':$scope.localVarsWidget.src_type_text[$scope.fd.PopulateType],'parentElementId':$scope.fd.ParentElementId};
 
                                }
                                    //$scope.fd.wItemValue=data.data.widgets;
                               
                                  // $timeout(function() {
                                     $scope.localVarsWidget.formLoaded=true;
                                    // $scope.localVarsWidget.initiateFormLoaded=true;
                                     $scope.localVarsWidget.formLoading=false;
                                     $scope.localVarsWidget.searchFormDownloaded = true;
                                      
                                  //  });   
                                  
                                   
                                //MIGOR - bitno - DOBIO RESOURCERECORD-ID ZA BROADCAST  - VIEW_RESOURCE WIDGET
                                WidgetDataExchangeService.setData({widgetId:$scope.fd.PresentationId, DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.localVarsWidget.resourceRecordId}]}]});
                                
                               // $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'ReadOnly',true);       
                                $scope.localVarsWidget.setField($scope.localVarsWidget.dashboard.widgets.layout,'Required',false);   
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
      
      
           $scope.localVarsWidget.setSelectedDataset = function(id)  {
              
              if ($scope.localVarsWidget.selectedResourceItem==undefined) $scope.localVarsWidget.selectedResourceItem={};
              var pos = $scope.localVarsWidget.resourceList.map(function(e) { return e.id; }).indexOf(id);
              
              if (pos>=0){
                $scope.localVarsWidget.selectedResourceItem.id = $scope.localVarsWidget.resourceList[pos].id;
                $scope.localVarsWidget.selectedResourceItem.name = $scope.localVarsWidget.resourceList[pos].name;
                $scope.localVarsWidget.selectedDataset = $scope.localVarsWidget.resourceList[pos];
                $scope.localVarsWidget.selectedResourceItem.name_confirmed = $scope.localVarsWidget.selectedResourceItem.name +' ('+$scope.localVarsWidget.selectedResourceItem.id +')'
                $scope.localVarsWidget.selectedResourceItem_c=$scope.localVarsWidget.selectedResourceItem;
                }
					}
          
                  
          // POPUNI LISTU DEFINICIJA OD RESURSA (RESOURCES LIST)
	$scope.localVarsWidget.getResources = function()  {
						
            if ($scope.localVarsWidget.resourceList==undefined) $scope.localVarsWidget.resourceList=[];
          
            var lang=$rootScope.globals.currentUser.lang;
            ResourcesDataService.getResourceDefListByType(lang,'smartcontract').then(
						function(data) {
										
											  $scope.localVarsWidget.resourceList = data.data;
                       if ($scope.fd.Related!=null)
                       {
                        $scope.localVarsWidget.setSelectedDataset($scope.fd.Related);
                
                        }     
											 
										  },function(error){    
                                MessagingService.addMessage(error.msg,'error');
										 });
					}
   
   

$scope.localVarsWidget.getPreviousTaskWidget  = function(linkType)
    {
      var pTaskData;
      var pTaskDataRet = WidgetDataExchangeService.getPrevTaskData($scope.prevTaskExeId);
      if (Array.isArray(pTaskDataRet) && pTaskDataRet.length>0) pTaskData = pTaskDataRet[0];
            var datasetId=0,datasetRecordId=0;
            if (pTaskData!=undefined)
            {
               if (pTaskData.DEI!=undefined && pTaskData.DEI!=null)
               {
               for (var i=0;i<pTaskData.DEI.length;i++)
               {
                 if (pTaskData.DEI[i].name==linkType)
                 {
                   if (pTaskData.DEI[i].value!=undefined && pTaskData.DEI[i].value!=null)
                   {
                     for (var j=0;j<pTaskData.DEI[i].value.length;j++)
                     {
                      if (pTaskData.DEI[i].value[j].name=="resourceId" && pTaskData.DEI[i].value[j].value!=undefined) datasetId=pTaskData.DEI[i].value[j].value;
                      if (pTaskData.DEI[i].value[j].name=="resourceRecordId" && pTaskData.DEI[i].value[j].value!=undefined) datasetRecordId=pTaskData.DEI[i].value[j].value;
                      
                       
                     }
                        
                       
                   }
                     
                 }
                   
                 }
               }
            }
    return {'datasetId':datasetId,'datasetRecordId':datasetRecordId};
    };


   
   
  $scope.localVarsWidget.previousTaskData=$scope.localVarsWidget.getPreviousTaskWidget("resourceLinkDatasetList");
 
          
  if ($scope.fd.Related!=null &&  !$scope.formDefinition && !$scope.localVarsWidget.searchFormDownloaded)
  {
    
    // $scope.localVarsWidget.getFormResourceWithData();
    // $scope.localVarsWidget.getDatasets();
    // $scope.localVarsWidget.setSelectedDataset($scope.fd.Related);
    
  }else if ($scope.fd.Related!=null)
  {
     //$scope.localVarsWidget.getFormResource();
     $scope.localVarsWidget.getResources();
     //$scope.localVarsWidget.setSelectedDataset($scope.fd.Related);
    
  } 

  
      //novi FILE CONNECTOR - view resource widget
      $scope.openWidgetConnector  = function(){
             $scope.localVarsWidget.showWidgetConnector= !$scope.localVarsWidget.showWidgetConnector;
             if ($scope.localVarsWidget.showWidgetConnector) {
                           
                 //POPUNI LISTU DATASETOVA            
                  ResourcesDataService.getResourceDefListByType(lang,'smartcontract').then(
                                function(data) {
                                                  $scope.resourceList = data.data;
                                                  //dodaj ime dataseta u konektore
                                                  if ($scope.localVarsWidget.listOfConnectors!=undefined) 
                                                             for (var i=0;i< $scope.localVarsWidget.listOfConnectors.length;i++) {
                                                                   //var p = $scope.resourceList.map(function(e) { return e.id; }).indexOf($scope.localVarsWidget.listOfConnectors[i].parentDatasetDefId);
                                                                   var p = $scope.localVarsWidget.resourceList.map(function(e) { return e.id; }).indexOf($scope.localVarsWidget.listOfConnectors[i].parentDatasetDefId);
                                                                   if (p>=0) $scope.localVarsWidget.listOfConnectors[i].parentDatasetName = $scope.resourceList[p].name;  
                                                             }
                                                  
                                                  },function(error){    
                                        MessagingService.addMessage(error.msg,'error');
                                                 });
                                                 
                                                 
 							if ($scope.fd.tItemValue==undefined || $scope.fd.tItemValue[0]==null) $scope.fd.tItemValue = []; 
							// ovo je  Kt-UPLOADER
							  $scope.localVarsWidget.listOfConnectors= WidgetDataExchangeService.getRegisteredWidgets();//MIGOR  - uzmi sve registrirane widgete
							  
							  //Brisi sve koji se vec nalaze u tItemValue
							   if ($scope.localVarsWidget.listOfConnectors!=undefined) 
								  for (var i=0;i< $scope.fd.tItemValue.length ;i++) {
										 for (var j=0;j< $scope.localVarsWidget.listOfConnectors.length;j++) {
                                              if ($scope.fd.tItemValue[i]!=null) {
                                                 if ($scope.localVarsWidget.listOfConnectors[j].widgetId==$scope.fd.tItemValue[i].widgetId)  {
                                                     $scope.localVarsWidget.listOfConnectors.splice(j,1);//brisi lokalni				   					    		
                                                 }
                                              }
                                         }
							    }
                                //dodaj sa servera
                                for (var i=0;i< $scope.fd.tItemValue.length ;i++) 
                                              if ($scope.fd.tItemValue[i]!=null) 
                                                  $scope.localVarsWidget.listOfConnectors.push($scope.fd.tItemValue[i]);		
                                

							  //Ostavi u listi konektora samo WIDGETE koji imaju "ResourceID" jer su to data widgeti
							  var newlist=[];
                              
							    if ($scope.localVarsWidget.listOfConnectors!=undefined){
                 
                                 for (var i=0;i< $scope.localVarsWidget.listOfConnectors.length;i++) {
                                                     if ($scope.localVarsWidget.listOfConnectors[i].DEI!=undefined)  
                                                         for (var j=0;j< $scope.localVarsWidget.listOfConnectors[i].DEI.length;j++) {
                                                                 //ako DEI nije true/false - resetiraj na false
                                                                 if ( ($scope.localVarsWidget.listOfConnectors[i].DEI[j].value!=false) && ($scope.localVarsWidget.listOfConnectors[i].DEI[j].value!=true) ) $scope.localVarsWidget.listOfConnectors[i].DEI[j].value=false;
                                                                 //dodaj konektor u novu listu 
                                                                
                                                             };
                                                        newlist.push($scope.localVarsWidget.listOfConnectors[i]);
                                               }
                                }//if
							  $scope.localVarsWidget.listOfConnectors=newlist; //konacna lista NOVIH dataset konektora 
 
			
	
            } //if showfileconnetor  
		}; // open FILECONNECTOR - VIEW
    

            $scope.localVarsWidget.okWidgetConnector = function () {
                $timeout(function() {
                $scope.fd.tItemValue=$scope.localVarsWidget.listOfConnectors;
                $scope.localVarsWidget.showWidgetConnector=false;  
                
                });
            };

          $scope.localVarsWidget.cancelWidgetConnector = function () {
                $timeout(function() {
                
                $scope.localVarsWidget.showWidgetConnector=false;  
                
                });
            };

       
        $scope.localVarsWidget.processWidgetDependencies = function() { //-----                
           if ($scope.fd.tItemValue!=undefined) 
            for (var i=0;i< $scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=$scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = $scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<$scope.fd.tItemValue[i].DEI.length;j++) {
                        if (($scope.fd.tItemValue[i].DEI[j].name==linkedItem) && ($scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = $scope.fd.tItemValue[i].DEI[j].name;  
                             //console.log("view "+$scope.fd.PresentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             $scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj $scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                              //  console.log("view "+ $scope.fd.PresentationId+" dobio event " + event.name);
                                for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    $scope.localVarsWidget.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    $scope.localVarsWidget.resourceRecordId = $scope.localVarsWidget.linkedResourceId;
                                  }
                                  if (data[k].name=='resourceId') $scope.localVarsWidget.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                }
                          
                              //get selectedRecord
                              $scope.localVarsWidget.getFormResourceWithData();
                                
                                    //	data: {resource_def_id:$scope.fd.linkedResourceDefId,resource_id:$scope.fd.linkedResourceId}
                                    
                              }) //on_END
                        }//if
                    }//for j
                }//if

             // else {  $scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget

          if ($scope.fd.PresentationId==undefined || $scope.fd.PresentationId==null) $scope.fd.PresentationId=0;
          
          $scope.localVarsWidget.formType=$scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies($scope.taskId,$scope.fd.PresentationId,$scope.localVarsWidget.formType).then(function(data) {
                               
                                $scope.fd.tItemValue=data.data;
                                $scope.localVarsWidget.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
                              
    
            let template;
            //if ($scope.formDefinition) template = $templateCache.get('ktdirectivetemplates/ktTaskViewResource.html');
            //else template = $templateCache.get('ktdirectivetemplates/ktTaskViewResourceExe.html');
            template = $templateCache.get('ktdirectivetemplates/ktExecuteSmartContract.html');
            $element.append($compile(template)($scope)); 

  
      }];

  return {
            restrict: 'A',
           
            controller: controller,
            link: function (scope, elem, attrs) {

            }     
  }
  
};