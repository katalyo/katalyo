'use strict';

export function ktTaskFiles () {
       
     return {
        restrict: 'A', 
        templateUrl: 'ktdirectivetemplates/ktTaskFiles.html',
         controller: function ($scope, $rootScope,$uibModal,KatalyoStateManager,ResourcesDataService,ProcessingService,MessagingService,CODES_CONST,WidgetDataExchangeService,GridsterService,TaskDataService,$timeout,$q) {
				 
              $scope.fd.ReadOnly=false;
              $scope.animationsEnabled = true;
              
              $scope.gsrvc = GridsterService;
              
              if ($scope.fd.localVars==undefined)  $scope.fd.localVars = {datasetMapperType:0};
              
                $scope.fd.localVars.dashboard = {widgets:[]}
                $scope.fd.localVars.showMenu = false
                $scope.fd.localVars.widgetState={}
                $scope.fd.localVars.gridsterType = 'file'
                $scope.fd.localVars.TablePrefix = 'resource'+$scope.fd.Related+'_'+$rootScope.globals.currentUser.organisation.id
                $scope.fd.localVars.showSearchFilters = true
                $scope.fd.localVars.formParams={formViewType:1,showGridNavigation:true};
                $scope.fd.localVars.taskId=$scope.resourceDefinition.ResourceDefId;
                $scope.fd.localVars.InitialLoad = true
                if ($scope.resourceDefinition.form_definition && $scope.fd.localVars.formParams.formViewType==1) $scope.fd.Parameters.BuildMode = true
                else $scope.fd.Parameters.BuildMode = false

                $scope.fd.localVars.toggleDropdown = function(){
                           $scope.fd.localVars.showMenu =  $scope.fd.localVars.showMenu ? false: true 
                }
              
              if ($scope.fd.Parameters == null ||  $scope.fd.Parameters==undefined)  $scope.fd.Parameters = {};
              
                 $scope.fd.localVars.paginationOptions = {
                    pageNumber: 1,
                    pageSize: 10,
                    sort: null
                };
              
              $scope.showMetadata = function (fileId) {
            
                    
                  var modalInstance = $uibModal.open({
                  animation: $scope.animationsEnabled,
                  templateUrl: 'metadata_modal.html',
                  controller: 'FileMetadataModalInstanceCtrl',
                  size: 'lg',
                  resolve: {
                    retValue: function () {
                      return {file_id:fileId};
                    }
                  }
              });
           
                  modalInstance.result.then(function (retValue,windex,index) {
                    
            
                 }, function () {
             
                 });
          
       
            };
            


           $scope.fd.localVars.showProperties=function(datasetMapperType)
           {
            
               if ($scope.fd.localVars.datasetMapperType==undefined)
               {
                  $scope.fd.localVars.datasetMapperType = datasetMapperType;
               }else
               {
                  if ($scope.fd.localVars.datasetMapperType==0 || $scope.fd.localVars.datasetMapperType!=datasetMapperType) $scope.fd.localVars.datasetMapperType = datasetMapperType;
                  else $scope.fd.localVars.datasetMapperType=0;
               }
            
           };

        
        let valueState= $scope.ksm.addSubject($scope.resourceDefinition.page_state_id+"-"+$scope.fd.ElementType+"ValueStateName-"+$scope.fd.PresentationId,$scope.fd.localVars.widgetState);
        let valueStateName = valueState.subject.name;
        

           //*********************** get state if exists********************************************
        //***************************************************************************************
        if (valueState.subject.currentStateIndex>=0 && !$scope.fd.AutoSearch)
        {
            $scope.fd.localVars.loadFromState=true;
            $scope.fd.localVars.widgetState = valueState.subject.getState;
            $scope.fd.localVars.dashboard.widgets.layout=$scope.fd.localVars.widgetState.widgets;
            $scope.fd.localVars.searchFormDownloaded = true;
            $scope.fd.localVars.formLoaded=true;
        }else
        
        {
          $scope.fd.localVars.widgetState.formData=[{}];  
        }

           /*
            $scope.fd.localVars.okFileConnector = function () {
                $timeout(function() {
                $scope.fd.tItemValue=$scope.fd.listOfConnectors;
                $scope.fd.localVars.showProperties(0);
                
                });
            };
            
             $scope.fd.localVars.cancelFileConnector = function () {
                $timeout(function() {
                //Can't copy! Making copies of Window or Scope instances is not supported
                $scope.fd.listOfConnectors=angular.copy($scope.fd.tItemValue);
                //$scope.fd.listOfConnectors=$scope.fd.tItemValue;
                $scope.fd.localVars.showProperties(0);
                
                });
            };
            */
          //   kt taskfiles - FILECONNECTOR  -  ista funkcija kao i za uploader!
          /*
		  $scope.fd.localVars.openFileConnector = function(){
             $scope.fd.localVars.showProperties(1);
             
             if ($scope.fd.localVars.datasetMapperType==1) {
				//POPUNI LISTU DATASETOVA 
                  var lang = $rootScope.globals.currentUser.lang;                          
                  ResourcesDataService.getResourceDefListByType(lang,'dataset').then(
                               function(data) {
                                           $scope.fd.datasetList = data.data;
                                           //dodaj ime dataseta u konektore
                                           if ($scope.fd.listOfConnectors!=undefined) 
                                                      for (var i=0;i< $scope.fd.listOfConnectors.length;i++) {
                                                            var p = $scope.fd.datasetList.map(function(e) { return e.id; }).indexOf($scope.fd.listOfConnectors[i].parentDatasetDefId);
                                                            if (p>=0) $scope.fd.listOfConnectors[i].parentDatasetName = $scope.fd.datasetList[p].name;  
                                                      }
                                           },function(error){  MessagingService.addMessage(error.msg,'error');
                  });
						
							
                  if ($scope.fd.tItemValue==undefined || $scope.fd.tItemValue[0]==null) $scope.fd.tItemValue = [];  
						 
                  //ovo je kt-taskFiles
                  $scope.fd.listOfConnectors= WidgetDataExchangeService.getRegisteredWidgets();//MIGOR  - uzmi sve registrirane widgete
                          
                            //Brisi sve koji se vec nalaze u tItemValue
						if ($scope.fd.listOfConnectors!=undefined) 
								  for (var i=0;i< $scope.fd.tItemValue.length ;i++) {
										 for (var j=0;j< $scope.fd.listOfConnectors.length;j++) {
                                              if ($scope.fd.tItemValue[i]!=null) {
                                                 if ($scope.fd.listOfConnectors[j].widgetId==$scope.fd.tItemValue[i].widgetId)  {
                                                     $scope.fd.listOfConnectors.splice(j,1);//brisi lokalni				   					    		
                                                 }
                                              }
                                         }
							    }
                                //dodaj sa servera
                                for (var i=0;i< $scope.fd.tItemValue.length ;i++) 
                                              if ($scope.fd.tItemValue[i]!=null) 
                                                  $scope.fd.listOfConnectors.push($scope.fd.tItemValue[i]);	
                                                  
							  
							  //Ostavi u listi konektora samo WIDGETE koji imaju "ResourceID" jer su to data widgeti
							  var newlist=[];
							   
								if ($scope.fd.listOfConnectors!=undefined){
									for (var i=0;i< $scope.fd.listOfConnectors.length;i++) {
                                                     if ($scope.fd.listOfConnectors[i].DEI!=undefined)  
                                                         for (var j=0;j< $scope.fd.listOfConnectors[i].DEI.length;j++) {
                                                             if ($scope.fd.listOfConnectors[i].DEI[j].name=='resourceLink' || $scope.fd.listOfConnectors[i].DEI[j].name=='resourceLinkDatasetList') {
                                                                 //ako DEI nije true/false - resetiraj na false
                                                                 if ( ($scope.fd.listOfConnectors[i].DEI[j].value!=false) && ($scope.fd.listOfConnectors[i].DEI[j].value!=true) ) $scope.fd.listOfConnectors[i].DEI[j].value=false;
                                                                 //dodaj konektor u novu listu 
                                                                 newlist.push($scope.fd.listOfConnectors[i]);
                                                             };						 						  						   					      
                                                    }
                                               }
									}//if
							  $scope.fd.listOfConnectors=newlist; //konacna lista NOVIH dataset konektora (NE ukljucuje one u tItemValue koja se salje posebno)
 
						
             }//if
			}; // open FIleConnector - ktTaskFiles 
        */
        
        $scope.fd.fileListDownloaded = false;
        $scope.fd.isCollapsedFilesList = true;
       $scope.fd.localVars.hidePopulateMethod=true;

       $scope.fd.localVars.resourceType="file";
        
        
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;

         if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
         if ($scope.taskId==undefined)  $scope.taskId=0;
       

        $scope.fd.localVars.setSelectedResource = function(id,saved)  {

              if ($scope.fd.localVars.selectedResourceItem==undefined) $scope.fd.localVars.selectedResourceItem={};
              $scope.fd.localVars.getPublishedResourceById(id,saved);

        }
     
        $scope.fd.localVars.getPublishedResourceById = function(id,saved) 
 
 {
    
            let pos
            let old_publish_id = $scope.fd.Parameters.PublishedResourceId
           
                   
            if (saved && $scope.fd.FormDef!==undefined && $scope.fd.FormDef!==null && $scope.fd.FormDef.length>0)
              { 
                    pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  
                    if (pos>=0){
                        $scope.fd.localVars.selectedResourceItem = $scope.fd.localVars.publishedResourceList[pos];
                        $scope.fd.localVars.selectedResource = $scope.fd.localVars.publishedResourceList[pos];

                        $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                    }
                    
                    $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.FormDef
                   
                    if ($scope.resourceDefinition.form_definition) $scope.fd.fileListDownloaded =true
                    $scope.fd.localVars.formLoaded=true;

              }else{
                  pos = $scope.fd.localVars.publishedResourceList.map(function(e) { return e.id; }).indexOf(id);
                  if (pos>=0){
                    ResourcesDataService.GetPublishedResourceById(id).then(function (data) {

                            $scope.fd.localVars.selectedResourceItem = $scope.fd.localVars.publishedResourceList[pos];
                            $scope.fd.localVars.selectedResourceItem.Form = data.data.Form
                            $scope.fd.localVars.selectedResource = $scope.fd.localVars.publishedResourceList[pos];
                            
                            $scope.fd.localVars.selectedResourceItem.name_confirmed = $scope.fd.localVars.selectedResourceItem.name +' ('+$scope.fd.localVars.selectedResourceItem.id +')'
                            if ($scope.fd.localVars.selectedResourceItem!==undefined)
                            {
                                $scope.fd.Parameters.PublishedResourceId=$scope.fd.localVars.selectedResourceItem.id;
                                $scope.fd.Parameters.PublishedResourceVersion=$scope.fd.localVars.selectedResourceItem.Version;
                                $scope.fd.Related=$scope.fd.localVars.selectedResourceItem.ResourceDefId; 
                                $scope.fd.localVars.dashboard.widgets.layout = $scope.fd.localVars.selectedResourceItem.Form
                                $scope.fd.FormDef = $scope.fd.localVars.selectedResourceItem.Form
                                $scope.fd.localVars.freshForm=angular.copy($scope.fd.localVars.selectedResourceItem.Form)
                            }

                            if ($scope.fd.GridState?.columnsDef===undefined || old_publish_id!=id) $scope.fd.localVars.getColumnsDef()
                            else $scope.fd.fileListDownloaded =true
                            $scope.fd.localVars.formLoaded=true;

                        },function(error){    
                          MessagingService.addMessage(error.msg,'error');
                    });

                }
            }     
}      

   
  
        var out_fields = ['resourceId'];
        var widgetDefinition = { DEI: [{'name':'resourceLinkR','value':[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'ktTaskFiles',widgetId: $scope.fd.PresentationId,taskId:  $scope.lTaskId,resourceDefId : $scope.selectedResource, output:out_fields, };
        $scope.fd.localVars.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
        $scope.fd.localVars.LocalId=$scope.fd.localVars.localWidgetId;
    
        $scope.fd.LocalId=$scope.fd.localVars.localWidgetId;
        
        $scope.ksm = KatalyoStateManager;
        
        $scope.fd.localVars.formType=$scope.resourceDefinition.form_type;
        
      
        ResourcesDataService.GetPublishedResourcesByType('file','form').then(function (data) {       
           $scope.fd.localVars.publishedResourceList = data.data;
           //debugger  //GetPublishedResourcesByType   
           //$scope.fd.localVars.dashboard.widgets.layout = JSON.parse(data.data[0].Form)
                        
           if ($scope.fd.Parameters.PublishedResourceId!=null) $scope.fd.localVars.setSelectedResource($scope.fd.Parameters.PublishedResourceId,true);
           else {

              $scope.fd.localVars.formLoaded=true;
              $scope.fd.localVars.NoPublishedResourceMsg="Definition error --> Published dataset not defined!"
            }
             
           },function(error){    
                          MessagingService.addMessage(error.msg,'error');
    });



	   $scope.fd.downloadFile = function(fileId,fileName){
                ResourcesDataService.downloadFile($scope.fd.Related,fileId).then(function(data) {
                               
                                    MessagingService.addMessage('File '+fileName+' downloaded','info');
                                    var url = URL.createObjectURL(new Blob([data.data]));
                                    var a = document.createElement('a');
                                    a.href = url;
                                    a.download = fileName;
                                    a.target = '_blank';
                                    a.click();
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// dowload file END
      
      
       $scope.fd.previewFile = function(fileId,fileName,fileType){
           
            ResourcesDataService.downloadFile($scope.fd.Related,fileId).then(function(data) {
                               
                                    var url = URL.createObjectURL(new Blob([data.data],{type:fileType}));
                                    let new_doc = window.open(url,"_blank");
                                  
                                },function(error){    
                                MessagingService.addMessage(error.msg,'error');
                                 
                                
                             });
      };// preview file END
   
      //kt TASK FILES
    
    $scope.fd.localVars.filesData = {data:[]};
    
    $scope.fd.localVars.fileSelectionChanged = function (item) {
       $scope.fd.fileSelected=item.isSelected;
       $scope.fd.SelectedFile = item;
     
    }

   
    $scope.fd.localVars.getFiles= function(tableState, pageNumber,pageSize){


        //taskExecuteId: 0   taskInitiateId: 0
        let deferred = $q.defer();    
        $scope.resourceLoading=true;
        var lang=$rootScope.globals.currentUser.lang;
        var newPage
       
        
        if (pageNumber==undefined) pageNumber = $scope.fd.localVars.paginationOptions.pageNumber;
        if (pageSize==undefined) pageSize = $scope.fd.localVars.paginationOptions.pageSize;
        
        if ($scope.fd.localVars.filesData!==undefined) {
                $scope.fd.localVars.totalCount= $scope.fd.localVars.filesData.data?.length
                $scope.fd.localVars.paginationOptions.totalItemCount=$scope.fd.localVars.filesData.data?.length
                $scope.fd.localVars.paginationOptions.numberOfPages = Math.ceil($scope.fd.localVars.paginationOptions.totalItemCount/pageSize);
        }

        if ($scope.fd.localVars.totalCount>0){
            if (Math.ceil($scope.fd.localVars.totalCount/pageSize)<pageNumber)
                newPage = Math.ceil($scope.fd.localVars.totalCount/pageSize);
        }
        
         if ($scope.fd.localVars.filesData) 
                $scope.fd.localVars.results = {results : $scope.fd.localVars.filesData.data.slice( (pageNumber -1) * pageSize, pageNumber * pageSize) }
         else 
                $scope.fd.localVars.results = {}     
         $scope.fd.localVars.results.columns=$scope.fd.GridState?.columnsDef

         $scope.fd.localVars.currentPage=pageNumber;
         $scope.fd.localVars.results.pageSize = pageSize;
         $scope.fd.localVars.results.numberOfPages = $scope.fd.localVars.paginationOptions.numberOfPages;
         $scope.fd.localVars.results.count =   $scope.fd.localVars.totalCount
         
         $scope.fd.localVars.widgetState.currentPage = $scope.fd.localVars.currentPage;
         $scope.fd.localVars.widgetState.pageSize = $scope.fd.localVars.results.pageSize;
         
         //$scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
         //if (!$scope.fd.localVars.loadFromState) $scope.fd.localVars.resetSelection();
         
         deferred.resolve({data: $scope.fd.localVars.results});
         
      return deferred.promise;
    }

	$scope.fd.getFilesData = function (file_def_id,dataset_def_id) {
				
				$scope.filter = {};
				var lang = $rootScope.globals.currentUser.lang;
				//$scope.fd.linkedResourceId = resource_id; //MIGOR - mozda je visak (je)
				//$scope.fd.linkedResourceDefId = resource_def_id; //MIGOR - mozda je visak (je)
           if (file_def_id!=undefined)
           {
              
                    $scope.fd.localVars.filesData.data.length=0;
                    $scope.fd.fileSelected = false;
                    $scope.fd.localVars.resourceDataDownloading = true
                    ResourcesDataService.getFilesData($scope.fd.PresentationId,file_def_id,dataset_def_id,0).then(function(data) {
                                   
                         
                             
                        $scope.fd.localVars.filesData = data.data
                        
                        $scope.lastResourceDefId = file_def_id;
                        $scope.lastResourceId = dataset_def_id;
                        $scope.fd.fileListDownloaded = true;
                        $scope.fd.localVars.InitialLoad = false
                        $scope.fd.localVars.resourceDataDownloading = false
                    
                    }			
                    ,function(error){    
                         MessagingService.addMessage(error.msg,'error');	
                         $scope.fd.fileListDownloaded = true;
                         $scope.fd.localVars.resourceDataDownloading = false
                    });
            }
          
	};//getFilsData END

    $scope.fd.localVars.GetFilesClick = function()

    {
       
        
        $scope.fd.localVars.selectedResourceId=$scope.fd.Related;
        $scope.fd.localVars.GetFilesFilter($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,1,$scope.fd.localVars.widgetState.pageSize); 
           

    }
          //**********************************************************
      //           GET RESOURCE DATA PAGINATED
      //**********************************************************      
    $scope.fd.localVars.GetFilesFilter = function (urlNext,urlPrev,newPage,pageSize) 
    {
        
        if ($scope.fd.localVars.filesData.data==undefined) $scope.fd.localVars.filesData.data = [];
        if (Math.ceil($scope.fd.localVars.totalCount/pageSize)<newPage) newPage = Math.ceil($scope.fd.localVars.totalCount/pageSize);
        const lang = $rootScope.globals.currentUser.lang;
        if (pageSize==undefined)
        {
          $scope.fd.localVars.widgetState.pageSize = paginationOptions.pageSize;
          pageSize= $scope.fd.localVars.widgetState.pageSize;
        }
          
           
        $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
        $scope["taskFilesSearchForm" + $scope.fd.PresentationId]?.$setSubmitted()
        const is_valid = $scope["taskFilesSearchForm" + $scope.fd.PresentationId]?.$valid
           //let is_valid = $scope.datasetListSearchForm?.$valid
           
        if (is_valid)
        {
                 $scope.fd.localVars.resourceDataDownloading = true;
                 $scope.fd.localVars.filesData.data.length=0;
        
                ResourcesDataService.getResourceDataPaginatedFilter($scope.fd.localVars.selectedResourceId,urlNext,urlPrev,newPage,pageSize,lang,$scope.fd.localVars.widgetState.formData, $scope.fd.localVars.dashboard.widgets.layout,$scope.fd.localVars.columnsForQuery).then(function(data) {
                                    
                                    
                                    //setup data grid or card
                                    $scope.fd.localVars.filesData.data = data.data;
                                    if ($scope.fd.Parameters.PresentationType=='card') $scope.fd.localVars.gridOptionsResourceData.data = $scope.fd.localVars.TransformDataForCard(data.data);
                                    $scope.fd.localVars.restoreGridState()
                                    
                                    $timeout(function(){
                                        $scope.fd.fileListDownloaded = true;
                                        $scope.fd.localVars.InitialLoad = false
                                        $scope.fd.localVars.resourceDataDownloading = false
                                       
                                        if (!$scope.fd.localVars.loadFromState) $scope.fd.localVars.widgetState.currentPage=1;
                                        if ($scope.fd.localVars.totalCount>0 && $scope.fd.localVars.widgetState.currentPage==0) $scope.fd.localVars.widgetState.currentPage=1;
                                        $scope.fd.localVars.numPages = Math.ceil($scope.fd.localVars.totalCount/$scope.fd.localVars.widgetState.pageSize);
                                        
                                        $scope.fd.localVars.filesData.totalItems=$scope.fd.localVars.totalCount;
                                        
                                        WidgetDataExchangeService.setData({
                                        DEI: [{name:'resourceLinkTaskFiles',value:[{name:'resourceId',value:$scope.fd.Related},{name:'resourceRecordId',value:$scope.fd.localVars.widgetState.selectedDatasetRecordId}
                                        ,{name:'resourceRecordIds',value: null},{name:'resourceRecordsAll',value:$scope.fd.localVars.filesData.data}]}],widgetId: $scope.fd.PresentationId,task_execute_id:$scope.taskExecuteId});
                
                                        $scope.fd.localVars.loadFromState = false;
                                    })
                                   
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');
                                    $scope.fd.fileListDownloaded = true;
                                    $scope.fd.localVars.resourceDataDownloading = false
                             });
        }else{
                 MessagingService.addMessage($scope.fd.ErrorMsg,'warning');
                 $timeout(function(){ $scope.fd.fileListDownloaded = true;$scope.fd.localVars.resourceDataDownloading = false});
            }

    };
         


    $scope.fd.localVars.pageChanged = function(){
        
        $scope.ksm.executeAction(valueStateName,"valueChanged", $scope.fd.localVars.widgetState);
        $scope.fd.localVars.GetFilesFilter($scope.fd.localVars.nextPage,$scope.fd.localVars.previousPage,$scope.fd.localVars.widgetState.currentPage,$scope.fd.localVars.widgetState.pageSize);
         };

     //**********************************************************
     //           GET DATASET DIRECT FILTER
     //**********************************************************      
      $scope.fd.localVars.getDatasetDirect = function (item) {
        let lang = $rootScope.globals.currentUser.lang;             
        let children = [item[$scope.fd.Parameters.ParentDatasetFieldName]]
        $scope.fd.fileListDownloaded = false
        ResourcesDataService.getResourceDataDirectFilter($scope.fd.Related,$scope.fd.PresentationId,"i",lang,{id__in: children}).then(function(data) {
                                
                                    $scope.fd.localVars.filesData.data = data.data
                                    //$scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef
                                    $scope.fd.localVars.restoreGridState()
                                    $scope.fd.fileListDownloaded = true
                                    $scope.fd.localVars.InitialLoad = false
         //                            MessagingService.addMessage(data.msg,'success');
                                  },function(error){
                                    MessagingService.addMessage(error.msg,'error');               
                             });
        };

      /* Get children for parent*/
        $scope.fd.localVars.GetResourceDataForParent = function () 
        {
              var lang = $rootScope.globals.currentUser.lang;
              if ($scope.fd.localVars.ParentRecordId>0)
              {
                $scope.fd.fileListDownloaded = false
                ResourcesDataService.getResourceDataM2m($scope.fd.PresentationId,$scope.fd.ParentDatasetDefId,$scope.fd.Related,$scope.fd.localVars.ParentRecordId,$scope.taskId,0,lang).then(function(data) {
                            $scope.fd.localVars.filesData.data = data.data
                            //$scope.fd.localVars.gridOptionsResourceData.columnDefs = data.columnsDef
                            $scope.fd.localVars.restoreGridState()
                            $scope.fd.fileListDownloaded = true
                            $scope.fd.localVars.InitialLoad = false
                                           
                },function(error){    
                    MessagingService.addMessage(error.msg,'error');
                    
                                    });
            }              
        }

  $scope.fd.localVars.restoreGridState= function() {
        //debugger
        if ($scope.fd['GridState']!==undefined && $scope.fd['GridState']!==null)
        {
          
            if ($scope.fd['GridState']['columnsDef']!==undefined) $scope.fd.localVars.filesColumns =  $scope.fd['GridState']['columnsDef']
            if ($scope.fd['GridState']['columnsQuery']!==undefined) $scope.fd.localVars.columnsForQuery =  $scope.fd['GridState']['columnsQuery']

        }
    };
      $scope.fd.localVars.saveGridState= function(showMsg) {
               //debugger //ktTaskDatasetList smarttable
               let columnsDef = $scope.fd.localVars.filesColumns

               let columnsQuery = $scope.fd.localVars.columnsForQuery
               $scope.fd['GridState']={'columnsDef':columnsDef,'columnsQuery': columnsQuery}

               if (showMsg) MessagingService.addMessage("To make changes stick please save form",'info');            
    };

      $scope.fd.localVars.getColumnsDef = function () {

        let lang = $rootScope.globals.currentUser.lang;
        ResourcesDataService.getResourceColumns($scope.fd.Parameters.PublishedResourceId,'file',lang,$scope.fd.Parameters.PublishedResourceVersion).then(function(data) {//MIGOR
                            
                                  $scope.fd.localVars.filesColumns = data.data.columnsDef;
                                  //$scope.fd.localVars.columnsDownloaded = true;
                                  $scope.fd.fileListDownloaded = true
                                  $scope.fd.localVars.InitialLoad = false
                                  $scope.fd.localVars.saveGridState()
                                 
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });

      }

        //ktTaskFiles  - ProcessEvent
      $scope.fd.ProcessEvent=function(data)
      {
        for (var i=0;i<data.length;i++)
        {
         if (data[i].name=='resourceRecordId')
         {
               if (data[i].value!=undefined && typeof data[i].value =='object')
               {
                     $scope.fd.linkedResourceId = data[i].value.id;
               }else $scope.fd.linkedResourceId = data[i].value;

         }
         if (data[i].name=='resourceId')
          {
               
                if (data[i].value!=undefined && typeof data[i].value =='object')
               {
                     $scope.fd.linkedResourceDefId = data[i].value.id;
               }else $scope.fd.linkedResourceDefId = data[i].value;
          }
        }

      }; //ktTaskFiles  - ProcessEvent
      
        //ktTaskFiles  - pr0cessWidgetDependencies
        $scope.fd.ProcessWidgetDependencies = function() {
                                    
            if ($scope.fd.tItemValue!=undefined)
                for (var i=0;i< $scope.fd.tItemValue.length;i++) { // prolazim po DEI od vezanih widgeta
                    var linkedWidgetId=$scope.fd.tItemValue[i].widgetId;
                    if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                     //linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                        for (var j=0;j<$scope.fd.tItemValue[i].DEI.length;j++) {
                            if (($scope.fd.tItemValue[i].DEI[j].name==='resourceLink' || $scope.fd.tItemValue[i].DEI[j].name==='resourceLinkDatasetList') && ($scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                           
                               //console.log("kt-taskfiles " + $scope.fd.PresentationId+" vezani widgetid=" + linkedWidgetId);       
                               var linkedItem = $scope.fd.tItemValue[i].DEI[j].name;
                               
                               $scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj $scope.on
                                                 
                                 //    console.log("kt-taskfiles "+$scope.fd.PresentationId+", dobio event " + event.name);              
                                     //ktTASKfiles
                                                    
                                 $scope.fd.ProcessEvent(data);  
                                                 
                                }) //$on_END
                                 var data = WidgetDataExchangeService.getDataByWidgetId($scope.resourceDefinition.task_execute_id,$scope.resourceDefinition.task_initiate_id,$scope.resourceDefinition.prev_task_id,linkedWidgetId,linkedItem);
                                 $scope.fd.ProcessEvent(data);
                            }//if
                    }//for j
                   }//if   
                } //for i
        }// fn pr0cessWidgetDependencies

    let refresh_data=function(data)
    {
            let state = data.state

            if (state!=undefined)
            {
                $scope.fd.ParentDatasetDefId = state.datasetDefId
                $scope.fd.localVars.ParentRecordId = state.recordId
                if ($scope.fd.Parameters.ParentRelationshipType==0) $scope.fd.localVars.GetResourceDataForParent()
                else $scope.fd.localVars.getDatasetDirect(state.record)
            }

    }

    if ($scope.fd.HasParentDataset && $scope.fd.PopulateParentMethod=='A')
    {
        

        //setup here state manager
        let dependent_field = "Field"+$scope.fd.UuidRef;
        let subscribedSubject = $scope.resourceDefinition.page_state_id+'-'+$scope.fd.SrcParentWidgetId;
        let subject = $scope.ksm.addSubject(subscribedSubject,null);                          
        let observer = $scope.ksm.subscribe(subscribedSubject,dependent_field,"valueChanged",refresh_data);
        let val = $scope.ksm.getState(subscribedSubject);
        if (val.state!==undefined && val.state!==null)
        {   
            let parent_item  = val.state
            $scope.fd.ParentDatasetDefId = parent_item.datasetDefId
            $scope.fd.localVars.ParentRecordId = parent_item.recordId
            if ($scope.fd.Parameters.ParentRelationshipType==0) $scope.fd.localVars.GetResourceDataForParent()
            else $scope.fd.localVars.getDatasetDirect(parent_item.record)
    
        }
        
    }
   
      //TODO - vidjeti da li ovaj treba i zbog èega
       $scope.$on('InitiateTaskFilesUploaded',function(event, data){
             $scope.fd.getFilesData($scope.fd.linkedResourceDefId ,$scope.fd.linkedResourceId ); 
          });
		  
      
              
        //get files (first everything then filter by dataset)
        if (!$scope.resourceDefinition.form_definition && $scope.fd.AutoSearch && !$scope.fd.HasParentDataset) $scope.fd.getFilesData($scope.fd.Related,0);
        else if ($scope.fd.GridState?.columnsDef!==undefined) $scope.fd.fileListDownloaded = true
                                 
            //ILJU fetch from server tItemValue ktTaskFiles - not needed (widget dependencies will be depreated)
          $scope.formType=$scope.resourceDefinition.form_type;
          if ($scope.fd.PresentationId!=undefined && $scope.fd.PresentationId!=null)
          {
          TaskDataService.getWidgetDependencies($scope.taskId,$scope.fd.PresentationId,$scope.formType).then(function(data) {
                               
                                $scope.fd.tItemValue=data.data;
                //console.log(' prije '+$scope.fd.tItemValue[0].widgetId+':'+$scope.fd.tItemValue[0].DEI[0].name+' '+$scope.fd.tItemValue[0].DEI[0].value);
                //console.log('prije '+$scope.fd.tItemValue[1].widgetId+':'+$scope.fd.tItemValue[1].DEI[0].name+' '+$scope.fd.tItemValue[1].DEI[0].value);
                                  //ovo je da trenutno radi
                               // WidgetDataExchangeService.setData($scope.fd.tItemValue);
                                $scope.fd.ProcessWidgetDependencies();
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
       
          }
             
         },//ktTaskFile $CONTROLER
        link: function (scope, elem, attrs) {
          
      
		}//LINK ktTaskFiles
    }
}; //ktTaskFiles END