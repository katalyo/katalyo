'use strict';

export function ktTaskAddMultipleResources () {
   
   var controller = ['$scope', '$uibModal', '$stateParams','$state','$timeout','CodesService','ResourcesDataService','$rootScope','CODES_CONST','ProcessingService','MessagingService','GridsterService','TaskDataService','WidgetDataExchangeService',
                     function ($scope,$uibModal,$stateParams,$state,$timeout,CodesService,ResourcesDataService,$rootScope,CODES_CONST,ProcessingService,MessagingService,GridsterService,TaskDataService,WidgetDataExchangeService) {
      
    $scope.searchFormDownloaded = false;
    $scope.src_type_text=CODES_CONST._POPULATE_TYPE_   
     
    $scope.widgetType = 1;
    $scope.animationsEnabled = true;
     
          $scope.gridsterType="resource";
           $scope.gridsterOpts = GridsterService.getGridsterOptions();
          
  
  $scope.gsrvc = GridsterService;
  
  $scope.prevTaskPackage = $stateParams.prev_task_package;
  
  $scope.dashboard = {
  				id: '1',
  				name: 'Home',
  				widgets: GridsterService.getEmptyDatasetForm()
				} ;
      $scope.localVars.datasetMapperType = 0;
     //register widget
     var out_fields = ['resourceRecordId']
       var widgetDefinition = {DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:''}]}],actions: [{name:'PrepareData',status:false}],name: 'updateResourceWidget',widgetId: $scope.fd.presentationId,taskId:  $scope.lTaskId,
       resourceDefId : $scope.fd.related, //resourceDefId : $scope.selectedResource
       resourceRecordId:$scope.resourceRecordId,task_initiate_id:$scope.taskInitiateId,task_execute_id:$scope.taskExecuteId, output:out_fields };
      $scope.localWidgetId = WidgetDataExchangeService.registerWidget(widgetDefinition);
       $scope.fd.localId=$scope.localWidgetId;  
 
        $scope.taskId=$scope.resourceDefinition.ResourceDefId;
        if ($scope.taskId==undefined)  $scope.taskId=$scope.resourceDefinition.ResourceDefId_id;
        if ($scope.taskId==undefined) $scope.taskId=0;
        
        $scope.parentDatasetDefId=$scope.fd.related;
        $scope.prevTaskExeId =  $scope.resourceDefinition.prev_task_id;
        $scope.formType=$scope.resourceDefinition.form_type;
        $scope.source_type=$scope.resourceDefinition.source_type;
        $scope.taskInitiateId = $scope.resourceDefinition.task_initiate_id;
        $scope.taskExecuteId = $scope.resourceDefinition.task_execute_id;
        $scope.formDefinition = $scope.resourceDefinition.form_definition;
                
        if ($scope.taskInitiateId==undefined)
       {
        $scope.taskInitiateId =0;
        
       }
       if ($scope.taskExecuteId==undefined)
       {
        $scope.taskExecuteId =0;
        
       }
        if ($scope.prevTaskExeId==undefined)
        {
          $scope.prevTaskExeId =0;
        }
        
        
        
        if ($scope.source_type=="fromOutcome")
        {
           $scope.resourceRecordId = WidgetDataExchangeService.getDataByWidgetId($scope.prevTaskExeId,$scope.resourceDefinition.task_initiate_id,$scope.resourceDefinition.prev_task_id,$scope.fd.parentElementId,'resourceRecordId');
        }
         
        if ($scope.resourceRecordId==undefined)
         {
          $scope.resourceRecordId = 0; 
         }
          
         $scope.getFormResourceOriginal = function(){
              var lang=$rootScope.globals.currentUser.lang;
              if ($scope.fd.presentationId==null)
              {
                $scope.fd.presentationId = 0;
                
              }
              ResourcesDataService.getResourceForm($scope.fd.related,"i",lang).then(function(data) {
                            
                                $scope.resourceWidgetDefinition= data.data.resourceDefinition;
                               
                                if ( $scope.taskInitiateId==0 && $scope.taskExecuteId==0)
                                {
                                 $scope.dashboard.widgets.layout=[data.data.widgets];
                                }
                                 $scope.selectedResourceItem = {'name':$scope.resourceWidgetDefinition.name,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                            
                            
                               
                                $scope.fd.wItemValue= $scope.dashboard.widgets.layout;
                                
                                //MIGOR BITNO ZA BROADCAST - ktTaskUpdateResource 
                                //WidgetDataExchangeService.setData($scope.widgetId,'resourceRecordId',{'resourceRecordId':$scope.fd.wItemValue.resourceRecordId});
                                 //ovo je da trenutno radi
                                WidgetDataExchangeService.setData({DEI: [{'name':'resourceLink','value':[{name:'resourceId',value:$scope.fd.related},{name:'resourceRecordId',value:$scope.resourceRecordId}]}],widgetId: $scope.fd.presentationId});
								
                                
                                
                                   $timeout(function() {
                                    $scope.formLoaded=true;
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
                              
      };
         
        $scope.getFormResource = function(type){
              var lang=$rootScope.globals.currentUser.lang;
             
              if ($scope.fd.presentationId==null) {
                $scope.fd.presentationId=0;
              }
              ResourcesDataService.getResourceFormExtendedMultiple($scope.fd.related,$scope.taskId,$scope.formType,$scope.fd.presentationId,["w"],lang).then(function(data) {
                               
                          $scope.resourceWidgetDefinition= data.data.resourceDefinition;
                          //$scope.dashboard.widgetsTemplate = angular.copy(data.data.widgets[0]);
                         // $scope.dashboard.widgetsFilter= data.data.widgets.widgetsf;
                           
                          $scope.selectedResourceItem = {'name':$scope.resourceWidgetDefinition.name,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                          if (type==0)
                          {
                            $scope.dashboard.widgets.layout.push(angular.copy(data.data.widgets.widgetsw));
                            $scope.fd.wItemValue= $scope.dashboard.widgets.layout;
                             
                          }else
                          {
                             if ( $scope.taskInitiateId==0 && $scope.taskExecuteId==0)
                                  {
                                  $scope.dashboard.widgets.layout = [angular.copy(data.data.widgets.widgetsw)];
                                  $scope.fd.wItemValue=$scope.dashboard.widgets.layout;
                              }else
                              {
                                 $scope.dashboard.widgets.layout=angular.copy(data.data.widgets.widgetsw);
                                 $scope.fd.wItemValue=$scope.dashboard.widgets.layout;
                              }
                          }
                          //$scope.fd.fItemValue= $scope.dashboard.widgetsFilter;
                        
                          
                           $timeout(function() {
                                     $scope.formDownloaded = true;
                                 //   $scope.setRequired($scope.fd.fItemValue,false);
                                                                     
                                    });
                           
                      
                  },function(error){
                  MessagingService.addMessage(error.msg,'error');
                                 
                                 
                                
              });
                             
      };
        
        $scope.addNewItem = function(){
         $scope.getFormResource(0); 
        }
       
        $scope.selectedDatasetRecords=[];
        $scope.getFormResourceWithData = function(){
              var lang=$rootScope.globals.currentUser.lang;
              $scope.updateFormLoaded=false;
              if ($scope.fd.presentationId==null)
              {
                $scope.fd.presentationId = 0;
                
              }
              
              if ($scope.fd.parentElementId==null)
              {
                $scope.fd.parentElementId = 0;
                
              }
              //$scope.selectedDatasetRecords = $scope.getSelectedRecordsFromSource();
              $scope.selectedDatasetRecords = [];
               
                $scope.filter = $scope.prevTaskPackage;
                
              
              ResourcesDataService.getResourceFormAddMultiple($scope.fd.related,$scope.taskInitiateId,$scope.taskExecuteId, $scope.fd.parentElementId,$scope.fd.presentationId,$scope.prevTaskExeId,$scope.formType,$scope.widgetType, "w",$scope.filter,lang).then(function(data) {
                                //$scope.formLocked = false;
                                  $scope.resourceWidgetDefinition= data.data.resourceDefinition;
                                  $scope.dashboard.widgets.layout=data.data.widgets;
                                  //$scope.dashboard.widgetsTemplate = angular.copy(data.data.widgets[0]);
                                  //$scope.dashboard.widgetsFilter=data.data.widgetsf;
                                  $scope.resourceRecordIds = data.data.resourceRecordId;
                                  if ($scope.resourceRecordIds.length>0)
                                  {
                                    $scope.resourceRecordId=$scope.resourceRecordId[0];
                                  }
                                  $scope.selectedResourceItem = {'name':$scope.resourceWidgetDefinition.name,'relatedId':$scope.fd.related,'presentationId':$scope.fd.presentationId,'src_type_id':$scope.fd.populateType,'src_type_text':$scope.localVars.src_type_text[$scope.fd.populateType],'parentElementId':$scope.fd.parentElementId};
                                /*
                                if ($scope.formLocked) {
                                  //code
                                  $scope.dashboard.widgets= GridsterService.ProcessWidgets($scope.dashboard.widgetsOriginal);
                                }else
                                {
                                  
                                  $scope.dashboard.widgets =  $scope.dashboard.widgetsOriginal;
                                }
                               */
                                $scope.fd.wItemValue=$scope.dashboard.widgets.layout;
                               // $scope.fd.fItemValue=data.data.widgetsf;
                                
                                
                                   $timeout(function() {
                                     $scope.formDownloaded = true;
                                  
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
               } 
                              
               
     
       $scope.saveDatasetRecordsMultiple = function(){
              //var lang=$rootScope.globals.currentUser.lang;
              //$scope.updateFormLoaded=false;
               
              ResourcesDataService.saveDatasetRecordsMultiple($scope.fd.related,$scope.fd.wItemValue).then(function(data) {
                             
                                
                                   MessagingService.addMessage(data.msg,'success');
                                   //$scope.dashboard.widgets.length=0;
                                  // $scope.getResourceDataFiltered();                               
                                   $timeout(function() {
                                    //$scope.formLoaded=true;
                                    //$scope.updateFormLoaded=true;
                                     
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
              
                              
               
      };
      
      
     $scope.gridOptionsResourceData = {
        columnDefs: [],
        data:[],
        enableGridMenu: true,
        showGridFooter: true,
        showColumnFooter: true,
        enableRowSelection: true,
        multiSelect: true,
        enableHorizontalScrollbar : 1,
        enableVerticalScrollbar : 2,
        enableFiltering: true,
        enableRowHeaderSelection: true,
        enableColumnResizing: true,
        enableColumnReordering: true,
      //exporterCsvFilename: 'myFile.csv',
     /*
        paginationPageSizes: [5, 10, 20, 50, 100],
         paginationPageSize: 5,
       enableSelectAll: false,
       enableCellEdit: false*/

   }
      
      
      $scope.gridOptionsResourceData.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
           
          //gridApi.selection.on.rowSelectionChanged($scope,function(row){

         
                  
              
         // });
     }
     
   
      
       $scope.getResourceDataFiltered = function () {

            $scope.filter = {};
            //$scope.gridOptionsResourceData.columnDefs.length=0;
            //$scope.gridOptionsResourceData.data.length=0;
            var lang = $rootScope.globals.currentUser.lang;
            $scope.dataDownloading=true;
          if ($scope.selectedResource==null) {
            $scope.selectedResource = $scope.fd.related;
          }
          
         ResourcesDataService.getResourceDataFiltered($scope.selectedResource,$scope.taskId,lang,$scope.dashboard.widgetsFilter.layout).then(function(data) { 
                            
                                  $scope.gridOptionsResourceData.data = data.data;
                                  $scope.gridOptionsResourceData.columnDefs = data.columnsDef;
                                  
                                    $timeout(function(){
                                      $scope.resourceDataDownloaded = true;
                                      $scope.restoreGridState();
                                    });
                                 
                                     
                                  },function(error){
                                MessagingService.addMessage(error.msg,'error');
                             });
              $scope.dataDownloading=false;
        }
    
    
    $scope.saveDatasetRecordsMultiple = function(){
              //var lang=$rootScope.globals.currentUser.lang;
              //$scope.updateFormLoaded=false;
               
              ResourcesDataService.saveDatasetRecordsMultiple($scope.fd.related,$scope.fd.wItemValue).then(function(data) {
                             
                                
                                   MessagingService.addMessage(data.msg,'success');
                                   $scope.dashboard.widgets.layout.length=0;
                                   $scope.getResourceDataFiltered();                               
                                   $timeout(function() {
                                    //$scope.formLoaded=true;
                                    //$scope.updateFormLoaded=true;
                                     
                                     
                                    });
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                             });
              
                              
               
      };
    
    
    $scope.saveGridState= function() {
       var state = $scope.gridApi.saveState.save();
       $scope.fd.gridState = state;
       MessagingService.addMessage("To make changes stick please save form",'info');
                 
    }
  
   $scope.restoreGridState= function() {
      if ($scope.fd.gridState!=null)
      {
         if ($scope.gridApi!=undefined)
         {
      
          $scope.gridApi.saveState.restore($scope,$scope.fd.gridState);
        
         }else
         {
        $timeout(function(){
            $scope.restoreGridState();
          });
         }
      }
    }
       $scope.setReadOnly = function(form,readOnly)
        {
          if (form!=undefined){
     					for(var i=0;i<form.length;i++) {
                
                    if ('readOnly' in form[i])
                    {
                      form[i].readOnly = readOnly;
                     }
                      if ('layout' in form[i])
                      {
                           
                         $scope.setReadOnly(form[i].layout,readOnly);
                      }
              }
          }
        };
        
    $scope.setRequired = function(form,required)
        {
          if (form!=undefined){
     					for(var i=0;i<form.length;i++) {
                
                    if ('required' in form[i])
                    {
                      form[i].required = required;
                     }
                      if ('layout' in form[i])
                      {
                           
                         $scope.setRequired(form[i].layout,required);
                      }
              }
          }
        }
  
 // $scope.getFormResource();
   
  if ($scope.fd.related!=null &&  !$scope.formDefinition && !$scope.formDownloaded)
  {
     $scope.getFormResourceWithData();
    
  }else if ($scope.fd.related!=null)
  {
     $scope.getFormResource();
  }
  
      }];

  return {
            restrict: 'A',
            templateUrl: 'ktdirectivetemplates/ktTaskAddMultipleResources.html',
            controller: controller,
            link: function (scope, elem, attrs) {

            scope.resourceData = {};
  
            scope.prepareResourceData = function(resourceRecordId){
           
     
                scope.fd.itemValue =  ResourcesDataService.prepareResourceWidgetData(scope.dashboard.widgets.layout,resourceRecordId);
  
          }
          
          
           scope.processWidgetDependencies = function() { //-----  ktUploader  -----
              
           if (scope.fd.tItemValue!=undefined) 
            for (var i=0;i< scope.fd.tItemValue.length;i++) {
                var linkedWidgetId=scope.fd.tItemValue[i].widgetId;  // linkedWidgetId= WidgetDataExchangeService.getLocalWidgetId(linkedWidgetId);//MIGOR - moram dobiti localwidgetId od vezanog WidgetId 
                var linkedItem = 'resourceLink'                      //var linkedItem = scope.fd.tItemValue[i].name;
                  
                if (linkedWidgetId!=undefined && linkedWidgetId!=null ) {
                    for (var j=0;j<scope.fd.tItemValue[i].DEI.length;j++) {
                        if ((scope.fd.tItemValue[i].DEI[j].name==linkedItem) && (scope.fd.tItemValue[i].DEI[j].value==true)) { //SCOPE.ON samo kad je DEI value = true
                             var linkedItem = scope.fd.tItemValue[i].DEI[j].name;  
                             console.log("view "+scope.fd.presentationId+" se veze na serverski " + linkedWidgetId); 
                             
                             scope.$on('Event'+linkedWidgetId+linkedItem,function(event, data){  //ide po svim vezanim widgetima i dodaj scope.on   //MIGOR TODO - DODATI SAMO U SLUCAJU DA JE DEI VALUE = TRUE !!!!     <<<<<<<<<<<<< !!!!!!!!!
                                
                                console.log("view "+ scope.fd.presentationId+" dobio event " + event.name);
                                for (var k=0;k<data.length;k++) {
                                  if (data[k].name=='resourceRecordId')
                                  {
                                    scope.fd.linkedResourceId = data[k].value; //TODO - ILJ - ovo bi isto trebala biti varijabla - sta ako promjenim interface odnosno naziv polja!
                                    scope.resourceRecordId = scope.fd.linkedResourceId;
                                  }
                                  if (data[k].name=='resourceId') scope.fd.linkedResourceDefId = data[k].value;  //TODO- ILJ - ovo bi isto trebala biti varijabla
                                }
                          
                              //get selectedRecord
                              //scope.getFormResourceWithData();
                                
                                    //	data: {resource_def_id:scope.fd.linkedResourceDefId,resource_id:scope.fd.linkedResourceId}
                                    
                              }) //on_END
                        }//if
                    }//for j
                }//if

             // else {  scope.fd.tItemValue.splice(i,1);  } // nije mi jasno zasto sam radio SPLICE
            } //for i
      }//fn  pr0cessWidgetDependecies -------------    ktViewSingleResourceWidget

          if (scope.fd.presentationId==undefined || scope.fd.presentationId==null) scope.fd.presentationId=0;
          
          scope.formType=scope.resourceDefinition.form_type;
          TaskDataService.getWidgetDependencies(scope.taskId,scope.fd.presentationId,scope.formType).then(function(data) {
                               
                                scope.fd.tItemValue=data.data;
                               // scope.processWidgetDependencies();
                            
                                },function(error){
                                   MessagingService.addMessage(error.msg,'error');
                                
                                });
          
          
          scope.$on('InitiateTaskPostProcess',function(event, data){
              
              //find data
            //  var item = data;
              //send data to widget exchange service
              //WidgetDataExchangeService.setData(item);
              scope.setReadOnly(scope.dashboard.widgets.layout,true);
          });
  
          }
    } 
        
};