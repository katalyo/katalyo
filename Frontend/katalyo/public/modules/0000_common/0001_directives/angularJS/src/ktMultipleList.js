'use strict';

export function multipleList (){ 
   return {
      require: 'ngModel',
      link: function(scope, elem, attr,ngModel) {
        
      //MIGOR START
              scope.search=[
				{id:1,filter:'onlist',name:"On list:",icon:"fa fa-chevron-right"},
				{id:2,filter:'notonlist',name:"Not on list:",icon:"fa fa-expand"},
                ];
				
              // if (scope.fd!=undefined) scope.fd.search=scope.search[0]; //MIGOR TODO - greska kod incijacije za multiple , mozda treba kod promjene na multiple ovo jos jednom okinuti
                 
              //MIGOR END
          var lenList = attr.multipleList;
          scope.doneBlur = false;
          if (lenList == undefined)
          {
            lenList = 0;
          }
        
        scope.updateValues=function(){
         if (scope.fd!=undefined)
         {
           if (scope.formData[0]['Field'+scope.fd.itemId]==undefined || scope.formData[0]['Field'+scope.fd.itemId]==null)
           {
          
             scope.formData[0]['Field'+scope.fd.itemId]=[];
       
           }
           scope.$select.selectedItemsMultipleList= scope.formData[0]['Field'+scope.fd.itemId];
           
           if (scope.formData[0]['Field'+scope.fd.itemId].length>0)
           {
              scope.fd.search.value=scope.formData[0]['Field'+scope.fd.itemId]; //MIGOR
           }
         }else
         {
          scope.$select.selectedItemsMultipleList=scope.selectedItems;
          
         }
         
        };
        
       // scope.updateValues();
        
        scope.$watch('selectedItems', function(){
               
               scope.updateValues();

        });
    
          /*function for multiple list START ILJ*/
          scope.removeItemFromList =  function(index){
              
              if (scope.formData[0]['Field'+scope.fd.itemId]!=undefined)
              {
                scope.formData[0]['Field'+scope.fd.itemId].splice(index,1);
              }else
              {
                
                scope.selectedItems.splice(index,1);
              }
              scope.checkValidity(); 
          if (scope.fd!=undefined) scope.fd.search.value=scope.formData[0]['Field'+scope.fd.itemId]; //MIGOR
        

          };
        
            
       scope.addItemToList =  function(item) {
        
        if (item!=null)
        {
          var pos;
          if (scope.formData[0]['Field'+scope.fd.itemId]!=undefined)
          {
           pos = scope.formData[0]['Field'+scope.fd.itemId].map(function(e) { return e.id; }).indexOf(item.id);
              
              if (pos<0){
              
                 scope.formData[0]['Field'+scope.fd.itemId].push(angular.copy(item));
                 
                
                
              }
          }else
          {
              if (scope.selectedItems==undefined) scope.selectedItems = [];
              pos = scope.selectedItems.map(function(e) { return e.id; }).indexOf(item.id);
              
              if (pos<0){
               
                 scope.selectedItems.push(angular.copy(item));
                
          
              }
          }
              if (scope.doneBlur)
              {
               scope.checkValidity(); 
              }
              if (scope.fd!=undefined) scope.fd.search.value=scope.formData[0]['Field'+scope.fd.itemId]; //MIGOR
        }
   

      };
    scope.checkValidity = function(){
        scope.doneBlur = true;
        var valid;
        if (scope.fd!=undefined)
        {
          valid = (!scope.fd.required) || (scope.formData[0]['Field'+scope.fd.itemId].length >= lenList);
        }else
        {
          valid = (!scope.required) || (scope.selectedItems.length >= lenList);
        }
        ngModel.$setValidity('multipleList', valid);
      
    };
    /*function for multiple list END*/   
      }
   };
};