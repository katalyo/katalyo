'use strict';

export function ktTaskForm () {

        return {
            restrict: 'A',
             templateUrl: 'ktdirectivetemplates/ktTaskForm.html',
             controller: function($scope,TaskDefinitionService,TaskDataService,MessagingService,GridsterService,WidgetRegisterService,ProcessingService,
              ResourcesDataService,KatalyoStateManager,WidgetDataExchangeService,dragulaService,$stateParams,$rootScope,$timeout){
                $scope.tds=TaskDefinitionService;
                
                $scope.resourceObject = $stateParams.resource;
                $scope.CTRL='ktTaskForm'
                $scope.formLoaded=false;
                $scope.formVersion={};
                $scope.resourceId=$stateParams.id;
                $scope.gsrvc = GridsterService;
                $scope.showFieldProperties = false;
                $scope.formType=$scope.tds.showTaskFormFn();

                $scope.taskDefinition.ResourceDefId_id=$scope.resourceId;
                $scope.taskDefinition.form_type=$scope.formType;
                $scope.taskDefinition.form_definition=true;

                $scope.gridsterType="task";
                $scope.refreshForm=false;
                $scope.dashboard = {'i':{},'e':{}};
                $scope.reloadTaskToolbox = false;
                if ($scope.localVars==undefined) $scope.localVars={};
                
                $scope.localVars.cbAutoExecuteChanged = false;
                $scope.localVars.cbAutoInitiateChanged = false;
                // $scope.gsrvc.destroyDragula('bag-columns');
                // $scope.gsrvc.destroyDragula('bag-rows-'+$scope.gridsterType);
                 
                 //$rootScope.$emit('RegisterTaskWidgets',null);
                 
                // WidgetRegisterService.ListenToEvents();
                // WidgetRegisterService.EmitRegisterTaskWidgetEvent();
                 
                 WidgetDataExchangeService.deRegisterAllWidgets();	
                 
                 $scope.hideTabIndex = true;
                 $scope.outcomes = {outcomeCodesDetails:[]};
                 $scope.outcomes.selectedOutcomeIndex=-1;
                 $scope.dashboardLocal ={widgets:{layout: GridsterService.getEmptyTaskForm(),data:{}}};
                 var lang=$rootScope.globals.currentUser.lang;
                
                $scope.ksm = KatalyoStateManager;

                var form_saved = function(data)
                    {
                        formDefinitionState.subject.setStateChanges = data;
                        formDefinitionState.subject.notifyObservers("formSaved");
                    };
                
               
                var formDefinitionState,formDefinitionStateName;   
                $scope.setupKatalyoStateManager = function()
                {
                    formDefinitionState= $scope.ksm.addSubject('formDefinitionState'+$scope.resourceId+$scope.taskDefinition.form_type,null);
                    formDefinitionStateName = formDefinitionState.subject.name;
                    
                    let action_value_changed = $scope.ksm.addAction(formDefinitionStateName,"formSaved",form_saved);
                    
                }
                 $scope.setupKatalyoStateManager();
                 
                $scope.ShowFormView = function(viewId,locked)
                {
                   //$timeout(function(){
                    if ($scope.taskDefinition.formParams===undefined) 
                    {
                       $scope.taskDefinition.formParams={} 
                    }
                       
                        $scope.taskDefinition.formParams.formViewType=viewId;
                        $scope.gsrvc.setFormViewType($scope.taskDefinition.formParams.formViewType);

                  //  })
                  
                         
                };
            
           
                $scope.showHideTaskForm = function(form_type)
                {
                        
                        $scope.tds.showHideTaskForm(form_type);
                        $scope.resourceDefinition.form_type=form_type;
                        $scope.ShowFormView(1,false);
                        $scope.setupKatalyoStateManager();
                        $scope.ksm.executeAction("taskDefinitionState"+$scope.resourceId,"formTypeChanged",form_type)
             
                };
                      
                $scope.showHideProperties = function(value)
                {
                        $scope.gsrvc.setFieldPropertiesOpened(value);
                        $scope.showFieldProperties=$scope.gsrvc.getFieldPropertiesOpened();
             
                };
            
            $scope.$watch('gsrvc.getFieldPropertiesOpened()',function(newValue,oldValue){
                    
                    if (newValue!=oldValue)
                    {
                     $scope.selectedField=$scope.gsrvc.getFieldProperties();
                     $scope.showFieldProperties = $scope.gsrvc.getFieldPropertiesOpened();
                    }
            });
            
            
             $scope.$watch('gsrvc.getFieldProperties()',function(newValue,oldValue){
                    
                    if (newValue!=oldValue)
                    {
                     $scope.selectedField=$scope.gsrvc.getFieldProperties();
                     $scope.showFieldProperties = $scope.gsrvc.getFieldPropertiesOpened();
                    }
            });
   
   
          
      //save task parameters
      /*
      $scope.saveTaskParameters = function(){
        
      
        
        TaskDataService.SaveTaskParameters($scope.taskDefinition.id,$scope.taskDefinition).then(function(data) {
 
                                  MessagingService.addMessage(data.msg,'success');
                                  $scope.taskDefinition = data.data;
                                  $scope.taskDefinition.form_type=$scope.formType;
                                  $scope.taskOriginal=angular.copy($scope.taskDefinition);
                                  $scope.localVars.cbAutoInitiateChanged = false;
                                  $scope.localVars.cbAutoExecuteChanged = false;
                            },function(error){     
                                  MessagingService.addMessage(error.msg,'error');   
                             });  
      };
         
      */

  
  
  /*
             $scope.getTaskParameters = function(){
                TaskDataService.GetTaskParameters($scope.resourceId).then(function(data) {
                                         //  $scope.ac.alerts = data.data;
                                      
                                      if (data.length>0) {
                                                                               
                                          $scope.taskDefinition = data[0];
                                          $scope.taskDefinition.form_type=$scope.formType;
                                          $scope.taskDefinition.form_definition=true;

                                          $scope.getResourceForm();

                                          if (!$scope.taskDefinition.InfiniteTask)
                                          {
                                                 //get Task Outcomes 
                                              if ($scope.taskDefinition.TaskOutcomeHeadId == null || $scope.taskDefinition.TaskOutcomeHeadId==undefined)
                                                  {
                                                   $scope.taskDefinition.TaskOutcomeHeadId=0; //vidjeti da li ovo treba MIGOR
                                                  }
                                         
                                            
                                              //TaskDataService.GetTaskCodesActionsById($scope.resourceId,lang,$scope.taskDefinition.TaskOutcomeHeadId).then(function(data) {
                                              if ($scope.form_type==="e")
                                              {
                                                TaskDataService.GetTaskOutcomes($scope.resourceId,$scope.taskDefinition.TaskOutcomeHeadId,lang).then(function(data) {            
                                                                        $scope.outcomes.outcomeCodesDetails = data.data;
                                                                        $scope.taskDefinition = {outcomes:{outcomeCodesDetails:data.data}};
                                                                        //prikazi defaultno detalje od prvog
                                                                       // if ($scope.outcomes.outcomeCodesDetails.length>0)   $scope.clickedOutcome = $scope.outcomes.outcomeCodesDetails[0];
                                                                        
                                                                  },function(error){
                                                                        MessagingService.addMessage(error.msg,'error');
                                                                    });
                                              }
                                          }
                                          
                                            
                                      }  
                                   },function(error){
                                    
                                    MessagingService.addMessage(error.msg,'error');
                                    });
                                     
                                  
             };
        */        
     

            $scope.formFromServerLoaded = {};
            
            $scope.localVars.getResourceForm = function (afterSave,afterSaveMsg) {
                  if (!$scope.formFromServerLoaded[$scope.formType])
                  {
                    ResourcesDataService.getResourceForm($scope.resourceId,$scope.formType,lang).then(function(data) {
                                      
                                         var resourceFormData = data.data
                                         $scope.resourceDefinition = resourceFormData.resourceDefinition
                                          ResourcesDataService.GetWidgetToolbox($scope.resourceDefinition.ResourceCode).then(
                                          
                                          function (widgetData) {
                                            
                                             $scope.gsrvc.setTaskToolboxItems(widgetData.data);
                                                
                                                
                                              if (resourceFormData.widgets.length>0) {
                                                $scope.dashboard[$scope.formType]={widgets:{layout:resourceFormData.widgets}}
                                             }else {
                                              $scope.dashboard[$scope.formType] = {widgets:{layout:$scope.resourceDefinition.ResourceExtended.DefaultForm}}
                                             }
                                            
                                             $scope.formVersion [$scope.formType] = $scope.resourceDefinition.FormVersion;
                                            
                                             $scope.gsrvc.setTaskFormVersion($scope.resourceDefinition.id+$scope.formType,$scope.formVersion[$scope.formType]); 

                                             $scope.resourceDefinition.formParams={formViewType:1,showGridNavigation:false};
                                             if ($scope.gsrvc.formLocked == true)   $scope.resourceDefinition.formParams.formViewType = 2;
                                            
                                           
                                             $scope.formLoaded=true;
                                             $scope.formFromServerLoaded[$scope.formType] = true;
                                             $scope.ShowFormView($scope.resourceDefinition.formParams.formViewType,false);
                                             //debugger; //getResourceForm migor
                                        
                                        
                                            
                                         },function(error){
                                             MessagingService.addMessage(error.msg,'error');  
                                         }); 
                                         
                                         
                        

                                         
                                         
                                         if (afterSave)
                                         {
                                           MessagingService.addMessage(afterSaveMsg,'success');
                                          
                                         }
                                      },function(error){
                                   
                                        
                                       MessagingService.addMessage(error.msg,'error');
                                      
                                      
                                   });
                  }
            };
        
           
        //**********************************************************
          //           SAVE TASK FORM DEFINITION
          //**********************************************************
      
        $scope.saveTaskForm = function () {
 
               $scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving task form for '+$scope.resourceId);
               $scope.savingTaskForm = true;
               ResourcesDataService.saveResourceForm($scope.dashboard[$scope.formType].widgets.layout,$scope.resourceId,$scope.formType).then(function(data) {
                                
                                        MessagingService.addMessage(data.msg,data.msgType);
                                        $scope.formVersion [$scope.formType] = data.version;
                                        $scope.resourceDefinition.FormVersion=$scope.formVersion [$scope.formType]
                                        $scope.gsrvc.setTaskFormVersion($scope.resourceDefinition.id+$scope.formType,$scope.formVersion[$scope.formType]); 
                                        $scope.ksm.executeAction(formDefinitionStateName,"formSaved",data.new_items);
                                        $scope.savingTaskForm = false;
                                        let rememberTaskFormFn = $scope.tds.showTaskFormFn()
                          
                                        if (data.data.formData.length>0) {
                                            $scope.dashboard[$scope.formType]={widgets:{layout:data.data.formData}}
                                            $scope.tds.showHideTaskForm('')
                                            $timeout(function(){ 
                                                 $scope.tds.showHideTaskForm(rememberTaskFormFn)
                                            })
                                            
                                        }   
  
                                        $scope.formLoaded=true;
                                        $scope.formFromServerLoaded[$scope.formType] = true;
                                        // $scope.ShowFormView($scope.resourceDefinition.formParams.formViewType,false);
                                         
        
                                   },function(error){
                                     
                                       $scope.savingTaskForm = false;
                                       MessagingService.addMessage(error.msg,'error');
                                      
                                    });
                                      
                                  
              };
              
         //**********************************************************
          //           SAVE TASK BUTTONS
          //**********************************************************      
        $scope.saveTaskBtns = function () {
       
          
               //$scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving task btns for '+$scope.resourceId);
               
               TaskDataService.SaveTaskBtns($scope.resourceDefinition.initTaskBtnDef,$scope.resourceId,lang).then(function(data) {
                                       
                                        
                                       MessagingService.addMessage(data.msg,'success');
                                  
                                   },function(error){
                                        
                                       MessagingService.addMessage(error.msg,'error');
                                      
                                    });
                                      
                                  
       
              };
              

             
                             
            $scope.$on('CtrlPBPressed',function(event,data){
                  
                var key=data;
                if (key=='b' || key=='B')
                {
                    if ($scope.resourceDefinition.formParams.formViewType!=1) $scope.ShowFormView(1);
            
                }
                if (key=='p' || key=='P')
                {
                    if ($scope.resourceDefinition.formParams.formViewType!=2) $scope.ShowFormView(2);
                }
                  
            });  
          
           //**********************************************************
          //           CALL FUNCTIONS FOR FETCHING TASK FORM, PARAMETERS, OUTCOMES
          //**********************************************************
          
           $scope.$watch('tds.showTaskFormFn()',function(newValue,oldValue){
                    
                     $scope.formType = newValue;
                     //$scope.ksm.executeAction("taskToolBox","toolboxLoaded",{widget_id:null,state:false});
                     
                     if ($scope.resourceId != undefined)
                        {
                            $timeout(function() {
                                $scope.localVars.getTaskParameters()
                            })
                            
                           
                        }
            
                        
                    
                  });
           
            
                
                },
             link: function (scope, elem, attrs) {
              
            
            
             
                  
                  
            }
        }
};