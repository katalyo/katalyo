'use strict';

export function ktDropdown ($timeout) {

        return {
            restrict: 'EA',
            require:['^form','ngModel'],
            templateUrl: 'ktdirectivetemplates/ktDropdown.html',
            controller: function($scope)
            {
               
                $scope.processKeyboardEntry = function(ev,index)
                {
                    let code = ev.keyCode || ev.which;
                    let key = ev.key;

                    //enter
                    if (code==13 && (ev.currentTarget.type=="button" || ev.currentTarget.type=="text")){
                        //move focus to next element
                        ev.preventDefault();
                        let next=index+1;
                        let id=$scope.parentFormId+"."+$scope.dropdownId+".DropdownItem"+next;
                        let elem = document.getElementById(id);
                        if (elem!=null) elem.focus(); 
                    }
                    //keydown
                    if (code==40){
                      //move focus to next element
                       ev.preventDefault();
                       let next=index+1;
                       let id=$scope.parentFormId+"."+$scope.dropdownId+".DropdownItem"+next;
                       let elem = document.getElementById(id);
                       if (elem!=null) elem.focus(); 
                    }
                    //keyup
                    if (code==38){
                      //move focus to previous element
                       ev.preventDefault();
                       let prev=index-1;
                       let elemDef = $scope.parentFormId+"."+$scope.dropdownId+".DropdownItem"+prev;
                       if (prev==-1) elemDef = $scope.parentFormId+"."+$scope.dropdownId+"search";
                       let elem = document.getElementById(elemDef);
                       
                       if (elem!=null) elem.focus(); 
                    }
                }
                
                /*
                $scope.$watch('selectedItems',function(newValue,oldValue){
                    if (newValue!=oldValue)
                    {
                            $scope.selectItem(null,newValue);    
                    
                    }
                });
                */
                $scope.selectItem = function(ev,item)
                {
                    if (item!=undefined)
                    {
                        if (!$scope.isMultiple){
                          $scope.selectedItem= item;
                          $scope.ngModel.$setViewValue($scope.selectedItem);
                          //ngModel.$validate();
                          let elemBtn = document.getElementById($scope.parentFormId+"."+$scope.dropdownId)
                          if (ev!=undefined) elemBtn.focus();
                          $scope.onSelectFn({item:$scope.selectedItem,model:$scope.ngModel});
                        }else
                        {
                          if (ev!=undefined) ev.stopPropagation();
                          if ($scope.selectedItems==null || $scope.selectedItems==undefined || !Array.isArray($scope.selectedItems)) $scope.selectedItems=[];
                          let pos = $scope.selectedItems.map(function(e) { return e.id; }).indexOf(item.id);
                          if (pos<0) $scope.selectedItems.push(item);
                          else $scope.selectedItems.splice(pos,1);
                          $scope.ngModel.$setViewValue($scope.selectedItems);
                          $scope.onSelectFn({item:$scope.selectedItems,model:$scope.ngModel});
                        }
                        
                    }
                }
                
                 $scope.dropdownBlurred = function()
                {
                    let elemBtn = document.getElementById($scope.parentFormId+"."+$scope.dropdownId)
                    
                    $scope.ngModel.$setTouched();
                                      
                    let elemSearch = document.getElementById($scope.parentFormId+"."+$scope.dropdownId+"search")
                    if (elemSearch!=null) elemSearch.focus();
                  
                }
                
                $scope.removeItem = function(index)
                {
                    $scope.selectedItems.splice(index,1);
                    $scope.ngModel.$setViewValue($scope.selectedItems); 
                
                }
                
                $scope.clearOnly = function()
                {
                    
                        $scope.selectedItem = null;
                        //set ngModel to null
                        $scope.ngModel.$setViewValue(null);
                   
                }
                
                
                $scope.loadItems = function(items)
                {
                    if (items!=undefined)
                    {
                        $scope.dropdownItems = items;
                        if ($scope.search.searchFilter!=undefined && $scope.search.searchFilter!="") $scope.dropdownItemsFiltered = $scope.filterItems();
                        else $scope.dropdownItemsFiltered = angular.copy($scope.dropdownItems.slice(0,50));
                    }                      
                   
                }
                 $scope.loadItemsOnChange = function()
                {
                    if ($scope.dropdownItems!=undefined)
                    {
                        if ($scope.search.searchFilter!=undefined && $scope.search.searchFilter!="") $scope.dropdownItemsFiltered = $scope.filterItems();
                        else $scope.dropdownItemsFiltered = angular.copy($scope.dropdownItems.slice(0,50));
                    }  
                   
                }
                $scope.filterItems = function()
                {
                    if ($scope.search.searchFilter=="") return angular.copy($scope.dropdownItems.slice(0,50));
                    else{
                        $scope.dropdownItemsFiltered=[];
                        let temp = [];
                        for (let i=0;i<$scope.dropdownItems.length;i++)
                        {
                            if ($scope.dropdownItems[i].name.toLowerCase().indexOf($scope.search.searchFilter.toLowerCase())>-1) temp.push($scope.dropdownItems[i]);
                        }
                        return angular.copy(temp);
                    }
                }
                $scope.clear = function(ev)
                {
                    if (!$scope.isMultiple){
                        if (ev!=undefined) ev.preventDefault();
                        $scope.clearOnly();
                    }
                }
               
          
                /*
                $scope.clearMultiple = function()
                {
      
                  $scope.formData[0][$scope.fd.FieldName] = null;
                   $scope.fd.Search.value.length=0;
                }
                */ 
                
            },
            scope:{
            
            dropdownItems:'=',
            dropdownId:'=',
            parentFormId:'=',
            searchText:'=',
            isMultiple:'=',
            selectedItems:'=?',
            onSelectFn:'&',
            clearFn: '&',
            selectItemFn:'&',
            loadItemsFn:'&',
            size:'=?',
            },
             
            link: function (scope, elem, attrs,ctrls) {
                
                scope.form = ctrls[0];
                scope.ngModel= ctrls[1];
                scope.selectedItem = null; //{id:null,name:scope.searchText};
                scope.clearCloseLink="Clear";
                scope.search={};
                
                if (scope.dropdownItems!=undefined) scope.dropdownItemsFiltered = angular.copy(scope.dropdownItems.slice(0,50));
                
                scope.clearFn({theDirFn: scope.clearOnly});
                scope.selectItemFn({theSelectItemFn: scope.selectItem});
                scope.loadItemsFn({theLoadItemsFn: scope.loadItems});
                
                if (!scope.isMultiple)
                {
                    if (scope.selectedItems!="" && scope.selectedItems!=undefined && scope.selectedItems!=null) scope.selectedItem=scope.selectedItems;
                    scope.ngModel.$setViewValue(scope.selectedItem); 
                }else
                {
                    scope.ngModel.$setViewValue(scope.selectedItems);
                }
                
                scope.showSearchInput = false;
                if (scope.isMultiple) scope.clearCloseLink="Close";
                
                let dropdownShow = elem[0].getElementsByClassName("dropdown");
                
                
                angular.element(dropdownShow).on("shown.bs.dropdown", function(event){
                   $timeout(function(){
                     scope.showSearchInput = true;
                     let elementId = scope.parentFormId+"."+scope.dropdownId+"search";
                     let searchInput = document.getElementById(elementId);
                     if (searchInput!=null) searchInput.focus();
                    });
                });
                
                angular.element(dropdownShow).on("hidden.bs.dropdown", function(event){
                   $timeout(function(){
                     scope.showSearchInput = false;
                    
                    });
                });
               
                 $timeout(function(){
                            scope.showSearchInput = false;
                           
                        });
               
                 scope.isSelectedMultiple = function(item)
                {
                 
                    //let items = ngModel.$modelValue;
                    if (item!=undefined)
                    {
                        if (item.id!=undefined)
                        {
                            let items = scope.ngModel.$modelValue;
                            if (items!=undefined)
                            {
                                for (let i=0;i<items.length;i++)
                                {
                                    if (items[i]!=undefined)
                                    {
                                        if (items[i].id==item.id) return true;
                                    }
                                }
                            }
                        }
                    }
                    
                    return false;
                }
            }
        }
  
};