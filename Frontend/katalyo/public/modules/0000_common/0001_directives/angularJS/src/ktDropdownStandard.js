'use strict';

export function ktDropdownStandard ($timeout) {

        return {
            restrict: 'EA',
            require:['^form','ngModel'],
            templateUrl: 'ktdirectivetemplates/ktDropdownStandard.html',
            controller: function($scope,$element,$timeout)
            {
                if  ($scope.isMultiple==undefined) $scope.isMultiple = false 
                $scope.CTRL='ktDropdownStandard';
                $scope.search={searchFilter:''};
                $scope.model = {
                        selectedItems: $scope.selectedItems,
                        deletedItems: []
                        };
                if ($scope.dropdownItems==undefined) $scope.dropdownItems=[];  
                $scope.editMode=false;
            
                $scope.$watch("selectedItems",function(newValue,oldValue){  //MIGOR - iz parametra "selectedItems" puni selectedItems objekt iz dropdown elemenata - defaultna vrijednost
                   if (newValue!==undefined) {
                        //debugger //ktdropdownstandard selectedItems                      
                            if (oldValue?.id != newValue?.id) $scope.selectItemsOnLoad()
                        }     
                  
                });
                   
                $scope.dropdownItemsDict= {}
                
                $scope.$watch("dropdownItems",function(newValue,oldValue){  //MIGOR - iz parametra "selectedItems" puni selectedItems objekt iz dropdown elemenata - defaultna vrijednost 
                  if (newValue!==undefined && (newValue!==oldValue) ) {
                      
                      for (let i=0; i<newValue.length; i++) {
                          $scope.dropdownItemsDict[newValue[i].id] = i
                    }     
                }
                                      //debugger //ktdropdownstandard selectedItems
                      //$scope.selectItemsOnLoad()
                });
                
              
                $scope.selectItem = function()  {
                    //debugger //ktDropdownStandard  //selectItem 
                        if (!$scope.isMultiple && $scope.model.selectedItems!=undefined && $scope.model.selectedItems!=null){
                          if ($scope.model.selectedItems.id!==undefined && $scope.model.selectedItems.id===null)
                            {
                                $scope.model.selectedItems=null;
                                $scope.ngModel.$setViewValue(null);
                                let element = $element[0].getElementsByClassName('form-control');
                                $timeout(function(){
                                    element[0].selectedIndex =-1;
                                });
                            }
                          else $scope.ngModel.$setViewValue($scope.model.selectedItems);
                         
                        }else
                        {
                          if ($scope.model.selectedItems==null || $scope.model.selectedItems==undefined || !Array.isArray($scope.model.selectedItems)) $scope.model = {selectedItems:[]};
                          $scope.ngModel.$setViewValue($scope.model.selectedItems);
                          
                        }
                        $scope.onSelectFn({item:$scope.model.selectedItems , model:$scope.ngModel});
                        
                }
                
                $scope.setItem = function(item)
                {
                    //debugger
                    if (item.id!=undefined && item.id==null) $scope.model.selectedItems=item.id;
                    else $scope.model.selectedItems=item;           
                }
                
                
                
                $scope.removeItem = function(index)
                {
                    //debugger
                    $scope.model.deletedItems.push($scope.model.selectedItems[index]);
                    $scope.model.selectedItems.splice(index,1);
                    $scope.ngModel.$setViewValue($scope.model.selectedItems); 
                
                }
                
                $scope.clearOnly = function()
                {
                    //debugger
                        $scope.selectedItems = null;
                        //set ngModel to null
                        $scope.ngModel.$setViewValue(null);     
                }
                
                
                $scope.loadItems = function(items)
                {
                    //debugger
                    if (items!=undefined)
                    {
                        $scope.dropdownItems = items;
                        if ($scope.search.searchFilter!=undefined && $scope.search.searchFilter!="") $scope.dropdownItemsFiltered = $scope.filterItems();
                        else $scope.dropdownItemsFiltered = angular.copy($scope.dropdownItems.slice(0,50));
                    }                          
                }
                
                $scope.setEditMode = function(value)
                {
                    //debugger
                    $scope.editMode = value;
                    if (!value)
                    {   
                        for (let i = 0;i<$scope.model.deletedItems.length;i++)
                            $scope.model.selectedItems.push($scope.model.deletedItems[i]);  
                    }
                        
                    $scope.model.deletedItems.length=0;
                }
                
                $scope.acceptChanges = function()
                {
                    //debugger
                    $scope.editMode = false;
                    $scope.model.deletedItems.length=0;
                }
                
                $scope.selectItemsOnLoad = function() 
                {
                    let pos;
                   //debugger //selectItemsOnLoad
                    if ($scope.selectedItems!=undefined)
                    {
                        if ($scope.isMultiple)
                        {
                           $scope.model.selectedItems=[];
                           for(let j=0;j<$scope.selectedItems.length;j++)
                           {
                                if ($scope.selectedItems[j]!==null && $scope.dropdownItems!==undefined && $scope.dropdownItems.length>0)
                                {
                                 pos = $scope.dropdownItems.map(function(e) { return e.id; }).indexOf($scope.selectedItems[j].id);
                  
                                    if (pos>=0 && $scope.dropdownItems[pos].id!=null){
                                        $scope.model.selectedItems.push($scope.dropdownItems[pos]);
                                    }
                                    if ($scope.model.selectedItems.length==0) $scope.model.selectedItems = null;
                                }
                           }
                           
                        }
                        else
                        {
                            if ($scope.selectedItems!==null && $scope.dropdownItems!==undefined && $scope.dropdownItems.length>0)
                            {
                                pos = $scope.dropdownItems.map(function(e) { return e.id; }).indexOf($scope.selectedItems.id);
                                if (pos>=0 && $scope.dropdownItems[pos].id!=null){     
                                    $scope.model.selectedItems = $scope.dropdownItems[pos];
                                }
                                else
                                {
                                    $scope.model.selectedItems = null;
                                    
                                }
                            }
                        }
                        
                    }                      
                   //debugger
                };
                
               $scope.selectItemsOnLoad(); 
            },
            scope:{
                dropdownItems:'=',
                dropdownId:'=',
                parentFormId:'=',
                searchText:'=',
                isMultiple:'=',
                selectedItems:'=?',
                requiredField:'=?',
                onSelectFn:'&',
                clearFn: '&',
                selectItemFn:'&',
                loadItemsFn:'&',
                setItemFn:'&',
                size:'=?',
                showId:'=?',
                modelType:'=?',
            },
             
            link: function (scope, elem, attrs,ctrls) {
                
                scope.form = ctrls[0];
                scope.ngModel= ctrls[1];
                scope.selectedItem = null; //{id:null,name:scope.searchText};
                
                
                scope.clearFn({theDirFn: scope.clearOnly});
                scope.selectItemFn({theSelectItemFn: scope.selectItem});
                scope.loadItemsFn({theLoadItemsFn: scope.loadItems});
                scope.setItemFn({theSetItemFn: scope.setItem});
                
                
                 scope.isSelectedMultiple = function(item)
                {
                 
                    //let items = ngModel.$modelValue;
                    if (item!=undefined)
                    {
                        if (item.id!=undefined)
                        {
                            let items = scope.ngModel.$modelValue;
                            if (items!=undefined)
                            {
                                for (let i=0;i<items.length;i++)
                                {
                                    if (items[i]!=undefined)
                                    {
                                        if (items[i].id==item.id) return true;
                                    }
                                }
                            }
                        }
                    }
                    
                    return false;
                }
            }
        }
  
};