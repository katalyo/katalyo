'use strict';

export function ktRichtextbox () {
       
        return {
            restrict: 'A',
                controller: function ($scope,$rootScope,$templateCache,$element,$compile,GridsterService,CodesService,$sce) {
                        $scope.gsrvc = GridsterService;
                        if ($scope.localVars==undefined) $scope.localVars={};
                        
                        $scope.fd.colLength=[];
                        if ($scope.formData==undefined)
                        {
                                $scope.formData = [{}];     
                        }else if ($scope.formData.length==0) $scope.formData = [{}];
                        
                        var lang=$rootScope.globals.currentUser.lang;
                         CodesService.getCodesByName('usertags',lang).then(function (data) {
                                $scope.fd.tagList = data.data;
                                
                                 var pos = $scope.fd.tagList.map(function(e) { return e.id; }).indexOf($scope.fd.UniqueId);

                                   if (pos>=0){
                                       $scope.fd.UniqueIdObj=$scope.fd.tagList[pos];
                                      
                                   }
                             });
                         
                        if ($scope.formData[0]!=undefined &&  $scope.fd.ReadOnly) $scope.fd.readOnlyVal=$sce.trustAsHtml($scope.formData[0][$scope.fd.FieldName]);
                        
                         $scope.$watch("fd.ReadOnly",function(newValue,oldValue){
                                  
                                  if (newValue)
                                  {
                                     //if ($scope.showSearchFilters)
                                       if ($scope.formData[0]!=undefined &&  $scope.fd.ReadOnly) $scope.fd.readOnlyVal=$sce.trustAsHtml($scope.formData[0][$scope.fd.FieldName]);
                                  } 
                         });
                         
                        for (let i=0;i<24;i++) $scope.fd.colLength.push('col-lg-'+(i+1));
                        
                        $scope.isOpen=true;
                        $scope.isOpen=false;
                                                
                                                $scope.search=[
                                                {id:1,filter:'begins',name:"Begins with",icon:"fa fa-chevron-right"},
                                                {id:2,filter:'contains',name:"Contains",icon:"fa fa-expand"},
                                                {id:3,filter:'ends',name:"Ends with",icon:"fa fa-chevron-left"},
                                                {id:4,filter:'eq',name:"Equals",icon:"fa fa-bars"} ];
                                                
                       $scope.fd.SetDefault = function(){
                                $scope.fd.DefaultValue = $scope.formData[0][$scope.fd.FieldName];
                        }
                        
                                          $scope.applyTextFilters = function(index) {
                                                   $scope.fd.Search=$scope.search[index];
                                    if ($scope.fd.Search) $scope.fd.Search.value=$scope.fd.sItemValue; 
                                    $scope.isOpen=false;		
                                                };
                                
                                $scope.refreshSearchValue = function() {
                                 if ($scope.fd.Search) $scope.fd.Search.value=$scope.fd.sItemValue;      
                             };
                         
                         $scope.fd.onSelectUnique = function(item,model)
                        {
                                
                                $scope.fd.UniqueId=item.id;
                        }
                             
                              $scope.showProperties = function(event)
                                {
                                  $scope.gsrvc.setFieldProperties($scope.fd,event);
                       
                                };
                             
                               if ($scope.fd.showSearchForm)  {
                                   if ($scope.fd.Search==undefined) $scope.fd.Search=$scope.search[3];//initialize to equals
                              }
                        
                        let template;
                        if ($scope.resourceDefinition.form_definition) template = $templateCache.get('ktdirectivetemplates/ktRichtextbox.html');
                        else template = $templateCache.get('ktdirectivetemplates/ktRichtextboxExe.html');
          
                        $element.append($compile(template)($scope));  
                        
                        },
             
             link: function (scope, elem, attrs) {
				


              
            }
        };
};