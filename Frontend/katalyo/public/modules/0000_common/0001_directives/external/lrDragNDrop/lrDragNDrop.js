(function (ng) {
    'use strict';

    function isJqueryEventDataTransfer(){
        if (window.jQuery.event.props===undefined) window.jQuery.event.props=[];
        return window.jQuery && (-1 == window.jQuery.event.props.indexOf('dataTransfer'));
    }

    if (isJqueryEventDataTransfer()) {
        window.jQuery.event.props.push('dataTransfer');
    }

    var module = ng.module('lrDragNDrop', []);
    
     module.filter('ngRepeatfilter', function() {
                var fakeItem=[{$fake:true}];
                
                              return function(items, search) {
                                const itemSet = new Set() 
                                
                                for (let i=0; i<items.length; i++) { 
                                    if (itemSet.has(items[i].id) && items[i].id!==undefined)
                                        items.splice(i,1) //remove duplicate
                                    else  
                                        itemSet.add(items[i].id) 
                                }
                                //debugger
                                if (items.length==0) 
                                    return fakeItem
                                else 
                                    return items
                                
                              };
     });
                            
                            

    module.service('lrDragStore', ['$document', function (document) {

        var store = {};

        this.hold = function hold(key, item, collectionFrom, safe) {
            store[key] = {
                item: item,
                collection: collectionFrom,
                safe: safe === true
            }
        };

        this.get = function (namespace) {
            var
                modelItem = store[namespace], itemIndex;

            if (modelItem) {
                itemIndex = modelItem.collection.indexOf(modelItem.item);
                return modelItem.safe === true ? modelItem.item : modelItem.collection.splice(itemIndex, 1)[0];
            } else {
                return null;
            }
        };

        this.clean = function clean() {
            store = {};
        };

        this.isHolding = function (namespace) {
            return store[namespace] !== undefined;
        };

        document.bind('dragend', this.clean);
    }]);

    module.service('lrDragHelper', function () {
        var th = this;

        th.parseRepeater = function(scope, attr) {
            var
                repeatExpression = attr.ngRepeat,
                match;

            if (!repeatExpression) {
                throw Error('this directive must be used with ngRepeat directive');
            }
            match = repeatExpression.match(/^(.*\sin).(\S*)/);
            if (!match) {
                throw Error("Expected ngRepeat in form of '_item_ in _collection_' but got '" +
                    repeatExpression + "'.");
            }

            return scope.$eval(match[2]);
        };

        th.lrDragSrcDirective = function(store, safe) {
            return function compileFunc(el, iattr) {
                iattr.$set('draggable', true);
                return function linkFunc(scope, element, attr) {
                    var
                        collection,
                        key = (safe === true ? attr.lrDragSrcSafe : attr.lrDragSrc ) || 'temp',
                        target;//migor

                    if(attr.lrDragData) {
                        scope.$watch(attr.lrDragData, function (newValue) {
                            collection = newValue;
                        });
                    } else {
                        collection = th.parseRepeater(scope, attr);
                    }

                  var handle = element[0].querySelector('.element-drag-handle')
                   element.bind('dragstart', function (evt) {   
                       if (handle.contains(target)) {
                            store.hold(key, collection[scope.$index], collection, safe);
                            if(angular.isDefined(evt.dataTransfer))   evt.dataTransfer.setData('text/html', null); //FF/jQuery fix  
                       } else evt.preventDefault()                            
                        
                    });
                    
                     element.bind('mousedown', function (evt) {
                         target = evt.target   
                    });
                    
                }
            }
        }
    });

    module.directive('lrDragSrc', ['lrDragStore', 'lrDragHelper', function (store, dragHelper) {
        return{
            compile: dragHelper.lrDragSrcDirective(store)
        };
    }]);

    module.directive('lrDragSrcSafe', ['lrDragStore', 'lrDragHelper', function (store, dragHelper) {
        return{
            compile: dragHelper.lrDragSrcDirective(store, true)
        };
    }]);

    module.directive('lrDropTarget', ['lrDragStore', 'lrDragHelper', '$parse', function (store, dragHelper, $parse) {
        return {
            link: function (scope, element, attr) {

                var
                    collection,
                    key = attr.lrDropTarget || 'temp',
                    classCache = null;

                //debugger
                 if ( attr.id=='0') {
                   var  repeatExpression = attr.ngRepeat,    match;
                   
                   match = repeatExpression.match(/^(.*)\sin.(\S*)/);
                   var isfake = Object.keys(scope[match[1]]).length == 0 //fake object have 0 properties
                   if (scope[match[1]].hasOwnProperty('$fake')) isfake = true
                  // if (scope[match[1]]['$fake'])  isfake = true
                   if (isfake)  {
                       element[0].style.opacity = "0";
                       element[0].style.minHeight = attr.minHeight ||  "100px";
                   }
                }
                
                function isAfter(x, y) {
                    //check if below or over the diagonal of the box element
                    return (element[0].offsetHeight - x * element[0].offsetHeight / element[0].offsetWidth) < y;
                }

                function resetStyle() {
                    if (classCache !== null) {
                        element.removeClass(classCache);
                        classCache = null;
                    }
                }

                if(attr.lrDragData) {
                    scope.$watch(attr.lrDragData, function (newValue) {
                        collection = newValue;
                    });
                } else {
                    collection = dragHelper.parseRepeater(scope, attr);
                }

                element.bind('drop', function (evt) {
                    var
                        collectionCopy = ng.copy(collection),
                        item = store.get(key),
                        dropIndex, i, l;
                    if (item !== null) {
                        dropIndex = scope.$index;
                        dropIndex = isAfter(evt.offsetX, evt.offsetY) ? dropIndex + 1 : dropIndex;
                        //srcCollection=targetCollection => we may need to apply a correction
                        if (collectionCopy.length > collection.length) {
                            for (i = 0, l = Math.min(dropIndex, collection.length - 1); i <= l; i++) {
                                if (!ng.equals(collectionCopy[i], collection[i])) {
                                    dropIndex = dropIndex - 1;
                                    break;
                                }
                            }
                        }
                        scope.$apply(function () {
                            collection.splice(dropIndex, 0, item);
                            //var fn = $parse(attr.lrDropSuccess) || ng.noop;
                            //fn(scope, {e: evt, item: item, collection: collection});
                              var fn = $parse(         attr.lrDropSuccess + '(' +  dropIndex + ')'      ) || ng.noop;
                              fn(scope, {e: evt, item: item, collection: collection});
                            
                        });
                        
                        
                        evt.preventDefault();
                        resetStyle();
                        store.clean();
                    }
                });

                element.bind('dragleave', resetStyle);

                element.bind('dragover', function (evt) {
                    var className;
                    if (store.isHolding(key)) {
                        className = isAfter(evt.offsetX, evt.offsetY) ? 'lr-drop-target-after' : 'lr-drop-target-before';
                        if (classCache !== className && classCache !== null) {
                            element.removeClass(classCache);
                        }
                        if (classCache !== className) {
                            element.addClass(className);
                        }
                        classCache = className;
                    }
                    evt.preventDefault();
                });
            }
        };
    }]);
})(angular);