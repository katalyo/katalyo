/*!
 * angular-ui-uploader
 * https://github.com/angular-ui/ui-uploader
 * Version: 1.1.3 - 2015-12-01T00:54:49.732Z
 * License: MIT
 */


(function () { 
'use strict';
/*
 * Author: Remy Alain Ticona Carbajal http://realtica.org
 * Description: The main objective of ng-uploader is to have a user control,
 * clean, simple, customizable, and above all very easy to implement.
 * Licence: MIT
 */

angular.module('ui.uploader', []).service('uiUploader', uiUploader);

uiUploader.$inject = ['$log'];

function uiUploader($log) {

    /*jshint validthis: true */
    var self = this;
	self.files = [];
    self.options = {};
    self.activeUploads = 0;
    self.uploadedFiles = 0;
    $log.info('uiUploader loaded');
	var uploaders = {};
	
    function addFiles(files,id) {
        for (var i = 0; i < files.length; i++) {
            uploaders[id].files.push(files[i]);  
        }
    }
	
    
	 function createUploader(id) {
        uploaders[id] = angular.copy(self);
    }
	
    function getFiles(id) {
        return uploaders[id].files;
    }

    function startUpload(id,options) {
      
	  if (options!=undefined) {
        if (options.url!=undefined) options.url=window._env.apiUrl + options.url
        uploaders[id].options = options;

        //headers are not shared by requests
        var headers = uploaders[id].options.headers || {};

        for (var i = 0; i < uploaders[id].files.length; i++) {
            if (uploaders[id].activeUploads == uploaders[id].options.concurrency) {
                break;
            }
            if (uploaders[id].files[i].active)
                continue;
            ajaxUpload(uploaders[id].files[i], uploaders[id].options.url, uploaders[id].options.data,id, headers);
        }
	  }
    }

    function removeFile(file,id) {
        uploaders[id].files.splice(uploaders[id].files.indexOf(file), 1);
    }

    function removeAll(id) {
        //migor TODO - vidjeti zasto uopce kod importdata ovo poziva npr http://localhost:8080/index/#/resources/import/55/
        if (uploaders[id]!=undefined && uploaders[id]!=null) uploaders[id].files.splice(0, uploaders[id].files.length);
    }

    return {
        addFiles: addFiles,
        getFiles: getFiles,
        //files: uploaders[id].files,
        startUpload: startUpload,
		createUploader: createUploader,
        removeFile: removeFile,
        removeAll: removeAll
    };

    function getHumanSize(bytes) {
        var sizes = ['n/a', 'bytes', 'KiB', 'MiB', 'GiB', 'TB', 'PB', 'EiB', 'ZiB', 'YiB'];
        var i = (bytes === 0) ? 0 : +Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, i)).toFixed(i ? 1 : 0) + ' ' + sizes[isNaN(bytes) ? 0 : i + 1];
    }

    function ajaxUpload(file, url, data,id, headers) {
        var xhr, formData, prop, key = 'file';
        data = data || {};

        uploaders[id].activeUploads += 1;
        file.active = true;
        xhr = new window.XMLHttpRequest();

        // To account for sites that may require CORS
        if (data.withCredentials === true) {
            xhr.withCredentials = true;
        }

        formData = new window.FormData();
        xhr.open('POST', url);

        if (headers) {
            for (var headerKey in headers) {
                if (headers.hasOwnProperty(headerKey)) {
                    xhr.setRequestHeader(headerKey, headers[headerKey]);
                }
            }
        }

        // Triggered when upload starts:
        xhr.upload.onloadstart = function() {
        };

        // Triggered many times during upload:
        xhr.upload.onprogress = function(event) {
            if (!event.lengthComputable) {
                return;
            }
            // Update file size because it might be bigger than reported by
            // the fileSize:
            //$log.info("progres..");
            //console.info(event.loaded);
            file.loaded = event.loaded;
            file.humanSize = getHumanSize(event.loaded);
            if (angular.isFunction(uploaders[id].options.onProgress)) {
                uploaders[id].options.onProgress(file);
            }
        };

        // Triggered when upload is completed:
        xhr.onload = function() {
            uploaders[id].activeUploads -= 1;
            uploaders[id].uploadedFiles += 1;
            startUpload(id,uploaders[id].options);
            if (angular.isFunction(uploaders[id].options.onCompleted)) {
                uploaders[id].options.onCompleted(file, xhr.responseText, xhr.status);
            }            
            if (uploaders[id].uploadedFiles === uploaders[id].files.length) {
                uploaders[id].uploadedFiles = 0;
                if (angular.isFunction(uploaders[id].options.onCompletedAll)) {
                    uploaders[id].options.onCompletedAll(uploaders[id].files);
                }
            }
        };

        // Triggered when upload fails:
        xhr.onerror = function(e) {
            if (angular.isFunction(uploaders[id].options.onError)) {
                uploaders[id].options.onError(e);
            }
        };

        // Append additional data if provided:
        if (data) {
            for (prop in data) {
                if (data.hasOwnProperty(prop)) {
                    formData.append(prop, data[prop]);
                }
            }
        }
       
        //Append resource_id if field exists
        if (file.hasOwnProperty('fileResourceID')) formData.append('resource_id', file.fileResourceID);
        if (file.hasOwnProperty('fileVerificationId')) formData.append('blockchain_file_id', file.fileVerificationId);
        // Append file data:
        formData.append(key, file, file.name);

        // Initiate upload:
        xhr.send(formData);

        return xhr;
    }

}

}());