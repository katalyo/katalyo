//**************************************************************************************************
//*************************************** SettingsService - ES6 module ****************************
//**************************************************************************************************

'use strict';

class SettingsService{
   
    
   constructor ($q,$rootScope,RestApiService) {
     
     
      this.RestApiService = RestApiService;
      this.$q=$q;
      this.$rootScope =$rootScope; 

    }

   static $inject = ['$q','$rootScope','RestApiService'];

   //**********************************************************
	//           getOrganisationInfo
	//**********************************************************      
   getOrganisationInfo (organisationId,lang){
     
      if (organisationId != undefined) {
         var deferred = this.$q.defer();                   

         var instance=this;
         
         let url = '../api/organisations/'+lang+'/'+organisationId+'/';
         this.RestApiService.get(url).then(response =>{
                        
                       
                           if (response.status===200) deferred.resolve({data: response.data , status:response.status});
                           else deferred.reject({data: data , status:response.status});
                      
                        
                });
          
      }
      else {
             
             deferred.resolve({data: this.listOfCodesByName[code_name+language].codesList,status:200});
      }
      return deferred.promise;
   }
   
     GenerateApiKeys (api_user) {
        
                     var deferred = this.$q.defer();   
                     var instance = this;
                     let url = '../api/generate-api-keys/';
                     let post_data = {'api_id':api_user};
                     this.RestApiService.post(url,post_data).then(response =>{
                               if (response.status===200) deferred.resolve({data:response.data,status:response.status}) 
                                   else deferred.reject({data: response.data,status:response.status});
              
                     });
         
               
                return deferred.promise;
 
        };
        
        GetKtyloBalance (address) {
        
                     var deferred = this.$q.defer();   
                        
                     let url = '../api/get-ktlyo-balance/';
                     let post_data = {address};
                     this.RestApiService.post(url,post_data).then(response =>{
                           
                         if (response.status===200) deferred.resolve({data:response.data,status:response.status})
                                else deferred.reject({data:response.data,status:response.status});
                              
                     });
                     
                       
                return deferred.promise;
 
        };
        
        SaveBlockchainAccount (address) {
        
                     var deferred = this.$q.defer();   
                     let url = '../api/save-eth-account/';
                     let post_data = {address};
                     this.RestApiService.post(url,post_data).then(response =>{
                         
                               if (response.status===200) deferred.resolve({data:response.data,status:response.status})  
                                    else deferred.reject({data:response.data,status:response.status});
                             
                     });
                     
                      
                return deferred.promise;
 
        };
}

export default SettingsService;