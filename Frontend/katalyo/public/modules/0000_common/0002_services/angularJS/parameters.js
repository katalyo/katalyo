'use strict';


var app = angular.module('Katalyo');

//konstante za processing
app.constant('PROCESSING_CONST', {'_START_REQUEST_':'_START_REQUEST_','_END_REQUEST_': '_END_REQUEST_'});

//ovdje su konstante za dohvat sifranata za dodavanje šifranata i resursa  
app.constant('CODES_CONST', {'_POPULATE_TYPE_':{11:'Current task initiate form',12:'Current task execute form',21:'Previous task initiate form',22:'Previous task execute form',3:'Fixed resource'},
              '_POPULATE_TYPE_NEW_':{1:'Fixed dataset',2:'Dataset record from this task',3:'Dataset record from previous task'}});

//ovdje su konstante za dohvat sifranata za dodavanje šifranata
/*app.constant('CODES_LIST', {'_LANG_LIST_': [{id:'en-GB',name:'English'},{id:'hr-HR',name:'Hrvatski'}],
                              '_CODE_TYPE_LIST_':[{id:1,name:'System'},{id:2,name:'User'}],'_CODE_STATUS_LIST_':[{id:1,name:'Active'},{id:2,name:'Not active'}]});
                              */