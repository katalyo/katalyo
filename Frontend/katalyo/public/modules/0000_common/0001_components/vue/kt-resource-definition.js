
export default Vue.component('kt-resource-definition', {
  props: ['resourceObj'],
  data: function () {
    return {
      resource: this.resourceObj,
      editResource: false,
    }
  },
  methods:{
    saveResourceDef()
    {
      
    },
     EditResourceFn(val)
    {
      this.editResource=val;
    }
    
    
  },
  computed: {
    // a computed getter
    somethingComputed: function () {
      // `this` points to the vm instance
      return !this.editResource;
    }
  }
});
