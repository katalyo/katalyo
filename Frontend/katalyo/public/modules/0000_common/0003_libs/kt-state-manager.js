/**
 * Katalyo state manager (kt-state-manager.js) implements observer pattern in immutable manner to manage state in Katalyo platform
 * @license MIT 
 * @build: 2019-12-14
 * @version 0.1
 * https://gitlab.com/katalyo/kt-state-manager
 * Copyright (c) 2019 Katalyo
**/

 'use strict';
 
        
        /**
         * Class SubjectCollection is a collection class for Subjects and contains all subjects in the application
         * @since 0.1
         * @method addSubject is used to add new subject to collection
         * @method createSubject is used to create new instance of subject class  
         */
        
        class SubjectCollection {
            
            constructor() {
                this.subjects = {};
                this.pendingSubscriptions = {}; //due to asynchronous nature of javascript observers may send subscription requests before subject is created
            };
            
            addSubject(subject)
            {
              let msg ='',status='ok';
              if (subject.name in this.subjects)
              {
                if (this.subjects[subject.name]!==undefined)
                {
                    msg = 'Subject '+subject.name+' allready exists!';
                    status = 'nok';
                    return {'msg':msg,'status':status,'subject':this.subjects [subject.name]};
                }
              }
              this.subjects [subject.name] = subject;
              if (this.pendingSubscriptions[subject.name]!=undefined) this.processPendingSubscriptions(subject.name);
              return {'msg':msg,'status':status,'subject':this.subjects [subject.name]};
            }
            
            processPendingSubscriptions(subjectName)
            {
                let sub;
                for (let i=0;i<this.pendingSubscriptions[subjectName].length;i++)
                {
                    sub = this.pendingSubscriptions[subjectName][i];
                    //subscribe(name,action,notifyFn,override)
                    this.subjects[subjectName].subscribe(sub.observer,sub.action,sub.data,sub.override);  
                }
             
            }
            addPendingObserverSubscription(subscription)
            {
              let msg ='',status='ok';
              if (subscription.subject in this.pendingSubscriptions)
              {
                if (this.pendingSubscriptions[subscription.subject]==undefined)
                {
                    /*
                    if (this.pendingSubscriptions[subscription.subject][subscription.observer]!= undefined)
                    {
                        msg = 'Pending subscription for '+subscription.subject+' and observer '+subscription.observer+' allready exists!';
                        status = 'nok';
                        return {'msg':msg,'status':status,'subject':this.pendingSubscriptions[subscription.subject]};
                
                    }
                    */
                    this.pendingSubscriptions[subscription.subject] = [];
                }
              }else
              {
                this.pendingSubscriptions[subscription.subject] = [];
                
              }
              this.pendingSubscriptions[subscription.subject].push(subscription);
              
              return {'msg':msg,'status':status,'subject':this.pendingSubscriptions [subscription.subject]};
            }
             /**
            * fn for creating Subjects and adding them to collection
            *  @since 0.1
            * @param name is the name of the subject 
            */
            createSubject (name,state_object)
            {
                let newSubject = new Subject(name,state_object);
                let asRet = this.addSubject(newSubject);
                return asRet;
            };
             /**
            * fn for getting Subjects and adding them to collection
            *  @since 0.1
            * @param name is the name of the subject 
            */
            getSubject (name)
            {
                let subject = this.subjects[name];
                return subject;
            };
             /**
            * fn for adding actions to Subjects and adding them to collection
            *  @since 0.1
            * @param subjectName is the name of the subject 
            */
            addAction (subjectName,actionName,actionFn)
            {
                if (this.subjects[subjectName]===undefined) return null;
                let newAction = new Action(actionName,actionFn);
                this.subjects[subjectName].actions[actionName]=newAction.getAction;
                return newAction;
            };
            
             /**
            * fn for getting current state of Subjects
            *  @since 0.1
            * @param name is the name of the subject 
            */
            getState (name)
            {
                let msg="",status="";
                let subject = this.subjects[name];
                if (subject===undefined)
                
                {
                    msg="Subject '"+name+"' is undefined!";
                    status="nok";
                    return {'msg':msg,'status':status};
                }else
                {
                    let state = subject.getState;
                    msg="";
                    status="ok";
                    return {'msg':msg,'status':status,'state':state};
                }
    
            };
                
        
        };
        
        /**
         * Class Subject is used to store different subjects ie stores
         * @since 0.1
         * @param name is unique name od the subject
         * @var observers is a collections of observers subscribed to this subject
         * @var state is used to store the states of the subject
         * @var actions is a collection of actions that cn be executed on this subject
         * @var curentState is index of the current state
         * @method subscribe is used by observers to subscribe to this subject
         * @method getState return the current state of the subject
         * @method executeAction executes action on the subject
         * @method undo reverts to previous state
         * @method redo
         * @method notifyObservers is used to notify all observers on state changes
         * */
        
        class Subject {
            
            constructor(name,state,actions) {
                this.name = name;
                this.observers = {};
                this.stateChanges = [];
                this.currentStateIndex = -1;
                this.currentFullState = state;
                if (actions===undefined) this.actions={};
                else this.actions = actions;
                
            };
            subscribe(name,action,notifyFn,override)
            {
              let msg ='Subscription successful',status='ok';
              let observer = {};
              if (override == undefined) override=false;
              if (name in this.observers)
              {
                if (this.observers[name]!==undefined)
                {
                        if ( this.observers[name].actions[action] !== undefined && !override)
                        {
                           msg = 'Observer with name '+name+' is already subscribed for action '+action+'!';
                           status = 'nok';
                           return {'msg':msg,'status':status};
                        }else if ( this.observers[name].actions[action] !== undefined && override)
                        {
                           msg = 'Observer with name '+name+' is already subscribed for action '+action+'! Override executed!';
                           status = 'ok';
                           this.observers[name].actions[action]= {'notifyFn':notifyFn};
                           return {'msg':msg,'status':status};
                            
                            
                        }
                        else
                        {
                            this.observers[name].actions[action]= {'notifyFn':notifyFn};
                        }
                }
              }else
              {
                observer.name = name;
                observer.actions = {};
                observer.actions[action] = {'notifyFn':notifyFn};
                this.observers[name] = observer;
              }
              return {'msg':msg,'status':status};
            }
            
            set setStateChanges(newState)
            {
                this.currentStateIndex++;
                if (this.currentStateIndex<0) this.currentStateIndex=0;
                this.stateChanges.splice(this.currentStateIndex,0,newState);
                this.currentFullState=newState;
            }
            
            get getStateChanges()
            {
                return this.stateChanges[this.currentStateIndex];
            }
            set setState(newState)
            {
               this.currentFullState=newState;
            }
            get getState()
            {
                return this.currentFullState;
            }
            get getCurrentStateIndex()
            {
                return this.currentStateIndex;
            }
            set setCurrentStateIndex(index)
            {
                this.currentStateIndex=index;
            }
            addAction(action)
            {
                this.actions[action.name] = action;
                
            }
            executeAction(actionName,data)
            {
                if (actionName in this.actions) this.actions[actionName].executeAction(data);
                
            }
            undo()
            {
                if (this.currentStateIndex>0)
                {
                    this.currentStateIndex--;
                    this.notifyObservers();
                    return this.currentStateIndex;
                } else return -1;
                
            }
            redo()
            {
                if (this.currentStateIndex < this.stateChanges.length-1)
                {
                    this.currentStateIndex++;
                    this.notifyObservers();
                    return this.currentStateIndex;
                } else return -1;
                
            }
            notifyObservers()
            {
                
                for(let observer in this.observers)
                {
                    if (this.observers[observer]!==undefined)
                    {
                        for(let action in this.observers[observer].actions)
                        {
                            if (action.notifyFn!==undefined)
                            {
                                action.notifyFn({sender:this.name,state:this.stateChanges[this.currentStateIndex]});
                            }
                        }
                    }
                }
            }
            
            
            notifyObservers(action)
            {
                
                for(let observer in this.observers) {
                    if (this.observers[observer]!==undefined)
                    {
                        if (this.stateChanges[this.currentStateIndex] == undefined)   
                                throw new Error("this.stateChanges[this.currentStateIndex] is undefined");
                        if (action in this.observers[observer].actions)  {
                        if (this.observers[observer].actions[action].notifyFn!==undefined) 
                                this.observers[observer].actions[action].notifyFn( {sender:this.name,state:this.stateChanges[this.currentStateIndex], action:action} );
                        }
                        
                    }
                }
            }
        };
        
        /**
         * Class Action is used to define actions to be executed on Subjects
         * @since 0.1 
         */
        
        class Action {
            constructor(name,fnAction)
            {
                this.name = name;
                this.executeAction = fnAction;
            }
            get getAction()
            {
                return {'action':this.name,'executeAction':this.executeAction};
                
            }
        };
         /**
         * fn for initializing SubjectCollection return SubjectCollection object
         *  @since 0.1
         */
        var KatalyoStateManager = (function()
        {
          var subjectCollection = new SubjectCollection();
          
          return subjectCollection;
        });
        export default KatalyoStateManager;