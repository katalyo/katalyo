	
const template = document.createElement('template');

template.innerHTML = `
<style>

 {
  box-sizing: border-box;
}

body {
  margin: 0;
  font-family: Arial;
}


.row {
display:flex;
overflow-x:hidden;
scrollbar-width: none;
}


.column {
  float: left;
  width: 25%;
  margin: 5px;
}


.column img {
  opacity: 0.8; 
  cursor: pointer; 
}

.column img:hover {
  opacity: 1;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.container {
  position: relative;
  display: none;
  height:300px
}


#imgtext {
  position: absolute;
  bottom: 15px;
  left: 15px;
  color: white;
  font-size: 20px;
}

.closebtn {
  position: absolute;
  top: 10px;
  right: 15px;
  color: white;
  font-size: 35px;
  cursor: pointer;
}

.prev {
    left: 10px;
}

.next {
    right: 0px;
}
  
.ibutton {
    position: absolute;
    top: 50%;
    width: 44px;
    height: 44px;
    border: none;
    border-radius: 50%;
    background: hsla(0, 0%, 100%, 0.75);
    cursor: pointer;
    transform: translateY(-50%);
 } 
 
 .ibutton:focus {
     outline: none;  
 } 
 
.expanded {
    height: 300px;
    max-height: 300px;
   /* max-width: 300px;*/
    width:100%;
}

</style>


<div style="width:400px;height:300px">
<div id="images" class="row">
    <!-- Fake slot for image elements-->
</div>

<div class="container">
  <span onclick="this.parentElement.style.display='none'" class="closebtn">&times;</span>
  <img id="expandedImg" class="expanded">
   <div id="prevImage"  >
   <button class="ibutton prev" type="button">
      <svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(15,0)"></path></svg>
     </button>
   </div>
   
      
 <div id="nextImage" >
      <button class="ibutton next" type="button">
      <svg viewBox="0 0 100 100"><path d="M 50,0 L 60,10 L 20,50 L 60,90 L 50,100 L 0,50 Z" class="arrow" transform="translate(85,100) rotate(180)"></path></svg>
      </button>
  </div>
  <div id="imgtext"></div>
</div>


</div> `

class imageGallery extends HTMLElement {
   
    constructor() {
        super();
        this._shadow = this.attachShadow({ 'mode': 'open' });
        this._shadow.appendChild(template.content.cloneNode(true));
        this._shadow.getElementById("nextImage").addEventListener('click', this._nextImageClick.bind(this)); 
        this._shadow.getElementById("prevImage").addEventListener('click', this._prevImageClick.bind(this)); 
       // this.inputs = ["/static/img/resource_dude.png", "/static/img/signup.png", "/static/img/signup-dude.png"]   
     }
     

    _nextImageClick(e) {

        var current=this.images.indexOf(this.$currentImg)        
        if ( (current >= 0) && (current < this.images.length-1)) {
           this._expandImage (current+1) 
        } 
      }
      
     _prevImageClick(e) {

        var current=this.images.indexOf(this.$currentImg)
        if ( current > 0) {
           this._expandImage (current-1) 
        } 
     }
   
    _render() {
        var ic = this._shadow.getElementById("images");
        ic.innerHTML = '';
        this.images=[]
        this._inputs.forEach( el=> {   
            var inner=document.createElement("div")
            inner.className="column"
            var im=document.createElement("img") 
            im.src=el
            im.style.width="100%"
            this.images.push(im)
            inner.appendChild(im)
            ic.appendChild(inner)
            im.addEventListener("click", this.clickHandler.bind(this));
 
            } 
        )
       // if (this.images.length === 1) {
            this.$currentImg =  this.images[0]
            this._expandImage (0)
       // }
        this.$currentImg.style.border='yellow solid 2px'
            
    }

    
    set inputs(value) {
        this._inputs = value
        this._render()
    }

    get inputs() {
        return this._inputs
    }
    
    
    clickHandler(evt) {
          var current=this.images.indexOf(evt.target)
          if (current != undefined) this._expandImage(current)
    }
     
    _expandImage (num) {
          this._shadow.getElementById("nextImage").hidden = num === this.images.length-1 || this.images.length===0
          this._shadow.getElementById("prevImage").hidden = num === 0
          //if (this.images.length===1 || num === this.images.length -1 ) this._shadow.getElementById("nextImage").hidden=true
          //if (num === 0 )  this._shadow.getElementById("prevImage").hidden=true
          
          this.$currentImg.style.border=''
          this.images[num].style.border='yellow solid 2px'
          this.$currentImg = this.images[num]
          var expandImg = this._shadow.getElementById("expandedImg");
          var imgText = this._shadow.getElementById("imgtext");
          expandImg.src = this.images[num].src;
          imgText.innerHTML = this.images[num].alt;
          expandImg.parentElement.style.display = "block";
    }

}


window.customElements.define('image-gallery', imageGallery);


