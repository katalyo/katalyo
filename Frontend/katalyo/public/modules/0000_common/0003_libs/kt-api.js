/**
 * Katalyo api class (kt-api.js) implements REST functionality in Katalyo platform using fetch
 * @license MIT 
 * @build: 2021-12-14
 * @version 0.1
 * https://gitlab.com/katalyo/kt-api
 * Copyright (c) 2021 Katalyo
**/

 'use strict';
 
        
          /**
         * Class KatalyoApi is a class where all parameters for fetch calls are defined
         * @since 0.1
         */
        
        class KatalyoApiMain {
            
            constructor() {
              this.KatalyoApiClass = new KatalyoApiTokenAuth();
              //this.KatalyoApiSso = null;
              
            };
            
            
            add_auth_token(csrf_token,auth_data)
            {
             this.KatalyoApiClass = new KatalyoApiTokenAuth(csrf_token,auth_data); 
            }
            add_auth_sso(csrf_token)
            {
             this.KatalyoApiClass = new KatalyoApiSso(csrf_token); 
            }
        
        };
        
        /**
         * Class KatalyoApiSso is a class for sending fetch using sso
         * @since 0.1
         * @method add_header is used to add new headers
         * @method send is used to send the call to the server 
         */
        
        class KatalyoApiSso {
            
            constructor(csrf_token) {
                this.url = '';
                this.method = 'GET';
                this.headers={};
                this.headers['X-CSRFToken'] = csrf_token;
                this.headers['Content-Type']= 'application/json';
            };
            
            
            add_header(header)
            {
             this.headers.push(header); 
            }
            set_header(header,header_value)
            {
                this.headers[header]= header_value;
            }
            send()
            {
              
            }
            async get(url)
            {
              url=window._env.apiUrl + url    
              return fetch(url,{
                                headers: this.headers,
                                method: 'GET'
                                })
                                .then(response => {
                                    // handle the response
                                     return response;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            async post(url,post_data) 
            {
                url=window._env.apiUrl + url
                //debugger
                return fetch(url,{
                                headers: this.headers,
                                method: 'POST',
                                body: JSON.stringify(post_data)
                                })
                    .then(response => {
                                    // handle the response
                                    return response;
                                })
                    .catch(error => {
                                   
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                            
                                })
            
            }
            
            
            
          async fake_post(err_code) //SIMULATE
            {
                url = "https://httpstat.us/"+ err_code   
                return fetch(url,{  headers: this.headers,    method: 'GET',   })
                .then(response => {        return response;         })
                    .catch(error => {
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }                         
                                })
            }
            
            async put(url,put_data) 
            {
                url=window._env.apiUrl + url   
                return fetch(url,{
                                headers: this.headers,
                                method: 'PUT',
                                body: JSON.stringify(put_data)
                                })
                    .then(response => {
                                    // handle the response
                                    return response;
                                })
                    .catch(error => {
                                   
                                       return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                            
                                })
            
            }
            async send(url,method,post_data)
            {
              url=window._env.apiUrl + url
              return fetch(url,{
                            headers: this.headers,
                            method: method,
                            body: JSON.stringify(post_data)
                                })
                                .then(response => {
                                    // handle the response
                                   
                                        return response;
                                     
                                })
                                .catch(error => {
                                    
                                   return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                  
                                })
            }
        
        };
        
        /**
         * Class KatalyoApiTokenAuth is a class for sending fetch using Token auth
         * @since 0.1
         * @method add_header is used to add new headers
         * @method send is used to send the call to the server 
         */
        
        class KatalyoApiTokenAuth {
            
            constructor(csrf_token,auth_data) {
                this.url = '';
                this.method = 'GET';
                this.headers={};
                this.headers['X-CSRFToken'] = csrf_token;
                this.headers['Authorization'] = auth_data;
                this.headers['Content-Type']= 'application/json';
            };
            
             static recursiveDeleteGarbage (obj,  patchData)      { //  recursively patching dashboard object
                var o = {};
                if (obj)  Object.keys(obj).forEach(
                    function(key) {   
                        if ( (typeof(obj[key]) === 'object') && obj[key]   ) { 
                            //container level
                            if (  (key === 'codesDefinitionList') || (key ==='publishedResourceList') || (key==='resourceList') || (key==='datasetListParent') || (key=='localVars') || (key=='tagList')  ) {
                                delete obj[key]
                            }
                            else    KatalyoApiTokenAuth.recursiveDeleteGarbage(obj[key],  patchData)
                        } 
                        else {  // leaf level
                                if (key == 'nesto' ) {  
                                   // o['id']= obj[key];
                                }       
                            }
                        }) ;  
                //if (Object.keys(o).length > 0) ..do somoething with o   
            };
            
            
          static cloneWithDelete(obj) {
                if (obj === null || typeof (obj) !== 'object' || 'isActiveClone' in obj ||  obj instanceof File)  return obj;
                if (obj instanceof Date )
                    var temp = new obj.constructor(); //or new Date(obj);
                else
                    var temp = obj.constructor();

                for (var key in obj) {
                    if (Object.prototype.hasOwnProperty.call(obj, key)) {
                        if ( (obj[key] instanceof Function) || (key === 'codesDefinitionList') || (key ==='publishedResourceList') || (key==='resourceList') || (key==='datasetListParent') || (key=='localVars') || (key=='tagList')  )  { 
                            //skip functions , lists, LOCALVARS
                        }
                        else {
                            obj['isActiveClone'] = null;
                            temp[key] = KatalyoApiTokenAuth.cloneWithDelete(obj[key]);
                            delete obj['isActiveClone'];
                        }
                    }
                }
                return temp;
            }
   
            
    
            add_header(header)
            {
                Object.extend(this.headers,header); 
            }
            
            set_header(header,header_value)
            {
                this.headers[header]= header_value;
            }
            
            async get(url)
            {
                url=window._env.apiUrl +url ;
               //debugger
                
                
              return fetch(url,{
                                headers: this.headers,
                                method: 'GET'
                                })
                                .then(response => {
                                    // handle the response
                                     return response;
                                })
                                .catch(error => {
                                    return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            
           async post(url,post_data) 
            {
                url=window._env.apiUrl + url   
                //var new_post_data=KatalyoApiTokenAuth.cloneWithDelete(post_data)
                var new_post_data = angular.copy (post_data)
                KatalyoApiTokenAuth.recursiveDeleteGarbage(new_post_data, null)
                return fetch(url,{
                                headers: this.headers,
                                method: 'POST',
                                body: JSON.stringify(new_post_data )
                                })
                    .then(response => {
                                    // handle the response
                                    return response;
                                })
                    .catch(error => {
                                   
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                })
            }
            
            
             async fake_post(err_code) //SIMULATE
            {
                let url = "https://httpstat.us/"+ err_code   
                return fetch(url,{  headers: this.headers,    method: 'GET',   })
                .then(response => {        return response;         })
                    .catch(error => {
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }                         
                                })
            }
            
            async put(url,put_data) 
            {
                url=window._env.apiUrl + url     
                return fetch(url,{
                                headers: this.headers,
                                method: 'PUT',
                                body: JSON.stringify(put_data)
                                })
                    .then(response => {
                                    // handle the response
                                    return response;
                                })
                    .catch(error => {
                                   
                                        return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                            
                                })
            
            }
            
            async send(url,method,post_data)
            {
              url=window._env.apiUrl + url 
              return fetch(url,{
                            headers: this.headers,
                            method: method,
                            body: JSON.stringify(post_data)
                                })
                                .then(response => {
                                    // handle the response
                                   
                                        return response;
                                     
                                })
                                .catch(error => {
                                   return {
                                            status:499,
                                            msg:error.message,
                                            data: error.stack
                                        }
                                  
                                })
            }
        
        

            
        };
     
        /**
         * fn for initializing KatalyoApi Class returns KatalyoApi object based on auth_type
         *  @since 0.1
         */
        var KatalyoApi = (function()
        {
          let api = new KatalyoApiMain();
          
          return api;
        });
        export default KatalyoApi;