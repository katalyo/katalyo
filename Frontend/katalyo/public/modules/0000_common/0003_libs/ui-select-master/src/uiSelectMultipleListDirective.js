
uis.directive('uiSelectMultipleList', ['uiSelectMinErr','uiSelectConfig','$timeout','$parse', function(uiSelectMinErr,uiSelectConfig, $timeout,$parse) {
  return {
    restrict: 'EA',
    require: ['^uiSelect', '^ngModel'],
    controller: ['$scope','$timeout', function($scope, $timeout){

      var ctrl = this,
          $select = $scope.$select,
          ngModel;
      
      
      var $selectMultiple = $scope.$selectMultiple;
                
      $select.multipleList=true; 
      if (angular.isUndefined($select.selectedItemsMultipleList))  
        $select.selectedItemsMultipleList = [];
        
      //Wait for link fn to inject it
      $scope.$evalAsync(function(){ 
        ngModel = $scope.ngModel;
        //ngModel = $select.ngModel;
        });

      //ctrl.activeMatchIndex = -1;

      ctrl.updateModel = function(){
        //ngModel.$setViewValue($select.selectedItemsMultipleList); //Set timestamp as a unique string to force changes
        ngModel.$setViewValue(Date.now()); //Set timestamp as a unique string to force changes
        
        ctrl.refreshComponentMultiple();
      };
      
      
      ctrl.refreshComponent = function(){
        //Remove already selected items
        //e.g. When user clicks on a selection, the selected array changes and
        //the dropdown should remove that item
        if($select.refreshItems){
          $select.refreshItems();
        }
        if($select.sizeSearchInput){
          $select.sizeSearchInput();
        }
      };
      
       ctrl.refreshComponentMultiple = function(){
        //Remove already selected items
        //e.g. When user clicks on a selection, the selected array changes and
        //the dropdown should remove that item
      //  if($select.refreshItems){
       //   $select.refreshItems();
        //}
       
      };
    
    ctrl.refreshItemsMultiple = function (data){
      data = data || ctrl.parserResult.source($scope);
      var selectedItems = $select.selectedItemsMultipleList;
      if ($select.isEmpty() || (angular.isArray(selectedItems) && !selectedItems.length) || !$select.multiple || !$select.removeSelected) {
        $select.setItemsFn(data);
      }else{
        if ( data !== undefined && data !== null ) {
          var filteredItems = data.filter(function(i) {
            return angular.isArray(selectedItems) ? selectedItems.every(function(selectedItem) {
              return !angular.equals(i, selectedItem);
            }) : !angular.equals(i, selectedItems);
          });
          $select.setItemsFn(filteredItems);
        }
      }
      
      $scope.$broadcast('uis:refresh');
    };
      
        
              /*function for multiple list START ILJ*/
      ctrl.removeItemFromList =  function(index){
              $select.selectedItemsMultipleList.splice(index,1);
              //$select.selected=null;
              //ngModel.$setViewValue(Date());
              ctrl.updateModel();
              //$select.checkValidity(); 
              $scope.$evalAsync(); //To force $digest

        };
        
        ctrl.isSelectedMultipleList =  function(item) {
              var pos;
              if ($select.selectedItemsMultipleList==undefined) $select.selectedItemsMultipleList = [];
              pos = $select.selectedItemsMultipleList.map(function(e) { return e.id; }).indexOf(item.id);
              
              if (pos<0) return false;
              else return true;
              
    };    
       ctrl.addItemToList =  function(item) {
        
        if (item!=null)
        {
          var pos;
              if ($select.selectedItemsMultipleList==undefined) $select.selectedItemsMultipleList = [];
              pos = $select.selectedItemsMultipleList.map(function(e) { return e.id; }).indexOf(item.id);
              
              if (pos<0)
              {
                   $select.selectedItemsMultipleList.push(angular.copy(item));
                   $select.selected=null;
                   ctrl.updateModel();
                   $scope.$evalAsync(); //To force $digest
          
              }
        }
              if ($select.doneBlur)
              {
               $select.checkValidity(); 
              }
              
   

      };     
      
      // Remove item from multiple select
      ctrl.removeChoice = function(index){

        // if the choice is locked, don't remove it
        if($select.isLocked(null, index)) return false;

        var removedChoice = $select.selected[index];

        var locals = {};
        locals[$select.parserResult.itemName] = removedChoice;

        $select.selectedItemsMultipleList.splice(index, 1);
        ctrl.activeMatchIndex = -1;
        $select.sizeSearchInput();

        // Give some time for scope propagation.
        $timeout(function(){
          $select.onRemoveCallback($scope, {
            $item: removedChoice,
            $model: $select.parserResult.modelMapper($scope, locals)
          });
        });

        ctrl.updateModel();

        return true;
      };

      ctrl.getPlaceholder = function(){
        //Refactor single?
        if($select.selected && $select.selected.length) return;
        return $select.placeholder;
      };


    }],
    controllerAs: '$selectMultiple',

    link: function(scope, element, attrs, ctrls) {

      var $select = ctrls[0];
      //var ngModel = scope.ngModel = ctrls[1];
      var ngModel = scope.ngModel = ctrls[1];
      var $selectMultiple = scope.$selectMultiple;

      //$select.selected = raw selected objects (ignoring any property binding)

      $select.multiple = true;
      
      if (angular.isUndefined($select.selectedItemsMultipleList))  
        $select.selectedItemsMultipleList = [];
      
      //Input that will handle focus
      $select.focusInput = $select.searchInput;

      //Properly check for empty if set to multiple
      ngModel.$isEmpty = function(value) {
        return !value || value.length === 0;
      };

     
      
      //Watch for external model changes
     
      scope.$watchCollection(function(){ return ngModel.$modelValue; }, function(newValue, oldValue) {
        if (oldValue != newValue){
          //update the view value with fresh data from items, if there is a valid model value
          if(angular.isDefined(ngModel.$modelValue)) {
            ngModel.$modelValue = null; //Force scope model value and ngModel value to be out of sync to re-run formatters
          }
            $selectMultiple.refreshComponentMultiple();  
        }
        
      });
    
      /*********************************/
      //From view --> model
      ngModel.$parsers.unshift(function () {
        var locals = {},
            result,
            resultMultiple = [];
        for (var j = $select.selectedItemsMultipleList.length - 1; j >= 0; j--) {
          locals = {};
          locals[$select.parserResult.itemName] = $select.selectedItemsMultipleList[j];
          result = $select.parserResult.modelMapper(scope, locals);
          resultMultiple.unshift(result);
        }
        return resultMultiple;
      });
   
      // From model --> view
      ngModel.$formatters.unshift(function (inputValue) {
        var data = $select.parserResult && $select.parserResult.source (scope, { $select : {search:''}}), //Overwrite $search
            locals = {},
            result;
        if (!data) return inputValue;
        var resultMultiple = [];
        var checkFnMultiple = function(list, value){
          if (!list || !list.length) return;
          for (var p = list.length - 1; p >= 0; p--) {
            locals[$select.parserResult.itemName] = list[p];
            result = $select.parserResult.modelMapper(scope, locals);
            if($select.parserResult.trackByExp){
                var propsItemNameMatches = /(\w*)\./.exec($select.parserResult.trackByExp);
                var matches = /\.([^\s]+)/.exec($select.parserResult.trackByExp);
                if(propsItemNameMatches && propsItemNameMatches.length > 0 && propsItemNameMatches[1] == $select.parserResult.itemName){
                  if(matches && matches.length>0 && result[matches[1]] == value[matches[1]]){
                      resultMultiple.unshift(list[p]);
                      return true;
                  }
                }
            }
            if (angular.equals(result,value)){
              resultMultiple.unshift(list[p]);
              return true;
            }
          }
          return false;
        };
        if (!inputValue) return resultMultiple; //If ngModel was undefined
        for (var k = inputValue.length - 1; k >= 0; k--) {
          //Check model array of currently selected items
          if (!checkFnMultiple($select.selectedItemsMultipleList, inputValue[k])){
            //Check model array of all items available
            if (!checkFnMultiple(data, inputValue[k])){
              //If not found on previous lists, just add it directly to resultMultiple
              resultMultiple.unshift(inputValue[k]);
            }
          }
        }
        return resultMultiple;
      });
    
    
      
    /*****************************************/
      
      ngModel.$render = function() {
        // Make sure that model value is array
        if(!angular.isArray(ngModel.$viewValue)){
          // Have tolerance for null or undefined values
          if(angular.isUndefined(ngModel.$viewValue) || ngModel.$viewValue === null){
            ngModel.$viewValue = [];
          } else {
            throw uiSelectMinErr('multiarr', "Expected model value to be array but got '{0}'", ngModel.$viewValue);
          }
        }
        $select.selectedItemsMultipleList = ngModel.$viewValue;
        $selectMultiple.refreshComponentMultiple();
        scope.$evalAsync(); //To force $digest
      };

      scope.$on('uis:select', function (event, item) {
        
        //if($select.selectedItemsMultipleList.length >= $select.limit) {
         // return;
        //}
       
        //$select.selectedItemsMultipleList.push(item);
        
       // $selectMultiple.updateModel();
      });

      scope.$on('uis:activate', function () {
        //$selectMultiple.activeMatchIndex = -1;
      });
      
       $select.closeOnSelect = function() {
          if (angular.isDefined(attrs.closeOnSelect)) {
            return $parse(attrs.closeOnSelect)();
          } else {
            return uiSelectConfig.closeOnSelect;
          }
        }();
        
      scope.$watch('$select.disabled', function(newValue, oldValue) {
        // As the search input field may now become visible, it may be necessary to recompute its size
       // if (oldValue && !newValue) $select.sizeSearchInput();
      });


      $select.searchInput.on('blur', function() {
       // $timeout(function() {
         // $selectMultiple.activeMatchIndex = -1;
        //});
      });
      
      // force digest to setup the $modelValue
      //if ($select.multiple)
        //ngModel.$setViewValue(Date.now());
    }
  };
}]);
