const path = require('path');
const { VueLoaderPlugin } = require('vue-loader')



module.exports = {
output: {
    filename: 'katalyo_bundle.js',
  },
      devtool: 'source-map',
entry:  './0001_modules/0001_index/angularJS/bootstrap.js',
module: {
  rules: [
    {

      test: /\.m?js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env']
        }
      }, 
    },
     {
      test: /\.vue$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'vue-loader'
      }
     },
     // this will apply to both plain `.css` files AND `<style>` blocks in `.vue` files
       {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader'
                ]
            }
    
    
  ]
},
optimization: {
   minimize: false
},
 plugins: [
    // make sure to include the plugin!
    new VueLoaderPlugin()
  ]
}