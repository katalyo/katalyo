'use strict';
import authJson from './auth.json' //assert {type: 'json'};

export function changePasswordCtrl ($scope, $rootScope, $state,$cookies,$timeout,AuthenticationService,MenuService,$stateParams,$location) {
       
    $scope.CTRLNAME='changePasswordCtrl';
    let touched=[false,false]
    AuthenticationService.ClearCredentials();
    $scope.signupkey = $stateParams.digest

    $scope.cp={username : "", password1 : "",password2 : ""}
    
    let lang = $location.search().lang
    if (lang===undefined) lang ='hr-HR'
    
    $scope.pageLabels = authJson[lang]

    $scope.CheckChangeRequest = function(){
                
                
                AuthenticationService.CheckPassChangeRequest($scope.signupkey,function(response) {
                      $timeout(function(){
                       if (response.status === 200) {
                                        
                          $scope.checkDigestOk = true
                          $scope.userId = response.data.user_id
                        }else
                        {
                          $scope.checkDigestOk = false
                          
                        }
                        $scope.checkDone = true
                      })
                });
                 
      
        };

    $scope.CheckChangeRequest()

     $scope.checkUsernameValidity=function() { 
           
            if ($scope.cp.username==='' || $scope.cp.username===undefined) $scope.usernameInvalid=true
            else $scope.usernameInvalid=false
        }

     $scope.hideError=function() { 
           
            $scope.errorMsg=""
        }

    $scope.checkPasswordValidity1=function() { 
             
            if ($scope.cp.password1==='' || $scope.cp.password1===undefined) $scope.passwordInvalid1=true
            else $scope.passwordInvalid1=false
        }
      $scope.checkPasswordValidity2=function() { 
             //touched[i]=true       
             //if (this.password[i]=='' || this.password[i]==undefined || this.password[i]==null) this.passwordInvalid[i]= true;
             //else this.passwordInvalid[i]=false;
             //if (!this.password[i] && touched[i]==true
             //passwords must match
             if ($scope.cp.password1!==$scope.cp.password2) $scope.passwordInvalid2=true
              else $scope.passwordInvalid2=false
        }


        $scope.ResetPassword=function(username,userId,password1,password2,digest) {
          
          $scope.dataLoading = true;

          AuthenticationService.PswReset(username,userId,password1,password2,digest, function(response) {
            
            $timeout(function(){

                if(response.success) {

                    $scope.passwordChangeSuccess=true
                    $scope.successMsg = response.message;
       
                    }
                    else {
                         
                      $scope.errorMsg = response.message;
                      $scope.passwordChangeSuccess=false
                      
                    }

                  $scope.dataLoading = false;


            })
          });

        }
        
         
  }

changePasswordCtrl.$inject=['$scope', '$rootScope', '$state','$cookies','$timeout','AuthenticationService','MenuService','$stateParams','$location'];


  export function changePasswordRequestCtrl ($scope, $rootScope, $state,$cookies,$timeout,AuthenticationService,MenuService) {
       
    $scope.CTRLNAME='changePasswordRequestCtrl';
    AuthenticationService.ClearCredentials();
    let vueLogin;
    
    
    function loadLogin()
    {
    vueLogin = Vue.createApp({
        data(){
          
          return {
            reg: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,24}))$/,
            signupSuccess: false,
            errorMsg: '',
            dataLoading: false,
            emailInvalid:false,
            email:''
          }
        },
      
        methods: {
           isEmailValid() {
              return (this.email == "")? "" : (this.reg.test(this.email)) ? 'is-valid' : 'is-invalid';
              },
            hideError() {
         
          },
          checkEmailValidity() {
           this.errorMsg=''
           if (this.email=='' || this.email==undefined || this.email==null) this.emailInvalid= true;
           else this.emailInvalid=false; 
          },
          login() {
            
            let vm = this;
            AuthenticationService.RequestPswReset(this.email, function(response) {
        
              if(response.success) {
                  
                  //AuthenticationService.SetCredentials(response.data, vm.username,vm.password);
                  vm.signupSuccess=true
     
                  }
                  else { 
                    vm.errorMsg = response.message;
                  /*  setTimeout(function(){
                      vm.errorMsg="";
                      },10000);*/
                    vm.dataLoading = false;
                  }
            });
  
          }
        
      },
      computed: {
          validity: function () {
              return this.reg.test(this.email)
      }
      
      }
    
    })

      vueLogin.mount('#loginForm')
    }
    
    $timeout(loadLogin, 0, false );


      
    }

  changePasswordRequestCtrl.$inject=['$scope', '$rootScope', '$state','$cookies','$timeout','AuthenticationService','MenuService'];

//import {createApp} from 'vue';
export default function loginCtrl ($scope, $rootScope, $state,$cookies,AuthenticationService,MenuService,$location,$timeout) {
    
    AuthenticationService.ClearCredentials();
    let vueLogin;
    
    
    
    function loadLogin(lang)
    {
    vueLogin = Vue.createApp({
         
      data() {
        return {
          errorMsg: '',
          dataLoading: false,
          usernameInvalid:false,
          passwordInvalid:false,
          firstTime:true,
          username:'',
          password:'',
          query_hash:'',
          pageLabels:authJson[lang],
          loginMsg: authJson[lang]?.loginMsg || 'Loging in...',
          usernameLabel: authJson[lang]?.username || 'Username',
          passwordLabel: authJson[lang]?.password || 'Password',
          usernameError: authJson[lang]?.usernameError || 'Username is required',
          passwordError: authJson[lang]?.passwordError || 'Password is required',
          loginButton: authJson[lang]?.loginButton || 'Login',
          changePasswordType: authJson[lang]?.changePasswordType || 'external',
          pageHeader: authJson[lang]?.pageHeader || 'Please login',
          dataLoadingText:"<span class='fa fa-hourglass mr-2'></span>"+authJson[lang]?.dataLoadingText || "<span class='fa fa-hourglass'></span> Logging in...",
          loginErrorMsg:authJson[lang]?.loginErrorMsg || 'Login failed'
          

        }
      },
      
      methods: {
        checkUsernameValidity() {
         if (this.username=='' || this.username==undefined || this.username==null) this.usernameInvalid= true;
         else this.usernameInvalid=false; 
        },
        checkPasswordValidity() {
         if (!this.firstTime && this.password=='' || this.password==undefined || this.password==null) this.passwordInvalid= true;
         else this.passwordInvalid=false;
         this.firstTime=false;
        },
        login() {
          
          
          this.firstTime=false;
          this.checkUsernameValidity()
          this.checkPasswordValidity()
         
          if (!this.usernameInvalid && !this.passwordInvalid)
          {
            this.dataLoading=true;
            let vm = this;
            AuthenticationService.Login(this.username, this.password,function(response) {
        
              if(response.success) {
                  
                  AuthenticationService.SetCredentials(response.data, vm.username,vm.password);
                    vm.dataLoading=true;
                   //load menus
                  
                  
                  var lang = $rootScope.globals.currentUser.lang;
                  var default_page = $rootScope.globals.currentUser.uep?.Params?.DefaultPageUri;
                  var params = $rootScope.globals.currentUser.uep?.Params?.DefaultPageParams;
                  let default_app = $rootScope.globals.currentUser.organisation?.DefaultApp

                  var stateName
                  if (default_page!=undefined) stateName=default_page
                  else stateName = 'app.navbar.home'          

                  if (default_app!=undefined)
                  {
                    let app_ref=$location.search().app_ref;

                    if (app_ref===undefined)
                    {
                      app_ref = $rootScope.globals.currentUser?.organisation?.DefaultApp
                    }
                    AuthenticationService.loadApp("",{},app_ref);

                  }else{
                  
                    var navbar=MenuService.getNavbarByTargetName(stateName,lang);
                    if (params==undefined) params={};
                    $state.go(stateName,params);
                  }
              }
              else {
                       
                  vm.errorMsg = vm.loginErrorMsg;
                  vm.dataLoading = false;
              }
            });
          }
        },
       
      }
      
    })

      vueLogin.mount('#loginForm')
    }
    
    $timeout(function()
      {
        let lang = $location.search().lang
        if (lang===undefined) lang ='hr-HR'

        loadLogin(lang)

      }, 0, false);
  
  }

  loginCtrl.$inject=['$scope', '$rootScope','$state','$cookies','AuthenticationService','MenuService','$location','$timeout'];

export function ssoCtrl ($scope, $rootScope, $state,$cookies,AuthenticationService,MenuService,$location) {
    
    AuthenticationService.ClearCredentials();

      let vueLogin;
    
    
    function loadLogin()
    {
    vueLogin = Vue.createApp({
         
      data() {
        return {
          errorMsg: '',
          dataLoading: false,
  
        }
      },
      
      methods: {
        loginSso() {
          let vm = this;

          vm.dataLoading=true;
          AuthenticationService.LoginSsoFetch(function(response) {
      
            if(response.success) {
                
                AuthenticationService.SetCredentialsSso(response.data, null,null);
                  vm.dataLoading=true;
                 //load menus
                var lang = $rootScope.globals.currentUser.lang;
                var default_page = $rootScope.globals.currentUser.uep?.Params?.DefaultPageUri;
                var params = $rootScope.globals.currentUser.uep?.Params?.DefaultPageParams;
                
                var stateName = 'app.navbar.home';           
                if (default_page!=undefined) stateName=default_page;
                
               if (params==undefined) params={};
                $state.go(stateName,params);
          
            }
            else {
                     
                vm.errorMsg = response.message;
                vm.dataLoading = false;
                }
          });

        }

        },
      mounted(){
      
        this.loginSso();   
      }
      
  })

    vueLogin.mount('#loginForm')
  }
    
    $timeout(loadLogin, 0, false );  
      
}

  ssoCtrl.$inject=['$scope', '$rootScope', '$state','$cookies','AuthenticationService','MenuService','$location'];


export function logoutCtrl ($scope,$rootScope, $state, AuthenticationService, MessagingService,ProcessingService) {
        // reset login status
      $scope.CTRL='logoutCtrl';
      $scope.ac={
         alerts : []
         };
      let lang=$rootScope.globals.currentUser.lang
      $scope.pageLabels = authJson[lang]
       AuthenticationService.Logout(function(response) {
         $scope.activity.activityMessage = ProcessingService.setActivityMsg('Logging out...');
         if(response.success) {
              AuthenticationService.ClearCredentials();
              
              MessagingService.clearAllMessages();
              $scope.activity.activityMessage = ProcessingService.setActivityMsg('');
         }else
         {
             $scope.errorMsg = response.message;
                
            
        }
      //  spinnerService._unregister('html5spinner');
        
       });
    
        $scope.login_page = function()
        
        {
            let auth_type = $rootScope.globals.currentUser.auth_type;
            if (auth_type==='sso') $state.go('app.sso');
            else $state.go('app.login');
            
        }
    
       // User.signOut();
        // $state.go('app.logout');
                    
        };

logoutCtrl.$inject=['$scope', '$rootScope', '$state','AuthenticationService','MessagingService','ProcessingService'];

