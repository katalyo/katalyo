'use strict';

export function vpageCtrl($scope, $rootScope, $location,KatalyoStateManager,ResourcesDataService,UtilityService,MessagingService,$timeout,$stateParams,GridsterService,$element,dragularService,MenuService,$state) {
    
    $scope.gridsterType ="page";
    $scope.CTRLNAME='vpageCtrl';

      var page_id=$stateParams.pageid;
      var lang=$rootScope.globals.currentUser.lang;
      $scope.resourceId=page_id;//MenuService.getResourceDefIdByPageId(page_id,lang).id;
      $scope.localVars={page_state:{}};
      if ($stateParams.parameters!=undefined && $stateParams.parameters!="")  $scope.localVars.page_state = {};
     
      
       $scope.formRenderingCompleted=false;
       $scope.ksm = KatalyoStateManager;
        $scope.gsrvc = GridsterService

        $scope.gsrvc.setFormViewType(2)
       
        let pageValueState
        let pageValueStateName 
       
        let menuVisibility= $scope.ksm.addSubject("menuVisibility",$scope.localVars.page_state?.Parameters);
        let menuVisibilityName = menuVisibility.subject.name;
        

        let page_value_changed = function(data)
        {
            pageValueState.subject.setStateChanges = data;
            pageValueState.subject.notifyObservers("pageValueChanged");
        };
        
        let menu_visibility_changed = function(data)
        {
            menuVisibility.subject.setStateChanges = data;
            menuVisibility.subject.notifyObservers("menuVisibilityChanged");
        };

         let menu_visibility_action = $scope.ksm.addAction(menuVisibilityName,menuVisibilityName+"Changed",menu_visibility_changed);
        
        $scope.setPageState= function () {
            
            let action_page_value_changed

            if ($stateParams.parameters.page_state_id!=undefined && $stateParams.parameters.page_state_id.trim()!="" && $stateParams.parameters.use_state)
            {
                //load state directly from params
                $scope.localVars.page_state.page_state_id = $stateParams.parameters.page_state_id;
                $scope.localVars.page_state.previousTask = $stateParams.parameters.prev_task
                pageValueState  = $scope.ksm.addSubject($stateParams.parameters.page_state_id,$scope.localVars.page_state);
                pageValueStateName = pageValueState.subject.name
                if (pageValueState.status==='ok') action_page_value_changed = $scope.ksm.addAction(pageValueStateName,"pageValueChanged",page_value_changed);
       

            }else if ($stateParams.parameters.use_state || $stateParams.parameters?.use_state===undefined && $stateParams.parameters.page_state_id!==undefined)
            {   
                pageValueState  = $scope.ksm.addSubject($stateParams.parameters.page_state_id,null);
                pageValueStateName = pageValueState.subject.name
                if (pageValueState.status==='ok') action_page_value_changed = $scope.ksm.addAction(pageValueStateName,"pageValueChanged",page_value_changed);
                //load last state
                 if (pageValueState.subject.currentStateIndex>=0)
                {
                    $scope.localVars.page_state = pageValueState.subject.getState;
                } else{
                    //create new state
                    //generate guid
                    let uuid = UtilityService.uuidv4();
                    $scope.localVars.page_state.page_state_id = $state.current.name+"-"+page_id+"-"+uuid;
                    $scope.localVars.page_state.previousTask = $stateParams.parameters.prev_task
                    pageValueState  = $scope.ksm.addSubject($scope.localVars.page_state.page_state_id,null);
                    pageValueStateName = pageValueState.subject.name
               
                    if (pageValueState.status==='ok') action_page_value_changed = $scope.ksm.addAction(pageValueStateName,"pageValueChanged",page_value_changed);
                    pageValueStateName = pageValueState.subject.name;
                    
                    $scope.ksm.executeAction(pageValueStateName,"pageValueChanged", $scope.localVars.page_state);
                }
            }
            else
            {
                //create new state
                //generate guid
                let uuid = UtilityService.uuidv4();
                $scope.localVars.page_state.page_state_id = $state.current.name+"-"+page_id+"-"+uuid;
                $scope.localVars.page_state.previousTask = $stateParams.parameters.prev_task
                pageValueState  = $scope.ksm.addSubject($scope.localVars.page_state.page_state_id,null)
                pageValueStateName = pageValueState.subject.name
                if (pageValueState.status==='ok') action_page_value_changed = $scope.ksm.addAction(pageValueStateName,"pageValueChanged",page_value_changed);
                
                $scope.ksm.executeAction(pageValueStateName,"pageValueChanged", $scope.localVars.page_state);
            }
            $scope.resourceDefinition.page_state_id=$scope.localVars.page_state.page_state_id
        
           
            $scope.ksm.executeAction(menuVisibilityName,menuVisibilityName+"Changed", $scope.localVars.page_state.Parameters);
        }
        /*        
        var modalId =  document.getElementById('loadingPageModal')
        
        $(modalId).modal({
            keyboard: false
        })

        $scope.localVars.loadPageModal = function(modalState,pageLoaded){
            
            $(modalId).modal(modalState)

            if (pageLoaded!==undefined) $timeout(function(){$scope.pageLoaded=pageLoaded})
        
        };
        */
        $scope.getResourceForm = function () {
                
              ResourcesDataService.getResourceForm($scope.resourceId,'i',lang).then(function(data) {
                                if (data.data.widgets.length>0)
                                {
                                     $scope.dashboard = {widgets:{layout:data.data.widgets}}
                                }else $scope.dashboard={widgets : {layout:GridsterService.getEmptyTaskForm()}};          
                                $scope.resourceDefinition= data.data.resourceDefinition;

                                $scope.resourceDefinition.formParams = {formViewType:2}

                                $scope.gsrvc.setFormViewType($scope.resourceDefinition.formParams.formViewType)
                                $scope.resourceDefinition.form_definition = false

                                $scope.setPageState();

                                $scope.pageLoaded=true
                                
                                },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                                  $scope.pageLoaded=true
                                    
                             });
      };
        $scope.getResourceDefinition = function () {
          
                ResourcesDataService.getResourceDefAll($scope.resourceId).then(function(data) {
                               
                             // MessagingService.addMessage(data.msg,'success');
                        $scope.resource = data.data;
                        if ($scope.resource.Parameters==null)
                        {
                                
                                $scope.resource.Parameters = {navbar:true,sideMenu:true,pageGridItems:[{id:1}],grid:{NoOfRows:null,NoOfColumns:null,RowGap:null,ColumnGap:null}}       
                        }
                        $scope.localVars.page_state = $scope.resource;
                        $scope.localVars.sideMenus = $scope.resource.ResourceExtended?.page_menu
                        
                        if ($scope.localVars.sideMenus === undefined) $scope.localVars.sideMenus = []
                        $scope.getResourceForm()

                        //$scope.calcColumnSize();
                       
                        //$scope.calcRowSize();
                              
                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                });
        };
        
        

      if ($scope.resourceId != undefined && $scope.resourceId)
      {
        $scope.getResourceDefinition();
        
      }else{
        
         $scope.formLoaded=true;
         $scope.unauthorised = true;
         
      }


       /*
      $scope.pageGridItems = [];
     
      $scope.calcRowSize = function()
      {
        $scope.rowSizes="";
        
        
        for (let i=0;i<$scope.resource.Parameters.grid.NoOfRows;i++)
        {
         //$scope.rowSizes = $scope.rowSizes+" auto";
         $scope.rowSizes = $scope.rowSizes+ "minmax(0,1fr)"; //TODO - zbog overflowa tablice u gridu, vidjeti negativne efekte
        }
      }
      $scope.calcColumnSize = function()
      {
        $scope.columnSizes="";
        for (let i=0;i<$scope.resource.Parameters.grid.NoOfColumns;i++)
        {
         //$scope.columnSizes = $scope.columnSizes+"auto"
         $scope.columnSizes = $scope.columnSizes+"minmax(0,1fr)";//TODO - zbog overflowa tablice u gridu, vidjeti negativne efekte
        }
            
        
      }
      */
      
    }
