'use strict';

angular.module('angularjs-dragula',[]);

//import 'ngVue';

import katalyoCtrls from './katalyoCtrls.js';

import * as katalyoDirectives from '../../../0000_common/0001_directives/angularJS/build/ktdirectives.js';

import * as widgetServices from '../../../0000_common/0002_services/angularJS/build/services.js';

import AuthenticationService,{UserService} from '../../../0000_common/0004_modules/authModule.js';
import {MenuService,ClearService} from '../../../0000_common/0004_modules/menuModule.js';
import CodesService from '../../../0000_common/0004_modules/codesModule.js';
import SettingsService from '../../../0000_common/0004_modules/settingsModule.js';
import MessagingService from '../../../0000_common/0004_modules/messagingModule.js';
import KatalyoStateManager from '../../../0000_common/0004_modules/stateManagerModule.js';
import ProcessingService from '../../../0000_common/0004_modules/processingModule.js';
import ResourcesDataService from '../../../0000_common/0004_modules/resourcesDataModule.js';
import TaskDataService,{PublicTaskDataService,TaskDefinitionService,TaskProcService} from '../../../0000_common/0004_modules/tasksDataModule.js';
import GridsterService from '../../../0000_common/0004_modules/gridsterModule.js';
import RestApiService from '../../../0000_common/0004_modules/apiModule.js';
import WidgetDataExchangeService from '../../../0000_common/0004_modules/widgetDataExchangeModule.js';
import WidgetRegisterService from '../../../0000_common/0004_modules/widgetRegisterModule.js';
import UtilityService from '../../../0000_common/0004_modules/utilityModule.js';
import ProcessingInterceptor from '../../../0000_common/0004_modules/interceptorModule.js';
import {AuthInterceptor} from '../../../0000_common/0004_modules/interceptorModule.js';
import exceptionHandler from '../../../0000_common/0004_modules/errorModule.js';

var kds = angular.module('KatalyoDirectiveServices',[]);

angular.forEach(widgetServices, function(value, key) {
  kds.service(key,value);
 });

var ceh = angular.module('CustomErrorHandler',[]);
ceh.decorator('$exceptionHandler',exceptionHandler);


var appModuleName = 'Katalyo';

var app = angular.module(appModuleName, [
    katalyoCtrls,
    'colorpicker',
    'KatalyoDirectiveServices',
    'CustomErrorHandler',
    'ui.router',
    'ui.grid',
    'ui.bootstrap',
    'ngAnimate',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.resizeColumns',
    'ui.grid.autoResize',
    'ui.grid.grouping' ,
    'ui.grid.moveColumns',
    'ui.grid.saveState',
    'ui.grid.exporter',
    'ui.grid.edit',
    'ngCookies',
    'ui.select',
    'ngSanitize',
    'ui.uploader',
    'ngFileUpload',
    'dragularModule',
    angularDragula(angular),
    'vcRecaptcha',
    'trumbowyg-ng',
    'chart.js',
    'smart-table',
    'lrDragNDrop'
])
.service('AuthenticationService',AuthenticationService)
.service('MenuService',MenuService)
.service('ClearService',ClearService)
.service('CodesService',CodesService)
.service('SettingsService',SettingsService)
.service('MessagingService',MessagingService)
.service('KatalyoStateManager',KatalyoStateManager)
.service('ProcessingService',ProcessingService)
.service('ResourcesDataService',ResourcesDataService)
.service('TaskDataService',TaskDataService)
.service('PublicTaskDataService',PublicTaskDataService)
.service('TaskDefinitionService',TaskDefinitionService)
.service('TaskProcService',TaskProcService)
.service('GridsterService',GridsterService)
.service('RestApiService',RestApiService)
.service('UserService',UserService)
.service('WidgetDataExchangeService',WidgetDataExchangeService)
.service('WidgetRegisterService',WidgetRegisterService)
.service('UtilityService',UtilityService)
.config(['$provide','$httpProvider','$locationProvider',function ($provide, $httpProvider,$locationProvider) {
  $httpProvider.interceptors.push(ProcessingInterceptor.ProcessingInterceptorService);
  $httpProvider.interceptors.push(AuthInterceptor.AuthInterceptorService);
  $locationProvider.html5Mode(true);
}]);

angular.forEach(katalyoDirectives, function(value, key) {
  app.directive(key,value);
});

//import vue components - find a generic way
//app.value('kt-time',ktTime);


export default appModuleName;

app.constant('PROCESSING_CONST', {'_START_REQUEST_':'_START_REQUEST_','_END_REQUEST_': '_END_REQUEST_'});

app.constant('CODES_CONST', {'_POPULATE_TYPE_':{11:'Current task initiate form',12:'Current task execute form',21:'Previous task initiate form',22:'Previous task execute form',3:'Fixed resource'},
              '_POPULATE_TYPE_NEW_':{1:'Fixed dataset',2:'Dataset record from this task',3:'Dataset record from previous task'}});

app.filter('getCode', function () {
  return function (input) {
      
      if (input!=null) {
        //code
     
          if (input.trim()!="") {
            //code
          
          var cleanInput=input.split('\'').join('"');
          var inputObj={};
          
          inputObj = angular.fromJson(cleanInput);
          return inputObj.name;
          }else
          {
            return input;
            
          }
      }else
      {
        
        return input;
        
      }
    };
});

app.filter('getUser',['UserService', function (UserService) {
  var cache = {};
  
  function getUser(input) {
      
     // if (cache['user'+input]) {
       // return cache['user'+input].name;
     // }else
     // {
        if (input!=undefined) {
          
          var user_obj=null;
          UserService.getUserObjectById(input).then(function (data){
            user_obj = data;
            if (user_obj!=null)
            {
                     
             return  user_obj.name;              
            }
          }); 
         
         // else
          //{
          //  getUser(input);
         // }
      
        }
      //}
      
    };
    getUser.$stateful = true;
    return getUser;
    
}]);

app.filter('getCodeById',['$rootScope','CodesService', function ($rootScope,CodesService) {
  var cache = {}
  function getCodeById (input,entity) {
    
     var lang = $rootScope.globals.currentUser.lang;
      if (cache[lang+entity+input]) {
        //code
        return cache[lang+entity+input];
      }
      cache[lang+entity+input] ='fetching codes...';
        if (entity!=undefined && input!=undefined) {
    
          CodesService.getCodesValue(entity,lang,input,1).then(function (data){
    
            cache[lang+entity+input] = data;
          }); 
     
     
                          
        }else{
          
           cache[lang+entity+input] ='';
        }
         
        
    };
    getCodeById.$stateful = true;
    return getCodeById;
    
}]);

app.filter('getCodeByName',['$rootScope','CodesService', function ($rootScope,CodesService) {
   var cache = {}
  function getCodeByName (input,entity) {
    
     var lang = $rootScope.globals.currentUser.lang;
      if (cache[lang+entity+input]) {
        //code
        return cache[lang+entity+input];
      }
      cache[lang+entity+input] ='fetching codes...';
      if (entity!=undefined && input!=undefined) {
    
      
         CodesService.getCodesValueByName(entity,lang,input,2).then(function (data){
    
            cache[lang+entity+input] = data;
         }); 
      }else{
          
           cache[lang+entity+input] ='';
        }
     
                          
     //                         
               //            });
         
    };
    getCodeByName.$stateful = true;
    return getCodeByName;
}]);


app.config(['$provide','$stateProvider', '$urlRouterProvider','$locationProvider','$httpProvider','$controllerProvider','$compileProvider','$filterProvider',
            function($provide,$stateProvider, $urlRouterProvider,$locationProvider,$httpProvider,$controllerProvider,$compileProvider,$filterProvider) {
   
      app.register = {
      controller: $controllerProvider.register,
      directive: $compileProvider.directive,
      filter: $filterProvider.register,
      factory: $provide.factory,
      service: $provide.service
    };
    
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

  
   //$urlRouterProvider.otherwise("app.otherwise"); 
   //$locationProvider.hashPrefix("");
   //.html5Mode(true)
  $stateProvider
    .state('app', {
           abstract: true,
           url:'?app_ref',
          template: "<div ui-view></div>",
            data: {
            requireLogin: true // this property will apply to all children of 'app'
            }
    
        })
.state('app.menu', {
            url:'/m',
            //parent: 'app',
           abstract: true,
            views: {
                'header@': {
                    templateUrl: '/0001_index/angularTemplates/navbar.html',
                   // controller: 'homeNavbarCtrl'
                },
                'content@': {
                    templateUrl: '/0001_index/angularTemplates/layout.html',
                    //controller: 'homeContentCtrl' 
                },
              
              'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'homeCtrl' 
            },
            }
        }) 
.state('app.nomenu', {
            url:'/nm',
           
           abstract: true,
            views:
            {
                'content@': {
                    templateUrl: '/0001_index/angularTemplates/layout.html',
                   
                },
               'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               },
            }
})

.state('app.navbar', {
            url:'/n',
            //parent: 'app',
           abstract: true,
            views: {
                'header@': {
                    templateUrl: '/0001_index/angularTemplates/navbar.html',
                   // controller: 'homeNavbarCtrl'
                },
                'content@': {
                    templateUrl: '/0001_index/angularTemplates/layout.html',
                    //controller: 'homeContentCtrl' 
                },
              
              'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'homeCtrl' 
            },
            }
        })

.state('app.navbar.onboarding', {
            url:'/onboarding',
            //parent: 'app',
              views: {
              
               'contentR': {
                    templateUrl: '/0001_index/angularTemplates/onboarding.html',
                    controller: 'onboardingCtrl', 
                },
              /*   'menu': {
                    templateUrl: '/0003_resources/angularTemplates/homeMenu.html',
                   controller: 'menuCtrl' 
                },*/   
          
              },
               
            
})
.state('app.navbar.home', {
            url:'/',
            //parent: 'app',
              views: {
              
               'contentR': {
                    templateUrl: '/0001_index/angularTemplates/home.html',
                    controller: 'homeCtrl', 
                },
              /*   'menu': {
                    templateUrl: '/0003_resources/angularTemplates/homeMenu.html',
                   controller: 'menuCtrl' 
                },*/   
          
              }
              
            
        })
.state('app.menu.home.dashboard', {
            url:'/dashboard',
            //parent: 'app',
            views: {
              'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/homeDashboard.html',
                    controller: 'homeDashboardCtrl' 
                
              },
            }
        }) 
.state('app.menu.resources', {
      //      parent: 'app',
              abstract: true,
              url:'/resources',
              views: {
              /*  'header@': {
                    templateUrl: '/0001_index/angularTemplates/navbar.html',
                   // controller: 'homeNavbarCtrl'
                },*/
                'content@': {
                    templateUrl: '/0001_index/angularTemplates/layout.html',
                    //controller: 'resourceUnifiedCtrl' 
                },
              /*
              'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'loginCtrl' 
            },*/
              }
              
        })
.state('app.navbar.resources', {
      //      parent: 'app',
              abstract: true,
              url:'/resources',
              views: {
              /*  'header@': {
                    templateUrl: '/0001_index/angularTemplates/navbar.html',
                   // controller: 'homeNavbarCtrl'
                },*/
                'content@': {
                    templateUrl: '/0001_index/angularTemplates/layout.html',
                    //controller: 'resourceUnifiedCtrl' 
                },
              /*
              'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'loginCtrl' 
            },*/
              }
              
        })
.state('app.menu.resources.import', {
             url:'/import/:resource_id/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/importData.html',
                    controller: 'importDataCtrl' 
                },
                 'menu': {
                   templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
                params: {
                resource_id: {
                value: '',
                },
                id: {
                value: '',

                },
               
                menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
 })
 .state('app.menu.resources.list', {
           url:'',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/resourceList.html',
                    controller: 'resourceListCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
               params: {
                menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                resource_type: {
                value: '',
                //squash: false,
                },
                resource_name: {
                value: '',
                //squash: false,
                },
                }
})
 .state('app.menu.resources.new', {
           url:'/new',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/newResource.html',
                   controller: 'newResourceCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
              },
                  params: {
                from_resource: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                resource_type: {
                value: '',
                //squash: false,
                },
                }
              
})
 .state('app.menu.resources.edit', {
           url:'/edit/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/editResource.html',
                   controller: 'editResourceCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
              },
              params: {
                id: {
                value: '',
                //squash: false,
                },
                prev_state: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
              }
})
 .state('app.navbar.resources.view', {
           url:'/view/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/viewResource.html',
                   controller: 'viewResourceCtrl' 
                },
               
              },
              params: {
                id: {
                value: '',
                //squash: false,
                },
                prev_state:{
                 value:'', 
                }
              }
}) 
.state('app.menu.resources.form', {
           url:'/form/:id',
      //      parent: 'app',
              views: {
       
                 'contentR': {
                  templateUrl: '/0003_resources/angularTemplates/resourceForm.html',
                  controller: 'resourceFormCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/toolbox.html',
                    controller: 'toolboxCtrl' 
                },
              },
              params: {
                id: {
                value: '',
                //squash: false,
                },
                menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                subTypeSelected: {
                value: '',
                },
                returnState: {
                value: '',
                },
                
                }
}) 
.state('app.menu.resources.form-vue', {
           url:'/form-vue/:id',
      //      parent: 'app',
              views: {
          
                 'contentR': {
                  templateUrl: '/0003_resources/angularTemplates/resourceFormVue.html',
                  controller: 'resourceFormComponent' 
                },
                 'menu': {
                    templateUrl: '/0003_resources/angularTemplates/toolboxVue.html',
                    controller: 'toolboxComponent' 
                },
              },
              params: {
                id: {
                value: '',
                //squash: false,
                },
                menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        }) 

.state('app.menu.resources.mappings', {
           url:'/mappings/:id',
              views: {
                 'contentR': {
                  templateUrl: '/0003_resources/angularTemplates/mappingsForm.html',
                  controller: 'mappingsFormCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
              },
              params: {
                id: {
                value: '',
                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        })
 .state('app.menu.resources.data', {
           url:'/rdata/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/resourceData.html',
                    controller: 'resourceDataCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        })
 .state('app.menu.resources.data-record', {
           url:'/data-record/:resource_id/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/resourceDataRecord.html',
                    controller: 'resourceDataRecordCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
                params: {
                resource_id: {
                value: '',
                //squash: false,
                },
                id: {
                value: '',
                //squash: false,
                },
                menu_id: {
                value: '',
                //squash: false,
                },
                }
        })
  .state('app.menu.resources.dataset-transform', {
           url:'/dataset-transform',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/datasetTransform.html',
                    controller: 'datasetTransformCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
               params: {
               
                menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
              
        })
.state('app.navbar.resources.application-definition', { 			
             url:'/application-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/applicationDefinition.html',
                    controller: 'applicationDefinitionCtrl' 
                },
          
              },
              params: {
                navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                id: {
                value: '',
                //squash: false,
                },
              }
                
              
 })
.state('app.navbar.resources.app-menu-definition', { 			
             url:'/app-menu-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/appMenuDefinition.html',
                    controller: 'appMenuDefinitionCtrl' 
                },
          
              },
              params: {
                id: {
                value: '',
                
                },
                fromApplication: {
                value: '',
                
                },
              }
                
              
 })
.state('app.navbar.resources.integration-definition', { 			
             url:'/integration-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/integrationDefinition.html',
                    controller: 'integrationDefinitionCtrl' 
                },
          
              },
              params: {
                navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                id: {
                value: '',
                //squash: false,
                },
              }
                
              
 })
.state('app.navbar.resources.notification-definition', { 			
             url:'/notification-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/notificationDefinition.html',
                    controller: 'notificationDefinitionCtrl' 
                },
          
              },
              params: {
                navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                id: {
                value: '',
                //squash: false,
                },
              }
                
              
 })
.state('app.navbar.resources.token-definition', { 			
             url:'/token-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/tokenDefinitionVue.html',
                    controller: 'tokenDefinitionCtrl' 
                },
          
              },
              params: {
               
                id: {
                value: '',
                //squash: false,
                },
              }
                
              
 })
.state('app.navbar.resources.smart-contract-definition', { 			
             url:'/smart-contract-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/smartContractDefinition.html',
                    controller: 'smartContractDefinitionCtrl' 
                },
          
              },
              params: {
                id: {
                value: '',
                //squash: false,
                },
              }
                
              
 })
  .state('app.menu.resources.pages', {
             url:'/pages',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/pages.html',
                    controller: 'pagesCtrl' 
                },
                 'menu': {
                   templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
                navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
              }
                
              
 })
.state('app.menu.resources.pages-form', {
             url:'/pages-form/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/pagesForm.html',
                    controller: 'pagesFormCtrl' 
                },
                 'menu': {
                   templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
              prev_state: {
                value: '',
                //squash: false,
                },
              navbar_id: {
                value: '',
                //squash: false,
                },
              sidemenu_id: {
                value: '',
                //squash: false,
                },
              }
                
 })
.state('app.navbar.resources.pages-definition', {
             url:'/pages-definition/:id',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/pagesDefinition.html',
                    controller: 'pagesDefinitionCtrl' 
                },
                
          
              },
              params: {
              id: {
                value: '',

                },
               
               navbar_id: {
                value: '',
                //squash: false,
                },
               prev_state: {
                value: '',
                //squash: false,
                },
                }
 })
.state('app.menu.resources.menus-definition', {
             url:'/menus-definition',
              views: {
                 'contentR': {
                    templateUrl: '/0003_resources/angularTemplates/menusDefinition.html',
                    controller: 'menusDefinitionCtrl' 
                },
                 'menu': {
                   templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
            
 })
 .state('app.menu.codes', {
    url: '/codes',
    //parent:'app',
    abstract: true,
      
    views: {
               
                  'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
              
     },
     
     
  })
.state('app.menu.codes.list', {
    url: '',
    //parent:'app',
      views: {
                'contentR@app.menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesData.html',
                    controller: 'codesDataCtrl' 
                },
               
      },
                  'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
      
          params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
   
  })
    .state('app.menu.codes.details', {
    url: '/details',
    //parent:'app',
        views: {
                 'contentR@app.menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesDetails.html',
                  controller: 'codesDetailsCtrl' 
                },
               /* 'menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesMenu.html',
                   controller: 'menuCtrl' 
                },*/   
      },
      params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
   
   
  })
    
.state('app.menu.codes.new', {
    url: '/new',
    //parent:'app',
        views: {
                 'contentR@app.menu': {
                  templateUrl: '/0004_settings/angularTemplates/codesNew.html',
                  controller: 'codesNewCtrl' 
                },
               /* 'menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesMenu.html',
                   controller: 'menuCtrl' 
                },*/   
      },
      params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
   
   
  })
   
 /*
.state('app.menu.codes.edit', {
    url: '/edit/:id',
    //parent:'app',
        views: {
                 'contentR@app.menu': {
                  templateUrl: '/0004_settings/angularTemplates/codesEdit.html',
                  controller: 'codesEditCtrl' 
                },   
              },
       params: {
                id: {
                value: '',
                //squash: false,
                },
               
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        
   
   
  })
  */


.state('app.menu.user', {
    url: '/user',
    //parent:'app',
    abstract: true,
      
    views: {
               
                  'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
              
     },
     params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
   
   
  })
 .state('app.menu.user.details', {
    url: '/details',
    //parent:'app',
        views: {
                 'contentR@app.menu': {
                    templateUrl: '/0004_settings/angularTemplates/userDetails.html',
                  controller: 'userDetailsCtrl' 
                },
               /* 'menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesMenu.html',
                   controller: 'menuCtrl' 
                },*/   
      },
      params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
   
   
  })
.state('app.menu.groups', {
           url:'/groups',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0004_settings/angularTemplates/groupsDefinition.html',
                    controller: 'groupsCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        })
.state('app.menu.users', {
           url:'/users',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0004_settings/angularTemplates/usersDefinition.html',
                    controller: 'usersDefinitionCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        })

.state('app.menu.tasks', {
    url: '/tasks',
    abstract:true,
        views: {
                 'content@': {
                  templateUrl: '/0001_index/angularTemplates/layout.html',
                  //controller: 'taskListCtrl' 
                },
              /*  'menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesMenu.html',
                   controller: 'menuCtrl' 
                },
                */
      },
      
   
  })
.state('app.navbar.tasks', {
    url: '/tasks',
    abstract:true,
        views: {
                 'content@': {
                  templateUrl: '/0001_index/angularTemplates/layout.html',
                  //controller: 'taskListCtrl' 
                },
              /*  'menu': {
                    templateUrl: '/0004_settings/angularTemplates/codesMenu.html',
                   controller: 'menuCtrl' 
                },
                */
      },
      
   
  })
.state('app.menu.tasks.list', {
           url:'/',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksList.html',
                    controller: 'taskListCtrl' 
                  },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                  },
                
                },
                params: {
                  
              menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
           
})
.state('app.menu.tasks.form', {
           url:'/form/:formType/:id',
      //      parent: 'app',
              views: {
  
                 'contentR': {
                  templateUrl: '/0005_tasks/angularTemplates/tasksForm.html',
                  controller: 'taskFormCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/tasktoolbox.html',
                   controller: 'taskToolboxCtrl' 
                },
              },
              params: {
                id: {
                value: '',
                //squash: false,
                },
                formType: {
                value: '',
                //squash: false,
                },
               
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                resource:{value: {}},
                }
        }) 
.state('app.menu.tasks.new', {
           url:'/new',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksNew.html',
                    controller: 'taskNewCtrl' 
                },
                'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
})
.state('app.navbar.tasks.definition', {
           url:'/definition/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksDefinition.html',
                    controller: 'taskDefinitionCtrl' 
                },
            /*     'menu': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksDefinitionList.html',
                   controller: 'taskDefinitionListCtrl' 
                },   
          */  
          
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                  tab_id: {
                value: '',
                //squash: false,
                },
              
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
})
.state('app.menu.tasks.definition2', {
           url:'/definition2/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksDefinitionDetailed.html',
                    controller: 'taskDefinitionCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
            
          
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                  tab_id: {
                value: '',
                //squash: false,
                },
              
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
})
.state('app.menu.tasks.initiate-list', {
           url:'/initiate-list',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksInitiateList.html',
                    controller: 'taskInitiateListCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },
              },
            params: {
                              
                navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
              
              }
               
                
})
.state('app.menu.tasks.execute-list', {
           url:'/execute-list',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksExecuteList.html',
                    controller: 'taskExecuteListCtrl' 
                  },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                  },
                },  
             params: {
                navbarId: {
                value: '',
                //squash: false,
                },
              menu_id: {
                value: '',
                //squash: false,
                },
          
                navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
               
                }
})
.state('app.menu.tasks.proc', {
           url:'/proc/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksProc.html',
                    controller: 'taskProcCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                    controller: 'menuCtrl' 
                },   
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                fromStateName: {
                value: '',
                //squash: false,
                },
                fromStateParams: {
                value: '',
                //squash: false,
                },
                prev_task_execute_id: {
                value: '',
                //squash: false,
                },
                prev_task_package: {
                value: '',
                //squash: false,
                },
                outcome_id: {
                value: '',
                //squash: false,
                },
                presentation_id: {
                value: '',
                //squash: false,
                },
                 init_type: {
                value: '',
                //squash: false,
                },
                menu_id: {
                value: '',
                //squash: false,
                },
                 navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                
                }
})
.state('app.menu.tasks.initiate', {
           url:'/initiate/:id/:prev_task_execute_id/:outcome_id/:presentation_id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksInitiate.html',
                    controller: 'taskInitiateCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                    controller: 'menuCtrl' 
                },   
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                fromStateName: {
                value: '',
                //squash: false,
                },
                fromStateParams: {
                value: '',
                //squash: false,
                },
                prev_task_execute_id: {
                value: '',
                //squash: false,
                },
                prev_task_package: {
                value: '',
                //squash: false,
                },
                outcome_id: {
                value: '',
                //squash: false,
                },
                presentation_id: {
                value: '',
                //squash: false,
                },
                
                menu_id: {
                value: '',
                //squash: false,
                },
                
                }
})
.state('app.menu.tasks.execute', {
           url:'/execute/:id/:task_initiate_id/:task_execute_id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksExecute.html',
                    controller: 'taskExecuteCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                    controller: 'menuCtrl' 
                },   
            
          
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                fromStateName: {
                value: '',
                //squash: false,
                },
                fromStateParams: {
                value: '',
                //squash: false,
                },
                  task_initiate_id: {
                value: '',
                //squash: false,
                },
                 task_execute_id: {
                value: '',
                //squash: false,
                },
                prev_task_package: {
                value: '',
                //squash: false,
                },
                for_group: {
                value: '',
                //squash: false,
                },
               
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                
                }
})
.state('app.menu.tasks.claim', {
           url:'/claim',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksExecuteList.html',
                    controller: 'taskExecuteListCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                    controller: 'menuCtrl' 
                },   
            
          
              },
              params: {
               
               navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
})

.state('app.menu.tasks.outcome-definition', {
           url:'/outcome-definition/:id',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksOutcomeDefinition.html',
                    controller: 'taskOutcomeDefinitionCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                    controller: 'menuCtrl' 
                },   
            
          
              },
                params: {
                id: {
                value: '',
                //squash: false,
                },
                fromStateName: {
                value: '',
                //squash: false,
                },
                fromStateParams: {
                value: '',
                //squash: false,
                },
             
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                
                }
})
.state('app.menu.organisation', {
           url:'/organisation',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0004_settings/angularTemplates/organisation.html',
                    controller: 'organisationCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
                   navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        })
.state('app.menu.community', {
           url:'/community',
      //      parent: 'app',
              views: {
                 'contentR': {
                    templateUrl: '/0004_settings/angularTemplates/community.html',
                    controller: 'organisationCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
          
              },
              params: {
              id: {
                value: '',

                },
                   navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
        })

.state('app.menu.menu-create', {
           url:'/menu-create',
           abstract:true,
      //      parent: 'app',
              views: {
                 'content@': {
                  templateUrl: '/0001_index/angularTemplates/layout.html',
                  //controller: 'taskListCtrl' 
                },
          
              }
})
.state('app.menu.menu-create.navbar', {
           url:'/menu-create/navbar',
           
      //      parent: 'app',
             views: {
                 'contentR': {
                    templateUrl: '/0004_settings/angularTemplates/createNavbar.html',
                    controller: 'organisationCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                   controller: 'menuCtrl' 
                },   
          
              },
              params: {
              id: {
                value: '',

                },
                 menu_id: {
                value: '',
                //squash: false,
                },
                  navbar_id: {
                value: '',
                //squash: false,
                },
                sidemenu_id: {
                value: '',
                //squash: false,
                },
                }
})
.state('app.menu.vpage', {
           url:'/vpage/:pageid',
           
      //      parent: 'app',
             views: {
                 'contentR': {
                    templateUrl: '/0006_vpage/angularTemplates/vpage.html',
                    controller: 'vpageCtrl' 
                },
                 'menu': {
                    templateUrl: '/0001_index/angularTemplates/menu.html',
                    controller: 'menuCtrl' 
                },   
          
              },
              params: {
                pageid: {
                value: '',
                //squash: false,
                },
                parameters: {
                value: '',
                //squash: false,
                }
                }
})
.state('app.signup', {
            url:'/signup',
           // parent: 'app',
            views: {
          /*  'header@': {
                templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
               // controller: 'simpleheaderController'
            },*/
            'content@': {
                templateUrl: '/0001_index/angularTemplates/signup.html',
                controller: 'signupCtrl' 
            },
           /* 'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'loginCtrl' 
            },
           */
        },
         data: {
            requireLogin: false // change default value from 'app'
            
            }
    })
.state('app.register', {
            url:'/register/:signupid',
           // parent: 'app',
            views: {
           /* 'header@': {
                templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
               // controller: 'simpleheaderController'
            },*/
            'content@': {
                templateUrl: '/0001_index/angularTemplates/register.html',
                controller: 'registerCtrl' 
            },
            'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'loginCtrl' 
            },
           params: {
                signupid: {
                value: '',
                //squash: false,
                },
           }

        },
         data: {
            requireLogin: false // change default value from 'app'
            
            }
    }) 
.state('app.nomenu.publictasks', {
           url:'/public/:org_id/:task_name',
            //parent: 'app',
           views: {
              
               'contentR': {
                    templateUrl: '/0005_tasks/angularTemplates/tasksPublicExecute.html',
                    controller: 'taskPublicCtrl',
                },
             
              },
           params: {
            
             org_id: {
              value: '',
               
            },
            task_name: {
              value: '',
               
            },
            task_id: {
              value: '',
               
            }            
          },
           data: {
           
              requireLogin: false // change default value from 'app'
            
                }
            
})


.state('app.index', {
 
 // redirectTo: () => "app.navbar.home",
 // data: {
           
 //   requireLogin: true // change default value from 'app'
            
  //}  
   url:'/',
            views: {
            /*'header@': {
                templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
            },*/
            'content@': {
                templateUrl: '/0001_index/angularTemplates/app_home.html',
                controller: 'changePasswordRequestCtrl' 
            },
            'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
            },
           
        },
       data: {
              requireLogin: false // change default value from 'app'
              }  
})
.state('app.changepassrequest', {
            url:'/changepassrequest',
            views: {
            'header@': {
                templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
            },
            'content@': {
                templateUrl: '/0002_auth/angularTemplates/changePasswordRequest.html',
                controller: 'changePasswordRequestCtrl' 
            },
            'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
            },
           
        },
       data: {
              requireLogin: false // change default value from 'app'
              }  
}) 
.state('app.changepass', {
            url:'/changepass/:digest',
            views: {
                'header@': {
                    templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
                },
                'content@': {
                    templateUrl: '/0002_auth/angularTemplates/changePassword.html',
                    controller: 'changePasswordCtrl' 
                },
                'footer@': {
                    templateUrl: '/0001_index/angularTemplates/footer.html',
                },
                params: {
                    digest: {
                    value: '',
                    },
                } 
            },
               data: {
                      requireLogin: false // change default value from 'app'
                      }  
}) 
.state('app.login', {
            url:'/login',
           // parent: 'app',
            views: {
            'header@': {
                templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
               // controller: 'simpleheaderController'
            },
            'content@': {
                templateUrl: '/0002_auth/angularTemplates/login.html',
                controller: 'loginCtrl' 
            },
            'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'loginCtrl' 
            },
        },
       data: {
           
              requireLogin: false // change default value from 'app'
            
              }  
}) 
.state('app.logout', {
    url: '/logout',
    //parent:'app',
      views: {
                'header@': {
                    templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
                   // controller: 'simpleheaderController'
                },
                'content@': {
                    templateUrl: '/0002_auth/angularTemplates/logged_out.html',
                    controller: 'logoutCtrl'
                  
                },
      },
   
    data: {
      requireLogin: false
    }
})
.state('app.sso', {
            url:'/sso',
           // parent: 'app',
            views: {
            'header@': {
                templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
               // controller: 'simpleheaderController'
            },
            'content@': {
                templateUrl: '/0002_auth/angularTemplates/sso.html',
                controller: 'ssoCtrl' 
            },
            'footer@': {
                templateUrl: '/0001_index/angularTemplates/footer.html',
               // controller: 'loginCtrl' 
            },
            params: {
                    is_ldap: {
                    value: false,
                    },
                } 
        },
       data: {
           
              requireLogin: false // change default value from 'app'
            
              }  
}) 
.state("app.otherwise", {
    url: "*path",
    views: {
                'header@': {
                    templateUrl: '/0002_auth/angularTemplates/simpleheader.html',
                    
                },
                'content@': {
                    templateUrl: '/0001_index/angularTemplates/error_not_found.html',
                    controller: 'errorNotFoundCtrl'
                  
                },
      },
     
    data: {
            requireLogin: false   
            }
});    

}]);

app.run(['$rootScope','$state', '$cookies','$location','$timeout','RestApiService','AuthenticationService','MenuService',
    function ($rootScope,$state,$cookies, $location,$timeout,RestApiService,AuthenticationService,MenuService) {
        
        if ($rootScope.globals===undefined) $rootScope.globals={}

        $rootScope.globals.currentUser = $cookies.getObject('currentUser') || null;
        
        
        if ($rootScope.globals.currentUser) {
          if ($rootScope.globals.currentUser.authdata.substring(0,5)==='Token')
          {
            RestApiService.add_api_classes('token',$cookies.get('csrftoken'),$rootScope.globals.currentUser.authdata);
          }
          else RestApiService.add_api_classes('sso',$cookies.get('csrftoken'));    
          }
        else{
          $rootScope.globals = {currentUser:{lang:'en-GB'}};
          RestApiService.add_api_classes('sso',$cookies.get('csrftoken'));
        }
     
        var lastFromState,lastToState;
        var requireLogin;
        
        // M -- Sets up the router to report state changes to the Hotjar script. 
        //$rootScope.$on('$stateChangeSuccess',function () {
        //    hj('stateChange', window.location.href);
        //});
        
        
        $rootScope.$on('$stateChangeStart', function (ev,toState,toParams,fromState,fromParams) {
        // $transitions.onStart({},function(trans) {
          var loadApp=false
          if (toState.name!="app.login" && toState.name!="app.sso" && toState.name!="app.logout" && toState.name!="app.otherwise" && toState.name!='app.nomenu.publictasks' && toState.name!='app.changepassrequest')
          {
            var today = new Date();
            var expiresValue = new Date(today);

            //Set 'expires' option in 10 minute
            expiresValue.setMinutes(today.getMinutes() + 10); 
            
            $cookies.putObject('lastToState',toState, {'expires' : expiresValue});
            $cookies.putObject('lastToParams',toParams, {'expires' : expiresValue});
            loadApp=true
          }
           if ((fromState==toState) || (lastFromState==fromState &&  lastToState==toState)) 
            {
              MenuService.setSelectedAppMenuWithParams(toState.name,toParams)
              return;
            }
           if (typeof toState.data != undefined)  {                
                requireLogin = toState.data.requireLogin;
           }else requireLogin = true;
           
          // if (bypass) return;
           let app_ref=$location.search().app_ref;

           if (app_ref===undefined)
           {
            app_ref = $rootScope.globals.currentUser?.organisation?.DefaultApp
           }
         
          
          if ($rootScope.globals.currentUser?.username!==undefined && $rootScope.globals.currentUser?.username!==null && $rootScope.globals.currentUser?.username!=="") //actual non-empty user exists
          { 
            lastFromState=fromState;
            lastToState=toState;
            $rootScope.globals.InterceptorIsActive = true;
         
            if (app_ref!=undefined &&  app_ref!=null && !MenuService.isAppSelected() && loadApp) 
              {
                
                AuthenticationService.loadApp(toState.name,toParams,app_ref);
                
              }
            else 
              {
                MenuService.setSelectedAppMenuWithParams(toState.name,toParams)

                $state.go(toState.name,toParams);
              }
            
          }
          else //user is empty
          {
            if (!requireLogin) //login not required
            {
              lastFromState=fromState;
              lastToState=toState;
              $rootScope.globals.InterceptorIsActive = true;
             
             
             //ui-router 1.x mig
              //if (app_ref!=undefined &&  app_ref!=undefined!=null && !MenuService.isAppSelected()) AuthenticationService.loadApp(toState.name,toParams,app_ref);
              //else
              MenuService.setSelectedAppMenuWithParams(toState.name,toParams)
              $state.go(toState.name,toParams); 
             // return trans.router.stateService.target(trans.router.stateService.to);
             
              
            }
            else //login is required - go to login page
            {
              //ui-router 1.x mig
              ev.preventDefault();
              $state.go('app.login');
              //return trans.router.stateService.target('app.login');
            }
          }
           
       
         });
        
    }]);

