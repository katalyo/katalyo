'use strict';


//import Katalyo from '../../0001_index/angularJS/index.js';
/*
var moduleName='indexCtrls';
export default moduleName;
var module = angular.module(moduleName);
*/
import indexJson from './index.json' assert {type: 'json'};

export default function appController ($scope,$rootScope,$window,$location,$state,GridsterService,MenuService,ResourcesDataService,MessagingService,AuthenticationService,KatalyoStateManager) {
        
      
        $scope.CTRL='appController';
        $scope.gsrvc = GridsterService;
        $scope.menu_service = MenuService;
        var padForB = $scope.gsrvc.getPaddingForBody();
        $scope.activity={
        activityMessage : ""
        };
        
         $scope.ac={
         alerts : []
         }; //{dt:"",message:"",type:"",show:false}
        
        $scope.currentState=$state.current.name;

        $scope.showActivity = function (msg) {
            setTimeout(function () {
            
              $scope.activity.activityMessage = msg;
              
             });
         
        };
        $scope.showNavbar = true;
        $scope.showSideMenu = true;
        let menu_visibility_process = function(data)

        {
                if (data.navbar!==undefined) $scope.showNavbar = data.navbar;
                if (data.sideMenu!==undefined) $scope.showSideMenu = data.sideMenu;
        }

        $scope.ksm = KatalyoStateManager; 
       
        let menuVisibility= $scope.ksm.addSubject("menuVisibility",null);
        let menuVisibilityName = menuVisibility.subject.name;

        let dependent_field = "appCtrl"
        let observer = $scope.ksm.subscribe(menuVisibilityName,dependent_field,"menuVisibilityChanged",menu_visibility_process);
        let state = $scope.ksm.getState(menuVisibilityName);
        if (state.status=="ok" && state.state!==null) menu_visibility_process(state.state);

        let lang  = $rootScope.globals.currentUser.lang
        $scope.pageLabels = indexJson[lang]
        $scope.checkIfActive = function(ni)

        {
              return  ni.katalyonode_pages_Target==$scope.menu_service.currentTarget && ni.katalyonode_menus_Target_id==$scope.menu_service.currentPageId
        }
        $scope.multiSelectKeyUpDown = function (ev,id) {
          var code = ev.keyCode || ev.which;
          var key = ev.key;
          var elem=$window.document.getElementById(id);
         
             //enter
             if ((key==='p' || key==='P' || key==='b' || key==='B') && ev.ctrlKey)
             {
                ev.preventDefault();
                $scope.$broadcast("CtrlPBPressed",key);
             }
             
             /*
              if (code==13){
                //select elemnt in focus
                   
              }
              //keyup
              if (code==38){
                //move focus to next element
                 ev.preventDefault();
                 elem.next().focus(); 
              }
              //keydown
              if (code==40){
                //move focus to previous element
                 ev.preventDefault();
                 elem.previous().focus(); 
              }
              */
        }
       
};
appController.$inject = ['$scope','$rootScope','$window','$location','$state','GridsterService','MenuService','ResourcesDataService','MessagingService','AuthenticationService','KatalyoStateManager'];

export function homeNavbarCtrl($scope,$rootScope,$state) {
        //$scope.navbar = "/static/modules/0001_index/angularTemplates/navbar.html";
       $scope.CTRL='homeNavbarCtrl';
    };

homeNavbarCtrl.$inject = ['$scope','$rootScope','$state'];
    
export function errorNotFoundCtrl ($scope,$rootScope,$state,$stateParams) {
        //$scope.navbar = "/static/modules/0001_index/angularTemplates/navbar.html";
        //$scope.previousPage=$stateParams.fromState;
        $scope.params = $state.params;
        $scope.CTRL='errorNotFoundCtrl';
};

errorNotFoundCtrl.$inject = ['$scope','$rootScope','$state','$stateParams'];


export function homeCtrl($scope, $rootScope,$state,$cookies,KatalyoStateManager,ProcessingService,MessagingService,CodesService,AuthenticationService,MenuService,ResourcesDataService,TaskDataService,GridsterService,SettingsService,$timeout,$stateParams){
        
     
        $scope.CTRL='homeCtrl';
        $rootScope.headTitle ="Katalyo | Home";
        $scope.currentState=$state.current.name;
        $scope.selectedApp = {'name':''}
        $scope.localVars = {};
        $scope.fd={};
        $scope.taskDefinition={};
        var lang = $rootScope.globals.currentUser.lang;
        $scope.currentUserFirstName=$rootScope.globals.currentUser.first_name;
        $scope.extendedUser = angular.copy($rootScope.globals.currentUser.uep);
        $scope.resource={};
        $scope.resource_page_size=12;
        if ($scope.extendedUser==undefined) $scope.extendedUser = {};
        
        $scope.menu_service = MenuService;
        
        $scope.gsrvc=GridsterService;
        //connect to metamask
       
        $scope.linkWeb3Wallet = function()
        {
                $scope.balance="";
                $scope.balance_ktlyo="";
                $scope.ktlyo_balance_ok = false;
                $scope.ktlyo_balance_msg = "";
                $scope.eth_address=null;
               
                        if ($scope.fd.eth_address!=null)
                        {
                                $scope.ktlyoAddressConnected=true;
                                var wei;
                                web3.eth.getBalance($scope.fd.eth_address, function (error, wei) {
                                        if (!error) {
                                        var balance = web3.utils.fromWei(wei, 'ether');
                                        $scope.balance = balance + " ETH";
                                        }
                                });
                                
                                var tokenAddress = "0x24E3794605C84E580EEA4972738D633E8a7127c8";
                                var tokenABI = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}];
                               
                                var tokenInst = new web3.eth.Contract(tokenABI,tokenAddress);
                                
        
                                tokenInst.methods.balanceOf($scope.fd.eth_address).call().then(function (bal_raw) {
                                        var bal = web3.utils.fromWei(bal_raw, 'ether');
                                        if (bal<100)
                                        {
                                                $scope.ktlyo_balance_ok = false;
                                                $scope.ktlyo_balance_msg = "Insufficient balance";
                                        }else $scope.ktlyo_balance_ok = true;
                                        $timeout(function(){
                                                bal = parseFloat(bal).toLocaleString(navigator.language,{minimumFractionDigits:4,maximumFractionDigits:4});
                                                $scope.balance_ktlyo = bal+ " KTLYO";
                                                });
                                        });
                        
                         return true;
                }
              
                return false;
        }     
        
        $scope.getKtlyoBal = function()
        {
                //var tokenAddress = "0x24E3794605C84E580EEA4972738D633E8a7127c8";
                var eth_address = $rootScope.globals.currentUser.organisation.ETHAccount;
                
                //"0xc30c523a8eee10e36beb7a6d07c636e02762cb27";
                if (eth_address==null || eth_address.trim()=="")
                {
                    $scope.noKtlyoAddress=true;
                } else
                {
                        
                        $scope.linkedBlockchainAddress=true;
                        $scope.fd.eth_address = eth_address;
                        SettingsService.GetKtyloBalance(eth_address).then(function (data) {
                                let bal = data.data.balance;
                                
                                $rootScope.globals.currentUser.uep.ktlyo_block = data.data.block
                                $rootScope.globals.currentUser.uep.ktlyo_balance = bal

                                bal = bal.toLocaleString(navigator.language,{minimumFractionDigits:4,maximumFractionDigits:4});
                                $scope.balance_ktlyo =bal+" KTLYO" ;
                                $scope.block_ktlyo = "(at block "+data.data.block+ ")";
                                $scope.balance = data.data.balance_eth + " ETH (at block "+data.data.block_eth+ ")" ;
                                $scope.balanceChecked=true;
                               

                                $cookies.putObject('currentUser',$rootScope.globals.currentUser)
                        },function(error){
                                      
                                        MessagingService.addMessage(error.msg,'error');  
                        });
                }
        }
        
        $scope.mapKtlyoBal = function()
        {
                //var tokenAddress = "0x24E3794605C84E580EEA4972738D633E8a7127c8";
                var eth_address = $rootScope.globals.currentUser.organisation.ETHAccount;
               
                //"0xc30c523a8eee10e36beb7a6d07c636e02762cb27";
                if (eth_address==null || eth_address.trim()=="")
                {
                    $scope.noKtlyoAddress=true;
                } else
                {
                        
                       // $timeout(function(){
                                $scope.linkedBlockchainAddress=true;
                                $scope.fd.eth_address = eth_address;
                                $scope.balance_ktlyo = $rootScope.globals.currentUser.uep.ktlyo_balance?.toLocaleString(navigator.language,{minimumFractionDigits:4,maximumFractionDigits:4})+" KTLYO" ;
                                $scope.balance_ktlyo_num = $rootScope.globals.currentUser.uep.ktlyo_balance;
                                $scope.block_ktlyo = "(at block "+$rootScope.globals.currentUser.uep.ktlyo_block?.toLocaleString(navigator.language,{minimumFractionDigits:0,maximumFractionDigits:0})+ ")";
                                $scope.balanceChecked=true;
                                if ($scope.balance_ktlyo_num>=100) $scope.ktlyo_balance_ok = true;
                                else
                                {
                                        $scope.ktlyo_balance_ok = false;
                                        $scope.ktlyo_balance_msg = "Insufficient balance!";
                                }
                                
                         //       });
                        /*
                        SettingsService.GetKtyloBalance(eth_address).then(function (data) {
                                let bal = data.data.balance;
                                $scope.balance_ktlyo =bal+" KTLYO" ;
                                $scope.block_ktlyo = "(at block "+data.data.block+ ")";
                                $scope.balance = data.data.balance_eth + " ETH (at block "+data.data.block_eth+ ")" ;
                                $scope.balanceChecked=true;
                        },function(error){
                                      
                                        MessagingService.addMessage(error.msg,'error');  
                        });
                        */
                }
        }
        
        //$scope.getKtlyoBal();
        
        if ($scope.extendedUser.use_blockchain) $scope.mapKtlyoBal(); 
        
        $scope.$watch("fd.web3Connected",function(newValue,oldValue){
                if ($scope.fd.eth_address!=null && !$scope.linkedBlockchainAddress) $scope.linkWeb3Wallet();
        });
        
        
         $scope.changeWeb3WalletLinkStatus = function(status)
        {
          $scope.linkedBlockchainAddress=status;
          $scope.fd.web3Connected=status;
          $scope.fd.eth_address=null;
        }
        
        $scope.openedTaskInNewPage={localVars:null,taskDefinition:null};
    
        $scope.openTaskInNewPage=function(){
        
                $scope.openedTaskInNewPage = $scope.gsrvc.getTaskDataInNewPage();
        };
        
        
        $scope.saveBlockchainAccount=function(){
        
                SettingsService.SaveBlockchainAccount($scope.fd.eth_address).then(function (data) {
                                MessagingService.addMessage(data.data,'success');
                                //MenuService.LoadMenus($scope).then(function () { });
                                $rootScope.globals.currentUser.organisation.ETHAccount = $scope.fd.eth_address
                               
                                $scope.getKtlyoBal();
                                $scope.ktlyoAddressConnected=false; 
                        },function(error){
                                      
                                        MessagingService.addMessage(error.data,'error');  
                });        };
        
        $scope.localVars.loadedTasks=[];
        
        $scope.extendedUser.taskDefinitions = [];
        
      
      $scope.LoadTasks = function()
      {
        if ($scope.extendedUser.TaskListHome!=undefined)
        {
                $scope.localVars.loadedTasks.length = 0;
        
                for (var i=0;i<$scope.extendedUser.TaskListHome.length;i++)
                {
                    var localVars = {'isInlineTask':$scope.localVars.isInlineTask,'taskIndex':$scope.localVars.loadedTasks.length,'loadTaskDirect' : true,'taskId':$scope.extendedUser.TaskListHome[i].id,'outcomeId':0,'previousTask':0};
                    $scope.localVars.loadedTasks.push({'taskId':$scope.extendedUser.TaskListHome[i].id,'outcomeId':0,'previousTask':0,'localVars':localVars});
                      
                }
        }
      };
      //$scope.LoadTasks();
        //get apps
        $scope.getApps = function()
        {
            ResourcesDataService.GetResourceSubscriptions('application').then(function (data) {
                               
                          $scope.appList =data.data;
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
          
        }
        
        $scope.getMyApps = function()
        {
            ResourcesDataService.GetResourceSubscriptions('application').then(function (data) {
                               
                          $scope.appList =data.data;
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
          
        }
        
        $scope.getUnsubscribedAppsFiltered = function(start,end)
        {
            ResourcesDataService.GetResourceSubscriptions('application').then(function (data) {
                               
                          $scope.appList =data.data;
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
          
        }
        
        /*
         $scope.getAllApps = function()
        {
          ResourcesDataService.getResourceDefByType(lang,'application').then(function (data) {
                               
                          $scope.appListFull =data.data;
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        
        }*/
        $scope.params={showAppList:1,subscribing:false};
        $scope.getApps();
        
         $scope.loadApp = function(app)
        {
               app.loadingApp=true
               //get application params and load menus
               AuthenticationService.SetMenusForApp(app).then(function(data) {

                   //load default page
                   $scope.getDefaultPage(app,data);
                   
                 },function(error){
                  MessagingService.addMessage(error.msg,'error')
                  app.loadingApp=false
                 });
               
               
        };
        
        $scope.findLink = function (link_type,app) {
                for (let i=0;i<app.ResourceExtended.length;i++) 
                {
                        if (app.ResourceExtended[i].LinkType==link_type) return app.ResourceExtended[i].Resource2;
                }
                return null;
        }
        $scope.getDefaultPage = function (iapp,menu_loaded) {
                
                ResourcesDataService.getResourceDefAll(iapp.id).then(function(data) {
                               
                        // MessagingService.addMessage(data.msg,'success');
                        let app = data.data;
                        let pageId = $scope.findLink("default-page",app);
                              
                              
                        if (pageId===undefined || pageId===null)
                        {
                                
                                MessagingService.addMessage('Default page is not set!','info');
                                MessagingService.addMessage('#'+app.Name+' application is not fully launched!','warning');
                                iapp.loadingApp=false
                        }
                        else
                        {
                               ResourcesDataService.getResourceDefAll(pageId).then(function(data) {
                               
                                        // MessagingService.addMessage(data.msg,'success');
                                        let page = data.data;
                                        if (!menu_loaded)  MessagingService.addMessage('#'+app.Name+' application is not fully launched!','warning');
                                        else MessagingService.addMessage('#'+app.Name+' application launched successfuly!','info');
                                        
                                        
                                        $state.go(page.ResourceExtended.Target  ,{ pageid: pageId, app_ref:app.id});
                                        iapp.loadingApp=false 
                                },function(error){
                                         
                                         MessagingService.addMessage(error.msg,'error');  
                                         iapp.loadingApp=false
                                });
                        }
                              
                },function(error){
                              
                      MessagingService.addMessage(error.msg,'error');  
                      iapp.loadingApp=false
                });
                                
           
        };
        
           /*
                ResourcesDataService.getResourceDefByType(lang,'application').then(function (data) {
                               
                          $scope.appListFull =data.data;
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
              
                
                 */
         $scope.subscribe_unsubscribe = function(app,index,sub_type)
        {
                app.subscribing=true;
                
                ResourcesDataService.ManageResourceSubscriptions(app,'application',sub_type).then(function (data) {
                               
                          MessagingService.addMessage(data.data.msg,'success');
                          app.subscribing=false;
                          $scope.appList[index].SubId=data.data.data.SubId;
                          $scope.appList[index].ActiveSub=data.data.data.ActiveSub;
                          
                          
                },function(error){
                              
                              if (error.data['msg']!=undefined) MessagingService.addMessage(error.data.msg,'error');
                              else MessagingService.addMessage(error.data,'error');
                              app.subscribing=false;
                });
                
        }
        
        
        $scope.getApplications = function(pageNumber,pageSize){
   
                
                $scope.resource_page_size = pageSize;
                $scope.resourceLoading=true;
            
                var filter=$scope.resource;
                 
                if ($scope.totalCount>0){
                    if (Math.ceil($scope.totalCount/pageSize)<newPage)
                    {
                      newPage = Math.ceil($scope.totalCount/pageSize);
                    }
                 }

        
                ResourcesDataService.getAppsFilteredBySubStatus(pageNumber,pageSize,filter,sub_status,lang).then(function(data) {
      
                                $scope.firstQuery=true;
                                $scope.appListUnsubscribed = data.data.results;
                                $scope.nextPage = data.data.next;
                                $scope.previousPage = data.data.previous;
                                $scope.totalItemCount=data.data.count;
                                $scope.currentPage=pageNumber;
                                $scope.numberOfPages = Math.ceil($scope.totalItemCount/pageSize);
                                
                                if (($scope.totalItemCount<=pageSize && $scope.resourceData.length>0) || $scope.totalItemCount==0) $scope.hideBtnMore = true;
                                else $scope.hideBtnMore=false;
                                
                                $scope.resourceLoading=false;
                                
                                },function(error){
       
                                 MessagingService.addMessage(error.msg,'error');
                                 $scope.resourceLoading=false;
                             });
        };
        $scope.loadMoreApplications = function(){
                $scope.resource_page_size = $scope.resource_page_size+12;
                $scope.getApplications(1,$scope.resource_page_size);        
                
        }
        
        
        $scope.getResourceDef = function(pageNumber,pageSize){
   
                
                $scope.resource_page_size = pageSize;
                $scope.resourceLoading=true;
            
                var filter=$scope.resource;
                 
                if ($scope.totalCount>0){
                    if (Math.ceil($scope.totalCount/pageSize)<newPage)
                    {
                      newPage = Math.ceil($scope.totalCount/pageSize);
                    }
                 }

        
                ResourcesDataService.getResourceDefFiltered(pageNumber,pageSize,filter,lang).then(function(data) {
      
                                $scope.firstQuery=true;
                                $scope.resourceData = data.data.results;
                                $scope.nextPage = data.data.next;
                                $scope.previousPage = data.data.previous;
                                $scope.totalItemCount=data.data.count;
                                $scope.currentPage=pageNumber;
                                $scope.numberOfPages = Math.ceil($scope.totalItemCount/pageSize);
                                
                                if (($scope.totalItemCount<=pageSize && $scope.resourceData.length>0) || $scope.totalItemCount==0) $scope.hideBtnMore = true;
                                else $scope.hideBtnMore=false;
                                
                                $scope.resourceLoading=false;
                                
                                },function(error){
       
                                 MessagingService.addMessage(error.msg,'error');
                                 $scope.resourceLoading=false;
                             });
        };
        $scope.loadMoreResource = function(){
                $scope.resource_page_size = $scope.resource_page_size+12;
                $scope.getResourceDef(1,$scope.resource_page_size);        
                
        }
        $scope.saveHomePage = function()
        {
          
           MenuService.saveHomePage($scope.extendedUser).then(function(data) {
                         $rootScope.globals.currentUser.uep = data.data;
                         $cookies.put('globals', $rootScope.globals);
                         $scope.extendedUser = $rootScope.globals.currentUser.uep;
                         $scope.LoadTasks();
                          MessagingService.addMessage(data.msg,'success');
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };
        
        /*
               ResourcesDataService.getResourceDefListByType(lang,'task').then(function(data) {
        
                                   $scope.taskList = data.data;
                                  
                               },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                                 
                            });
        */
        
    }
    
homeCtrl.$inject = ['$scope', '$rootScope','$state','$cookies','KatalyoStateManager','ProcessingService','MessagingService','CodesService','AuthenticationService','MenuService','ResourcesDataService','TaskDataService','GridsterService','SettingsService','$timeout','$stateParams'];
    
export function onboardingCtrl ($scope, $rootScope,$state,$cookies,KatalyoStateManager,ProcessingService,MessagingService,CodesService,AuthenticationService,MenuService,ResourcesDataService,TaskDataService,GridsterService,$timeout,$stateParams){

        $scope.CTRL='onboardingCtrl';
        $rootScope.headTitle ="Katalyo | Onboarding";
        $scope.currentState=$state.current.name;
        $scope.selectedApp = {'name':''}
        $scope.localVars = {};
        $scope.fd={};
        $scope.taskDefinition={};
        $scope.localVars.pageLoaded=false;
        var lang = $rootScope.globals.currentUser.lang;
        $scope.currentUserFirstName=$rootScope.globals.currentUser.first_name;
        $scope.extendedUser = angular.copy($rootScope.globals.currentUser.uep);
        $scope.menu_service = MenuService;
        
        
        $scope.gsrvc=GridsterService;
        
        $scope.openedTaskInNewPage={localVars:null,taskDefinition:null};
    
        $scope.openTaskInNewPage=function(){
        
                $scope.openedTaskInNewPage = $scope.gsrvc.getTaskDataInNewPage();
        };
        
        $scope.localVars.loadedTasks=[];
        
        $scope.extendedUser.taskDefinitions = [];
        
       
     
       $scope.getResourceAll = function () {
          
            ResourcesDataService.getResourceDefAll($scope.extendedUser.Params.OnboardingAppId).then(function(data) {
                               
                             // MessagingService.addMessage(data.msg,'success');
                              $scope.resource = data.data;
                              $scope.application_name=$scope.resource.resourcedeflang_set[0].Name;
                               
                                },function(error){
                              
                              MessagingService.addMessage(error.msg,'error');  
                            });
        };      
         //get app details
        if ($scope.extendedUser.Params.Onboarding=='AppSetup')
        {
                 $scope.getResourceAll();      
        }

         $scope.localVars.pageLoaded=true;
}


 onboardingCtrl.$inject = ['$scope', '$rootScope','$state','$cookies','KatalyoStateManager','ProcessingService','MessagingService','CodesService','AuthenticationService','MenuService','ResourcesDataService','TaskDataService','GridsterService','$timeout','$stateParams'];

 
 export function demoCtrl ($scope, $rootScope,$state,$cookies,KatalyoStateManager,ProcessingService,MessagingService,CodesService,AuthenticationService,MenuService,ResourcesDataService,TaskDataService,GridsterService,$timeout,$stateParams){

        $scope.CTRL='demoCtrl';
        $rootScope.headTitle ="Katalyo | Demo";
        $scope.currentState=$state.current.name;
        //Login
}


 demoCtrl.$inject = ['$scope', '$rootScope','$state','$cookies','KatalyoStateManager','ProcessingService','MessagingService','CodesService','AuthenticationService','MenuService','ResourcesDataService','TaskDataService','GridsterService','$timeout','$stateParams'];

 
export function navbarCtrl($scope,$rootScope,$state,ResourcesDataService,CodesService,MenuService,$stateParams,$cookies,$timeout,MessagingService){


        $scope.CTRL='navbarCtrl';
        $scope.menu_service = MenuService;
        $scope.menus= {editMode: $scope.menu_service.getMenuEditMode()};
        $scope.logoutLoaded=false;
        $scope.navbarLoaded=true;
        
        $scope.cMenus=[];
        $scope.is_superuser =  $rootScope.globals.currentUser.is_superuser;
        $scope.currentPage = $state.current.name;
        $scope.currentNavbarId = $stateParams.navbar_id;
        $scope.menus.currentSideMenuId = $stateParams.sidemenu_id;
        var lang =  $rootScope.globals.currentUser.lang;
       
        $scope.getNavbarItems = function(lang)
        {
           
           $scope.menu_service.getNavbars(lang).then(function (data) {
          
            if ($scope.currentNavbarId==undefined || isNaN($scope.currentNavbarId) || $scope.currentNavbarId===0 || $scope.currentNavbarId==="")
            {
                $scope.currentNavbarId = $scope.getLinkNavbarByName($scope.currentPage);
            }
            
            
            
            if ($scope.menu_service.getMenuEditMode())
            {
              $scope.setEditNavbar($scope.currentEditNavbarId);
             
            }
            else
            {
              $scope.setNavbar($scope.currentNavbarId);
            }
            
             
          });
          
        };
        
       
     
       
       //get navbarId by page id
       $scope.getLinkNavbar = function(pageid) {
      
          return  $scope.menu_service.getPageNavbar(pageid,lang);
    
        };
      
      $scope.LoadLogout = function() {
        //$timeout(function(){
                $scope.logoutLoaded=true;
       // },1000);
        };
      //get navbarId by state name
      $scope.getLinkNavbarByName = function(currentStateName) {
      
          return  $scope.menu_service.getNavbarByTargetName(currentStateName,lang);
    
      };
     
      
    $scope.setSideMenu = function(menuId) {     
      $scope.menu_service.setSelectedSidemenu(menuId) ;
      //$scope.currentSideMenuId=menuId;
    };
      
      //SET current navbarId for editMode=false
      $scope.setNavbar = function(navbarId) {
        $scope.currentNavbarId = navbarId;
        $scope.menu_service.setCurrentNavbarId($scope.currentNavbarId,lang);
        
        //GET child menus
        $scope.cMenus = $rootScope.globals.currentUser.cMenus;
        
        if ($scope.menus.currentSideMenuId>0) $scope.setSideMenu($scope.menus.currentSideMenuId);
      //  $scope.getMenusForNavbar();
      };
    
    //GET navbar definition details
    $scope.getNavbarDetails = function(navbarId,index) {
      
      
       $scope.currentEditNavbarIndex=index;
      $scope.setEditNavbar(navbarId);
      
           //navbarItem.Users
                
    }
      
      //SET current navbarId for editMode=true
    $scope.setEditNavbar = function(navbarId) {
      $scope.menu_service.navbarSelected = false;
      $timeout(function(){
        $scope.currentEditNavbarId = navbarId;
        $scope.menu_service.setCurrentNavbarId(navbarId,lang);
       
      });
    }
      
     //generate children navbar right
              
     $scope.checkIfLast = function() {
     
      var rval=true;
     // if ($scope.menu_service.navbarItems != undefined) 
          for (var i=$scope.currentEditNavbarIndex+1; i<$scope.menu_service?.navbarItems?.length ;i++)
          {
           if ($scope.menu_service.navbarItems[$scope.currentEditNavbarIndex].katalyonode_menus_MenuPosition.id==$scope.menu_service.navbarItems[i].katalyonode_menus_MenuPosition.id)
            {
              rval= false;
            }
          }
       return rval;
     
    };
    
     $scope.checkIfFirst = function() {
     
      var rval=true;
      if ($scope.currentEditNavbarIndex==0) return rval;
      for (var i=$scope.currentEditNavbarIndex-1;i>=0;i--)
      {
       if ($scope.menu_service.navbarItems[$scope.currentEditNavbarIndex].katalyonode_menus_MenuPosition.id==$scope.menu_service.navbarItems[i].katalyonode_menus_MenuPosition.id)
        {
          rval= false;
        }
      }
       return rval;
     
    };
      
    $scope.moveNavbarLeft = function() {
     
      var element1 =  $scope.menu_service.navbarItems[$scope.currentEditNavbarIndex];
      var newIndex=$scope.currentEditNavbarIndex-1;
      while ($scope.menu_service.navbarItems[$scope.currentEditNavbarIndex].katalyonode_menus_MenuPosition.id!=$scope.menu_service.navbarItems[newIndex].katalyonode_menus_MenuPosition.id && newIndex>0)
      {
        newIndex=newIndex-1;
      }
      
      $scope.menu_service.navbarItems.splice($scope.currentEditNavbarIndex, 1);
      $scope.menu_service.navbarItems.splice(newIndex, 0, element1);
      $scope.currentEditNavbarIndex = newIndex;
     
    };
    $scope.moveNavbarRight = function() {
     
      var element2 =  $scope.menu_service.navbarItems[$scope.currentEditNavbarIndex];
      var newIndex=$scope.currentEditNavbarIndex+1;
      while ($scope.menu_service.navbarItems[$scope.currentEditNavbarIndex].katalyonode_menus_MenuPosition.id!=$scope.menu_service.navbarItems[newIndex].katalyonode_menus_MenuPosition.id && newIndex<$scope.menu_service.navbarItems.length-1)
      {
        newIndex=newIndex+1;
      }
      
      $scope.menu_service.navbarItems.splice($scope.currentEditNavbarIndex, 1);
      $scope.menu_service.navbarItems.splice(newIndex, 0, element2);
      $scope.currentEditNavbarIndex = newIndex;
    };
    
      $scope.setNavbarActive = function(navbarId) {
      if ($scope.navbarLoaded)
      {
      
      return navbarId==$scope.currentNavbarId ? "active" : "";
      }else $scope.setNavbarActive(navbarId);
    }
    
       $scope.newNavbar = function(){
           $scope.currentEditNavbarId = 0;
           $scope.menu_service.newNavbar(lang,'navbar');
       };            
    
        $scope.newChildNavbar = function(){
           $scope.currentEditNavbarId = 0;
           $scope.menu_service.newNavbar(lang,'navbarrightc');
       }; 
    
    $scope.activeLinkEditNavbar = function(navbar) {
      $scope.navbarEdit = null;
      if ($scope.navbarLoaded)
      {
      if ($scope.currentEditNavbarId!=undefined && $scope.currentEditNavbarId!=null)
      {
        $scope.navbarEdit = $scope.currentEditNavbarId;
      }
      return navbar==$scope.navbarEdit ? "active" : "";
      }else $scope.activeLinkEditNavbar(navbar);
    };
    
        $scope.saveNavbarOrder = function()
                
                {
     
                    $scope.menu_service.saveMenuOrder(lang,0).then(function(data) {
                                                                     
                                       MessagingService.addMessage(data.msg,'success');
                                    },function(error){    
                                    MessagingService.addMessage(error.msg,'error');
                              
                                 });
                  
                }; 
    
    $scope.gotoState=function(name,params)
    {
      $state.go(name,{navbarId: params});
      
    };

}

navbarCtrl.$inject = ['$scope', '$rootScope','$state','ResourcesDataService','CodesService','MenuService','$stateParams','$cookies','$timeout','MessagingService'];

export function menuCtrl($scope,$rootScope,$state,CodesService,MenuService,$stateParams,$cookies,MessagingService)
{
 
        $scope.CTRL='menuCtrl';
        $scope.menu_service = MenuService;
        $scope.menus={currentSideMenuId : $stateParams.sidemenu_id};
        var lang =  $rootScope.globals.currentUser.lang;
        $scope.activeLinkSidemenu = function(n) {
        
          return ($state.current.name.indexOf(n)>=0 ? "active" : "");
      };
      
      $scope.setSideMenu = function(menuId) {
       
        $scope.menu_service.setSelectedSidemenu(menuId) ;
        $scope.menus.currentSideMenuId=menuId;
      };
      

      //GET side menu definition details
      $scope.getSideMenuDetails = function(sidemenuId,index) {
       $scope.currentEditSideMenuIndex=index;
       $scope.currentEditSideMenuId = sidemenuId;
       $scope.setSideMenu(sidemenuId);
      }
      
      $scope.moveMenuUp = function() {
       
        var element = $scope.menu_service.selectedNavbar.SideMenus[ $scope.currentEditSideMenuIndex];
        $scope.menu_service.selectedNavbar.SideMenus.splice( $scope.currentEditSideMenuIndex, 1);
        $scope.menu_service.selectedNavbar.SideMenus.splice( $scope.currentEditSideMenuIndex-1, 0, element);
       $scope.currentEditSideMenuIndex--;
      }
      $scope.moveMenuDown = function() {
       
        var element = $scope.menu_service.selectedNavbar.SideMenus[ $scope.currentEditSideMenuIndex];
        $scope.menu_service.selectedNavbar.SideMenus.splice( $scope.currentEditSideMenuIndex, 1);
        $scope.menu_service.selectedNavbar.SideMenus.splice( $scope.currentEditSideMenuIndex+1, 0, element);
       $scope.currentEditSideMenuIndex++;
      }
      
         $scope.newSideMenu = function(){
             $scope.currentEditSideMenuId = 0;
             $scope.menu_service.newSideMenu(lang);
         }; 
       
       $scope.saveMenuOrder = function()
                  
                  {
       
                      $scope.menu_service.saveMenuOrder(lang,1).then(function(data) {
                                         
                                         $scope.menu_service.setSidemenuOrder(lang);                              
                                         MessagingService.addMessage(data.msg,'success');
                                         
                                      },function(error){    
                                      MessagingService.addMessage(error.msg,'error');
                                
                                   });
                    
                  };
      
       $scope.activeLinkSideByMenuId = function(menuItem) {
          
         return ($scope.menus.currentSideMenuId==menuItem.katalyonode_menus_id ? true : false);
         
      };
      
                   
       $scope.activeLinkSideEditByParams = function(params,menuItem) {
      
          
         return ($scope.currentEditSideMenuId==menuItem.katalyonode_menus_id ? true : false);
      
         
      };
      
      
      if ($scope.sideMenuId!=undefined && $scope.sideMenuId!="" && $scope.sideMenuId!=null)
      {
        $scope.setSideMenu($scope.sideMenuId);
      }


 }
 
 menuCtrl.$inject = ['$scope', '$rootScope','$state','CodesService','MenuService','$stateParams','$cookies','MessagingService'];

export function toolboxCtrl ($scope,GridsterService) {
         $scope.CTRL='toolboxCtrl';
       $scope.gsrvc = GridsterService;
}
toolboxCtrl.$inject =['$scope','GridsterService'];
export function taskToolboxCtrl ($scope,GridsterService) {
        
        $scope.CTRL='taskToolboxCtrl';
        $scope.gsrvc = GridsterService;
       
}
taskToolboxCtrl.$inject = ['$scope','GridsterService'];   

export function signupCtrl($scope,$http,$rootScope,$state,$timeout,vcRecaptchaService,RestApiService) {
        
       $scope.CTRL='signupCtrl';
       $rootScope.headTitle ="Katalyo | Sign up";
       $scope.signupSuccess = false;
       $scope.hideError="";
       var widgetId;
         $scope.signup = function(){
          
          // var language = $rootScope.globals.currentUser.lang;
           //if (language == undefined) language="en-GB";
           var language = "en-GB";
           $scope.dataLoading=true;
            
           let url = '../api/signup/';
                RestApiService.post(url,{'email':$scope.email,'captchaResponse':$scope.captchaResponse}).then(
                    response =>{
                          $timeout(function(){
                                    if (response.status === 200) {
                                          $scope.signupSuccess = true;
                                     } else {
                                         $scope.signupSuccess = false;
                                         $scope.errorMsg= response.data;
                                         vcRecaptchaService.reload(widgetId);  
                                    }
                                   $scope.checkingSignup=false;
                                   $scope.signupCheckDone = true;
                                   $scope.dataLoading=false;
                          });
                    },error=>{
                                $scope.dataLoading=false;
                                $scope.signupSuccess = false;
                                $scope.errorMsg=error;
                                vcRecaptchaService.reload(widgetId);        
                        
                }); 

                /*
            $http({
                method: 'POST',
                url: '../api/signup/',
                headers: { 'Content-Type': 'application/json' },

                data: {'email':$scope.email,'captchaResponse':$scope.captchaResponse}
             
            })
            .then(function (data) {

                    if (data.status === 200) {
                      $scope.signupSuccess = true;
                     
                    } else
                    
                    {
                       $scope.signupSuccess = false;
                      $scope.errorMsg=data.data;
                       vcRecaptchaService.reload(widgetId);
                    }
                    $scope.dataLoading=false;

                },
                function (error) {
                        $scope.dataLoading=false;
                        $scope.signupSuccess = false;
                        $scope.errorMsg=error.data;
                        vcRecaptchaService.reload(widgetId);
                });
				
             */   
           
          
         };

         
          $scope. onWidgetCreate = function(_widgetId){
                widgetId = _widgetId;
            };
          
          $scope.hideError = function(){
            $scope.errorMsg="";
          };
          
    }

 signupCtrl.$inject =   ['$scope','$http', '$rootScope','$state','$timeout','vcRecaptchaService','RestApiService'];
 
export function registerCtrl($scope,$http,$rootScope,$timeout,$state,$stateParams,AuthenticationService,RestApiService,MenuService,MessagingService) {
        
        $scope.CTRLNAME='registerCtrl';
        $rootScope.headTitle ="Katalyo | Register | Onboarding";
        
        //get data from hash and email
        $scope.signupkey=$stateParams.signupid;
        $scope.InvalidRequest=false;
        $scope.checkingSignup=true;
        $scope.signupOK=false;
        $scope.signupCheckDone=false;
        $scope.checkingEOSAccount = false;
        $scope.register={};
        
        $scope.register.organisation_type="Personal";
        $scope.register.signup_key=$scope.signupkey;
        
        var weakRegularExp = new RegExp("^(?=.*[a-z])|(?=.*[A-Z])|(?=.*[0-9])|(?=.*[!@#\$%\^&\*\"\(\)\=\?\+\'<>;:,\-\._/\\\\])(?=.{8,})");
        
        var midRegularExp = new RegExp("^((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))(?=.*[!@#\$%\^&\*\"\(\)\=\?\+\'<>;:,\-\._/\\\\])(?=.{8,})");  
               
        var strongRegularExp = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*\"\(\)\=\?\+\'<>;:,\-\._/\\\\])(?=.{8,})");  
       
        $scope.checkPassword = function(element)
        {
          
          if (!strongRegularExp.test(element.$modelValue)) element.$setValidity("required",false);
        }
       
        $scope.checkSignup = function(){
                
                
                
                let url = '../api/check-signup/';
                RestApiService.post(url,{'signupkey':$scope.signupkey}).then(response =>{
                       if (response.status === 200) {
                                        
                                $scope.signupOK = true;
                        }else
                        {
                         $scope.InvalidRequest = true;  
                          
                        }
                        $timeout(function(){
                                $scope.checkingSignup=false;
                                $scope.signupCheckDone = true;
                        })
                });
                 
		  
        };
        
        
        $scope.fn_register = function(valid){
          
          if (valid)
          {
                $scope.dataLoading = true
                let url = '../api/register/';
                RestApiService.post(url,$scope.register).then(response =>{
                        $timeout(function(){
                                if (response.status === 200) { // if successful, bind success message to message
                                      //set user info

                                    AuthenticationService.SetCredentials(response.data.data, $scope.register.username,$scope.register.password);
                                     MessagingService.addMessage(response.data.msg,'success');
                                    //go to home page
                                    $state.go("app.navbar.home",{});
                                    $scope.dataLoading = false
                
                                } else {
                                    $scope.errorMsg = response.data; 
                                    MessagingService.addMessage(response.data,'error');
                                    $scope.dataLoading = false     
                                }
                }); 
            });
                
          
          }			  
        };
        
        
        
        
        if ($scope.signupkey==undefined || $scope.signupkey=="")
        {
          $scope.signupCheckDone = true;
          $scope.checkingSignup=false;
        }
        else
        {
          
          $scope.checkSignup(); 
          
        }
        
        $scope.eosObject =  { account:''};
         
        $scope.checkEOSAccount = function(){
          
          // var language = $rootScope.globals.currentUser.lang;
           //if (language == undefined) language="en-GB";
           var language = "en-GB";
           $scope.eosObject.lang=language;
           $scope.eosAccountObject={result_code:-1};
          
          	
            $scope.checkingEOSAccount = true;
            
            $http({
                method: 'POST',
                url: 'https://api-prd.eosflare.io/chain/get_account',
                headers: { 'Content-Type': 'application/json' },

                data: $scope.eosObject
             
            })
            .then(function (data) {

                    if (data.status === 200) {
                      if (data.data.err_code==0)
                      {
                        $scope.eosAccountObject=data.data.result;
                        $scope.eosAccountObject.result_code=data.data.err_code;
                        $scope.eosAccountObject.account_name= $scope.eosObject.account;
                        $scope.eosAccountObject.balance_staked_progress= ($scope.eosAccountObject.balance_staked/$scope.eosAccountObject.balance_total*100);
                        if ($scope.eosAccountObject.balance_staked_progress<0.5 && $scope.eosAccountObject.balance_staked_progress) $scope.eosAccountObject.balance_staked_style_small={'min-width':'1%'};
                        $scope.eosAccountObject.balance_unstaked_progress= ($scope.eosAccountObject.balance_unstaked/$scope.eosAccountObject.balance_total*100);
                        if ($scope.eosAccountObject.balance_unstaked_progress<0.5 && $scope.eosAccountObject.balance_unstaked_progress>0) $scope.eosAccountObject.balance_unstaked_style_small={'min-width':'1%'};
                        $scope.eosAccountObject.cpu_limit_progress= ($scope.eosAccountObject.cpu_limit_used/$scope.eosAccountObject.cpu_limit_max*100);
                        if ($scope.eosAccountObject.cpu_limit_progress<0.5 && $scope.eosAccountObject.cpu_limit_progress>0) $scope.eosAccountObject.cpu_style_small={'min-width':'1%'};
                        $scope.eosAccountObject.net_limit_progress= ($scope.eosAccountObject.net_limit_used/$scope.eosAccountObject.net_limit_max*100);
                        if ($scope.eosAccountObject.net_limit_progress<0.5 && $scope.eosAccountObject.net_limit_progress>0) $scope.eosAccountObject.net_style_small={'min-width':'1%'};
                        $scope.eosAccountObject.ram_progress= ($scope.eosAccountObject.ram_usage/$scope.eosAccountObject.ram_quota*100);
                        if ($scope.eosAccountObject.ram_progress<0.5 && $scope.eosAccountObject.ram_progress>0) $scope.eosAccountObject.ram_style_small={'min-width':'1%'};
                     
                        $scope.eosAccountObject.cpu_unit_max="us";
                        $scope.eosAccountObject.net_unit_max="Bytes";
                        $scope.eosAccountObject.ram_unit_quota="Bytes";
                      
                        var convertCpuMax = $scope.convertTime($scope.eosAccountObject.cpu_limit_max);
                      
                        $scope.eosAccountObject.cpu_limit_max=convertCpuMax.size;
                        $scope.eosAccountObject.cpu_unit_max=convertCpuMax.unit;
                      
                        var convertCpuUsed = $scope.convertTime($scope.eosAccountObject.cpu_limit_used);
                      
                        $scope.eosAccountObject.cpu_limit_used=convertCpuUsed.size;
                        $scope.eosAccountObject.cpu_unit_used=convertCpuUsed.unit;
                      
                      
                        var convertNetMax = $scope.convertMemory($scope.eosAccountObject.net_limit_max);
                        
                        $scope.eosAccountObject.net_limit_max=convertNetMax.size;
                        $scope.eosAccountObject.net_unit_max=convertNetMax.unit;
                    
                    
                        var convertNetUsed = $scope.convertMemory($scope.eosAccountObject.net_limit_used);  
                        
                        $scope.eosAccountObject.net_limit_used=convertNetUsed.size;
                        $scope.eosAccountObject.net_unit_used=convertNetUsed.unit;
                    
                    
                        var convertRamQuota = $scope.convertMemory($scope.eosAccountObject.ram_quota);  
                     
                        $scope.eosAccountObject.ram_quota=convertRamQuota.size;
                        $scope.eosAccountObject.ram_unit_quota=convertRamQuota.unit;
                        
                      
                        var convertRamUsage = $scope.convertMemory($scope.eosAccountObject.ram_usage);  
                     
                        $scope.eosAccountObject.ram_usage=convertRamUsage.size;
                        $scope.eosAccountObject.ram_unit_usage=convertRamUsage.unit;
                        
                       
                    }
                    else {
                        $scope.errorMsg=data.err_msg;
                    }
                  }
                      $scope.checkingEOSAccount = false;
                    
                },function (error) {
                    
                    $scope.checkingEOSAccount = false;
                  
                });
				
               $scope.convertMemory = function(sizeInBytes){
                  
                    var convertedMemory={size:0,unit:''};
                     if (sizeInBytes==0)
                    {
                      convertedMemory.size=sizeInBytes;
                      convertedMemory.unit = "";
                    }
                    else if ((sizeInBytes/1024)<0.5)
                    {
                      convertedMemory.size=sizeInBytes;
                      convertedMemory.unit = "Bytes";
                    }
                      else if ((sizeInBytes/1024)<1024)
                      {
                        convertedMemory.size=(sizeInBytes/1024).toFixed(2);
                       
                        convertedMemory.unit ="KB";
                        
                      }
                      else if (sizeInBytes/1024<1024*1024)
                      {
                        convertedMemory.size=(sizeInBytes/1024/1024).toFixed(2);
                       
                        convertedMemory.unit ="MB";
                      }
                      else
                      {
                        convertedMemory.size=(sizeInBytes/1024/1024/1024).toFixed(2);
                    
                        convertedMemory.unit ="GB";
                      }
                  
                    return convertedMemory;
                }; 
           
          
          $scope.convertTime = function(sizeInMicroSec){
                  
                    var convertedTime={size:0,unit:''};
                    if (sizeInMicroSec==0)
                    {
                      convertedTime.size=sizeInMicroSec;
                      convertedTime.unit = "";    
                    }
                    else if ((sizeInMicroSec/1000)<0.01)
                    {
                      convertedTime.size=sizeInMicroSec;
                      convertedTime.unit = "microsec";
                    }
                    else if ((sizeInMicroSec/1000)<1000)
                      {
                        convertedTime.size=(sizeInMicroSec/1000).toFixed(2);
                       
                        convertedTime.unit ="ms";
                        
                      }
                      else if (sizeInMicroSec/1000/1000<1000)
                      {
                        convertedTime.size=(sizeInMicroSec/1000/1000).toFixed(2);
                       
                        convertedTime.unit ="s";
                      }
                      else if (sizeInMicroSec/1000/1000/60<60)
                      {
                        convertedTime.size=(sizeInMicroSec/1000/1000/60).toFixed(2);
                       
                        convertedTime.unit ="min";
                      }
                      else
                      {
                        convertedTime.size=(sizeInMicroSec/1000/1000/60/60).toFixed(2);
                    
                        convertedTime.unit ="h";
                      }
                  
                    return convertedTime;
                }; 
           
          
          
         };
          
    }
    
 registerCtrl.$inject = ['$scope','$http', '$rootScope','$timeout','$state','$stateParams','AuthenticationService','RestApiService','MenuService','MessagingService'];