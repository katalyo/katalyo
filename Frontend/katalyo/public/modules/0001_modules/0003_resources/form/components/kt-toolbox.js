
import GridsterService from '../../../../0000_common/0004_modules/gridsterModule.js';

const ktToolbox =  Vue.component('kt-toolbox', {
  data: function () {
    return {
      toolboxItems: [],
     }
  },
  methods:{
   
  },
  created: function()
  {
      this.toolboxItems= GridsterService.getToolboxItems();
          
  },
  
});

export default ktToolbox;