
import GridsterService from '../../../../0000_common/0004_modules/gridsterModule.js';

const ktTime =  Vue.component('kt-time', {
  props: ['inputFd','inputFormData','inputFormParams','inputFormParamsParent'],
  data: function () {
    return {
      fd: this.inputFd,
      formData: this.inputFormData,
      formParams: this.inputFormParams,
      formParamsParent: this.inputFormParamsParent,
      colLength: [],
      localVars: {},
     }
  },
  methods:{
    showProperties:function (event)
    {
      GridsterService.setFieldProperties(this.fd,event);
    },
  },
  created: function()
  {
          if (this.formData===undefined) this.formData = [{'Field':0}];
          if (this.formParams===undefined) this.formParams = {formViewType:1};
          if (this.formParamsParent===undefined) this.formParamsParent = {formViewType:1};
          
          if (this.fd.ParametersLang===undefined) this.fd.ParametersLang={format1:'HH:mm'};
          
          for (var i=0;i<24;i++) this.colLength.push('col-lg-'+(i+1));
             
          this.fd.timeFormatList = [{id:1,name:'HH:mm',mask:'99:99'},{id:2,name:'HH:mm:ss',mask:'99:99:99'},{id:3,name:'HH:mm'},{id:4,name:'HH:mm:ss:sss'}];
             
          var posFormatTime = this.fd.timeFormatList.map(function(e) { return e.name.toString(); }).indexOf(this.fd.ParametersLang.format1);
              
          if (posFormatTime>=0){
             
            this.fd.ParametersLang.format1=this.fd.timeFormatList[posFormatTime];
                                  
          }
             
          this.localVars.format = this.fd.ParametersLang.format1;
          
  },
  
  computed: {
    // a computed getter
    getSelectedFd: function () {
      
      return GridsterService.getSelectedFd();
    },
    getFieldPropertiesOpened: function () {
      
      return GridsterService.getFieldPropertiesOpened();
    },
    
     getName: function () {
      // 'this' points to the vm instance
      return String(this.fd.Name)+String(this.fd.ItemId);
    },
    
  }
});

export default ktTime;