'use strict';


import userDefinitionJson from './usersDefinition.json' assert {type: 'json'};

export default function codesDataCtrl ($scope, $stateParams,$state,$rootScope,uiGridConstants,ProcessingService,MessagingService,CodesService,ResourcesDataService, $q, $timeout) {
    
    $scope.CTRLNAME='codesDataCtrl';
    $scope.isCollapsed  = false;
    $scope.pageState={} //MIGOR - samo da proradi GETRESOUCEDEF
    $scope.paginationOptions={}//MIGOR - samo da proradi GETRESOUCEDEF
    $scope.showTableActions=true;// Needed by SmartTable
    $scope.codeId=0;
    $scope.searchInitiated = true
    var lang=$rootScope.globals.currentUser.lang;
     
    $scope.columnDefs = [// Needed by SmartTable
         {name: 'id', displayName: 'ID', type: 'number'},
         { name: "act", displayName: ''}, //special actions column
         {name: 'CodeName', displayName: 'Code name', type: 'string'},
         {name: 'CodeHeadName', displayName: 'Code unique identifier',  type: 'string'},
         {name: 'Description', displayName: 'Code description',  type: 'string'},
         {name: 'Active', displayName: 'Active', type: 'boolean'},
         {name: 'CodeType.name', displayName: 'Code Type',  type: 'number'},
       //  {name: 'Version', displayName: 'Version', type: 'string'},
        // {name: 'Lang', displayName: 'Language',  type: 'string'}
        ]
 
    $scope.ResourceSearch = function(page){
            $scope.searchInitiated=false;
            $scope.paginationOptions.totalItemCount=0;
            if (page!=undefined) $scope.currentPage=page;
            $timeout(function(){
                $scope.searchInitiated=true;
            });
     };
     
       $scope.gotoMessageDef = function(){
               $state.go("app.menu.codes.new",{id:$scope.resourceId,returnState:{Label:'Return to notification definition',state:$state.current,params:{id:$scope.resourceId}}});
        }
                        
                        
     $scope.processTableAction = function(row,actionId)
         {   // Needed by SmartTable - on EDIT ROW selection
            
            $scope.codeId = row.id
            $scope.codeSelected=true;
            $scope.selectedCode= row.id
            $scope.CodeHead = {}
            $scope.codesList = []
            $scope.codesDetails = []
             
            $scope.codeSelected=row.isSelected      
            $scope.selectedCode = row.id;
                  
            $scope.selectedCodeLang = row.Lang;     
            $scope.CodeHead = {CodeName: row.CodeName,CodesHeadId: row.id,Description:row.Description,Lang:row.Lang};
            
            $scope.getCodesDetails($scope.selectedCode, $scope.CodeHead.CodeName)
                  
            CodesService.CodesGetById($scope.selectedCode,lang)
                        .then(function(data) {          
                                    $scope.CodeHead = data.data;
                         },function(error){    
                                    MessagingService.addMessage(error.msg,'error');
                    });      
        };
    

       
     $scope.getCodesFiltered = function(pageNumber,pageSize,filter, lang) {// Needed by SmartTable
          let deferred = $q.defer();
         
         // if ($scope.data1 == undefined) 
        //  {
                CodesService.CodesGet(lang).then(function(data) {
                                   
                                   if (filter?.Name?.length>0)  //filter by Search query
                                       $scope.data1  = data.data.filter(item =>  item?.CodesHeadLang?.[0]?.CodeName.includes(filter.Name))
                                   else
                                       $scope.data1 = data.data;
                                   
                                   $scope.totalCount = $scope.data1.length
                                   $scope.paginationOptions.totalItemCount  =$scope.data1.length
                                   let startAt = (pageNumber - 1) *  pageSize 
                                   let newData =  $scope.data1.slice(startAt , Math.min( pageNumber * pageSize,  $scope.totalCount) )
                                  //debugger
                                   deferred.resolve( {status: 200, data:newData, totalCount: $scope.totalCount});
                              },function(error){
                                  MessagingService.addMessage(error.msg,'error');
                            })
         // } else deferred.resolve( $scope.data1);

        return deferred.promise;       
         
     }
    
     
    $scope.getResourceDef = function (pageNumber,pageSize) {// Called by SmartTable
       
        let deferred = $q.defer();    
        $scope.resourceLoading=true;
        var filter=$scope.resource;
        //debugger //getResourceDef
        
        if (pageNumber==undefined) pageNumber = $scope.paginationOptions.pageNumber;
        if (pageSize==undefined) pageSize = $scope.paginationOptions.pageSize;
        if ($scope.totalCount>0){
            if (Math.ceil($scope.totalCount/pageSize) < pageSize) {
              let newPage = Math.ceil($scope.totalCount/pageSize);
            }
         }
         $scope.pageState.resource = $scope.resource;
         
         if ($scope.firstQuery) {
          $scope.pageState.currentPage = pageNumber;
          $scope.pageState.pageSize = pageSize;           
         } else  {
                if ($scope.pageState.rememberSearch) {
                        if ($scope.currentPage == 1)  {
                                $scope.pageState.currentPage = $scope.currentPage;
                        }
                        pageNumber = $scope.pageState.currentPage;
                        pageSize = $scope.pageState.pageSize;
                }
         }
        
         // $scope.ksm.executeAction(resourceListStateName,"resourceListSearch",$scope.pageState);
        
            $scope.getCodesFiltered (pageNumber,pageSize,filter,lang).then( 
                function(response) {
                     
                   //debugger
                    
                    $scope.firstQuery=true;
                    $scope.resourceTableData = response.data;
                    let newData = {}
                    newData.results = response.data;
                    $scope.paginationOptions.totalItemCount = response.totalCount
                    
                    
                    //$scope.nextPage = data.data.next;
                    //$scope.previousPage = data.data.previous;
                 
                    $scope.currentPage=pageNumber;
                    $scope.paginationOptions.numberOfPages = Math.ceil($scope.paginationOptions.totalItemCount/pageSize);
                     
                    newData.pageSize = pageSize;
                    newData.numberOfPages = $scope.paginationOptions.numberOfPages;
                    newData.columns = $scope.columnDefs;
                    newData.count = response.totalCount
                    deferred.resolve({data: newData });
                   
                    $scope.resourceLoading=false;
                }
                , function (error) {
                 deferred.resolve( {data: newData });
                }
            ) 
                                 
                            
     return deferred.promise;
    }; // ---getResourceDEf ---
      
   
    CodesService.getCodesByName('languages',lang)
		.then( (data) =>  {
            //debugger
            $scope.codeLanguageList = data.data; });
    CodesService.getCodesByName('_CODE_TYPE_LIST_',lang)
		.then( (data) =>{ $scope.codeTypeList = data.data; });
     CodesService.getCodesByName('codestatus',lang)
		.then( (data) => {  $scope.codeStatusList = data.data; });
                               
           
     
    
    $scope.setDirectiveFn = function(directiveFn) { // directiveFn == getCodesDetails()
        //debugger //setDirectiveFn 
        $scope.directiveFn = directiveFn;
    };

     $scope.isCollapsedEditItem = false;
     
     
    $scope.editCodeItem = function(){
        
        //populate from grid
        $scope.codeItemHeader={Text: 'Edit code item',Type:'Edit'};
         if ($scope.CodesDetailLang.Lang==undefined) {
          //code
           $scope.CodesDetailLang.Lang=$scope.CodeHeadLang.Lang;
        }
         $scope.CodesDetailLang.CreatedBy=null;
         $scope.CodesDetailLang.ChangedBy=null;
        
        $scope.isCollapsedEditItem = !$scope.isCollapsedEditItem;
            
    }
    
  $scope.CodesDetail = {codesdetaillang_set: []};
  $scope.CodesDetailLang ={CodesDetailId:{}}; 
  
  
      $scope.newCodeItem = function(){
        
            $scope.codeItemHeader={Text: 'New code item',Type:'New'};
            //populate from grid
            $scope.isCollapsedEditItem = !$scope.isCollapsedEditItem;
            $scope.CodesDetailLang.id=undefined;
            $scope.CodesDetailLang.Lang=$scope.CodeHeadLang.Lang;
            $scope.CodesDetailLang.CodesDetailId.CodesHeadId=$scope.CodeHeadLang.CodesHeadId;
        }
     
    $scope.editCodeLang = function(){
        //populate from grid
        $scope.isCollapsedEditLang = !$scope.isCollapsedEditLang 
    }
     
    $scope.saveCodeLang = function(id) {
        //debugger
            $scope.isCollapsedEditLang = !$scope.isCollapsedEditLang
       
            CodesService.CodesPost($scope.CodeHead).then(function(data) {    
                         $scope.data1 = data.data;
                          //$scope.gridOptions1.data = $scope.data1;
                       
                    },function(error){
                      
                        MessagingService.addMessage(error.msg,'error');
                  });
      
    }
     
    $scope.getListOfCodes = function(){

       var lang=$rootScope.globals.currentUser.lang;
       CodesService.CodesGet(lang).then(function(data) {
                                
                                   $scope.data1 = data.data;
                                  //  $scope.gridOptions1.data = $scope.data1;
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });    
    }
    
 
    $scope.getListOfCodes();
    
    
     
      $scope.saveCodeItem = function (isValid) {
           //debugger
           if (isValid) {
              //var codesStatus = CODES_LIST._CODE_STATUS_LIST_;
            $scope.CodesDetailLang.CodesDetailId.Status = 'Active';
        
                CodesService.addUpdateCodesDetails($scope.codeItemHeader.Type,$scope.CodesDetailLang).then(function(data) {
                                
                                  MessagingService.addMessage(data.msg,'success');
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });
          }
        };
      
    }
    
    
    
export function codesDetailsCtrl ($scope, $rootScope,uiGridConstants,CodesService) {
    
       $scope.CTRLNAME='codesDetailsCtrl';
        //Get data from server
        var colDefs = [
         { name: 'id', displayName: 'ID', aggregationType: uiGridConstants.aggregationTypes.max,type: 'number', width:'5%',enableHiding: false, visible: true },
         { name: 'CodeName', displayName: 'Code name'},
         { name: 'Description', displayName: 'Code description' },
         { name: 'Status', displayName: 'Status'},
         { name: 'Lang', displayName: 'Language',cellFilter:"getCodeByName: 'languages'"},
         ];
         
     $scope.paginationOptions = {
        pageNumber: 1,
        pageSize: 10,
        sort: null
    };
    
    $scope.gridOptions = {
        columnDefs: colDefs,
      enableGridMenu: true,
        showGridFooter: true,
    showColumnFooter: true,
        enableRowSelection: true,
       multiSelect: false,
       enableHorizontalScrollbar : 0, 
      enableVerticalScrollbar : 2,
       enableFiltering: true,
       enableRowHeaderSelection: true,
       enableColumnResizing: true,
     /* 
        paginationPageSizes: [5, 10, 20, 50, 100],
         paginationPageSize: 5,
       enableSelectAll: false,
       
      
       
       enableCellEdit: false*/
      
   }
   

    $scope.gridOptions.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope,function(row){
            //debugger
            $scope.codeSelected=row.isSelected
            });
    
     }
    
    $scope.getListOfCodes = function(){
      
      var lang=$rootScope.globals.currentUser.lang;
      CodesService.CodesGet(lang)
            .then(function(data) { 
                     $scope.data = data.data;
                     $scope.gridOptions.data = $scope.data;
          },function(error){
                    MessagingService.addMessage(error.msg,'error');
              });
       
    }
    
    getListOfCodes();
  
    
     
    }


export function codesNewCtrl ($scope, $rootScope,uiGridConstants,ProcessingService,MessagingService,CodesService) {
    
    $scope.CodeHead = {CodesHeadLang:[]};
    $scope.CTRLNAME='codesNewCtrl';
    $scope.activity.activityMessage = ProcessingService.setActivityMsg('Processing request');
    
    /*
    $scope.setDirectiveFn = function(directiveFn) {
        //debugger
        $scope.directiveFn = directiveFn;
    };*/
    
    var lang=$rootScope.globals.currentUser.lang;
     
        
        
     CodesService.getCodesByName('_CODE_TYPE_LIST_',lang).then(function (data) {
                                $scope.codeTypeList = data.data;
                                $scope.codeTypeListDownloaded=true;
                             });
     
      CodesService.getCodesByName('codestatus',lang).then(function (data) {
                                $scope.codeStatusList = data.data;
                                $scope.codeStatusListDownloaded=true;
                             });
      
    $scope.saveNewCode = function (isValid) {

         //debugger

          if (isValid) {
    
            $scope.CodeHead.Version=1;
              
               CodesService.CodesPost($scope.CodeHead).then(function(data) {
                                
                                   MessagingService.addMessage(data.data.msg,'success');
                                   $scope.CodeHead.id=data.data.data;
                                   
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });                             
           
            
          }
        };
        
       
    }


export function codesEditCtrl ($scope, $rootScope,uiGridConstants,ProcessingService,MessagingService,CodesService,$stateParams) {
    
     $scope.CTRLNAME='codesEditCtrl';
    
    var lang=$rootScope.globals.currentUser.lang;

    $scope.activity.activityMessage = ProcessingService.setActivityMsg('Processing request');
     
    CodesService.getCodesByName('languages',lang).then(function (data) {
                                $scope.codeLanguageList = data.data;
                             });
     CodesService.getCodesByName('_CODE_TYPE_LIST_',lang).then(function (data) {
                                $scope.codeTypeList = data.data;
                             });
     
      CodesService.getCodesByName('codestatus',lang).then(function (data) {
                                $scope.codeStatusList = data.data;
                             });
    $scope.codeId = $stateParams.id;
    
     $scope.getCode = function (codeid) {
      
       
       CodesService.CodesGetById($scope.codeId,lang).then(function(data) {
                                
                    $scope.CodeHead = data.data[0];
         },function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });                             
           
     }
     
     $scope.getCode($scope.codeId);
     
      $scope.clear = function(event, select)
      
      {
        select.selected = null;
        
      }
    $scope.saveCode = function (isValid) {

         //debugger

          if (isValid) {
    
            $scope.activity.activityMessage = ProcessingService.setActivityMsg('Saving code '+$scope.CodeHead.CodesHeadLang[0].CodeName);
            //$scope.CodeHead.Version=1;
              
               CodesService.CodesPost($scope.CodeHead).then(function(data) {
                                
                                   MessagingService.addMessage(data.data,'success');
                                   
},function(error){
                                
                                  MessagingService.addMessage(error.msg,'error');
                            });                             
           
            
          }
        };
        
       
    }
    



export function userDetailsCtrl ($scope, $rootScope, $location, $timeout,AuthenticationService,ProcessingService,MessagingService,CodesService) {
    
    $scope.CTRLNAME='userDetailsCtrl';
    var lang=$rootScope.globals.currentUser.lang;    
    //$scope.strongPassRegex = "^(?=.*[A-Z].*[A-Z])(?=.*[!@#$&()=\|+/-?:.;,_//*])(?=.*[0-9].*[0-9])(?=.*[a-z].*[a-z]).{8,}$";
    
    $scope.strongPassRegex ="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#%&<>;:,_/'\"\$\\s\*\(\)\=\?\+\^\.\-]).{8,}$";
    // var special =\\\\ \| \(\)\=\?\+\^\-\.
    
   /* CodesService.getCodesByName('languages',lang).then(function (data) {
                                $scope.codeLanguageList = data.data;
                                
    });
      
       $scope.getUser = function () {
 
      };
      */   

    $scope.pageLabels = userDefinitionJson[lang]

    $scope.validate_pass2 = function (form) {
        
        if (form.pass1.$valid && $scope.pass1!=$scope.pass2) form.pass2.$setValidity('pass2',false);
        else if (form.pass1.$valid && $scope.pass1==$scope.pass2) form.pass2.$setValidity('pass2',true);
        else form.pass2.$setValidity('pass2',false);
    };
      
    $scope.user = $rootScope.globals.currentUser;

    $scope.change_password = function() {
        
        AuthenticationService.ChangePasswordWithAuth($scope.currentPass,$scope.pass1).then(function(response) {
            
           
                if (response.success)
                {
                    MessagingService.addMessage(response.data.msg,'success');
                    AuthenticationService.UpdateAuthToken(response.data.token);
                }else
                {
                     MessagingService.addMessage(response.message,'error');
                }
            },function(error){
                MessagingService.addMessage(error.message,'error');

            })

    };
       
      
}
export function usersDefinitionCtrl ($scope, $rootScope, $timeout,UserService,MessagingService,CodesService) {
    
    $scope.CTRLNAME='usersDefinitionCtrl';
    var lang=$rootScope.globals.currentUser.lang;    
    
    //$scope.newUser=true;            
    
    $scope.pageLabels = userDefinitionJson[lang]

    CodesService.getCodesByName('languages',lang).then(function (data) {
                                $scope.codeLanguageList = data.data;
                                
    });
        
        $scope.getGroups = function () {
          UserService.getUsersGroups('groups').then(function(data){
          $scope.groupList = data.data;
          $scope.groupsDownloaded = true;
				});

        };
        $scope.getGroups();
        $scope.editUser = function (user,index) {  
            $scope.selectedUser = angular.copy(user); //MIGOR - dodao CLONE za test
            $scope.newUser=false;
            $scope.showUserDetails=true;
            $scope.selectedIndex=index;
        };
        
        $scope.showUserDetailsFn = function (show) {  
           
            $scope.showUserDetails=show;
            $scope.newUser = null;
            $scope.selectedUser = null;
        };
        
       $scope.getUsersExtended = function () {
          UserService.getUsersExtended().then(function(data){
          $scope.users = data.data;
          $scope.usersLoaded=true
				});

        };
				//$scope.getUsers();
        $scope.getUsersExtended();
      
       $scope.newUserFunc = function (form) {
          $scope.newUser=true;
          $scope.showUserDetails=true;
          $scope.selectedUser = {'UserId':null,'Lang':null};
          if (form!=undefined)
          {
            form.$setPristine();
            if (form.username!=undefined) form.username.$setUntouched();
            //form.password.$setUntouched();
            if (form.first_name!=undefined) form.first_name.$setUntouched();
            if (form.last_name!=undefined) form.last_name.$setUntouched();
            if (form.email!=undefined) form.email.$setUntouched();
            if (form.lang!=undefined) form.lang.$setUntouched();
          }
       };
       
        $scope.ResetPassword = function (user_id) {
          UserService.ResetPassword(user_id).then(function(data){
                $scope.selectedUser.resetPasswordRef = data.data.digest;
                MessagingService.addMessage(data.data.msg,'success');
            },
            function(error){
                    MessagingService.addMessage(error.data.msg,'error');

            });

        };

          $scope.CopyResetPasswordLink = function () {
         
            
            let pass_reset_link = window.location.origin+"/changepass/"+$scope.selectedUser.resetPasswordRef

             $scope.copyPassResetLink = true;

                    navigator.clipboard.writeText(pass_reset_link).then( 
                        () => {
                            $timeout(function(){$scope.copyPassResetLink = false;},1000);
                        },
                        () => {    /* clipboard write failed */
                            MessagingService.addMessage('Copy to clipboard failed','error');
                            $timeout(function(){$scope.copyPassResetLink = false;},1000);
                    });
                                    

        };

      $scope.saveUser = function (isValid) {
          
           if (isValid) {

            
             UserService.SaveUser($scope.selectedUser).then(function(data) {
                                
                                   
                                   MessagingService.addMessage(data.data.msg,'success');
                                   $scope.selectedUser=data.data.user;
                                   if ($scope.newUser)
                                   {
                                    
                                    $scope.users.push($scope.selectedUser);
                                    $scope.newUser=false;
                                   }else{
                                    $scope.users[$scope.selectedIndex] = $scope.selectedUser;
                                    
                                   }
                                  
                                   //find and update user
                                   
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.data.msg,'error');
                            });                
             
            } 
        
        
      };
       
      
    }
export function groupsCtrl ($scope, $rootScope, $timeout,UserService,MessagingService,CodesService) {
    
     var lang=$rootScope.globals.currentUser.lang; 
     $scope.CTRLNAME='groupsCtrl';
   	   
        
    CodesService.getCodesByName('languages',lang).then(function (data) {
                                $scope.codeLanguageList = data.data;
                                
    });
        
        $scope.editGroup = function (group,index) {  
          $scope.selectedGroup = angular.copy(group);
          $scope.isShowNewGroup=false;
		$scope.selectedIndex=index;
        		};

         $scope.deselectGroup = function () {  
            $scope.newGroup = null;
            $scope.selectedGroup = null;
            $scope.isShowNewGroup=false;

		};
        
        $scope.showNewGroup = function (form) {
            
            if (form!=undefined)
            {
                form.$setPristine();
                if (form.groupName!=undefined) form.groupName.$setUntouched();
            }
           $scope.isShowNewGroup=true;
           $scope.selectedGroup=$scope.newGroup;
       }
        
       $scope.getUsers = function () {
          UserService.getUsersGroups('users').then(function(data){
          $scope.userList = data.data;
				});
        };
        
       $scope.getGroupsExtended = function () {
          UserService.getGroupsExtended().then(function(data){
          $scope.groups = data.data;
				});

        };
        

       
       
        //Execute this
        $scope.getUsers();
        $scope.getGroupsExtended(); 

    
    
      $scope.saveGroup = function (selectedGroup,isValid) {
          
           if (isValid) {
            
             UserService.SaveGroup(selectedGroup).then(function(data) {
                                
                                   
                                   MessagingService.addMessage(data.data.msg,'success');
                                   $scope.selectedGroup=data.data.group;
                                   if (selectedGroup.id == null)
                                   {
                                    
                                    $scope.groups.push($scope.selectedGroup);
                                   }
                                   else
                                   {
                                     $scope.groups[$scope.selectedIndex] = $scope.selectedGroup;
                                   }
                                   $scope.deselectGroup();
                                 
                              },function(error){
                                
                                  MessagingService.addMessage(error.data.msg,'error');
                            });                
             
            } 
        
        
      };
       
      
    }
export function organisationCtrl ($scope, $rootScope, $location, $timeout,SettingsService,CodesService,MessagingService) {
    
  
    $scope.CTRLNAME='organisationCtrl';
    $scope.organisationId = $rootScope.globals.currentUser.organisation.id;
    var lang = $rootScope.globals.currentUser.langObj.id;
    //get all codes
      
    //status
   /* CodesService.getCodesByName('codestatus',lang).then(function (data) {
        $scope.statusList = data.data;
    });
     */ 
    $scope.getOrganisationInfo = function () {
        
        SettingsService.getOrganisationInfo($scope.organisationId,lang).then(function(data) {
                                
            $scope.organisation = data.data;
             //country
            CodesService.getCodesByName('countries',lang).then(function (data) {
                $scope.countryList = data.data;
                $scope.countryList.unshift({id:null});
            });
      
            //industry
            CodesService.getCodesByName('industry',lang).then(function (data) {
                $scope.industryList = data.data;
                $scope.industryList.unshift({id:null});
            });
                                  
        },function(error){
                                
            MessagingService.addMessage(error.data.msg,'error');
        });
    
    };
    
    $scope.GenerateApiKeys = function () {
        let api_id;
        if ($scope.organisation.ApiUser == null) api_id = $scope.organisation.ApiUser;
        else api_id = $scope.organisation.ApiUser.username;
        SettingsService.GenerateApiKeys(api_id).then(function(data) {
                                
            $scope.api_id = data.data.api_id;
            $scope.api_key = data.data.api_key;
            
            MessagingService.addMessage(data.data.msg,'success');
            
                                  
        },function(error){
                                
            MessagingService.addMessage(error.msg,'error');
        });
    
    };

     $scope.getOrganisationInfo();
     
    $scope.saveOrganisation = function (isValid) {
          
        if (isValid) {
             
      
            $scope.saveOrganisationExecute = function () {

            //  $scope.auction.currentUser = $rootScope.globals.currentUser;

            };
        }

    }
      
};