(function () {
 
 if (window._env == undefined)  {
      window._env =  {};
  }

  
  window._env.apiUrl = 'https://beta-api.katalyo.com/'; // API url
  window._env.baseUrl = '/';                            // Base url
  window._env.enableDebug = true;                       // Whether or not to enable debug mode
  
  
  // override api url if local development - DO NOT REMOVE!!!
  if (window.location.hostname == 'localhost')  window._env.apiUrl = 'http://localhost:8000/';
  
}());