/**
 * Katalyo http (kt-http.js) this is a wrapper around axios to abstract it
 * @license MIT 
 * @build: 2020-29-03
 * @version 0.1
 * https://gitlab.com/katalyo/kt-http
 * Copyright (c) 2020 Katalyo
**/

		'use strict';
        
		import axios from 'axios'
		
        /**
         * Class Katalyohttp is a main class for executing REST API calls
         * @since 0.1
         * @method send is used to send calls to the server
         * @method createSubject is used to create new instance of subject class  
         */
        
        class KatalyoHttp {
            
            constructor(_server,_http_method,_endpoint) {
                this.server 		= _server;
				this.httpMethod		= _http_method;
				this.endpoint 		= _endpoint;
				this.params 		= '';
				this.http_headers 	= '';
				this.axios = axios;
				this.axios.defaults.xsrfCookieName = 'csrftoken';
				this.axios.defaults.xsrfHeaderName = "X-CSRFTOKEN";
				//this.axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
            }
            
			/**
            * fn for setting Authorisation header
            *  @since 0.1
            * @authdata authentication token
            */
			
			setAuthHeader(authdata)
			{
				this.axios.defaults.headers.common['Authorization'] = authdata; 
			}
			/**
            * fn for sending a rest api call to he server
            *  @since 0.1
            * @http_method contains parameters to send 
            * @endpoint api endpoint address
            * @params contains parameters to send 
            */
            
			send(params)
            {
				

				var self = this;
				return 	axios({
							method: self.httpMethod,
							url: self.server+self.endpoint,
							data: params
						})
						.then(response => {
							return {'response':response,'status':response.status};
						})
						.catch(error => {
							let err_msg,err_status='n/a',err_headers;
							if (error.response) {
								// The request was made and the server responded with a status code
								// that falls out of the range of 2xx
								err_msg = error.response.data;
								err_status = error.response.status;
								err_headers = error.response.headers;
								
								throw {'msg':err_msg,'status':err_status,'headers':err_headers};
							} else if (error.request) {
								// The request was made but no response was received
								// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
								// http.ClientRequest in node.js
								err_msg = error;
								err_status=error.request.status;
								throw {'msg':err_msg,'msg_full':err_msg.stack,'request':error.request,'status':err_status};
							} else {
								// Something happened in setting up the request that triggered an Error
								throw {'msg':error.message,'status':err_status};
							}
							
						});
					
					
				
				
            }
			send_with_params(server,http_method,endpoint)
            {
				
				return	axios({
							method: http_method,
							url: server+endpoint
							//data: params
						})
						.then((response) => {
							return response;
						}, (error) => {
							return error;
						});
					
					
				
				
            }
           
        }
        
        /**
        * fn for initializing KatalyoHttp returns KatalyoHttp object
		*  @since 0.1
        */
		var http;
        var KatalyoHttpManager = (function(server,method,endpoint)
        {
          if(!http) http = new KatalyoHttp(server,method,endpoint);
          
          return http;
        });
        export default KatalyoHttpManager;