import { createApp } from "vue";
import { createPinia } from "pinia";

import Login from "./components/Login.vue";
import router from "./router";

const app = createApp(Login);

app.use(createPinia());
app.use(router);

app.mount("#app");
