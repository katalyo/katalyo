from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from katalyonode.models.models import ResourceDef,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang
from katalyonode.models.models import TaskDef,TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskInitiationInstance,FilesTable,FilesToResource,PresentationElementsTasks,TaskCodesActions,TaskExecutionInstances
from katalyonode.serializers.serializersresources import ResourceDefByLangSerializer,ResourceDefLangSerializer,ResourceDefSerializer
from katalyonode.serializers.serializerssettings import CodesDetailSerializer,CodesDetailLangSerializer
from katalyonode.models.models import TaskOutcomesDetail,TaskOutcomesDetailLang,TaskOutcomesHead,TaskOutcomesHeadLang,TaskAssignments,TaskParameters

from katalyonode.serializers.serializers import UserSerializer,GroupSerializer,UserSimpleSerializer
from katalyonode.serializers.serializerssettings import GetCodeObjectByName
import pdb
from django.utils import timezone
import json

def ProcessPresentationElementsTasks(form,resource_id,relatedResource,parent_id):
        
    for widget in form:
      #process item
      ProcessProcessPresentationElementsTasksItem([widget],resource_id,relatedResource,parent_id)
      if 'layout' in widget:
        ProcessPresentationElementsTasks(widget['layout'],resource_id,relatedResource,parent_id)
      else:
        ProcessProcessPresentationElementsTasksItem([widget],resource_id,relatedResource,parent_id)
        
def ProcessProcessPresentationElementsTasksItem(widgetLayout,resource_id,relatedResource,parent_id):
    
    #pdb.set_trace()
    for resourceLayout in widgetLayout:
            if 'showField' in resourceLayout:
              showField =  resourceLayout['showField']
            else:
              showField=True;
            if 'useDefault' in resourceLayout:
              useDefault =  resourceLayout['useDefault']
            else:
              useDefault=False;
            if 'useSystemDate' in resourceLayout:
              useSystemDate =  resourceLayout['useSystemDate']
            else:
              useSystemDate=False;
            if showField or useDefault:
              useField = True
            else:
              useField=False
            if 'readOnly' in resourceLayout:
              readOnly =  resourceLayout['readOnly']
            else:
              readOnly=False;
            if 'noUpdate' in resourceLayout:
              noUpdate =  resourceLayout['noUpdate']
            else:
              noUpdate=False;
            if 'updateWithDefault' in resourceLayout:
              updateWithDefault =  resourceLayout['updateWithDefault']
            else:
              updateWithDefault=False;
            if 'sItemValue' in resourceLayout:
              sItemValue =  resourceLayout['sItemValue']
            else:
              sItemValue='';
            if 'required' in resourceLayout:
              required =  resourceLayout['required']
            else:
              required=False;
           
            #pdb.set_trace()
            extendedProps,created = PresentationElementsTasks.objects.update_or_create(TaskDefId=resource_id,ResourceDefId_id=relatedResource ,PresentationElementsId_id = resourceLayout['presentationId'] ,ParentElementId_id = parent_id,
                                        defaults={'Required' : required, 'ShowField':showField,'ReadOnly':readOnly,'UseField':useField,'UseDefault':useDefault,'UseSystemDate':useSystemDate,
                                        'DefaultValue':sItemValue,'NoUpdate':noUpdate,'UpdateWithDefault':updateWithDefault})

class SaveTaskParametersSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskDef
        fields = '__all__'
      
    def create(self, validated_data):
      
        #pdb.set_trace()
        request = self.context['request']
        kwargs_var = request.parser_context['kwargs']
        if 'resourceId' in kwargs_var:
           resource_id=int(kwargs_var['resourceId'])
        else:
           resource_id = None
           
        if validated_data['DefaultOutcomeId'] ==0:
          defaultOutcomeId = None
        else:
          defaultOutcomeId = validated_data['DefaultOutcomeId']
          
        if validated_data['TaskOutcomeHeadId'] == 0:
          taskOutcomeHeadId = None
        else:
          taskOutcomeHeadId = validated_data['TaskOutcomeHeadId']
        #pdb.set_trace()  
        taskparameters,created = TaskDef.objects.update_or_create(id=validated_data['id'],defaults={'ResourceDefId' : resource_id,'Name' : validated_data['TaskType'],
                                'MaxActualOwners' : validated_data['MaxActualOwners'],'SelfAssigned'  : validated_data['SelfAssigned'],'AutoStart'  : validated_data['AutoStart'],
                                'AutoClaim'  : validated_data['AutoClaim'],'AutoInitiate'  : validated_data['AutoInitiate'],'AutoExecute'  : validated_data['AutoExecute'],'CanDelegate'  : validated_data['CanDelegate'],'CanHold'  : validated_data['CanHold'],
                                'CanRelease'  : validated_data['CanRelease'],'CreatedBy'  : request.user,'ChangedBy'  : request.user,'TaskOutcomeHeadId' : taskOutcomeHeadId,
                                'DefaultOutcomeId' : defaultOutcomeId,'CreatedDateTime'  : timezone.now,'ChangedDateTime'  : timezone.now(),'VersionFrom'  : 1,'VersionTo'  : 1})
        #pdb.set_trace()
        return taskparameters



class TaskOutcomeDefinitionSerializer(serializers.Serializer):
    
    id = serializers.IntegerField(allow_null = True)
    Name = serializers.CharField(max_length=255)
    OutcomeName = serializers.CharField(max_length=255)
    Lang = serializers.CharField(max_length=255)
    Active = serializers.NullBooleanField()
    #Status = serializers.DictField()
    #CodesDetailLang = CodesDetailLangSerializer(many=True)
    
    class Meta:
        model = TaskOutcomesHead
        #fields = ('id','CodesHeadId','Name','Status','codesdetail_set','Lang','Value','CreatedBy','CreatedDateTime','ChangedByCodesHeadId', = 'ChangedDateTime')
        fields = ('id','OutcomeName','Active','Name','Lang')
         #exclude = ('id','CodesDetailId')
     
    def create(self, validated_data):
        # Create the Codes instance
        user = self.context['user']
        #pdb.set_trace()        
        outcome,outcomeCreated =  TaskOutcomesHead.objects.get_or_create(id=validated_data['id'], defaults={'Active':validated_data['Active'],'OutcomeName':validated_data['OutcomeName'],
                                                  'CreatedBy': user, 'CreatedDateTime':timezone.now()})
        #pdb.set_trace()
        if outcomeCreated:
          validated_data['id'] = outcome.id
          lang_object = GetCodeObjectByName('languages',validated_data['Lang'],0)
          #pdb.set_trace()
        
          outcomeLang = TaskOutcomesHeadLang.objects.create(Lang=lang_object,Name=validated_data['Name'],TaskOutcomeHeadId=outcome,CreatedBy= user,CreatedDateTime=timezone.now())
        elif outcome and not outcomeCreated:
          outcome.Active = validated_data['Active']
          outcome.OutcomeName = validated_data['OutcomeName']
          outcome.ChangedBy= user
          outcome.ChangedDateTime=timezone.now()
          
          outcomeLangs = TaskOutcomesHeadLang.objects.filter(Lang__contains=validated_data['Lang'],TaskOutcomeHeadId=outcome)
          if outcomeLangs.count()>0:
            for outcomeLang in outcomeLangs:
              if outcomeLang is not None:
                #pdb.set_trace()
                outcomeLang.Name=validated_data['Name']
                outcomeLang.ChangedBy= user
                outcomeLang.ChangedDateTime=timezone.now()
                outcomeLang.save()  
                outcome.save()
            #pdb.set_trace()
            
            validated_data['id'] = outcome.id
            #return code
          else:
            raise ValueError('Outcome language record for outcome %s not found',str(code.id))
        else:
          raise ValueError('Outcome record %s not found',str(validated_data['id']))
        
        #pdb.set_trace()
        return validated_data
      
class CodesActionsSerializer(serializers.Serializer):
    
    toaId = serializers.IntegerField(required=False)
    id1 = serializers.IntegerField()
    name1 = serializers.CharField(max_length=255)
    value1 = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    id2 = serializers.IntegerField(required=False,allow_null=True)
    name2 = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    value2 = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    taskId = serializers.IntegerField(required=False,allow_null=True)
    type = serializers.IntegerField()
    actionParams = serializers.DictField(required=False,allow_null=True)
   
    
class TaskCodesActionsSerializer(serializers.Serializer):
    id1 = serializers.IntegerField(allow_null=True)
    name1 = serializers.CharField(max_length=255)
    value1 = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    id2 = serializers.IntegerField(required=False,allow_null=True)
    name2 = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    value2 = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    btnClass = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    icon = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    btnName = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    outcomeName = serializers.CharField(max_length=255,required=False,allow_null=True,allow_blank=True)
    
    actions = CodesActionsSerializer(many=True)
    
    class Meta:
        model = TaskCodesActions     
    
    def create(self, validated_data):
        
        #pdb.set_trace()
        task_id=int(self.context['task_id'])
        user = self.context['user']
        code_name1 = self.context['code_name1']
        code_name2 = self.context['code_name2']
        if 'lang' in self.context:
          lang = self.context['lang']
          
        if 'outcome' in self.context:
          outcome = self.context['outcome']
          TaskOutcomesHeadId = self.context['TaskOutcomesHeadId']
        else:
          outcome=None
          TaskOutcomesHeadId = None
          
        vData = validated_data
        actionsArr = validated_data['actions']
        
        if 'id1' in vData:
          id1 = vData['id1']
         
        else:
          id1=None
          
        if 'id2' in vData:
          id2 = vData['id2']
         
        else:
          id2=None
        
        
        
        if outcome is not None:
          outcomeId = vData['id1']
          id1=None
          #now add or update all outcome definitions  
          
          #if outcomeId is not None:
         
          tods =  TaskOutcomesDetail.objects.filter(id=outcomeId,TaskOutcomeHeadId=TaskOutcomesHeadId)
          
          if len(tods)>0:
            for to in tods:
              to.Active = True
              to.Name = vData['outcomeName']
              to.BtnClass = vData['btnClass']
              to.Icon = vData['icon']
              #to.TaskOutcomeHeadId_id  =TaskOutcomesHeadId
              to.ChangedBy= user
              to.ChangedDateTime=timezone.now()
            
            toLang = TaskOutcomesDetailLang.objects.get(Lang__contains=lang,TaskOutcomeDetailId=to)
            #pdb.set_trace()
            if toLang is not None:
              #pdb.set_trace()
              toLang.DisplayName=vData['name1']
              toLang.BtnName=vData['btnName']
              toLang.ChangedBy= user
              toLang.ChangedDateTime=timezone.now()
              toLang.save()  
              to.save()
              #pdb.set_trace()
              vData['id1'] = to.id
            #return code
            else:
              raise ValueError('Outcomes language record for outcome %s not found',str(to.id)) 
          else:
            to,toCreated =  TaskOutcomesDetail.objects.get_or_create(id=None,
                                      defaults={'Active':True,'Name':vData['outcomeName'],'Icon':vData['icon'],'BtnClass':vData['btnClass'],
                                      'TaskOutcomeHeadId_id':TaskOutcomesHeadId,'CreatedBy': user,'CreatedDateTime':timezone.now()})
          
            if toCreated:
              vData['id1'] = to.id
              outcomeId = vData['id1']
              toLang = TaskOutcomesDetailLang.objects.create(Lang=lang,DisplayName=vData['name1'],BtnName=vData['btnName'],CreatedBy= user,
                                                  CreatedDateTime=timezone.now(),TaskOutcomeDetailId=to)
       
          
        else:
          outcomeId=None
        
       
        #TaskCodesActions.objects.filter(TaskDefId_id=task_id,CodeName1=code_name1,CodeId1_id=id1,CodeId2_id=id2).delete()  
        for action in actionsArr:
          
          if 'toaId' in action:
            toa_id=action['toaId']
          else:
            toa_id = None
          
          if 'actionParams' in action:
            action_params=action['actionParams']
          else:
            action_params = None
          
          if 'taskId' in action:
            task_init_id = action['taskId']
          else:
            task_init_id = None
            
          taskcodesactions,created = TaskCodesActions.objects.get_or_create(id=toa_id,defaults={'TaskDefId_id' : task_id,'CodeId1_id' : id1,
                                'CodeName1':code_name1,'CodeId2_id' : id2,'CodeName2':code_name2,'OutcomeId_id':outcomeId,'OutcomeName':outcome,
                                'ActionName':action['name1'],'ActionId' : action['id1'],'ActionType'  : action['type'],'InitiateTaskId_id'  : task_init_id,
                                'CreatedBy'  : user,'CreatedDateTime'  : timezone.now(),'ActionParams':action_params})
          
         
          if not created:
            taskcodesactions.TaskDefId_id = task_id
            taskcodesactions.CodeName1 = code_name1
            taskcodesactions.CodeName2 = code_name2
            taskcodesactions.CodeId1_id = id1
            taskcodesactions.CodeId2_id = id2
            taskcodesactions.ActionId = action['id1']
            taskcodesactions.ActionName = action['name1']
            taskcodesactions.ActionType = action['type']
            taskcodesactions.InitiateTaskId_id = action['taskId']
            taskcodesactions.ActionParams = action_params
            taskcodesactions.ChangedBy = user
            taskcodesactions.ChangedDateTime = timezone.now()
            taskcodesactions.save()
          else:
            action['toaId'] = taskcodesactions.id
       
        #pdb.set_trace()
        return vData
      
class TaskParametersSerializer(serializers.ModelSerializer):
      TaskTypeObject = CodesDetailSerializer(read_only=True,required=False,source='TaskType')
      #Parameters=serializers.JSONField(required=False,allow_null=True)
      Parameters = serializers.DictField(required=False,allow_null=True)
      ParametersObject = serializers.DictField(required=False,allow_null=True,source='Parameters')
      class Meta:
        model = TaskParameters
        fields = '__all__'
        read_only_fields=('TaskTypeObject','ParametersObject')
        
class TaskDefByResourceIdSerializer(serializers.ModelSerializer):
      #TaskType = CodesDetailSerializer()      
      class Meta:
        model = TaskDef
        fields = '__all__'
        #exclude = 'id'
   
class TaskAssignmentDefUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskAssignmentDefUser
        fields = '__all__'

    def create(self, validated_data):
        
        user=self.context['user']
        validated_data['CreatedDateTime'] = timezone.now()
        validated_data['CreatedBy'] = user
        assignU,created = TaskAssignmentDefUser.objects.get_or_create(**validated_data)
        return assignU
      
class TaskAssignmentDefGroupSerializer(serializers.ModelSerializer):

    class Meta:
        model = TaskAssignmentDefGroup
        fields = '__all__'     
    
    def create(self, validated_data):
        
     
        #pdb.set_trace()
        user=self.context['user']
        validated_data['CreatedDateTime'] = timezone.now()
        validated_data['CreatedBy'] = user
        #pdb.set_trace()
        assignG,created = TaskAssignmentDefGroup.objects.get_or_create(**validated_data)
        return assignG
      
class AssignmentDefUserSerializer(serializers.ModelSerializer):
    
    AssigneeId = UserSerializer()
    
    
    class Meta:
        model = TaskAssignmentDefUser
        fields = ('id','AssignType','SubAssignType','AssignGroup','TaskDefId','AssigneeId')#,'AssigneeId_set')
    
    
      
class AssignmentDefGroupSerializer(serializers.ModelSerializer):
    
    AssigneeId = GroupSerializer()
    
    class Meta:
        model = TaskAssignmentDefGroup
        fields = ('id','AssignType','SubAssignType','AssignGroup','TaskDefId','AssigneeId')#'__all__'
    

    
class TaskInitiationInstanceSerializer (serializers.ModelSerializer):
  
  #taskdefid_set = ResourceDefSerializer()
  #resource_def = ResourceDefSerializer(source='TaskDefId')
  #initiator = UserSerializer(source='InitiatorId')
  class Meta:
        model = TaskInitiationInstance#TaskInitiationInstance#
        fields ='__all__'#('id','resourcedeflang_set')
        #fields = ('id','Status','initiator')
        depth = 1
        
class TaskExecutionInstancesSerializer (serializers.ModelSerializer):
  
  #taskdefid_set = ResourceDefSerializer()
  #resource_def = ResourceDefSerializer(source='TaskDefId')
  #initiator = UserSerializer(source='InitiatorId')
  Status = CodesDetailSerializer()
  class Meta:
        model = TaskExecutionInstances#TaskInitiationInstance#
        fields ='__all__'#('id','resourcedeflang_set')
        #fields = ('id','Status','initiator')
        depth = 1



class TaskOutcomesDetailLangSerializer(serializers.ModelSerializer):
    Lang = serializers.DictField()
    class Meta:
      model = TaskOutcomesDetailLang
      fields = '__all__'

class TaskOutcomesDetailSerializer(serializers.ModelSerializer):
    
    TaskOutcomesDetailLang_taskoutcomedetailid = TaskOutcomesDetailLangSerializer(many=True)
    
    class Meta:
        model = TaskOutcomesDetail
       #fields = ('id','CodesDetailId','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')
        fields = ('id','TaskOutcomeHeadId','Name','Active','TaskOutcomesDetailLang_taskoutcomedetailid')
        #fields = ('id','CodesHeadId','Name','Status','codesdetaillang_id','codesdetaillang_set')


class InitiatedTasksSerializer (serializers.ModelSerializer):
  
  Resourcedef= ResourceDefByLangSerializer(source='TaskDefId')
  InitiatorId= UserSerializer()
  Status= CodesDetailSerializer()
  CreatedBy= UserSerializer()
  
  class Meta:
        model = TaskInitiationInstance
        #fields ='__all__'#('id','resourcedeflang_set')
        fields = ('id','Status','CreatedDateTime','CreatedBy','InitiatorId','Resourcedef')
        #depth = 2
        
class TaskAssignmentsSerializer (serializers.ModelSerializer):
    
    TaskInitiationId = InitiatedTasksSerializer()
    class Meta:
      model = TaskAssignments
      fields =  '__all__'
      depth = 1
      
class ClaimedTasksSerializer (serializers.ModelSerializer):
  
  #resourcedef= ResourceDefByLangSerializer(source='TaskDefId')
  resourcedef= ResourceDefLangSerializer(source='TaskDefId.resourcedeflang_set',many=True)
  #resourcedef= ResourceDefSerializer(source='TaskDefId')
  actualOwner= UserSimpleSerializer(source='ActualOwner')
  createdBy= UserSimpleSerializer(source='CreatedBy')
  status= CodesDetailLangSerializer(source='Status.CodesDetailLang',many=True)
  #outcome= TaskOutcomesDetailLangSerializer(source='TaskOutcomeCodeItemId.TaskOutcomesDetailLang_taskoutcomedetailid',many=True)
  #resourcedeflang_set = ResourceDefLangSerializer(many=True)
  
  class Meta:
        model = TaskExecutionInstances
        #model = ResourceDef
        #fields ='__all__'#('id','resourcedeflang_set')
        #fields =('id','taskexecutioninstances_set','resourcedeflang_set')#('id','resourcedeflang_set')
        fields = ('id','actualOwner','CreatedDateTime','createdBy','resourcedef','status')
        #fields = ('id','actualOwner','TaskOutcomeCodeItemId','CreatedDateTime','createdBy','TaskDefId','Status')
        #depth = 1
        
class InitiatedTasksSerializerv2 (serializers.ModelSerializer):
  
  taskinitiationinstance_set= TaskInitiationInstanceSerializer(many=True)
  resourcedeflang_set = ResourceDefLangSerializer(many=True)
  createdBy= UserSerializer(source='CreatedBy')
  
  class Meta:
        model = ResourceDef#TaskInitiationInstance#
        #fields ='__all__'#('id','resourcedeflang_set')
        fields = ('id','Status','CreatedDateTime','createdBy','resourcedeflang_set','taskinitiationinstance_set')
        #depth = 1
class FormWidgetSerializer(serializers.ModelSerializer):
   class Meta:
      model = ResourceDefLang
      fields = '__all__'
      depth=1