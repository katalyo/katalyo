from rest_framework import serializers
from katalyonode.models.models import CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,Menus,Pages,ResourceDefLang,ResourceDef,ResourceParams,Language
#from katalyonode.serializers.serializersresources import ResourceParamsSerializer
from katalyonode.serializers.serializers import UserSerializer,GroupSerializer
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
import pdb
from django.utils import timezone
import json
import sys

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'

class CodesHeadLangSerializer(serializers.ModelSerializer):
   Lang=LanguageSerializer(required=False)
   class Meta:
      model = CodesHeadLang
      fields = '__all__'
      #depth=1

      #fields = ('id','CodesHeadId','Lang','CodeName','Description','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','OnInsert')


class CodesSerializer(serializers.ModelSerializer):
    Status = serializers.DictField()
    CodeType = serializers.DictField()
    CodesHeadLang = CodesHeadLangSerializer(many=True)
    code_id = serializers.IntegerField(required=False,allow_null = True)
    class Meta:
        model = CodesHead
        #fields = ('id',,'Lang','CodeName','Description','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','OnInsert')
        fields = ('id','code_id','CodeType','CreatedBy','CodeHeadName','CreatedDateTime','ChangedBy','ChangedDateTime','UserGeneratedId','Status','Version','CodesHeadLang')
      
    def create(self,validated_data):
        # Create the Codes instance
        
        #try:  
          user = self.context['request'].user
          lang_data = validated_data.pop('CodesHeadLang')
          helper = ResourceHelperClass()
          org_id,language = helper.get_lang_org(user)
          code_name = ''
          if 'code_id' in validated_data:
            code_id = validated_data['code_id']
          else:
            code_id=None
                 
          if code_id is not None:
            code_head_name = CodesHead.objects.filter(CodeHeadName=validated_data['CodeHeadName']).exclude(id=code_id)
          else:
            code_head_name = CodesHead.objects.filter(CodeHeadName=validated_data['CodeHeadName'])
          
          if code_head_name.count()>0:
            raise ValueError('Code unique identifier already exists')
          
          org_id=self.context['request'].session.get('OrganisationId')
          
          code,created =  CodesHead.objects.get_or_create(id=code_id,defaults={'CodeType':validated_data['CodeType'],'CodeHeadName':validated_data['CodeHeadName'],
                                          'CreatedBy':user,'CreatedDateTime':timezone.now(), 'OrganisationId_id':org_id, 'UserGeneratedId':validated_data.get('UserGeneratedId',False),'Status':validated_data['Status'],'Version':validated_data['Version']})
          
          if not created and code is not None:
            code.CodeType = validated_data['CodeType']
            code.CodeHeadName = validated_data['CodeHeadName']
            code.Status = validated_data['Status']
            code.UserGeneratedId = validated_data['UserGeneratedId']
            code.ChangedDateTime = timezone.now()
            code.ChangedBy = user
            code.save()
          
          
          if code is not None:
            for lang in lang_data:
              if 'CodeName' in lang:
                code_name = lang['CodeName']
              else:
                raise AttributeError('CodeName is mandatory')
            
              
              codeLangItems= CodesHeadLang.objects.filter(CodesHeadId=code,Lang=language)
              
              if codeLangItems.count()>0:
                for codeLang in codeLangItems:
                
                  if codeLang is not None:
                    codeLang.CodeName = code_name
                    codeLang.Description = lang['Description']
                    codeLang.Lang = language
                    codeLang.CodesHeadId = code
                    codeLang.ChangedDateTime = timezone.now()
                    codeLang.ChangedBy = user
                    codeLang.save()
              else:
                  codeLang = CodesHeadLang.objects.create(CodesHeadId=code,Lang=language,CodeName = code_name,
                              Description= lang['Description'],CreatedBy=user,CreatedDateTime=timezone.now(),OnInsert=True)
                
              code.CodesHeadLang.add(codeLang)
          
          return {'data': code.id,'msg':'Code '+code_name+' saved successfuly','error':False}
        
        #except:
          
          #return {'data': None,'msg':"Error saving code "+code_name+" -->" +str(sys.exc_info()[0]),'error':True}
    
        
class GetCodesByLangSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    Status = serializers.DictField()
    CodeType = serializers.DictField()
    #CodeName = serializers.CharField()
    #Description = serializers.CharField()
    #Lang = serializers.DictField()
    Version = serializers.IntegerField()
    
    class Meta:
      model = CodesHead
      fields = ('id','CodeType','CreatedBy','CodeHeadName''CreatedDateTime','ChangedBy','ChangedDateTime','Status','Version')

class CodesHeadSimpleSerializer(serializers.ModelSerializer):
   Status = serializers.DictField()
   CodeType = serializers.DictField()
   CodesHeadLang = CodesHeadLangSerializer(many=True)
   class Meta:
      model = CodesHead
      fields = ('id','CodeType','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','Status','CodeHeadName','UserGeneratedId','Version','CodesHeadLang')
      
class CodesHeadSerializer(serializers.ModelSerializer):
   CodesHeadLang = CodesHeadLangSerializer()
   class Meta:
      model = CodesHead
      fields = ('id','CodeType','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','Status','Version','CodesHeadLang')
      #depth=1
   def create(self, validated_data):
        # Create the Codes instance
        user = self.context['request'].user
        lang_data = validated_data.pop('CodesHeadLang')
        
        code =  CodesDetail.objects.create(**validated_data)
        codeLang = CodesDetailLang.objects.create(Lang=lang_data[0]['Lang'],Value=lang_data[0]['Value'],CreatedBy=user,CreatedDateTime=timezone.now(), CodesDetailId=code)
        return code
   def update(self, instance,validated_data):
        
        user = self.context['request'].user
        #update CodeType and Status
        lang_data = validated_data.pop('CodesHeadLang')
        for lang in lang_data:
          langItem = CodesHeadLang.objects.get(id=lang.id)
          langItem.Name
          langItem.Description
          langItem.save()
        instance.CodeType = validated_data.get('CodeType', instance.CodeType)
        instance.Status = validated_data.get('Status', instance.Status)
        instance.ChangedBy = user
        instance.ChangeDateTime = timezone.now()
        instance.save()
        return instance
       
        return code
      
class CodesDetailHeadSerializer(serializers.ModelSerializer):
   #id = serializers.IntegerField()
   class Meta:
      model = CodesDetail
      fields = ('id','CodesHeadId','Name','Status')      
 
class CodesDetailLangSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    #Lang = serializers.DictField()
    class Meta:
      model = CodesDetailLang
      #fields = '__all__','codesdetaillang_id'
      fields = ('id','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')
      #depth = 1
      
   
class CodesDetailSerializer(serializers.ModelSerializer): # NE KORISTI SE -  "GetCodesDetailByIdLangSerializer" JE BRZI
    
    CodesDetailLang = CodesDetailLangSerializer(many=True)
    Value = serializers.CharField(source='CodesDetailLang.first.Value')
    Params = serializers.DictField(allow_null = True)

    class Meta:
        model = CodesDetail
       #fields = ('id','CodesDetailId','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')
        fields = ('id','CodesHeadId','Name','Active','Value','Params','CodesDetailLang')
        #fields = ('id','CodesHeadId','Name','Status','codesdetaillang_id','codesdetaillang_set')
        
    def create(self, validated_data):
        # Create the Codes instance
        user = self.context['request'].user
        lang_data = validated_data.pop('CodesDetailLang')
        
        code =  CodesDetail.objects.create(**validated_data)
        codeLang = CodesDetailLang.objects.create(Lang=lang_data[0]['Lang'],Value=lang_data[0]['Value'],CreatedBy=lang_data[0]['CreatedBy'], CodesDetailId=code)
      
        return code
   
      
    def update(self, instance, validated_data):
        # Get  CodesDetailId
           
        #update Value
        user = self.context['request'].user
        for langItem in validated_data['CodesDetailLang']:
          
          codesLangDetails = CodesDetailLang(id=langItem['id'],ChangedBy=user,
          Value=langItem['Value'])
          #codesLangDetails = instance.codesdetaillang_set
          
          #codesLangDetails['Value'] = langItem['Value']
          codesLangDetails.save()
        
        #update Name and Status
        instance.Name = validated_data.get('Name', instance.Name)
        instance.Status = validated_data.get('Status', instance.Status)
        instance.save()
        return instance

class GetCodesDetailLangSerializer(serializers.ModelSerializer):
    Lang=serializers.DictField()  
    class Meta:
        model = CodesDetailLang
        fields = '__all__'
        

        
class GetCodesDetailSerializer(serializers.ModelSerializer):
    
    #Active=serializers.NullBooleanField()
    CodesDetailLang = CodesDetailLangSerializer(many=True,read_only=True)
    Value = serializers.CharField(source='CodesDetailLang.first.Value')
    class Meta:
        model = CodesDetail
        fields = ('id','CodesHeadId','Name','Active','CodesDetailLang')
        # fields = ('id','CodesHeadId','Name','Active','CodesDetailLang')

class GetCodesDetailByIdLangSerializer(serializers.ModelSerializer):
    value = serializers.CharField(source='Name')
    active = serializers.NullBooleanField(source='Active')
    name = serializers.SerializerMethodField('name_field')
     
    class Meta:
        model = CodesDetail
        fields = ('id','value','name','active')
        
    def name_field(self,CodesDetailLocal):
        
      language=None
      
     
      name=''
      allcodes = CodesDetailLang.objects.filter(CodesDetailId_id=CodesDetailLocal.id)
      #pdb.set_trace() #GetCodesDetailByIdLangSerializer
      for gg in allcodes:
          if gg.CodesDetailId_id == CodesDetailLocal.id:
              name = gg.Value        
      
      return name

class AddUpdateCodesDetailSerializer(serializers.ModelSerializer):
      Status = serializers.DictField() 
      #CodesHeadId = CodesHeadSerializer()
      class Meta:
        model = CodesDetail
        fields = '__all__'
        #exclude = 'id'

class AddUpdateCodesDetailLangSerializer(serializers.ModelSerializer):
    
    CodesDetailId = AddUpdateCodesDetailSerializer()
    codesDetail_id = serializers.PrimaryKeyRelatedField(queryset=CodesDetail.objects.all(), required=False,source='CodesDetailId', write_only=True)
    Lang = serializers.DictField()
    #Status = serializers.DictField()
    class Meta:
        model = CodesDetailLang
        #fields = ('id','CodesHeadId','Name','Status','codesdetail_set','Lang','Value','CreatedBy','CreatedDateTime','ChangedByCodesHeadId', = 'ChangedDateTime')
        fields = ('id','codesDetail_id','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','CodesDetailId')
         #exclude = ('id','CodesDetailId')
     
    def create(self, validated_data):
        # Create the Codes instance
        code_data = validated_data.pop('CodesDetailId')
        user = self.context['request'].user
       
        #code_data['CodesHeadId_id'] = code_data['CodesHeadId'].id
        #serializer = AddUpdateCodesDetailSerializer(data=code_data)
        
        #serializer.is_valid(raise_exception=True)
        
        code =  CodesDetail.objects.create(Status=code_data['Status'],Name=code_data['Name'],CodesHeadId=code_data['CodesHeadId'])
        
        codeLang = CodesDetailLang.objects.create(Lang=validated_data['Lang'],Value=validated_data['Value'],CreatedBy= user,
                                                  CreatedDateTime=timezone.now(),CodesDetailId=code)
        return codeLang
    
    def update(self, instance, validated_data):
        # Get  CodesDetailId
        user = self.context['request'].user
        
        
        codeItem= validated_data.pop('CodesDetailId')
        #pdb.set_trace() #DBG #AddUpdateCodesDetailLangSerializer
        #update Value
        
        cd = CodesDetail.objects.get(id=instance.CodesDetailId_id)
        if cd:
          cd.Name=codeItem.get('Name',cd.Name)
          cd.Status=codeItem.get('Status',cd.Status)
          cd.save()
        
        instance.Value = validated_data.get('Value', instance.Value)
        instance.ChangedBy = user
        instance.ChangedDateTime = timezone.now()
        instance.CodesDetailId = cd
        instance.save()
        
        return instance

class SaveCodesDetailSerializer(serializers.Serializer):
    
    id = serializers.IntegerField(allow_null = True)
    Name = serializers.CharField(max_length=255)
    Value = serializers.CharField(max_length=255)
    Active = serializers.NullBooleanField()
    Params = serializers.DictField(allow_null = True,required=False, default=None)
    
    class Meta:
        model = CodesDetail
        fields = ('id','Name','Active','Params','CodesDetailLang')
        extra_kwargs = {
            'Params': {'validators': []},
        }
        
    def create(self, validated_data):
        # Create the Codes instance
        user = self.context['request'].user
        codeHeadId = self.context['codeHeadId']
        lang = self.context['lang']
        language = Language.objects.get(id=lang)
        #get lang dict
        lang_dict = GetStatusCodeByName(lang,'languages')
        #pdb.set_trace()
       
        code,codeCreated =  CodesDetail.objects.get_or_create(id=validated_data['id'], defaults={'Active':validated_data['Active'],'Name':validated_data['Name'],'Params':validated_data['Params'],'CodesHeadId_id':codeHeadId})
        
        if codeCreated:
          validated_data['id'] = code.id

          codeLang = CodesDetailLang.objects.create(Lang=language,Value=validated_data['Value'],CreatedBy= user,    CreatedDateTime=timezone.now(),CodesDetailId=code)
        elif code and not codeCreated:
          code.Active = validated_data['Active']
          code.Name = validated_data['Name']
          code.Params = validated_data['Params']
          code.CodesHeadId_id  =codeHeadId
          
          codeLang = CodesDetailLang.objects.get(Lang=language,CodesDetailId=code)
        
          if codeLang is not None:
            
            codeLang.Value=validated_data['Value']
            codeLang.ChangedBy= user
            codeLang.ChangedDateTime=timezone.now()
            codeLang.save()  
            code.save()
            
            
            validated_data['id'] = code.id
            #return code
          else:
            raise ValueError('Code language record for code %s not found',str(code.id))
        else:
          raise ValueError('Code record %s not found',str(validated_data['id']))
       
        return validated_data
class ListCodesGenericSerializer(serializers.Serializer):
    
    id = serializers.IntegerField()
    name = serializers.CharField(required=False)
    #active = serializers.NullBooleanField(required=False,source='Active')
    value = serializers.CharField(required=False)
    #CodesDetailLang = CodesDetailLangSerializer(many=True)

class ListCodesSerializer(serializers.ModelSerializer):
    
    id = serializers.IntegerField()
    #name = serializers.CharField(source='CodesDetailLang.first.Value')
    active = serializers.NullBooleanField(required=False,source='Active')
    value = serializers.CharField(required=False,source='Name')
    #CodesDetailLang = CodesDetailLangSerializer(many=True)
    
    class Meta:
        model = CodesDetail
       #fields = ('id','CodesDetailId','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')
        fields = ('id','active','value')
        #fields = ('id','CodesHeadId','Name','Status','codesdetaillang_id','codesdetaillang_set')

class AddMenuSerializer(serializers.ModelSerializer):
    
      class Meta:
        model = Menus
       #fields = ('id','CodesDetailId','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')
        fields = '__all__'
        #fields = ('id','CodesHeadId','Name','Status','codesdetaillang_id','codesdetaillang_set')

class GetMenuSerializer(serializers.ModelSerializer):
    
      class Meta:
        model = Pages
        fields = '__all__'
        depth = 1
        
class GetNavbarSerializer(serializers.ModelSerializer):
      Params = serializers.DictField()
      
      class Meta:
        model = Menus
        fields = '__all__'
        #extra_fields = ['params']
        depth = 1
        
      #def get_field_names(self, declared_fields, info):
        #expanded_fields = super(GetNavbarSerializer, self).get_field_names(declared_fields, info)

        #if getattr(self.Meta, 'extra_fields', None):
         #   return expanded_fields + self.Meta.extra_fields
        #else:
         #   return expanded_fields

        
        #extra_fields = ['params']

class  FilteredListSerializer(serializers.ListSerializer):
   
   def to_representation(self, data):
      
        #pdb.set_trace()  #FilteredListSerializer
        if 'lang' in self.context['view'].kwargs:
          lang = self.context['view'].kwargs['lang']
        else:
          lang=''
        
          
        data = data.filter(Lang=lang)
      
        return super(FilteredListSerializer, self).to_representation(data)
     
 
class ResourceDefLangFilteredSerializer(serializers.ModelSerializer):
   id = serializers.IntegerField()
   Lang = serializers.DictField()
   #Name = serializers.SerializerMethodField('name_field')
   #allDefLang =  ResourceDefLang.objects.all() #TODO 
   
   """
   def name_field(self,Res):
       
        lang = self.context['view'].kwargs['lang']  if 'lang' in self.context['view'].kwargs else None
     
        name=''
        for gg in ResourceDefLangFilteredSerializer.allDefLang:
            if gg.id == Res.id:
                name = gg.Name
        
        return name"""
  
   class Meta:
      #list_serializer_class = FilteredListSerializer
      model = ResourceDefLang
      fields = ('id','ResourceDefId', 'Lang','Name','Subject','Description')
    
 
class ResourceParamsSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()
    DefaultForm =  serializers.SerializerMethodField('convert_to_json_df')
    def convert_to_json_df(self,instance):
        if instance.DefaultForm is not None:
            return json.loads(instance.DefaultForm) 
        else:
            return None
    Parameters =  serializers.SerializerMethodField('convert_to_json_params')
    
    def convert_to_json_params(self,instance):
        if instance.Parameters is not None:
            return json.loads(instance.Parameters)
        else:
            return None
    name=serializers.CharField(source="ResourceName")
   
    class Meta:
        model = ResourceParams
        fields = ('id','name','ResourceCode','ResourceName','ResourceDescription','DefaultForm','Parameters','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')

class ResourceDefFilteredSerializer(serializers.ModelSerializer):
  
   resourcedeflang_set = ResourceDefLangFilteredSerializer(many=True)
   ResourceType = ResourceParamsSerializer()
   #ResourceGroup = ListCodesSerializer(required=False,allow_null=True)
   Status = ListCodesSerializer()
   
   class Meta:
      model = ResourceDef
      fields = ('id','Organisation','Status','Private','ResourceType','resourcedeflang_set')


class GetPagesSerializer(serializers.ModelSerializer):
      Params = serializers.DictField()
      PageType = GetCodesDetailByIdLangSerializer()
      #Menus = MenusSerializer(many=True)
      ResourceDefId = ResourceDefFilteredSerializer()
      Users = UserSerializer(many=True)
      Groups = GroupSerializer(many=True)
      #name = serializers.CharField(source='Name')
      class Meta:
        model = Pages
        fields = '__all__'
        #extra_fields = ['params']
        #depth=1
class GetResourceDefSerializer(serializers.ModelSerializer):
    
   Status = ListCodesSerializer()
   ResourceType = ResourceParamsSerializer()
   ResourceGroup = ListCodesSerializer(required=False)
   name = serializers.SerializerMethodField('get_resource_name')
   def get_resource_name(self,validated_data):
      
      lang = self.context['request'].parser_context['kwargs']['lang']
      id=validated_data.id
      rdls = ResourceDefLang.objects.filter(ResourceDefId_id=id,Lang__icontains=lang)
      name=''
      for rdl in rdls:
         name = rdl.Name
      return name
   
   class Meta:
      model = ResourceDef
      fields = ('id','Organisation','Status','Private','ResourceType','ResourceGroup','TableCreated','FormDefined','name')
   
               
class MenusSerializer(serializers.ModelSerializer):
      Params = serializers.DictField()
      MenuPosition = serializers.DictField()
      MenuType = serializers.IntegerField()
      Users = UserSerializer(many=True)
      Groups = GroupSerializer(many=True)
      TargetPage = GetPagesSerializer()
      Target = GetResourceDefSerializer()

      class Meta:
        model = Menus
        fields = '__all__'
        #depth=1

      
def GetStatusCodeByName (code_detail_name,code_head_name):
  
  cds = CodesDetail.objects.filter(CodesHeadId__CodeHeadName=code_head_name,Name=code_detail_name)
  code_id = None
  for cd in cds:
    code_id = cd.id
  
  return code_id

def GetCodeObjectByName (code_head_name,code_detail_name,code_auto_id):
  
  cds = CodesDetail.objects.filter(CodesHeadId__CodeHeadName=code_head_name,Name=code_detail_name).prefetch_related('CodesDetailLang')
  code_id = None
  for cd in cds:
    
    code_object = {'id':cd.Name,'name':cd._prefetched_objects_cache['CodesDetailLang'][0].Value}
  
  return code_object    