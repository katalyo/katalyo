from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from katalyonode.models.models import ResourceDef,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang,GroupLang,GroupExtendedProperties,UserExtendedProperties
from katalyonode.models.models import TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskInitiationInstance,FilesTable,FilesToResource,PresentationElementsTasks,TaskCodesActions,TaskExecutionInstances,Cookies,Language
import pdb
#from katalyonode.serializersresources import OrganisationsSerializer
from django.utils import timezone
import json

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'

class GroupLangSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupLang
        fields = ('id','Name','Lang')
        
class GroupSerializer(serializers.HyperlinkedModelSerializer):
    #GroupLang_groupid = GroupLangSerializer(many=True, read_only=True)
    name = serializers.SerializerMethodField('name_field')
    #allgrps =  GroupLang.objects.filter(Lang_id=7) #TODO change Lang to be variable
    
    def name_field(self,instance):
        
        #pdb.set_trace()
        name=instance.name +'(No lang rec)'
        grp_lang = instance.GroupLang_groupid.all()
        #GroupLang
        if len (grp_lang)>0:
            name=grp_lang[0].Name   
           
        return name
        
        
    class Meta:
        model = Group
        fields = ('id','url','name')
       
class GroupSimpleSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    class Meta:
        model = Group
        fields = ('id','url','name')
        extra_kwargs = {
            'name': {'validators': []},
        }
       

class UserSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.SerializerMethodField('name_field')
    #groups=GroupSerializer(many=True)
    groups = [1,2,3]
    def name_field(self,User):
      return User.first_name+ ' ' + User.last_name + ' - '+ User.username
    class Meta:
        model = User
        fields = ('id','url', 'name','username', 'first_name','last_name','email','last_login','is_superuser', 'groups')
       
        #depth=1
class UserSimpleSerializer(serializers.HyperlinkedModelSerializer):
    id = serializers.IntegerField()
    name = serializers.SerializerMethodField('name_field')
    def name_field(self, User):
      return User.first_name+ ' ' + User.last_name + ' - '+ User.username 
    
    class Meta:
        model = User
        fields = ('id','url', 'name','username', 'first_name','last_name','email','last_login','is_superuser')
        extra_kwargs = {
            'username': {'validators': []},
        }
class GroupWithUsersSerializer(serializers.HyperlinkedModelSerializer):
    users=UserSimpleSerializer(many=True,source="user_set")
    class Meta:
        model = Group
        fields = ('id','url','name','users')              
class GetCodesDetailSerializer(serializers.ModelSerializer):
    
    #Active=serializers.NullBooleanField()
    #CodesDetailLang = CodesDetailLangSerializer(many=True,read_only=True)
    name = serializers.SerializerMethodField("get_code_lang_name")#(source='CodesDetailLang.first.Value')
    def get_code_lang_name(self,instance):
       
        if 'request' in self.context:
            request = self.context['request']
        else:
            return None
        kwargs_var = request.parser_context['kwargs']
        if 'lang' in kwargs_var:
           lang=kwargs_var['lang']
        else:
           lang=None

        if lang is None:
            raise ValueError('Language not available (GetCodesDetailSerializer)')

        cdl = instance.CodesDetailLang.filter(Lang=lang)
        
        if len(cdl)>=1:
            name = cdl[0].Value
        else:
            name=''
        return name 
    
    class Meta:
        model = CodesDetail
        fields = ('id','CodesHeadId','name','Active')
        
class UserExtendedSerializer(serializers.ModelSerializer):
    Lang=LanguageSerializer()
    UserId = UserSerializer()
    class Meta:
        #list_serializer_class = FilteredListSerializer
        model = UserExtendedProperties
        fields = ('id','UserId','Organisation','Lang')

        
class GroupExtendedSerializer(serializers.ModelSerializer):
    GroupOwners = UserSimpleSerializer(many=True)
    GroupId = GroupWithUsersSerializer()
    GroupName = serializers.SerializerMethodField("get_group_name")
    def get_group_name(self,instance):
       
        lang=None
        if 'user_lang' in self.context:
            lang = self.context['user_lang']
        
        grp_lang = GroupLang.objects.filter(GroupId=instance.GroupId,Lang=lang)
        name = ''
        for item in grp_lang:
            name=item.Name
        return name
    #Users = UserSimpleSerializer(many=True,source='groups_set__user_set')
    class Meta:
        #list_serializer_class = FilteredListSerializer
        model = GroupExtendedProperties
        fields = ('id','GroupName','GroupId','Organisation','GroupOwners')
        extra_kwargs = {
            'GroupOwners': {'validators': []},
        }
class CodesDetailLangSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)
    class Meta:
      model = CodesDetailLang
      fields = ('id','Lang','Value','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime')
              
class OrganisationsSerializer(serializers.ModelSerializer):
    Country = GetCodesDetailSerializer()
    Industry = GetCodesDetailSerializer()
    #Active = GetCodesDetailSerializer()
    ApiUser = UserSimpleSerializer()
    class Meta:
        model = Organisations
        fields = ('id','Name', 'Description','Country','Industry','OrgType','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','ApiUser',
                  'ETHAccount','Active','Version')      

class CookiesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = Cookies
        fields = '__all__' 
#MIGOR
class FilesSerializer(serializers.ModelSerializer):
   class Meta:
      model = FilesToResource
      fields = '__all__'
      depth=1
#MIGOR END