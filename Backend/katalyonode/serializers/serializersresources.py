from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from katalyonode.models.models import ResourceDef,ResourceModelDefinition,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang
from katalyonode.models.models import TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskInitiationInstance,FilesTable,FilesToResource,PresentationElementsTasks,TaskCodesActions,TaskExecutionInstances
from katalyonode.models.models import DatasetMappingHead,DatasetMappingHeadLang,DatasetTransformHead,DatasetTransformHeadLang,UserExtendedProperties,ResourceParams,ResourceSubscriptions
from katalyonode.models.models import PresentationElementsHistory,PresentationElementsLangHistory,ResourceModelDefinitionHistory,DatasetMappingDetail,DatasetTransformDetail,ResourceLinks,Language
from katalyonode.serializers.serializerssettings import GetCodesDetailByIdLangSerializer,ListCodesSerializer,CodesDetailSerializer,ListCodesGenericSerializer,ResourceParamsSerializer
from katalyonode.serializers.serializers import UserSimpleSerializer, GroupSimpleSerializer, OrganisationsSerializer
from django.forms.models import model_to_dict
#from katalyonode.functions.resourceutilityfn import IsUserResourceAdmin,ProcessElementDefinitionDefault
from rest_framework.response import Response
import pdb
import sys
from copy import deepcopy
from django.utils import timezone
import json
from django.apps import apps
import importlib
#from importlib import import_module
import os


class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = '__all__'

class ResourceDefLangSerializer(serializers.ModelSerializer):
   id = serializers.IntegerField()
   Lang = LanguageSerializer()
   class Meta:
      model = ResourceDefLang
      fields = ('id','ResourceDefId', 'Lang','Name','Subject','Description','InitiateForm','ExecuteForm','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','Version')

class  FilteredListSerializer(serializers.ListSerializer):
   
   def to_representation(self, data):
      
       
        if 'lang' in self.context['view'].kwargs:
          lang = self.context['view'].kwargs['lang']
        else:
          lang=''
        
          
        data = data.filter(Lang_id=lang)
      
        return super(FilteredListSerializer, self).to_representation(data)
     
 
class ResourceDefLangFilteredSerializer(serializers.ModelSerializer):
   id = serializers.IntegerField()
   Lang = LanguageSerializer()
  
   class Meta:
      list_serializer_class = FilteredListSerializer
      model = ResourceDefLang
      fields = ('id','ResourceDefId', 'Lang','Name','Subject','Description','InitiateForm','ExecuteForm','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','Version')
    
  

class ResourceDefSerializer(serializers.ModelSerializer):
    
    Status = ListCodesSerializer()
    ResourceType = ResourceParamsSerializer()
    Tags = ListCodesSerializer(required=False,many=True)
    Parameters = serializers.DictField()

    
    class Meta:
        model = ResourceDef
        fields = ('id','Organisation','Status','Private','ResourceType','Tags','TableCreated','FormDefined','Parameters')

class ResourceSubscriptionsSerializer(serializers.ModelSerializer):
    
   class Meta:
        model = ResourceSubscriptions
        fields = '__all__'


class ResourceDefSubsSerializer(serializers.ModelSerializer):
  
   resourcedeflang_set = ResourceDefLangSerializer(many=True)
   ResourceType = ResourceParamsSerializer()
   Status = ListCodesSerializer()
   Parameters = serializers.DictField(required=False,allow_null=True)
   Organisation = OrganisationsSerializer()
   resourcesubscriptions_set = ResourceSubscriptionsSerializer(many=True)
   class Meta:
      model = ResourceDef
      fields = ('id','Organisation','Status','Private','ResourceType','Parameters','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','resourcedeflang_set','resourcesubscriptions_set')
      
class ResourceDefAllSerializer(serializers.ModelSerializer):
  
   resourcedeflang_set = ResourceDefLangSerializer(many=True)
   ResourceType = ResourceParamsSerializer()
   Tags = ListCodesSerializer(required=False,allow_null=True,many=True)
   Status = ListCodesSerializer()
   AdminUsers = UserSimpleSerializer(required=False,many=True)
   AdminGroups = GroupSimpleSerializer(required=False,many=True)
   Parameters = serializers.DictField(required=False,allow_null=True)
   ResourceExtended = serializers.SerializerMethodField('resource_extended')
   
   def resource_extended(self,ResourceDef):
          
         language=None
         if 'user_lang' in self.context:
              language = self.context['user_lang']

         if 'request' in self.context:
              user = self.context['request'].user
         
         #get additional data based on resource type
         #pdb.set_trace()
         
         
         try:
            module_path = 'katalyonode.functions.resources.'+ResourceDef.ResourceType.ResourceCode+'def'
            module_ref = importlib.import_module(module_path)
            resource_class = getattr(module_ref, ResourceDef.ResourceType.ResourceCode)
            
            
            resource_object = resource_class(ResourceDef.id, ResourceDef.ResourceType.ResourceCode,'','','',ResourceDef.Status.Name,user,language,ResourceDef.Organisation_id,ResourceDef.Parameters,ResourceDef.Version)
            #definitionFnModule =  importlib.import_module(module_path, "GetExtendedResourceData")
            
            r_extended ={} 
            if resource_object is not None:
               try:
                  r_extended = resource_object.get_extended_resource_data()
               except AttributeError:
                  #raise serializers.ValidationError("AttributeError -->" + resource_object) #MIGOR - ako se ne raisa ovdje error onda se greska tesko uhvati na klijentu
                  r_extended ={'Error':'AttributeError --> '+str(vars(resource_object))} 
         except ModuleNotFoundError:
            #raise serializers.ValidationError("ModuleNotFoundError -->" + module_path) #MIGOR - ako se ne raisa ovdje error onda se greska tesko uhvati na klijentu
            r_extended ={'Error':'ModuleNotFoundError --> '+module_path} #MIGOR
            
         return r_extended
      
   class Meta:
      model = ResourceDef
      fields = ('id','Organisation','Status','Private','ResourceType','Tags','IsBlockchain','AdminUsers','AdminGroups','Parameters','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','resourcedeflang_set','ResourceExtended')
      depth = 1
   
      
      
   def create(self, validated_data):
    
        auth_user = self.context['request'].user
        
        validated_data['CreatedBy']=auth_user
        validated_data['CreatedByDatetime']=timezone.now()
        return validated_data
    
   def update(self, instance, validated_data):
    
        auth_user = self.context['request'].user
        #isAdmin = IsUserResourceAdmin(instance.id,auth_user)
        if not isAdmin:
            raise serializers.ValidationError('You are not authorised to change this resource !',code=404)
         
        rdl= validated_data.pop('resourcedeflang_set')
        
        tags_ids = []
        if 'Tags' in validated_data:
            tags = validated_data['Tags']
            for tag in tags:
              if 'id' in tag:
                tags_ids.append(tag['id'])
         
        aUsers = validated_data['AdminUsers']
        users_ids = []
        for user in aUsers:
          if 'id' in user:
            users_ids.append(user['id'])
        
        aGroups = validated_data['AdminGroups']
        groups_ids = []
        for group in aGroups:
          if 'id' in group:
            groups_ids.append(group['id'])
        
           
        resourceDefLang = ResourceDefLang.objects.get(id=rdl[0]['id'])
        resourceDefLang.Name = rdl[0]['Name']
        resourceDefLang.Subject = rdl[0]['Subject'] 
        resourceDefLang.Description = rdl[0]['Description']

        instance.ResourceType_id = validated_data['ResourceType']['id']
        instance.Status_id = validated_data['Status']['id']
        instance.Tags.set(tags_ids)
        instance.AdminUsers.set(users_ids)
        instance.AdminGroups.set( groups_ids)
        instance.Parameters = validated_data['Parameters']
        instance.IsBlockchain = validated_data['IsBlockchain']
        instance.ChangedBy = auth_user
        instance.ChangedDateTime = timezone.now()
        instance.save()
        resourceDefLang.save()
        return instance
       
class GetResourceDefByIdLangSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='resourcedeflang_set.first.Name')
    
    class Meta:
        model = ResourceDef
        fields = ('id','name')
        
class SaveResourceDefLangSerializer(serializers.ModelSerializer):
    
    ResourceDefId = ResourceDefSerializer()
    resource_id = serializers.PrimaryKeyRelatedField(queryset=ResourceDef.objects.all(), required=False,source='ResourceDef', write_only=True)
    Lang = LanguageSerializer()
    InitialVersion = serializers.IntegerField(required=False)
    class Meta:
        model = ResourceDefLang
        fields = ('id','resource_id','Name','Subject','Description','Version','InitialVersion','Lang','ParametersLang','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','ResourceDefId')
        
    def create(self, validated_data):
        # Create the ResourceDef instance
        

        resource_data = validated_data.pop('ResourceDefId')
        user = self.context['request'].user
        
        if 'Tags' in resource_data:
          resourceGroup_id = resource_data['Tags']['id']
        else:
          resourceGroup_id=None
          
        if 'Parameters' in resource_data:
          params = resource_data['Parameters']
        else:
          params={}
          
        if 'ParametersLang' in resource_data:
          paramsLang = resource_data['ParametersLang']
        else:
          paramsLang=None
        
        org_id=None
        uep = UserExtendedProperties.objects.filter(UserId_id=user.id)
        if len(uep)>0:
         org_id=uep[0].Organisation
        else:
         raise serializers.ValidationError('Error when retrieving organisation!',code=404)
      
        resourceId =  ResourceDef.objects.create(Status_id=resource_data['Status']['id'],Private=resource_data['Private'],ResourceType_id=resource_data['ResourceType']['id'],InitialVersion=1,Version=1,Parameters = params,
                                                 CreatedBy = user, CreatedDateTime=timezone.now(),Organisation=org_id,TableCreated=resource_data['TableCreated'],FormDefined=resource_data['FormDefined'])
        #pdb.set_trace()
        if resourceId is not None:
             
        
             if resourceGroup_id is not None:
                resourceId.Tags.set(resourceGroup_id)
                resourceId.save()
                #update resourceId for app
         
             
             for uep_rec in uep:
                if uep_rec.Params is not None:
                   if 'Onboarding' in uep_rec.Params:
                      if uep_rec.Params['Onboarding']!='Completed':
                         uep_params=uep_rec.Params
                         uep_params['OnboardingAppId'] = resourceId.id
                         uep_params['Onboarding'] = 'AppSetup'
                         uep_rec.Params = uep_params
                         uep_rec.save()
            
        resourceLang = ResourceDefLang.objects.create(Lang=validated_data['Lang'],Name=validated_data['Name'],Subject=validated_data['Subject'],Description=validated_data['Description'],ParametersLang = paramsLang,
                      CreatedBy=user, CreatedDateTime=timezone.now(),Version=1,InitialVersion=1,InitiateForm=False,ExecuteForm=False,ResourceDefId=resourceId)
        
        return resourceLang
    
    def update(self, instance, validated_data):
        # Get  CodesDetailId
        
        resource_data= validated_data.pop('ResourceDefId')
      
        #update Value
        
        resourceDef = ResourceDef(id=instance.resource_id,Status=resource_data['Status'],Private=resource_data['Private'],ResourceType=resource_data['ResourceType'],Organisation=resource_data['Organisation'])
        
        resourceDef.save()
        
        instance.Name = validated_data.get('Name', instance.Name)
        instance.Subject = validated_data.get('Subject', instance.Subject)
        instance.Description = validated_data.get('Description', instance.Description)
        instance.ChangedBy = validated_data.get('ChangedBy', instance.ChangedBy)
        instance.ResourceDef = resourceDef
        instance.save()
        return instance


      
class LayoutSerializer(serializers.Serializer):
    
    id = serializers.IntegerField()
    LocalId =serializers.IntegerField(required=False,allow_null=True)
    PresentationId = serializers.IntegerField(required=False,allow_null=True)
    PresentationLangId = serializers.IntegerField(required=False,allow_null=True)
    PresentationIdExt = serializers.IntegerField(required=False,allow_null=True)
    PresentationLangIdExt = serializers.IntegerField(required=False,allow_null=True)
    ItemId = serializers.IntegerField(required=False,allow_null=True)
    ItemValue = serializers.ListField(required=False,allow_null=True)
    Status = serializers.CharField(required=False,allow_null=True)
    MetaData = serializers.ListField(required=False,allow_null=True)
    #sItemValue = serializers.CharField(required=False,allow_null=True)
    DefaultValue = serializers.JSONField(required=False,allow_null=True) #ILJ 09.10.2019
    wItemValue = serializers.ListField(required=False,allow_null=True)
    fItemValue = serializers.ListField(required=False,allow_null=True)
    tItemValue = serializers.ListField(required=False,allow_null=True)
    SearchText = serializers.CharField(required=False,allow_null=True,allow_blank=True)
    CodeId = serializers.IntegerField(required=False)
    TabIndex = serializers.IntegerField(required=False,allow_null=True)
    LabelLength = serializers.IntegerField(required=False,allow_null=True)
    ValueLength = serializers.IntegerField(required=False,allow_null=True)
    ParentElementId = serializers.IntegerField(required=False,allow_null=True)
    Name = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=200)
    Required = serializers.NullBooleanField(required=False)
    ShowDetails = serializers.NullBooleanField(required=False)
    Multiline = serializers.NullBooleanField(required=False)
    ShowField = serializers.NullBooleanField(required=False)
    ShowHeader = serializers.NullBooleanField(required=False)
    UseField = serializers.NullBooleanField(required=False)
    ReadOnly = serializers.NullBooleanField(required=False)
    BackgroundColor = serializers.CharField(required=False,allow_null=True,allow_blank=True) #MIGOR2017
    TextAlign=serializers.CharField(required=False,allow_null=True,allow_blank=True)#MIGOR2017
    FontSize = serializers.IntegerField(required=False,allow_null=True)#MIGOR2017
    UseDefault = serializers.NullBooleanField(required=False)
    UpdateWithDefault = serializers.NullBooleanField(required=False)
    UseSystemDate = serializers.NullBooleanField(required=False)
    MaxLength = serializers.IntegerField(required=False,allow_null=True)
    Related = serializers.IntegerField(required=False,allow_null=True)
    PopulateType = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=2)
    PopulateMethod = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=1)
    SrcWidgetId = serializers.IntegerField(required=False,allow_null=True)
    SrcParentWidgetId = serializers.IntegerField(required=False,allow_null=True)
    HasParentDataset = serializers.NullBooleanField(required=False)
    ParentDatasetId = serializers.IntegerField(required=False,allow_null=True)
    PopulateTypeParent = serializers.CharField(required=False,allow_null=True,max_length=3)
    PopulateParentMethod = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=1)
    ElementType = serializers.CharField(max_length=200)
    #Type = serializers.CharField(max_length=200)
    ErrorMsg = serializers.CharField(required=False,allow_blank=True,max_length=200)
    Placeholder = serializers.CharField(required=False,allow_blank=True,max_length=200)
    Label = serializers.CharField(required=False,allow_blank=True,max_length=200)
    Dbtype = serializers.CharField(required=False,max_length=200)
    NoUpdate=serializers.NullBooleanField(required=False)  
    AutoSearch=serializers.NullBooleanField(required=False) #MIGOR    
    ShowSearchForm =serializers.NullBooleanField(required=False) #MIGOR 
    SizeX = serializers.IntegerField(required=False)
    UniqueId = serializers.IntegerField(required=False,allow_null=True)
    Locked = serializers.BooleanField(required=False)
    InlineTask = serializers.NullBooleanField(required=False)
    ResponseMsg = serializers.CharField(required=False,allow_null=True,allow_blank=True)
    GridState = serializers.DictField(required=False,allow_null=True)
    OriginalPresentationElementId = serializers.IntegerField(required=False,allow_null=True)
    Search = serializers.JSONField(required=False,allow_null=True)
    DatasetMapping = serializers.ListField(required=False,allow_null=True)
    PresentationType = serializers.CharField(required=False,allow_blank=True,allow_null=True,max_length=1)
    NameField = serializers.ListField(required=False,allow_null=True)
    Version = serializers.IntegerField(required=False,allow_null=True)
    AngularDirectiveName=serializers.CharField(required=False,allow_null=True,allow_blank=True)#ILJ 09.09.2017
    DirectiveName=serializers.CharField(required=False,allow_null=True,allow_blank=True)#LJ 09.09.2017
    Parameters = serializers.JSONField(required=False,allow_null=True) #ILJ 09.10.2019
    ParametersLang = serializers.JSONField(required=False,allow_null=True) #ILJ 09.10.2019
    Unique = serializers.NullBooleanField(required=False) #ILJ 18.03.2020
    FieldName = serializers.CharField(required=False,allow_null=True,allow_blank=True)#LJ 25.09.2021
    layout = RecursiveField(required=False,allow_null=True,many=True)
    
     
##########################################################
############# ResourceFormOutSerializer
#########################################################
class ResourceFormOutSerializer(serializers.Serializer):
    
        
    class Meta:
        model = PresentationElements
    
    def create(self, validated_data):
       
      
      
      item = validated_data
      
       
      return item
##########################################################
############# ResourceColumnsDefSerializer
#########################################################

class ResourceColumnsDefSerializer(serializers.Serializer):
    
    name = serializers.CharField(max_length=200)
    displayName = serializers.CharField(max_length=200)
    
    def __init__(self,instance):
        
        self.name = instance[1]+str(instance[0])
        self.displayName = instance[1]+str(instance[0])
        super(ResourceColumnsDefSerializer, self).__init__(instance=instance)
    
    
class GetResourceFormSerializer(serializers.Serializer):
    
    id = serializers.IntegerField()
    PresentationId = serializers.IntegerField(required=False,allow_null=True)
    PresentationLangId = serializers.IntegerField(required=False,allow_null=True)
    FormType = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=1)
    Name = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=255)
    Type = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=255)
    Dbtype = serializers.CharField(required=False,max_length=255)
    SizeX = serializers.IntegerField()
    Status = serializers.CharField(required=False,allow_null=True)
    #sizeY = serializers.CharField(max_length=5)
    SizeY = serializers.IntegerField()
    #row = serializers.IntegerField()
    #col = serializers.IntegerField()
    Locked = serializers.BooleanField()
    #order = serializers.IntegerField()
    TabIndex = serializers.IntegerField()
    ShowField = serializers.NullBooleanField(required=False)
    ShowHeader = serializers.NullBooleanField(required=False)
    #Required = serializers.BooleanField()
    layout = LayoutSerializer(many=True,required=False)
    BackgroundColor = serializers.CharField(required=False,allow_null=True,allow_blank=True) #MIGOR2017
    textAlign=serializers.CharField(required=False,allow_null=True,allow_blank=True)#MIGOR2017
    FontSize = serializers.IntegerField(required=False,allow_null=True)#MIGOR2017
    AngularDirectiveName=serializers.CharField(required=False,allow_null=True,allow_blank=True)#ILJ 09.09.2017
    DirectiveName=serializers.CharField(required=False,allow_null=True,allow_blank=True)#LJ 09.09.2017

def first_letter_upper_case(str_item):
  ret_str_item = str_item[:1].upper()+str_item[1:]
  return ret_str_item

def CheckForChanges(item_dict,new_item,resource_type_text,directive_name):
   
    has_changes = False
   
    if directive_name is not None:
        #if directive_name == 'ktResource':
        #pdb.set_trace()
        try:
            
            module_path = 'katalyonode.functions.widgets.'+resource_type_text+'.'+directive_name
            definitionFnModule =  importlib.import_module(module_path, "CheckForChanges")
            if definitionFnModule is not None:
               #get Fn from name
               fnToCall=None
               try:
                  fnToCall = getattr(definitionFnModule, 'CheckForChanges')
               except AttributeError:
                  pass   
               if fnToCall is not None:
                  #call Fn
                  has_changes = fnToCall(item_dict,new_item)
        except ModuleNotFoundError:
            pass

    return has_changes

def CheckForChanges_Old(item_dict,new_item,resource_type_text):
   has_changes = False
   for element in new_item.items():
      notFound = False
      field_id = element[0]
      
      if field_id not in ['id','Id','Version','ChangedDateTime','CreatedDateTime','ChangedBy_id','CreatedBy_id','ChangedBy','CreatedBy']:
               
         #pdb.set_trace()
         if field_id=='Placeholder':
            field_id='Label'
            
         if field_id in item_dict:
            item = item_dict[field_id]
         else:
            item=None
            notFound = True
         
         if item is not None:
            if type(element[1]) in [dict,list] and type(item) is str:

               if '{' and '}' in item:
                  item = json.loads(item)
                  if item!=element[1]:
                     has_changes=True
               elif '[' and ']' in item:
                  item = json.loads(item)
                  if item!=element[1]:
                     has_changes=True
               else:
                  if 'name' in element[1]:
                     if item!=element[1]['name']:
                        has_changes=True      
         
            else:
               if item!= element[1]:
                  has_changes = True
         elif not notFound:
            if element[1] is not None:
               if element[1]!='':
                  has_changes = True
         #elif notFound:
            #has_changes=True #use this when fields are the same in old and new
   if 'DirectiveName' in new_item:    

      if not has_changes:
         
         try:
            module_path = 'katalyonode.functions.widgets.'+resource_type_text+'.'+new_item['DirectiveName']
            definitionFnModule =  importlib.import_module(module_path, "CheckForChanges")
            if definitionFnModule is not None:
               #get Fn from name
               fnToCall=None
               try:
                  fnToCall = getattr(definitionFnModule, 'CheckForChanges')
               except AttributeError:
                  pass   
               if fnToCall is not None:
                  #call Fn
                  has_changes = fnToCall(item_dict,new_item)
         except ModuleNotFoundError:
            pass
   #pdb.set_trace()      
   return has_changes

class SaveResourceFormListSerializer(serializers.ListSerializer):
    def create(self, validated_data):
      #books = [Book(**item) for item in validated_data]
      
      counter = 0
      rVal=[]
      for item in validated_data:
        item['row'] = counter
        counter += 1
        rVal.append(self.child.create(item))
      return rVal
    
class SaveResourceFormSerializer(serializers.Serializer):
    
    id = serializers.IntegerField()
    LocalId =serializers.IntegerField(required=False,allow_null=True)
    PresentationId = serializers.IntegerField(required=False,allow_null=True)
    PresentationLangId = serializers.IntegerField(required=False,allow_null=True)
    FormType = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=1)
    Name = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=255)
    Type = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=255)
    Dbtype = serializers.CharField(required=False,max_length=255)
    SizeX = serializers.IntegerField()
    Status = serializers.CharField(required=False,allow_null=True)
    #sizeY = serializers.CharField(max_length=5)
    #SizeY = serializers.IntegerField()
    #row = serializers.IntegerField()
    #col = serializers.IntegerField()
    Locked = serializers.BooleanField()
    #order = serializers.IntegerField()
    TabIndex = serializers.IntegerField(required=False)
    ShowField = serializers.NullBooleanField(required=False)
    ShowHeader = serializers.NullBooleanField(required=False)
    #Required = serializers.BooleanField()
    layout = LayoutSerializer(many=True,required=False)
    BackgroundColor = serializers.CharField(required=False,allow_null=True,allow_blank=True) #MIGOR2017
    TextAlign = serializers.CharField(required=False,allow_null=True,allow_blank=True)#MIGOR2017
    FontSize = serializers.IntegerField(required=False,allow_null=True)#MIGOR2017
    AngularDirectiveName=serializers.CharField(required=False,allow_null=True,allow_blank=True)#ILJ 09.09.2017
    DirectiveName=serializers.CharField(required=False,allow_null=True,allow_blank=True)#LJ 09.09.2017
    Parameters = serializers.JSONField(required=False,allow_null=True) #ILJ 09.10.2019
    ParametersLang = serializers.JSONField(required=False,allow_null=True) #ILJ 09.10.2019
    Version = serializers.IntegerField(required=False,allow_null=True)
    
    class Meta:
        list_serializer_class = SaveResourceFormListSerializer
        
    def create(self, validated_data):
        
        rid = int(self.context['resourceId'])
        
        form_type = self.context['formType']
        current_user = self.context['user']
        version = self.context['version']
        new_version = self.context['new_version']
        organisation_id = self.context['org_id']
         
        row_number = 0
        lang=self.context['lang']
        
        vData = validated_data
                 
        resource_id = ResourceDef.objects.select_related().get(id=rid)
        org_id = Organisations.objects.get(id=organisation_id)
        resourceModelIds = []
        widgetLinks = []
        datasetWidgets = []
        count_changes = 0
        new_items = {}
        resource_type_text = resource_id.ResourceType.ResourceCode
        custom_fields = False
          #SaveResourceFormSerializer
        if resource_id.Parameters is not None:
         if 'externalDataSource' in resource_id.Parameters:
            if resource_id.Parameters['externalDataSource']:
               custom_fields = resource_id.Parameters['externalDataSource']
        if 'TabIndex' in vData:
          tabIndex=vData['TabIndex']
        else:
          tabIndex=0
        if 'ResponseMsg' in vData:
          response_msg=vData['ResponseMsg']
        else:
          response_msg=''
        if 'ShowHeader' in vData:
          showHeader=vData['ShowHeader']
        else:
          showHeader=False
        
        if 'ShowField' in vData:
          showField =  vData['ShowField']
        else:
          showField=True;

        #MIGOR2017
        if 'TextAlign' in vData:
          textAlign =  vData['TextAlign']
        else:
          textAlign='';
        if 'BackgroundColor' in vData:
          backgroundColor =  vData['BackgroundColor']
        else:
          backgroundColor='';          
        if 'FontSize' in vData:
          fontSize=vData['FontSize']
        else:
          fontSize=0 
        
        if 'DirectiveName' in vData:
            directiveName=vData['DirectiveName']
        else:
            directiveName=None
      
        if 'AngularDirectiveName' in vData:
            angularDirectiveName=vData['AngularDirectiveName']
        else:
            angularDirectiveName=None
      
        if 'Parameters' in vData:
            parameters=vData['Parameters']
        else:
            parameters=None
        
        if 'ParametersLang' in vData:
            parametersLang=vData['ParametersLang']
        else:
            parametersLang=None
            
        
        #change the logic for versioning ILJU 07.10.2019
        #check if it's a new record or changed
        vData['Order'] = vData['row']
        
        if vData['PresentationId'] == None: #new record
          #insert row data into PresentationElements
          
          peRow = PresentationElements.objects.create(FontSize=fontSize,TextAlign=textAlign,BackgroundColor=backgroundColor,ResourceDefId = resource_id,FormType=form_type,OrganisationId=org_id,ElementType = 'row',ContainerId= None,Locked=vData['Locked'],
                                                               Status = 'Active',SizeX = vData['SizeX'],SizeY= 0,Column= 0,Row = vData['row'],Order = vData['row'],Tabindex = tabIndex,PopulateMethod = '',PopulateType ='0',PopulateTypeParent='0',InitialVersion = new_version,
                                                                  Version = new_version,CreatedBy = current_user,CreatedDateTime=timezone.now(),Parameters=parameters,DirectiveName = directiveName,AngularDirectiveName = angularDirectiveName,ParentDatasetId = None,
                                                                     HasParentDataset=False,ShowHeader=showHeader,ShowField=showField,OverrideType='n',ResourceType='o') #MIGOR2017
          #then row into PresentationElementsLang
          
          peLangRow = PresentationElementsLang.objects.create(Lang = lang,Name = vData['Name'],Label = vData['Name'],Subject = '',
                                        InitialVersion=new_version,Version=new_version,CreatedBy = current_user,CreatedDateTime=timezone.now(),ParametersLang =parametersLang,
                                        ResponseMsg=response_msg,Description= '',ErrorMsg = '',PresentationElementsId=peRow)
          
          vData['PresentationId'] = peRow.id
          vData['PresentationLangId'] = peLangRow.id
          if 'LocalId' in vData:
            new_items[vData['LocalId']] = peRow.id
          peRow = model_to_dict(peRow)
        else:
         peRows = PresentationElements.objects.filter(id=vData['PresentationId']).values()
         
         if 'DirectiveName' in vData:

            directive_name = vData['DirectiveName']
         else:
            directive_name = None

         for peRow in peRows:
            changed = CheckForChanges(peRow,vData,resource_type_text,directive_name)
            if changed:
               #pdb.set_trace() #CheckForChanges
               #insert into history
               cRowHistory = deepcopy(peRow)
               cRowHistory['PresentationElementsId_id'] = peRow['id'];
               cRowHistory['id']=None
               peHistoryRow = PresentationElementsHistory.objects.create(**cRowHistory)
               count_changes+=1
               
               if peHistoryRow:   
                  #update changes
                  peRowUpd = PresentationElements.objects.get(id=vData['PresentationId'])
                  if peRowUpd:
                     peRowUpd.FontSize = fontSize
                     peRowUpd.TextAlign = textAlign
                     peRowUpd.BackgroundColor=backgroundColor
                     peRowUpd.ResourceDefId=resource_id
                     peRowUpd.FormType=form_type
                     peRowUpd.Status='Active'
                     peRowUpd.SizeX = vData['SizeX']
                     peRowUpd.Row = vData['row']
                     peRowUpd.Order = vData['row']
                     peRowUpd.Tabindex = tabIndex
                     peRowUpd.Version=new_version
                     peRowUpd.Parameters=parameters
                     peRowUpd.ShowHeader=showHeader
                     peRowUpd.ShowField=showField
                     peRowUpd.ChangedBy = current_user
                     peRowUpd.ChangedDateTime=timezone.now()
                     peRowUpd.save()
            
         peLangRows = PresentationElementsLang.objects.filter(id=vData['PresentationLangId']).values()
         peLangRow = None
         for peLangRow in peLangRows:
            langChanged = CheckForChanges(peLangRow,vData,resource_type_text,directive_name)
         
            if langChanged:
               #pdb.set_trace() #CheckForChanges

               #insert into history
               cRowLangHistory = deepcopy(peLangRow)
               cRowLangHistory['PresentationElementsLangId_id'] = peLangRow['id']
               cRowLangHistory['id']=None
               #cRowLangHistory.PresentationElementsLangId_id = peLangRow.id
               #cRowLangHistory.id=None
               peLangHistoryRow = PresentationElementsLangHistory.objects.create(**cRowLangHistory)
               
               #peLangHistoryRow=True
               count_changes+=1
               if peLangHistoryRow:
                  peLangRowUpd = PresentationElementsLang.objects.get(id=vData['PresentationLangId'])
                  if peLangRowUpd:
                     #update changes
                     peLangRowUpd.Name = vData['Name']
                     peLangRowUpd.Label=vData['Name']
                     peLangRowUpd.Version=new_version
                     peLangRowUpd.ParametersLang=parametersLang
                     peLangRowUpd.ResponseMsg = response_msg
                     peLangRowUpd.ChangedBy = current_user
                     peLangRowUpd.ChangedDateTime=timezone.now()
                     peLangRowUpd.save()
            
        
        vData['Version'] = new_version
        
        if 'layout' in vData:
          layoutArr = vData['layout']
     
          #vData['layout'],resourceModelIds,datasetWidgets,widgetLinks,no_of_changes = ProcessLayoutArray('n','o',layoutArr,resource_id,org_id,lang,form_type,current_user,peRow,peLangRow,tabIndex,peRow['id'],None,None,False,new_version,count_changes)
          retRowObject = ProcessLayoutArray('n','o',layoutArr,resource_id,org_id,lang,form_type,current_user,peRow,peLangRow,tabIndex,peRow['id'],None,None,False,new_version,count_changes,custom_fields)

          vData['layout'] = retRowObject['layout']
          resourceModelIds = retRowObject['resourceModelIds']
          datasetWidgets = retRowObject['datasetWidgets']
          widgetLinks = retRowObject['widgetLinks']
          no_of_changes = retRowObject['count_changes']
          new_items.update(retRowObject['new_items'])
          
        rVal = {'vData':vData,'resourceModelIds':resourceModelIds,'datasetWidgets':datasetWidgets,'widgetLinks':widgetLinks,'count_changes':no_of_changes,'new_items':new_items}
      
        return rVal
      
    def update(self,instance, validated_data):
        # Create the ResourceDef instance
      
        resource_id = self.context.resourceId
        for widget in validated_data:
           
            #insert widget
            
            #get layout
            layout = widget.pop('layout')
            for itemLayout in layout:
              
              #insert into ResourceModelDef
              if itemLayout["ElementType"] not in ('tabs','hr','label','uploader','id'):
                  if layout['ElementType']=='list':
                    field_type='codesdetail'
                  elif layout['ElementType']=='resource':
                    field_type=layout['ElementType']
                  else:
                    field_type=layout['ElementType']
                  resourceModel = ResourceModelDef.objects.Create(ResourceDefId=resource_id,FieldType=field_type,GenericType=layout['ElementType'],DBType = layout['dbtype'],
                                        Fieldname='Field',Status='Active')
                  
        return resourceModel

def  ProcessLayoutArray(override_type,resource_type,layoutArr,resource_id,org_id,lang,form_type,current_user,peRow,peRowLang,tabIndexP,parent_id,widget_id,related_dataset_id,topRows,new_version,count_changes,custom_fields):

  count = 0
  resourceModelIds = []
  resourceModelIdsTmp = []
  widgetLinks = []
  widgetLinksTmp = []
  datasetWidgets = []
  datasetWidgetsTmp = []
  params={}
  retLayoutObject = {}
  new_items = {}
  retLayoutObject['resourceModelIds'] = resourceModelIds
  retLayoutObject['widgetLinks'] = widgetLinks
  retLayoutObject['datasetWidgets'] = datasetWidgets
  retLayoutObject['new_items'] = new_items
  resource_type_text = resource_id.ResourceType.ResourceCode
  #prepare params
  params['topRows'] = topRows
  params['lang'] = lang
  params['override_type'] = override_type
  params['resource_id'] = resource_id
  params['org_id'] = org_id
  params['form_type'] = form_type
  params['current_user'] = current_user
  params['resource_type'] = resource_type
  params['parent_id'] = parent_id
  params['widget_id'] = widget_id
  params['tabIndexP'] = tabIndexP
  params['related_dataset_id'] = related_dataset_id
  params['new_version'] = new_version
  params['count'] = count
  
  #pdb.set_trace()
  for index,layout in enumerate(layoutArr):

    #element processing logic
    #get default layout
    procElementDefault = ProcessElementDefinitionDefault(layout,params)
    
    #then do processing specific to element/widget
    #import module
    procElementResult = procElementDefault
    directive_name = layout['DirectiveName']
    try:
      module_path = 'katalyonode.functions.widgets.'+resource_type_text.lower()+'.'+directive_name
      definitionFnModule =  importlib.import_module(module_path, "ProcessElementDefinition")
      
      if definitionFnModule is not None:
         #get Fn from name 
         fnToCall = None
         try:
            fnToCall = getattr(definitionFnModule, 'ProcessElementDefinition')
         except AttributeError:
            pass  
         if fnToCall is not None:
            #call Fn
            procElementResult = fnToCall(procElementDefault,params)
    except ModuleNotFoundError:
      procElementResult = procElementDefault
    
      
    layout = procElementResult['layout']
    #put Order in layout
    layout['Order'] = index
    layout['ContainerId_id'] = parent_id
    peDefault = procElementResult['peDefault']
    peDefaultLang = procElementResult['peDefaultLang']
    peSearch = procElementResult['peSearch']
    peSearchLang = procElementResult['peSearchLang']
    resourceModelDefaults =  procElementResult['resourceModelDefaults']
    resourceModelObject = procElementResult['resourceModelObject']
    if 'resourceLinks' in procElementResult:
      resourceLinks =   procElementResult['resourceLinks']
    else:
      resourceLinks = None
    #first PresentationElements
       
    peModel = apps.get_model('katalyonode', 'PresentationElements')
    
    pe,created = peModel.objects.get_or_create(**peSearch,defaults=peDefault)
    
    if created:
      #pdb.set_trace() #check for changes
      count_changes+=1
    if not created and pe:
      
      peValues = peModel.objects.filter(**peSearch).values()
      for peValue in peValues:
         changed = CheckForChanges(peValue,layout,resource_type_text,directive_name)
        
         if changed:
            #insert into history
            #pdb.set_trace() #CheckForChanges
            cRowHistory = deepcopy(peValue)
            cRowHistory['PresentationElementsId_id'] = peValue['id'];
            cRowHistory['id']=None
            peHistoryRow = PresentationElementsHistory.objects.create(**cRowHistory)
            count_changes+=1
            
            if peHistoryRow:
               if override_type=='n':
                 pe.ResourceDefId = resource_id
               else:
                 pe.ResourceDefId_id = related_dataset_id
                 pe.TaskDefId = resource_id
                 pe.ParentWidgetId_id = params['widget_id']
                 pe.OriginalPresentationElementId_id = layout['ope_id']
               pe.OrganisationId = org_id
               pe.FormType = form_type
               pe.ElementType = layout['ElementType']
               pe.ContainerId_id = layout['l_parent_id']#peRow.id
               pe.Locked = layout['Locked']
               pe.AutoSearch= layout['AutoSearch']
               pe.ShowSearchForm = layout['ShowSearchForm']
               pe.Order = index
               pe.SizeX = layout['SizeX']
               pe.Tabindex  =  tabIndexP
               pe.SrcWidgetId = layout['SrcWidgetId']
               pe.PopulateMethod = layout['PopulateMethod']
               pe.PopulateType = layout['PopulateType']
               pe.PopulateTypeParent = layout['PopulateTypeParent']
               pe.SrcParentWidgetId = layout['SrcParentWidgetId']
               pe.PopulateParentMethod = layout['PopulateParentMethod']
               pe.Related_id= layout['Related']
               pe.Status= layout['Status']
               pe.HasParentDataset=layout['HasParentDataset']
               pe.ParentDatasetId_id= layout['ParentDatasetId']
               pe.InlineTask= layout['InlineTask']
               pe.ShowHeader=layout['ShowHeader']
               pe.Required = layout['Required']
               pe.ShowField = layout['ShowField']
               pe.Multiline = layout['Multiline']
               pe.UniqueId_id = layout['UniqueId']
               pe.ReadOnly = layout['ReadOnly']
               pe.UseDefault = layout['UseDefault']
               pe.UseSystemDate = layout['UseSystemDate']
               pe.DefaultValue = layout['DefaultValue']
               pe.NoUpdate = layout['NoUpdate']
               pe.UpdateWithDefault = layout['UpdateWithDefault']
               pe.OverrideType = override_type
               pe.ResourceType = resource_type
               pe.ResourceModelDefinitionId_id= layout['ItemId']
               pe.Search = layout['Search']
               pe.DatasetMapping = layout['strDatasetMapping']
               pe.PresentationType = layout['PresentationType']
               pe.NameField = layout['NameField']
               pe.BackgroundColor  =  layout['BackgroundColor'] #MIGOR2017
               pe.TextAlign = layout['TextAlign'] #MIGOR2017
               pe.FontSize = layout['FontSize'] #MIGOR2017
               pe.Parameters = layout['Parameters'] #ILJU 07.10.2019
               pe.Version = new_version #ILJU 07.10.2019
               pe.DirectiveName = layout['DirectiveName'] #ILJ 09.09.2019
               pe.AngularDirectiveName = layout['AngularDirectiveName'] # ILJ 09.09.2019
               pe.ChangedBy = current_user
               pe.ChangedDateTime=timezone.now()
               pe.save()
   
    peModelLang = apps.get_model('katalyonode', 'PresentationElementsLang')
    
    peDefaultLang ['PresentationElementsId']= pe
    #peDefaultLang ['Lang']= lang
    
    peLang,created = peModelLang.objects.get_or_create(**peSearchLang,defaults = peDefaultLang)
    
    if created:
      #pdb.set_trace() #Check for changes
      count_changes+=1
    
    if not created and peLang:
      peLangValues = peModelLang.objects.filter(**peSearchLang).values()
      for peLangValue in peLangValues:
      
         changedLang = CheckForChanges(peLangValue,layout,resource_type_text,directive_name)
         
         if changedLang:
           #pdb.set_trace() #CheckForChanges
           #insert into history
           cRowLangHistory = deepcopy(peLangValue)
           cRowLangHistory['PresentationElementsLangId_id'] = peLangValue['id'];
           cRowLangHistory['id']=None
           peLangHistoryRow = PresentationElementsLangHistory.objects.create(**cRowLangHistory)
           count_changes+=1
         
           if peLangHistoryRow:
               peLang.PresentationElementsId = pe  
               peLang.Lang = lang
               peLang.Name = layout['Name']
               peLang.Label = layout['Label']
               peLang.Subject = layout['Name']
               peLang.Description = layout['Name']
               peLang.ValueLength = layout['ValueLength']
               peLang.LabelLength = layout['LabelLength']
               peLang.ErrorMsg = layout['ErrorMsg']
               peLang.SearchText = layout['SearchText']
               peLang.ResponseMsg = layout['ResponseMsg']
               peLang.ParametersLang = layout['ParametersLang'] #ILJU 09.10.2019
               peLang.GridState = layout['strGridState']
               peLang.ChangedBy = current_user
               peLang.ChangedDateTime=timezone.now()
               peLang.Version = new_version
               peLang.save()
    
    if 'LocalId' in layout and layout['PresentationId'] is None:
         
        new_items[layout['LocalId']] = pe.id
          
    if override_type=='n':
        layout['PresentationId'] = pe.id
        layout['id'] = pe.id
        layout['PresentationLangId'] = peLang.id
        
    else:
        layout['PresentationIdExt'] = pe.id
        layout['PresentationLangIdExt'] = peLang.id
    
    layout['Version'] = new_version  
   
   
    #create or update ResourceModelDefinition if required
    if layout['Dbtype']!='none' and params['override_type']=='n':
         
         if layout['Related'] is not None:
            related = ResourceDef.objects.get(id=layout['Related'])
         else:
            related = None
         #pdb.set_trace()
         resourceModel,created = ResourceModelDefinition.objects.get_or_create(id=layout['ItemId'],defaults=resourceModelDefaults)   
         
         if created:
            #pdb.set_trace() #Check for changes
            count_changes+=1
            
            rmdSave=False
            #create resourceLinks if needed
            if resourceLinks is not None:
               rlinks = ResourceLinks.objects.create(**resourceLinks)
               if rlinks:
                  resourceModel.ResourceLink = rlinks
                  rmdSave = True
            #update FieldName if needed
            if not custom_fields:
               field_name = resourceModel.FieldName+str(resourceModel.id)
               resourceModel.FieldName = field_name
               rmdSave = True
            if rmdSave:
               resourceModel.save()
         if resourceModel and not created:
            
            rmdValues = ResourceModelDefinition.objects.filter(id=layout['ItemId']).values()
            for rmdValue in rmdValues:
               changedRmd = CheckForChanges(rmdValue,resourceModelObject,resource_type_text,directive_name)
               #pdb.set_trace()
               if changedRmd:
                  #pdb.set_trace() #CheckForChanges
                  if layout['Status'] == 'Active':
                    if  resourceModel.DeployAction!='A':
                        resourceModelDefaults['DeployAction']='C'
                  else:
                     resourceModelDefaults['DeployAction']='D'
             
                  #insert into history
                  cRmdHistory = deepcopy(rmdValue)
                  cRmdHistory['ResourceModelDefinitionId_id'] = rmdValue['id'];
                  cRmdHistory['id']=None
                  rmdHistory = ResourceModelDefinitionHistory.objects.create(**cRmdHistory)
                  count_changes+=1
                  
                  if rmdHistory:
                       
                     if not custom_fields and resourceModel.FieldName=='Field':
                        field_name = resourceModel.FieldName+str(resourceModel.id)
                        resourceModel.FieldName = field_name
                     elif custom_fields:
                        resourceModel.FieldName = layout['FieldName']
                     
                     resourceModel.FieldType = layout['FieldType']
                     resourceModel.Dbtype = layout['Dbtype']
                     resourceModel.CodeId= layout['CodeId']
                     resourceModel.GenericType = layout['ElementType']
                     resourceModel.Unique = layout['Unique']
                     resourceModel.ChangedBy= current_user
                     resourceModel.ChangedDateTime=timezone.now()
                     resourceModel.RequiredField=  layout['Required']
                     resourceModel.MaxLength= layout['MaxLength']
                     resourceModel.Related= related
                     resourceModel.Version = new_version
                     resourceModel.Status = layout['Status']
                     resourceModel.DeployAction = resourceModelDefaults['DeployAction']
                     resourceModel.save()
               
               else:
                  
                  if not custom_fields and resourceModel.FieldName=='Field':
                     field_name = resourceModel.FieldName+str(resourceModel.id)
                     resourceModel.FieldName = field_name
                     resourceModel.Version = new_version
                     resourceModel.ChangedBy= current_user
                     resourceModel.ChangedDateTime=timezone.now()
                     resourceModel.save()
         
         resourceModelIds.append(resourceModel.id)
         layout['ItemId'] = resourceModel.id
         layout['Saved'] = True
         
         pe.ResourceModelDefinitionId = resourceModel
         pe.save()
    
    if layout['ElementType'] in ('add','view','update','rlist','mupdate','madd','itask','resource','resourcelink','init','exe','sendn','actionbtns','taskcontainer','integrationwidget'):
              #add dataset widget for update of widget links later
              if 'LocalId' in layout:
                  datasetWidgets.append({'localId':layout['LocalId'],'PresentationId': pe.id})
              
              #relatedResource=layout['Related']
         
              #save form
              taskForm = PresentationElementsLang.objects.get(id=layout['PresentationLangId'])
              
              #this must be separated - so that FormDef is properly updated
              if  layout['ElementType'] in ('resource','rlist'):
                taskForm.FormDef = layout['strtItemValue']
              else:
                taskForm.FormDef = layout['strwItemValue']
              
              taskForm.save()
              
              #Must move ProcessLayoutArray to Utility module - ToDo
              if layout['ElementType'] not in ('itask','resourcelink','actionbtns','taskcontainer'):
                if layout['wItemValue']:
                  
                  if layout['ElementType'] in ('mupdate','madd'):
                    retLayoutObjectW = ProcessLayoutArray('w','t',layout['wItemValue'][0],resource_id,org_id,lang,form_type,current_user,pe,peLang,layout['TabIndex'],parent_id,pe.id,layout['Related'],True,new_version,count_changes,custom_fields)
                  else:
                    retLayoutObjectW = ProcessLayoutArray('w','t',layout['wItemValue'],resource_id,org_id,lang,form_type,current_user,pe,peLang,layout['TabIndex'],parent_id,pe.id,layout['Related'],True,new_version,count_changes,custom_fields)
                  count_changes+=retLayoutObjectW['count_changes']
                
                if layout['fItemValue']:
                  retLayoutObjectF=ProcessLayoutArray('f','t',layout['fItemValue'],resource_id,org_id,lang,form_type,current_user,pe,peLang,layout['TabIndex'],parent_id,pe.id,layout['Related'],True,new_version,count_changes,custom_fields)
                  count_changes+=retLayoutObjectF['count_changes']
                
    #Must be separated as well - ToDo                      
    if layout['ElementType'] in ('files','uploader','view','update','mupdate','add','madd','sendn'):
            if 'tItemValue' in  layout:
               tItemValue = layout['tItemValue']
               if tItemValue is not None:
                widgetLink = {}
                
                for item in tItemValue:
                  
                  if item is not None:
                    
                    if item['widgetId'] is None:
                      if 'localWidgetId' in item:
                        widgetLink['localId'] = item['localWidgetId']
                        widgetLink['widgetId'] =item['widgetId']
                        widgetLink['dependentWidgetId'] = pe.id
                        widgetLink['dei'] =item['DEI']
                      else:
                        widgetLink['localId'] = None
                        widgetLink['widgetId'] =item['widgetId']
                        widgetLink['dependentWidgetId'] = pe.id
                        widgetLink['dei'] =item['DEI']
                    else:
                      widgetLink['localId'] = None
                      widgetLink['widgetId'] =item['widgetId']
                      widgetLink['dependentWidgetId'] = pe.id
                      widgetLink['dei'] =item['DEI']
                    
                    widgetLinks.append(widgetLink)     
               
       
    if 'layout' in (layout):
      retLayoutObjectTmp = ProcessLayoutArray(override_type,resource_type,layout['layout'],resource_id,org_id,lang,form_type,current_user,pe,peLang,layout['TabIndex'],pe.id,widget_id,related_dataset_id,False,new_version,count_changes,custom_fields)
      retLayoutObject['resourceModelIds'].extend(retLayoutObjectTmp['resourceModelIds'])
      retLayoutObject['datasetWidgets'].extend(retLayoutObjectTmp['datasetWidgets'])
      retLayoutObject['widgetLinks'].extend(retLayoutObjectTmp['widgetLinks'])
      retLayoutObject['new_items'].update(retLayoutObjectTmp['new_items'])
      count_changes+=retLayoutObjectTmp['count_changes']
      
      layout['layout']= retLayoutObjectTmp['layout']
    
    count+=1
    
    params['count'] = count
    layoutArr[index] = layout
  
  retLayoutObject['layout'] = layoutArr
  retLayoutObject['resourceModelIds'] = resourceModelIds
  retLayoutObject['datasetWidgets'] = datasetWidgets
  retLayoutObject['widgetLinks'] = widgetLinks
  retLayoutObject['count_changes'] = count_changes
  retLayoutObject['new_items'] = new_items
  #pdb.set_trace() 
  return retLayoutObject


class GetResourceDefSerializer(serializers.ModelSerializer):
   class Meta:
      model = ResourceDef
      fields = '__all__'
     
      
class ResourceDefByLangSerializer(serializers.ModelSerializer):
  
   resourcedeflang_set = ResourceDefLangSerializer(many=True)
   #Status = CodesDetailSerializer()
   #Status = GetCodesDetailByIdLangSerializer()   #MIGOR - Brza zamjena za CodesDetailSerializer (zagradeeee)
   ResourceType = ResourceParamsSerializer()
   class Meta:
      model = ResourceDef
      #fields = ('id','Organisation','Status','Private','ResourceType','ResourceDef')
      fields = ('id','Organisation','Active','Private','ResourceType','resourcedeflang_set')
      #depth=1      

class ResourceDefByLangFilteredSerializer(serializers.ModelSerializer):
  
   resourcedeflang_set = ResourceDefLangFilteredSerializer(many=True)
   Status = CodesDetailSerializer()
   ResourceType = ResourceParamsSerializer()
   class Meta:
      model = ResourceDef
      #fields = ('id','Organisation','Status','Private','ResourceType','ResourceDef')
      fields = ('id','Organisation','Status','Private','ResourceType','resourcedeflang_set')
      depth=1        


class MutantModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = None
        fields = '__all__'
        #depth = 2
    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally
        super(MutantModelSerializer, self).__init__(*args, **kwargs)
        
        #get dataset fields
        resource_id = kwargs['context']['request'].parser_context['kwargs'].pop('resourceId',None)
        if resource_id is not None:
          rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',MaxLength=1).exclude(Related_id=0).order_by('Related_id')
          for rmd in rmds:
            field_name = rmd.FieldName+'_id'
            
            self.fields[field_name] = serializers.IntegerField(read_only=True)
                
    #def create(self, validated_data):
        # Create the mutant instance
        
        #request = self.context['request']
        #kwargs_var = request.parser_context['kwargs']
        #if 'resourceId' in kwargs_var:
           #resource_id=int(kwargs_var['resourceId'])
        #else:
           #resource_id = None
        
        #mutantModel =  mutant_insert_data(validated_data,resource_id)
        
        
        #return validated_data   

class MutantModelRelatedSerializer(serializers.ModelSerializer):
    class Meta:
        model = None
        #fields = '__all__'
        #fields = ('id')
     
        #depth = 1

class RelatedResourceSerializer(serializers.Serializer):       
    M2MFieldId = serializers.IntegerField()
    ResourceSourceId = serializers.IntegerField()
    ResourceDestinationId = serializers.IntegerField()



class GetResourceDataSerializer(serializers.ModelSerializer):
    resource_data = serializers.SerializerMethodField('get_serialized_resource_data')
    
    class Meta:
        model = ResourceDef
        fields = ('id', 'resource_data')

        def get_serialized_resource_data(self, obj):
            content_type, pk = obj.target_content_type, obj.target_object_id
            
            if content_type and pk:
                model_class = content_type.model_class()
                try:
                  instance = model_class.objects.all()
                except model_class.DoesNotExist:
                  return None
                return GeneralModelSerializer(instance=instance,many=True).data
            else:
                return None


class  DatasetMappingHeadLangSerializer(serializers.ModelSerializer):
   Lang=LanguageSerializer()
   class Meta:
      model = DatasetMappingHeadLang
      fields = '__all__'
      
class GetDatasetMappingHeadSerializer(serializers.ModelSerializer):
    DatasetMappingHeadLang_datasetmappingheadid = DatasetMappingHeadLangSerializer(many=True)
    Dataset1 = ResourceDefByLangFilteredSerializer()
    Dataset2 = ResourceDefByLangFilteredSerializer()
    MappingType = CodesDetailSerializer()
    name = serializers.CharField(source="DatasetMappingHeadLang_datasetmappingheadid.first.Name")
    class Meta:
      model = DatasetMappingHead
      fields = '__all__'
      read_only_fields = ('name',)
      #fields = ('id','MappingName','Dataset1','Dataset2','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','DatasetMappingHeadLang_datasetmappingheadid')
      #depth = 1

class DatasetTransformHeadLangSerializer(serializers.ModelSerializer):

    class Meta:
      list_serializer_class = FilteredListSerializer
      model = DatasetTransformHeadLang
      fields = '__all__'
      #fields = ('id','MappingName','Dataset1','Dataset2','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','DatasetMappingHeadLang_datasetmappingheadid')
      #depth = 1

class GetDatasetTransformHeadSerializer(serializers.ModelSerializer):
    DatasetTransformHeadLang_transformationheadid = DatasetTransformHeadLangSerializer(many=True)
    FieldType1 = CodesDetailSerializer()
    FieldType2 = CodesDetailSerializer()
    class Meta:
      model = DatasetTransformHead
      fields = '__all__'
      #fields = ('id','MappingName','Dataset1','Dataset2','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','DatasetMappingHeadLang_datasetmappingheadid')
      #depth = 1
class PresentationElementsLangSerializer(serializers.ModelSerializer):
    
    class Meta:
      list_serializer_class = FilteredListSerializer
      model = PresentationElementsLang
      fields = '__all__'
      
class PresentationElementsSerializer(serializers.ModelSerializer):
    presentationelementslang_set = PresentationElementsLangSerializer(many=True)
   
    class Meta:
      model = PresentationElements
      fields = '__all__'
      #fields = ('id','MappingName','Dataset1','Dataset2','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','DatasetMappingHeadLang_datasetmappingheadid')
      #depth = 1
class GetResourceFieldsSerializer(serializers.ModelSerializer):
    PresentationElements_resourcemodeldefinitionid = PresentationElementsSerializer(many=True)
   
    class Meta:
      model = ResourceModelDefinition
      fields = '__all__'
      #fields = ('id','MappingName','Dataset1','Dataset2','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','DatasetMappingHeadLang_datasetmappingheadid')
      #depth = 1

class GetTransformDetailSerializer(serializers.ModelSerializer):
   
    class Meta:
      model = DatasetTransformDetail
      fields = '__all__'
      #fields = ('id','MappingName','Dataset1','Dataset2','CreatedBy','CreatedDateTime','ChangedBy','ChangedDateTime','DatasetMappingHeadLang_datasetmappingheadid')
      depth = 2  
class SaveDatasetMappingDetailSerializer(serializers.Serializer):
    
    id = serializers.IntegerField(required=False,allow_null=True)
    name = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=255)
    type = serializers.CharField(required=False,allow_null=True,allow_blank=True,max_length=255)
    fields = serializers.ListField(required=False)
    fields2 = serializers.ListField(required=False)
    
    def create(self, validated_data):
        # Create the ResourceDef instance
        
        try:
          user = self.context['request'].user
          mapping_id = self.context['mappingId']
          fields = validated_data.pop('fields')
          if 'fields2' in validated_data:
            fields2 = validated_data.pop('fields2')
          else:
            fields2=[]
            
            #fields.update(fields2)
          
          
          mappingDetail = None
        
          rec_type=1
        
          for field in fields:
            mappingDetail,Created = DatasetMappingDetail.objects.get_or_create(DatasetMappingHeadId_id=mapping_id,Field1_id=field['id'],Field2_id=validated_data['id'],RecType=rec_type,
                                       defaults={'TransformationId':None,'CreatedBy':user,'CreatedDateTime':timezone.now()})
            
            if mappingDetail and not Created:
              
              #mappingDetail.DatasetMappingHeadId = models.ForeignKey(DatasetMappingHead,related_name='DatasetMappingDetail_datasetmappingheadid')
              mappingDetail.Field1_id = field['id']
              mappingDetail.Field2_id = validated_data['id']
              mappingDetail.TransformationId =None
              mappingDetail.RecType =rec_type
          
              mappingDetail.ChangedBy = user
              mappingDetail.ChangedDateTime = timezone.now()
              mappingDetail.save()
              
          rec_type=2
        
          for field in fields2:
            mappingDetail,Created = DatasetMappingDetail.objects.get_or_create(DatasetMappingHeadId_id=mapping_id,Field1_id=field['id'],Field2_id=validated_data['id'],RecType=rec_type,
                                       defaults={'TransformationId':None,'CreatedBy':user,'CreatedDateTime':timezone.now()})
            
            if mappingDetail and not Created:
              
              #mappingDetail.DatasetMappingHeadId = models.ForeignKey(DatasetMappingHead,related_name='DatasetMappingDetail_datasetmappingheadid')
              mappingDetail.Field1_id = field['id']
              mappingDetail.Field2_id = validated_data['id']
              mappingDetail.TransformationId =None
              mappingDetail.RecType =rec_type
          
              mappingDetail.ChangedBy = user
              mappingDetail.ChangedDateTime = timezone.now()
              mappingDetail.save()
              
          return mappingDetail
        except Exception as exc:
          #raise Exception(exc) from exc
          raise exc#,sys.exc_info()
        

class SaveDatasetTransformDetailSerializer(serializers.Serializer):
    id = serializers.IntegerField(allow_null=True)
    TransformationHeadId = serializers.DictField(allow_null=True)
    TextItem1 = serializers.CharField(required=False,allow_blank=True, allow_null=True)
    TextItem2 = serializers.CharField(required=False,allow_blank=True, allow_null=True)
    CodeHead1 = serializers.DictField(required=False,allow_null=True)
    CodeHead2 = serializers.DictField(required=False,allow_null=True) 
    CodeDetail1 = serializers.DictField(required=False, allow_null=True)
    CodeDetail2 = serializers.DictField(required=False, allow_null=True)
    User1 = serializers.DictField(required=False,allow_null=True)
    User2 = serializers.DictField(required=False,allow_null=True)
    Group1 = serializers.DictField(required=False,allow_null=True)
    Group2 = serializers.DictField(required=False,allow_null=True)
  
    def create(self, validated_data):
        # Create the ResourceDef instance
        
        try:
          user = self.context['request'].user
          transform_id = self.context['transformId']
          
          #DatasetTransformDetail.objects.filter(DatasetTransformHeadId_id=transform_id).delete() #nije optimalno - kasnije optimizirati
          
          transformDetail = None
          
          
          TextItem1 = None
          if 'TextItem1' in validated_data:
           TextItem1=validated_data['TextItem1'] 
          
          TextItem2 = None
          if 'TextItem2' in validated_data:
           TextItem2=validated_data['TextItem2'] 
          
          CodeHead1 = None
          if 'CodeHead1' in validated_data:
            if validated_data['CodeHead1'] is not None:
              if 'id' in validated_data['CodeHead1']:
                CodeHead1=validated_data['CodeHead1']['id']
          
          CodeHead2 = None
          if 'CodeHead2' in validated_data:
            if  validated_data['CodeHead2'] is not None:
              if 'id' in validated_data['CodeHead2']:
                CodeHead2=validated_data['CodeHead2']['id']
          
          CodeDetail1 = None
          if 'CodeDetail1' in validated_data:
            if  validated_data['CodeDetail1'] is not None:
              if 'id' in validated_data['CodeDetail1']:
                CodeHead2=validated_data['CodeDetail1']['id']
          CodeDetail2 = None
          if 'CodeDetail2' in validated_data:
            if  validated_data['CodeDetail2'] is not None:
              if 'id' in validated_data['CodeDetail2']:
                CodeHead2=validated_data['CodeDetail2']['id']
              
          User1 = None
          if 'User1' in validated_data:
            if  validated_data['User1'] is not None:
              if 'id' in validated_data['User1']: 
                CodeHead2=validated_data['User1']['id']
          User2 = None
          if 'User2' in validated_data:
            if  validated_data['User2'] is not None:
              if 'id' in validated_data['User2']:
                User2=validated_data['User2']['id']
              
          Group1 = None
          if 'Group1' in validated_data:
            if  validated_data['Group1'] is not None:
              if 'id' in validated_data['Group1']:
                CodeHead2=validated_data['Group1']['id']
          Group2 = None
          if 'Group2' in validated_data:
            if  validated_data['Group2'] is not None:
              if 'id' in validated_data['Group2']:
                Group2=validated_data['Group2']['id']
          
          transformDetail,Created = DatasetTransformDetail.objects.get_or_create(id = validated_data['id'],
                                       defaults={'TransformationHeadId_id':transform_id,'TextItem1': TextItem1 ,'TextItem2': TextItem2 ,
                                                 'CodeHead1_id': CodeHead1 ,'CodeHead2_id': CodeHead2 ,'CodeDetail1_id':CodeDetail1,'CodeDetail2_id': CodeDetail1 ,
                                                 'User1_id': User1 ,'User2_id': User2 ,'Group1_id': Group1 ,'Group2_id': Group2 ,
                                                 'CreatedBy':user,'CreatedDateTime':timezone.now()})
          if transformDetail and not Created:
          
            transformDetail.TextItem1 =   TextItem1 
            transformDetail.TextItem2 =   TextItem2
            transformDetail.CodeHead1_id =   CodeHead1 
            transformDetail.CodeHead2_id =   CodeHead2 
            transformDetail.CodeDetail1_id =   CodeDetail1
            transformDetail.CodeDetail2_id =  CodeDetail2 
            transformDetail.User1_id =   User1 
            transformDetail.User2_id =  User2 
            transformDetail.Group1_id =   Group1 
            transformDetail.Group2_id =  Group2 
            transformDetail.ChangedBy = user
            transformDetail.ChangedDateTime = timezone.now()
            transformDetail.save()
          if Created:
            validated_data['id'] = transformDetail.id
            
          return validated_data
        
        except Exception as exc:
          #raise Exception(exc) from exc
          raise exc#,sys.exc_info()