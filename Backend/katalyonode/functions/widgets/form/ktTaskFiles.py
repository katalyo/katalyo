######################## ktTaskFiles processing module ###############################

###############################################################################

import pdb
#from katalyonode.models.models import PresentationElementsLang
from sqlalchemy import and_
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.resources.filedef import file
import json



class ktTaskFiles (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = False
        self.File = None
        
    def populate_definition_json(self):

        super().populate_definition_json()
        #pdb.set_trace()
        
        
        
        widget_object = {
        
        'AutoSearch':self.PresentationElementObject.AutoSearch,
        'ParametersLang':json.loads(self.PeLangObject.ParametersLang or '{}'),
        'FormDef':json.loads(self.PeLangObject.FormDef or '{}'),
        'GridState':json.loads(self.PeLangObject.GridState or '{}'),
        'ShowSearchForm':self.PresentationElementObject.ShowSearchForm,
        'PopulateMethod':self.PresentationElementObject.PopulateMethod,
        'HasParentDataset':self.PresentationElementObject.HasParentDataset,
        'PopulateParentMethod':self.PresentationElementObject.PopulateParentMethod,
        'PopulateTypeParent':self.PresentationElementObject.PopulateTypeParent,
        'ParentDatasetId':self.PresentationElementObject.ParentDatasetId,
        'SrcWidgetId':self.PresentationElementObject.SrcWidgetId,
        'SrcParentWidgetId':self.PresentationElementObject.SrcParentWidgetId,
        'PopulateMethod': self.PresentationElementObject.PopulateMethod,

        }
        self.WidgetDict.update(widget_object)



    def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'GridState' in self.NewItem:    
            self.PeLang.GridState= json.dumps(self.NewItem['GridState'])
        #if 'ResponseMsg' in self.NewItem:    
            #self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        #if 'GridState' in self.NewItem:    
            #self.PeLang.GridState=self.NewItem['GridState']

    def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'GridState' in self.NewItem:    
            self.PeLangDict['GridState']= json.dumps(self.NewItem['GridState'])

    def process_widget_execute(self):
    
               
        #self.Dataset = dataset(params['resource_id'],'dataset','','','','',params['user'],params['language'],params['org_id'],{})

        resource_id = self.ParentResource.id #params['file_def_id']
        presentation_id = self.id #params['presentation_id']
        user = self.ParentResource.User #params['user']
        language = self.ParentResource.Language
        paginate=False
        #pes = PresentationElements.objects.filter(id=presentation_id)

        #peLang = PresentationElementsLang.objects.filter(PresentationElementsId_id=presentation_id,Lang=language,PresentationElementsId__Status='Active')
        self.File = file(resource_id,'file','','','','',user,language,self.ParentResource.OrganisationId,{})
        self.File.set_session(self.ParentResource.Session)
        pes = self.ParentResource.Session.query(PresentationElementsSa,PresentationElementsLangSa).join(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==language.id)).filter(PresentationElementsSa.Status=='Active',PresentationElementsSa.id==presentation_id).all()
        
 
        p_element = None
        p_element_lang = None
        for pe in pes:
            p_element = pe[0]
            p_element_lang = pe[1]
        
        if p_element is None:
            raise Exception('Widget definition number '+(presentation_id)+' is not found.')
        #get form
        #check if something from here needs to come from the client
        
       
        parent_initiate_id = 0
        src_task_instance_id=0
        parent_initiate_id = presentation_id
        parent_id= 0
        populate_type=p_element.PopulateType
        resource_record_id = 0
        #self.Dataset.get_form_definition(form_type)
        #self.Dataset.generate_form() #GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)

        if type(p_element.Parameters)==str:
            parameters = json.loads(p_element.Parameters)
        else:
            parameters=None
        if parameters is not None:
            publish_id = parameters['PublishedResourceId']
        else:
           raise Exception('Published resource error --> not found id = '+str(publish_id))


        helper = ResourceHelperClass()

        published_resource = helper.get_published_resource(publish_id)

        if self.PeLangObject.FormDefSearch is not None:
            if type(self.PeLangObject.FormDefSearch)==str:
                search_form = json.loads(self.PeLangObject.FormDefSearch)
                formDefinition = search_form.get('SearchForm',[])
            else:
                formDefinition = self.PeLangObject.FormDefSearch.get('SearchForm',[])
        else:
            formDefinition = published_resource['Form']
            
        resourceDefinition = published_resource['Parameters']

        p_elements = published_resource['Definition']
        

        #GET DATA
        formData,recordId = self.ParentResource.get_dataset_form_data(p_element,p_elements,src_task_instance_id,parent_id,populate_type,resource_record_id)

        results = None
        selectedRecords=[]
        queryset=[]
        filter = None
        grid_state=json.loads(p_element_lang.GridState or '{}')
        
        columns = grid_state.get('columnsDef',[])
        columns_for_select = grid_state.get( 'columnsQuery',[])
        
        #if p_element.AutoSearch: #if auto search is checked then get data
        #TODO - uvesti zastitu ako je stavljen autosearch a nema filtera ... onda se cijela tablica i povezane skidaju !!
        rData,f,select_related, parameters = self.ParentResource.extract_form_data_search(formData,formDefinition)
        filter = f
                
        resourceData,m2mData = self.ParentResource.process_resource_data_search(rData)
        
        selectedRecords = self.File.get_dataset_data_filtered(resourceData,columns_for_select,select_related,parameters={})
       
        formDefinitionCard=None         
        response = {}
        response['resourceDefinition'] = resourceDefinition
        response['formDefinition'] = formDefinition
        response['formDefinitionCard'] = formDefinitionCard
        response['formData'] = formData
        response['columns'] = columns
        response['columnsForQuery'] = columns_for_select
        response['selectedRecords'] = selectedRecords
        response['data'] = selectedRecords
        #response['queryset'] = queryset
        response['org_id'] = self.ParentResource.OrganisationId
        response['filter'] = filter
        
        return response
