"""
ktGridColumn widget

form column widget

"""
from django.core.validators import ValidationError
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import pdb
import importlib
from sqlalchemy.sql import func
from sqlalchemy import and_
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa,ResourceDefSa,ResourceDefLangSa
from katalyonode.functions.rdbms.rdbms import RdbmsClass

rdbms_cls = RdbmsClass()

class ktGridColumn (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.layout = []
        self.CurrentIndex = 0
        self.HasResourceModel = False
    
    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {

       # 'Facss': 'fa fa-th-large',
        'SizeX': self.PresentationElementObject.SizeX,
        'ShowHeader': self.PresentationElementObject.ShowHeader,
        'layout': self.layout

        }
        self.WidgetDict.update(widget_object)

    def generate_form(self):
        
        #children = self.ParentResource.PresentationElements\
        #       .filter(PresentationElementsSa.ContainerElement == self.PresentationElementObject.UuidRef)
       
        for item in  self.ParentResource.PresentationElementsObjects: 
            if item[0].ContainerElement == self.PresentationElementObject.UuidRef:
                try:
                    module_path = 'katalyonode.functions.widgets.form.'+item[0].DirectiveName
                    module_ref = importlib.import_module(module_path)
                    widget_class = getattr(module_ref, item[0].DirectiveName)
                    widget_object = widget_class(item, self.ParentResource,self)
                    
                    if widget_object is not None:
                            widget_object.generate_form()
                            widgetJson=widget_object.get_definition_json() #column trace
                            self.layout.append(widgetJson)

                except ModuleNotFoundError:
                    #pdb.set_trace()
                    widget_object = WidgetBaseClass(item,self.ParentResource)
                    widget_json = widget_object.get_definition_json()
                    self.layout.append(widget_json)
    
    def save_definition(self):

        super().save_definition()
        #go through the form and save changes

        for newitem in self.NewItem.get('layout',[]):
            if newitem is None:
                Raise Exception("Column " +self.id +' contains null element!')
            self.increment_index()
            try:
                module_path = 'katalyonode.functions.widgets.form.'+newitem['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, newitem['DirectiveName'])
                widget_object = widget_class(None,self.ParentResource,self,newitem)
                
                if widget_object is not None:
                    #try:
                        widget_object.get_element_definition()
                        widget_object.save_definition()
                    #except AttributeError:
                        #raise ValidationError("AttributeError -->" + str(vars(widget_object)))

            except ModuleNotFoundError:
                #raise ValidationError("ModuleNotFoundError -->" + module_path)
                widget_object = WidgetBaseClass(None,self.ParentResource,self,newitem)
                widget_object.get_element_definition()
                widget_object.save_definition()

    def create_widget_css(self,uuidRefParent):

        #go through the form and generate_css
        children_css_style=''
        form_def = self.NewItem.get('layout',[])
        for item in form_def:
            try:
                module_path = 'katalyonode.functions.widgets.form.'+item['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, item['DirectiveName'])

                widget_object = widget_class(None,self.ParentResource,self,item)
               
                if widget_object is not None:
                    tmp_css = widget_object.create_widget_css(uuidRefParent)
                    if tmp_css is not None:
                        children_css_style+=tmp_css

                   
            except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError in generate_form_css-->" + module_path)

        element_style=super().create_widget_css(uuidRefParent)
        return children_css_style+element_style