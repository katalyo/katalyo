######################## ktTaskViewPdf processing module ###############################

###############################################################################

from django.conf import settings
from django.db import models
import pdb
from katalyonode.models.models import ResourceDefLang,UserExtendedProperties,PresentationElements,ErrorLog
from katalyonode.functions.formprocessing import GetResourceFormDB,ExtendResourceFormJSON,GetDatasetFormData,ExtractFormDataSearch,ProcessResourceDataSearch
from katalyonode.functions.dynamicmodels import mutant_get_model
from katalyonode.functions.datasets import mutant_get_data_filtered,mutant_get_data_filtered_sr,get_columns_def
from katalyonode.views.viewsresources import GetDatasetDataFilteredPaginatedFast
from rest_framework.pagination import PageNumberPagination
from rest_framework import viewsets
from rest_framework.response import Response
from django.utils import timezone
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10000
    
    def set_page_size(ps):
      page_size = ps
    
    def get_paginated_response(self, data):
   
      return {
        "count": self.page.paginator.count,
        "countItemsOnPage": self.page_size,
        'next': self.get_next_link(),
        'previous': self.get_previous_link(),
        'results': data
      }
    
class ktTaskViewPdfViewSet(viewsets.GenericViewSet):
    
    queryset = ResourceDefLang.objects.all()
     
    def list(self, request,resourceId,presentationId,taskDefId,formType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        
        if 'presentationId' in self.kwargs:
           presentation_id=int(self.kwargs['presentationId'])
        else:
           presentation_id = None
        if 'taskDefId' in self.kwargs:
           task_def_id=int(self.kwargs['taskDefId'])
        else:
           task_def_id = None
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = 'en-GB' 
        
        user=request.user
        params={}
        params['presentation_id'] = presentation_id
        params['resource_id'] = resource_id 
        params['form_type'] = form_type 
        params['language'] = language  
        params['task_def_id'] = task_def_id
        params['user'] = user
        #params['paginate'] = True
        #params['viewset'] = self
       
        response = ProcessWidgetExecute(params)
        #page = self.paginate_queryset(response['queryset'])
        
        #page = response['queryset']
        
        #selectedRecords = GetDatasetDataFilteredPaginatedFast(response['filter'],user,resource_id,response['org_id'],page,None,language,None)
        
        #if page is not None:
            #results =  self.get_paginated_response(selectedRecords)
        #else:
        results = response['selectedRecords']
           
        return Response({'resourceDefinition':response['resourceDefinition'],'widgets':response['formDefinition'],'widgetsCard':response['formDefinitionCard'],'formData':response['formData'],'columns':response['columns'],'data':results})



def ProcessWidgetExecute(params):
    
    if 'paginate' in params:
        paginate = params['paginate']
    else:
        paginate = False
    
    resource_id = params['resource_id']
    presentation_id = params['presentation_id']
    form_type = params['form_type']
    language = params['language']
    task_def_id = params['task_def_id']
    user = params['user']
    
    ueps = UserExtendedProperties.objects.filter(UserId_id=user.id)
    org_id=None
    for uep in ueps:
        org_id = uep.Organisation_id
        if language is None:
            language = uep.Lang     
   
    #get presentation elements
    pes = PresentationElements.objects.filter(id=presentation_id)

    p_element = None
    for pe in pes:
        p_element = pe
    
    if p_element is None:
        return Response(data='Widget definition number '+(presentation_id)+' is not found.',status=500)
    #get form
    #check if something from here needs to come from the client
    
    override_type = "f"
    parent_initiate_id = 0
    src_task_instance_id=0
    parent_initiate_id = presentation_id
    parent_id= 0
    populate_type=p_element.PopulateType
    resource_record_id = 0
    formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
    #get data
    formData,recordId = GetDatasetFormData(resource_id,form_type,override_type,task_def_id,src_task_instance_id,parent_id,populate_type,resource_record_id,org_id,user)
    
    formDefinitionCard=None
    #if presentation-type=card get card layout
    if p_element.Parameters is not None:
        if p_element.Parameters['PresentationType']=="card":
            override_type = "c"
            formDefinitionCard,resourceDefinitionCard = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
            
    
    #process from
    results = None
    selectedRecords=[]
    queryset=[]
    filter = None
    columns = get_columns_def(resource_id,task_def_id,form_type,org_id,language)
    #if auto search is checked then get data
    if p_element.AutoSearch:
        
        rData,f,select_related,parameters = ExtractFormDataSearch(formData,formDefinition,user)
        filter = f
                
        resourceData,m2mData = ProcessResourceDataSearch(rData,resource_id,org_id)
     
        queryset =  mutant_get_data_filtered_sr(resource_id,org_id,resourceData,select_related, parameters={})
        
        #pdb.set_trace()
        if not paginate:
            selectedRecords = GetDatasetDataFilteredPaginatedFast(filter,user,resource_id,org_id,None,queryset,language,None)
    
        ErrorLog.objects.create(Error1={'rdata':rData,'resourceData':resourceData,'count':len(queryset)},Error2={'formDef':formDefinition,'m2m':m2mData,'formData':formData,'filter':filter},CreatedDateTime=timezone.now()) 
            
    response = {}
    response['resourceDefinition'] = resourceDefinition
    response['formDefinition'] = formDefinition
    response['formDefinitionCard'] = formDefinitionCard
    response['formData'] = formData
    response['columns'] = columns
    response['selectedRecords'] = selectedRecords
    response['queryset'] = queryset
    response['org_id'] = org_id
    response['filter'] = filter
    
    return response


def ProcessElementDefinition(layout,params):
    
    #raise Exception('ProcessElementDefinition fn TEST exception raised')

    #get presentation elements record
    layoutLocal = layout['layout']
    if layoutLocal['Related'] is None and layoutLocal['Status']=='Active':
        raise ValueError('Dataset list record (Widget id = '+str(layoutLocal['PresentationId'])+') - error in definition  --> Dataset must be selected ')
    
    if layoutLocal['PresentationId'] is not None:
        
        pe = PresentationElements.objects.get(id=layoutLocal['PresentationId'])
        if not pe:
            raise ValueError('Dataset list - element definition not found --> id = '+str(layoutLocal['PresentationId']))
        else:
            if layoutLocal['Related'] != pe.Related_id: 
                updateStatus = SetResourceStatus(params['resource_id'],params['form_type'],params['override_type'],layoutLocal['PresentationId'],params['current_user'],params['new_version'],'NotActive')
                if not updateStatus:
                     raise ValueError('Dataset list widget - update status failed --> id = '+str(layoutLocal['PresentationId']))    
    
    return layout

def CheckForChanges(item_dict,new_item):
    has_changes=False
    #pdb.set_trace()
    if 'FormDef' in item_dict and 'wItemValue' in new_item:
        if item_dict['FormDef']!=new_item['wItemValue']:
            has_changes=True
    return has_changes

class ktTaskViewPdf (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)