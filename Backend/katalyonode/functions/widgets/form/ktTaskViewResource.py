######################## ktTaskViewResource processing module ###############################

###############################################################################

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
#from katalyonode.models.models import TaskInitiationInstanceToResource,TaskExecutionInstanceToResource
from katalyonode.functions.resources.datasetdef import dataset
from katalyonode.functions.rdbms.alchemymodels import PublishedResourcesSa,TaskInitiationInstanceToResourceSa
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,TaskExecutionInstanceToResourceSa
from datetime import datetime,timezone
import json
import pdb


class ktTaskViewResource (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.HasResourceModel = False
    
  def populate_definition_json(self):

    super().populate_definition_json()

    widget_object = {
    
     'PopulateType': self.PresentationElementObject.PopulateType,
     'PopulateMethod': self.PresentationElementObject.PopulateMethod,
     'Related':self.PresentationElementObject.Related,
     'ParametersLang':json.loads(self.PeLangObject.ParametersLang or '{}'),
     'FormDef':json.loads(self.PeLangObject.FormDef or '[]'),
     'GridState':json.loads(self.PeLangObject.GridState or '{}'),

    }
    self.WidgetDict.update(widget_object)

  def process_widget(self,widget_data,p_element,pe_lang,task_instance_id):
    return {}

  def pre_process_widget(self,widget_id,task_instance_id,form_type,dataset_def_id,dataset_record_id):
  
    parentDatasetDefId = None
    parentDatasetId = None

    self.Dataset = dataset(dataset_def_id,'dataset','','','','',self.ParentResource.User,self.ParentResource.Language,self.ParentResource.OrganisationId,{})
    self.Dataset.set_session(self.ParentResource.Session)
    #get UuidRef
    uuid_ref = None
          
    peFields = self.ParentResource.Session.query(PresentationElementsSa).filter(PresentationElementsSa.id==widget_id).all()
        
    for pe in peFields:
      uuid_ref = pe.UuidRef
    
    #session = self.Dataset.rdbms_cls.Session()
    #publish = session.query(PublishedResources).filter(PublishedResources.id==publishId).all()
    
    #rmds = None
    #resource_params = None
    #widget_data=None
    #for resource in publish:
      #widget_data = json.loads(resource.Form)
      #rmds = json.loads(resource.Definition)
      #resource_params = json.loads(resource.Parameters)

    #mainResource,m2mData = self.Dataset.generate_query(self.Dataset.process_form_data(widget_data,rmds))
    
    #resource_rec_id=mainResource.get('id',0)
    #pdb.set_trace()
    ret_value=None
    resource_ids=[]
    filter_data={}
    #resource_ids.append(dataset_record_id)
    id_key = 'resource'+str(self.Dataset.id)+'_'+str(self.ParentResource.OrganisationId)+'_id'
    
    if dataset_record_id==0:
      if form_type=='e':
        #teitrs=TaskExecutionInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskExecuteId_id = task_instance_id,
        #                                              PresentationElementUuidRef =uuid_ref)
        teitrs=self.ParentResource.Session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskExecutionInstanceToResourceSa.TaskExecuteId == task_instance_id,
                                                      TaskExecutionInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
              
                
      else:
        #teitrs=TaskInitiationInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskInitiateId_id = task_instance_id,
        #                                             PresentationElementUuidRef =uuid_ref)
        
        teitrs=self.ParentResource.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskInitiationInstanceToResourceSa.TaskInitiateId == task_instance_id,
                                                      TaskInitiationInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
          
      if len(teitrs)>0:

        dataset_record_id = teitrs[0].ResourceId

      else:

        raise Exception('Record not found for widget = '+str(widget_id))      
        
    
    filter_data[id_key]=dataset_record_id    
    #records = self.Dataset.get_data_filtered(self.Dataset.id,self.ParentResource.OrganisationId,filter_data,None)
    records = self.Dataset.get_dataset_data_filtered(filter_data,[],[],parameters={})
    #records = self.Dataset.rdbms_cls.get_data_filtered_in(self.Dataset.id,self.Dataset.OrganisationId,resource_ids) 
    record=None
    for record in records:
      #if record and len(m2mData)>0:
          #mutantm2m =  mutant_get_data_m2m(relatedId,record.id,org_id,m2mData)
      if record is not None:
      
        #insert TaskInitiationInstanceToResource
        if form_type=='e':
          #teitrs=TaskExecutionInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskExecuteId_id = task_instance_id,
          #                                            PresentationElementUuidRef =uuid_ref)
          
          teitrs=self.ParentResource.Session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskExecutionInstanceToResourceSa.TaskExecuteId == task_instance_id,
                                                      TaskExecutionInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
            
          if len(teitrs)==0:
              
            #teitr_rec=TaskExecutionInstanceToResource.objects.create( ResourceDefId_id = self.Dataset.id, TaskExecuteId_id = task_instance_id, ResourceId = record[id_key],
            #                                          CreatedBy = self.Dataset.User,CreatedDateTime=timezone.now(), PresentationElementUuidRef =uuid_ref)
            teitr_rec=TaskExecutionInstanceToResourceSa(ResourceDefId = self.Dataset.id, TaskExecuteId = task_instance_id, ResourceId = record[id_key],PresentationElementUuidRef =uuid_ref,
                                                               CreatedBy = self.Dataset.User.id, CreatedDateTime=datetime.now(timezone.utc))
            self.ParentResource.Session.add(teitr_rec)
          else:
            for teitr in teitrs:
              teitr.ChangedBy=self.Dataset.User.id
              teitr.ChangedDateTime=datetime.now(timezone.utc)
              teitr.ResourceId = record[id_key]
              teitr_rec = teitr
                
        else:
          #teitrs=TaskInitiationInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskInitiateId_id = task_instance_id,
          #                                            PresentationElementUuidRef =uuid_ref)
          
          teitrs=self.ParentResource.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskInitiationInstanceToResourceSa.TaskInitiateId == task_instance_id,
                                                      TaskInitiationInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
          
          if len(teitrs)==0:
              
            #teitr_rec=TaskInitiationInstanceToResource.objects.create( ResourceDefId = self.Dataset.id, TaskInitiateId_id = task_instance_id,PresentationElementUuidRef =uuid_ref, ResourceId = record[id_key],
            #                                          CreatedBy = self.Dataset.User,CreatedDateTime=timezone.now())
            teitr_rec = TaskInitiationInstanceToResourceSa(ResourceDefId = self.Dataset.id, TaskInitiateId = task_instance_id, ResourceId = record[id_key],PresentationElementUuidRef =uuid_ref,
                                                               CreatedBy = self.Dataset.User.id, CreatedDateTime=datetime.now(timezone.utc))
            self.ParentResource.Session.add(teitr_rec)
          else:
            for teitr in teitrs:
              teitr.ChangedBy=self.Dataset.User.id
              teitr.ChangedDateTime=datetime.now(timezone.utc)
              teitr.ResourceId = record[id_key]
              teitr_rec = teitr
       
        
      ret_value = {'widgetId':widget_id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':self.Dataset.id},{'name':'resourceRecordId','value':record[id_key]}]}],'taskResourceLink': teitr_rec.id}

    return {'ret_value':ret_value,'data':record} 

  def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLang.FormDefSearch= json.dumps(self.NewItem['FormDefSearch'])
        #if 'ResponseMsg' in self.NewItem:    
            #self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        #if 'GridState' in self.NewItem:    
            #self.PeLang.GridState=self.NewItem['GridState']

  def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLangDict['FormDefSearch']= json.dumps(self.NewItem['FormDefSearch'])

  def represents_int(self,s):
      try: 
          int(s)
          return True
      except ValueError:
          return False