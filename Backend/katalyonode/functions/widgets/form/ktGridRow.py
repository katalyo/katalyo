"""
ktGridRow widget

row widget

"""
from django.core.validators import ValidationError
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import pdb
import importlib

import os
from katalyonode.settings import PUBLISH_DIR
from katalyonode.functions.resources.datasetdef import dataset
from sqlalchemy.sql import func
from sqlalchemy import and_
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa,ResourceDefSa,ResourceDefLangSa
from katalyonode.functions.rdbms.rdbms import RdbmsClass

rdbms_cls = RdbmsClass()


class ktGridRow (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.layout = []
        self.CurrentIndex = 0
        self.HasResourceModel = False
        self.GenerateCss = True
        
    def set_value(self,l_value):
    
            
        self.__value = l_value
    
    def get_value(self):
    
        return self.__value

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {

        #'Facss': 'fa fa-th-large',
        'ShowHeader': self.PresentationElementObject.ShowHeader,
        'layout': self.layout
        }
        self.WidgetDict.update(widget_object)


    def generate_form(self):

        children = self.get_children() 
        self.layout = children

    def  get_children(self):
        
        widgets = []

        #children = self.ParentResource.PresentationElements\
        #       .filter(PresentationElementsSa.ContainerElement == self.PresentationElementObject.UuidRef)
        #children = self.ParentResource.PresentationElements.filter(ContainerElement=self.PresentationElementObject.UuidRef).all() # performanse??
         
        #for child in children:
        for child in self.ParentResource.PresentationElementsObjects:
            if child[0].ContainerElement == self.PresentationElementObject.UuidRef:
                try:
                    module_path = 'katalyonode.functions.widgets.form.'+child[0].DirectiveName
                    module_ref = importlib.import_module(module_path)
                    widget_class = getattr(module_ref, child[0].DirectiveName)
                    widget_object = widget_class(child, self.ParentResource,self)
                    
                    if widget_object is not None:
                            widget_object.generate_form()
                            widgetJson=widget_object.get_definition_json() # row trace
                            widgets.append(widgetJson)

                except ModuleNotFoundError:
                    raise ValidationError("ModuleNotFoundError -->" + module_path)
        
        return widgets

    def save_definition(self):

        super().save_definition()
        #go through the form and save changes
        
        for item in self.NewItem['layout']:

            self.increment_index()
            try:
                module_path = 'katalyonode.functions.widgets.form.'+item['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, item['DirectiveName'])
                widget_object = widget_class(None,self.ParentResource,self,item)
                
                if widget_object is not None:
                    #try:
                        widget_object.get_element_definition()
                        widget_object.save_definition()
                    #except AttributeError:
                        #raise ValidationError("AttributeError -->" + str(vars(widget_object)))

            except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError -->" + module_path)

    def check_changes_new(self):
        
        has_changes=False
        changes_detail=''
        old_item = self.get_definition_json()
        new_item = self.NewItem
        #check fields for changes
        OLD=0  
        NEW=1
        changes_detail=''
        fields = [('ReadOnly','ReadOnly'),('ShowField','ShowField'),('RequiredField','RequiredField'),('MaxLength','MaxLength'),('Label','Label')]
    

        for field in fields:
               has_changes =  self.check_equal(field[OLD],field[NEW],old_item,new_item)
               if (has_changes):
                    break

        return has_changes

    def create_widget_css(self,uuidRefParent=None):

        #go through the form and generate_css

        self.UuidRef = self.NewItem.get('UuidRef',None)
        if uuidRefParent is None and self.UuidRef is not None:
            uuidRefParent = self.UuidRef
        
        element_style='.element-style-'+str(uuidRefParent)+'-'+self.NewItem['ElementType']+'-'+str(self.UuidRef)+'{ background-color:'+self.NewItem['Parameters'].get('BackgroundColor','transparent')+'}'
        children_css_style=''
        if self.NewItem.get('layout',None) is not None:
            form_def = self.NewItem.get('layout',[])
            #pdb.set_trace()
            if len(form_def)>0:
                children_css_style = self.ParentResource.create_widget_css(form_def,uuidRefParent)

        return element_style+children_css_style