######################## ktUploadFile processing module ###############################

###############################################################################

import pdb
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import importlib,json
#from django.utils import timezone
from datetime import datetime,timezone
#from katalyonode.models.models import PublishedResources,TaskInitiationInstanceToResource,TaskExecutionInstanceToResource
from katalyonode.functions.rdbms.alchemymodels import PublishedResourcesSa,TaskInitiationInstanceToResourceSa,TaskExecutionInstanceToResourceSa
from django.conf import settings

class ktUploadFile (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.HasResourceModel = False
    
  def populate_definition_json(self):

    super().populate_definition_json()
    widget_object = {
        'PopulateType': self.PresentationElementObject.PopulateType,
        'PopulateMethod': self.PresentationElementObject.PopulateMethod,
        'Related':self.PresentationElementObject.Related,
        'HasParentDataset':self.PresentationElementObject.HasParentDataset,
        'PopulateParentMethod':self.PresentationElementObject.PopulateParentMethod,
        'PopulateTypeParent':self.PresentationElementObject.PopulateTypeParent,
        'ParentDatasetId':self.PresentationElementObject.ParentDatasetId,
        'SrcWidgetId':self.PresentationElementObject.SrcWidgetId,
        'SrcParentWidgetId':self.PresentationElementObject.SrcParentWidgetId,
        'ParametersLang':json.loads(self.PeLangObject.ParametersLang or '{}'),
        'ResponseMsg':self.PeLangObject.ResponseMsg,
    }
    self.WidgetDict.update(widget_object)


  def update_meta_data(self,widget_data,task_instance_id,prev_task_id,parent_dataset_id,parent_dataset_record_id,publish_id):
  
        parentDatasetDefId = parent_dataset_record_id
        parentDatasetId = parent_dataset_record_id

        if parentDatasetDefId==0:
          parentDatasetDefId=None
        if parentDatasetId==0:
          parentDatasetId=None

        #publish = PublishedResources.objects.filter(id=publish_id)
        publish = self.ParentResource.Session.get(PublishedResourcesSa,publish_id)
        rmd = None
        resource_params = None
        resource_id = None
        if publish is not None:
          rmds = json.loads(publish.Definition)
          resource_params = json.loads(publish.Parameters)

        mainResource,m2mData = self.ParentResource.generate_query(self.ParentResource.process_form_data(widget_data,rmds))
        status,record = self.ParentResource.rdbms_cls.update_data(mainResource,self.ParentResource.id,self.ParentResource.OrganisationId,self.ParentResource.User.id)
        
        if record and len(m2mData)>0:
            mutantm2m =  self.ParentResource.rdbms_cls.insert_data_m2m_v2(record.id,self.ParentResource.OrganisationId,m2mData,self.ParentResource.User.id,False)

        jsonResponseBChain=None
        jsonResponseBChain2=None

        if resource_params and settings.USE_BLOCKCHAIN:
          if resource_params['IsBlockchain']:
          #add record to blockchain
           
            datasetData = json.dumps(widget_data)
            h =  hashlib.sha512(datasetData.encode()).hexdigest()
            user = 'user'
            p_key=int(str(self.ParentResourceid)+str(record.id).zfill(12))
            trx_json = {'pKey':p_key,'datasetDef_Id':self.ParentResource.id,'datasetRec_Id':record.id,'dataset_Data': datasetData,'h':h,'v':0,'user':self.ParentResource.User.id}
            contract = 'kataly1.code'
            action='createds'
            
            jsonResponseBChain,block_id,status = self.send_transaction(trx_json,contract,action)
       
            bchain_id= int(jsonResponseEOS['processed']['action_traces'][0]['console'])
       
            jsonResponseBChain2,status2 = self.get_table_rows('kataly1.code','dataset','kataly1.code','primary','uint64_t',bchain_id)
       
            #save transaction id
            if status==200:
              mainResource['id'] = record.id
              mainResource['TrxId'] = jsonResponseEOS['transaction_id']
              mainResource['BChainRecId'] = bchain_id
              mainResource['BlockId'] = str(block_id)
              result = self.rdbms_cls.update_data(mainResource,resource_id,self.OrganisationId,self.User.id) 
       
        if self.PresentationElementObject.HasParentDataset:
          if self.PresentationElementObject.PopulateTypeParent!='' and record is not None:
              #if (int(element['parentDatasetId'])>0):
                parentDatasetDefId,parentDatasetId = self.ParentResource.process_parent_dataset(self.PresentationElementObject,widget_data,self.ParentResource.id,prev_task_id,task_instance_id,record,parentDatasetDefId,parentDatasetId)
        
        if record is not None:

          if self.ParentResource.FormType=='i':
            #titr=TaskInitiationInstanceToResource.objects.create( ResourceDefId_id = self.ParentResource.id, TaskInitiateId_id = task_instance_id, ResourceId = record.id,ParentDatasetDefId_id = parentDatasetDefId,
            #                                                   ParentDatasetId = parentDatasetId,PresentationElementId_id =self.PresentationElementObject.id ,
            #                                                   CreatedBy_id = self.ParentResource.User.id, CreatedDateTime=timezone.now())
            titr = TaskInitiationInstanceToResourceSa(ResourceDefId = self.ParentResource.id, TaskInitiateId = task_instance_id, ResourceId = record.id,ParentDatasetDefId = parentDatasetDefId,
                                                               ParentDatasetId = parentDatasetId,PresentationElementUuidRef =self.PresentationElementObject.UuidRef,
                                                               CreatedBy = self.ParentResource.User.id, CreatedDateTime=datetime.now(timezone.utc))
            self.ParentResource.Session.add(titr)
          else:
            #titr=TaskExecutionInstanceToResource.objects.create( ResourceDefId_id = self.ParentResource.id, TaskExecuteId_id = task_instance_id, ResourceId = record.id,ParentDatasetDefId_id = parentDatasetDefId,
            #                                        ParentDatasetId = parentDatasetId,PresentationElementId_id =self.PresentationElementObject.id , CreatedBy_id = self.ParentResource.User.id, CreatedDateTime=timezone.now())

            titr = TaskExecutionInstanceToResourceSa(ResourceDefId = self.ParentResource.id, TaskExecuteId = task_instance_id, ResourceId = record.id,ParentDatasetDefId = parentDatasetDefId,
                                                               ParentDatasetId = parentDatasetId,PresentationElementUuidRef =self.PresentationElementObject.UuidRef ,
                                                               CreatedBy = self.ParentResource.User.id, CreatedDateTime=datetime.now(timezone.utc))
            self.ParentResource.Session.add(titr)
          ret_value = {'widgetId':self.PresentationElementObject.id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':self.ParentResource.id},{'name':'resourceRecordId','value':record.id}]}],'bChainResponse':jsonResponseBChain,'bChainResponse2':jsonResponseBChain2,'taskResourceLink': titr.id}
        
        else:
          ret_value = {'widgetId':self.PresentationElementObject.id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':self.ParentResource.id},{'name':'resourceRecordId','value':0}]}],'bChainResponse':jsonResponseBChain,'bChainResponse2':jsonResponseBChain2,'taskResourceLink':0}

        if self.PeLangObject.ResponseMsg is not None and self.PeLangObject.ResponseMsg!='':
          response_msg = self.process_response_msg(record,self.PeLangObject.ResponseMsg)
          return {'ret_value':ret_value,'record':record.id,'ResponseMsg':response_msg}

        return {'ret_value':ret_value,'record':record.id}

  def process_response_msg(self,record,responseTemplate):
    
      responseMsg=responseTemplate
      for element_key,element_val in record.items():
        if element_val is not None:
          old_str = '<<'+str(element_key)+'>>'
          responseMsg = responseMsg.replace(old_str,str(element_val))
      return responseMsg