######################## ktResource processing module ###############################

###############################################################################
import pdb
import json
#from katalyonode.functions.taskprocessing import StartTask
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from katalyonode.models.models import PresentationElements,UserExtendedProperties
from django.utils import timezone
import importlib

class ktResourceid (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)

    def prepare_resource_model_insert(self):

        super().prepare_resource_model_insert()
        self.ResourceModelDict['FieldName'] = 'resource'+str(self.ParentResource.id)+'_'+str(self.ParentResource.OrganisationId)+'_id'

    def get_element_value(self,pe,rmd,item,params):
        
        field_name = rmd['FieldName']
        value = getattr(item,field_name,None)
        if value is None:
            field_name = 'id'
            value = getattr(item,field_name,None)
        return value

    def process_widget_search(self,item,filter_data):

        fieldId = 'id'
        r_val=None
        if fieldId in filter_data:
            value = filter_data[fieldId]
            if value!=None and value!='':
                r_val = {'type':'resourceId', 'value': s_item_value}

        return r_val,filter_data,[],{}