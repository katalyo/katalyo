######################## ktList processing module ###############################

###############################################################################
import pdb
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass

class ktHeader(WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = False

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {
        
        'ResponseMsg':self.PeLangObject.ResponseMsg,

        }
        self.WidgetDict.update(widget_object)

    def generate_css_classes(self,uuidRefParent):
        
        element_style=self.generate_css_styles(['padding','margin','border','rounded','bgcolor','shadow'])
        #font size
        css_style = self.NewItem.get('Parameters',{}).get('FontSize',None)
        if css_style is not None:
            element_style+='font-size:'+str(css_style)+self.NewItem.get('Parameters',{}).get('FontUnit','rem')+';'

        #rounded borders
        css_style = self.NewItem.get('Parameters',{}).get('FontColor',None)
        if css_style is not None:
            element_style+='color:'+css_style+';'

        css_style = self.NewItem.get('Parameters',{}).get('TextAlign',None)
        if css_style is not None:
            element_style+='text-align:'+css_style+';'

        element_css=''
        if element_style!='':
            element_css='.element-style-'+uuidRefParent+'-'+self.NewItem.get('ElementType','default')+'-'+self.NewItem.get('UuidRef','no-uuidref')+'{'+element_style+'}'

        return element_css

