######################## ktTaskDatasetList processing module ###############################

###############################################################################

from django.conf import settings
from django.db import models
import pdb
from katalyonode.models.models import ResourceDefLang,PresentationElements,ErrorLog
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa,ErrorLogSa
from sqlalchemy import and_
from rest_framework.pagination import PageNumberPagination
from rest_framework import viewsets
from rest_framework.response import Response
import importlib
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.resources.datasetdef import dataset
import json
import datetime
from katalyonode.functions.rdbms.rdbms import RdbmsClass

rdbms_cls = RdbmsClass()

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10000
    
    def set_page_size(ps):
      page_size = ps
    
    def get_paginated_response(self, data):
   
      return {
        "count": self.page.paginator.count,
        "countItemsOnPage": self.page_size,
        'next': self.get_next_link(),
        'previous': self.get_previous_link(),
        'results': data
      }
    
class ktTaskDatasetListViewSet(viewsets.GenericViewSet):
    
    #pagination_class = StandardResultsSetPagination
    queryset = ResourceDefLang.objects.all()
     
    def list(self, request,resourceId,presentationId,taskDefId):
        

        #ToDo this should be part of the pre_execute task process

        params={}
        params['presentation_id'] = int(self.kwargs['presentationId'])  if 'presentationId' in self.kwargs else None
        params['id'] = params['presentation_id']
        params['resource_id'] = int(self.kwargs['resourceId']) if 'resourceId' in self.kwargs else None 
        params['task_def_id'] =  int(self.kwargs['taskDefId']) if 'taskDefId' in self.kwargs else None       
        params['user'] = request.user
        
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (params['user']) 
        params['language'] = language
        params['org_id'] = org_id
        #create element here
        #pdb.set_trace()
        
        response = {}
        results=None
        session = rdbms_cls.Session()
        with session.begin():
            #pdb.set_trace()
            ktDatasetList = ktTaskDatasetList(params,None,None,None,session)
            response = ktDatasetList.process_widget_execute(params,session)
        
            results = response['selectedRecords']
           
        return Response(  
            {'resourceDefinition':response.get('resourceDefinition',None),
            'widgets':response.get('formDefinition',None),
            'widgetsCard':response.get('formDefinitionCard',None),
            'formData':response.get('formData',None),
            'columns':response.get('columns',None),
            'columnsForQuery':response.get('columnsForQuery',None),
            'analytics': response.get('analytics',None),
            'data':results} )

    def create(self, request,resourceId,presentationId,taskDefId):
        params={}
        
        params['presentation_id'] = int(self.kwargs['presentationId'])  if 'presentationId' in self.kwargs else None
        params['id'] = params['presentation_id']
        params['resource_id'] = int(self.kwargs['resourceId']) if 'resourceId' in self.kwargs else None 
        params['task_def_id'] =  int(self.kwargs['taskDefId']) if 'taskDefId' in self.kwargs else None       
        params['user'] = request.user
        
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (params['user']) 
        params['language'] = language
        params['org_id'] = org_id
        
        form_def = request.data.get('form',{})
        search_filter = request.data.get('filter',{})
        if type(search_filter)==list:
            if len(search_filter)>0:
                search_filter=search_filter[0]
        columns = request.data.get('columns',[])
        response = {}
        results=None
        session = rdbms_cls.Session()
        with session.begin():

            ktDatasetList = ktTaskDatasetList(params,None,None,None,session)
    
            response = ktDatasetList.process_widget_search(params,form_def,search_filter,columns,session)

            results = response.get('selectedRecords',None)
        
        return Response(  
            {'columns':response.get('columns',None),
            'columnsForQuery':response.get('columnsForQuery',None),
            'analytics': response.get('analytics',None),
            'data':results} )

class ktTaskDatasetList (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None,session=None):
        #pdb.set_trace()
        super().__init__(item,parent_resource,parent_widget,new_item,session)
        self.HasResourceModel = False
        self.Dataset = None

    def populate_definition_json(self):

        super().populate_definition_json()
        #pdb.set_trace()
           
        widget_object = {
        
        'AutoSearch':self.PresentationElementObject.AutoSearch,
        'ParametersLang':json.loads(self.PeLangObject.ParametersLang or '{}'),
        'FormDef':json.loads(self.PeLangObject.FormDef or '[]'),
        'FormDefSearch':json.loads(self.PeLangObject.FormDefSearch or '{}'),
        'GridState':json.loads(self.PeLangObject.GridState or '{}'),
        'ShowSearchForm':self.PresentationElementObject.ShowSearchForm,
        'PopulateMethod':self.PresentationElementObject.PopulateMethod,
        'HasParentDataset':self.PresentationElementObject.HasParentDataset,
        'PopulateParentMethod':self.PresentationElementObject.PopulateParentMethod,
        'PopulateTypeParent':self.PresentationElementObject.PopulateTypeParent,
        'ParentDatasetId':self.PresentationElementObject.ParentDatasetId,
        'SrcWidgetId':self.PresentationElementObject.SrcWidgetId,
        'SrcParentWidgetId':self.PresentationElementObject.SrcParentWidgetId,
        'PopulateMethod': self.PresentationElementObject.PopulateMethod,

        }
        self.WidgetDict.update(widget_object)



    def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
                self.PeLang.FormDefSearch = json.dumps(self.NewItem['FormDefSearch'])

        if 'GridState' in self.NewItem:    
            self.PeLang.GridState= json.dumps(self.NewItem['GridState'])
        #if 'ResponseMsg' in self.NewItem:    
            #self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        #if 'GridState' in self.NewItem:    
            #self.PeLang.GridState=self.NewItem['GridState']

    def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
                self.PeLangDict['FormDefSearch'] = json.dumps(self.NewItem['FormDefSearch'])

        if 'GridState' in self.NewItem:    
            self.PeLangDict['GridState']= json.dumps(self.NewItem['GridState'])

    def process_widget_execute(self,params,session=None):
    
        if 'paginate' in params:
            paginate = params['paginate']
        else:
            paginate = False
        
        self.Dataset = dataset(params['resource_id'],'dataset','','','','',params['user'],params['language'],params['org_id'],{})
        self.Dataset.rdbms_cls.SetOrganisation(self.Dataset.OrganisationId)
        if session is None:
            raise Exception('Database session error - session object is not available')
            
        self.Dataset.set_session(session)
        self.Dataset.get_definition()
        resource_id = params['resource_id']
        presentation_id = params['presentation_id']
        language = params['language']
        task_def_id = params['task_def_id']
        user = params['user']
        #pes = PresentationElements.objects.filter(id=presentation_id)
        
        pElement = session.query(PresentationElementsSa,PresentationElementsLangSa)\
        .join(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==language.id))\
        .filter(PresentationElementsSa.id==presentation_id).all()
    
        p_element = None
        p_element_lang = None
        for pe in pElement:
            p_element = pe[0]
            p_element_lang = pe[1]
    
    
        if p_element is None:
            raise Exception('Widget definition number '+(presentation_id)+' is not found.')
        #get form
        #check if something from here needs to come from the client
    
   
        parent_initiate_id = 0
        src_task_instance_id=0
        parent_initiate_id = presentation_id
        parent_id= 0
        populate_type=p_element.PopulateType
        resource_record_id = 0
        #self.Dataset.get_form_definition(form_type)
        #self.Dataset.generate_form() #GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)

    
        if type(p_element.Parameters)==str:
            parameters = json.loads(p_element.Parameters)
        else:
            parameters=None
        if parameters is not None:
            publish_id = parameters['PublishedResourceId']
        else:
           raise Exception('Published resource error --> not found id = '+str(publish_id)) 


        helper = ResourceHelperClass()

        published_resource = helper.get_published_resource(publish_id)

        if self.PeLangObject.FormDefSearch is not None:
            if type(self.PeLangObject.FormDefSearch)==str:
                search_form = json.loads(self.PeLangObject.FormDefSearch)
                formDefinition = search_form.get('SearchForm',[])
            else:
                formDefinition = self.PeLangObject.FormDefSearch.get('SearchForm',[])
        else:
            formDefinition = published_resource['Form']

        resourceDefinition = published_resource['Parameters']

        #this should be from task definition - SearchForm
        #process search form and get default data if any
        p_elements = published_resource['Definition']
        
    
        
        results = None
        selectedRecords=[]
        
        
        #GET DATA
        formData,recordId = self.Dataset.get_dataset_form_data(p_element,p_elements,src_task_instance_id,parent_id,populate_type,resource_record_id)
        
        if p_element.AutoSearch: #if auto search is checked then get data

            if self.Dataset.ResourceSubType=='view':

                selectedRecords,formData,columns,columns_for_select,results,filter_data = self.process_resource_view(session,formData,formDefinition,presentation_id,p_element_lang,None,params)
            
            else:
                #session.add(ErrorLogSa(Error1=json.dumps({'Before':'ktTaskDatasetList process_standard_dataset fn'}),CreatedDateTime=datetime.datetime.now(datetime.timezone.utc)))     
                 
                selectedRecords,formData,columns,columns_for_select,results,filter_data = self.process_standard_dataset(session,formData,formDefinition,presentation_id,p_element_lang,None)
                #session.add(ErrorLogSa(Error1=json.dumps({'After':'ktTaskDatasetList process_standard_dataset fn'}),CreatedDateTime=datetime.datetime.now(datetime.timezone.utc)))     

        
        formDefinitionCard=None         
        response = {}
        response['resourceDefinition'] = resourceDefinition
        response['formDefinition'] = formDefinition
        response['formDefinitionCard'] = formDefinitionCard
        response['formData'] = formData
        response['analytics'] = results or None
        response['columns'] = columns
        response['columnsForQuery'] = columns_for_select
        response['selectedRecords'] = selectedRecords
        response['org_id'] = params['org_id']
        response['filter'] = filter_data
        
        return response

    def process_widget_search(self,params,form_definition,search_filter,in_columns,session=None):
    
        
        self.Dataset = dataset(params['resource_id'],'dataset','','','','',params['user'],params['language'],params['org_id'],{})
        self.Dataset.rdbms_cls.SetOrganisation(self.Dataset.OrganisationId)

        if session is None:
            raise Exception('Database session error - session object is not available')

        self.Dataset.set_session(session)
        self.Dataset.get_definition()
        resource_id = params['resource_id']
        presentation_id = params['presentation_id']
        language = params['language']
        task_def_id = params['task_def_id']
        user = params['user']
        
        #pes = PresentationElements.objects.filter(id=presentation_id)
      
        pElement = session.query(PresentationElementsSa,PresentationElementsLangSa).join(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==language.id)).filter(PresentationElementsSa.id==presentation_id).all()
    
        p_element = None
        p_element_lang = None
        for pe in pElement:
            p_element = pe[0]
            p_element_lang = pe[1]
    
    
        if p_element is None:
            raise Exception('Widget definition number '+(presentation_id)+' is not found.')
        #get form
        #check if something from here needs to come from the client
    
   
        parent_initiate_id = 0
        src_task_instance_id=0
        parent_initiate_id = presentation_id
        parent_id= 0
        populate_type=p_element.PopulateType
        resource_record_id = 0

    
        if type(p_element.Parameters)==str:
            parameters = json.loads(p_element.Parameters)
        else:
            parameters=None
        if parameters is not None:
            publish_id = parameters['PublishedResourceId']
        else:
           raise Exception('Published resource error --> not found id = '+str(publish_id)) 

        helper = ResourceHelperClass()

        published_resource = helper.get_published_resource(publish_id)

        results = None
        selectedRecords=[]
        
        formData = search_filter

        formDefinition = form_definition
        
        if self.Dataset.ResourceSubType=='view':

            selectedRecords,formData,columns,columns_for_select,results,filter_data = self.process_resource_view(session,formData,formDefinition,presentation_id,p_element_lang,in_columns,params)
            
        else:
            
            selectedRecords,formData,columns,columns_for_select,results,filter_data = self.process_standard_dataset(session,formData,formDefinition,presentation_id,p_element_lang,in_columns)
                                    
        formDefinitionCard=None         
        response = {}
        #response['resourceDefinition'] = resourceDefinition
        #response['formDefinition'] = formDefinition
        #response['formDefinitionCard'] = formDefinitionCard
        #response['formData'] = formData
        response['analytics'] = results or None
        response['columns'] = columns
        response['columnsForQuery'] = columns_for_select
        response['selectedRecords'] = selectedRecords
        #response['org_id'] = params['org_id']
        #response['filter'] = filter_data
        
        return response

    def process_standard_dataset(self,session,formData,formDefinition,presentation_id,p_element_lang,in_columns_for_select=None):

        selectedRecords=[]
        results=[]
        if p_element_lang.GridState is not None:

            grid_state=json.loads(p_element_lang.GridState)
        
        columns = grid_state.get('columnsDef',[])
        if in_columns_for_select is None:
            columns_for_select = grid_state.get( 'columnsQuery',[])
        else:
            columns_for_select = in_columns_for_select
        #TODO - uvesti zastitu ako je stavljen autosearch a nema filtera ... onda se cijela tablica i povezane skidaju !!
        rData,f,select_related, parameters = self.Dataset.extract_form_data_search(formData,formDefinition)
        filter = f           
        resourceData,m2mData = self.Dataset.process_resource_data_search(rData)
        selectedRecords = self.Dataset.get_dataset_data_filtered(resourceData,columns_for_select,select_related,parameters={})
        
        p_element = session.query(PresentationElementsSa).filter(PresentationElementsSa.id==presentation_id).all()[0]
        if type(p_element.Parameters)==str:
            parameters = json.loads(p_element.Parameters)
        else:
            parameters=None

        if parameters is not None:
            if ('Analytics' in parameters):
                results = self.process_analytics(parameters,selectedRecords)


        formData = [resourceData]
     
        return selectedRecords,formData,columns,columns_for_select,results,filter              


    def process_resource_view(self,session,formData,formDefinition,presentation_id,p_element_lang,in_columns_for_select=None,params={}):

        selectedRecords=[]
        results=[]
        
        #take union definition from parameters  

        if p_element_lang.GridState is not None:

            grid_state=json.loads(p_element_lang.GridState)
        
        columns = grid_state.get('columnsDef',[])
        if in_columns_for_select is None:
            columns_for_select = grid_state.get( 'columnsQuery',[])
        else:
            columns_for_select = in_columns_for_select
        
        
        union_defs = []

        if self.Dataset.Parameters is not None:
            unions = self.Dataset.Parameters.get('union',[])
            union_type = self.Dataset.Parameters.get('union_type',0)

        rData,f,select_related, parameters = self.Dataset.extract_form_data_search(formData,formDefinition)
        filter = f
        
        #for each element in union
        for ui in unions:

            module_path = 'katalyonode.functions.resources.'+ui['ResourceType']+'def' 
            module_ref = importlib.import_module(module_path)
            resource_class = getattr(module_ref, ui['ResourceType'])
            resource_object = resource_class(ui['ResourceDefId'],ui['ResourceType'],'','','','',params['user'],params['language'],params['org_id'],{})
            resource_object.rdbms_cls.SetOrganisation(params['org_id'])

            #here transform rData to be able to execute search for each dataset from union
            
            rDataInput,columns_def = self.process_data_mapping(ui['FieldMap'],rData)
            resourceData,m2mData = resource_object.process_resource_data_search(rDataInput)
            m2m_map = ui.get('MapM2m',None)
            if m2m_map is not None:
                m2mWithFilter = self.process_m2m_filter(m2m_map,select_related)
                ui['MapM2m'] = m2mWithFilter
            union_defs.append({'resourceDefId':ui['ResourceDefId'],'filterData':resourceData,'columns':columns_def,'literals':ui.get('Literals',[]),'m2m':ui.get('MapM2m',[])})
        #call function that returns a union
        selectedRecords = self.Dataset.get_dataset_data_filtered_union(union_defs,union_type)
        
        p_element = session.query(PresentationElementsSa).filter(PresentationElementsSa.id==presentation_id).all()[0]
        if type(p_element.Parameters)==str:
            parameters = json.loads(p_element.Parameters)
        else:
            parameters=None

        if parameters is not None:
            if ('Analytics' in parameters):

                results = self.process_analytics(parameters,selectedRecords)


        formData = [resourceData]
     
        return selectedRecords,formData,columns,columns_for_select,results,filter      

    def process_analytics(self,parameters,selectedRecords):

        results= []
          
        for i in range(len(parameters['Analytics'])):
              results.append({ 'sum': {}, 'count': {}, 'max': {}, 'min': {}, 'mean': {}} )
        #pdb.set_trace() 
 
        for rec in selectedRecords: #{'sum'},{'mean'},{'min'},{'max'},{'count'},
            trec=dict(rec) #TODO - IMPROVE PERFORMANCE - REMOVE CONVERSION TO DICT (WE ONLY NEED COLUMN POSITION)
       
            for i in range(len(parameters['Analytics'])):
                try:
                      param = parameters['Analytics'][i]
                      agg = trec[param['AggregateField']['FieldName']]
                      
                      if param['AggregateField']['FieldName'] + '.name' in trec: # if we have list object take code.name  as aggregate (not code)
                            agg = trec[param['AggregateField']['FieldName'] + '.name']
                          
                      if isinstance( agg, datetime.date): #if agg is date convert to string
                            agg = str(agg)[:10]
                            
                      if isinstance( agg, datetime.datetime): #if agg is date convert to string
                            agg = str(agg)[:10]
                      
                      calc = param['CalculateField']['FieldName']
                      calcFun = param['CalculateFunction']['name']
                      res = results[i]
                      #pdb.set_trace()
                      res['count'][agg] =  res['count'].get(agg,0) + 1 
                      
                      if calc:
                        if trec[calc]:
                          res['sum'][agg] =  res['sum'].get(agg,0) + trec[calc]                          
                          if trec[calc] >= res['max'].get(agg, trec[calc] ) :
                              res['max'][agg] =  trec[calc]
                          if trec[calc] <= res['min'].get(agg, trec[calc] ) :
                              res['min'][agg] = trec[calc]
                except Exception as exc:
                    pdb.set_trace()
                    pass
        
        for res in results:   #   Calculate MEAN value
            for key,value in res['sum'].items():
                res['mean'][key] =  res['sum'][key] /  res['count'][key] 
        
        return results

    def process_data_mapping(self,fieldMap,filterData):
        columns = []
        filterDataRet={}
        for key,val in fieldMap.items():
            filterElement = filterData.get(key,None)
            if filterElement is not None:    
                filterDataRet[val] = filterElement
                filterDataRet[val]['fieldId'] = val
            columns.append({'field':val,'fieldId':key})

        return filterDataRet,columns

    def process_m2m_filter(self,m2m,filterData):
        filter_field = None
        for item in m2m:
            filterDataRet={}
            
            for sr in filterData:

                filter_field = sr.get('filter_data',None)
            
            if filter_field is not None:
                filterDataRet.update(filter_field)
            item['filter_data'] = filterDataRet

        return m2m