######################## ktList processing module ###############################

###############################################################################
import pdb
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import json
#def ProcessElementDefinition(layout,params):
        
#    return layout
"""
def GetElementValue(presentationElement,rmd,params):
    
    value = None
   
    if presentationElement.Search is not None:
        if 'value' in presentationElement.Search:
            value = presentationElement.Search['value']
           
      
    return value
"""
class ktList(WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = True
    def set_value(self,l_value ):
  
        self.__value = l_value
    
    def get_element_value(self,pe,rmd,item,params):
        
        field_name = rmd['FieldName']
        value = getattr(item,field_name,None)
        if type(value) is str:
            if '{' and '}' in value:
                value = json.loads(value)
                if 'id' in value:
                    value = value['id']
                elif 'value' in value:
                    value = value['value']
                elif type(value) is dict:
                    if 'id' in value:
                        value = value['id']
                    elif 'value' in value:
                        value = value['value']
        
        return value

    def get_definition(self):
    
        return self.get_element_json()

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {
        'CodeId':self.ResourceModelObject.CodeId

        }
        self.WidgetDict.update(widget_object)

    def prepare_resource_model_insert(self):
        
        super().prepare_resource_model_insert()
        self.ResourceModelDict['FieldType'] = 'codesdetail'
        self.NewItem['FieldType'] = 'codesdetail'

    def process_widget(self,form_data,rmd,pe=None):

        value = None
        field_name = rmd['FieldName']
        
        if not rmd['NoUpdate']:
            if field_name in form_data:
                value = form_data[field_name]

        if rmd['UpdateWithDefault'] and not rmd['NoUpdate']:
            value = rmd['DefaultValue']
            if value is not None and type(value) is not dict:
                valueJson = json.loads(value)
            else:
                valueJson = value
                
            value = valueJson.get('value',None)

        
        elif value is not None and type(value) in [str,bytes,bytearray]:
            value = json.loads(value)

        if value is not None and type(value) is dict:
            if 'id' in value:
                value = value['id']              
            elif 'value' in value:
                value = value['value']            

        widget_value={'type':rmd['GenericType'],'widget_name':rmd['DirectiveName'],'value':value}

        return widget_value


    def process_widget_search(self,item,filter_data):
        
        value=None
        r_val,f_data,sel_rel,param=super().process_widget_search(item,filter_data)
        updateWithDefault = item.get('UpdateWithDefault',False)
        input_value = r_val.get('value',None)
        if not updateWithDefault:
            if input_value!=None:
                
                if type(input_value) is str:
                    if input_value!='':
                        input_value = json.loads(input_value)

                if type(input_value) is dict:
                    if 'id' in input_value:
                        value = input_value['id']
                    elif 'value' in input_value:
                        value = input_value['value']
                elif type(input_value) is list:
                    value = []              
                    for el in input_value:
                        if type(el) is dict:
                            if 'id' in el:
                                value.append(el['id'])
                            elif 'value' in el:
                                value.append(el['value'])
                        else:
                          value.append(el)  
                else:
                  value = input_value
            
        if value is not None:
            r_val.update({'value':value})
        
        return r_val,f_data,sel_rel,param