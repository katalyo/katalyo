######################## ktResource processing module ###############################

###############################################################################
import pdb
import json
#from katalyonode.functions.taskprocessing import StartTask
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from katalyonode.models.models import PresentationElements,UserExtendedProperties
from django.utils import timezone
import importlib

class ktResourceLink (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        
    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {

        'PopulateType': self.PresentationElementObject.PopulateType,
        'PresentationType': self.PresentationElementObject.PresentationType,

        }
        self.WidgetDict.update(widget_object)

    
    def process_widget(self,form_data,rmd):

        resource_value = []
        widget_value={}
        field_name = rmd['FieldName']
        if 'Value' in form_data[field_name]:
            resource_value=form_data[field_name]['Value']
        else:
            resource_value=None
        if 'itemValue' in form_data[field_name]:
            resource_value=form_data[field_name]['itemValue']
        else:
            resource_value=None
                    
        if resource_value is not None:
            if len(resource_value)>0:
                resource_value=resource_value[0]
            else:
                resource_value=None
                      
        
        #ToDo - check how to populate ResourceLink
        if resource_value is not None and len(resource_value)>0:
          widget_value={'type':rmd['GenericType'],'widget_name':rmd['DirectiveName'],'max_length':rmd['MaxLength'],'value':{'resourceSourceDefId':self.ParentResource.id,'modelDefinitionId':rmd['ItemId'],'resourceDefId': rmd['Related'],'resourceId':resource_value}}

        return widget_value

    def generate_query(self,field):
        
        temp_value = field.get('value',None)
        value = None
        m2mData=[]
        if temp_value and field['max_length']!=1:
            m2mData.append(temp_value)
        elif temp_value and field['max_length']==1:
            
            if type(temp_value['resourceId']) is list:
                if len(temp_value['resourceId'])>0:
                    value=temp_value['resourceId'][0]
                else:
                    value=temp_value['resourceId']
        
        field['value'] = value
        field['m2mData'] = m2mData

        return field

    def prepare_resource_model_insert(self):

        super().prepare_resource_model_insert()
        
        if self.NewItem['Related'] is not None:
            self.ResourceModelDict['FieldType'] = 'resource'+str(self.NewItem['Related'])+'_'+str(self.ParentResource.OrganisationId)
        else:
            self.ResourceModelDict['FieldType'] = 'resource'+self.NewItem['ElementType'] +'_'+str(self.ParentResource.OrganisationId)
        self.NewItem['FieldType'] =  self.ResourceModelDict['FieldType']

    def prepare_resource_model_update(self):

        super().prepare_resource_model_update()
        
        if self.NewItem['Related'] is not None:
            self.ResourceModelDict['FieldType'] = 'resource'+str(self.NewItem['Related'])+'_'+str(self.ParentResource.OrganisationId)
        else:
            self.ResourceModelDict['FieldType'] = 'resource'+self.NewItem['ElementType'] +'_'+str(self.ParentResource.OrganisationId)
        self.NewItem['FieldType'] =  self.ResourceModelDict['FieldType']


    def get_element_value(self,pe,item,params):

        field_name=pe['FieldName']+'l'     
        if pe['ItemId'] in m2mIds:
            if (m2mIds [pe['ItemId']]) is None:
            m2mIds [pe['ItemId']] = []
        else:
            m2mIds.update({pe['ItemId']:[]})
        
        field_name = 'id'
        value = recDict[field_name]
      
        if value is not None:
            m2mIds[pe['ItemId']].append(value)  
        
        value = m2mIds[pe['ItemId']]

        return value