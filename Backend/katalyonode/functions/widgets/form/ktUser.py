"""
ktUser widget

user widget

"""

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import pdb

def ProcessElementDefinition(layout,params):
    
    #prepare resourceLinks    
    #layout['resourceLinks'] = {'Resource1':params['resource_id'],'Resource2_id':layout['resourceModelDefaults']['Related_id'],'Parameters':{},'LinkType':'FK','CreatedBy':params['current_user'],'CreatedDateTime':timezone.now()}
    return layout

def GetElementValue(presentationElement,rmd,params):

    l_value = None
    if 'user' in params:
        user = params['user']
        if presentationElement['PopulateType'] == '2':
            l_value = user.id

class ktUser (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        
    def set_value(self,PopulateType,Params):
    
        
        l_value = None
        if 'user' in Params:
            user = Params['user']
            if PopulateType == '2':
                l_value = user.id
            
        self.__value = l_value
    
    def get_element_value(self,pe,item,params):
        
        field_name = pe['FieldName']
        value = getattr(item,field_name,None)
        if type(value) is str:
            if '{' and '}' in value:
                value = json.loads(value)
                if 'id' in value:
                    value = value['id']
                elif 'value' in value:
                    value = value['value']
                elif type(value) is dict:
                    if 'id' in value:
                        value = value['id']
                    elif 'value' in value:
                        value = value['value']

        if pe['PopulateType'] == 2:
            value = self.ParentResource.User.id
        
        return value
    

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {

        'PopulateType': self.PresentationElementObject.PopulateType,
        #'PresentationType': self.PresentationElementObject.PresentationType,

        }
        self.WidgetDict.update(widget_object)


    def process_widget(self,form_data,rmd,pe=None):

        field_name = rmd.get('FieldName','FieldName')
        updateWithDefault = rmd.get('UpdateWithDefault',False)
        
        
        if not rmd.get('NoUpdate',False):
            value = form_data.get(field_name,None)
           
            if updateWithDefault:
                value_default = rmd.get('DefaultValue','')
                if 'id' in value_default:        
                    value = json.loads(value_default)
                    value = value['id']
                elif 'value' in value_default:
                    value = json.loads(value_default)
                    value = value['value']
                else:
                    value = json.loads(value_default)

            elif str(rmd.get('PopulateType',"0")) == '2':
                value = self.ParentResource.User.id 

            elif value is not None and type(value) in [str,bytes,bytearray]:
                value = json.loads(value)
                   
            elif value is not None and type(value) is dict:
                if 'id' in value:
                    value = value['id']              
                elif 'value' in value:
                    value = value['value']     

        directive_name = rmd.get('DirectiveName','ktUser')
        widget_value={'type':rmd.get('GenericType','no-generictype'),'widget_name':directive_name,'value':value}

        return widget_value

    def process_widget_search(self,item,filter_data):
        
        value=None
        r_val,f_data,sel_rel,param=super().process_widget_search(item,filter_data)
        updateWithDefault = item.get('UpdateWithDefault',False)
        input_value = r_val.get('value',None)
        if not updateWithDefault:
            if input_value is not None:
                if type(input_value) is dict:
                    if 'value' in input_value:
                        input_value = json.loads(input_value)
                        value = input_value['value']
                    else:
                        value =input_value
                else:
                    value =input_value
                
            fieldId = item.get('FieldName',None)

            if str(item['PopulateType']) == '2' and fieldId is not None and value is None:
                value = self.ParentResource.User.id
                if type(f_data) is dict:
                    f_data[fieldId] = value
                elif type(f_data) is list and len(f_data)>0:
                    f_data[0][fieldId] = value
        
        if value is not None:
            r_val['value'] = value
        
        search = item.get('Search',None)
        if search is not None: 
            item['Search'].update({'type':item.get('ElementType','user'),'value':r_val.get('value',None)})
            if r_val:    
                r_val.update({'search':item['Search']})
        
        
        return r_val,f_data,sel_rel,param