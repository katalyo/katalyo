######################## ktTaskMessage widget ###############################

###############################################################################

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass

class ktTaskMessage (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.HasResourceModel = False
  def populate_definition_json(self):

    super().populate_definition_json()

    widget_object = {
    
    'ResponseMsg':self.PeLangObject.ResponseMsg,

    }
    self.WidgetDict.update(widget_object)