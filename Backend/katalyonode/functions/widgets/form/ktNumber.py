"""
ktTextbox widget

textbox widget

"""

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import pdb


class ktNumber (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
    def set_value(self,l_value ):
  
        self.__value = l_value
    
    def get_value(self):
    
        return self.__value

    def get_definition(self):
    
        return self.get_element_json()