"""
ktDate widget

textbox widget

"""

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import pdb


class ktTime (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = True
   


    def get_definition(self):
    
        return self.get_element_json()

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {
        
        'ItemId':self.ResourceModelObject.id,
        'UseSystemDate':self.PresentationElementObject.UseSystemDate,

        }
        self.WidgetDict.update(widget_object)

    def check_changes_new(self):
         
        has_changes=False
        OLD=0  
        NEW=1
        changes_detail=''
        fields = [('ReadOnly','ReadOnly'),('ShowField','ShowField'),('RequiredField','RequiredField'),('UniqueId_id','UniqueId'),('MaxLength','MaxLength'),('Label','Label')]
        old_item = self.get_definition_json()
        #check fields for changes
        for field in fields:
               has_changes =  self.check_equal(field[OLD],field[NEW],old_item,self.NewItem)
               if (has_changes):
                    break

        return has_changes



    def process_widget(self,form_data,rmd,pe=None):
        
        field_name = rmd['FieldName']
        value=None
        if rmd['UseSystemDate'] is not None:
          if rmd['UseSystemDate']:
            if field_name in form_data:
              form_data[field_name] = None
            else:
              form_data.update({field_name:None})
                
        if field_name in form_data:
          if form_data[field_name]!=None:
            if type(form_data[field_name]) is dict:
               if 'id' in form_data[field_name]:
                 value = json.loads(form_data[field_name])
                 value = value['id']
               elif 'value' in form_data[field_name]:
                 value = json.loads(form_data[field_name])
                 value = value['value']
            else:
              value = form_data[field_name]
                
          filter_value={}
          
          if value is not None and type(value)==str():
            value = parse_datetime(value)
          format1 = format2 = ''
          peLang = None
          
          if hasattr(pe,'_prefetched_objects_cache'):
            if 'presentationelementslang_set' in pe._prefetched_objects_cache:
              if pe._prefetched_objects_cache['presentationelementslang_set'] is not None:
                if len(pe._prefetched_objects_cache['presentationelementslang_set']) > 0:
                  peLang = pe._prefetched_objects_cache['presentationelementslang_set'][0]
              
          if peLang is not None:
            if peLang.ParametersLang is not None:
              if 'format1' in peLang.ParametersLang:
                 format1=peLang.ParametersLang['format1']
                 if 'name' in format1:      format1=format1['name']
              if value is not None:
                  if hasattr(value,'year'):              filter_value[field_name+'__year']=  value.year
                  if hasattr(value,'month'):             filter_value[field_name+'__month']=  value.month
                  if hasattr(value,'day'):               filter_value[field_name+'__day']=  value.day
                      
          if peLang is not None:
            if peLang.ParametersLang is not None:
              if 'format2' in peLang.ParametersLang:
                  format2=peLang.ParametersLang['format2']
                  if 'name' in format2:    format2=format2['name']
              if value is not None:
                  if hasattr(value,'hour'):              filter_value[field_name+'__hour']=  value.hour
                  if hasattr(value,'minute'):            filter_value[field_name+'__minute']=  value.minute
                  if hasattr(value,'second'):            filter_value[field_name+'__second']=  value.second
               
          format = format1+' '+format2
          format = format.strip()

          if rmd['UseSystemDate'] is not None:
              if rmd['UseSystemDate']:
                widget_value={'type':'systemdate','value':value,'widget_name':rmd['DirectiveName'],'format1':format1,'format2':format2,'filter_value':filter_value}
              else:
                widget_value={'type':rmd['GenericType'],'value': value,'widget_name':rmd['DirectiveName'],'format1':format1,'format2':format2,'filter_value':filter_value}
                
          else:
            widget_value={'type':rmd['GenericType'],'value':value,'widget_name':rmd['DirectiveName'],'format':format,'filter_value':filter_value}


        return widget_value

    def generate_query (self,field):
        
        
        if field['type']=='systemdate':
              
            value = timezone.now()
              
            if field['format2']=='' or field[1]['format2'] is None:
                
                value = value.replace(hour=0, minute=0, second=0, microsecond=0)-timedelta(hours=2)
                field['value'] = value
                
        return field