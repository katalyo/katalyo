######################## ktTaskUpdateResource processing module ###############################

###############################################################################

import pdb
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import importlib,json
from datetime import datetime,timezone
from katalyonode.functions.resources.datasetdef import dataset
#from katalyonode.models.models import PublishedResources,TaskInitiationInstanceToResource,TaskExecutionInstanceToResource
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PublishedResourcesSa,TaskInitiationInstanceToResourceSa,TaskExecutionInstanceToResourceSa
from django.conf import settings

class ktTaskUpdateResource (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.Dataset = None
    self.HasResourceModel = False

  def populate_definition_json(self):

    super().populate_definition_json()
    widget_object = {
        'PopulateType': self.PresentationElementObject.PopulateType,
        'HasParentDataset':self.PresentationElementObject.HasParentDataset,
        'PopulateParentMethod':self.PresentationElementObject.PopulateParentMethod,
        'PopulateTypeParent':self.PresentationElementObject.PopulateTypeParent,
        'ParentDatasetId':self.PresentationElementObject.ParentDatasetId,
        'SrcWidgetId':self.PresentationElementObject.SrcWidgetId,
        'SrcParentWidgetId':self.PresentationElementObject.SrcParentWidgetId,
        'PopulateMethod': self.PresentationElementObject.PopulateMethod,
        'Related':self.PresentationElementObject.Related,
        'ResponseMsg':self.PeLangObject.ResponseMsg,
        'ParametersLang':json.loads(self.PeLangObject.ParametersLang or '{}'),
        'FormDef':json.loads(self.PeLangObject.FormDef or '[]'),
        'GridState':json.loads(self.PeLangObject.GridState or '{}'),
    }
    self.WidgetDict.update(widget_object)


  def ProcessElementDefinition(layout,params):
      
      #raise Exception('ProcessElementDefinition fn TEST exception raised')

      #get presentation elements record
      layoutLocal = layout['layout']
      if layoutLocal['Related'] is None and layoutLocal['Status']=='Active':
          raise ValueError('Add dataset record (Widget id = '+str(layoutLocal['PresentationId'])+') - error in definition  --> Dataset must be selected ')
      
      if layoutLocal['PresentationId'] is not None:
          #todo alchemy
          #pe = PresentationElements.objects.get(id=layoutLocal['PresentationId'])
          pe = PresentationElementsSa.filter(PresentationElementsSa.id == layoutLocal['PresentationId']).first()
          
          if not pe:
              raise ValueError('Add dataset record - element definition not found --> id = '+str(layoutLocal['PresentationId']))
          else:
              if layoutLocal['Related'] != pe.Related: 
                  updateStatus = SetResourceStatus(params['resource_id'],params['form_type'],params['override_type'],layoutLocal['PresentationId'],params['current_user'],params['new_version'],'NotActive')
                  if not updateStatus:
                       raise ValueError('Add dataset widget - update status failed --> id = '+str(layoutLocal['PresentationId']))    
      
      #check if 
      
      return layout

  def process_widget(self,widget_data,p_element,pe_lang,task_instance_id):
  
    parentDatasetDefId = None
    parentDatasetId = None

    self.Dataset = dataset(p_element.Related,'dataset','','','','',self.ParentResource.User,self.ParentResource.Language,self.ParentResource.OrganisationId,{})
    self.Dataset.set_session(self.ParentResource.Session)
    #get published dataset
    if type(self.PresentationElementObject.Parameters) == str:
      parameters = json.loads(self.PresentationElementObject.Parameters)
    else:
      parameters = self.PresentationElementObject.Parameters

    publishId = parameters.get('PublishedResourceId',None)
     
    session = self.Dataset.rdbms_cls.Session()

    publish = []
    with session.begin():
      #publish = PublishedResources.objects.filter(id=publishId)
      publish = session.query(PublishedResourcesSa).filter(PublishedResourcesSa.id==publishId).all()
      
    rmd = None
    resource_params = None
    for resource in publish:
      rmds = json.loads(resource.Definition)
      resource_params = json.loads(resource.Parameters)

    #pes = PresentationElements.objects.filter(ResourceDefId_id=p_element.Related_id,ParentWidgetId_id=p_element.id,FormType=self.ParentResource.FormType,OverrideType='w',Status='Active',ResourceModelDefinitionId__ResourceDefId_id=p_element.Related_id,ResourceModelDefinitionId__Status='Active').select_related('ResourceModelDefinitionId').prefetch_related(Prefetch('presentationelementslang_set', queryset=PresentationElementsLang.objects.filter(Lang=self.ParentResource.Language)))
    
    mainResource,m2mData = self.Dataset.generate_query(self.Dataset.process_form_data(widget_data,rmds))

    status,record = self.Dataset.rdbms_cls.update_data(mainResource,self.Dataset.id,self.Dataset.OrganisationId,self.Dataset.User.id)
    
    if record and len(m2mData)>0:
        mutantm2m =  self.Dataset.rdbms_cls.insert_data_m2m_v2(record.id,self.ParentResource.OrganisationId,m2mData,self.ParentResource.User,False)
    
   
    
    jsonResponseBChain=None
    jsonResponseBChain2=None

    if resource_params and settings.USE_BLOCKCHAIN:
      if resource_params['IsBlockchain']:
      #add record to blockchain
       
        datasetData = json.dumps(widget_data)
        h =  hashlib.sha512(datasetData.encode()).hexdigest()
        user = 'user'
        p_key=int(str(p_element.Related)+str(record.id).zfill(12))
        trx_json = {'pKey':p_key,'datasetDef_Id':p_element.Related,'datasetRec_Id':record.id,'dataset_Data': datasetData,'h':h,'v':0,'user':self.ParentResource.User.id}
        contract = 'kataly1.code'
        action='createds'
        
        jsonResponseBChain,block_id,status = self.send_transaction(trx_json,contract,action)
   
        bchain_id= int(jsonResponseEOS['processed']['action_traces'][0]['console'])
   
        jsonResponseBChain2,status2 = self.get_table_rows('kataly1.code','dataset','kataly1.code','primary','uint64_t',bchain_id)
   
        #save transaction id
        if status==200:
          mainResource['id'] = record.id
          mainResource['TrxId'] = jsonResponseEOS['transaction_id']
          mainResource['BChainRecId'] = bchain_id
          mainResource['BlockId'] = str(block_id)
          result = self.rdbms_cls.update_data(mainResource,p_element.Related,self.ParentResource.OrganisationId,self.ParentResource.User.id) 
   
    if p_element.HasParentDataset:
      if p_element.PopulateTypeParent!='' and record is not None:
          #if (int(element['parentDatasetId'])>0):
            parentDatasetDefId,parentDatasetId = self.Dataset.process_parent_dataset(p_element,widget_data,p_element.Related,record)
    
   
    if record is not None:

      if self.ParentResource.FormType=='i':
        #titr=TaskInitiationInstanceToResource.objects.create( ResourceDefId_id = p_element.Related, TaskInitiateId_id = task_instance_id, ResourceId = record.id,ParentDatasetDefId_id = parentDatasetDefId,
        #                                                   ParentDatasetId = parentDatasetId,PresentationElementId =p_element ,PresentationElementUuidRef=p_element.UuidRef,
        #                                                    CreatedBy = user_id, CreatedDateTime=timezone.now())
        titr=TaskInitiationInstanceToResourceSa(ResourceDefId = p_element.Related, TaskInitiateId = task_instance_id, ResourceId = record.id,PresentationElementUuidRef =p_element.UuidRef,
                                                               ParentDatasetDefId = parentDatasetDefId,ParentDatasetId = parentDatasetId,CreatedBy = self.ParentResource.User.id, CreatedDateTime=datetime.now(timezone.utc))
        self.ParentResource.Session.add(titr)
      else:
        #titr=TaskExecutionInstanceToResource.objects.create( ResourceDefId_id = p_element.Related, TaskExecuteId_id = task_instance_id, ResourceId = record.id,ParentDatasetDefId_id = parentDatasetDefId,
        #                                       ParentDatasetId = parentDatasetId,PresentationElementId_id =p_element.id ,PresentationElementUuidRef=p_element.UuidRef, CreatedBy_id = self.ParentResource.User.id, CreatedDateTime=timezone.now())
        
        titr=TaskExecutionInstanceToResourceSa(ResourceDefId = p_element.Related, TaskExecuteId = task_instance_id, ResourceId = record.id,PresentationElementUuidRef =p_element.UuidRef,
                                                               ParentDatasetDefId = parentDatasetDefId,ParentDatasetId = parentDatasetId,CreatedBy = self.ParentResource.User.id, CreatedDateTime=datetime.now(timezone.utc))
        self.ParentResource.Session.add(titr)
      ret_value = {'widgetId':p_element.id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':p_element.Related},{'name':'resourceRecordId','value':record.id}]}],'bChainResponse':jsonResponseBChain,'bChainResponse2':jsonResponseBChain2,'taskResourceLink': titr.id}
    
    else:
      ret_value = {'widgetId':p_element.id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':p_element.Related},{'name':'resourceRecordId','value':0}]}],'bChainResponse':jsonResponseBChain,'bChainResponse2':jsonResponseBChain2,'taskResourceLink':0}

    if pe_lang.ResponseMsg is not None and pe_lang.ResponseMsg!='':
      response_msg = self.process_response_msg(record,pe_lang.ResponseMsg)
      return {'ret_value':ret_value,'record':record,'ResponseMsg':response_msg}

    return {'ret_value':ret_value,'record':record}  
      
  def pre_process_widget(self,widget_id,task_instance_id,form_type,dataset_def_id,dataset_record_id):
  
    parentDatasetDefId = None
    parentDatasetId = None

    self.Dataset = dataset(dataset_def_id,'dataset','','','','',self.ParentResource.User,self.ParentResource.Language,self.ParentResource.OrganisationId,{})
    self.Dataset.set_session(self.ParentResource.Session)
    #get UuidRef
    uuid_ref = None
     
    
    
    
       
    peFields = self.ParentResource.Session.query(PresentationElementsSa).filter(PresentationElementsSa.id==widget_id).all()
      
    for pe in peFields:
      uuid_ref = pe.UuidRef
  

    #get published dataset
    #publishId = self.PresentationElementObject.Parameters['PublishedResourceId']

    
    #session = self.Dataset.rdbms_cls.Session()
    #publish = session.query(PublishedResources).filter(PublishedResources.id==publishId).all()
    
    #rmds = None
    #resource_params = None
    #widget_data=None
    #for resource in publish:
      #widget_data = json.loads(resource.Form)
      #rmds = json.loads(resource.Definition)
      #resource_params = json.loads(resource.Parameters)

    #mainResource,m2mData = self.Dataset.generate_query(self.Dataset.process_form_data(widget_data,rmds))
    
    #resource_rec_id=mainResource.get('id',0)
    #pdb.set_trace()
    ret_value=None
    resource_ids=[]
    filter_data={}
    #resource_ids.append(dataset_record_id)
    id_key = 'resource'+str(self.Dataset.id)+'_'+str(self.ParentResource.OrganisationId)+'_id'
    
    if dataset_record_id==0:
      if form_type=='e':
        #teitrs=TaskExecutionInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskExecuteId_id = task_instance_id,
        #                                              PresentationElementUuidRef=uuid_ref)
            
        teitrs=self.ParentResource.Session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskExecutionInstanceToResourceSa.TaskExecuteId == task_instance_id,
                                                      TaskExecutionInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
                  
      else:
        #teitrs=TaskInitiationInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskInitiateId_id = task_instance_id,
        #                                              PresentationElementUuidRef =uuid_ref)
        teitrs=self.ParentResource.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskInitiationInstanceToResourceSa.TaskInitiateId == task_instance_id,
                                                      TaskInitiationInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
          
      if len(teitrs)>0:

        dataset_record_id = teitrs[0].ResourceId

      else:

        raise Exception('Record not found for widget = '+str(widget_id))      
        
    
    filter_data[id_key]=dataset_record_id    
    #records = self.Dataset.get_data_filtered(self.Dataset.id,self.ParentResource.OrganisationId,filter_data,None)
    records = self.Dataset.get_dataset_data_filtered(filter_data,[],[],parameters={})
    #records = self.Dataset.rdbms_cls.get_data_filtered_in(self.Dataset.id,self.Dataset.OrganisationId,resource_ids) 
    record=None
    for record in records:
      #if record and len(m2mData)>0:
          #mutantm2m =  mutant_get_data_m2m(relatedId,record.id,org_id,m2mData)
      if record is not None:
      
        #insert TaskInitiationInstanceToResource
        if form_type=='e':
          #teitrs=TaskExecutionInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskExecuteId_id = task_instance_id,
          #                                            PresentationElementUuidRef =uuid_ref)
          teitrs=self.ParentResource.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskInitiationInstanceToResourceSa.TaskInitiateId == task_instance_id,
                                                      TaskInitiationInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
            
          if len(teitrs)==0:
              
            #teitr_rec=TaskExecutionInstanceToResource.objects.create( ResourceDefId_id = self.Dataset.id, TaskExecuteId_id = task_instance_id, ResourceId = record[id_key],
            #                                          CreatedBy = self.Dataset.User,CreatedDateTime=timezone.now(), PresentationElementUuidRef =uuid_ref)
            teitr_rec = TaskExecutionInstanceToResourceSa(ResourceDefId = self.Dataset.id, TaskExecuteId = task_instance_id, ResourceId = record[id_key],PresentationElementUuidRef =uuid_ref,
                                                               CreatedBy = self.Dataset.User.id, CreatedDateTime=datetime.now(timezone.utc))
            self.ParentResource.Session.add(teitr_rec)
          else:
            for teitr in teitrs:
              teitr.ChangedBy=self.Dataset.User.id
              teitr.ChangedDateTime=datetime.now(timezone.utc)
              teitr.ResourceId = record[id_key]
              teitr_rec = teitr
                
        else:
          #teitrs=TaskInitiationInstanceToResource.objects.filter( ResourceDefId_id = self.Dataset.id, TaskInitiateId_id = task_instance_id,
          #                                            PresentationElementUuidRef =uuid_ref)
          teitrs=self.ParentResource.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.ResourceDefId == self.Dataset.id, TaskInitiationInstanceToResourceSa.TaskInitiateId == task_instance_id,
                                                      TaskInitiationInstanceToResourceSa.PresentationElementUuidRef==uuid_ref).all()
          
        
          if len(teitrs)==0:
              
            #teitr_rec=TaskInitiationInstanceToResource.objects.create( ResourceDefId = self.Dataset.id, TaskInitiateId_id = task_instance_id,PresentationElementUuidRef =uuid_ref, ResourceId = record[id_key],
            #                                          CreatedBy = self.Dataset.User,CreatedDateTime=timezone.now())
            teitr_rec = TaskInitiationInstanceToResourceSa(ResourceDefId = self.Dataset.id, TaskInitiateId = task_instance_id, ResourceId = record[id_key],PresentationElementUuidRef =uuid_ref,
                                                               CreatedBy = self.Dataset.User.id, CreatedDateTime=datetime.now(timezone.utc))
            self.ParentResource.Session.add(teitr_rec)
          else:
            for teitr in teitrs:
              teitr.ChangedBy=self.Dataset.User
              teitr.ChangedDateTime=timezone.now()
              teitr.ResourceId = record[id_key]
              teitr.save()
              teitr_rec = teitr
       
        
      ret_value = {'widgetId':widget_id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':self.Dataset.id},{'name':'resourceRecordId','value':record[id_key]}]}],'taskResourceLink': teitr_rec.id}

    return {'ret_value':ret_value,'data':record} 
    
  def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
          self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
          self.PeLang.FormDefSearch= json.dumps(self.NewItem['FormDefSearch'])
        if 'ResponseMsg' in self.NewItem:    
          self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        if 'GridState' in self.NewItem:    
          self.PeLang.GridState=self.NewItem['GridState']

  def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLangDict['FormDefSearch']= json.dumps(self.NewItem['FormDefSearch'])
        if 'ResponseMsg' in self.NewItem:    
            self.PeLangDict['ResponseMsg']= self.NewItem['ResponseMsg']
        if 'GridState' in self.NewItem:    
          self.PeLangDict['GridState']= json.dumps(self.NewItem['GridState'])

  def process_response_msg(self,record,responseTemplate):
    
    responseMsg=responseTemplate
    for element_key,element_val in record.items():
      if element_val is not None:
        old_str = '<<'+str(element_key)+'>>'
        responseMsg = responseMsg.replace(old_str,str(element_val))
    return responseMsg

  def convert_to_json(self,strVal):

      if '{' and '}' in strVal:
        strVal = strVal.replace('"','')
        strVal = strVal.replace('\'','"')
        strVal = json.loads(strVal)
      return strVal


