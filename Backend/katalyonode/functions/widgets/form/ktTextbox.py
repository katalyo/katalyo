"""
ktTextbox widget

textbox widget

"""

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import pdb


class ktTextbox (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = True
    def set_value(self,l_value ):
  
        self.__value = l_value
    
    def get_value(self):
    
        return self.__value

    def get_definition(self):
    
        return self.get_element_json()

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {
        
        'ItemId':self.ResourceModelObject.id,

        }
        self.WidgetDict.update(widget_object)

    def check_changes_new(self):
         
        has_changes=False
        OLD=0  
        NEW=1
        changes_detail=''
        fields = [('ReadOnly','ReadOnly'),('ShowField','ShowField'),('RequiredField','RequiredField'),('UniqueId_id','UniqueId'),('MaxLength','MaxLength'),('Label','Label')]
        old_item = self.get_definition_json()
        #check fields for changes
        for field in fields:
               has_changes =  self.check_equal(field[OLD],field[NEW],old_item,self.NewItem)
               if (has_changes):
                    break

        return has_changes