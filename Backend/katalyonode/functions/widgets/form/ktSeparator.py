######################## ktList processing module ###############################

###############################################################################
import pdb
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass

class ktSeparator(WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = False