######################## ktTaskForPage widget ###############################

###############################################################################

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import json

class ktTaskContainer (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.HasResourceModel = False
  def populate_definition_json(self):

    super().populate_definition_json()

    widget_object = {
    
    #'ResponseMsg':json.loads(self.PeLangObject.ResponseMsg or '{}'),
    'FormDef':json.loads(self.PeLangObject.FormDef or '[]'),

    }
    self.WidgetDict.update(widget_object)

  def prepare_presentation_elements_lang_update(self):

    super().prepare_presentation_elements_lang_update()

    if 'FormDef' in self.NewItem:    
      self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
    
  def prepare_presentation_elements_lang_insert(self,new_element=False):

    super().prepare_presentation_elements_lang_insert(new_element)

    if 'FormDef' in self.NewItem:    
      self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])