######################## ktTaskDatasetList processing module ###############################

###############################################################################

from django.core.validators import ValidationError
from django.conf import settings
from django.core.management import sql, color
import operator
from django.db import connection
from django.forms.models import model_to_dict
from django.db import models
from django.db.models import Prefetch
from katalyonode.models.models import ResourceDef,PresentationElements,PresentationElementsLang,TaskInitiationInstanceToResource,TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskExecutionInstances
from katalyonode.models.models import TaskDefWidgetDependencies
from django.contrib.contenttypes.models import ContentType
from mutant import models
from django.contrib.auth.models import User, Group
import pdb
import json
import string
from datetime import datetime,timedelta
from django.utils import timezone
from functools import reduce
from django.db.models.functions import Concat
from django.utils.dateparse import parse_datetime,parse_date,parse_time
from django.db.models import Q
from katalyonode.serializers.serializerstasks import TaskParametersSerializer
from katalyonode.serializers.serializersresources import MutantModelSerializer,ResourceFormOutSerializer
import hashlib
from katalyonode.models.models import ResourceDefLang,TaskAssignments,PresentationElementsTasks,TaskExecutionInstanceToResource,CodesDetail,TaskInitiationInstance,AuditTrailTaskStatus,TaskCodesActions
from katalyonode.models.models import DatasetMappingDetail,DatasetMappingHead,TaskExecutionInstanceToResource,ResourceModelDefinition
from katalyonode.functions.dynamicmodels import mutant_insert_data,mutant_get_model,mutant_get_data_filtered_in,mutant_update_data,mutant_get_data_filtered,mutant_insert_data_m2m#,mutant_update_data_m2m
from katalyonode.functions.dynamicmodels import mutant_insert_data_m2m_no_delete,mutant_get_m2m_data,mutant_insert_data_m2m_v2,mutant_get_m2m_data_in
from katalyonode.serializers.serializerssettings import GetStatusCodeByName
from katalyonode.functions.blockchainprocessing import send_transaction,get_transaction,get_record_from_blockchain,get_table_rows
from katalyonode.functions.resourceutilityfn import SetResourceStatus
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass

def ProcessElementDefinition(layout,params):
        
    return layout

def CheckForChanges(item_dict,new_item):
    has_changes=False
    #pdb.set_trace()
    if 'FormDef' in item_dict and 'wItemValue' in new_item:
        if item_dict['FormDef']!=new_item['wItemValue']:
            has_changes=True
    return has_changes

class ktInitiateTaskWidget (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = False