"""
ktDate widget

textbox widget

"""

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from datetime import datetime,timezone
from django.utils.dateparse import parse_datetime
from datetime import datetime
import pdb
import json


class ktDate (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
   


    def get_definition(self):
    
        return self.get_element_json()

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {
        
        'ItemId':self.ResourceModelObject.id,
        'UseSystemDate':self.PresentationElementObject.UseSystemDate,
        'ParametersLang': json.loads(self.PeLangObject.ParametersLang or '{}')

        }
        self.WidgetDict.update(widget_object)

    def check_changes_new(self):
         
        has_changes=False
        OLD=0  
        NEW=1
        changes_detail=''
        fields = [('ReadOnly','ReadOnly'),('ShowField','ShowField'),('RequiredField','RequiredField'),('UniqueId_id','UniqueId'),('MaxLength','MaxLength'),('Label','Label')]
        old_item = self.get_definition_json()
        #check fields for changes
        for field in fields:
               has_changes =  self.check_equal(field[OLD],field[NEW],old_item,self.NewItem)
               if (has_changes):
                    break

        return has_changes



    def process_widget(self,form_data,rmd,pe=None):

        
        field_name = rmd['FieldName']
        value = None
        if rmd['UseSystemDate'] is not None:
          if rmd['UseSystemDate']:
            if field_name in form_data:
              form_data[field_name] = None
            else:
              form_data.update({field_name:None})
        
        widget_value = None
        
        if field_name in form_data:
          if form_data[field_name]!=None:
            if type(form_data[field_name]) is dict:
               if 'id' in form_data[field_name]:
                 value = json.loads(form_data[field_name])
                 value = value['id']
               elif 'value' in form_data[field_name]:
                 value = json.loads(form_data[field_name])
                 value = value['value']
            else:
              value = form_data[field_name]
                
          filter_value={}
          
          if value is not None and type(value)==str():
            if rmd['FieldType']=='date':
              value=datetime.strptime(value[0:10], "%Y-%m-%d")
            else:
              value=datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%fZ")
          format1 = format2 = ''
          
          if 'format1' in rmd:
            format1=rmd['format1']
            if 'name' in format1:      format1=format1['name']
              
            if value is not None:
              if hasattr(value,'year'):              filter_value[field_name+'__year']=  value.year
              if hasattr(value,'month'):             filter_value[field_name+'__month']=  value.month
              if hasattr(value,'day'):               filter_value[field_name+'__day']=  value.day
                      
          if 'format2' in rmd:
            format2=rmd['format2']
            if 'name' in format2:    format2=format2['name']
            if value is not None:
              if hasattr(value,'hour'):              filter_value[field_name+'__hour']=  value.hour
              if hasattr(value,'minute'):            filter_value[field_name+'__minute']=  value.minute
              if hasattr(value,'second'):            filter_value[field_name+'__second']=  value.second
               
          format_date = format1+' '+format2
          format_date = format_date.strip()

          if rmd['UseSystemDate'] is not None:
              if rmd['UseSystemDate']:
                widget_value={'type':'systemdate','value':value,'widget_name':rmd['DirectiveName'],'format1':format1,'format2':format2,'filter_value':filter_value}
              else:
                widget_value={'type':rmd['GenericType'],'value': value,'widget_name':rmd['DirectiveName'],'format1':format1,'format2':format2,'filter_value':filter_value}
                
          else:
            widget_value={'type':rmd['GenericType'],'value':value,'widget_name':rmd['DirectiveName'],'format':format_date,'filter_value':filter_value}

        return widget_value

    def generate_query (self,field):
        
        
        if field['type']=='systemdate':
              
          value = timezone.now()
              
          if field['format2']=='' or field['format2'] is None:
                
            #value = value.replace(hour=0, minute=0, second=0, microsecond=0)-timedelta(hours=2)
            field['value'] = value
                
        return field

    def process_widget_search(self,item,filter_data):
      
      r_val,f_data,sel_rel,param=super().process_widget_search(item,filter_data)

      updateWithDefault = item.get('UpdateWithDefault',False)
      input_value = r_val.get('value',None)
      if input_value is not None:
        if not updateWithDefault:
          if type(input_value) is dict:
            if 'value' in input_value:
              input_value = json.loads(input_value)
              value = input_value['value']
            else:
              value=None
          else:
            value = input_value
            field_type=item.get('FieldType','date')
            if value is not None and type(value)!=list:
              if 'UseSystemDate' in item:
                if item['UseSystemDate']:
                  r_val.update({'type':'systemdate','value':datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%fZ")}) #parse_datetime(value)}
                else:
                  if field_type=='date':
                    r_val.update({'type':item['ElementType'],'value':datetime.strptime(value[0:10], "%Y-%m-%d")})
                  else:
                    r_va.update({'type':item['ElementType'],'value':datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%fZ")})
              else:
                
                if field_type=='date':
                  r_val.update({'type':item['ElementType'],'value':datetime.strptime(value[0:10], "%Y-%m-%d")})
                else:
                  r_val.update({'type':item['ElementType'],'value':datetime.strptime(value, "%Y-%m-%dT%H:%M:%S.%fZ")})

            search = r_val.get('search',None)
            if search is not None: 
                filter_type=search.get('filter',None)
                if filter_type == 'between':
                  if field_type=='date':
                    search['between1']= datetime.strptime(search['between1'][0:10], "%Y-%m-%d")
                    search['between2'] = datetime.strptime(search['between2'][0:10], "%Y-%m-%d")
                  else:
                    search['between1']= datetime.strptime(search['between1'], "%Y-%m-%dT%H:%M:%S.%fZ")
                    search['between2'] = datetime.strptime(search['between2'], "%Y-%m-%dT%H:%M:%S.%fZ")
                
                if r_val:    
                    r_val.update({'search':search})

      return r_val,f_data,sel_rel,param

    def prepare_data_search(self,field):

      mainResource={}
      m2mData=[]
      if field[1]['type']=='systemdate':
        value = datetime.now(timezone.utc)
      else:
        value=field[1].get('value')

      if (value is not None): 
        mainResource[field[0]]=value                                 
      
      return mainResource,m2mData