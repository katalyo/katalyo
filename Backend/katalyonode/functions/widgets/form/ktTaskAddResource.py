######################## ktTaskAddResource processing module ###############################

###############################################################################

import pdb
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
import importlib,json
from datetime import datetime,timezone
from katalyonode.functions.resources.datasetdef import dataset
#from katalyonode.models.models import PublishedResources,TaskInitiationInstanceToResource,TaskExecutionInstanceToResource,PresentationElements,ResourceModelDefinition
from django.conf import settings
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PublishedResourcesSa,TaskInitiationInstanceToResourceSa,TaskExecutionInstanceToResourceSa
import os
from katalyonode.settings import PUBLISH_DIR

class ktTaskAddResource (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.Dataset = None
    self.HasResourceModel = False
    self.GenerateCss = True

  def populate_definition_json(self):

    super().populate_definition_json()
    widget_object = {
        'PopulateType': self.PresentationElementObject.PopulateType,
        'Related':self.PresentationElementObject.Related,
        'HasParentDataset':self.PresentationElementObject.HasParentDataset,
        'PopulateParentMethod':self.PresentationElementObject.PopulateParentMethod,
        'PopulateTypeParent':self.PresentationElementObject.PopulateTypeParent,
        'ParentDatasetId':self.PresentationElementObject.ParentDatasetId,
        'SrcWidgetId':self.PresentationElementObject.SrcWidgetId,
        'SrcParentWidgetId':self.PresentationElementObject.SrcParentWidgetId,
        'ResponseMsg':self.PeLangObject.ResponseMsg,
        'FormDef':json.loads(self.PeLangObject.FormDef or '[]'),
    }
    self.WidgetDict.update(widget_object)


  def ProcessElementDefinition(layout,params):
      
      #raise Exception('ProcessElementDefinition fn TEST exception raised')

      #get presentation elements record
      layoutLocal = layout['layout']
      if layoutLocal['Related'] is None and layoutLocal['Status']=='Active':
          raise Exception('Add dataset record (Widget id = '+str(layoutLocal['PresentationId'])+') - error in definition  --> Dataset must be selected ')
      
      if layoutLocal['PresentationId'] is not None:
        session = self.ParentResource.Session
        #pe = PresentationElements.objects.get(id=layoutLocal['PresentationId'])
        pe = session.get(PresentationElements,publishId)
        if not pe:
          raise Exception('Add dataset record - element definition not found --> id = '+str(layoutLocal['PresentationId']))
        else:
          if layoutLocal['Related'] != pe.Related: 
            updateStatus = SetResourceStatus(params['resource_id'],params['form_type'],params['override_type'],layoutLocal['PresentationId'],params['current_user'],params['new_version'],'NotActive')
            if not updateStatus:
              raise Exception('Add dataset widget - update status failed --> id = '+str(layoutLocal['PresentationId']))    
      
      #check if 
      
      return layout

  def process_widget(self,widget_data,p_element,pe_lang,task_instance_id):
  
    parentDatasetDefId = None
    parentDatasetId = None

    self.Dataset = dataset(p_element.Related,'dataset','','','','',self.ParentResource.User,self.ParentResource.Language,self.ParentResource.OrganisationId,{})
    self.Dataset.set_session(self.ParentResource.Session)
    if type(self.PresentationElementObject.Parameters) == str:
      parameters = json.loads(self.PresentationElementObject.Parameters)
    else:
      parameters = self.PresentationElementObject.Parameters
      
    publishId = parameters.get('PublishedResourceId',None)

    session = self.ParentResource.Session
    #get published dataset
    
    publish = session.query(PublishedResourcesSa).filter(PublishedResourcesSa.id==publishId).all()

    rmds = None
    resource_params = None
    for resource in publish:
      rmds = json.loads(resource.Definition)
      resource_params = json.loads(resource.Parameters)

    #pes = PresentationElements.objects.filter(ResourceDefId_id=p_element.Related,ParentWidgetId_id=p_element.id,FormType=self.ParentResource.FormType,OverrideType='w',Status='Active',ResourceModelDefinitionId__ResourceDefId_id=p_element.Related,ResourceModelDefinitionId__Status='Active').select_related('ResourceModelDefinitionId').prefetch_related(Prefetch('presentationelementslang_set', queryset=PresentationElementsLang.objects.filter(Lang=self.ParentResource.Language)))
  
    mainResource,m2mData = self.Dataset.generate_query(self.Dataset.process_form_data(widget_data,rmds))
  
    user_id = None
    if self.Dataset.User is not None:
      user_id = self.Dataset.User.id
    record = self.Dataset.rdbms_cls.insert_data(mainResource,self.Dataset.id,self.Dataset.OrganisationId,user_id)
  

    if record and len(m2mData)>0:
      mutantm2m =  self.Dataset.rdbms_cls.insert_data_m2m_v2(record.id,self.ParentResource.OrganisationId,m2mData,self.ParentResource.User,False)
  
 
  
    jsonResponseBChain=None
    jsonResponseBChain2=None

    if resource_params and settings.USE_BLOCKCHAIN:
      if resource_params['IsBlockchain']:
      #add record to blockchain
     
        datasetData = json.dumps(widget_data)
        h =  hashlib.sha512(datasetData.encode()).hexdigest()
        user = 'user'
        p_key=int(str(p_element.Related)+str(record.id).zfill(12))
        trx_json = {'pKey':p_key,'datasetDef_Id':p_element.Related,'datasetRec_Id':record.id,'dataset_Data': datasetData,'h':h,'v':0,'user':self.ParentResource.User.id}
        contract = 'kataly1.code'
        action='createds'
      
        jsonResponseBChain,block_id,status = self.send_transaction(trx_json,contract,action)
 
        bchain_id= int(jsonResponseEOS['processed']['action_traces'][0]['console'])
 
        jsonResponseBChain2,status2 = self.get_table_rows('kataly1.code','dataset','kataly1.code','primary','uint64_t',bchain_id)
 
        #save transaction id
        if status==200:
          mainResource['id'] = record.id
          mainResource['TrxId'] = jsonResponseEOS['transaction_id']
          mainResource['BChainRecId'] = bchain_id
          mainResource['BlockId'] = str(block_id)
          result = self.rdbms_cls.update_data(mainResource,p_element.Related,self.ParentResource.OrganisationId,self.ParentResource.User.id) 
 
    if p_element.HasParentDataset:
      if p_element.PopulateTypeParent!='' and record is not None:
        #if (int(element['parentDatasetId'])>0):
        parentDatasetDefId,parentDatasetId = self.Dataset.process_parent_dataset(p_element,widget_data,self.ParentResource.id,self.ParentResource.PreviousTaskId,task_instance_id,record,parentDatasetDefId,parentDatasetId)
        #pdb.set_trace()
    if record is not None:

      if self.ParentResource.FormType=='i':
        #titr=TaskInitiationInstanceToResource.objects.create( ResourceDefId_id = p_element.Related, TaskInitiateId_id = task_instance_id, ResourceId = record.id,ParentDatasetDefId_id = parentDatasetDefId,
        #                                                  ParentDatasetId = parentDatasetId,PresentationElementUuidRef=p_element.UuidRef,
        #                                                   CreatedBy = self.ParentResource.User, CreatedDateTime=timezone.now())
        user_id = None
        if self.ParentResource.User is not None:
          user_id = self.ParentResource.User.id
        titr=TaskInitiationInstanceToResourceSa(ResourceDefId = p_element.Related, TaskInitiateId = task_instance_id, ResourceId = record.id,PresentationElementUuidRef =p_element.UuidRef,
                                                               ParentDatasetDefId = parentDatasetDefId,ParentDatasetId = parentDatasetId,CreatedBy = user_id, CreatedDateTime=datetime.now(timezone.utc))
        self.ParentResource.Session.add(titr)
      else:
        #titr=TaskExecutionInstanceToResource.objects.create( ResourceDefId_id = p_element.Related, TaskExecuteId_id = task_instance_id, ResourceId = record.id,ParentDatasetDefId_id = parentDatasetDefId,
        #                                      ParentDatasetId = parentDatasetId,PresentationElementUuidRef=p_element.UuidRef, CreatedBy_id = self.ParentResource.User.id, CreatedDateTime=timezone.now())
        titr=TaskExecutionInstanceToResourceSa(ResourceDefId = p_element.Related, TaskExecuteId = task_instance_id, ResourceId = record.id,PresentationElementUuidRef =p_element.UuidRef,
                                                               ParentDatasetDefId = parentDatasetDefId,ParentDatasetId = parentDatasetId,CreatedBy = user_id, CreatedDateTime=datetime.now(timezone.utc))
        self.ParentResource.Session.add(titr)
      ret_value = {'widgetId':p_element.id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':p_element.Related},{'name':'resourceRecordId','value':record.id}]}],'bChainResponse':jsonResponseBChain,'bChainResponse2':jsonResponseBChain2,'taskResourceLink': titr.id}
  
    else:
      ret_value = {'widgetId':p_element.id,'DEI':[{'name':'resourceLink','value':[{'name':'resourceId','value':p_element.Related},{'name':'resourceRecordId','value':0}]}],'bChainResponse':jsonResponseBChain,'bChainResponse2':jsonResponseBChain2,'taskResourceLink':0}

    if pe_lang.ResponseMsg is not None and pe_lang.ResponseMsg!='':
      response_msg = self.process_response_msg(record,pe_lang.ResponseMsg)
      return {'ret_value':ret_value,'record':record,'ResponseMsg':response_msg}

    return {'ret_value':ret_value,'record':record}  
      
  def represents_int(self,s):
      try: 
          int(s)
          return True
      except ValueError:
          return False

  def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLang.FormDefSearch= json.dumps(self.NewItem['FormDefSearch'])
        if 'ResponseMsg' in self.NewItem:    
            self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        #if 'GridState' in self.NewItem:    
            #self.PeLang.GridState=self.NewItem['GridState']

  def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLangDict['FormDefSearch']= json.dumps(self.NewItem['FormDefSearch'])
        if 'ResponseMsg' in self.NewItem:    
            self.PeLangDict['ResponseMsg']= self.NewItem['ResponseMsg']

  def process_response_msg(self,record,responseTemplate):
    
    responseMsg=responseTemplate
    for element_key,element_val in record.items():
      if element_val is not None:
        old_str = '<<'+str(element_key)+'>>'
        responseMsg = responseMsg.replace(old_str,str(element_val))
    return responseMsg

  def convert_to_json(self,strVal):

      if '{' and '}' in strVal:
        strVal = strVal.replace('"','')
        strVal = strVal.replace('\'','"')
        strVal = json.loads(strVal)
      return strVal

  def create_widget_css(self,uuidRefParent=None):

    #process entire FormDef
    self.Dataset = dataset(self.NewItem['Related'],'dataset','','','','',self.ParentResource.User,self.ParentResource.Language,self.ParentResource.OrganisationId,{})
    self.Dataset.set_session(self.ParentResource.Session)
    self.UuidRef = self.NewItem.get('UuidRef',None)
    
    if self.UuidRef is not None:
      css_style = self.Dataset.create_widget_css(self.NewItem.get('FormDef',[]),self.UuidRef)
      pub_file=os.path.join(PUBLISH_DIR,self.UuidRef+'.css')

      with open(pub_file, "w+") as outfile:
        outfile.write(css_style)