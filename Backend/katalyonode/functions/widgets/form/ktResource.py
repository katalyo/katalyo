######################## ktResource processing module ###############################

###############################################################################
import pdb
import json
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from katalyonode.models.models import PresentationElements,UserExtendedProperties
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from rest_framework import viewsets
from rest_framework.response import Response
from katalyonode.functions.resources.datasetdef import dataset
from katalyonode.functions.ApiMessages import GetMessage
from katalyonode.functions.rdbms.rdbms import RdbmsClass

rdbms_cls = RdbmsClass()

class ktDeleteChildViewSet(viewsets.GenericViewSet):
    
    #pagination_class = StandardResultsSetPagination
    queryset = PresentationElements.objects.all()
     

    def list(self, request,presentationId,parentDefId,parentRecordId,childRecordId):
        params={}
        
        params['presentation_id'] = int(self.kwargs['presentationId'])  if 'presentationId' in self.kwargs else None
        params['id'] = params['presentation_id']
        params['parent_record_id'] = int(self.kwargs['parentRecordId']) if 'parentRecordId' in self.kwargs else None 
        params['parent_def_id'] = int(self.kwargs['parentDefId']) if 'parentDefId' in self.kwargs else None 
        params['child_record_id'] =  int(self.kwargs['childRecordId']) if 'childRecordId' in self.kwargs else None       
        params['user'] = request.user
        
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (params['user']) 
        params['language'] = language
        params['org_id'] = org_id

        session = rdbms_cls.Session()
        with session.begin():

            resourceWidget = ktResource(params,None,None,None,session)
    
            deleted = resourceWidget.delete_child_relationship(params,session)

            status=200
            ret_msg = GetMessage('ktResource-1',language.Code) or "Child deleted successfully!"
            if (deleted!=1):
                status=500
                ret_msg = GetMessage('ktResource-2',language.Code) or "Error deleting child!"
        
        return Response(data=ret_msg,status=status)

    def create(self, request):
        params={}
        
        params['presentation_id'] = int(self.kwargs['presentationId'])  if 'presentationId' in self.kwargs else None
        params['id'] = params['presentation_id']
        params['resource_id'] = int(self.kwargs['resourceId']) if 'resourceId' in self.kwargs else None 
        params['task_def_id'] =  int(self.kwargs['taskDefId']) if 'taskDefId' in self.kwargs else None       
        params['user'] = request.user
        
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (params['user']) 
        params['language'] = language
        params['org_id'] = org_id
        
        session = rdbms_cls.Session()
        with session.begin():

            resourceWidget = ktResource(params,session)
    
            response,status = resourceWidget.delete_child_relationship(session)
        
        return Response(data='',status=200)


class ktResource (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None,session=None):
        super().__init__(item,parent_resource,parent_widget,new_item,session)
        self.Dataset = None

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {
        'ShowSearchForm': self.PresentationElementObject.ShowSearchForm,
        'PopulateType': self.PresentationElementObject.PopulateType,
        'PresentationType': self.PresentationElementObject.PresentationType,
        'NameField': json.loads(self.PresentationElementObject.NameField or '[]'),
        'GridState': json.loads(self.PeLangObject.GridState or '{}'),
        'FormDef': json.loads(self.PeLangObject.FormDef or '[]'),

        }
        self.WidgetDict.update(widget_object)

    
    def process_widget(self,form_data,rmd,pe=None):

        resource_value = []
        widget_value=None
        field_name = rmd['FieldName']
        if field_name in form_data:
            if type(form_data[field_name]) is list:
              for item in form_data[field_name]:
                if type(item) is dict:
                  if 'id' in item:
                    resource_value.append(item['id'])
                  elif 'value' in item:
                    resource_value.append(item['value'])
            elif type(form_data[field_name]) is dict:
              item=form_data[field_name]
              if 'id' in item:
                 resource_value.append(item['id'])
              elif 'value' in item:
                 resource_value.append(item['value'])
            else:
              resource_value=form_data[field_name]
        
        if resource_value is not None and rmd['MaxLength']==1 and resource_value!=[{}]:
            widget_value={'type':rmd['GenericType'],'widget_name':rmd['DirectiveName'],'max_length':rmd['MaxLength'],'value':{'resourceDefId1':self.ParentResource.id,'resourceDefId2': rmd['Related'],'resourceId':resource_value}}

        return widget_value

    def generate_query(self,field):
        
        temp_value = field.get('value',None)
        value = None
        m2mData=None
        if temp_value and field['max_length']==0:
            m2mData = temp_value
        """   
        elif temp_value and field['max_length']==0:
            
            if type(temp_value['resourceId']) is list:
                if len(temp_value['resourceId'])>0:
                    value=temp_value['resourceId'][0]
                else:
                    value=temp_value['resourceId']
        """

        field['value'] = value
        field['m2mData'] = m2mData

        return field

    def prepare_resource_model_insert(self):

        super().prepare_resource_model_insert()
        
        if self.NewItem['Related'] is not None:
            self.ResourceModelDict['FieldType'] = 'resource'+str(self.NewItem['Related'])+'_'+str(self.ParentResource.OrganisationId)
        else:
            self.ResourceModelDict['FieldType'] = 'resource'+self.NewItem['ElementType'] +'_'+str(self.ParentResource.OrganisationId)
        self.NewItem['FieldType'] =  self.ResourceModelDict['FieldType']

        if type(self.PresentationElementsDict['Parameters'])==str:
            parameters = json.loads(self.PresentationElementsDict['Parameters'])
        else:
            parameters = self.PresentationElementsDict['Parameters']

        if parameters.get('ResourceRelationshipType','')=='existing':
            self.ResourceModelDict['GenericType'] = parameters.get('ResourceRelationshipType','')+'_resource'            
            self.ResourceModelDict['Dbtype'] = 'existing_'+self.NewItem['Dbtype']

    def prepare_resource_model_update(self):

        super().prepare_resource_model_update()
        
        if self.NewItem['Related'] is not None:
            self.ResourceModelDict['FieldType'] = 'resource'+str(self.NewItem['Related'])+'_'+str(self.ParentResource.OrganisationId)
        else:
            self.ResourceModelDict['FieldType'] = 'resource'+self.NewItem['ElementType'] +'_'+str(self.ParentResource.OrganisationId)
        self.NewItem['FieldType'] =  self.ResourceModelDict['FieldType']

      
        if 'Dbtype' in self.NewItem:
            if self.NewItem['Dbtype']!=self.ResourceModelObject.Dbtype and self.ResourceModelObject.Dbtype[0:10]=='manytomany':
                self.ResourceModelDict['Action'] = 'A'

    def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'GridState' in self.NewItem:    
            self.PeLang.GridState= json.dumps(self.NewItem['GridState'])
        #if 'ResponseMsg' in self.NewItem:    
            #self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        #if 'GridState' in self.NewItem:    
            #self.PeLang.GridState=self.NewItem['GridState']

    def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'GridState' in self.NewItem:    
            self.PeLangDict['GridState']= json.dumps(self.NewItem['GridState'])

    def get_element_value(self,pe,item,params):

        qs_field_name= pe['FieldName']
        field_name=qs_field_name+'l'

        if pe['ItemId'] in m2mIds:
            if (m2mIds [pe['ItemId']]) is None:
                m2mIds [pe['ItemId']] = []
            else:
                m2mIds.update({pe['ItemId']:[]})

        if rmd['MaxLength']>0:  
            m2mData =  self.ParentResource.rdbms_cls.get_m2m_data(self.id,pe['Related_id'],item['id'],pe['ItemId'],self.ParentResource.OrganisationId,0)


            if m2mData is not None:
                for e in m2mData:
                    if e['ResourceRelatedId'] not in m2mIds[pe['ItemId']]:
                        m2mIds[pe['ItemId']].append(e['ResourceRelatedId'])

                value = m2mIds[pe['ItemId']]
            else:
                value = [recDict[qs_field_name]]

    def process_widget_search(self,item,filter_data):
        resource_value = []
        r_val,f_data,sel_rel,param=super().process_widget_search(item,filter_data)
        updateWithDefault = item.get('UpdateWithDefault',False)
        input_value = r_val.get('value',None)
        fieldId = item.get('FieldName',None)
        if not updateWithDefault:

            if type(filter_data) is list:
                for felem in filter_data:
                    if felem is not None:
                        if fieldId in felem:
                            if type(felem[fieldId]) is list:
                                for elem in felem[fieldId]:
                                    if type(elem) is dict:
                                        if 'id' in elem:
                                            resource_value.append(elem['id'])
                                        elif 'value' in elem:
                                            resource_value.append(elem['value'])
                                        elif type(elem)==int:
                                            resource_value.append(elem)  
                                        elif type(felem[fieldId]) is dict:
                                            if 'id' in felem[fieldId]:
                                                resource_value.append(felem[fieldId]['id'])
                                            elif 'value' in felem[fieldId]:
                                                resource_value.append(felem[fieldId]['value'])
                                            else:
                                                resource_value=felem[fieldId]
                    
            else: #type(filter) is NOT list
                if fieldId in filter_data:
                    if type(filter_data[fieldId]) is list:
                        if filter_data[fieldId] is not None:
                            for elem in filter_data[fieldId]:
                                if type(elem) is dict:
                                    if 'id' in elem:
                                        resource_value.append(elem['id'])
                                    elif 'value' in elem:
                                        resource_value.append(elem['value'])
                                elif type(elem)==int:
                                    resource_value.append(elem)  
                                elif type(filter_data[fieldId]) is dict:
                                    if 'id' in filter_data[fieldId]:
                                        resource_value.append(filter_data[fieldId]['id'])
                                    elif 'value' in filter_data[fieldId]:
                                        resource_value.append(filter_data[fieldId]['value'])
                                    else:
                                        resource_value=filter_data[fieldId]
            if len(resource_value)>0:
                r_val={'type':item['ElementType'],'max_length':item['MaxLength'],'value':{'resourceDefId': item['Related'],'modelId':item_id,'resourceId':resource_value}}
            
            join=None
            
            #build join here
            parameters =item.get('Parameters',None)
            
            field_name = item.get('FieldName',None)

            if parameters is not None:

                if parameters.get('ResourceRelationshipType','')=='existing':
                    field_name = None
                    if item['MaxLength']==1:
                        field_name=item.get('Parameters',{}).get('FieldNameRelatedParent',None)
                    else:
                        field_name=item.get('FieldName',None)
                    
                    if field_name is not None:
                        join = {'MaxLength':item['MaxLength'],'Related':item['Related'],'ResourceDefId':self.ParentResource.id,'FieldName':field_name}
            
                else:

                    if item['MaxLength']==0 and parameters.get('LinkChildren',False):
                        join = {'MaxLength':item['MaxLength'],'Related':item['Related'],'ResourceDefId':self.ParentResource.id,'FieldName':field_name}    

            finalFilter={}
            if item['PresentationType']=='0' or item['PresentationType']=='2': #item['PresentationType']=='0' or 
                #process searh form
                formDefinition = item.get('FormDef',[])
                if type(filter_data)==dict:
                    formData = filter_data.get(field_name,None)
                else:
                    formData={}
    
                rData,f,select_related,parameters = self.ParentResource.extract_form_data_search(formData,formDefinition)
                finalFilter,m2mData = self.ParentResource.process_resource_data_search(rData)
            
            if join is not None:
                join['filter_data'] = finalFilter
                sel_rel.append(join)

        return r_val,filter_data,sel_rel,param



    def prepare_data_search(self,field):

        mainResource={}
        m2mData=[]
        value = field[1].get('value')
        max_length = field[1].get('max_length',None)
        if max_length is not None:
            if value and max_length>0:
                m2mData.append(value)
            elif value and max_length==0:
                if value['resourceId'] is not None:
                    if type(value['resourceId'])==int:
                        value['resourceId']=[value['resourceId']]
                        if len(value['resourceId'])>0:
                            mainResource[field[0]+'__in']=value['resourceId']
        
        ids_from_related={}
        resource_record_ids_final = []

        for m2m in m2mData:
          
            if m2m['modelId'] not in ids_from_related:
                ids_from_related[m2m['modelId']]=[]
              
            if m2m['resourceId'] is not None:
            
                if type(m2m['resourceId'])==int:
                    m2m['resourceId']=[m2m['resourceId']]
             
                if len(m2m['resourceId'])>0:
                    resource_record_ids=[]
                    for datasetItem in m2m['resourceId']:
                        if type(datasetItem)== int:
                            resource_record_ids.append(datasetItem)
                        elif type(datasetItem)== dict:
                            if 'id' in datasetItem:
                                resource_record_ids.append(datasetItem['id'])

                    relRec = self.rdbms_cls.get_m2m_data_in(resource_id,m2m['resourceDefId'],resource_record_ids,m2m['modelId'],org_id,1)
                    if relRec is not None:
                        for rr in relRec:
                            if rr.ResourceSourceId is not None:
                                ids_from_related[m2m['modelId']].append(rr.ResourceSourceId)
          
                                if len(ids_from_related[m2m['modelId']])>0:
                                    if 'id__in' in mainResource:
                                        if mainResource['id__in'] is not None:
                                            mainResource['id__in'] = list(set(mainResource['id__in']) & set(ids_from_related[m2m['modelId']]))
                                        else:
                                            mainResource['id__in'] = ids_from_related[m2m['modelId']]        
                                    else:
                                        mainResource['id__in'] = ids_from_related[m2m['modelId']]
               
                                elif len(resource_record_ids)>0:
                                    mainResource['id__in'] = ids_from_related[m2m['modelId']]

        return mainResource,m2mData

    def delete_child_relationship(self,params,session=None):
        
        parent_def_id=params.get('parent_def_id',None)
        parent_record_id=params.get('parent_record_id',None)
        child_record_id=params.get('child_record_id',None)
        self.Dataset = dataset(params['parent_def_id'],'dataset','','','','',params['user'],params['language'],params['org_id'],{})
        self.Dataset.set_session(session)
        child_def_id = self.PresentationElementObject.Related

        del_rec = self.Dataset.rdbms_cls.delete_child_m2m(parent_def_id,parent_record_id,child_def_id,child_record_id,self.Dataset.OrganisationId,self.Dataset.User)
        return del_rec

       
