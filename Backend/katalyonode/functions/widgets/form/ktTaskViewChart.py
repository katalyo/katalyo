######################## ktTaskViewChartViewSet processing module ###############################

###############################################################################

import json
import pdb
from katalyonode.models.models import ResourceDefLang
from rest_framework import viewsets
from rest_framework.response import Response
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass 
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.resources.datasetdef import dataset
from katalyonode.models.models import ResourceDefLang,PresentationElements
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa
from sqlalchemy import and_

    
class ktTaskViewChartViewSet(viewsets.GenericViewSet):
    
    queryset = ResourceDefLang.objects.all()
     
    def list(self, request,resourceId,presentationId,taskDefId,formType,lang):
       
        #ToDo this should be part of the pre_execute task process

        params={}
        params['presentation_id'] = int(self.kwargs['presentationId'])  if 'presentationId' in self.kwargs else None
        params['id'] = params['presentation_id']
        params['resource_id'] = int(self.kwargs['resourceId']) if 'resourceId' in self.kwargs else None 
        params['form_type'] =  self.kwargs['formType']  if 'formType' in self.kwargs else 'i'   
        params['task_def_id'] =  int(self.kwargs['taskDefId']) if 'taskDefId' in self.kwargs else None       
        params['user'] = request.user
        
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (params['user']) 
        params['language'] = language
        params['org_id'] = org_id

        ktViewChart= ktTaskViewChart(params)
        response = ktViewChart.process_widget_execute(params)
        results = response['selectedRecords']
           
        return Response(  
            {'chart':response['chart'],
            'resourceDefinition':response['resourceDefinition'],
            'widgets':response['formDefinition'],
            'widgetsCard':response['formDefinitionCard'],
            'formData':response['formData'],
            'columns':response['columns'],
            'columnsForQuery':response['columnsForQuery'],
            'data':results} )


class ktTaskViewChart (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
            super().__init__(item,parent_resource,parent_widget,new_item)
            self.HasResourceModel = False
            self.Dataset = None

    def populate_definition_json(self):

        super().populate_definition_json()
        #pdb.set_trace()
        
        widget_object = { 
            'AutoSearch':self.PresentationElementObject.AutoSearch,
            'ParametersLang':json.loads(self.PeLangObject.ParametersLang or '{}'),
            'FormDef':json.loads(self.PeLangObject.FormDef or '[]'),
            'FormDefSearch':json.loads(self.PeLangObject.FormDefSearch or '{}'),
            'GridState':json.loads(self.PeLangObject.GridState or '{}'),
            'ShowSearchForm':self.PresentationElementObject.ShowSearchForm,
            'PopulateMethod':self.PresentationElementObject.PopulateMethod,
        }
        self.WidgetDict.update(widget_object)



    def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
                self.PeLang.FormDefSearch = json.dumps(self.NewItem['FormDefSearch'])
        if 'GridState' in self.NewItem:    
            self.PeLang.GridState= json.dumps(self.NewItem['GridState'])


    def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
                self.PeLangDict['FormDefSearch'] = json.dumps(self.NewItem['FormDefSearch'])
        if 'GridState' in self.NewItem:    
            self.PeLangDict['GridState']= json.dumps(self.NewItem['GridState'])



    def process_widget_execute(self,params):
    
        if 'paginate' in params:
            paginate = params['paginate']
        else:
            paginate = False
        
        self.Dataset = dataset(params['resource_id'],'dataset','','','','',params['user'],params['language'],params['org_id'],{})
        self.Dataset.rdbms_cls.SetOrganisation(self.Dataset.OrganisationId)
        self.Dataset.set_session(self.ParentResource.Session)
        resource_id = params['resource_id']
        presentation_id = params['presentation_id']
        form_type = params['form_type']
        language = params['language']
        task_def_id = params['task_def_id']
        user = params['user']
        
        pElement = self.ParentResource.Session.query(PresentationElementsSa,PresentationElementsLangSa).join(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==language.id)).filter(PresentationElementsSa.id==presentation_id).all()
    
    
        p_element = None
        p_element_lang = None
        for pe in pElement:
            p_element = pe[0]
            p_element_lang = pe[1]      
        
        if p_element is None:
            raise Exception(data='Widget definition number '+(presentation_id)+' is not found.')
       
        parent_initiate_id = 0
        src_task_instance_id=0
        parent_initiate_id = presentation_id
        parent_id= 0
        populate_type=p_element.PopulateType
        resource_record_id = 0
      
        if type(p_element.Parameters)==str:
            parameters = json.loads(p_element.Parameters)
        else:
            parameters=None
        if parameters is not None:
            publish_id = parameters['PublishedResourceId']
        else:
            raise Exception('Published resource error --> not found') 


        helper = ResourceHelperClass()

        published_resource = helper.get_published_resource(publish_id)
        
        #pdb.set_trace()
        if self.PeLangObject.FormDefSearch is not None:
            if type(self.PeLangObject.FormDefSearch)==str:
                search_form = json.loads(self.PeLangObject.FormDefSearch)
                formDefinition = search_form.get('SearchForm',[])
            else:
                formDefinition = self.PeLangObject.FormDefSearch.get('SearchForm',[])
        else:
            formDefinition = published_resource['Form']

        resourceDefinition = published_resource['Parameters']

        p_elements = published_resource['Definition']
        
        #GET DATA
        formData,recordId = self.Dataset.get_dataset_form_data(p_element,p_elements,src_task_instance_id,parent_id,populate_type,resource_record_id)

        results = None
        selectedRecords=[]
        queryset=[]
        filter = None
        grid_state = {}
        
        if p_element_lang.GridState is not None:
            grid_state=json.loads(p_element_lang.GridState)
        
        columns = grid_state.get('columnsDef',[])
        columns_for_select = grid_state.get( 'columnsQuery',[])
        #self.getPandas()
        
        if p_element.AutoSearch: #if auto search SERVER is checked 
            #TODO - uvesti zastitu ako je stavljen autosearch a nema filtera ... onda se cijela tablica i povezane skidaju !!
            rData,f,select_related, parameters = self.Dataset.extract_form_data_search(formData,formDefinition)
            filter = f           
            resourceData,m2mData = self.Dataset.process_resource_data_search(rData)
            selectedRecords = self.Dataset.get_dataset_data_filtered(resourceData,columns_for_select,select_related,parameters={})
            
            if not paginate:
                #pdb.set_trace()
                selectedRecords = self.Dataset.get_dataset_data_filtered(filter,None,queryset,None)
                #nameDefs = PresentationElements.objects.get(id=presentation_id)
                p_element = self.ParentResource.Session.query(PresentationElementsSa).filter(PresentationElementsSa.id==presentation_id).all()[0]
                if type(p_element.Parameters)==str:
                    parameters = json.loads(p_element.Parameters)
                else:
                    parameters=None
                
                #pdb.set_trace()
                
                dict_resource_type={}
                for i in published_resource['Definition']:  
                    dict_resource_type[i['FieldName']] = i['FieldType']
                
                aggregateField = parameters['AggregateField']['FieldName']
                calculateField = parameters['CalculateField']['FieldName']
                calculateFunction = parameters['CalculateFunction']['name']
                chartType = parameters['chartType']['name']
                results = { 'sum': {}, 'count': {}, 'max': {}, 'min': {}, 'mean': {}} 
                    
                for rec in selectedRecords: #{'sum'},{'mean'},{'min'},{'max'},{'count'},
                    agg = rec[aggregateField]
                    if  dict_resource_type[aggregateField] =='codesdetail':
                        if rec[aggregateField] is not None: 
                            agg = rec[aggregateField]['name']                          
                       
                    results['sum'][agg] =  results['sum'].get(agg,0) + rec[calculateField]
                    results['count'][agg] =  results['count'].get(agg,0) + 1      
                    results['count'][agg] =  results['count'].get(agg,0) + 1      
                    if rec[calculateField] >= results['max'].get(agg, rec[calculateField] ) :
                        results['max'][agg] = rec[calculateField]
                    if rec[calculateField] <= results['min'].get(agg, rec[calculateField] ) :
                        results['min'][agg] = rec[calculateField]
                for key,value in results['sum'].items():
                    results['mean'][key] =  results['sum'][key] /   results['count'][key]   
    

        formDefinitionCard=None         
        response = {}
        response['chart'] = results
        response['resourceDefinition'] = resourceDefinition
        response['formDefinition'] = formDefinition
        response['formDefinitionCard'] = formDefinitionCard
        response['formData'] = formData
        response['columns'] = columns
        response['columnsForQuery'] = columns_for_select
        response['selectedRecords'] = selectedRecords
        response['queryset'] = queryset
        response['org_id'] = params['org_id']
        response['filter'] = filter
        
        return response

    def getDataWithPandas(resource_id,org_id):

   
        table_name,is_external = get_table_name(resource_id,org_id)
        data = pandas.read_sql_table(table_name,rdbms_cls.engine,coerce_float=False)
        #pdb.set_trace() #getDataWithPandas
        
        return data
    
    #def getPandas(self, request,resourceId,presentationId,taskId,formType, lang):
    def getPandas(self):               
        #resource_id = presentation_id = fields = users = groups = codes = fields = name_fields = org_id = None
        #language = None
        rmds =pids = rval = m2mData = []
        filter = resourceData = {}
        isIdIn = False
        session = rdbms_cls.Session()
        with session():
        
            #if 'resourceId' in self.kwargs:   resource_id=int(self.kwargs['resourceId']) 
            #if 'presentationId' in self.kwargs:   presentation_id=int(self.kwargs['presentationId'])
            #if 'lang' in self.kwargs:    language=self.kwargs['lang']
           
            #user=request.user
            #helper = ResourceHelperClass()
            #org_id,language = helper.get_lang_org (user) 
              
            
            #pdb.set_trace() #GetResourceDataPandasViewSet
            #nameDefs = PresentationElements.objects.get(id=presentation_id)
            nameDefs = session.query(PresentationElementsSa).where(PresentationElementsSa.id == presentation_id).first()    
            nameField = nameDefs.NameField   
            if nameField is not None:
              for element in nameField:
                pids.append(element['id']) 
              
            #fields = PresentationElements.objects.filter(id__in=pids) 
            fields = session.query(PresentationElementsSa, ResourceModelDefinitionId)\
                .join(ResourceModelDefinitionId, PresentationElementsSa.ResourceModelDefinitionId == ResourceModelDefinitionId.id)\
                .filter(PresentationElementsSa.id.in_(pids)).all()   
            
            concat_name_fields = list()
            if fields is not None:
                name_fields={}
                for (fld, remodef) in fields:
                    column = remodef.FieldName
                    if fld.ElementType=='resourceid':
                        column = 'resource'+str(resource_id)+'_'+str(org_id)+'_id'
                        
                    concat_name_fields.append(column)
                    name_fields[column]=column
            

            selectedRecords =  mutant_get_data_sa(resource_id,org_id,concat_name_fields)      
            
            pdata = getDataWithPandas(nameDefs.Related_id,nameDefs.OrganisationId_id)
            
            try:
                aggregateField = nameDefs.Parameters['AggregateField']['FieldName']
                calculateField = nameDefs.Parameters['CalculateField']['FieldName']
                calculateFunction = nameDefs.Parameters['CalculateFunction']['name']
                chartType = nameDefs.Parameters['chartType']['name']
       
            except KeyError as e:
                    return Response(data='Missing chart properties ' + str(e),status=400) 
             
            if (aggregateField[-3:] == '_id'):
                aggregateField='id'
            
            if calculateFunction == 'sum':
                     result = pdata.groupby([aggregateField])[calculateField].sum()
            if calculateFunction == 'mean':
                    result = pdata.groupby([aggregateField])[calculateField].mean()
            if calculateFunction == 'min':
                    result = pdata.groupby([aggregateField])[calculateField].min()
            if calculateFunction == 'max':
                    result = pdata.groupby([aggregateField])[calculateField].max()
            if calculateFunction == 'count':
                    result = pdata.groupby([aggregateField])[calculateField].count()
             
            rdefs = ResourceDefLang.objects.filter(Lang=language,ResourceDefId__Organisation_id=org_id,ResourceDefId__ResourceType__ResourceCode='').select_related()#.prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang=lang)))

            allFields={}
            usersDict = {}
             
            #columns = get_columns_def (resource_id,taskId,'i',org_id,lang)    
            #formDefinition,resourceDefinition = GetResourceFormDB(resource_id,'i',language,0,0,'w',user,org_id,False)
            #get_fields_recursive(formDefinition,allFields)
            
            users = User.objects.all()
            if len(users)>0:
               for user in users:
                 usersDict[user.id] = user.first_name+ ' ' + user.last_name
            
            # MAKE LABELS
            #pdb.set_trace() #pandas debug
            labels =  result.index.to_list()
            
            rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active').order_by('id')
            for rmd in rmds:
                if rmd.FieldName == nameDefs.Parameters['AggregateField']['FieldName']:
                        #found aggregate rmd
                        if  rmd.GenericType == 'resourceid':
                                pass
                        elif rmd.GenericType == 'list': 
                                codeLang = CodesDetailLang.objects.get(Lang=language,CodesDetailId=rmd.CodeId)  
                        elif rmd.GenericType == 'user':
                                print(usersDict[rmd.CodeId])
                        elif rmd.GenericType == 'resource' :
                                relatedPes = PresentationElements.objects.get(id=nameDefs.Parameters['AggregateField']['NameField']['id'])
                                ret = getResourceDataListAll(rmd.Related,presentation_id,org_id)
                                for item in ret:
                                    labels[item['id']] = item['Field' + str (relatedPes.ResourceModelDefinitionId_id) ]
                        else: #textbox,number...etc..
                             #pdb.set_trace()
                             a=3
                                          
            return Response({'data': result.to_dict(), 'parameters':  nameDefs.Parameters, 'labels': labels})
       
            if selectedRecords is not None:
                return Response(selectedRecords)
            else:
                return Response(status=404)
