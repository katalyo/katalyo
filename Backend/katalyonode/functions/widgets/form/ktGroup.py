"""
ktUser widget

group widget

"""

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass

class ktGroup (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        
    def get_element_value(self,pe,item,params):
        
        field_name = pe['FieldName']
        value = getattr(item,field_name,None)
        if type(value) is str:
            if '{' and '}' in value:
                value = json.loads(value)
                if 'id' in value:
                    value = value['id']
                elif 'value' in value:
                    value = value['value']
                elif type(value) is dict:
                    if 'id' in value:
                        value = value['id']
                    elif 'value' in value:
                        value = value['value']

        if pe['PopulateType'] == 2:
            value = self.ParentResource.User.id
        
        return value
    

    def populate_definition_json(self):

        super().populate_definition_json()

        widget_object = {

        'PopulateType': self.PresentationElementObject.PopulateType,
        #'PresentationType': self.PresentationElementObject.PresentationType,

        }
        self.WidgetDict.update(widget_object)


    def process_widget(self,form_data,rmd,pe=None):

        if field_name in form_data:
            if type(form_data[field_name]) is dict:
                if 'value' in form_data[field_name]:
                    value = json.loads(form_data[field_name])
                    value = value['value']
                else:
                    value = form_data[field_name]

        widget_value={'type':rmd.GenericType,'widget_name':pe.DirectiveName,'value':value}

        return widget_value