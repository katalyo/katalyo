######################## ktTaskForPage widget ###############################

###############################################################################

from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass
from katalyonode.functions.resources.taskdef import task

class ktTaskForPage (WidgetBaseClass):
    
  def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
    super().__init__(item,parent_resource,parent_widget,new_item)
    self.HasResourceModel = False
  def populate_definition_json(self):

    super().populate_definition_json()

    widget_object = {
    
    'ResponseMsg':self.PeLangObject.ResponseMsg,

    }
    self.WidgetDict.update(widget_object)

  #def build_css_style(self):

    #process entire FormDef
    
    #task_obj = task(self.NewItem['Parameters']['taskDef']['id'],'task','','','','',self.ParentResource.User,self.ParentResource.Language,self.ParentResource.OrganisationId,{})
    #self.UuidRef = self.NewItem['UuidRef']
    #pdb.set_trace()
    #if self.UuidRef is not None:
      #css_style = task_obj.generate_form_css(self.NewItem['FormDef'],self.NewItem['UuidRef'])
      #pub_file=os.path.join(PUBLISH_DIR,self.NewItem['UuidRef']+'.css')

      #with open(pub_file, "w+") as outfile:
        #outfile.write(css_style)