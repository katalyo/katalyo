######################## ktActionButtons processing module ###############################

###############################################################################

import json
from katalyonode.functions.widgets.base.widgetbase import WidgetBaseClass

class ktActionButtons (WidgetBaseClass):
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None):
        super().__init__(item,parent_resource,parent_widget,new_item)
        self.HasResourceModel = False

    def populate_definition_json(self):

        super().populate_definition_json()

        if self.PeLangObject.FormDef is None or self.PeLangObject.FormDef=='':
            form_def = []
        else:
            try:
                form_def = json.loads(self.PeLangObject.FormDef)
            except Exception:
                form_def=[]

        widget_object = {
        
        'FormDef':form_def,

        }
        self.WidgetDict.update(widget_object)

    def prepare_presentation_elements_lang_update(self):

        super().prepare_presentation_elements_lang_update()

        if 'FormDef' in self.NewItem:    
                self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLang.FormDefSearch= json.dumps(self.NewItem['FormDefSearch'])
        #if 'ResponseMsg' in self.NewItem:    
            #self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']
        #if 'GridState' in self.NewItem:    
            #self.PeLang.GridState=self.NewItem['GridState']

    def prepare_presentation_elements_lang_insert(self,new_element=False):

        super().prepare_presentation_elements_lang_insert(new_element)

        if 'FormDef' in self.NewItem:    
                self.PeLangDict['FormDef'] = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLangDict['FormDefSearch']= json.dumps(self.NewItem['FormDefSearch'])