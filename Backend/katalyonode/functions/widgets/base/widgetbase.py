"""
Widget base class

this is the base class that all widgets must inherit

"""
import pdb
import json
#from django.core.validators import ValidationError
#from katalyonode.models.models import PresentationElements,PresentationElementsLang,ResourceModelDefinition,PresentationElementsHistory,PresentationElementsLangHistory,ResourceModelDefinitionHistory#,ResourceDef,ErrorLog
#from django.db.models import Prefetch
#from django.forms.models import model_to_dict
#from copy import deepcopy
#from django.utils import timezone
from datetime import datetime,timezone
import uuid
from sqlalchemy import and_
from sqlalchemy import engine
from katalyonode.functions.resources.base.resourcehelper import PresentationElementClass,PeLangClass,ResourceModelDefinitionClass
from katalyonode.functions.rdbms.alchemymodels import UserExtendedPropertiesSa
from katalyonode.settings import PUBLISH_DIR
import os
import hashlib

from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa,ResourceDefSa,ResourceDefLangSa,ResourceModelDefinitionHistorySa
from katalyonode.functions.rdbms.rdbms import RdbmsClass

rdbms_cls = RdbmsClass()


class WidgetBaseClass:
    
    def __init__(self,item,parent_resource=None,parent_widget=None,new_item=None,session=None):             
        self.__value=None
        self.id=None
        self.UuidRef=None
        self.NewItem = new_item #will be PresentationElemenstDict sometimes
        self.ParentResource = parent_resource
        self.ParentWidget = parent_widget
        self.PresentationElementObject = None
        self.PresentationElementsLang = None
        self.PresentationElement = None
        self.PeLang = None
        self.ResourceModel = None
        self.PeLangObject = None
        self.ResourceModelObject = None
        self.WidgetDict = {}
        self.PresentationElementsDict = {}
        self.PeLangDict = {}
        self.ResourceModelDict = {}
        self.HasResourceModel = True
        self.GenerateCss = False
        self.HasChangesPe=False
        self.HasChangesPeLang=False 
        self.HasChangesRmd=False
        self.NoCheckFieldsPe=['id','VersionFrom','VersionTo','Order','ComponentName']
        self.NoCheckFieldsPeLang=['id']
        self.NoCheckFieldsRmd=['id','Version','InitialVersion','Deployed','DeployedVersion']
        
        
        #pdb.set_trace()
        if item is not None: #check item type
            if type(item)==dict:
                if 'id' in item:
                    self.id = item['id']
                    if self.id==0: self.id=None

                    if self.id is not None:
                        language = item.get('language',None)
                        item = self.get_pe_by_id(language,session)
                        if item is None:
                            raise Exception('Widget initialisation error -> item is not found by id')                 
                elif 'PresentationId' in item:
                    self.id = item['PresentationId']
                    
                    if self.id==0: self.id=None

                    if self.id is not None:
                        language = item.get('language',None)
                        item = self.get_pe_by_id(language,session)
                        if item is None:
                            raise Exception('Widget initialisation error -> item is not found by PresentationId')   
                #if self.id is not None:
                    #self.populate_db_items(item)
            elif type(item) == tuple or 'Row' in str(type(item)):
                if type(item[0]) == PresentationElementsSa:
                    self.populate_db_items(item)
                elif type(item[0]) == PresentationElementClass:
                    self.PresentationElementObject = item[0]
                    self.PeLangObject=item[1]
                    self.ResourceModelObject=item[2]
                    self.id=self.PresentationElementObject.id
                    self.UuidRef=self.PresentationElementObject.UuidRef
        else: 
            #self.PresentationElementObject = PresentationElementClass(item,self.NewItem)    
            if self.NewItem is not None:
                self.id = self.NewItem.get('PresentationId',None)
                if self.id is not None:
                    if int(self.id)>0:
                        item = self.get_pe_by_id(None,session)
                        if item is None:
                            raise Exception('Widget initialisation error -> id is not found in New item')   
            

    def get_pe_by_id(self,language=None,session=None):

        if language is None:
            if self.ParentResource is not None:
                language = self.ParentResource.Language
            else:
                raise Exception('Widget initialisation error -> language is not available')
       
        #pes = PresentationElements.objects.filter(id=self.id).prefetch_related(Prefetch('presentationelementslang_set',
                   #queryset=PresentationElementsLang.objects.filter(Lang=language))).select_related('ResourceModelDefinitionId').order_by('Order')

        if session is None and self.ParentResource is not None:
            session = self.ParentResource.Session #rdbms_cls.Session()
        
        assert session is not None,'Session error --> session is not available = Null'

        peSA = None
        #with session.begin():
        
            #peFields = session.query(PresentationElementsSa,PresentationElementsLangSa)\
            #.join(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==language.id))\
            #.filter(PresentationElementsSa.id==self.id,PresentationElementsSa.Status=='Active').all()
          
        peFields = session.query(PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa)\
        .outerjoin(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId, PresentationElementsLangSa.Lang==language.id))\
        .outerjoin(ResourceModelDefinitionSa, PresentationElementsSa.ResourceModelDefinitionId==ResourceModelDefinitionSa.id)\
        .filter(PresentationElementsSa.id==self.id).all() 
        
        for pe in peFields:  
            peSA=pe # it should be unique
        
            pe=PresentationElementClass(peSA[0],None)
            pl =PeLangClass(peSA[1])
            #pdb.set_trace()
            rm = ResourceModelDefinitionClass(peSA[2])
            

            self.PresentationElementObject = pe
            self.PeLangObject = pl   
            self.ResourceModelObject = rm   
        return peSA
            
    def populate_db_items(self,item):


        #pdb.set_trace()
        # EXPECTiNG SQL ALCHEMY item OBJECT !!!
        if type(item[0]) not in [PresentationElementsSa] :  # item is SQL Alchemy tuple
            #pdb.set_trace()
            raise  AssertionError('item must be SQL alchemy row tuple!') 
        
        self.PresentationElement = item[0] #populate_db_items
        self.PresentationElementObject = PresentationElementClass(item[0],self.NewItem)
        #self.PresentationElementObject = item[0]
        self.id=self.PresentationElementObject.id
        self.UuidRef=self.PresentationElementObject.UuidRef

        self.PresentationElementsLang = item[1]
        self.ResourceModel = item[2]

        self.ResourceModelObject = ResourceModelDefinitionClass(self.ResourceModel) #pdb.set_trace() #zadnji bug

        self.PeLangObject = PeLangClass(self.PresentationElementsLang)
        

    def prepare_update(self):

        self.prepare_presentation_elements_update()

        self.prepare_presentation_elements_lang_update()

        if self.HasResourceModel and self.ResourceModel is not None:
            self.prepare_resource_model_update() 

    def execute_insert(self,new_element=False):
        
        session = self.ParentResource.Session
        if self.HasChangesPe or self.HasChangesPeLang or new_element:
           
            pe = PresentationElementsSa(**self.PresentationElementsDict)
            session.add(pe)
            session.flush()
         
            if 'LocalId' in self.NewItem and (self.NewItem['PresentationId'] is None or self.NewItem['PresentationId']==0):
                self.ParentResource.NewFormItems[self.NewItem['LocalId']] = pe.id
            elif self.NewItem['PresentationId'] is not None and self.NewItem['PresentationId']>0:
                self.ParentResource.NewFormItems[self.NewItem['PresentationId']] = pe.id
                self.ParentResource.ExcludeFromVersionUpdate.append(self.NewItem['PresentationId'])

            self.NewItem['PresentationId'] = pe.id
            self.NewItem['id'] = pe.id
            self.NewItem['Saved'] = True
            self.PresentationElementsDict['id'] = pe.id
            self.id=pe.id
            self.UuidRef=pe.UuidRef
            self.PeLangDict.update({'PresentationElementsId':pe.id})
            peLang =  PresentationElementsLangSa(**self.PeLangDict)
            session.add(peLang)
            session.flush()
            
            if peLang.id is not None:
                self.NewItem['PresentationLangId'] = peLang.id
                self.PeLangDict['id'] = peLang.id
        else:
            pe = self.PresentationElement

        if self.HasResourceModel: 
            if new_element:
                rmd = ResourceModelDefinitionSa(**self.ResourceModelDict)
                session.add(rmd)
                session.flush()
                if rmd.id is not None:
                    pe.ResourceModelDefinitionId = rmd.id
                    self.ResourceModelDict['id'] = rmd.id
                    self.NewItem['ItemId'] = rmd.id
                    if not self.ParentResource.CustomFields and rmd.FieldName=='Field':
                        rmd.FieldName = rmd.FieldName+str(rmd.id)            
            else:
                rmd = self.ResourceModel


    def execute_update(self):

        if self.HasChangesPe:
            self.PresentationElement.save()

        if self.HasChangesPeLang:
            self.PeLang.save()

        if self.HasResourceModel and self.ResourceModel is not None and self.HasChangesRmd:
           self.ResourceModel.save()

    def prepare_presentation_elements_insert(self,new_element=False): #actually = prepare 'PresentationElementsDict'

        self.PresentationElementsDict['ResourceDefId'] = self.ParentResource.id
        self.PresentationElementsDict['OrganisationId'] = self.ParentResource.OrganisationId
        self.PresentationElementsDict['FormType'] = self.ParentResource.FormType
        self.PresentationElementsDict['ElementType'] = self.NewItem['ElementType']
        if self.ParentWidget is not None:
            #self.PresentationElementsDict['ContainerId_id'] = self.ParentWidget.id #peRow.id
            self.PresentationElementsDict['ContainerElement'] = self.ParentWidget.UuidRef
        if 'AutoSearch' in self.NewItem:
            self.PresentationElementsDict['AutoSearch']= self.NewItem['AutoSearch']
        else:
            self.PresentationElementsDict['AutoSearch'] = False
        if 'ShowSearchForm' in self.NewItem:
            self.PresentationElementsDict['ShowSearchForm'] = self.NewItem['ShowSearchForm']
        else:
            self.PresentationElementsDict['ShowSearchForm'] = False
        if self.ParentWidget is not None:
            self.PresentationElementsDict['Order'] = self.ParentWidget.CurrentIndex
        else:
            self.PresentationElementsDict['Order'] = self.ParentResource.CurrentIndex
        if 'SizeX' in self.NewItem:   
            self.PresentationElementsDict['SizeX'] = self.NewItem['SizeX']    
        else:
            self.PresentationElementsDict['SizeX'] = 0
        if 'SizeY' in self.NewItem:   
            self.PresentationElementsDict['SizeY'] = self.NewItem['SizeY']    
        else:
            self.PresentationElementsDict['SizeY'] = ''
        self.PresentationElementsDict['Tabindex']  =  self.ParentResource.get_tab_index()
        
        if 'SrcWidgetId' in self.NewItem:    
            self.PresentationElementsDict['SrcWidgetId'] = self.NewItem['SrcWidgetId']
        if 'PopulateMethod' in self.NewItem:    
            self.PresentationElementsDict['PopulateMethod'] = self.NewItem['PopulateMethod']
        if 'PopulateType' in self.NewItem:    
            self.PresentationElementsDict['PopulateType'] = self.NewItem['PopulateType']
        else:
            self.PresentationElementsDict['PopulateType'] =''    
        if 'PopulateTypeParent' in self.NewItem:   
            self.PresentationElementsDict['PopulateTypeParent'] = self.NewItem['PopulateTypeParent']
        if 'SrcParentWidgetId' in self.NewItem:    
            self.PresentationElementsDict['SrcParentWidgetId'] = self.NewItem['SrcParentWidgetId']
        if 'PopulateParentMethod' in self.NewItem:    
            self.PresentationElementsDict['PopulateParentMethod'] = self.NewItem['PopulateParentMethod']
        if 'Related' in self.NewItem:    
            self.PresentationElementsDict['Related']= self.NewItem['Related']
        if 'Status' in self.NewItem:    
            self.PresentationElementsDict['Status']= self.NewItem['Status']
        if 'HasParentDataset' in self.NewItem:    
            self.PresentationElementsDict['HasParentDataset']=self.NewItem['HasParentDataset']
        else:
            self.PresentationElementsDict['HasParentDataset']=False
        if 'ParentDatasetId' in self.NewItem:    
            self.PresentationElementsDict['ParentDatasetId']= self.NewItem['ParentDatasetId']
        if 'InlineTask' in self.NewItem:    
            self.PresentationElementsDict['InlineTask']= self.NewItem['InlineTask']
        if 'ShowHeader' in self.NewItem:    
            self.PresentationElementsDict['ShowHeader']=self.NewItem['ShowHeader']
        if 'Required' in self.NewItem:    
            self.PresentationElementsDict['Required']= self.NewItem['Required']
        if 'ShowField' in self.NewItem:    
            self.PresentationElementsDict['ShowField']= self.NewItem['ShowField']
        if 'Multiline' in self.NewItem:    
            self.PresentationElementsDict['Multiline']= self.NewItem['Multiline']
        if 'UniqueId' in self.NewItem:    
            self.PresentationElementsDict['UniqueId']= self.NewItem['UniqueId']
        if 'ReadOnly' in self.NewItem:    
            self.PresentationElementsDict['ReadOnly']= self.NewItem['ReadOnly']
        if 'UseDefault' in self.NewItem:    
            self.PresentationElementsDict['UseDefault']= self.NewItem['UseDefault']
        if 'UseSystemDate' in self.NewItem:    
            self.PresentationElementsDict['UseSystemDate'] = self.NewItem['UseSystemDate']
        if 'DefaultValue' in self.NewItem:    
            self.PresentationElementsDict['DefaultValue'] = json.dumps(self.NewItem['DefaultValue'])
        if 'NoUpdate' in self.NewItem:    
            self.PresentationElementsDict['NoUpdate'] = self.NewItem['NoUpdate']
        if 'UpdateWithDefault' in self.NewItem:   
            self.PresentationElementsDict['UpdateWithDefault'] = self.NewItem['UpdateWithDefault']
        if 'Search' in self.NewItem:   
            self.PresentationElementsDict['Search'] = json.dumps(self.NewItem['Search'])
        if 'DatasetMapping' in self.NewItem:       
            self.PresentationElementsDict['DatasetMapping'] = self.NewItem['DatasetMapping']
        if 'PresentationType' in self.NewItem:       
            self.PresentationElementsDict['PresentationType'] = self.NewItem['PresentationType']
        if 'NameField' in self.NewItem:       
            self.PresentationElementsDict['NameField'] = json.dumps(self.NewItem['NameField'])
        if 'Parameters' in self.NewItem:       
            self.PresentationElementsDict['Parameters'] = json.dumps(self.NewItem['Parameters'])

        self.PresentationElementsDict['OverrideType']=  self.ParentResource.FormOverrideType
        self.PresentationElementsDict['ResourceType']=  self.ParentResource.FormResourceType
        
        #pdb.set_trace()
        self.PresentationElementsDict['VersionFrom'] = self.ParentResource.NewVersion
        if new_element:
            
            self.PresentationElementsDict['UuidRef'] = str(uuid.uuid4())
        else:
            self.PresentationElementsDict['UuidRef'] = self.PresentationElementObject.UuidRef
            self.PresentationElementsDict['ChangedBy']=self.ParentResource.User.id
            self.PresentationElementsDict['ChangedDateTime'] = datetime.now(timezone.utc)
            if self.HasResourceModel:
                self.PresentationElementsDict['ResourceModelDefinitionId'] = self.ResourceModelObject.id

        
        self.PresentationElementsDict['VersionTo'] = self.ParentResource.NewVersion
        
        if 'DirectiveName' in self.NewItem:       
            self.PresentationElementsDict['DirectiveName'] = self.NewItem['DirectiveName'] #ILJ 09.09.2019
        if 'AngularDirectiveName' in self.NewItem:       
            self.PresentationElementsDict['AngularDirectiveName'] = self.NewItem['AngularDirectiveName'] # ILJ 09.09.2019

        self.PresentationElementsDict['CreatedBy'] = self.ParentResource.User.id
        self.PresentationElementsDict['CreatedDateTime']=datetime.now(timezone.utc)

    def prepare_presentation_elements_lang_insert(self,new_element=False):
        
        assert  type(self.NewItem.get('FormDef',[]) ) in [dict, list], 'FormDef must be dict or list!'
        assert  type(self.NewItem.get('FormDefSearch',[]) ) in [dict, list], 'FormDefSearch must be dict or list!'
        assert  type(self.NewItem.get('GridState',[]) ) in [dict, list], 'GridState must be dict or list!'
        assert  type(self.NewItem.get('ParametersLang',{}) ) in [dict, list], 'ParametersLang must be dict or list!'
 
 
        self.PeLangDict['Name']= self.NewItem.get('Name','')
        self.PeLangDict['Label'] = self.NewItem.get('Label','')
        self.PeLangDict['Subject'] = self.NewItem.get('Subject','')
        self.PeLangDict['Description'] = self.NewItem.get('Description','')   
        self.PeLangDict['ErrorMsg']= self.NewItem.get('ErrorMsg','')  
        self.PeLangDict['SearchText'] = self.NewItem.get('SearchText','')
        self.PeLangDict['ValueLength'] = self.NewItem.get('ValueLength',None)
        self.PeLangDict['LabelLength'] = self.NewItem.get('LabelLength',None)       
        self.PeLangDict['FormDef'] = json.dumps(self.NewItem.get('FormDef',{}))
        self.PeLangDict['FormDefSearch']= json.dumps(self.NewItem.get('FormDefSearch',{}))
        self.PeLangDict['ResponseMsg']= self.NewItem.get('ResponseMsg','')
        self.PeLangDict['GridState']=json.dumps(self.NewItem.get('GridState',{}))
        self.PeLangDict['ParametersLang'] = json.dumps(self.NewItem.get('ParametersLang',{}))
        self.PeLangDict['Lang']= self.ParentResource.Language.id
        self.PeLangDict['CreatedBy'] = self.ParentResource.User.id
        self.PeLangDict['CreatedDateTime']=datetime.now(timezone.utc)

        if not new_element:
            
            self.PeLangDict['ChangedBy'] = self.ParentResource.User.id
            self.PeLangDict['ChangedDateTime']=datetime.now(timezone.utc)
            
    def prepare_resource_model_insert(self):

        self.ResourceModelDict['ResourceDefId'] = self.ParentResource.id
        
        if 'FieldName' in self.NewItem:

            self.ResourceModelDict['FieldName'] = self.NewItem['FieldName']
            
            if self.ResourceModelDict['FieldName'] is not None and self.ResourceModelDict['FieldName'][0:4]=='File':
                self.ResourceModelDict['FieldName'] = 'resource'+str(self.ParentResource.id)+'_'+str(self.ParentResource.OrganisationId)+'_'+self.ResourceModelDict['FieldName']
        else:
            self.ResourceModelDict['FieldName']='Field'



        self.ResourceModelDict['FieldType'] = self.NewItem['ElementType']
        self.NewItem['FieldType'] =  self.ResourceModelDict['FieldType'] 

        if 'CodeId' in self.NewItem:
            self.ResourceModelDict['CodeId'] = self.NewItem['CodeId']
        else:
            self.ResourceModelDict['CodeId'] = 0
        if 'Dbtype' in self.NewItem:
            self.ResourceModelDict['Dbtype']= self.NewItem['Dbtype']

        self.ResourceModelDict['GenericType'] = self.NewItem['ElementType']

        if 'Unique' in self.NewItem:
            self.ResourceModelDict['Unique'] = self.NewItem['Unique']
        if 'Required' in self.NewItem:
            self.ResourceModelDict['RequiredField']=  self.NewItem['Required']
        if 'MaxLength' in self.NewItem:
            self.ResourceModelDict['MaxLength']= self.NewItem['MaxLength']
        if 'Related' in self.NewItem:
            self.ResourceModelDict['Related']= self.NewItem['Related']
        
        if 'Status' in self.NewItem:
            self.ResourceModelDict['Status'] = self.NewItem['Status']

        if self.ParentResource.ResourceSubType!='standard':
            self.ResourceModelDict['DeployAction'] = 'N'
            self.ResourceModelDict['Deployed'] = True            
            self.ResourceModelDict['DeployedVersion'] = 0
        else:
            self.ResourceModelDict['DeployAction'] = 'A'
            self.ResourceModelDict['Deployed'] = False
            self.ResourceModelDict['DeployedVersion'] = self.ParentResource.NewVersion

        self.ResourceModelDict['InitialVersion'] = self.ParentResource.NewVersion
        self.ResourceModelDict['Version'] = self.ParentResource.NewVersion
        self.ResourceModelDict['CreatedBy'] = self.ParentResource.User.id
        self.ResourceModelDict['CreatedDateTime']=datetime.now(timezone.utc)

    def prepare_presentation_elements_update(self):
            
        #pes = PresentationElements.objects.filter(id=self.PresentationElementObject.id).values()
        #if len(pes)>0:
        #    cRowHistory = deepcopy(pes[0])
        #else:
        #    cRowHistory=None
        #    raise ValidationError("PresentationElements history error --> " + str(self.PresentationElementObject.id))
        
        #cRowHistory['PresentationElementsId'] = self.PresentationElementObject.id
        #cRowHistory['id']=None
        
        #peHistoryRow = PresentationElementsHistory.objects.create(**cRowHistory)
        
        #pdb.set_trace()
        #if peHistoryRow:
        self.PresentationElement.OrganisationId = self.ParentResource.OrganisationId
        self.PresentationElement.FormType = self.ParentResource.FormType
        self.PresentationElement.ElementType = self.NewItem['ElementType']
        #if self.ParentWidget is not None:
            #self.PresentationElement.ContainerId_id = self.ParentWidget.id #peRow.id
        if 'AutoSearch' in self.NewItem:
            self.PresentationElement.AutoSearch= self.NewItem['AutoSearch']
        if 'ShowSearchForm' in self.NewItem:
            self.PresentationElement.ShowSearchForm = self.NewItem['ShowSearchForm']

        if self.ParentWidget is not None:
            self.PresentationElement.Order = self.ParentWidget.CurrentIndex
        else:
            self.PresentationElement.Order = self.ParentResource.CurrentIndex
        if 'SizeX' in self.NewItem:   
            self.PresentationElement.SizeX = self.NewItem['SizeX']    
        
        self.PresentationElement.Tabindex  =  self.ParentResource.get_tab_index()
        
        if 'SrcWidgetId' in self.NewItem:    
            self.PresentationElement.SrcWidgetId = self.NewItem['SrcWidgetId']
        if 'PopulateMethod' in self.NewItem:    
            self.PresentationElement.PopulateMethod = self.NewItem['PopulateMethod']
        if 'PopulateType' in self.NewItem:    
            self.PresentationElement.PopulateType = self.NewItem['PopulateType']
        else:
            self.PresentationElement.PopulateType = ''
        if 'PopulateTypeParent' in self.NewItem:   
            self.PresentationElement.PopulateTypeParent = self.NewItem['PopulateTypeParent']
        if 'SrcParentWidgetId' in self.NewItem:    
            self.PresentationElement.SrcParentWidgetId = self.NewItem['SrcParentWidgetId']
        if 'PopulateParentMethod' in self.NewItem:    
            self.PresentationElement.PopulateParentMethod = self.NewItem['PopulateParentMethod']
        if 'Related' in self.NewItem:    
            self.PresentationElement.Related= self.NewItem['Related']
        if 'Status' in self.NewItem:    
            self.PresentationElement.Status= self.NewItem['Status']
        if 'HasParentDataset' in self.NewItem:    
            self.PresentationElement.HasParentDataset=self.NewItem['HasParentDataset']
        if 'ParentDatasetId' in self.NewItem:    
            self.PresentationElement.ParentDatasetId= self.NewItem['ParentDatasetId']
        if 'InlineTask' in self.NewItem:    
            self.PresentationElement.InlineTask= self.NewItem['InlineTask']
        if 'ShowHeader' in self.NewItem:    
            self.PresentationElement.ShowHeader=self.NewItem['ShowHeader']
        if 'Required' in self.NewItem:    
            self.PresentationElement.Required = self.NewItem['Required']
        if 'ShowField' in self.NewItem:    
            self.PresentationElement.ShowField = self.NewItem['ShowField']
        if 'Multiline' in self.NewItem:    
            self.PresentationElement.Multiline = self.NewItem['Multiline']
        if 'UniqueId' in self.NewItem:    
            self.PresentationElement.UniqueId = self.NewItem['UniqueId']
        if 'ReadOnly' in self.NewItem:    
            self.PresentationElement.ReadOnly = self.NewItem['ReadOnly']
        if 'UseDefault' in self.NewItem:    
            self.PresentationElement.UseDefault = self.NewItem['UseDefault']
        if 'UseSystemDate' in self.NewItem:    
            self.PresentationElement.UseSystemDate = self.NewItem['UseSystemDate']
        if 'DefaultValue' in self.NewItem:    
            self.PresentationElement.DefaultValue = self.NewItem['DefaultValue']
        if 'NoUpdate' in self.NewItem:    
            self.PresentationElement.NoUpdate = self.NewItem['NoUpdate']
        if 'UpdateWithDefault' in self.NewItem:   
            self.PresentationElement.UpdateWithDefault = self.NewItem['UpdateWithDefault']
        if 'Search' in self.NewItem:   
            self.PresentationElement.Search = self.NewItem['Search']
        if 'DatasetMapping' in self.NewItem:    
            self.PresentationElement.DatasetMapping = self.NewItem['DatasetMapping']
        if 'PresentationType' in self.NewItem:       
            self.PresentationElement.PresentationType = self.NewItem['PresentationType']
        if 'NameField' in self.NewItem:       
            self.PresentationElement.NameField = self.NewItem['NameField']
        if 'Parameters' in self.NewItem:       
            self.PresentationElement.Parameters = self.NewItem['Parameters'] #ILJU 07.10.2019     
        
        self.PresentationElement.Version = self.ParentResource.NewVersion #ILJU 07.10.2019
        
        if 'DirectiveName' in self.NewItem:       
            self.PresentationElement.DirectiveName = self.NewItem['DirectiveName'] #ILJ 09.09.2019
        if 'AngularDirectiveName' in self.NewItem:       
            self.PresentationElement.AngularDirectiveName = self.NewItem['AngularDirectiveName'] # ILJ 09.09.2019

        self.PresentationElement.ChangedBy = self.ParentResource.User.id
        self.PresentationElement.ChangedDateTime=datetime.now(timezone.utc)

    def prepare_presentation_elements_lang_update(self):
            
        #pes = PresentationElementsLang.objects.filter(id=self.PeLangObject.id).values()
        #if len(pes)>0:
        #    cRowHistory = deepcopy(pes[0])
        #else:
        #    cRowHistory=None
        #    raise 
        #    Exception("PresentationElements history error --> " + str(self.PeLangObject.id))
        
        #cRowHistory['PresentationElementsLangId'] = self.PeLangObject.id
        #cRowHistory['id']=None
        
        #peHistoryRow = PresentationElementsLangHistory.objects.create(**cRowHistory)
        
        ##pdb.set_trace()
        #if peHistoryRow:
           
        if 'Name' in self.NewItem:
            self.PeLang.Name= self.NewItem['Name']
        if 'Label' in self.NewItem:
            self.PeLang.Label = self.NewItem['Label']            
        if 'Subject' in self.NewItem:   
            self.PeLang.Subject = self.NewItem['Subject']    
        if 'Description' in self.NewItem:    
            self.PeLang.Description = self.NewItem['Description']
        if 'ErrorMsg' in self.NewItem:    
            self.PeLang.ErrorMsg = self.NewItem['ErrorMsg']
        if 'SearchText' in self.NewItem:    
            self.PeLang.SearchText = self.NewItem['SearchText']
        if 'ValueLength' in self.NewItem:   
            self.PeLang.ValueLength = self.NewItem['ValueLength']
        if 'LabelLength' in self.NewItem:    
            self.PeLang.LabelLength = self.NewItem['LabelLength']
        if 'FormDef' in self.NewItem:    
            self.PeLang.FormDef = json.dumps(self.NewItem['FormDef'])
        if 'FormDefSearch' in self.NewItem:    
            self.PeLang.FormDefSearch = json.dumps(self.NewItem['FormDefSearch'])
        if 'GridState' in self.NewItem:    
                self.PeLang.GridState= json.dumps(self.NewItem['GridState'])    
        if 'ResponseMsg' in self.NewItem:    
            self.PeLang.ResponseMsg= self.NewItem['ResponseMsg']

        if 'InitialVersion' in self.NewItem:    
            self.PeLang.InitialVersion= self.NewItem['InitialVersion']
        if 'ParametersLang' in self.NewItem:   
            self.PeLang.ParametersLang = json.dumps(self.NewItem['ParametersLang'])
        
        #self.PeLang.Version = self.ParentResource.NewVersion
        self.PeLang.ChangedBy = self.ParentResource.User.id
        self.PeLang.ChangedDateTime=datetime.now(timezone.utc)

    def prepare_resource_model_update(self):
           

        if self.NewItem['Status'] == 'Active':
            if self.ResourceModelObject.DeployAction!='A':
                self.ResourceModelObject.DeployAction='C'
        elif self.NewItem['Status'] == 'Deleted' and self.ResourceModelObject.Deployed==False:
            self.ResourceModelObject.DeployAction='N'
        elif self.ResourceModelObject.Deployed==True:
            self.ResourceModelObject.DeployAction='D'

        session = self.ParentResource.Session
        
        #with session.begin():

        #rmd = ResourceModelDefinition.objects.filter(id=self.ResourceModelObject.id).values()

        rmd = session.query(ResourceModelDefinitionSa).filter(ResourceModelDefinitionSa.id==self.ResourceModelObject.id).all()

        
        if len(rmd)>0:
            self.ResourceModel = rmd[0]
        else:
            #cRowHistory=None
            raise Exception("ResourceModelDefinition history error --> " + str(self.ResourceModelObject.id))
    
        #cRowHistory['ResourceModelDefinitionId'] = self.ResourceModelObject.id
        #cRowHistory['id']=None
        
        #create alchemy model here
        #rmdHistory = ResourceModelDefinitionHistory.objects.create(**cRowHistory)
        rmdHistory = ResourceModelDefinitionHistorySa(
            ResourceModelDefinitionId = self.ResourceModelObject.id,
            ResourceDefId = self.ResourceModel.ResourceDefId,
            FieldName = self.ResourceModel.FieldName,
            FieldType = self.ResourceModel.FieldType,
            Dbtype = self.ResourceModel.Dbtype,
            CodeId = self.ResourceModel.CodeId,
            GenericType = self.ResourceModel.GenericType,
            Unique = self.ResourceModel.Unique,
            RequiredField = self.ResourceModel.RequiredField,
            MaxLength = self.ResourceModel.MaxLength,
            Related = self.ResourceModel.Related,
            Version  =self.ResourceModel.Version,
            InitialVersion  =self.ResourceModel.InitialVersion,
            Status = self.ResourceModel.Status,
            Deployed = self.ResourceModel.Deployed,
            DeployedVersion = self.ResourceModel.DeployedVersion,
            DeployedBy= self.ResourceModel.DeployedBy,
            DeployDate= self.ResourceModel.DeployDate,
            ExternalKey= self.ResourceModel.ExternalKey,
            ResourceLink= self.ResourceModel.ResourceLink,
            DeployAction = self.ResourceModel.DeployAction,
            CreatedBy = self.ResourceModel.CreatedBy,
            CreatedDateTime = self.ResourceModel.CreatedDateTime,
            ChangedBy = self.ResourceModel.ChangedBy,
            ChangedDateTime = self.ResourceModel.ChangedDateTime
            )
    
        session.add(rmdHistory)

        session.flush()

        if rmdHistory.id is not None:
               
            if not self.ParentResource.CustomFields and self.ResourceModel.FieldName=='Field': 
                self.ResourceModel.FieldName = self.ResourceModel.FieldName+str(self.ResourceModel.id)
             
            elif self.ParentResource.CustomFields:
                self.ResourceModel.FieldName = self.NewItem.get('FieldName','')
            self.ResourceModel.FieldType = self.NewItem.get('FieldType','')
        
            self.ResourceModel.Dbtype = self.NewItem.get('Dbtype','')
            self.ResourceModel.CodeId= self.NewItem.get('CodeId',0)
            self.ResourceModel.GenericType = self.NewItem.get('ElementType','')
            self.ResourceModel.Unique = self.NewItem.get('Unique',False)
            self.ResourceModel.RequiredField=  self.NewItem.get('Required',False)
            self.ResourceModel.MaxLength= self.NewItem.get('MaxLength',0)
            self.ResourceModel.Related= self.NewItem.get('Related',None)
            self.ResourceModel.Version = self.ParentResource.NewVersion
            self.ResourceModel.Status = self.NewItem.get('Status','Active')
            
            self.ResourceModel.DeployAction = self.ResourceModelObject.DeployAction

            if not self.ParentResource.CustomFields and self.ResourceModel.FieldName=='Field':
                self.ResourceModel.FieldName = self.ResourceModel.FieldName+str(self.ResourceModel.id)

            self.ResourceModel.Version = self.ParentResource.NewVersion
            self.ResourceModel.ChangedBy = self.ParentResource.User.id
            self.ResourceModel.ChangedDateTime=datetime.now(timezone.utc)

    def save_definition(self):
        saved = False
        try:
            if 'Saved' in self.NewItem and self.id is not None:
                saved = self.NewItem['Saved']      
            if saved:
                self.HasChangesPe,self.HasChangesPeLang,self.HasChangesRmd = self.check_changes()
                if self.HasChangesPe or self.HasChangesPeLang or self.HasChangesRmd:
                    self.ParentResource.add_change()
                    self.save_new()
            else:
                
                self.ParentResource.add_change()
                if 'Status' in self.NewItem:
                    if self.NewItem['Status']=='Active':
                        self.save_new(True)
            
        except Exception as e:
            #pdb.set_trace()
            raise e
            
            
    def save_changes(self):

        self.prepare_update()
        self.execute_update()
        self.publish_element()
        
        
        
    def save_new(self,new_element=False):

        self.prepare_presentation_elements_insert(new_element)
        self.prepare_presentation_elements_lang_insert(new_element)     
        
        if self.HasResourceModel:
            self.prepare_resource_model_insert() if new_element else self.prepare_resource_model_update()
                
        self.execute_insert(new_element)
        self.publish_element()

    def get_element_definition(self):

        session = self.ParentResource.Session
        #with session.begin(): 
        #queryset = PresentationElements.objects.filter(id=self.PresentationElementObject.id).prefetch_related(Prefetch('presentationelementslang_set',
        #           queryset=PresentationElementsLang.objects.filter(Lang=self.ParentResource.Language))).select_related('ResourceModelDefinitionId').order_by('Order')#TODO - remove when SQL alchemy works!!

        queryset_SA = session.query(PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa)\
        .outerjoin(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId, PresentationElementsLangSa.Lang==self.ParentResource.Language.id))\
        .outerjoin(ResourceModelDefinitionSa, PresentationElementsSa.ResourceModelDefinitionId==ResourceModelDefinitionSa.id)\
        .filter(PresentationElementsSa.id==self.id).all()

        if len(queryset_SA)>0:
            self.populate_db_items (queryset_SA[0])

    def publish_element(self):

        if not os.path.exists(PUBLISH_DIR):
            os.makedirs(PUBLISH_DIR,exist_ok=True)
        
        pub_file=os.path.join(PUBLISH_DIR,self.UuidRef+'.json') 

        #pdb.set_trace()
        with open(pub_file, "w+") as outfile:
            json_object = {'FormDef':self.NewItem.get('FormDef',[]),'FormDefSearch':self.NewItem.get('FormDefSearch',[]),'GridState':self.NewItem.get('GridState',[])}
            #pdb.set_trace()
            json.dump(json_object,outfile)

    def create_widget_css(self,uuidRefParent=None):
        
        return self.generate_css_classes(uuidRefParent)
        
    def generate_css_classes(self,uuidRefParent='widget'):
        
        
        #padding
        element_style=self.generate_css_styles(['all'])
        label_style=self.generate_css_styles(['all'])    

        label_css=''
        if label_style!='':
            label_css='.label-style-'+uuidRefParent+'-'+self.NewItem.get('ElementType','default')+'-'+self.NewItem.get('UuidRef','no-uuidref')+'{'+label_style+'}'

        element_css=''
        if element_style!='':
            element_css='.element-style-'+uuidRefParent+'-'+self.NewItem.get('ElementType','default')+'-'+self.NewItem.get('UuidRef','no-uuidref')+'{'+element_style+'}'

        return label_css+element_css

    def generate_css_styles(self,styles=[]):
        
        
      
        element_style=''
        params = self.NewItem.get('Parameters',None)

        if params is not None:
            
            #padding
            if 'padding' in styles or 'all' in styles:

                css_style = params.get('padding',{}).get('top',None)

                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='padding-top:'+str(css_style.get('value'))+str(css_style.get('units'))+';'

                css_style = params.get('padding',{}).get('bottom',None)
                if css_style is not None:
                    if css_style.get('value')>0:        
                        element_style+='padding-bottom:'+str(css_style.get('value'))+str(css_style.get('units'))+';'

                css_style = params.get('padding',{}).get('left',None)
                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='padding-left:'+str(css_style.get('value'))+str(css_style.get('units'))+';'

                css_style = params.get('padding',{}).get('right',None)
                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='padding-right:'+str(css_style.get('value'))+css_style.get('units')+';'
                
            #margin
            if 'margin' in styles or 'all' in styles:
                css_style = params.get('margin',{}).get('top',None)
                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='margin-top:'+str(css_style.get('value'))+str(css_style.get('units'))+';'

                css_style = params.get('margin',{}).get('bottom',None)
                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='margin-bottom:'+str(css_style.get('value'))+str(css_style.get('units'))+';'

                css_style = params.get('margin',{}).get('left',None)
                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='margin-left:'+str(css_style.get('value'))+css_style.get('units')+';'

                css_style = params.get('margin',{}).get('right',None)
                if css_style is not None:
                    if css_style.get('value')>0:
                        element_style+='margin-right:'+str(css_style.get('value'))+str(css_style.get('units'))+';'

            #border
            if 'margin' in styles or 'all' in styles:
                css_style = params.get('border',{}).get('top',None)
                if css_style is not None:
                    if css_style.get('width')>0:
                        element_style+='border-top:'+str(css_style.get('width'))+'px '+css_style.get('style')+' '+css_style.get('color')+';'

                css_style = params.get('border',{}).get('bottom',None)
                if css_style is not None:
                    if css_style.get('width')>0:
                        element_style+='border-bottom:'+str(css_style.get('width'))+'px '+css_style.get('style')+' '+css_style.get('color')+';'

                css_style = params.get('border',{}).get('left',None)
                if css_style is not None:
                    if css_style.get('width')>0:
                        element_style+='border-left:'+str(css_style.get('width'))+'px '+css_style.get('style')+' '+css_style.get('color')+';'

                css_style = params.get('border',{}).get('right',None)
                if css_style is not None:
                    if css_style.get('width')>0:
                        element_style+='border-right:'+str(css_style.get('width'))+'px '+css_style.get('style')+' '+css_style.get('color')+';'

            if 'font' in styles or 'all' in styles:
            #font size
                css_style = params.get('ValueFontSize',None)
                if css_style is not None:
                    element_style+='font-size:'+str(css_style)+params.get('ValueFontUnit','px')+';'

                #rounded borders
                css_style = params.get('ValueFontColor',None)
                if css_style is not None:
                    element_style+='color:'+css_style+';'

                #font bold
                css_style = params.get('ValueFontBold',None)
                if css_style is not None:
                    element_style+='font-weight:700;'

            if 'rounded' in styles or 'all' in styles:        
            #rounded borders
                css_style = params.get('RoundedBorders',False)
                if css_style:
                    element_style+='border-radius:0.25rem;'

                

            #background color
            if 'bgcolor' in styles or 'all' in styles:
                css_style = params.get('BackgroundColor',None)
                if css_style is not None:
                    element_style+='background-color:'+css_style+';'
            
            if 'shadow' in styles or 'all' in styles:    
                css_style = params.get('ShadowHOffset',0)+params.get('ShadowVOffset',0)+params.get('ShadowBlur',0)+params.get('ShadowSpread',0)
                if css_style>0:
                    element_style+='box-shadow:'+str(params.get('ShadowHOffset',0))+params.get('ShadowUnits','px')+' '
                    element_style+=str(params.get('ShadowVOffset',0))+params.get('ShadowUnits','px')+' '
                    element_style+=str(params.get('ShadowBlur',0))+params.get('ShadowUnits','px')+' '
                    element_style+=str(params.get('ShadowSpread',0))+params.get('ShadowUnits','px')+' '
                    element_style+=str(params.get('ShadowColor','transparent'))+';'


            return element_style

    def generate_form(self):
        pass

    def process_element_data(self):
        pass

    def get_element_value(self,pe,item,params):
        
        field_name = pe['FieldName']
        value = getattr(item,field_name,None)

        return value
    
    

    def populate_definition_json(self):
        #pdb.set_trace()
        self.WidgetDict = {

            'id':self.PresentationElementObject.id,
            'PresentationId':self.PresentationElementObject.PresentationId,
            'UuidRef':self.PresentationElementObject.UuidRef,
            'ElementType' : self.PresentationElementObject.ElementType,
            'Name' : self.PeLangObject.Name ,
            'Placeholder' : self.PeLangObject.Placeholder ,
            'Label' : self.PeLangObject.Label ,
           # 'Facss' : self.PresentationElementObject.Facss,
            'Status' : self.PresentationElementObject.Status,
            'ShowField' : self.PresentationElementObject.ShowField,
            'Required':self.PresentationElementObject.Required,
            'ReadOnly': self.PresentationElementObject.ReadOnly,
            'DirectiveName' : self.PresentationElementObject.DirectiveName, #change this to component with vue
            'AngularDirectiveName': self.PresentationElementObject.AngularDirectiveName, #change this to component with vue
            'ComponentName':'Widget'+self.PresentationElementObject.ElementType.capitalize(),
            'VersionFrom': self.PresentationElementObject.VersionFrom,
            'VersionTo': self.PresentationElementObject.VersionTo,
            'Parameters': json.loads(self.PresentationElementObject.Parameters or '{}'),
            'Related': self.PresentationElementObject.Related,
            'Saved':True,
            'ValueLength': self.PeLangObject.ValueLength,
            'LabelLength': self.PeLangObject.LabelLength,
            'ErrorMsg': self.PeLangObject.ErrorMsg,
            'SearchText': self.PeLangObject.SearchText,
            'Multiline': self.PresentationElementObject.Multiline,
            'NoUpdate': self.PresentationElementObject.NoUpdate,
            'UpdateWithDefault': self.PresentationElementObject.UpdateWithDefault,
            'DefaultValue': json.loads(self.PresentationElementObject.DefaultValue or '{}'),
            'UseDefault': self.PresentationElementObject.UseDefault,


        }
        if self.HasResourceModel:
        
            self.WidgetDict.update({
                'MaxLength': self.ResourceModelObject.MaxLength,
                'ItemId':self.ResourceModelObject.id,
                'FieldType':self.ResourceModelObject.FieldType,
                'FieldName':self.ResourceModelObject.FieldName,
                'GenericType': self.ResourceModelObject.GenericType,
                'Dbtype': self.ResourceModelObject.Dbtype,
                'RelatedRmd': self.ResourceModelObject.Related,
                'Unique': self.ResourceModelObject.Unique
            })

    def get_definition_json(self,re_populate=True):

        if re_populate: self.populate_definition_json()
        return self.WidgetDict

    def process_widget(self,form_data,rmd):
          
        field_name = rmd['FieldName']

        value = form_data.get(field_name,None)
        
        if (rmd['NoUpdate'] is None or not rmd['NoUpdate']):
            if rmd['UpdateWithDefault']:
                value_default = rmd['DefaultValue']
                if 'id' in value_default:        
                    value = json.loads(value_default)
                    value = value['id']
                elif 'value' in value_default:
                    value = json.loads(value_default)
                    value = value['value']
                else:
                    value = json.loads(value_default)
            
        widget_value = {}

        if value is not None:
            widget_value = {'type':rmd['GenericType'],'widget_name':rmd['DirectiveName'], 'value':value}

        return widget_value

    def generate_query(self,field):
        
        value = field.get('value',None)
          
        if 'UpdateWithDefault' in field:
          
          if field['UpdateWithDefault']:
            
            value=field['DefaultValue']
            field['value'] = value

        return field
    
    def check_changes(self,_has_changes=False):
        
        #check presentation elements
        has_changes_pe = self.check_changes_presentation_elements()
        #check for changes presentation elements lang
        has_changes_pe_lang = self.check_changes_presentation_elements_lang()
        #check for changes
        has_changes_rmd = False
        if self.HasResourceModel:
            has_changes_rmd = self.check_changes_resource_model()
        #pdb.set_trace()
        return has_changes_pe,has_changes_pe_lang,has_changes_rmd

    def check_changes_presentation_elements(self):

        has_changes = False
        #pdb.set_trace()
        #check position
        current_index = getattr(self.ParentWidget,'CurrentIndex',None)
        if current_index is None:
            current_index = getattr(self.ParentResource,'CurrentIndex',None)
        if current_index!=self.PresentationElementObject.Order:# and current_index is not None:
                #pdb.set_trace()
                has_changes = True

        #but also check if parent widget has changed
        current_parent = getattr(self.ParentWidget,'UuidRef',None)
        if current_parent is None:
            current_parent = getattr(self.ParentResource,'UuidRef',None)
        if current_parent!=self.PresentationElementObject.ContainerElement:# and current_index is not None:
                #pdb.set_trace()
                has_changes = True

        if not has_changes:
            for element in vars(self.PresentationElementObject).items():
                
                if element[0] in self.NewItem and element[0] not in self.NoCheckFieldsPe:
                    if type(self.NewItem[element[0]]) in [dict,list]:
                        if element[1] is not None:
                            if type(element[1]) in [dict,list]:

                                old_item = hashlib.sha1(json.dumps(element[1]).encode('utf-8')).hexdigest()
                            else:
                                old_item = hashlib.sha1(element[1].encode('utf-8')).hexdigest()
                        else:
                            old_item = element[1]

                        new_item = hashlib.sha1(json.dumps(self.NewItem[element[0]]).encode('utf-8')).hexdigest()
                    else:
                        old_item = element[1]
                        new_item = self.NewItem[element[0]]
                
                    if old_item!=new_item:
                        #pdb.set_trace()
                        has_changes = True
                        break
        return has_changes


    def check_changes_presentation_elements_lang(self):
        
        has_changes = False
        for element in vars(self.PeLangObject).items():
            if element[0] in self.NewItem and element[0] not in self.NoCheckFieldsPeLang:
                    
                if type(self.NewItem[element[0]]) in [dict,list]:
                    if element[1] is not None:
                        if type(element[1]) in [dict,list]:

                            old_item = hashlib.sha1(json.dumps(element[1]).encode('utf-8')).hexdigest()
                        else:
                            old_item = hashlib.sha1(element[1].encode('utf-8')).hexdigest()
                    else:
                        old_item = element[1]
                    new_item = hashlib.sha1(json.dumps(self.NewItem[element[0]]).encode('utf-8')).hexdigest()
                else:
                    old_item = element[1]
                    new_item = self.NewItem[element[0]]
                
                if old_item!=new_item:
                    #pdb.set_trace()
                    has_changes = True
                    break
        return has_changes


    def check_changes_resource_model(self):

        has_changes = False
        if self.ResourceModelObject.ResourceModelStatus!=self.NewItem.get('Status',None) and self.ResourceModelObject.id is not None:
            #pdb.set_trace()
            has_changes = True 
            return has_changes
        for element in vars(self.ResourceModelObject).items():
            if element[0] in self.NewItem and element[0] not in self.NoCheckFieldsRmd:
                if type(self.NewItem[element[0]]) in [dict,list]:
                    if element[1] is not None:
                        if type(element[1]) in [dict,list]:

                            old_item = hashlib.sha1(json.dumps(element[1]).encode('utf-8')).hexdigest()
                        else:
                            old_item = hashlib.sha1(element[1].encode('utf-8')).hexdigest()
                    else:
                        old_item = element[1]

                    new_item = hashlib.sha1(json.dumps(self.NewItem[element[0]]).encode('utf-8')).hexdigest()
                else:
                    old_item = element[1]
                    new_item = self.NewItem[element[0]]
                
                if old_item!=new_item:
                    #pdb.set_trace()
                    has_changes = True
                    break
        return has_changes    


    def check_equal(self,field_name_old,field_name_new,old_item,new_item):
        has_changes = False
        if field_name_old in old_item and field_name_new in new_item:
            if old_item[field_name_old]!=new_item[field_name_new]:
                has_changes=True
        return has_changes

    def increment_index(self):
        self.CurrentIndex+=1
        self.ParentResource.TabIndex+=1

    def process_widget_search(self,item,filter_data):

        select_related=[]
        fieldId = None
        s_item_value=None
        r_val = {}
        if 'id__in' in filter_data:
          r_val['id__in'] = filter_data['id__in'] 
        
        no_update=item.get('NoUpdate',False)
        item_id=item.get('ItemId', None)  
        item_name = item.get('FieldName', None)
        fieldId=item_name
                          
        if item_id is not None:
            if type(filter_data) is dict:
                if fieldId in filter_data:
                    s_item_value = filter_data[fieldId]
            elif type(filter_data) is list:
                s_item_value = []
                for el in filter_data:
                    if fieldId in el:
                        s_item_value = el[fieldId]
                if type(s_item_value)==list and len(s_item_value)==0:
                    s_item_value=None
            else:
                s_item_value = filter_data

        #pdb.set_trace()
        if not no_update and item_id is not None: 
            value = s_item_value             
            if item.get('UpdateWithDefault', False):
                value_default = item.get('DefaultValue',None)
                if value_default is not None:
                
                    if type(value_default)==str:  #expecting dict - convert to dict if received as string              
                        value_default = json.loads(value_default)       

                    if type(value_default)==dict:
                        tmp_value = value_default.get('value',value_default.get('Value', None))
                        if type(tmp_value)==dict and tmp_value is not None:
                            value = tmp_value.get('id',None)
                            if value is None:
                                value= tmp_value.get('value',tmp_value.get('Value', None))
                        else:
                            value = tmp_value
            elif item.get('Parameters', None) is not None and value is None:
                params = item.get('Parameters',{})
                if type(params)==str:
                    params = json.loads(params)
                
                if params.get('UseUserProperty',False):
                    #get advanced user properties
                    session=self.ParentResource.Session
                    #with session.begin():
                    uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.UserId==self.ParentResource.User.id).first()
                       
                    if uep:
                        if uep.AdvancedParams is not None:
                            uep_adv_params = json.loads(uep.AdvancedParams)
                            user_props = uep_adv_params.get('AdvancedProperties',None)
                            user_property_id =  params.get('UserPropertyId',None)
                            if user_property_id is not None and user_props is not None:
                                value = user_props.get(user_property_id,None)

            if type(value) is list:
                if len(value)>0:
                    r_val =  {'type':item['ElementType'], 'fieldId':fieldId,'value': value }
            elif value!='':
                r_val =  {'type':item['ElementType'],'fieldId':fieldId, 'value': value }
   
            search = item.get('Search',None)
            if search is not None: 
                item['Search'].update({'type':item['ElementType'],'value':r_val.get('value',None)})
                if r_val:    
                    r_val.update({'search':item['Search']})
        
        params = item.get('Parameters',{})     
        return r_val,filter_data,select_related,params

    def prepare_data_search(self,field):

        mainResource={}
        m2mData=[]
        value = field[1].get('value',None)

        if (value is not None): 
            mainResource[field[0]]=value    
        return mainResource,m2mData