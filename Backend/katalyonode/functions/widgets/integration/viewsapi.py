from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from django.core import serializers
from katalyonode.settings import FILES_DIR,USE_BLOCKCHAIN
from django.template import RequestContext
from django.db.models import Prefetch
from django.contrib.auth.models import User, Group
from rest_framework import viewsets,permissions,authentication,pagination
from django.contrib.auth import update_session_auth_hash
from django.utils.dateparse import parse_datetime
from datetime import datetime,timedelta
from django.core.validators import ValidationError
from django.utils import timezone
from rest_framework.authtoken.models import Token
from katalyonode.functions.finance.paymentplanheta import process_single_contract
from django.core.exceptions import ObjectDoesNotExist
import pdb
  


class ktHetaCalcPaymentPlanViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    
    def create(self, request):
    
        user = request.user
        params = request.data


        if 'contract_number' in params:
            contract_number = params['contract_number']
            msg = 'Ugovor '+contract_number+' uspješno obrađen!'
            ret_status=200
        else:
            ret_status=400
            msg = 'Greška - ugovor nije unešen!'


        if 'penalty_interest_date' in params and ret_status==200:
            penalty_interest_date = params['penalty_interest_date']
            if penalty_interest_date is not None:
                try:
                    datetime.strptime(penalty_interest_date, '%Y-%m-%dT%H:%M:%S.%fZ')
                except ValueError:
                    ret_status=400
                    msg = 'Greška - neispravan format datuma za obračun zatezne kamate!'
        else:
            ret_status=400
            msg = 'Greška - obavezan unos datuma za obračun zatezne kamate!'

        limit_date=None
        if 'limit_date' in params and ret_status==200:
            limit_date = params['limit_date']
            if limit_date is not None:
                try:
                    datetime.strptime(limit_date, '%Y-%m-%dT%H:%M:%S.%fZ')
                except ValueError:
                    ret_status=400
                    msg = 'Greška - neispravan format datuma zastare!'
        elif ret_status==200:
            if 'reset_princ' in params:
                reset_princ = params['reset_princ']
                if reset_princ:
                    ret_status=400
                    msg = 'Greška - obavezan unos datuma zastare!'

        princ_copy_date = limit_date

        if ret_status==200:
            msg,ret_status = process_single_contract(contract_number,limit_date,penalty_interest_date,princ_copy_date,user)

        return Response ({'msg':msg},status=ret_status)
