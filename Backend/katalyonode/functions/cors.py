'''
To enable CORS in DRF
'''
import pdb
from django.http import HttpResponse
from django.conf import settings

class CorsMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        if "HTTP_REFERER" in request.META and "HTTP_ORIGIN" in request.META:
            #pdb.set_trace()
            for referer in settings.ALLOWED_REFERERS:
                if referer in request.META["HTTP_REFERER"] and request.META["HTTP_ORIGIN"] in request.META["HTTP_REFERER"]:
                    response["Access-Control-Allow-Origin"] = request.META["HTTP_ORIGIN"]
                    response["Access-Control-Allow-Headers"]= "*"
                    response["Access-Control-Allow-Methods"]=" GET, POST, PATCH, PUT, DELETE, HEAD, OPTIONS"
       
        if request.method == 'OPTIONS': #currently only local, for server this is processed by NGINX
            #pdb.set_trace()
            response["Access-Control-Max-Age"]= "86400"
            response.status_code=200
        return response
     