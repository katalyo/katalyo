'''
Functions to calculate the payment plans, annuities, penlty interest, etc...
'''

from sqlalchemy import create_engine,inspect,event,literal_column
from sqlalchemy import Table, Column, Integer,Boolean, String, Text, ForeignKey,Numeric,Float,Date,Time,DateTime, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker,relationship, joinedload, subqueryload, join,outerjoin,defer,undefer
from datetime import date,time,datetime
import pdb
db_engine = "postgresql://postgres:Ivica123@localhost:5433/katalyodb"
Base = declarative_base()
engine = create_engine(db_engine,echo=False,connect_args={'options': '-c lock_timeout=5000'})

metadata=MetaData(bind=engine)
#cut_off_date = date(2021, 7, 1)
cut_off_date = '31/12/21'
princ_copy_date = '31/12/16'
limit_date = '31/12/16'
penalty_interest_rates=[
{'date_from':date(1994,6,1),'date_to':date(1994,6,30),'interest_rate':30,'days':30},
{'date_from':date(1994,7,1),'date_to':date(1996,5,7),'interest_rate':22,'days':677},
{'date_from':date(1996,5,8),'date_to':date(1996,9,10),'interest_rate':24,'days':126},
{'date_from':date(1996,9,11),'date_to':date(2002,6,30),'interest_rate':18,'days':2119},
{'date_from':date(2002,7,1),'date_to':date(2007,12,31),'interest_rate':15,'days':2010},
{'date_from':date(2008,1,1),'date_to':date(2011,6,30),'interest_rate':17,'days':1277},
{'date_from':date(2011,7,1),'date_to':date(2015,7,31),'interest_rate':15,'days':1492},
{'date_from':date(2015,8,1),'date_to':date(2015,12,31),'interest_rate':10.14,'days':153},
{'date_from':date(2016,1,1),'date_to':date(2016,6,30),'interest_rate':10.05,'days':181},
{'date_from':date(2016,7,1),'date_to':date(2016,12,31),'interest_rate':9.88,'days':184},
{'date_from':date(2017,1,1),'date_to':date(2017,6,30),'interest_rate':9.68,'days':181},
{'date_from':date(2017,7,1),'date_to':date(2017,12,31),'interest_rate':9.41,'days':184},
{'date_from':date(2018,1,1),'date_to':date(2018,6,30),'interest_rate':9.09,'days':181},
{'date_from':date(2018,7,1),'date_to':date(2018,12,31),'interest_rate':8.82,'days':184},
{'date_from':date(2019,1,1),'date_to':date(2019,6,30),'interest_rate':8.54,'days':181},
{'date_from':date(2019,7,1),'date_to':date(2019,12,31),'interest_rate':8.3,'days':184},
{'date_from':date(2020,1,1),'date_to':date(2020,6,30),'interest_rate':8.11,'days':182},
{'date_from':date(2020,7,1),'date_to':date(2020,12,31),'interest_rate':7.89,'days':184},
{'date_from':date(2021,1,1),'date_to':date(2021,6,30),'interest_rate':7.75,'days':181},
{'date_from':date(2021,7,1),'date_to':date(2021,12,31),'interest_rate':7.61,'days':184},
{'date_from':date(2022,1,1),'date_to':date(2022,1,31),'interest_rate':7.61,'days':31},
#{'date_from':date(2021,7,1),'date_to':date(2021,10,31),'interest_rate':7.61,'days':123}
]
class PaymentPlan(object):
    def process_single_contract(self,contract_number,pc_date,co_date,lm_date):
       
        #select contract record from database
        table_name_contracts="hc"
        table_name_plans="hp"
        
        tbl = Table(table_name_contracts, Base.metadata, autoload=True,autoload_with=engine)
        tbl2 = Table(table_name_plans, Base.metadata, autoload=True,autoload_with=engine)
      
        conn = engine.connect()
        #rec = tbl.select().where(tbl.c[table_name+"_id"].in_(list_id)).order_by(tbl.c[table_name+"_id"])
        rec = tbl.select().where(tbl.c["contract_number"]==contract_number)
        contracts = conn.execute(rec).fetchall()
        #pdb.set_trace()
        count_contracts=0
        dt_start = datetime.now()
        for contract in contracts:
            count_contracts+=1
            
            contract_number = contract.contract_number
            payment_plan_select = tbl2.select().where(tbl2.c["contract_number"]==contract_number,tbl2.c["exclude"]==0).order_by(tbl2.c["period"],tbl2.c["tip_dugovanja"],tbl2.c["plan_detail_id"])
            payment_plan = conn.execute(payment_plan_select).fetchall()
            remaining_principal = float(contract.financed_amount)
            remaining_periods = float(contract.number_of_installments)
            total_tax_penalty = 0
            total_interest_penalty = 0
            total_tax_penalty2016 = 0
            total_interest_penalty2016 = 0
            total_new_debt = 0
            total_old_debt = 0
            total_new_debt2016 = 0
            total_old_debt2016 = 0
            total_principal_adjustment = 0
            prev_remaining_principal = remaining_principal
            prev_principal = 0
            total_interest_diff=0
            total_tax_diff=0
            total_interest_diff2016=0
            total_tax_diff2016=0
            found_first = False
            prev_tip_dugovanja='Installment'
            total_paid = float(contract.paid_installments)+float(contract.paid_installments_m)
            if (contract.discount):
                total_paid+=float(contract.discount_amount)
            
            for installment in payment_plan:
                
                principal_adjustment = 0                         
                residual_amount = 0
                
                #pdb.set_trace()
                period_interest_rate = self.calculate_interest_rate(float(installment.interest_rate),360,30)
                if contract.interest_type=="F":
                     period_interest_rate = self.calculate_interest_rate(float(contract.fixed_interest_rate),360,30)
                
                if installment.tip_dugovanja == 'Installment':
                    if  ((prev_remaining_principal - float(installment.glavnica_numeric) - float(installment.stanje_glavnice_numeric))>0.1 and installment.stanje_glavnice_numeric>0):
                        principal_adjustment =  prev_remaining_principal -  float(installment.stanje_glavnice_numeric) - float(installment.glavnica_numeric)
                        total_principal_adjustment+=principal_adjustment
                else:
                    if installment.neto_numeric==0:
                        principal_adjustment =  prev_remaining_principal - float(installment.glavnica_numeric)
                        total_principal_adjustment+=principal_adjustment
                
                if (installment.tip_dugovanja == 'Installment' and contract.reset_princ and installment.datum_date>datetime.strptime(pc_date, '%d/%m/%y').date() and not found_first):
                    found_first = True
                    remaining_principal = prev_remaining_principal
                    principal_adjustment=0
                
                if contract.restart_count>0:
                    remaining_periods = contract.restart_count
                
                if installment.restart_installment_number>0:
                    remaining_periods = installment.restart_installment_number
                
                    
                remaining_principal = remaining_principal - principal_adjustment
                
                if installment.reprogram_principal>0:
                    remaining_principal = float(installment.reprogram_principal)
                
                remaining_principal_for_calc = remaining_principal
                if contract.residual_amount>0:
                    residual_amount = float(contract.residual_amount)
                    remaining_principal_for_calc = remaining_principal - residual_amount
                     
                if installment.tip_dugovanja.strip()=='Installment':
                    new_annuity_net = self.calculate_annuity(remaining_principal_for_calc,period_interest_rate,remaining_periods,residual_amount,installment.contract_number)
                    new_interest = remaining_principal*period_interest_rate
                    if installment.kamata_numeric==0:    
                        new_interest = float(installment.kamata_numeric)
                    new_principal = new_annuity_net - new_interest
                    new_vat_amount = new_interest*float(installment.vat_rate)
                    if contract.contract_type=="OL":
                        new_vat_amount = new_annuity_net*float(installment.vat_rate)    
                    new_annuity_gross = new_annuity_net+new_vat_amount                
                
                elif installment.tip_dugovanja.strip()=='Last Installment':
                    
                    new_annuity_net = remaining_principal
                    new_interest = 0
                    new_principal = remaining_principal
                    new_vat_amount = 0
                    new_annuity_gross = remaining_principal
                        
                elif installment.tip_dugovanja.strip()=='Termination':
                    
                    if (prev_remaining_principal-principal_adjustment == float(installment.glavnica_numeric)):       
                        
                        new_annuity_net = remaining_principal
                        new_interest = 0
                        new_principal = remaining_principal
                        new_vat_amount = (remaining_principal)*float(installment.vat_rate)
                        new_annuity_gross = new_annuity_net+new_vat_amount        
                    else:
                        fee_rate = float(installment.neto_numeric)/prev_remaining_principal
                        new_annuity_net = remaining_principal*fee_rate
                        new_interest = 0
                        new_principal = new_annuity_net
                        new_vat_amount = new_principal*float(installment.vat_rate)
                        new_annuity_gross = new_annuity_net+new_vat_amount
                    
                if installment.copy_data:
                    new_annuity_net = float(installment.neto_numeric)
                    new_interest = float(installment.kamata_numeric)
                    new_principal = float(installment.glavnica_numeric)
                    new_vat_amount = float(installment.porez_numeric)
                    new_annuity_gross = float(installment.bruto_numeric)
                    remaining_principal = prev_remaining_principal
                    principal_adjustment=0
                   
                    
                if installment.neto_numeric==0:
                    new_annuity_net = float(installment.neto_numeric)
                if installment.kamata_numeric==0:    
                    new_interest = float(installment.kamata_numeric)
                if installment.glavnica_numeric==0:       
                    new_principal = float(installment.glavnica_numeric)
                if installment.porez_numeric==0:   
                    new_vat_amount = float(installment.porez_numeric)
                if installment.bruto_numeric==0:   
                    new_annuity_gross = float(installment.bruto_numeric)
               
                
                
               
                number_of_days_for_penalty = datetime.strptime(co_date, '%d/%m/%y').date()-installment.datum_date
                #pdb.set_trace()
                tax_difference = new_vat_amount-float(installment.porez_numeric) 
                interest_difference = new_interest-float(installment.kamata_numeric)
                
                if (installment.datum_date>datetime.strptime(co_date, '%d/%m/%y').date()):
                    interest_difference = 0
                    tax_difference = 0
                
                if tax_difference!=0 and total_paid>=round(total_old_debt+float(installment.bruto_numeric),2):
                    penalty_interest_tax = self.calculate_penalty_interest_complex(installment.datum_date,tax_difference)
                elif tax_difference!=0 and total_paid-total_old_debt>0:
                    covered_part = float(installment.bruto_numeric)- (total_old_debt-total_paid)
                    paid_ratio  = covered_part/float(installment.bruto_numeric)
                    #interest_ratio = installment.kamata_numeric/installment.gross_numeric
                    old_tax_adj = float(installment.porez_numeric)*paid_ratio
                    tax_difference = new_vat_amount-old_tax_adj
                    penalty_interest_tax = self.calculate_penalty_interest_complex(installment.datum_date,tax_difference)
                else:
                    tax_difference = 0
                    penalty_interest_tax = 0
                   
                if interest_difference!=0 and total_paid>=round(total_old_debt+float(installment.bruto_numeric),2):
                    #print('1 ---> '+str(installment.period)+' ---> '+str(total_paid)+' ---> '+str(total_old_debt+float(installment.bruto_numeric)))
                    penalty_interest = self.calculate_penalty_interest_complex(installment.datum_date,interest_difference)
                elif interest_difference!=0 and total_paid-total_old_debt>0:
                    covered_part = float(installment.bruto_numeric)- (total_old_debt-total_paid)
                    paid_ratio  = covered_part/float(installment.bruto_numeric)
                    #interest_ratio = installment.kamata_numeric/installment.gross_numeric
                    old_interest_adj = float(installment.kamata_numeric)*paid_ratio
                    interest_difference = new_interest-old_interest_adj
                    penalty_interest = self.calculate_penalty_interest_complex(installment.datum_date,interest_difference)
                    #print('Total paid '+str(total_paid))
                    #print('Total old debt '+str(total_old_debt+float(installment.bruto_numeric)))
                    print('Old interest adj'+str(old_interest_adj))
                    print('Old interest '+str(float(installment.kamata_numeric)))
                    #print('2 ---> '+str(installment.period)+' ---> '+str(total_paid)+' ---> '+str(total_old_debt+float(installment.bruto_numeric)))
                else:
                    interest_difference = 0
                    penalty_interest = 0
                   
                total_interest_diff+=interest_difference
                total_tax_diff+=tax_difference
                
                total_tax_penalty+= penalty_interest_tax
                total_interest_penalty+= penalty_interest
                
                if installment.datum_date>datetime.strptime(lm_date, '%d/%m/%y').date() and installment.tip_dugovanja.strip() in ['Installment','Termination']:
                    total_tax_penalty2016+= penalty_interest_tax
                    total_interest_penalty2016+=penalty_interest
                    total_interest_diff2016+=interest_difference
                    total_tax_diff2016+=tax_difference
                    
                if float(installment.stanje_glavnice_numeric)>0:
                    prev_remaining_principal = float(installment.stanje_glavnice_numeric)
                
                prev_principal = float(installment.glavnica_numeric)
                prev_tip_dugovanja = installment.tip_dugovanja
                #print(prev_remaining_principal)
                
                
                #update_records
                update_data={'remaining_principal':remaining_principal,'interest_difference':interest_difference,'tax_difference':tax_difference,
                             'penalty_interest':penalty_interest,'penalty_interest_tax':penalty_interest_tax,
                             'interest_rate_final':period_interest_rate,'number_of_installments':remaining_periods,
                             'new_annuity_gross':new_annuity_gross,'new_vat_amount':new_vat_amount,'new_annuity_net':new_annuity_net,'new_principal':new_principal,'new_interest':new_interest}
                upd = tbl2.update().where(tbl2.c["id"]==installment.id).values(**update_data).returning(literal_column('*'))
   
                result = conn.execute(upd)
                if remaining_principal - new_principal!=0:
                    remaining_principal-= new_principal
                    
                                                                                          
                    #pdb.set_trace()                                                                                
                                                                                                
                if installment.tip_dugovanja=='Installment':
                    total_new_debt+= new_annuity_gross
                    total_old_debt+= float(installment.bruto_numeric)
                    if (installment.datum_date>datetime.strptime(lm_date, '%d/%m/%y').date()):
                        total_new_debt2016+= new_annuity_gross
                        total_old_debt2016+= float(installment.bruto_numeric)
                        
                    remaining_periods-=1;
                elif installment.tip_dugovanja=='Termination':    
                    total_new_debt+= new_principal
                    total_old_debt+= float(installment.glavnica_numeric)
                    if (installment.datum_date>datetime.strptime(lm_date, '%d/%m/%y').date()):
                        total_new_debt2016+= new_principal
                        total_old_debt2016+= float(installment.glavnica_numeric)
                
               
                #
                #pdb.set_trace()
            total_difference = total_paid-total_new_debt
            return_amount = total_difference-total_tax_penalty-total_interest_penalty
            
            
            update_contract_data={'contract_processed':True,'new_debt_installments':total_new_debt,'old_debt_installments':total_old_debt,
                            'new_debt_installments2016':total_new_debt2016,'old_debt_installments2016':total_old_debt2016,'penalty_interest':total_interest_penalty,
                             'interest_diff':total_interest_diff,'tax_diff':total_tax_diff,'interest_diff2016':total_interest_diff2016,'tax_diff2016':total_tax_diff2016,
                             'penalty_interest2016':total_interest_penalty2016,'total_principal_adjustment':total_principal_adjustment,
                             'penalty_interest_tax':total_tax_penalty,'penalty_interest_tax2016':total_tax_penalty2016,'difference':0,'return_amount':0,'difference2016':0,'return_amount2016':0}
                
            upd2 = tbl.update().where(tbl.c["contract_number"]==contract.contract_number).values(**update_contract_data).returning(literal_column('*'))
            result = conn.execute(upd2)
                    
            if ((count_contracts % 100)==0):
                dt_end = datetime.now()
                timedelta = dt_end - dt_start
                print('Processed '+str(count_contracts) +' contracts in '+str(timedelta.seconds)+' seconds')
                #print('Processing contract '+str(contract.contract_number) +' '+str(timedelta.microseconds/1000)+' ms')
    def process_contracts(self):
       
        #select contract record from database
        table_name_contracts="hc"
        table_name_plans="hp"
        
        tbl = Table(table_name_contracts, Base.metadata, autoload=True,autoload_with=engine)
        tbl2 = Table(table_name_plans, Base.metadata, autoload=True,autoload_with=engine)
        
        
        #for column in tbl.columns.values():
        #    if isinstance(column.type, Numeric):
        #        column.type.asdecimal = False
                
        #for column2 in tbl2.columns.values():
        #    if isinstance(column2.type, Numeric):
        #        column2.type.asdecimal = False
            
        conn = engine.connect()
        #rec = tbl.select().where(tbl.c[table_name+"_id"].in_(list_id)).order_by(tbl.c[table_name+"_id"])
        rec = tbl.select().where(tbl.c["process"]==True,tbl.c["contract_processed"]==False).order_by(tbl.c["contract_number"])
        contracts = conn.execute(rec).fetchall()
        #pdb.set_trace()
        count_contracts=0
        dt_start = datetime.now()
        for contract in contracts:
            count_contracts+=1
            
            contract_number = contract.contract_number
            payment_plan_select = tbl2.select().where(tbl2.c["contract_number"]==contract_number,tbl2.c["exclude"]==0).order_by(tbl2.c["period"],tbl2.c["tip_dugovanja"],tbl2.c["plan_detail_id"])
            payment_plan = conn.execute(payment_plan_select).fetchall()
            remaining_principal = float(contract.financed_amount)
            remaining_periods = float(contract.number_of_installments)
            total_tax_penalty = 0
            total_interest_penalty = 0
            total_tax_penalty2016 = 0
            total_interest_penalty2016 = 0
            total_new_debt = 0
            total_old_debt = 0
            total_new_debt2016 = 0
            total_old_debt2016 = 0
            total_principal_adjustment = 0
            prev_remaining_principal = remaining_principal
            prev_principal = 0
            total_interest_diff=0
            total_tax_diff=0
            total_interest_diff2016=0
            total_tax_diff2016=0
            found_first = False
            prev_tip_dugovanja='Installment'
            total_paid = float(contract.paid_installments)+float(contract.paid_installments_m)+float(contract.discount_amount)
            
            for installment in payment_plan:
                
                principal_adjustment = 0                         
                residual_amount = 0
                
                #pdb.set_trace()
                period_interest_rate = self.calculate_interest_rate(float(installment.interest_rate),360,30)
                if contract.interest_type=="F":
                     period_interest_rate = self.calculate_interest_rate(float(contract.fixed_interest_rate),360,30)
                
                if installment.tip_dugovanja == 'Installment':
                    if  ((prev_remaining_principal - float(installment.glavnica_numeric) - float(installment.stanje_glavnice_numeric))>0.1 and installment.stanje_glavnice_numeric>0):
                        principal_adjustment =  prev_remaining_principal -  float(installment.stanje_glavnice_numeric) - float(installment.glavnica_numeric)
                        total_principal_adjustment+=principal_adjustment
                else:
                    if installment.neto_numeric==0:
                        principal_adjustment =  prev_remaining_principal - float(installment.glavnica_numeric)
                        total_principal_adjustment+=principal_adjustment
                
                if (installment.tip_dugovanja == 'Installment' and contract.reset_princ and installment.datum_date>datetime.strptime(princ_copy_date, '%d/%m/%y').date() and not found_first):
                    found_first = True
                    remaining_principal = prev_remaining_principal
                    principal_adjustment=0
                
                if contract.restart_count>0:
                    remaining_periods = contract.restart_count
                
                if installment.restart_installment_number>0:
                    remaining_periods = installment.restart_installment_number
                
                    
                remaining_principal = remaining_principal - principal_adjustment
                
                if installment.reprogram_principal>0:
                    remaining_principal = float(installment.reprogram_principal)
                
                remaining_principal_for_calc = remaining_principal
                if contract.residual_amount>0:
                    residual_amount = float(contract.residual_amount)
                    remaining_principal_for_calc = remaining_principal - residual_amount
                     
                if installment.tip_dugovanja.strip()=='Installment':
                    new_annuity_net = self.calculate_annuity(remaining_principal_for_calc,period_interest_rate,remaining_periods,residual_amount,installment.contract_number)
                    new_interest = remaining_principal*period_interest_rate
                    if installment.kamata_numeric==0:    
                        new_interest = float(installment.kamata_numeric)
                    new_principal = new_annuity_net - new_interest
                    new_vat_amount = new_interest*float(installment.vat_rate)
                    if contract.contract_type=="OL":
                        new_vat_amount = new_annuity_net*float(installment.vat_rate)    
                    new_annuity_gross = new_annuity_net+new_vat_amount                
                
                elif installment.tip_dugovanja.strip()=='Last Installment':
                    
                    new_annuity_net = remaining_principal
                    new_interest = 0
                    new_principal = remaining_principal
                    new_vat_amount = 0
                    new_annuity_gross = remaining_principal
                        
                elif installment.tip_dugovanja.strip()=='Termination':
                    
                    if (prev_remaining_principal-principal_adjustment == float(installment.glavnica_numeric)):       
                        
                        new_annuity_net = remaining_principal
                        new_interest = 0
                        new_principal = remaining_principal
                        new_vat_amount = (remaining_principal)*float(installment.vat_rate)
                        new_annuity_gross = new_annuity_net+new_vat_amount        
                    else:
                        fee_rate = float(installment.neto_numeric)/prev_remaining_principal
                        new_annuity_net = remaining_principal*fee_rate
                        new_interest = 0
                        new_principal = new_annuity_net
                        new_vat_amount = new_principal*float(installment.vat_rate)
                        new_annuity_gross = new_annuity_net+new_vat_amount
                    
                if installment.copy_data:
                    new_annuity_net = float(installment.neto_numeric)
                    new_interest = float(installment.kamata_numeric)
                    new_principal = float(installment.glavnica_numeric)
                    new_vat_amount = float(installment.porez_numeric)
                    new_annuity_gross = float(installment.bruto_numeric)
                    remaining_principal = prev_remaining_principal
                    principal_adjustment=0
                   
                    
                if installment.neto_numeric==0:
                    new_annuity_net = float(installment.neto_numeric)
                if installment.kamata_numeric==0:    
                    new_interest = float(installment.kamata_numeric)
                if installment.glavnica_numeric==0:       
                    new_principal = float(installment.glavnica_numeric)
                if installment.porez_numeric==0:   
                    new_vat_amount = float(installment.porez_numeric)
                if installment.bruto_numeric==0:   
                    new_annuity_gross = float(installment.bruto_numeric)
               
                
                
               
                number_of_days_for_penalty = datetime.strptime(cut_off_date, '%d/%m/%y').date()-installment.datum_date
                #pdb.set_trace()
                tax_difference = new_vat_amount-float(installment.porez_numeric) 
                interest_difference = new_interest-float(installment.kamata_numeric)
                
                if (installment.datum_date>datetime.strptime(cut_off_date, '%d/%m/%y').date()):
                    interest_difference = 0
                    tax_difference = 0
                
                if tax_difference!=0 and total_paid>=round(total_old_debt+float(installment.bruto_numeric),2):
                    penalty_interest_tax = self.calculate_penalty_interest_complex(installment.datum_date,tax_difference)
                elif tax_difference!=0 and total_paid-total_old_debt>0:
                    covered_part = total_paid-total_old_debt
                    paid_ratio  = covered_part/float(installment.bruto_numeric)
                    #interest_ratio = installment.kamata_numeric/installment.gross_numeric
                    old_tax_adj = float(installment.porez_numeric)*paid_ratio
                    tax_difference = new_vat_amount-old_tax_adj
                    penalty_interest_tax = self.calculate_penalty_interest_complex(installment.datum_date,tax_difference)
                else:
                    tax_difference = 0
                    penalty_interest_tax = 0
                   
                if interest_difference!=0 and total_paid>=round(total_old_debt+float(installment.bruto_numeric),2):
                    #print('1 ---> '+str(installment.period)+' ---> '+str(total_paid)+' ---> '+str(total_old_debt+float(installment.bruto_numeric)))
                    penalty_interest = self.calculate_penalty_interest_complex(installment.datum_date,interest_difference)
                elif interest_difference!=0 and total_paid-total_old_debt>0:
                    covered_part = total_paid-total_old_debt
                    paid_ratio  = covered_part/float(installment.bruto_numeric)
                    #interest_ratio = installment.kamata_numeric/installment.gross_numeric
                    old_interest_adj = float(installment.kamata_numeric)*paid_ratio
                    interest_difference = new_interest-old_interest_adj
                    penalty_interest = self.calculate_penalty_interest_complex(installment.datum_date,interest_difference)
                    #print('Total paid '+str(total_paid))
                    #print('Total old debt '+str(total_old_debt+float(installment.bruto_numeric)))
                    print('Period '+str(installment.period))
                    print('Bruto '+str(installment.bruto_numeric))
                    print('Old debt '+str(total_old_debt))
                    print('Total paid '+str(total_paid))
                    print('Covered part '+str(covered_part))
                    print('Paid ratio '+str(paid_ratio))
                    print('Old interest adj '+str(old_interest_adj))
                    print('Old interest '+str(float(installment.kamata_numeric)))
                    #print('2 ---> '+str(installment.period)+' ---> '+str(total_paid)+' ---> '+str(total_old_debt+float(installment.bruto_numeric)))
                else:
                    interest_difference = 0
                    penalty_interest = 0
                   
                total_interest_diff+=interest_difference
                total_tax_diff+=tax_difference
                
                total_tax_penalty+= penalty_interest_tax
                total_interest_penalty+= penalty_interest
                
                if installment.datum_date>datetime.strptime(limit_date, '%d/%m/%y').date() and installment.tip_dugovanja.strip() in ['Installment','Termination']:
                    total_tax_penalty2016+= penalty_interest_tax
                    total_interest_penalty2016+=penalty_interest
                    total_interest_diff2016+=interest_difference
                    total_tax_diff2016+=tax_difference
                    
                if float(installment.stanje_glavnice_numeric)>0:
                    prev_remaining_principal = float(installment.stanje_glavnice_numeric)
                
                prev_principal = float(installment.glavnica_numeric)
                prev_tip_dugovanja = installment.tip_dugovanja
                #print(prev_remaining_principal)
                
                
                #update_records
                update_data={'remaining_principal':remaining_principal,'interest_difference':interest_difference,'tax_difference':tax_difference,
                             'penalty_interest':penalty_interest,'penalty_interest_tax':penalty_interest_tax,
                             'interest_rate_final':period_interest_rate,'number_of_installments':remaining_periods,
                             'new_annuity_gross':new_annuity_gross,'new_vat_amount':new_vat_amount,'new_annuity_net':new_annuity_net,'new_principal':new_principal,'new_interest':new_interest}
                upd = tbl2.update().where(tbl2.c["id"]==installment.id).values(**update_data).returning(literal_column('*'))
   
                result = conn.execute(upd)
                if remaining_principal - new_principal!=0:
                    remaining_principal-= new_principal
                    
                                                                                          
                    #pdb.set_trace()                                                                                
                                                                                                
                if installment.tip_dugovanja=='Installment':
                    total_new_debt+= new_annuity_gross
                    total_old_debt+= float(installment.bruto_numeric)
                    if (installment.datum_date>datetime.strptime(limit_date, '%d/%m/%y').date()):
                        total_new_debt2016+= new_annuity_gross
                        total_old_debt2016+= float(installment.bruto_numeric)
                        
                    remaining_periods-=1;
                elif installment.tip_dugovanja=='Termination':    
                    total_new_debt+= new_principal
                    total_old_debt+= float(installment.glavnica_numeric)
                    if (installment.datum_date>datetime.strptime(limit_date, '%d/%m/%y').date()):
                        total_new_debt2016+= new_principal
                        total_old_debt2016+= float(installment.glavnica_numeric)
                
               
                #
                #pdb.set_trace()
            total_difference = total_paid-total_new_debt
            return_amount = total_difference-total_tax_penalty-total_interest_penalty
            
            paid_debt_diff = total_paid - total_old_debt - float(contract.old_debt_interim_interest)
            difference =total_old_debt + float(contract.old_debt_interim_interest) -total_new_debt- float(contract.new_debt_interim)
            difference2016 =total_old_debt2016 -total_new_debt2016
            ret_amount =difference-total_interest_penalty-total_tax_penalty
            return_amount2016 =difference2016-total_interest_penalty2016-total_tax_penalty2016
            update_contract_data={'contract_processed':True,'new_debt_installments':total_new_debt,'old_debt_installments':total_old_debt,
                            'new_debt_installments2016':total_new_debt2016,'old_debt_installments2016':total_old_debt2016,'penalty_interest':total_interest_penalty,
                             'interest_diff':total_interest_diff,'tax_diff':total_tax_diff,'interest_diff2016':total_interest_diff2016,'tax_diff2016':total_tax_diff2016,
                             'penalty_interest2016':total_interest_penalty2016,'total_principal_adjustment':total_principal_adjustment,
                             'penalty_interest_tax':total_tax_penalty,'penalty_interest_tax2016':total_tax_penalty2016,
                             'paid_debt_diff':paid_debt_diff,'difference':difference,'return_amount':ret_amount,'difference2016':difference2016,'return_amount2016':return_amount2016}
                
            upd2 = tbl.update().where(tbl.c["contract_number"]==contract.contract_number).values(**update_contract_data).returning(literal_column('*'))
            result = conn.execute(upd2)
                    
            if ((count_contracts % 100)==0):
                dt_end = datetime.now()
                timedelta = dt_end - dt_start
                print('Processed '+str(count_contracts) +' contracts in '+str(timedelta.seconds)+' seconds')
                #print('Processing contract '+str(contract.contract_number) +' '+str(timedelta.microseconds/1000)+' ms')
            
    def calculate_annuity(self,principal,interest_rate,period,residual_amount,contract):
        #(principal)/((1-POWER(1+interest_rate;(-1)*period))/interest_rate)
        #print('Principal '+str(principal))
        #print('Residual '+str(residual_amount))
        #print('Interest_rate '+str(interest_rate))
        #print('Period '+str(period))
        middle_part = 1-pow(1+interest_rate,-1*period)
        if middle_part!=0:
            annuity = principal/(middle_part/interest_rate)
            #annuity=annuity1/interest_rate
        else:
            annuity = 0
            print('Middle part is zero')
            print('Contract '+contract)
            print('Period '+str(period))
        #print('Annuity before '+str(annuity))    
        if (residual_amount>0):
            annuity+=residual_amount*interest_rate
        #print('Annuity after '+str(annuity))
        #print('Middle '+str(middle_part))
        return annuity
    
    def calculate_interest_rate(self,annual_interest_rate,number_of_days_in_year,number_of_days_in_period):
       
        interest_rate = annual_interest_rate/(number_of_days_in_year/number_of_days_in_period)
        interest_rate = interest_rate/100
        return interest_rate
    def calculate_daily_interest_rate(self,annual_interest_rate,number_of_days_in_year):
       
        interest_rate = annual_interest_rate/number_of_days_in_year
        interest_rate_final = interest_rate/100
        return interest_rate_final
    
    def calculate_penalty_interest(principal,days,daily_interest_rate):
       
        penalty_interest_amount = principal*days*daily_interest_rate
        
        return penalty_interest_amount
    
    def calculate_penalty_interest_complex(self,start_date,principal):
        '''Penalty interest rates
        OD	DO	% zatezne kamate	Days
        31.05.1994	30.06.1994	30	31
        01.07.1994	07.05.1996	22	676
        08.05.1996	10.09.1996	24	125
        11.09.1996	30.06.2002	18	2118
        01.07.2002	31.12.2007	15	2009
        01.01.2008	30.06.2011	17	1276
        01.07.2011	31.07.2015	15	1491
        01.08.2015	31.12.2015	10.14	152
        01.01.2016	30.06.2016	10.05	181
        01.07.2016	31.12.2016	9.88	184
        01.01.2017	30.06.2017	9.68	181
        01.07.2017	31.12.2017	9.41	184
        01.01.2018	30.06.2018	9.09	181
        01.07.2018	31.12.2018	8.82	184
        01.01.2019	30.06.2019	8.54	181
        01.07.2019	31.12.2019	8.3	    183
        01.01.2020	30.06.2020	8.11	182
        01.07.2020	31.12.2020	7.89	184
        01.01.2021	30.06.2021	7.75	181
        '''
        started_calculation = False
        penalty_interest_amount=0
        penalty_interest_interim = 0
        for item in penalty_interest_rates:
            if started_calculation:
                penalty_interest_interim= principal*item['days']*self.calculate_daily_interest_rate(item['interest_rate'],365)
            elif item['date_from']<=start_date and item['date_to']>=start_date:
                started_calculation = True
                no_days = item['date_to']-start_date
                #print("No days = ",no_days.days)
                #print("Principal = ",principal)
                penalty_interest_interim = principal*no_days.days*self.calculate_daily_interest_rate(item['interest_rate'],365)
               
                #print("Interest = ",penalty_interest_amount)
            penalty_interest_amount+= penalty_interest_interim
            #print("Penalty interest = ",penalty_interest_interim)
        return penalty_interest_amount
    
    
                
        
#create class
pp = PaymentPlan()
#process contracts
pp.process_contracts()
