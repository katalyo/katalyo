######################## blockchain processing module ###############################

###############################################################################

import json
#from django.forms.models import model_to_dict
import hashlib
import requests
import pdb
from django.utils.dateparse import parse_datetime
from datetime import datetime,timedelta
from django.utils import timezone

def get_record_from_blockchain(contract,table,pKey):    

  #nodeos server
  nodeos_url = "http://192.168.80.129:8888"
    
  #keosd wallet server
  #keosd_url = "http://192.168.80.130:8900"
  #convert abi json to bin
  url = nodeos_url+"/v1/chain/get_table_by_scope"
  #pdb.set_trace()
  r0 = requests.post(url, None,{
        'code': contract,
        'table': table,
        'lower_bound': str(pKey),
        'upper_bound': '',
        'limit':1})
  #pdb.set_trace()
  js0 = r0.json()
  return js0,r0.status_code

def get_table_rows(contract,table,scope,index_position,key_type,pKey):    

  #nodeos server
  nodeos_url = "http://192.168.80.129:8888"
    
  #keosd wallet server
  #keosd_url = "http://192.168.80.130:5555"
  #convert abi json to bin
  url = nodeos_url+"/v1/chain/get_table_rows"
  #pdb.set_trace()
  r0 = requests.post(url, None,{
        'code': contract,
        'table': table,
        'scope': scope,
        'index_position': index_position,
        'key_type':key_type,
        'lower_bound': str(pKey),
        'limit':1,
        'json':True})
  #pdb.set_trace()
  js0 = r0.json()
  return js0,r0.status_code 

def get_transaction(trx_id,block_num_hint):    

  #nodeos server
  nodeos_url = "http://192.168.80.129:8888"
    
  #keosd wallet server
  #keosd_url = "http://192.168.80.130:5555"
  #convert abi json to bin
  url = nodeos_url+"/v1/history/get_transaction"
  #pdb.set_trace()
  r0 = requests.post(url, None,{
        'id': trx_id,
        'block_num_hint': block_num_hint})
  #pdb.set_trace()
  js0 = r0.json()
  return js0,r0.status_code 
    

def send_transaction(trx_json,contract,action):    
    #nodeos server
    nodeos_url = "http://192.168.80.129:8888"
    
    #keosd wallet server
    keosd_url = "http://192.168.80.129:5555"
   
    #convert abi json to bin
    url_abi = nodeos_url+"/v1/chain/abi_json_to_bin"
    #pdb.set_trace()
    r0 = requests.post(url_abi, None,{
        'code': contract,
        'action': action,
        'args':trx_json})
    js0 = r0.json()
    
    
    if 'code' in js0:
      if js0['code']!=200:
        return js0,0,js0['code']  
    
    
    #get info - we need this to get the block number
    url_get_info = nodeos_url+"/v1/chain/get_info"
    
    r1 =  requests.post(url_get_info)
    
    js1 = r1.json()
    
   
    
    #get block
    url_get_block = nodeos_url+"/v1/chain/get_block"
    
    r2 =  requests.post(url_get_block,None,{'block_num_or_id':js1['last_irreversible_block_num']})
    
    js2 = r2.json()
    
    expired_date = parse_datetime(js2['timestamp'])
   
    expired_date = expired_date+timedelta(minutes=1)
    expired = str(expired_date.isoformat())
    
    #unlock wallet
    #status 500 ok if already unlocked error.code = 3120007
    url_wallet_unlock = keosd_url+"/v1/wallet/unlock"
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    #wallet_data = ['name':'default','password':'PW5Jpz7MpViNRnqMHtYiskvh5oys7ZFzMkLbtyUg5tJm6W5azPzek']
    wallet_data = '["katalyo","PW5JfSa3cY2khNUFncfUsNvsMi99w8iJArPLQg9umMoipqFzvX56s"]'
    #r2 =  requests.post(url_wallet_unlock,data=wallet_data,headers=headers)
    r3 = requests.request("POST", url_wallet_unlock,headers=headers, data=wallet_data)
    #pdb.set_trace()
    if r3.status_code not in [200,500]:
      return r3,js1['last_irreversible_block_num'],r3.status_code
    js3 = r3.json()
    
    if 'code' in js3:
      if js3['code']!=200:
        if js3['error']['code']!=3120007:
          return js3,js1['last_irreversible_block_num'],js3['code']  
    
    
    
    #get all available keys for wallet
    url_get_public_keys = keosd_url+"/v1/wallet/get_public_keys"
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    #r3 =  requests.post(url_get_public_keys,headers=headers)
    r4 = requests.request("POST", url_get_public_keys, headers=headers)
    js4 = r4.json()
    
    if 'code' in js4:
      if js4['code']!=200:
        return js4,js1['last_irreversible_block_num'],js4['code']
    elif len(js4)==0:
      return js4,js1['last_irreversible_block_num'],500 
    
   
     
    #get required keys
    url_get_required_keys = nodeos_url+"/v1/chain/get_required_keys"
    #how to get signatures
   
    
   
    r5 = requests.post(url_get_required_keys,None,{
         'available_keys': js4,
         'transaction': {
          'actions': [
            {
              'account':'kataly1.code',
              'authorization':[
                {
                  'actor':'kataly1.code',
                  'permission':'active'
                  
                }
              ],
              'data':js0['binargs'],
              'name':'hi'
            }
            ],
          'context_free_actions':[],
          'context_free_data':[],
          
          'expiration': expired,
          
          'ref_block_num':js2['block_num'],
          'ref_block_prefix': js2['ref_block_prefix'],
         
          'max_cpu_usage_ms': 0,
          'max_net_usage_words':0,
          'delay_sec':0,
          'signatures':[]
          }
       })

   
    js5 = r5.json()
    
     
    if 'code' in js5:
      if js5['code']!=200:
        return js5,js1['last_irreversible_block_num'],js5['code']
   
    
    #sign transaction
    
    url_sign = keosd_url+"/v1/wallet/sign_transaction"
    
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    #wallet_data = ['name':'default','password':'PW5Jpz7MpViNRnqMHtYiskvh5oys7ZFzMkLbtyUg5tJm6W5azPzek']
    sign_data = "["+str({
        "expiration": expired,
        "ref_block_num":js2['block_num'],
        "ref_block_prefix": js2['ref_block_prefix'],
        "max_cpu_usage_ms": 0,
        "max_net_usage_words":0,
        "delay_sec":0,
        "context_free_actions": [],
          "actions": [
            {
              "account":"kataly1.code",
              "name":"createds",
              "authorization":[
                {
                  "actor":"kataly1.code",
                  "permission":"active"
                  
                }
              ],
              "data":js0['binargs']  
            }
            ],
           "transaction_extensions": [],
          "signatures": [],
          "context_free_data": []
        })
    
    #pdb.set_trace()
    sign_data=sign_data+','+str( js5['required_keys'])+",'"+str(js1['chain_id'])+str("']")
    sign_data=sign_data.replace("'", "\"") 
    r6 = requests.request("POST", url_sign,headers=headers, data=sign_data)    
        
    js6=r6.json()
    #  
    if 'code' in js6:
      if js6['code']!=200:
        return js6,js1['last_irreversible_block_num'],js6['code']
   
    
    #push transaction
    url_trx = nodeos_url+"/v1/chain/push_transaction"
    
    js6['expiration']=int(expired_date.replace(tzinfo=timezone.utc).timestamp())
    
    packed_trx=pack_transaction(js6)
    #pdb.set_trace()
    for sign in js6["signatures"]:
      sign = sign.replace("'", "\"")
      
    push_trx = {
        "signatures": js6["signatures"],
        "compression": "none",
        "packed_context_free_data":"",
        "packed_trx":packed_trx}
  
    
    
    r7 = requests.post(url_trx, None,push_trx)
    
    js7 = r7.json()
   
    return js7,js1['last_irreversible_block_num'],200

def send_generic_transaction(trx_json,contract,funciton,account):    
    #nodeos server
    nodeos_url = "http://192.168.80.129:8888"
    
    #keosd wallet server
    keosd_url = "http://192.168.80.129:5555"
   
    #convert abi json to bin
    url_abi = nodeos_url+"/v1/chain/abi_json_to_bin"
    #pdb.set_trace()
    r0 = requests.post(url_abi, None,{
        'code': contract,
        'action': action,
        'args':trx_json})
    js0 = r0.json()
    
    
    if 'code' in js0:
      if js0['code']!=200:
        return js0,0,js0['code']  
    
    
    #get info - we need this to get the block number
    url_get_info = nodeos_url+"/v1/chain/get_info"
    
    r1 =  requests.post(url_get_info)
    
    js1 = r1.json()
    
   
    
    #get block
    url_get_block = nodeos_url+"/v1/chain/get_block"
    
    r2 =  requests.post(url_get_block,None,{'block_num_or_id':js1['last_irreversible_block_num']})
    
    js2 = r2.json()
    
    expired_date = parse_datetime(js2['timestamp'])
   
    expired_date = expired_date+timedelta(minutes=1)
    expired = str(expired_date.isoformat())
    
    #unlock wallet
    #status 500 ok if already unlocked error.code = 3120007
    url_wallet_unlock = keosd_url+"/v1/wallet/unlock"
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    #wallet_data = ['name':'default','password':'PW5Jpz7MpViNRnqMHtYiskvh5oys7ZFzMkLbtyUg5tJm6W5azPzek']
    wallet_data = '["katalyo","PW5JfSa3cY2khNUFncfUsNvsMi99w8iJArPLQg9umMoipqFzvX56s"]'
    #r2 =  requests.post(url_wallet_unlock,data=wallet_data,headers=headers)
    r3 = requests.request("POST", url_wallet_unlock,headers=headers, data=wallet_data)
    #pdb.set_trace()
    if r3.status_code not in [200,500]:
      return r3,js1['last_irreversible_block_num'],r3.status_code
    js3 = r3.json()
    
    if 'code' in js3:
      if js3['code']!=200:
        if js3['error']['code']!=3120007:
          return js3,js1['last_irreversible_block_num'],js3['code']  
    
    
    
    #get all available keys for wallet
    url_get_public_keys = keosd_url+"/v1/wallet/get_public_keys"
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    #r3 =  requests.post(url_get_public_keys,headers=headers)
    r4 = requests.request("POST", url_get_public_keys, headers=headers)
    js4 = r4.json()
    
    if 'code' in js4:
      if js4['code']!=200:
        return js4,js1['last_irreversible_block_num'],js4['code']
    elif len(js4)==0:
      return js4,js1['last_irreversible_block_num'],500 
    
   
     
    #get required keys
    url_get_required_keys = nodeos_url+"/v1/chain/get_required_keys"
    #how to get signatures
   
    
   
    r5 = requests.post(url_get_required_keys,None,{
         'available_keys': js4,
         'transaction': {
          'actions': [
            {
              'account':'kataly1.code',
              'authorization':[
                {
                  'actor':'kataly1.code',
                  'permission':'active'
                  
                }
              ],
              'data':js0['binargs'],
              'name':'hi'
            }
            ],
          'context_free_actions':[],
          'context_free_data':[],
          
          'expiration': expired,
          
          'ref_block_num':js2['block_num'],
          'ref_block_prefix': js2['ref_block_prefix'],
         
          'max_cpu_usage_ms': 0,
          'max_net_usage_words':0,
          'delay_sec':0,
          'signatures':[]
          }
       })

   
    js5 = r5.json()
    
     
    if 'code' in js5:
      if js5['code']!=200:
        return js5,js1['last_irreversible_block_num'],js5['code']
   
    
    #sign transaction
    
    url_sign = keosd_url+"/v1/wallet/sign_transaction"
    
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    #wallet_data = ['name':'default','password':'PW5Jpz7MpViNRnqMHtYiskvh5oys7ZFzMkLbtyUg5tJm6W5azPzek']
    sign_data = "["+str({
        "expiration": expired,
        "ref_block_num":js2['block_num'],
        "ref_block_prefix": js2['ref_block_prefix'],
        "max_cpu_usage_ms": 0,
        "max_net_usage_words":0,
        "delay_sec":0,
        "context_free_actions": [],
          "actions": [
            {
              "account":"kataly1.code",
              "name":"createds",
              "authorization":[
                {
                  "actor":"kataly1.code",
                  "permission":"active"
                  
                }
              ],
              "data":js0['binargs']  
            }
            ],
           "transaction_extensions": [],
          "signatures": [],
          "context_free_data": []
        })
    
    #pdb.set_trace()
    sign_data=sign_data+','+str( js5['required_keys'])+",'"+str(js1['chain_id'])+str("']")
    sign_data=sign_data.replace("'", "\"") 
    r6 = requests.request("POST", url_sign,headers=headers, data=sign_data)    
        
    js6=r6.json()
    #  
    if 'code' in js6:
      if js6['code']!=200:
        return js6,js1['last_irreversible_block_num'],js6['code']
   
    
    #push transaction
    url_trx = nodeos_url+"/v1/chain/push_transaction"
    
    js6['expiration']=int(expired_date.replace(tzinfo=timezone.utc).timestamp())
    
    packed_trx=pack_transaction(js6)
    #pdb.set_trace()
    for sign in js6["signatures"]:
      sign = sign.replace("'", "\"")
      
    push_trx = {
        "signatures": js6["signatures"],
        "compression": "none",
        "packed_context_free_data":"",
        "packed_trx":packed_trx}
  
    
    
    r7 = requests.post(url_trx, None,push_trx)
    
    js7 = r7.json()
   
    return js7,js1['last_irreversible_block_num'],200
def pack_transaction(trx_obj):
  
  ba_trx = bytearray()
  
  push_int(ba_trx,trx_obj["expiration"] & 0xFFFFFFFF)
  push_short(ba_trx,trx_obj["ref_block_num"] & 0xFFFF)
  push_int(ba_trx,trx_obj["ref_block_prefix"] & 0xFFFFFFFF)
  push_variableUInt(ba_trx,trx_obj["max_net_usage_words"])
  push_variableUInt(ba_trx,trx_obj["max_cpu_usage_ms"])
  push_variableUInt(ba_trx,trx_obj["delay_sec"])
  push_actions(ba_trx,list())  # TODO packfreedata
  push_actions(ba_trx,trx_obj["actions"])
  push_variableUInt(ba_trx,0) # TODO packexdata
  
  return ba_trx.hex()

def push_base(bytes_list, val, iteration):
  
        for i in iteration:
            bytes_list.append(0xFF & val >> i)

def push_short(bytes_list,val):
        push_base(bytes_list,val, range(0, 9, 8))

def push_int(bytes_list,val):
        push_base(bytes_list,val, range(0, 25, 8))

def push_long(bytes_list, val):
        push_base(bytes_list,val, range(0, 57, 8))

def push_char(bytes_list,val):
        bytes_list.append(int(val))

def push_variableUInt(bytes_list,val):
        b = int((val) & 0x7f)
        val = val >> 7
        b = b | (((val > 0) if 1 else 0) << 7)
        push_char(bytes_list,b)
        while val:
            b = int((val) & 0x7f)
            val = val >> 7
            b = b | (((val > 0) if 1 else 0) << 7)
            push_char(bytes_list,b)

def push_actions(bytes_list, val_list):
        push_variableUInt(bytes_list,len(val_list))
        for i in val_list:
            push_long(bytes_list,type_name_to_long(i["account"]))
            push_long(bytes_list,type_name_to_long(i["name"]))
            push_permission(bytes_list,i["authorization"])
            if i["data"]:
                push_data(bytes_list,i["data"])
            else:
                push_variableUInt(bytes_list,0)

def push_permission(bytes_list, val_list):
        push_variableUInt(bytes_list,len(val_list))
        for i in val_list:
            push_long(bytes_list,type_name_to_long(i["actor"]))
            push_long(bytes_list,type_name_to_long(i["permission"]))

def push_data(bytes_list, val):
        bytes = bytearray.fromhex(val)
        push_variableUInt(bytes_list,len(bytes))
        for i in bytes:
            bytes_list.append(i)

def push_transaction_extensions(bytes_list, val_list):
        push_variableUInt(bytes_list,len(val_list))

def char_to_symbol(c):
    if (ord(c) >= ord('a') and ord(c) <= ord('z')):
        return chr(((ord(c) - ord('a')) + 6))

    if (ord(c) >= ord('1') and ord(c) <= ord('5')):
        return chr(((ord(c) - ord('1')) + 1))
    return chr(0)
  
def type_name_to_long(type_name):
    if type_name == None or type_name == "":
        return 0
    c = 0
    value = 0
    type_name_len = len(type_name)
    for i in range(13):
        if (i < type_name_len and i <= 12):
            c = ord(char_to_symbol(type_name[i]))
        if (i < 12):
            c &= 0x1f
            c <<= 64 - 5 * (i + 1)
        else:
            c &= 0x0f
        value |= c
    return value
