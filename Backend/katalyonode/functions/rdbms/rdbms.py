# myapp/rdbms.py
import pdb
from django.conf import settings
from django.db import IntegrityError
import sqlalchemy as sa
from sqlalchemy import create_engine,inspect,event,literal_column
from katalyonode.functions.rdbms.alchemymodels import UserExtendedPropertiesSa,LanguageSa, OrganisationsSa

from sqlalchemy import Table, Column, Integer,Boolean, String, Text, ForeignKey,Numeric,Float,Date,Time,DateTime, MetaData
from sqlalchemy import and_,union ,union_all
from sqlalchemy.orm import sessionmaker,relationship, joinedload, subqueryload, join,outerjoin,defer,undefer,load_only,declarative_base,aliased,load_only
from sqlalchemy.sql import alias,label,elements,literal
from sqlalchemy.sql.expression import literal_column
#from sqlalchemy.sql.elements import quoted_name
#from katalyonode.models.models import ResourceDef,ErrorLog
from django.utils import timezone
from katalyonode.functions.rdbms.alchemymodels import auth_user,codesdetail,ResourceDefSa,ResourceModelDefinitionSa
from sqlalchemy.pool import NullPool
import json

      

@event.listens_for(Table, "column_reflect")
def column_reflect(inspector, table, column_info):
     
    table_name=table.name
    if column_info is not None:
        if not column_info['name'].startswith("Field") and not column_info['name'].startswith(table_name) and table_name.count("resource")<=1:
            column_info['key'] = table_name+"_"+column_info['name']

        elif column_info['name'] in ("FieldType","FieldName") and table_name =='katalyonode_resourcemodeldefinition':
            column_info['key'] = table_name+"_"+column_info['name']

  
    
#poolclass=NullPool,     
g_db_engine = create_engine(settings.DB_ENGINE, echo=settings.DB_ENGINE_ECHO, connect_args={'options': '-c lock_timeout=5000'})
g_db_engine_autocommit = g_db_engine.execution_options(isolation_level="AUTOCOMMIT")
g_Base = declarative_base()
g_metadata = MetaData(bind=g_db_engine)

class RdbmsClass():
    def __init__(self):
        #super().__init__()
        self.engine = g_db_engine
        self.metadata=g_metadata
        self.Base = g_Base
        self.Language=None
        self.OrganisationId = None
        self.LocalSession=None
            
    
    def get_user_extended_prop(self,user_id):     #get advanced user properties #MIGORSA  
        session = self.Session()
        with session.begin():
            #uep = session.query(UserExtendedPropertiesSa, LanguageSa).filter(UserExtendedPropertiesSa.UserId == user_id).first() 
            uep,lang,org = session.query(UserExtendedPropertiesSa,LanguageSa,OrganisationsSa).outerjoin(LanguageSa,LanguageSa.id==UserExtendedPropertiesSa.Lang).outerjoin(OrganisationsSa,OrganisationsSa.id==UserExtendedPropertiesSa.Organisation).filter(UserExtendedPropertiesSa.UserId==user_id).first()                
            session.expunge_all()
        return uep,lang,org
    
    def SetLanguage(self,lang):
    
        if hasattr(lang,'id'):
            self.Language=lang
        else:
            raise Exception('Language is '+str(type(lang)) +' in SetLanguage '+str( lang)+' Orga id = '+str(self.OrganisationId))
                                
                                
        

    def SetOrganisation(self,org_id):
        self.OrganisationId=org_id

    def Session(self):
        _Session = sessionmaker(bind=self.engine)
        self.LocalSession = _Session()
        return self.LocalSession
   #not used 
    #def reflect(self,tables,_schema=None):
        
     #   self.metadata.reflect(bind=self.engine,schema=_schema, views=False, only=tables, extend_existing=False, autoload_replace=True, resolve_fks=True)
    
    def define_alchemy_table_attr(self,table_name,columns,schema='public'):
    
        attr_dict = {'__tablename__': table_name,'extend_existing':True,'schema':schema}
        attr_dict.update(columns)
        return attr_dict

    def get_alchemy_table_class(self,attr_dict):
        
        return type('DatasetTableClass', (self.Base,), attr_dict)
        
    def get_table(self,table_name): 
        return Table(table_name, self.metadata,autoload_with=self.engine)

    def create_alchemy_table(self,table_name,resource_id,resource_type,org_id,version,user,history):
        
        #don't clear meta 
        #self.Base.metadata.clear()
        orig_table=table_name
        columns={'id':Column('id',Integer, key=orig_table+'_id', primary_key=True)}
        columns.update(self.alchemy_add_audit_columns())
        #if (resource_type.lower()=='file'):
              #columns.update(self.alchemy_add_file_fields())   
        if history:
            table_name = table_name+'h'
            main_table=Table(orig_table, self.Base.metadata, autoload_with=self.engine) 
            #pdb.set_trace()
            columns.update({orig_table+'Id': Column(Integer,ForeignKey(main_table.c[orig_table+'_id']),nullable=False)}) # *** KeyError: 'resource246_1_id *** (orig_table='resource246_1h', main_table= Table('resource246_1h')

        attrs = self.define_alchemy_table_attr(table_name,columns)   
        dtc = self.get_alchemy_table_class(attrs)
        self.Base.metadata.create_all(self.engine)
        
        #pdb.set_trace() #create_alchemy_table
        self.update_alchemy_table(resource_id, user, org_id, orig_table, version, history)

        #again reflect all changes    
        self.Base.metadata.reflect(self.engine)
            
    def update_alchemy_table(self,resource_id,user,org_id,table_name,version,history):

        def rmd_column_h(colname):  return rmd_table_history.c['katalyonode_resourcemodeldefinitionhistory_'+colname]       
        def rmd_column(colname):  return rmd_table.c['katalyonode_resourcemodeldefinition_'+colname]
                 
        #create table with org_id to separate data among organisations
        orig_table = table_name
        l_table_name = table_name
        if history:
          l_table_name = l_table_name+'h'
        
        columns = {}
        session = self.Session()
        with session.begin():

            rmd_table = Table('katalyonode_resourcemodeldefinition', self.Base.metadata, autoload_with=self.engine)
            rmd_table_history = Table('katalyonode_resourcemodeldefinitionhistory', self.Base.metadata, autoload_with=self.engine)
         
            last_id = (
                session.query(rmd_column_h('id'))
                .filter(rmd_column_h('ResourceModelDefinitionId_id') == rmd_column('id'))
                .order_by(rmd_column_h('id').desc())
                .limit(1)
                .correlate(rmd_table)
                .as_scalar()
            )
            if not history:
                rmd = session.query(rmd_table,rmd_column_h('MaxLength'),rmd_column_h('Unique'))\
                .filter(  rmd_column('ResourceDefId_id')==resource_id,   rmd_column('DeployAction').in_(['A','C','D']))\
                .outerjoin(rmd_table_history,rmd_column('id') == last_id).options(undefer('*'))
            else:
                rmd = session.query(rmd_table,rmd_column_h('MaxLength'),rmd_column_h('Unique'))\
                .filter(rmd_column('ResourceDefId_id')==resource_id,rmd_column('DeployAction').in_(['AH','CH','DH']))\
                .outerjoin(rmd_table_history,  rmd_column('id') == last_id).options(undefer('*'))
        
            columns.update({'id': Column(Integer, primary_key=True, autoincrement=True)})

            conn = self.engine.connect()

            for field in rmd:
                rmd_update={}

                rmd_update.update({'katalyonode_resourcemodeldefinition_Deployed':True})
                if history:
                    rmd_update.update({'katalyonode_resourcemodeldefinition_DeployAction':'N'})
                else:
                    rmd_update.update({'katalyonode_resourcemodeldefinition_DeployAction':field.katalyonode_resourcemodeldefinition_DeployAction+'H'})
                    rmd_update.update({'katalyonode_resourcemodeldefinition_DeployedVersion':version})
                    rmd_update.update({'katalyonode_resourcemodeldefinition_DeployDate':timezone.now()})
                    rmd_update.update({'katalyonode_resourcemodeldefinition_DeployedBy_id':user.id})
            
                if field.katalyonode_resourcemodeldefinition_Dbtype != 'id':
                    #za sve osim ID kolone
                    last_field_name = session.query(rmd_table_history.c['FieldName'])\
                    .filter(rmd_column_h('ResourceModelDefinitionId_id') == rmd_column('id'),rmd_column_h('ResourceModelDefinitionId_id')==field.katalyonode_resourcemodeldefinition_id)\
                    .order_by(rmd_column_h('id').desc()).limit(1)
                    #.filter(rmd_column_h('ResourceModelDefinitionId_id')==field.id)
                

                    if field.katalyonode_resourcemodeldefinition_DeployAction in ['C','CH']:
                        if (last_field_name[0][0] == field.katalyonode_resourcemodeldefinition_FieldName):
                            fieldr=self.alchemy_change_column(field,resource_id,user,org_id, history, last_field_name[0][0])
                        else:
                            fieldr=self.alchemy_add_column(field,resource_id,user,org_id, history)
                        
                    if field.katalyonode_resourcemodeldefinition_DeployAction in['A','AH']:
                        fieldr = self.alchemy_add_column(field,resource_id,user, org_id, history)
                        if fieldr is not None: columns.update(fieldr)

                    if field.katalyonode_resourcemodeldefinition_DeployAction in ['D','DH'] and field.katalyonode_resourcemodeldefinition_Deployed:
                        if field.katalyonode_resourcemodeldefinition_Dbtype!='manytomanyfielddefinition' and field.katalyonode_resourcemodeldefinition_Dbtype[0:8]!='existing':
                            deleteColumn = self.alchemy_drop_column(l_table_name,field.katalyonode_resourcemodeldefinition_FieldName)
                    
                        rmd_update.update({'katalyonode_resourcemodeldefinition_Deployed':False})
                 
                    fld_upd = rmd_table.update().where(rmd_column('id')==field.katalyonode_resourcemodeldefinition_id).values(**rmd_update).returning(literal_column('*'))
                    
                    result = conn.execute(fld_upd)
            
                    #conn.close()

        return columns    
        
    def alchemy_change_column(self,field,resource_id,user,org_id,history,last_field_name):
        schema='public'    
        constraints=[]
        sql_commands=[]
        nullable=True# TODO
        unique=False#TODO
        
        nullStr={True:'',False:'NOT NULL'}
        table_name,is_external=self.get_table_name(resource_id,org_id)
        if history: 
            table_name=table_name+'h'
        column = None

        field_name = field.katalyonode_resourcemodeldefinition_FieldName
        field_type = field.katalyonode_resourcemodeldefinition_FieldType
        unique_val = field.katalyonode_resourcemodeldefinition_Unique
        max_length_val = field.katalyonode_resourcemodeldefinition_MaxLength
        db_type = field.katalyonode_resourcemodeldefinition_Dbtype
        
        #pdb.set_trace()  #alchemy_change_column
        main_table= Table(table_name, self.Base.metadata, autoload_with=self.engine)

        column = main_table.c.get(field_name,None)
        #pdb.set_trace()
        if column is None:
            return column

        if (last_field_name != field_name): # kad se ne mijenja naziv polja
            sql_commands.append('ALTER TABLE {schema}.{table_name}  RENAME COLUMN "{last_field_name}" TO "{field_name}"'.format(schema=schema, field_name=field_name, table_name=table_name, last_field_name=last_field_name))
            column = {field_name:Column(field_name,String(max_length_val),unique=unique_val,nullable=True)}

           
        if db_type == 'charfielddefinition':
                    
            if (max_length_val>0):
                sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE VARCHAR({max_length_val})')
            else:
                sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" Text')
                
            column = {field_name:Column(field_name,String(max_length_val),unique=unique_val,nullable=True)}
        
        elif db_type == 'textfielddefinition':
            
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE Text')
            column = {field_name:Column(field_name,Text,unique=unique_val,nullable=True)}
        
        elif db_type == 'integerfielddefinition':

            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE Integer')
            column = {field_name:Column(field_name,Integer,unique=unique_val,nullable=True)}
        
        elif db_type == 'foreignkeydefinition':
            
            column_prefix = "katalyonode_"
            
            if field_type =='user' or field_type =='group':
                column_prefix = "auth_"
            
            if field_type[0:8] == 'resource':
                column_prefix = ""
            
            rtr= Table(column_prefix+field_type, self.Base.metadata, autoload_with=self.engine) 
            
            column_ref = column_prefix+field_type+'_id'
            column = {field_name:Column(field_name,Integer, ForeignKey(rtr.c[column_ref]),nullable=True,unique=unique_val)}
            
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE Integer')
            constraints.append('ALTER TABLE {schema}.{table_name}  DROP CONSTRAINT IF EXISTS {schema}_{table_name}_{field_name}_fk CASCADE')
            
            
            if field_type == 'codesdetail':
                
                constraints.append('ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_fk FOREIGN KEY ("{field_name}")'  +
                ' REFERENCES {schema}.katalyonode_codesdetail (id)'.format(schema=schema)+
                ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')        
            elif field_type == 'user':
                
                constraints.append('ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_fk FOREIGN KEY ("{field_name}")'  +
                ' REFERENCES {schema}.auth_user (id)'.format(schema=schema)+
                ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')
            else:
                constraints.append('ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_fk FOREIGN KEY ("{field_name}")'  +
                ' REFERENCES {schema}.{fkResourceId} (id)'.format(schema=schema,fkResourceId=field_type, fkOrgId=org_id)+
                ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')
                
            #za listu TJ. referenca na codes-detail
            #ALTER TABLE public.resource51_1   ADD CONSTRAINT "resource51_1_Field83_5aed97aa_fk_katalyonode_codesdetail_id" FOREIGN KEY ("Field83")
            #REFERENCES public.katalyonode_codesdetail (id)     #MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;

             
        elif db_type == 'manytomanyfielddefinition':
            self.alchemy_create_m2m(resource_id,field.katalyonode_resourcemodeldefinition_Related_id,org_id,user)
        elif db_type == 'decimalfielddefinition':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE Numeric(1000,{max_length_val})')
            #column = {field_name:Column(field_name,Numeric,precision=max_length_val,unique=unique_val,nullable=True)}
        elif db_type == 'booleanfielddefinition':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE BOOLEAN ')
            #column = {field_name:Column(field_name,Boolean,unique=unique_val,nullable=True)}
        elif db_type == 'Float':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE Float({max_length_val})')
            #column = {field_name:Column(field_name,Float,precision=max_length_val,unique=unique_val,nullable=True)}
        elif db_type =='datetimefielddefinition':
            #column = {field_name:Column(field_name,DateTime,nullable=True,unique=unique_val)}
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" datetime' )
        elif db_type =='datefielddefinition':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" date' )
            #column = {field_name:Column(field_name,Date,nullable=True,unique=unique_val)}
            
        elif field.Dbtype =='date':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE timestamp with time zone' )
            #column = {field_name:Column(field_name,Date,nullable=True,unique=unique_val)}
        elif db_type =='time':     
            sql_commands.append('ALTER TABLE {schema}.{table_name} ALTER column "{field_name}" TYPE timestamp with time zone' )
            #column = {field_name:Column(field_name,Time,nullable=True,unique=unique_val)}
                 
        if unique_val == True:
            constraints.append('ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_unique UNIQUE ("{field_name}")')
        else:
            constraints.append('ALTER TABLE {schema}.{table_name} DROP CONSTRAINT IF EXISTS {schema}_{table_name}_{field_name}_unique')
        
        if nullable==False:    #NOT WORKING: How to add non-null fields if no default defined
            constraints.append('ALTER TABLE {schema}.{table_name} ALTER COLUMN "{field_name}" SET NOT NULL') 
            #constraints.append('ALTER TABLE {schema}.{table_name} ALTER COLUMN {field_name} DROP DEFAULT')
        else:
            constraints.append('ALTER TABLE {schema}.{table_name} ALTER COLUMN "{field_name}" DROP NOT NULL') 
            
        
        sql_command=';'.join(sql_commands).format(schema=schema,table_name=table_name, field_name=field_name, max_length_val=max_length_val)+";"
        constraints_command=';'.join(constraints).format(schema=schema,table_name=table_name, field_name=field_name, max_length_val=max_length_val)+";"
        if len(sql_command)>1: self.engine.execute(sql_command)
        if len(constraints_command)>1: self.engine.execute(constraints_command)
       
        tbl = sa.Table(table_name, self.Base.metadata, extend_existing=False, autoload_with=self.engine) #load to Meta

        return column

    def alchemy_drop_column(self,table_name, column_name):

     
          base_command = ('ALTER TABLE public.{table_name} DROP column IF EXISTS "{column_name}" ')
          
          sql_command = base_command.format(table_name=table_name, column_name=column_name)
          self.engine.execute(sql_command)
          #remove from META
          #my_table=self.Base.metadata.tables[table_name]
          # get the column instance from table object
          #my_col_instance = [col for col in my_table._columns if col.name == column_name][0]
          # remove from the ColumnCollection, note that we're removing it from ._columns not .columns
          #my_table._columns.remove(my_col_instance) 
          
          #tbl = sa.Table(table_name, meta, extend_existing=False,  autoload_with=self.engine) #load to Meta ALTERNATIVNO
      
    def alchemy_add_column (self,field,resource_id,user, org_id,history):
           
        #field: redak iz ResourceModelDefinition 
        schema='public'    
        constraints=[]
        sql_commands=[]
        nullable=True# TODO
        unique=False#TODO
        
        nullStr={True:'',False:'NOT NULL'}
        table_name,is_external=self.get_table_name(resource_id,org_id)
        if history: 
            table_name=table_name+'h'
        column = None
        
        field_name = field.katalyonode_resourcemodeldefinition_FieldName
        field_type = field.katalyonode_resourcemodeldefinition_FieldType
        unique_val = field.katalyonode_resourcemodeldefinition_Unique
        max_length_val = field.katalyonode_resourcemodeldefinition_MaxLength
        db_type = field.katalyonode_resourcemodeldefinition_Dbtype
           
        if db_type == 'charfielddefinition':
        
            if (max_length_val>0):
                sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" VARCHAR({max_length_val})')
            else:
                sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Text')
                
            column = {field_name:Column(field_name,String(max_length_val),unique=unique_val,nullable=True)}
        
        elif db_type == 'textfielddefinition':
            
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Text')
            column = {field_name:Column(field_name,Text,unique=unique_val,nullable=True)}
        
        elif db_type == 'integerfielddefinition':

            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Integer')
            column = {field_name:Column(field_name,Integer,unique=unique_val,nullable=True)}
        
        elif db_type == 'foreignkeydefinition':
            
            #rtr= Table("katalyonode_"+field_type, self.Base.metadata, autoload_with=self.engine) 
            #orig_table
            #column_prefix = "katalyonode_"
            #if field_type =='user':
            #    column_prefix = "auth_"
            #column_ref = column_prefix+field_type+'_id'
            #column = {field_name:Column(field_name,Integer, ForeignKey(rtr.c[column_ref]),nullable=True,unique=unique_val)}
            
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Integer')
            constraints.append('ALTER TABLE {schema}.{table_name} DROP CONSTRAINT IF EXISTS {schema}_{table_name}_{field_name}_fk CASCADE')     
            
            if field_type == 'codesdetail':
               

                constraints.append(
                'ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_fk FOREIGN KEY ("{field_name}")'  +
                ' REFERENCES {schema}.katalyonode_codesdetail (id)'.format(schema=schema)+
                ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')        
            elif field_type == 'user':

                constraints.append(
                'ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_fk FOREIGN KEY ("{field_name}")'  +
                ' REFERENCES {schema}.auth_user (id)'.format(schema=schema)+
                ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')  
            elif field_type == 'group':

                constraints.append(
                'ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{field_name}_fk FOREIGN KEY ("{field_name}")'  +
                ' REFERENCES {schema}.auth_group (id)'.format(schema=schema)+
                ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')  
            elif field_type[0:8] == 'resource': # example: field_type = resource17_1
                
                constraint_str = 'ALTER TABLE {schema}.{table_name} ADD CONSTRAINT {schema}_{table_name}_{fkResourceId}_{field_name}_fk FOREIGN KEY ("{field_name}") REFERENCES {schema}.{fkResourceId} (id)'.format(schema=schema,table_name=table_name,field_name=field_name,fkResourceId=field_type, fkOrgId=org_id)
                constraint_str += ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED'
                constraints.append(constraint_str)
            else:
                raise Exception("FieldType not found! ")
              
            #za listu TJ. referenca na codes-detail
            #ALTER TABLE public.resource51_1   ADD CONSTRAINT "resource51_1_Field83_5aed97aa_fk_katalyonode_codesdetail_id" FOREIGN KEY ("Field83")
            #REFERENCES public.katalyonode_codesdetail (id)     #MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED;
             
        elif db_type == 'manytomanyfielddefinition':
            #create m2m table
            m2mTblName=self.alchemy_create_m2m(resource_id,field.katalyonode_resourcemodeldefinition_Related_id,org_id,user)
            #sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Integer') 
            #constraints.append(
               # 'ALTER TABLE {schema}.{m2mTblName} ADD FOREIGN KEY ("ModelDefinitionId") REFERENCES {schema}.katalyonode_resourcemodeldefinition (id)'.format(m2mTblName=m2mTblName,schema=schema)+
               # ' MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED')
                       
        elif db_type == 'decimalfielddefinition':
            #pdb.set_trace()
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Numeric(1000,{max_length_val})')
            #column = {field_name:Column(field_name,Numeric,unique=unique_val,nullable=True)}
        elif db_type == 'booleanfielddefinition':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" BOOLEAN ')
            #column = {field_name:Column(field_name,Boolean,unique=unique_val,nullable=True)}
        elif db_type == 'Float':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" Float({max_length_val})')
            #column = {field_name:Column(field_name,Float,precision=max_length_val,unique=unique_val,nullable=True)}
        elif db_type =='datetimefielddefinition':
            column = {field_name:Column(field_name,DateTime,nullable=True,unique=unique_val)}
            if field_type=='date':
                sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" timestamp with time zone' )
                #column = {field_name:Column(field_name,Date,nullable=True,unique=unique_val)}
            if field_type =='time':     
                sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" timestamp with time zone' )
                #column = {field_name:Column(field_name,Time,nullable=True,unique=unique_val)}
        elif db_type =='datefielddefinition':
            sql_commands.append('ALTER TABLE {schema}.{table_name} ADD COLUMN IF NOT EXISTS "{field_name}" date' )
            #column = {field_name:Column(field_name,Date,nullable=True,unique=unique_val)}
            
        if unique_val == True:  #NOT WORKING: Difficult to add unique column afterwards.. TBD
            constraints.append('create unique index {table_name}_{field_name}_unique_idx on {schema}.{table_name} ({field_name})')
        
        if nullable==False:    #NOT WORKING: How to add non-null fields if no default defined
            constraints.append('ALTER TABLE {schema}.{table_name} ALTER COLUMN "{field_name}" SET NOT NULL') 
            #constraints.append('ALTER TABLE {schema}.{table_name} ALTER COLUMN {field_name} DROP DEFAULT')
        
        
        sql_command=';'.join(sql_commands).format(schema=schema,table_name=table_name, field_name=field_name, max_length_val=max_length_val)+";"
        constraints_command=';'.join(constraints).format(schema=schema,table_name=table_name, field_name=field_name, max_length_val=max_length_val)+";"
        if len(sql_command)>1: self.engine.execute(sql_command)
        if len(constraints_command)>1: self.engine.execute(constraints_command)

        return column

    def alchemy_add_file_fields (self):
        
        file_columns = {}
        file_columns.update({'Name':Column('Name',String(2000),unique=False,nullable=True)})
        file_columns.update({'FileType':Column('FileType',String(255),unique=False,nullable=True)})
        file_columns.update({'Path':Column('Path',String(4000),unique=False,nullable=True)})
        file_columns.update({'NodeId':Column('NodeId',String(1000),unique=False,nullable=True)})
        file_columns.update({'Sha512':Column('Sha512',String(2000),unique=False,nullable=True)})
        file_columns.update({'Size':Column('Size',Integer,unique=False,nullable=True)})
        file_columns.update({'StorageType':Column('StorageType',Integer,unique=False,nullable=True)})   
        return  file_columns

    def alchemy_create_m2m (self,resource_id,related_id,org_id,user):
        
        
        

        m2m_columns = {'id': Column('id' , Integer() , primary_key=True)}
        
        table_name,is_external=self.get_table_name_related(resource_id,related_id,org_id)
        
        table_exists = inspect(self.engine).has_table(table_name)
        if not table_exists:
            auth_user= Table('auth_user', self.Base.metadata, autoload_with=self.engine)

            #pdb.set_trace()
            if int(resource_id)<int(related_id):
                resource1_table_name,is_external1=self.get_table_name(resource_id,org_id) 
                resource2_table_name,is_external2=self.get_table_name(related_id,org_id)
            else:
                resource2_table_name,is_external1=self.get_table_name(resource_id,org_id) 
                resource1_table_name,is_external2=self.get_table_name(related_id,org_id)

            resource1_table = Table(resource1_table_name,self.Base.metadata, autoload_with=self.engine)
            resource2_table=Table(resource2_table_name,self.Base.metadata, autoload_with=self.engine)
            #m2m_columns.update({'ModelDefinitionId':Column('ModelDefinitionId',Integer,unique=False,nullable=True)})
            #m2m_columns.update({'ResourceSourceDefId':Column('ResourceSourceDefId',Integer,unique=False,nullable=True)})
            m2m_columns.update({'Resource1':Column('Resource1',Integer,ForeignKey(resource1_table.c[resource1_table_name+'_id']),unique=False,nullable=True)})
            m2m_columns.update({'Resource2':Column('Resource2',Integer,ForeignKey(resource2_table.c[resource2_table_name+'_id']),unique=False,nullable=True)})
            m2m_columns.update({'CreatedDateTime':Column('CreatedDateTime',DateTime,unique=False,nullable=True)})
            m2m_columns.update({'ChangedDateTime':Column('ChangedDateTime',DateTime,unique=False,nullable=True)})
            m2m_columns.update({'CreatedBy':Column('CreatedBy',Integer,ForeignKey(auth_user.c['auth_user_id']),unique=False,nullable=True)})
            m2m_columns.update({'ChangedBy':Column('ChangedBy',Integer,ForeignKey(auth_user.c['auth_user_id']),unique=False,nullable=True)})  
            attrs = self.define_alchemy_table_attr(table_name,m2m_columns)
           
            dtc = self.get_alchemy_table_class(attrs)
            self.Base.metadata.create_all(self.engine)
        return table_name

    def alchemy_add_audit_columns (self):
        
        audit_columns = {}
        #pdb.set_trace() # alchemy_add_audit_columns
        auth_user=Table('auth_user', self.Base.metadata, autoload_with=self.engine)
        
        
        audit_columns.update({'CreatedDateTime':Column('CreatedDateTime',DateTime,unique=False,nullable=True)})
        audit_columns.update({'ChangedDateTime':Column('ChangedDateTime',DateTime,unique=False,nullable=True)})
        audit_columns.update({'CreatedBy':Column('CreatedBy',Integer,ForeignKey(auth_user.c['auth_user_id']),unique=False,nullable=True)})
        audit_columns.update({'ChangedBy':Column('ChangedBy',Integer,ForeignKey(auth_user.c['auth_user_id']),unique=False,nullable=True)})
         
        return audit_columns

    def alchemy_add_blockchain_columns (self):
        
        block_columns = {}
        audit_columns.update({'TrxId':Column('TrxId',String(1024),unique=False,nullable=True)})
        audit_columns.update({'BlockId':Column('BlockId',String(64),unique=False,nullable=True)})
        #audit_columns.update({'NotificationStatus':Column('NotificationStatus',String(16),unique=False,nullable=True)})
        audit_columns.update({'BChainRecId':Column('BChainRecId',Integer,unique=False,nullable=True)})
         
        return audit_columns

    def inspect_if_table_exists(self,table_name):

        return inspect(self.engine).has_table(table_name)

    def get_model_data(self,resourceId):
        
        #resourceModelDef = ResourceModelDefinition.objects.filter(ResourceDefId=resourceId,Status='Active')
        session = self.Session()
        fields = []
        with session.begin():
            resourceModelDef = session.query(ResourceModelDefinitionSa).filter(ResourceModelDefinitionSa.ResourceDefId==resourceId,ResourceModelDefinitionSa.Status=='Active').all()

            fields = [(f.FieldName, f.id) for f in resourceModelDef]
        return fields
        
    def get_resource_data(self,resourceId,lang):
        
        #resourceDef = ResourceDef.objects.get(id=resourceId)
        resourceDefs = self.LocalSession.query(ResourceDefSa).filter(ResourceDefSa.id==resourceId).all()
        resourceDef = None
        for rd in resourceDefs:
            resourceDef = rd
        return resourceDef

    def build_joins_users_codes(self,tbl_related,tbl_related_name,field_name=''):

        if hasattr(tbl_related,'metadata'):
            columns_related = tbl_related.metadata.tables[tbl_related_name].c.keys()
        else:
            columns_related = tbl_related.c.keys()
        joins = []
        tables = []
        col_for_query = []          
        for col in columns_related:
            if hasattr(tbl_related,'metadata'):
                col_obj = tbl_related.metadata.tables[tbl_related_name].c[col]
            else:
                col_obj = tbl_related.c[col]

            if len(col_obj.foreign_keys)>0:
                tbl_fk_set = set()
                for fk in col_obj.foreign_keys:
                    tbl_fk_name = str(fk._colspec).split('.')[0]

                    if tbl_fk_name not in tbl_fk_set:
                        
                        tbl_fk_set.add(tbl_fk_name)
                        
                        if tbl_fk_name=='auth_user':
                            tbl_fk=auth_user
                            tbl_fk_alias = aliased(tbl_fk,name=field_name+col)
                            col_for_query.append(label(col+'.name',tbl_fk_alias.first_name+' '+tbl_fk_alias.last_name+' ('+tbl_fk_alias.username+')'))
                            
                            joins.append((tbl_fk_alias,tbl_related.c[col]==tbl_fk_alias.id))

                        elif tbl_fk_name=='katalyonode_codesdetail':
                            
                            tbl_fk=codesdetail
                            tbl_fk_alias = aliased(tbl_fk,name=field_name+col)
                            col_for_query.append(label(col+'.name',tbl_fk_alias.Value))
                            joins.append((tbl_fk_alias,and_(tbl_related.c[col]==tbl_fk_alias.id,tbl_fk_alias.Lang==self.Language.id)))
                    
                        tables.append(tbl_fk_alias)

        return col_for_query,joins,tables

    def build_query(self,session,table,table_name,columns_visible,filter, parameters,injoins=[],order_by=None,with_columns=True):
        
        if columns_visible is None: 
            columns_visible = []
        if parameters is None:
            parameters={}
            
        newparams={}       
        if parameters is not None:

            for key, value in parameters.items():
                if key is not None:     
                    newparams[table_name + "_" +  key] = value
            
            parameters=newparams
        else:
            parameters = {}
        #print("build query filter")
        #print(filter)
        orderArray = []
        col_for_query = []
        joins=[]
        tables=[table]
        joined_fk_tables=[]
        if len(columns_visible)==0:
            columns = table.metadata.tables[table_name].c.keys()
        else:
            columns = columns_visible
        # build_query
        filter_extended=[]
        for col in columns:
            
            if type(col) == elements.Label:
                if hasattr(col,'element'):
                    if col.name.endswith('_id'):
                        col_obj = table.metadata.tables[table_name].c.get(col.element.key,None)
                        col_name = col.element.name
                    else:
                        if hasattr(col.element,'name'):
                            col_obj = table.metadata.tables[table_name].c.get(col.element.name,None)
                            col_name = col.element.name
                        else:
                            col_obj=col
                            col_name=col.name
                    if col_obj is None:
                        col_object = col
                    
                    col_for_query.append(col)
                else:
                    
                    col_obj = col
                    col_name = col.name
                    col_for_query.append(col)
                    #raise Exception('Invalid column definition in build_query fn '+str(col))

            else:            
                col_obj = table.metadata.tables[table_name].c[col]
                col_name = col
                col_for_query.append(label(col_name,col_obj))
            
           

            if hasattr(col_obj,'foreign_keys') and len(col_obj.foreign_keys)>0:# migor TODO - da sprijecimo ogromni join prema foreign tablici jer nema filter (treba doraditi da gleda da li postoji filter za tu tablicu)
                #pdb.set_trace() #build_query
                tbl_fk_set = set()
                for fk in col_obj.foreign_keys:
                    tbl_fk_name = fk._colspec.split('.')[0]
                    if tbl_fk_name not in tbl_fk_set:

                        tbl_fk_set.add(tbl_fk_name)
                       

                        if tbl_fk_name=='auth_user':


                            tbl_fk=auth_user
                            tbl_fk_alias = aliased(tbl_fk,name=col_name) 
                            
                            col_for_query.append(label(col_name+'.name',tbl_fk_alias.first_name+' '+tbl_fk_alias.last_name+' ('+tbl_fk_alias.username+')'))
                            joins.append((tbl_fk_alias,table.c[col_name]==tbl_fk_alias.id))
                            


                        elif tbl_fk_name=='katalyonode_codesdetail':
                            tbl_fk=codesdetail
                            tbl_fk_alias = aliased(tbl_fk,name=col_name)
                            col_for_query.append(label(col_name+'.name',tbl_fk_alias.Value))

                            if hasattr(self.Language,'id'):
                                joins.append((tbl_fk_alias,and_(table.c[col_name]==tbl_fk_alias.id,tbl_fk_alias.Lang==self.Language.id)))
                            else:
                                raise Exception('Language is '+str(type(self.Language))+' in self.Language '+str(self.Language)+' Orga id = '+str(self.OrganisationId))
                        else:

                            tbl_fk = Table(tbl_fk_name, self.Base.metadata, autoload_with=self.engine,extend_existing=False)
                            tbl_fk_alias=aliased(tbl_fk,name=col_name)
                            joins.append((tbl_fk_alias,table.c[col_name]==tbl_fk_alias.c[tbl_fk_name+"_id"]))
                        
                        tables.append(tbl_fk_alias)

            # How do we sort
            if col_name in parameters:
                if 'GridState' in parameters[col_name]:
                    if 'sort' in parameters[col_name]['GridState']:
                        sortOrder=None
                        orderBy=None
                        sortType= parameters[col_name]['GridState']['sort']
                        
                        if sortType == "ascending":
                            orderBy = asc(col_name)
                        if sortType == "descending":
                            orderBy = desc(col_name)
                        if 'sortOrder' in parameters[col_name]['GridState']:
                            sortOrder = parameters[col_name]['GridState']['sortOrder']
                        if sortType in ["ascending","descending"]:
                            orderArray.append({'orderBy':orderBy, 'sortOrder': sortOrder})
   
        inner_joins=[]
        if injoins is not None:
            
            for join_item in injoins:
                if type(join_item)==tuple:
                    inner_joins.append(join_item)
                elif type(join_item)==dict:
                    max_length = join_item.get('MaxLength',-1)
                    if max_length==0:
                        #lets do a subquery
                        join_result,m2m_qry_alias,tables_related,cfq = self.process_join_m2m(join_item,table_name,table,session)
                        #pdb.set_trace()
                        col_for_query.extend(cfq)
                        tables=cfq
                        inner_joins.append(join_result)  

                    else:
                        
                        join_result,joins_related,tables_related_join,cfq,filter_related = self.process_join_fk(join_item,table)
                        
                        if filter_related is not None:
                            filter_extended.append(filter_related)

                        #pdb.set_trace()
                        col_for_query.extend(cfq)
                        inner_joins.append(join_result)
                        joins.extend(joins_related)
                        #tables.extend(tables_related_join)
                        tables = cfq
                        #pdb.set_trace()
                    

        qry=session.query(*tables)

        orderArray.sort(key=lambda x: x['sortOrder'], reverse=False)
        
        if len(col_for_query)>0 and with_columns:
            qry=qry.with_entities(*col_for_query)

        for join_items in inner_joins:
            qry=qry.join(*join_items)

        for join_args in joins:
            qry=qry.outerjoin(*join_args)

        if order_by is not None:
            qry=qry.order_by(order_by)

        #pdb.set_trace()
        #process_filter here
        qry=self.process_filter(filter,table_name,table,columns,qry)
        #process_extended filter here
        for f in filter_extended:
            qry=self.process_filter(f['filter_data'],f['table_name'],f['table'],f['columns'],qry)
        if len(orderArray) > 0:
            for item in orderArray:
                #print(item)
                qry = qry.order_by(item['orderBy'])
        else:
            qry = qry.order_by(table.c[table_name+"_id"])
     
        return qry

    def process_join_m2m(self,join_item,table_name,table,session):

        
        col_for_query=[]
        tables=[]
        inner_joins=[]
        #lets do a subquery
        tbl_m2m_name,is_external_related = self.get_table_name_related(join_item['ResourceDefId'],join_item['Related'],self.OrganisationId)
        tbl_m2m = Table(tbl_m2m_name, self.Base.metadata, autoload_with=self.engine,extend_existing=False)
        
        tbl_related_name,is_external_related = self.get_table_name(join_item['Related'],self.OrganisationId)
        tbl_related = Table(tbl_related_name, self.Base.metadata, autoload_with=self.engine,extend_existing=False)
        
        tbl_related_alias = aliased(tbl_related,name=join_item['FieldName']+tbl_related_name)  
        if int(join_item['ResourceDefId'])<int(join_item['Related']):
            join = (tbl_m2m,tbl_m2m.c.Resource2==tbl_related_alias.c[tbl_related_name+'_id'])
            resource_column='Resource1'
        else:
            join = (tbl_m2m,tbl_m2m.c.Resource1==tbl_related_alias.c[tbl_related_name+'_id'])
            resource_column='Resource2'




      
        columns_related,joins_related,tables_related = self.build_joins_users_codes(tbl_related_alias,tbl_related_name,join_item['FieldName'])
        tables_subquery = [tbl_related_alias]
 
        tables_subquery.extend(columns_related)

        m2m_qry = session.query(*tables_subquery,tbl_m2m.c.Resource1,tbl_m2m.c.Resource2).select_from(tbl_related_alias).join(join)
        for jl in joins_related:
            #pdb.set_trace()
            m2m_qry=m2m_qry.outerjoin(*jl)
        f_data = join_item.get('filter_data',None)

        r_cols = join_item.get('Columns',[])
        
        r_columns = tbl_related.metadata.tables[tbl_related_name].c.keys()     

        if f_data is not None:
            
            m2m_qry=self.process_filter(f_data,tbl_related_name,tbl_related_alias,r_columns,m2m_qry)


        #m2m_qry = m2m_qry.with_entities(*col_for_query)
        m2m_qry = m2m_qry.subquery()

        m2m_qry_alias=aliased(m2m_qry,name=join_item['FieldName'])
                          
        #pdb.set_trace()
        for rcol in r_cols:
            r_col_obj = m2m_qry_alias.c[rcol]
            col_for_query.append(label(rcol,r_col_obj))      
        #pdb.set_trace()
        
        if len(r_cols)==0:
            #col_for_query.extend(columns_related)
            for rcol in r_columns:
                r_col_obj = m2m_qry_alias.c[rcol]
                #col_for_query.append(label(join_item['FieldName']+'_'+rcol,r_col_obj)) #this is the solution for joining same resource multiple times
                col_for_query.append(label(rcol,r_col_obj))
            for rcol in columns_related:
                if type(rcol)==elements.Label:
                    r_col_obj = m2m_qry_alias.c[rcol.name]
                    #col_for_query.append(label(join_item['FieldName']+'_'+rcol,r_col_obj)) #this is the solution for joining same resource multiple times
                    col_for_query.append(label(rcol.name,r_col_obj))
        
        #r_columns_alias = m2m_qry_alias.c.keys()
           
        
        join_result = (m2m_qry_alias,m2m_qry_alias.c[resource_column]==table.c[table_name+'_id'])
        
        tables.append(m2m_qry_alias)
        tables.append(tbl_related)
        inner_joins.append(join_result)

        return join_result,m2m_qry_alias,tables,col_for_query

    def process_join_fk (self,join_item,table):

        col_for_query=[]
        joins=[]
        filter_extended = None
        join_result,tbl_related,tbl_related_name = self.build_join_fk(table,join_item['ResourceDefId'],join_item['Related'],join_item['FieldName'])

        columns_related,joins_related,tables_related = self.build_joins_users_codes(tbl_related,tbl_related_name)
       
        r_cols = join_item.get('Columns',[])
        
        for rcol in r_cols:
            col_name = tbl_related.metadata.tables[tbl_related_name].c.get(rcol,None)
            if col_name is not None:
                r_col_obj = tbl_related.metadata.tables[tbl_related_name].c[rcol]
                col_for_query.append(label(rcol,r_col_obj))
            else:
                for colr in columns_related:
                    if type(colr)==elements.Label:
                        if hasattr(colr,'name'):
                            if rcol==colr.name:
                                col_for_query.append(colr)

        if len(r_cols)==0:
            col_for_query.extend(columns_related)
            r_columns = tbl_related.metadata.tables[tbl_related_name].c.keys()
            for rcol in r_columns:
                r_col_obj = tbl_related.metadata.tables[tbl_related_name].c[rcol]
                col_for_query.append(label(rcol,r_col_obj))

        #col_for_query.extend(columns_related)
        tables_related_join = [tbl_related]
        tables_related_join.extend(tables_related)

        f_data = join_item.get('filter_data',None)        

        if f_data is not None:
            filter_extended = {'table_name':tbl_related_name,'table':tbl_related,'filter_data':f_data,'columns':r_columns}

        return join_result,joins_related,tables_related_join,col_for_query,filter_extended

    def process_filter (self,filter_data,table_name,table,columns,query):

        if type(filter_data)==dict:
            search_filter = filter_data.items()  
         
            for key,value in search_filter:
                if key.endswith("__in"):
                    field_name = key.split("__")[0]
                    
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col

                        if value is not None and type(value) is list:
                            if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name].in_(value))   
                
                elif key.endswith("__notin"):
                    field_name = key.split("__")[0]
                    
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col

                        if value is not None and type(value) is list:
                            if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name].notin_(value))
                                
                elif key.endswith("__contains"):
                    field_name = key.split("__")[0]

                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col

                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name].like('%'+str(value)+'%'))
                
                elif key.endswith("__icontains"):
                    field_name = key.split("__")[0]

                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col

                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name].ilike('%'+str(value)+'%'))
                elif key.endswith("__istartswith"):
                    field_name = key.split("__")[0]
                    for col in columns:
                        if type(col)==elements.Label:
                            if hasattr(col,'element'):
                                col_name = col.element.name
                            else:
                                raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col
                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name].ilike(str(value)+'%'))
                elif key.endswith("__iendswith"):
                    field_name = key.split("__")[0]
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col

                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name].ilike('%'+str(value)))
                elif key.endswith("__gte"):
                    field_name = key.split("__")[0]
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col
                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name]>=value)
                elif key.endswith("__lte"):
                    field_name = key.split("__")[0]
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col
                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name]<=value)
                elif key.endswith("__exact"):
                    field_name = key.split("__")[0]
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col
                        if (col_name==field_name or col_name==table_name+"_"+field_name):# and len(value)>0:
                                query = query.filter(table.c[col_name]==value)
                else:
                    
                    field_name = key
                    for col in columns:
                        if type(col)==elements.Label:
                            col_name = None
                            if hasattr(col,'element'):
                                if hasattr(col.element,'name'):
                                    col_name = col.element.name
                            if col_name == None:
                                if hasattr(col,'name'):
                                    col_name = col.name
                                else:
                                    raise Exception('Invalid column definition in process_filter fn '+str(col))
                        else:
                            col_name = col
                        
                        if col_name==field_name:                         
                            query = query.filter(table.c[col_name]==value)
        return query
    #SqlAlchemy done
    def get_data_sa (self,resource_id,org_id,cnfs):
        
        objects = None
          
        table_name,is_external = self.get_table_name(resource_id,org_id)
        
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        
        session = self.Session()
        session.begin()
        result = []
        try:
            objects = session.query(tbl)
            order_col = table_name+"_id"
            if order_col in tbl.c:
                objects = objects.order_by(tbl.c[order_col])
            
            columns = tbl.metadata.tables[table_name].c.keys()
            
            for item in objects.all():
                itemDict = dict((col, getattr(item, col)) for col in columns)
                itemDict['id'] = itemDict[order_col]
                if len(cnfs)>0:
                    name=""
                    for cnf in cnfs:
                        name = name+str(itemDict[cnf])+" "
                    itemDict['name'] = name.strip()
                result.append(itemDict)
        except Exception as exc:
            if session: 
                session.rollback()
                session.close()
            raise exc
        finally:
            if session:
                session.commit()
                session.close()

        return  result
        
    def get_data_related (self,resource_id,org_id):
          objects = None
          if model_def is not None:
            
            objects = model_def.model_class().objects.all().select_related().order_by('id')
            
            
          return  objects
     
    def get_data_filtered (self,resource_id,org_id,filtered_data, parameters):
      
        table_name,is_external = self.get_table_name(resource_id,org_id)
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        session = self.Session()
        objects = None
        session.begin()
        try:
            query = self.build_query(session,tbl,table_name,columns_visible=[],filter = filtered_data, parameters = parameters)
            objects = query.all()
        except Exception as exc:
            if session: 
                session.rollback()
                session.close()
            raise exc
        finally:
            if session:
                session.commit()
                session.close()
        return  objects

    def get_data_filtered_sr (self,resource_id,org_id,filtered_data,columns,sr, parameters,session=None):
        
        table_name,is_external = self.get_table_name(resource_id,org_id)
        self.SetOrganisation(org_id)
        #pdb.set_trace()
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        close_session = False
        if session is None:
            session = self.Session()
            session.begin()
            close_session = True
        objects = None
        try:
            query = self.build_query(session,tbl,table_name,columns,filtered_data, parameters,sr)
            objects = query.all()
        except Exception as exc:
            if session and close_session:
                session.rollback()
                session.close()
            raise exc
        finally:
            if session and close_session:
                session.commit()
                session.close()
        return  objects  
    
    def pre_build_query (self,resource_id,org_id,filtered_data,columns,literals,sr, parameters,session=None):
        
        table_name,is_external = self.get_table_name(resource_id,org_id)
        self.SetOrganisation(org_id)
        #pdb.set_trace()
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        close_session = False
        if session is None:
            session = self.Session()
            session.begin()
            close_session = True
        query = None
        try:
            lcols = []
            for col in columns:
                if col['field'] in tbl.c:
                    lcols.append(label(col['fieldId'],tbl.c[col['field']]))
                else:
                    lcols.append(label(col['fieldId'],literal(col['field'])))
            
            for lit in literals:    
                for name,val in lit.items():
                    lcols.append(literal(val).label(name))
            query = self.build_query(session,tbl,table_name,lcols,filtered_data, parameters,sr)
            #query = aliased(query,name=table_name)
        except Exception as exc:
            if session and close_session:
                session.rollback()
                session.close()
            raise exc
        finally:
            if session and close_session:
                session.commit()
                session.close()
        return  query  

    def execute_union (self,query,queries,union_type,session=None):
        
        union = None
        objects = []
        close_session = False
        if session is None:
            session = self.Session()
            session.begin()
            close_session = True
        objects = None
        try:
            #union in python
            """
            objects=query.all()

            for qitem in queries:
                objects.extend(qitem.all())
            
            """
            #union in alchemy
            if union_type==0:
                union = query.union_all(*queries)
            else:
                union = query.union(*queries)
            objects = union.all()
            
        except Exception as exc:
            if session and close_session:
                session.rollback()
                session.close()
            raise exc
        finally:
            if session and close_session:
                session.commit()
                session.close()
        return  objects  
        
    def get_data_filtered_in (self,resource_id,org_id,list_id):

        
        objects = None
          
        table_name,is_external = self.get_table_name(resource_id,org_id)
        
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        try:
            conn = self.engine.connect()
            rec = tbl.select().where(tbl.c[table_name+"_id"].in_(list_id)).order_by(tbl.c[table_name+"_id"])
        
            objects = conn.execute(rec).fetchall()
        finally:
            conn.close()   
        return  objects

    #SqlAlchemy done       
    def insert_data (self,data,resource_id,org_id,user_id):
          
       
        rec=None
        try:
            
            table_name,is_external= self.get_table_name(resource_id,org_id)
            
            if user_id is not None:
                data[table_name+'_CreatedBy'] = user_id
          
            data[table_name+'_CreatedDateTime'] = timezone.now()
            
                
            #insert record with SqlAlchemy
            tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
            #tbl = Table(table_name, self.Base.metadata, autoload=False)
            rec = tbl.insert().values(**data).returning(literal_column('*'))
            
            #sess = Session()
            try:
                conn = self.engine.connect()
                result = conn.execute(rec)
            finally:
                conn.close()
            
            return result.first()
        except IntegrityError as e:
            #ErrorLog.objects.create(Error1=str(e.__cause__),Error2={'data':data},CreatedDateTime=timezone.now())
            raise ValueError (str(e.__cause__))
        except Exception as exc:
            #ErrorLog.objects.create(Error1=str(exc.__cause__),Error2={'data':data},CreatedDateTime=timezone.now())
            raise exc
    #SqlAlchemy done  
    def update_data (self,data,resource_id,org_id,user_id):
          
        table_name,is_external=self.get_table_name(resource_id,org_id)
        if not is_external:
            data[table_name+'_ChangedDateTime'] = timezone.now()
            if user_id is not None:
                data[table_name+'_ChangedBy'] = user_id
        
        id_field = table_name+'_id'
        
        conn = self.engine.connect()
        rec_id = data.get(id_field,None)
        if rec_id is not None:
            del data[id_field]    
        else:
            id_field = 'id'
            rec_id = data.get(id_field,None)
            if rec_id is not None:
                del data[id_field] 
            else:  
                raise Exception ('Dataset record id is not provided using field '+str(id_field))
        
        data_for_update=data

        #for items in data:
            #data_for_update[table_name+'_'+items[0]]=items[1]
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        try:
            if is_external == False: 
                #insert into history when not External
                table_history = table_name+"h"

                rec_history = {}
                rec = tbl.select().where(tbl.c[id_field]==rec_id)
               
                select = conn.execute(rec).fetchone()
                
                rec_history = dict(select)

                rec_history['resource'+str(resource_id)+'_'+str(org_id)+'Id'] = select['id']
                del rec_history['id']
                rec_history_fix={}
                rec_history_del=[]
                for key,value in rec_history.items():
                    if not key.startswith(table_history) and not key.startswith('Field'):
                        rec_history_fix[table_history+"_"+key] = value
                        rec_history_del.append(key)
                for key in rec_history_del:
                    del rec_history[key]
                    
                rec_history.update(rec_history_fix)
                     
                tbl_hist = Table(table_history, self.Base.metadata, autoload_with=self.engine)
                
                #ToDo history not working - unconsumed columns (the problem looks like with definition of history table)
                hist = tbl_hist.insert().values(**rec_history)#.returning((tbl_hist.c.id).label('id'))
                result_history = conn.execute(hist)
            
            if id_field in tbl.c:  
                upd = tbl.update().where(tbl.c[id_field]==rec_id).values(**data_for_update).returning(literal_column('*'))
            elif table_name+'_'+id_field in tbl.c:
                upd = tbl.update().where(tbl.c[table_name+'_'+id_field]==rec_id).values(**data_for_update).returning(literal_column('*'))
            result = conn.execute(upd)
        finally:
            conn.close()

        return result,result.first()

    def update_data_no_history (self,data,resource_id,org_id,user_id):
          
        table_name,is_external=self.get_table_name(resource_id,org_id)
         
        if user_id is not None:
            data[table_name+'_ChangedBy'] = user_id.id
        data[table_name+'_ChangedDateTime'] = timezone.now()
           #insert into history
        
        #insert record with SqlAlchemy
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        try:
            conn = self.engine.connect()
            rec_id = data['id']
            del data['id']
            upd = tbl.update().where(tbl.c[table_name+"_id"]==rec_id).values(**data).returning(literal_column('*'))
           
            result = conn.execute(upd)
        finally:
            conn.close()
        return result,result.first()
        
    def delete_data (self,resource_def_id,resource_id,org_id,user_id):
          
        rec_del=0

        #insert into history
        table_name,is_external=self.get_table_name(resource_def_id,org_id)
         
        
        table_history = table_name+"h"
        #insert record with SqlAlchemy
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        try:
            conn = self.engine.connect()
            rec_history = {}
            rec = tbl.select().where(tbl.c[table_name+"_id"]==resource_id)
           
            select = conn.execute(rec).fetchone()
            
            rec_history = dict(select)
            rec_history['resource'+str(resource_def_id)+'_'+str(org_id)+'Id'] = select['id']
            del rec_history['id']
            rec_history_fix={}
            rec_history_del=[]
            for key,value in rec_history.items():
                if not key.startswith("Field") and not key.startswith(table_history):
                    rec_history_fix[table_history+"_"+key] = value
                    rec_history_del.append(key)
            for key in rec_history_del:
                del rec_history[key]
                
            rec_history.update(rec_history_fix)
            
            if user_id is not None:
                rec_history[table_history+'_ChangedBy'] = user_id.id
            rec_history[table_history+'_ChangedDateTime'] = timezone.now()
            
            tbl_hist = Table(table_history, self.Base.metadata, autoload_with=self.engine)
            
            hist = tbl_hist.insert().values(**rec_history)#.returning((tbl_hist.c.id).label('id'))
            result_history = conn.execute(hist)
            session=self.Session()
            session.begin()
            deleted = session.query(tbl).filter(tbl.c[table_name+"_id"]==resource_id).delete()
        except Exception as exc:
            if session: 
                session.rollback()
                session.close()
            raise exc
        finally:
            conn.close()
            if session:
                session.commit()
                session.close()
       
        return deleted
   

    def delete_child_m2m (self,parent_def_id,parent_id,child_def_id,child_id,org_id,user_id):
          
        del_rec=[]
    
        if user_id is not None:
            userid =user_id.id
        else:
            userid=None
     
        table_name,is_external = self.get_table_name_related(parent_def_id,child_def_id,org_id)
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        
        session=self.Session()

        with session.begin():
           
            if int(parent_def_id)<int(child_def_id):
                resource1=parent_id
                resource2=child_id
            else:
                resource2=parent_id
                resource1=child_id

            
            deleted = session.query(tbl).filter(tbl.c.Resource1==resource1,tbl.c.Resource2==resource2).delete()

        return  deleted
 
    #SqlAlchemy done
    def insert_data_m2m_v2 (self,resource_source_id,org_id,data,user_id,delete):
          
            rec=None
            ins_rec=[]
          #try:
            
            #resource_source_id,resource_related_def_id,model_definition_id,
            if user_id is not None:
                userid =user_id.id
            else:
                userid=None
            for item in data:
                if 'resourceDefId1' in item:
                  resource_def_id1 = int(item['resourceDefId1'])
                else:
                  raise Exception('mutant_insert_data_m2m_v2--> Resource1 definition is missing')
                
                if 'resourceDefId2' in item:
                  resource_def_id2 = int(item['resourceDefId2'])
                else:
                  raise Exception('mutant_insert_data_m2m_v2--> Resource2 definition is missing')
                
                
                table_name,is_external = self.get_table_name_related(resource_def_id1,resource_def_id2,org_id)
                tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
                conn = None
                session = None
                try:

                    if (resource_def_id1<resource_def_id2):
                         
                        #delete existing
                        if delete:
                            session=self.Session()
                            session.begin()
                            deleted = session.query(tbl).filter(tbl.c.Resource1_id==resource_source_id).delete()
                    else:
                 
                        #delete existing
                        if delete:
                            session=self.Session()
                            session.begin()
                            deleted = session.query(tbl).filter(tbl.c.Resource2_id==resource_source_id).delete()
                   
                    conn = self.engine.connect()
                    if item['resourceId'] is not None:
                        
                      for resourceId in item['resourceId']:
                            if type(resourceId)==dict:
                              id = resourceId['id']
                            elif type(resourceId)==int:
                              id = resourceId
                            else:
                              id = None
                            if (resource_def_id1<resource_def_id2):
                                resource1=resource_source_id
                                resource2=resourceId
                            else:
                                resource2=resource_source_id
                                resource1=resourceId

                            rec = conn.execute(tbl.insert().values(Resource1=resource1,Resource2=resource2,CreatedBy=userid,CreatedDateTime=timezone.now()))
                            
                            #rec = model_def.model_class().objects.create(ResourceSourceDefId=resource_source_def_id,ResourceSourceId=resource_source_id,ModelDefinitionId_id=model_definition_id,ResourceRelatedId=id,CreatedBy_id=userid,CreatedDateTime=timezone.now())
                            ins_rec.append(rec)
                    if session is not None: 
                        session.rolback()
                        session.close()
                finally:
                    if conn is not None: conn.close()  
                    if session is not None: 
                        session.commit()
                        session.close()
            return  ins_rec
    #SqlAlchemy done    
    def insert_data_m2m_no_delete (self,resourceDefId1,resourceDefId2,resource_record_id,data,org_id,user_id):
          
            
        rec=None
        ins_rec=[]
        
        resource_def_id1 = int(resourceDefId1)
        resource_def_id2 = int(resourceDefId2)

        table_name,is_external = self.get_table_name_related(resource_def_id1,resource_def_id2,org_id)
            
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        try:        
            conn = self.engine.connect()  
            if user_id is not None:
                userid =user_id.id
            else:
                userid=None

            for item in data:
                if item.get('resourceId',None) is not None:
                    for resourceId in item['resourceId']:
                        if (resource_def_id1<resource_def_id2):
                            resource1=resource_record_id
                            resource2=resourceId
                        else:
                            resource2=resource_record_id
                            resource1=resourceId
                                  
                        rec = conn.execute(tbl.insert().values(Resource1=resource1,Resource2=resource2,CreatedBy=userid,CreatedDateTime=timezone.now()))
                        #ErrorLog.objects.create(Error1={'item':item,'resource_source_id':resource_source_id,'table_name':table_name,'model_id':model_id},Error2={'data':data},CreatedDateTime=timezone.now())                         
           
                        ins_rec.append(rec)
        finally:
            conn.close()    
        return  ins_rec

    #SqlAlchemy done
    def get_m2m_data_full (self,resource_source_id,related_id,resource_record_id,org_id,type,session=None):
          
         
            rec=None
            ins_rec=[]
            m2mRecords=None
            #try:
            
            if type==0:
                table_name,is_external = self.get_table_name(related_id,org_id)
            else:
                table_name,is_external = self.get_table_name(resource_source_id,org_id) 
                
          
            table_name_m2m,is_external_related = self.get_table_name_related(resource_source_id,related_id,org_id)
                
            tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
            tbl_m2m = Table(table_name_m2m, self.Base.metadata, autoload_with=self.engine)
            ret_m2m = None
            
            close_session = False
            try:
                
                if not session:           
                    session = self.Session()
                    session.begin()
                    close_session=True

                order_by = tbl.c[table_name+'_id'].desc()
                columns_visible=[]
                filtered_data = {}
                if (resource_source_id<related_id):
                    joins=[(tbl_m2m,and_(tbl.c[table_name+'_id'] == tbl_m2m.c.Resource2,tbl_m2m.c.Resource1==resource_record_id))]
                    m2mRecords = self.build_query(session,tbl,table_name,columns_visible,filtered_data,{},joins,order_by)
                else:
                    joins=[(tbl_m2m,and_(tbl.c[table_name+'_id'] == tbl_m2m.c.Resource1,tbl_m2m.c.Resource2==resource_record_id))]
                    m2mRecords = self.build_query(session,tbl,table_name,columns_visible,filtered_data,{},joins,order_by)
                
                if m2mRecords is not None:
                    ret_m2m = m2mRecords.all()
                else:
                    ret_m2m = m2mRecords
            except Exception as exc:
                if session and close_session:
                    session.rollback()
                    session.close()
                raise exc
            finally:
                if session and close_session:
                    session.commit()
                    session.close()

            return ret_m2m

     #SqlAlchemy done
    def get_m2m_data_filter (self,resource_source_id,related_id,resource_record_id,org_id,type,filtered_data,session=None):
          
         
            rec=None
            ins_rec=[]
            m2mRecords=None
            #try:
            
            if type==0:
                table_name,is_external = self.get_table_name(related_id,org_id)
            else:
                table_name,is_external = self.get_table_name(resource_source_id,org_id) 
                
          
            table_name_m2m,is_external_related = self.get_table_name_related(resource_source_id,related_id,org_id)
                
            tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
            tbl_m2m = Table(table_name_m2m, self.Base.metadata, autoload_with=self.engine)
            ret_m2m = None
            
            close_session = False
            try:
                
                if not session:           
                    session = self.Session()
                    session.begin()
                    close_session=True

                order_by = tbl.c[table_name+'_id'].desc()
                columns_visible=[]
                if (resource_source_id<related_id):
                    joins=[(tbl_m2m,and_(tbl.c[table_name+'_id'] == tbl_m2m.c.Resource2,tbl_m2m.c.Resource1==resource_record_id))]
                    m2mRecords = self.build_query(session,tbl,table_name,columns_visible,filtered_data,{},joins,order_by)
                else:
                    joins=[(tbl_m2m,and_(tbl.c[table_name+'_id'] == tbl_m2m.c.Resource1,tbl_m2m.c.Resource2==resource_record_id))]
                    m2mRecords = self.build_query(session,tbl,table_name,columns_visible,filtered_data,{},joins,order_by)
                
                if m2mRecords is not None:
                    ret_m2m = m2mRecords.all()
                else:
                    ret_m2m = m2mRecords
            except Exception as exc:
                if session and close_session:
                    session.rollback()
                    session.close()
                raise exc
            finally:
                if session and close_session:
                    session.commit()
                    session.close()

            return ret_m2m      
           
        
    def get_m2m_data (self,resource_source_id,related_id,resource_record_id,model_id,org_id,type):
          
         
        rec=None
        ins_rec=[]
        m2mRecords=None
        ret_m2m = None
        try:
            
             
            table_name,is_external = self.get_table_name(resource_source_id,related_id,org_id) 
            tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
           

            session = self.Session()
            session.begin()
            if type==0:
                #m2mRecords = ResourceSourceDefId=resource_source_id,ResourceSourceId=resource_record_id,ModelDefinitionId_id=model_id).order_by('-id')
                m2mRecords = session.query(tbl).filter(tbl.c.ResourceSourceDefId==resource_source_id,tbl.c.ResourceSourceId==resource_record_id,tbl.c.ModelDefinitionId==model_id).order_by(tbl.c.id.desc())
            else:
                #m2mRecords = model_def.model_class().objects.filter(ResourceSourceDefId=resource_source_id,ResourceRelatedId=resource_record_id,ModelDefinitionId_id=model_id).order_by('-id')
                m2mRecords = session.query(tbl).filter(tbl.c.ResourceSourceDefId==resource_source_id,tbl.c.ResourceRelatedId==resource_record_id,tbl.c.ModelDefinitionId==model_id).order_by(tbl.c.id.desc())
            ret_m2m =  m2mRecords.all()

        except Exception as exc:
            if session: 
                session.rollback()
                session.close()
            raise exc
        finally:
            if session:
                session.commit()
                session.close()  
            
        return ret_m2m
    #SqlAlchemy done
    def get_m2m_data_all (self,resource_source_id,related_id,model_id,org_id):
          
         
        rec=None
        m2mRecords=None
        try:
            
            
            table_name,is_external = self.get_table_name(resource_source_id,related_id,org_id)
            
            tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
            
            conn = self.engine.connect()
            rec = tbl.select().where(tbl.c.ResourceSourceDefId==resource_source_id,tbl.c.ModelDefinitionId==model_id).order_by(tbl.c.id.desc())
            
            m2mRecords = conn.execute(rec).fetchall()
            
        except Exception as exc:
            
            conn.close()
            raise exc

        finally:
            conn.close()  
        
        return  m2mRecords


    def insert_data_generic (self,table_name,data):
            
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        ret_value = None
        try:
            conn = self.engine.connect()
            rec = conn.execute(tbl.insert().values(**data).returning(tbl.c[table_name+'_id']))
            ret_value = rec.fetchone()
        finally:
            conn.close()
        
        return ret_value

    def update_data_generic (self,table_name,rec_id,data):
            
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        ret_val=None
        try:
            conn = self.engine.connect()

            upd_rec = tbl.update().where(tbl.c[table_name+'_id']==rec_id).values(**data).returning(tbl.c[table_name+'_id'])
            result = conn.execute(upd_rec)
            ret_val=result.fetchone()
        finally:
            conn.close()

        return  ret_val

    def get_data_generic (self,table_name,table_joins,filter_data,order_by=None):
        
        op = {'>': lambda x, y: x > y,
            '<': lambda x, y: x < y,
            '<=': lambda x, y: x <= y,
            '>=': lambda x, y: x >= y,
            '==': lambda x, y: x == y}

        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        tables = [tbl]
        result=None
        session = self.Session()
        with session.begin():
            #prepare joins
            join_conditions = []
            if table_joins is not None:
                for join in table_joins:
                    tbl_join = Table(join['table'], self.Base.metadata, autoload_with=self.engine)
                    for key1,val1 in join['join'].items():

                        join_conditions.append((tbl_join,tbl.c[key1]==tbl_join.c[val1]))
                    
                    tables.append(tbl_join)
            
            #prepare where
            qry = session.query(*tables)
            for join_condition in join_conditions:
                qry=qry.join(*join_condition)
            for where_item in filter_data:
                
                tbl_where = Table(where_item['table'], self.Base.metadata, autoload_with=self.engine)
                for key2,val2 in where_item['condition'].items():
                    if 'operator' in where_item:
                        qry=qry.filter(op[where_item['operator']](tbl_where.c[key2],val2))
                    else:
                        qry=qry.filter(tbl_where.c[key2]==val2)
                #pdb.set_trace()
            if order_by is not None:
                qry = qry.order_by(*order_by)
            result =  qry.all()
       

        return result
    
    def build_join_fk(self,master_table,resource_id,related_id,field_name):
        
        table_name,is_external = self.get_table_name(resource_id,self.OrganisationId)
        table_name_related,is_external_related = self.get_table_name(related_id,self.OrganisationId)
        tbl_related = Table(table_name_related, self.Base.metadata, autoload_with=self.engine)
        join=(tbl_related,master_table.c[table_name+'_id']==tbl_related.c[field_name])
        return join,tbl_related,table_name_related

    def build_join_m2m(self,master_table,master_table_name,resource_id,related_id,field_name):
        
        table_related_name,is_external = self.get_table_name(related_id,self.OrganisationId)
        table_name_m2m,is_external_related = self.get_table_name_related(resource_id,related_id,self.OrganisationId)
        tbl_related = Table(table_related_name, self.Base.metadata, autoload_with=self.engine)
        tbl_m2m = Table(table_name_m2m, self.Base.metadata, autoload_with=self.engine)
        tbl_related_alias = aliased(tbl_related,name=field_name)

       
        
        if int(resource_id)<int(related_id):
            join=(tbl_m2m,and_(master_table.c[master_table_name+'_id'] == tbl_m2m.c.Resource1,tbl_related_alias.c[table_related_name+'_id']==tbl_m2m.c.Resource2))
        else:
            join=(tbl_m2m,and_(master_table.c[master_table_name+'_id'] == tbl_m2m.c.Resource2,tbl_related_alias.c[table_related_name+'_id']==tbl_m2m.c.Resource1))
        
        return join,tbl_m2m,tbl_related_alias
    #SqlAlchemy done    
    def count_records (self,resource_id,org_id,filter):
        
        
        table_name,is_external = self.get_table_name(resource_source_id,org_id)
        
        tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
        rec_count = None
        try:

            session = self.Session()
            session.begin()
            rec_count = session.query(tbl).filter(filter).count()
        except Exception as exc:
            if session: 
                session.rollback()
                session.close()
            raise exc
        finally:
            if session:
                session.commit()
                session.close()

        return rec_count
        
    #ToDo - migrate to SQLAlchemy
    def validate_unique (self,resource_id,org_id,filter,field_names,invalid_msg):
        
        model_def = mutant_get_model(resource_id,org_id)
        
        if model_def is not None:
            rec_count = model_def.model_class().objects.filter(filter).exists()
            if rec_count:
                raise ValidationError('mutant_validate_unique --> '+str(field_names)+' '+invalid_msg)  
            return rec_count
        else:
            raise ValidationError('mutant_validate_unique --> model definition is null')    
    #SqlAlchemy done     
    def get_m2m_data_in (self,resource_source_id,related_id,resource_record_ids,model_id,org_id,type):
          
        
        rec=None
        m2mRecords=None
        try:
            
            table_name,is_external = self.get_table_name_related(resource_source_id,related_id,org_id)
            
            tbl = Table(table_name, self.Base.metadata, autoload_with=self.engine)
            
            conn = self.engine.connect()
            rec = tbl.select().where(tbl.c.ResourceSourceDefId==resource_source_id,tbl.c.ResourceSourceId.in_(resource_record_ids),tbl.c.ModelDefinitionId==model_id,tbl.c.id.in_(resource_record_ids)).order_by(tbl.c.id.desc())
            
            m2mRecords = conn.execute(rec).fetchall()
        
        except: 'DoesNotExist'
        
        finally:

            conn.close()    
        return  m2mRecords


    def get_table_name (self,resource_id,org_id):
        
        session = self.Session()
        with session.begin():
            #rdef = ResourceDef.objects.get(id=resource_id)
            rdefs = session.query(ResourceDefSa).filter(ResourceDefSa.id==resource_id).all()
            #pdb.set_trace() #get_table_name
            exTableType = False
            table_name = "resource"+str(resource_id)+"_"+str(org_id) 
            for rdef in rdefs:
                if rdef.Parameters is not None:
                    if 'externalDataSource' in rdef.Parameters: 
                        exTableType = rdef.Parameters['externalDataSource']
                        if exTableType:
                            if 'extDataSourceLink' in rdef.Parameters:
                                table_name = rdef.Parameters['extDataSourceLink']
      
        return table_name,exTableType

    def get_table_name_related (self,resource_id_related,related_id,org_id):
        
        #rdef = ResourceDef.objects.get(id=resource_id_related)
        session = self.Session()
        with session.begin():
            rdefs = session.query(ResourceDefSa).filter(ResourceDefSa.id==resource_id_related).all()
            extTableType = False
            table_name = "resource"+str(resource_id_related)+"_"+str(org_id) 
            for rdef in rdefs:
                if rdef.Parameters is not None:
                    if 'externalDataSource' in rdef.Parameters:
                        extTableType = rdef.Parameters['externalDataSource']
                    else:
                        extTableType = False
                else:
                    extTableType = False
                
                if extTableType:
                    if 'extDataSourceLink' in rdef.Parameters:
                        table_name = rdef.Parameters['extDataSourceLink']
                    else:
                        if related_id>resource_id_related:
                            table_name = "resource"+str(resource_id_related)+"_"+str(org_id)+"resource"+str(related_id)+"_"+str(org_id)
                        else:
                            table_name = "resource"+str(related_id)+"_"+str(org_id)+"resource"+str(resource_id_related)+"_"+str(org_id)
                else:
                    if related_id > int(resource_id_related): #TODO - provjeriti zasto je uopce tu String
                        table_name = "resource"+str(resource_id_related)+"_"+str(org_id)+"resource"+str(related_id)+"_"+str(org_id)
                    else:
                        table_name = "resource"+str(related_id)+"_"+str(org_id)+"resource"+str(resource_id_related)+"_"+str(org_id)
        
        return table_name,extTableType