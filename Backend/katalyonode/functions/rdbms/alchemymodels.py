######################## alchemy models ##########################
###############################################################################

import pdb
from sqlalchemy import Table, Column, Integer,Boolean, String, Boolean,Text, ForeignKey,Numeric,Float,Date,Time,DateTime, MetaData
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from datetime import datetime
from sqlalchemy import inspect
import json

Base = declarative_base()


class AdminGroups(Base):
   __tablename__ = 'katalyonode_resourcedef_AdminGroups'
   id = Column('id', Integer, primary_key=True) 
   resourcedef_id = Column('resourcedef_id', Integer) 
   group_id = Column('group_id', Integer) 
   
   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}    

class AdminUsers(Base):
   __tablename__ = 'katalyonode_resourcedef_AdminUsers'
   id = Column('id', Integer, primary_key=True) 
   resourcedef_id = Column('resourcedef_id', Integer) 
   user_id = Column('user_id', Integer) 
   
   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}    

class DatasetTransformHeadSa(Base):
    __tablename__ = 'katalyonode_datasettransformhead'
    id = Column('id', Integer, primary_key=True)
    TransformName = Column('TransformName', String(255))
    CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
    CreatedDateTime = Column('CreatedDateTime',DateTime)
    ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
    ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
    FieldType1_id = Column('FieldType1_id',Integer,ForeignKey("katalyonode_codesdetail.id")) 
    FieldType2_id = Column('FieldType2_id',Integer,ForeignKey("katalyonode_codesdetail.id")) 
    Params = Column('Params',Text,nullable=True)

    def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}
class DatasetTrasnformHeadLangSa(Base):
    __tablename__ = 'katalyonode_datasettransformheadlang'
    id = Column('id', Integer, primary_key=True)
    Name = Column('TransformName', String(255))
    LangId =Column('LangId',  Integer, ForeignKey("katalyonode_language") )
    TransformationHeadId= Column('TransformationHeadId_id', Integer, ForeignKey("katalyonode_datasettransformhead.id"))

    def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns} 

class DatasetTransformDetailSa(Base):
   __tablename__ = 'katalyonode_datasettransformdetail'
   id = Column('id', Integer, primary_key=True)
    
   TextItem1 = Column('TextItem1', Text)
   TextItem2 = Column('TextItem2', Text)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)  
   CodeDetail1 = Column('CodeDetail1_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   CodeDetail2 = Column('CodeDetail2_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   CodeHead1 = Column('CodeHead1_id',Integer,ForeignKey("katalyonode_codeshead.id"),nullable=True)
   CodeHead2= Column('CodeHead2_id',Integer,ForeignKey("katalyonode_codeshead.id"),nullable=True)
   Group1_id= Column('Group1_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   Group2_id= Column('Group2_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   User1= Column('User1_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   User2= Column('User2_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   TransformationHeadId= Column('TransformationHeadId_id', Integer, ForeignKey("katalyonode_datasettransformhead.id"))
    
   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns} 



class DatasetMappingDetailSa(Base):
   __tablename__ = 'katalyonode_datasetmappingdetail'
   id = Column('id', Integer, primary_key=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   DatasetMappingHeadId = Column('DatasetMappingHeadId_id',Integer,ForeignKey("katalyonode_datasetmappinghead.id"))
   Field1 = Column('Field1_id',Integer,ForeignKey("katalyonode_resourcemodeldefinition.id")) 
   Field2 = Column('Field2_id',Integer,ForeignKey("katalyonode_resourcemodeldefinition.id")) 
   TransformationId = Column('TransformationId_id',Integer,ForeignKey("katalyonode_datasettransformhead.id")) 
   Search = Column('Search',Boolean)
   RecType = Column('RecType', Integer)

   
   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns} 

class DatasetMappingHeadSa(Base):
   __tablename__ = 'katalyonode_datasetmappinghead'
   id = Column('id', Integer, primary_key=True)
   MappingName = Column('MappingName',String(150))
   Active = Column('Active',Boolean)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   Dataset1 = Column('Dataset1_id',Integer, ForeignKey("katalyonode_resourcedef.id"))
   Dataset2 = Column('Dataset2_id',Integer, ForeignKey("katalyonode_resourcedef.id"))
   MappingType = Column('MappingType_id',Integer, ForeignKey("katalyonode_codesdetail.id"))

   def _asdict(self): #todo  - dodati if else "endswith"
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
              r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if hasattr(self, c.name+'_id'):
               r_dict[c.name+'_id'] = getattr(self, c.name+'_id')
      return r_dict
      
class DatasetMappingHeadLangSa(Base):
   __tablename__ = 'katalyonode_datasetmappingheadlang'
   id = Column('id', Integer, primary_key=True)
   Name = Column('Name',String(255))
   Lang = Column('Lang_id',Integer, ForeignKey("katalyonode_language.id")) 
   DatasetMappingHeadId = Column('DatasetMappingHeadId_id',Integer, ForeignKey("katalyonode_datasetmappinghead.id")) 
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
  
      
   def _asdict(self): #todo  - dodati if else "endswith"
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
              r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if hasattr(self, c.name+'_id'):
               r_dict[c.name+'_id'] = getattr(self, c.name+'_id')
      return r_dict
      
class auth_user(Base):
   __tablename__ = 'auth_user'
   id = Column('id', Integer, primary_key=True)
   password = Column('password', String(128))
   username = Column('username',String(150))
   first_name = Column('first_name',String(150))
   last_name = Column('last_name',String(150))
   email = Column('email',String(254))
   is_staff = Column('is_staff',Boolean)
   is_active = Column('is_active',Boolean)
   is_superuser = Column('is_superuser',Boolean)
   last_login = Column('last_login',DateTime)
   date_joined = Column('date_joined',DateTime)

   def _asdict(self):
      user_obj = {}
      for c in self.__table__.columns:
         user_obj[c.name] = getattr(self, c.name)
      user_obj['name'] = self.first_name+' '+self.last_name+' - '+self.username
      return user_obj


class auth_group(Base):
   __tablename__ = 'auth_group'
   id = Column('id', Integer, primary_key=True)
   name = Column('name',String(150))

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class auth_user_groups(Base):
   __tablename__ = 'auth_user_groups'
   id = Column('id', Integer, primary_key=True)
   user = Column('user_id',Integer,ForeignKey("auth_user.id"))
   group = Column('group_id',Integer,ForeignKey("auth_group.id"))
   

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class codesdetail(Base):
   __tablename__ = 'katalyonode_codesdetaillang'
   id_lang = Column('id', Integer, primary_key=True)
   id = Column('CodesDetailId_id', Integer)
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   Value = Column('Value',String(1000))
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

    

class WidgetDefinitionSa(Base):
   __tablename__ = 'katalyonode_widgetdefinition'
   id = Column('id', Integer,primary_key=True )
   WidgetName = Column('WidgetName',String(500))
   WidgetReference = Column('WidgetReference',String(500))
   ElementType = Column('ElementType',String(500))
   Status = Column('Status',String(20))
   Definition = Column('Definition',Text) 
   Parameters = Column('Parameters',Text) 
   
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns} 

class ResourceWidgetLinksSa(Base):
   __tablename__ = 'katalyonode_resourcewidgetlinks'
   id = Column('id', Integer,primary_key=True )
   LinkType = Column('LinkType',String(100))
   HideFromToolbox = Column('HideFromToolbox',Boolean)
   WidgetId = Column('WidgetId_id', Integer, ForeignKey("katalyonode_widgetdefinition.id"))
   ResourceId = Column('ResourceId_id', Integer, ForeignKey("katalyonode_resourceparams.id"))
   Parameters = Column('Parameters',Text)
   ToolboxGroup = Column('ToolboxGroup',String(255))
   ToolboxOrder = Column('ToolboxOrder', Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)


   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict


"""   

class CodesDetailLangSa(Base):
   __tablename__ = 'katalyonode_codesdetaillang'
   id = Column('id', Integer,primary_key=True )
   Lang = Column('Lang_id', Integer, ForeignKey("katalyonode_language.id"))
   Value = Column('Value',String(1000))
   CodesDetailId = Column('CodesDetailId_id', Integer, ForeignKey("katalyonode_codesdetail.id"))
   ParamsLang = Column('ParamsLang',String())
   
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {'id':self.id,'name':self.Value}
      
""" 

class MappingsTableSa(Base):
   __tablename__ = 'katalyonode_mappingstable'
   id = Column('id', Integer, primary_key=True)
   ResourceDefId  =  Column('ResourceDefId_id', Integer, ForeignKey('katalyonode_resourcedef.id')) 
   Name = Column('Name',String(255))
   FileFields = Column('FileFields',Text)
   ResourceMapping = Column('ResourceMapping',Text)


class CodesDetailSa(Base):
   __tablename__ = 'katalyonode_codesdetail'
   id = Column('id', Integer, primary_key=True)
   CodesHeadId  =  Column('CodesHeadId_id', Integer, ForeignKey('katalyonode_codeshead.id')) 
   Name = Column('Name',String(255))
   Active = Column('Active',Boolean,nullable=True)
   Params = Column('Params',Text,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict

class CodesHeadSa(Base):
   __tablename__ = 'katalyonode_codeshead'
   id = Column('id', Integer, primary_key=True) 
   Version=   Column('Version', Integer)
   CodeType   = Column('CodeType',Text)#, ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   Active   = Column('Active', Boolean,nullable=True)   
   CodeHeadName = Column('CodeHeadName', String(255))
   OrganisationId  =      Column('OrganisationId_id', Integer, ForeignKey('katalyonode_organisations.id')) 
   UserGeneratedId  =    Column('UserGeneratedId', Boolean,nullable=True) 
   CreatedDateTime   =   Column('CreatedDateTime', DateTime) 
   ChangedDateTime =       Column('ChangedDateTime', DateTime) 
   ChangedBy =        Column('ChangedBy_id', Integer, ForeignKey('auth_user.id'))
   CreatedBy =        Column('CreatedBy_id', Integer, ForeignKey('auth_user.id'))

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict
      
class CodesHeadLangSa(Base):
   __tablename__ = 'katalyonode_codesheadlang'
   id = Column('id', Integer, primary_key=True)
   CodesHeadId = Column('CodesHeadId_id',Integer,ForeignKey("katalyonode_codeshead.id"))
   CodeName = Column('CodeName',String(255))
   Description = Column('Description',Text,nullable=True)
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"),nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class PublishedResourcesSa(Base):
   __tablename__ = 'katalyonode_publishedresources'
   id = Column('id', Integer, primary_key=True)
   ResourceId = Column('ResourceId_id',Integer,ForeignKey("katalyonode_resourcedef.id")) #models.ForeignKey(ResourceDef,related_name='PublishedResources_resourceid',on_delete=models.CASCADE)
   OrganisationId = Column('OrganisationId_id',Integer,ForeignKey("katalyonode_organisations.id")) #models.ForeignKey(Organisations,on_delete=models.CASCADE)
   PublishType = Column('PublishType',Text)
   Definition = Column('Definition',Text)
   Form = Column('Form',Text)
   Parameters = Column('Parameters',Text)
   Description = Column('Description',Text)
   Active = Column('Active',Boolean)
   Version = Column('Version',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) #models.ForeignKey(User,related_name='PublishedResources_createdby',blank=True, null=True,on_delete=models.CASCADE)
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) #models.ForeignKey(User,related_name='PublishedResources_changedby',blank=True, null=True,on_delete=models.CASCADE)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class PresentationElementsSa(Base):
   __tablename__ = 'katalyonode_presentationelements'
   id = Column('id', Integer, primary_key=True)
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   OrganisationId = Column('OrganisationId_id',Integer,ForeignKey("katalyonode_organisations.id"))
   #TaskDefId = models.ForeignKey(ResourceDef,related_name='PresentationElements_taskdefid',blank=True, null=True,on_delete=models.CASCADE)
   UuidRef = Column('UuidRef',Text)
   FormType = Column('FormType',String(1))
   ResourceType=Column('ResourceType',String(1))
   DirectiveName=Column('DirectiveName',String(255))
   AngularDirectiveName=Column('AngularDirectiveName',String(255))
   OverrideType=Column('OverrideType',String(1))
   #ParentWidgetId = models.ForeignKey('self',related_name='PresentationElements_parentwidgetid',blank=True, null=True,on_delete=models.CASCADE)
   #OriginalPresentationElementId = models.ForeignKey('self',related_name='PresentationElements_originalpresentationelementid',blank=True, null=True,on_delete=models.CASCADE)
   ResourceModelDefinitionId = Column('ResourceModelDefinitionId_id',Integer,ForeignKey("katalyonode_resourcemodeldefinition.id"),nullable=True)
   ElementType = Column('ElementType',String(200))
   #ContainerId = models.ForeignKey('self',related_name='PresentationElements_containerid',blank=True, null=True,on_delete=models.CASCADE)
   ContainerElement = Column('ContainerElement',Text,nullable=True)
   Status = Column('Status',String(200))
   SizeX = Column('SizeX',Integer,nullable=True)
   SizeY = Column('SizeY',String(5))
   #Column =Column('Column',Integer)
   #Row = Column('Row',Integer)
   #Locked = Column('Locked',Boolean)
   Order = Column('Order',Integer)
   Tabindex =  Column('Tabindex',Integer)
   PopulateMethod = Column('PopulateMethod',String(1),nullable=True) #models.CharField(max_length=1,blank=True, null=True)
   PopulateType = Column('PopulateType',String(2),nullable=True) #models.CharField(max_length=2,null=True)
   SrcWidgetId = Column('SrcWidgetId',Text,nullable=True) #models.TextField(blank=True, null=True)
   AutoSearch= Column('AutoSearch',Boolean,nullable=True) #models.BooleanField( null=True) #MIGOR
   ShowSearchForm =  Column('ShowSearchForm',Boolean,nullable=True) #models.BooleanField(null=True) #MIGOR  
   Related =  Column('Related_id',Integer,ForeignKey("katalyonode_resourcedef.id"),nullable=True)
   PopulateTypeParent = Column('PopulateTypeParent',String(3),nullable=True) #models.CharField(blank=True,null=True,max_length=3)
   PopulateParentMethod = Column('PopulateParentMethod',String(1),nullable=True) #models.CharField(blank=True,null=True,max_length=1)
   SrcParentWidgetId = Column('SrcParentWidgetId',Text,nullable=True) #models.TextField(blank=True, null=True)
   HasParentDataset =  Column('HasParentDataset',Boolean,nullable=True) #models.BooleanField()
   ParentDatasetId = Column('ParentDatasetId_id',Integer,ForeignKey("katalyonode_resourcedef.id"),nullable=True)
   InlineTask = Column('InlineTask',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   ShowHeader = Column('ShowHeader',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   Required =  Column('Required',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   ShowField =  Column('ShowField',Boolean,nullable=True)#models.BooleanField(blank=True, null=True)
   UseDefault =  Column('UseDefault',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   DefaultValue = Column('DefaultValue',Text,nullable=True) #jsonfield.JSONField(blank=True,null=True)
   UseSystemDate =  Column('UseSystemDate',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   ReadOnly =  Column('ReadOnly',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   UpdateWithDefault =  Column('UpdateWithDefault',Boolean,nullable=True) #models.BooleanField(blank=True, null=True)
   NoUpdate=  Column('NoUpdate',Boolean,nullable=True) #models.BooleanField(null=True)
   Multiline=  Column('Multiline',Boolean,nullable=True) #models.BooleanField(null=True)
   UniqueId = Column('UniqueId_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   Search = Column('Search',Text,nullable=True) #jsonfield.JSONField(blank=True, null=True)
   DatasetMapping = Column('DatasetMapping',Text,nullable=True) #jsonfield.JSONField(blank=True, null=True)
   PresentationType = Column('PresentationType',String(1),nullable=True) #models.CharField(blank=True, null=True,max_length=1)
   NameField = Column('NameField',Text,nullable=True)
   Parameters = Column('Parameters',Text,nullable=True)
   VersionFrom = Column('VersionFrom',Integer)
   VersionTo = Column('VersionTo',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) 
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class PresentationElementsLangSa(Base):
   __tablename__ = 'katalyonode_presentationelementslang'
   id = Column('id', Integer, primary_key=True)
   PresentationElementsId = Column('PresentationElementsId_id',Integer,ForeignKey("katalyonode_presentationelements.id"))  #models.ForeignKey(PresentationElements,on_delete=models.CASCADE)
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   Name = Column('Name',String(1000),nullable=True) #models.CharField(max_length=1000)
   Label = Column('Label',String(1000),nullable=True) #models.CharField(max_length=1000)
   Subject = Column('Subject',String(1000),nullable=True) #models.CharField(max_length=1000)
   Description = Column('Description',String(4000),nullable=True) #models.CharField(max_length=4000)
   ErrorMsg = Column('ErrorMsg',String(4000),nullable=True) #models.CharField(max_length=4000)
   SearchText = Column('SearchText',String(4000),nullable=True) #models.CharField(blank=True,null=True,max_length=4000)
   LabelLength = Column('LabelLength',Integer) #models.IntegerField(blank=True,null=True)
   ValueLength = Column('ValueLength',Integer) #models.IntegerField(blank=True,null=True)
   FormDef = Column('FormDef',Text,nullable=True) #models.TextField()
   FormDefSearch = Column('FormDefSearch',Text,nullable=True) #models.TextField(blank=True,null=True)
   ResponseMsg = Column('ResponseMsg',Text,nullable=True) #Column('NameField',Text,nullable=True) models.TextField(blank=True,null=True)
   GridState = Column('GridState',Text,nullable=True) #models.TextField(blank=True,null=True)
   ParametersLang = Column('ParametersLang',Text,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) #models.ForeignKey(User,related_name='PublishedResources_createdby',blank=True, null=True,on_delete=models.CASCADE)
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) #models.ForeignKey(User,related_name='PublishedResources_changedby',blank=True, null=True,on_delete=models.CASCADE)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class ResourceModelDefinitionSa(Base):
   __tablename__ = 'katalyonode_resourcemodeldefinition'
   id = Column('id', Integer, primary_key=True) 
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   Related = Column('Related_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   ResourceLink = Column('ResourceLink_id',Integer,ForeignKey("katalyonode_resourcelinks.id"))
   FieldType = Column('FieldType',String(200))
   GenericType = Column('GenericType',String(200))
   Dbtype = Column('Dbtype',String(200))
   FieldName = Column('FieldName',String(200))
   Status = Column('Status',String(200))
   DeployAction = Column('DeployAction',String(2))
   CodeId = Column('CodeId',Integer) 
   DeployedVersion = Column('DeployedVersion',Integer) 
   MaxLength = Column('MaxLength',Integer)
   InitialVersion = Column('InitialVersion',Integer)
   Version = Column('Version',Integer)
   DeployedBy = Column('DeployedBy_id',Integer,ForeignKey("auth_user.id"))
   DeployDate = Column('DeployDate',DateTime)
   RequiredField =  Column('RequiredField',Boolean)
   ExternalKey =  Column('ExternalKey',Boolean,nullable=True)
   Unique =  Column('Unique',Boolean,nullable=True)
   Deployed =  Column('Deployed',Boolean)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class ResourceModelDefinitionHistorySa(Base):
   __tablename__ = 'katalyonode_resourcemodeldefinitionhistory'
   id = Column('id', Integer, primary_key=True) 
   ResourceModelDefinitionId = Column('ResourceModelDefinitionId_id',Integer,ForeignKey("katalyonode_resourcemodeldefinition.id"))
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   Related = Column('Related_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   ResourceLink = Column('ResourceLink_id',Integer,ForeignKey("katalyonode_resourcelinks.id"))
   FieldType = Column('FieldType',String(200))
   GenericType = Column('GenericType',String(200))
   Dbtype = Column('Dbtype',String(200))
   FieldName = Column('FieldName',String(200))
   Status = Column('Status',String(200))
   DeployAction = Column('DeployAction',String(2))
   CodeId = Column('CodeId',Integer) 
   DeployedVersion = Column('DeployedVersion',Integer) 
   MaxLength = Column('MaxLength',Integer)
   InitialVersion = Column('InitialVersion',Integer)
   Version = Column('Version',Integer)
   DeployedBy = Column('DeployedBy_id',Integer,ForeignKey("auth_user.id"))
   DeployDate = Column('DeployDate',DateTime)
   RequiredField =  Column('RequiredField',Boolean)
   ExternalKey =  Column('ExternalKey',Boolean,nullable=True)
   Unique =  Column('Unique',Boolean,nullable=True)
   Deployed =  Column('Deployed',Boolean)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}
"""
class ResourceDefTagsSa(Base):
   __tablename__ = 'katalyonode_resourcedef_Tags'
   id = Column('id', Integer, primary_key=True)
   ResourceDef = Column('ResourceDef_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   CodesDetail = Column('CodesDetail_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
"""
#ResourceDefTagsSa = Table('katalyonode_resourcedef_Tags',self.metadata,autoload_with=self.engine)

class ResourceDefSa(Base):
   __tablename__ = 'katalyonode_resourcedef'
   id = Column('id', Integer, primary_key=True) 
   Status = Column('Status_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   Private =  Column('Private',Boolean)
   InitialVersion = Column('InitialVersion',Integer)
   Version = Column('Version',Integer)
   Parameters = Column('Parameters',Text,nullable=True)
   FormDefined =  Column('FormDefined',Boolean)
   TableCreated =  Column('TableCreated',Boolean)
   IsBlockchain =  Column('IsBlockchain',Boolean,nullable=True)
   Organisation = Column('Organisation_id',Integer,ForeignKey("katalyonode_organisations.id"))
   ResourceType=Column('ResourceType_id',Integer,ForeignKey("katalyonode_resourceparams.id"))
   Active =  Column('Active',Boolean)
   #Tags = relationship('CodesDetailSa', secondary=ResourceDefTagsSa, back_populates='katalyonode_resourcedef_Tags') #check if it works
   #AdminUsers = relationship('auth_user', secondary='katalyonode_resourcedef_AdminUsers', back_populates='adminusers')
   #AdminGroups = relationship('auth_group', secondary='katalyonode_resourcedef_AdminGroups', back_populates='admingroups')
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   #def _asdict(self):
   #   return {c.name: getattr(self, c.name) for c in self.__table__.columns}    
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict
      
   def _toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }

class ResourceDefLangSa(Base):
   __tablename__ = 'katalyonode_resourcedeflang'
   id = Column('id', Integer, primary_key=True) 
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   Name = Column('Name',String(100))
   Subject = Column('Subject',String(200))
   Description = Column('Description',Text)
   PresentationDef = Column('PresentationDef',Text)
   InitiateForm =  Column('InitiateForm',Boolean)
   PresentationDefExe = Column('PresentationDefExe',Text)
   ExecuteForm =  Column('ExecuteForm',Boolean)
   PresentationDefExe = Column('PresentationDefExe',Text)
   InitFormBtnDef = Column('InitFormBtnDef',Text,nullable=True)
   InitForm = Column('InitForm',Text,nullable=True)
   ExeForm = Column('ExeForm',Text,nullable=True)
   ParametersLang = Column('ParametersLang',Text,nullable=True)
   InitialVersion = Column('InitialVersion',Integer)
   Version = Column('Version',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   #def _asdict(self):
   #   return {c.name: getattr(self, c.name) for c in self.__table__.columns}    
      
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Parameters":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            elif c.name == "PresentationDef":
                pass
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
            else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict

class LanguageSa(Base):
   __tablename__ = 'katalyonode_language'
   id = Column('id', Integer, primary_key=True)
   Name = Column('Name',Text) #models.CharField(max_length=1000)
   Code = Column('Code',Text) #models.CharField(max_length=1000)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) #models.ForeignKey(User,related_name='PublishedResources_createdby',blank=True, null=True,on_delete=models.CASCADE)
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True) #models.ForeignKey(User,related_name='PublishedResources_changedby',blank=True, null=True,on_delete=models.CASCADE)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)


   def toDict(self):
        return { c.key: getattr(self, c.key) for c in inspect(self).mapper.column_attrs }

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class UserExtendedPropertiesSa(Base):
   __tablename__ = 'katalyonode_userextendedproperties'
   id = Column('id', Integer, primary_key=True)
   UserId = Column('UserId_id',Integer,ForeignKey("auth_user.id"))
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   Organisation = Column('Organisation_id',Integer,ForeignKey("katalyonode_organisations.id"))
   HideCustomizeHome =  Column('HideCustomizeHome',Boolean)
   HideCreateResourceHome =  Column('HideCreateResourceHome',Boolean)
   HideSearchResourceHome =  Column('HideSearchResourceHome',Boolean)
   HideAppsHome =  Column('HideAppsHome',Boolean)
   ChangePassDigest = Column('ChangePassDigest',Text,nullable=True)
   ChangePassRequested = Column('ChangePassRequested',Boolean,nullable=True)
   Params = Column('Params',Text,nullable=True)
   APIUser = Column('APIUser',Boolean)
   AdvancedParams = Column('AdvancedParams',Text,nullable=True)
   ChangePassValid = Column('ChangePassValid',DateTime,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)


   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            value = getattr(self, c.name)              
            if c.name=="Params" and value:
              r_dict[c.name] = json.loads(value)
            elif c.name == "EXAMPLE":
                pass
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict

class TokenSa(Base):
   __tablename__ = 'authtoken_token'
   key = Column('key', String(40), primary_key=True)
   created = Column('created',DateTime)
   user_id = Column('user_id',Integer,ForeignKey("auth_user.id"),nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class ErrorLogSa(Base):
   __tablename__ = 'katalyonode_errorlog'
   id = Column('id', Integer, primary_key=True)
   Error1 = Column('Error1',Text)
   Error2 = Column('Error2',Text)
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: datetime_handler(getattr(self, c.name)) for c in self.__table__.columns}

class OrganisationsSa(Base):
   __tablename__ = 'katalyonode_organisations'
   id = Column('id', Integer, primary_key=True)
   Name = Column('Name',String(200))
   Description = Column('Description',Text)
   Country = Column('Country_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   Industry = Column('Industry_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   OrgType = Column('OrgType',String(10))
   Active =  Column('Active',Boolean)
   ETHAccount = Column('ETHAccount',Text)
   ETHBalance = Column('ETHBalance',Numeric(1000,18))
   GroupEveryone = Column('GroupEveryone_id',Integer,ForeignKey("auth_group.id")) 
   ApiUser = Column('ApiUser_id',Integer,ForeignKey("auth_user.id")) 
   Parameters = Column('Parameters',Text,nullable=True)
   Version = Column('Version',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class ResourceSubscriptionsSa(Base):
   __tablename__ = 'katalyonode_resourcesubscriptions'
   id = Column('id', Integer, primary_key=True) 
   ResourceId = Column('ResourceId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   SubscriptionType = Column('SubscriptionType',String(255))
   Parameters = Column('Parameters',Text,nullable=True)
   Active =  Column('Active',Boolean)
   UserId = Column('UserId_id',Integer,ForeignKey("auth_user.id"))
   GroupId = Column('GroupId_id',Integer,ForeignKey("auth_group.id"))
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}   


class ResourceParamsSa(Base):
   __tablename__ = 'katalyonode_resourceparams'
   id = Column('id', Integer, primary_key=True) 
   ResourceCode = Column('ResourceCode',String(255))
   ResourceName = Column('ResourceName',String(255))
   ResourceDescription = Column('ResourceDescription',String(1000))
   Parameters = Column('Parameters',Text,nullable=True)
   DefaultForm = Column('DefaultForm',Text,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   #def _asdict(self):
   #   return {c.name: getattr(self, c.name) for c in self.__table__.columns}   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            value = getattr(self, c.name)              
            if c.name=="Parameters" and value:
              r_dict[c.name] = json.loads(value)
            elif c.name=="DefaultForm" and value:
              r_dict[c.name] = json.loads(value)
            elif c.name == "EXAMPLE":
                pass
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))

      return r_dict

def datetime_handler(x):
   
   if isinstance(x, datetime):
        return str(x)#.isoformat()
   else:
      return x
      
class ResourceLinksSa(Base):
   __tablename__ = 'katalyonode_resourcelinks'
   id = Column('id', Integer, primary_key=True) 
   LinkType = Column('LinkType',String(100))
   Parameters = Column('Parameters',Text())
   Resource1 = Column('Resource1_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   Resource2 = Column('Resource2_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}   
     
 
class PagesSa(Base):
   __tablename__ = 'katalyonode_pages'
   id = Column('id', Integer, primary_key=True) 
   Target = Column('Target',String(1000))
   Params = Column('Params',Text())
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   PageType = Column('PageType_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id"))
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}   
     

class MenusUsersSa(Base):
   __tablename__ = 'katalyonode_menus_Users'
   id = Column('id', Integer, primary_key=True) 
   MenusId = Column('menus_id',Integer,ForeignKey("katalyonode_menus.id"))
   UserId = Column('user_id',Integer,ForeignKey("auth_user.id"))

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}   
     
class MenusGroupsSa(Base):
   __tablename__ = 'katalyonode_menus_Groups'
   id = Column('id', Integer, primary_key=True) 
   MenusId = Column('menus_id',Integer,ForeignKey("katalyonode_menus.id"))
   GroupId = Column('group_id',Integer,ForeignKey("auth_group.id"))

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}   
     
class MenusSa(Base):
   __tablename__ = 'katalyonode_menus'
   id = Column('id', Integer, primary_key=True) 
   Name = Column('Name', String(200))
   MenuType=   Column('MenuType', Integer)
   Params   = Column('Params', Text())
   ParentItem=   Column('ParentItem', Integer )
   CreatedDateTime   =   Column('CreatedDateTime', DateTime) 
   ChangedDateTime =       Column('ChangedDateTime', DateTime) 
   ChangedBy =        Column('ChangedBy_id', Integer, ForeignKey('auth_user.id'))
   CreatedBy =        Column('CreatedBy_id', Integer, ForeignKey('auth_user.id'))
   Icon =     Column('Icon', String(200))
   Navbar=         Column('Navbar', Integer) 
   Order  =      Column('Order', Integer) 
   Active  =    Column('Active', Boolean) 
   TargetPageId =      Column('TargetPage_id', Integer, ForeignKey('katalyonode_pages.id')) 
   MenuPosition =       Column('MenuPosition', Text()) 
   AlwaysNew    =    Column('AlwaysNew', Boolean) 
   ResourceDefId =      Column('ResourceDefId_id', Integer, ForeignKey('katalyonode_resourcedef.id')) 
   Target     =  Column('Target_id', Integer, ForeignKey('katalyonode_resourcedef.id')) 

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}   
   
 



class CookiesSa(Base):
   __tablename__ = 'katalyonode_cookies'
   id = Column('id', Integer, primary_key=True)
   eid = Column('EMail',String(50))
   ver = Column('SignupKey',Integer)
   time =  Column('time',DateTime)
   apps =Column('apps',Text,nullable=True)
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict


class SignupsSa(Base):
   __tablename__ = 'katalyonode_signups'
   id = Column('id', Integer, primary_key=True)
   EMail = Column('EMail',String(500))
   SignupKey = Column('SignupKey',String(1000))
   MailConfirmed =  Column('MailConfirmed',Boolean)
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class group_owners(Base):
   __tablename__ = 'katalyonode_groupextendedproperties_GroupOwners'
   id = Column('id', Integer, primary_key=True)
   groupextendedproperties = Column('groupextendedproperties_id',Integer,ForeignKey("katalyonode_groupextendedproperties.id"))
   user = Column('user_id',Integer,ForeignKey("auth_user.id"))

   def _asdict(self):
      return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class GroupExtendedPropertiesSa(Base):
   __tablename__ = 'katalyonode_groupextendedproperties'
   id = Column('id', Integer, primary_key=True)
   GroupId = Column('GroupId_id',Integer,ForeignKey("auth_group.id")) 
   Organisation = Column('Organisation_id',Integer,ForeignKey("katalyonode_organisations.id"))
   GroupOwners = relationship('auth_user', secondary='katalyonode_groupextendedproperties_GroupOwners')
   Params =Column('Params',Text,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict


class GroupLangSa(Base):
   __tablename__ = 'katalyonode_grouplang'
   id = Column('id', Integer, primary_key=True)
   GroupId = Column('GroupId_id',Integer,ForeignKey("auth_group.id")) 
   Name = Column('Name',String(200))
   Description = Column('Description',Text)
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class ApiMessagesSa(Base):
   __tablename__ = 'katalyonode_apimessages'
   id = Column('id', Integer, primary_key=True)
   MessageRef = Column('MessageRef',Text)
   Message = Column('Message',Text)
   MessageType = Column('MessageType',String(10))
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)

   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class TaskInitiationInstanceSa(Base):
   __tablename__ = 'katalyonode_taskinitiationinstance'
   id = Column('id', Integer, primary_key=True)
   TaskDefId = Column('TaskDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   InitiatorId = Column('InitiatorId_id',Integer,ForeignKey("auth_user.id"))
   Status = Column('Status_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   TaskType = Column('TaskType_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   NoOfExeInstances = Column('NoOfExeInstances',Integer)
   ExeInstanceLimit = Column('ExeInstanceLimit',Integer)
   PreviousTaskId = Column('PreviousTaskId_id',Integer,ForeignKey("katalyonode_taskexecutioninstances.id"))
   TransactionId = Column('TransactionId',String(1000))
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict
    
class TaskExecutionInstancesSa(Base):    
   __tablename__ = 'katalyonode_taskexecutioninstances'
   id = Column('id', Integer, primary_key=True)
   TaskDefId = Column('TaskDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   TaskInitiateId = Column('TaskInitiateId_id',Integer,ForeignKey("katalyonode_taskinitiationinstance.id"))
   ActualOwner = Column('ActualOwner_id',Integer,ForeignKey("auth_user.id"))
   Status = Column('Status_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   TaskOutcomeCodeItemId = Column('TaskOutcomeCodeItemId_id',Integer,ForeignKey("katalyonode_taskoutcomesdetail.id"),nullable=True)
   FormData = Column('FormData',Text)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict


class TaskInitiationInstanceToResourceSa(Base): 
   __tablename__ = 'katalyonode_taskinitiationinstancetoresource'
   id = Column('id', Integer, primary_key=True)
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   TaskInitiateId = Column('TaskInitiateId_id',Integer,ForeignKey("katalyonode_taskinitiationinstance.id"))
   ResourceId = Column('ResourceId',Integer)
   ParentDatasetDefId = Column('ParentDatasetDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   ParentDatasetId = Column('ParentDatasetId',Integer)
   PresentationElementUuidRef = Column('PresentationElementUuidRef',Text)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict
    
    
class TaskExecutionInstanceToResourceSa(Base):
   __tablename__ = 'katalyonode_taskexecutioninstancetoresource'
   id = Column('id', Integer, primary_key=True)
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   TaskExecuteId = Column('TaskExecuteId_id',Integer,ForeignKey("katalyonode_taskexecutioninstances.id"))
   ResourceId = Column('ResourceId',Integer)
   ParentDatasetDefId = Column('ParentDatasetDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   ParentDatasetId = Column('ParentDatasetId',Integer)
   PresentationElementUuidRef = Column('PresentationElementUuidRef',Text)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict


class TaskAssignmentsSa(Base):
   __tablename__ = 'katalyonode_taskassignments'
   id = Column('id', Integer, primary_key=True)
   UserId = Column('UserId_id',Integer,ForeignKey("auth_user.id"))
   TaskInitiationId = Column('TaskInitiationId_id',Integer,ForeignKey("katalyonode_taskinitiationinstance.id"))
   ForGroup = Column('ForGroup_id',Integer,ForeignKey("auth_group.id"),nullable=True)
   AssignGroup = Column('AssignGroup',String(255),nullable=True)
   Claimed = Column('Claimed',Boolean)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict


class TaskParametersSa(Base):
   __tablename__ = 'katalyonode_taskparameters'
   id = Column('id', Integer, primary_key=True)
   ResourceDefId = Column('ResourceDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   OrganisationId  =      Column('OrganisationId_id', Integer, ForeignKey('katalyonode_organisations.id')) 
   TaskType = Column('TaskType_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   MaxActualOwners = Column('MaxActualOwners',Integer)
   NoLimit = Column('NoLimit',Boolean,nullable=True)
   SelfAssigned = Column('SelfAssigned',Boolean,nullable=True)
   AutoAssign = Column('AutoAssign',Boolean,nullable=True)
   AutoStart = Column('AutoStart',Boolean,nullable=True)
   AutoClaim = Column('AutoClaim',Boolean,nullable=True)
   AutoInitiate = Column('AutoInitiate',Boolean,nullable=True)
   CanDelegate = Column('CanDelegate',Boolean,nullable=True)
   CanHold = Column('CanHold',Boolean,nullable=True)
   CanRelease = Column('CanRelease',Boolean,nullable=True)
   AutoExecute = Column('AutoExecute',Boolean,nullable=True)
   InfiniteTask = Column('InfiniteTask',Boolean,nullable=True)
   TaskInitiatorsSaved = Column('TaskInitiatorsSaved',Boolean,nullable=True)
   PotentialOwnersSaved = Column('PotentialOwnersSaved',Boolean,nullable=True)
   TaskOutcomesSaved = Column('TaskOutcomesSaved',Boolean,nullable=True)
   TaskOutcomeHeadId = Column('TaskOutcomeHeadId_id',Integer,ForeignKey("katalyonode_taskoutcomeshead.id"),nullable=True)
   DefaultOutcomeId = Column('DefaultOutcomeId_id',Integer,ForeignKey("katalyonode_taskoutcomesdetail.id"),nullable=True)
   UniqueId = Column('UniqueId_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   UniqueIdGroup = Column('UniqueIdGroup_id',Integer,ForeignKey("katalyonode_codesdetail.id"),nullable=True)
   PublicTask = Column('PublicTask',Boolean,nullable=True)
   OnMarketAsDef = Column('OnMarketAsDef',Boolean,nullable=True)
   OnMarketAsService = Column('OnMarketAsService',Boolean,nullable=True)
   DefaultLang = Column('DefaultLang_id',Integer,ForeignKey("katalyonode_language.id"))
   AssignTypeInitiator = Column('AssignTypeInitiator',String(255),nullable=True)
   AssignTypeInitiatorGroup = Column('AssignTypeInitiatorGroup',String(255),nullable=True)
   AssignTypeOwner = Column('AssignTypeOwner',String(255),nullable=True)
   AssignTypeOwnerGroup = Column('AssignTypeOwnerGroup',String(255),nullable=True)
   AssignTypeAdmin = Column('AssignTypeAdmin',String(255),nullable=True)
   UniqueName = Column('UniqueName',String(4000),nullable=True)
   Parameters = Column('Parameters',Text,nullable=True)
   Version = Column('Version',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class TaskCodesActionsSa(Base):
   __tablename__ = 'katalyonode_taskcodesactions'
   id = Column('id', Integer, primary_key=True)
   TaskDefId = Column('TaskDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   CodeId1 = Column('CodeId1_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   CodeName1 = Column('CodeName1',String(255),nullable=True)
   CodeId2 = Column('CodeId2_id',Integer,ForeignKey("katalyonode_codesdetail.id"))
   CodeName2 = Column('CodeName2',String(255),nullable=True)
   OutcomeId = Column('OutcomeId_id',Integer,ForeignKey("katalyonode_taskoutcomesdetail.id"))
   OutcomeName = Column('OutcomeName',String(255),nullable=True)
   ActionId = Column('ActionId',Integer)
   ActionType = Column('ActionType',String(255),nullable=True)
   ActionName = Column('ActionName',String(255),nullable=True)
   InitiateTaskId = Column('InitiateTaskId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   ActionParams = Column('ActionParams',Text,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class TaskAssignmentDefUserSa(Base):
   __tablename__ = 'katalyonode_taskassignmentdefuser'
   id = Column('id', Integer, primary_key=True)
   AssigneeId = Column('AssigneeId_id',Integer,ForeignKey("auth_user.id")) 
   TaskDefId = Column('TaskDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   AssignType = Column('AssignType',String(255),nullable=True)
   SubAssignType = Column('SubAssignType',String(255),nullable=True)
   AssignGroup = Column('AssignGroup',String(255),nullable=True)
   VersionFrom = Column('VersionFrom',Integer)
   VersionTo = Column('VersionTo',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict
   

class TaskAssignmentDefGroupSa(Base):
   __tablename__ = 'katalyonode_taskassignmentdefgroup'
   id = Column('id', Integer, primary_key=True)
   AssigneeId = Column('AssigneeId_id',Integer,ForeignKey("auth_group.id")) 
   TaskDefId = Column('TaskDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   AssignType = Column('AssignType',String(255),nullable=True)
   SubAssignType = Column('SubAssignType',String(255),nullable=True)
   AssignGroup = Column('AssignGroup',String(255),nullable=True)
   VersionFrom = Column('VersionFrom',Integer)
   VersionTo = Column('VersionTo',Integer)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict


class TaskTransactionsSa(Base):
   __tablename__ = 'katalyonode_tasktransactions'
   id = Column('id', Integer, primary_key=True)
   TransactionId = Column('TransactionId',String(1000))
   Status = Column('Status',String(10),nullable=True)
   Params = Column('Params',Text,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if hasattr(self, c.name):
            if c.name=="Params":
              r_dict[c.name] = json.loads(getattr(self, c.name) or '{}')
            else:
               if c.name.endswith('_id'):
                  col_name = c.name.replace('_id','')
                  r_dict[col_name] = getattr(self, col_name)
               else:
                  r_dict[c.name] = datetime_handler(getattr(self, c.name))
         else:
            if c.name.endswith('_id'):
               col_name = c.name.replace('_id','')
               r_dict[col_name] = getattr(self, col_name)
            else:
               r_dict[c.name] = datetime_handler(getattr(self, c.name))
      return r_dict

class TaskDefWidgetDependenciesSa(Base):
   __tablename__ = 'katalyonode_taskdefwidgetdependencies'
   id = Column('id', Integer, primary_key=True)
   TaskDefId = Column('TaskDefId_id',Integer,ForeignKey("katalyonode_resourcedef.id"))
   PresentationId = Column('PresentationId_id',Integer,ForeignKey("katalyonode_presentationelements.id"))
   InterfacePresentationId = Column('InterfacePresentationId_id',Integer,ForeignKey("katalyonode_presentationelements.id"))
   FieldName = Column('FieldName',String(255),nullable=True)
   FormType = Column('FormType',String(1),nullable=True)
   Params = Column('Params',Text,nullable=True)
   Status = Column('Status',Boolean,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class TaskOutcomesHeadSa(Base):
   __tablename__ = 'katalyonode_taskoutcomeshead'
   id = Column('id', Integer, primary_key=True)
   OutcomeName = Column('OutcomeName',String(255),nullable=True)
   Active = Column('Active',Boolean,nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class TaskOutcomesDetailSa(Base):
   __tablename__ = 'katalyonode_taskoutcomesdetail'
   id = Column('id', Integer, primary_key=True)
   TaskOutcomeHeadId = Column('TaskOutcomeHeadId_id',Integer,ForeignKey("katalyonode_taskoutcomeshead.id"))
   Name = Column('Name',String(255),nullable=True)
   Active = Column('Active',Boolean,nullable=True)
   BtnClass = Column('BtnClass',String(255),nullable=True)
   Icon = Column('Icon',String(255),nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict

class TaskOutcomesDetailLangSa(Base):
   __tablename__ = 'katalyonode_taskoutcomesdetaillang'
   id = Column('id', Integer, primary_key=True)
   TaskOutcomeDetailId = Column('TaskOutcomeDetailId_id',Integer,ForeignKey("katalyonode_taskoutcomesdetail.id"))
   Lang = Column('Lang_id',Integer,ForeignKey("katalyonode_language.id"))
   DisplayName = Column('DisplayName',String(255),nullable=True)
   BtnName = Column('BtnName',String(255),nullable=True)
   CreatedBy = Column('CreatedBy_id',Integer,ForeignKey("auth_user.id")) 
   CreatedDateTime = Column('CreatedDateTime',DateTime)
   ChangedBy = Column('ChangedBy_id',Integer,ForeignKey("auth_user.id"),nullable=True)
   ChangedDateTime = Column('ChangedDateTime',DateTime,nullable=True)
   
   def _asdict(self):
      r_dict={}
      for c in self.__table__.columns:
         if c.name.endswith('_id'):
            col_name = c.name.replace('_id','')
            r_dict[col_name] = getattr(self, col_name)
         else:
            r_dict[c.name] = datetime_handler(getattr(self, c.name))   
      return r_dict