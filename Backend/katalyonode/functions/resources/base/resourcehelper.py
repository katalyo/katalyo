"""
Resource helper class

helper classes for resources

"""
import pdb
from katalyonode.models.models import TaskInitiationInstance,TaskExecutionInstances,UserExtendedProperties
from rest_framework import serializers
from katalyonode.functions.rdbms.rdbms import RdbmsClass
import json
from datetime import datetime,timezone
from katalyonode.settings import PUBLISH_DIR
from katalyonode.functions.rdbms.alchemymodels import AdminGroups,AdminUsers,auth_user_groups
from sqlalchemy import and_
rdbms_cls = RdbmsClass()

class ResourceHelperClass():
    def __init__(self):
        pass
    def get_resource_type(self, resource_id):
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+"resourcedef"
        column_prefix_resource = table_resource+'_'
        query_filter = [{'table':table_resource,'condition':{column_prefix_resource+'id':resource_id}}]
        
        table_resource_params= table_prefix+'resourceparams'
        column_prefix_params= table_resource_params+'_'

        join_params = [{'table':table_resource_params,'join':{column_prefix_resource+'ResourceType_id':column_prefix_params+'id'}}]
        
        #pdb.set_trace()
        resource_records = rdbms_cls.get_data_generic (table_resource,join_params,query_filter)

        resource_type_code = None
        if len(resource_records)>0:
            resource_type_code = resource_records[0]['katalyonode_resourceparams_ResourceCode']
            status=200
        else:
            status = 400

        return resource_type_code,status

    def get_published_resource(self,pub_resource_id):

        table_prefix = 'katalyonode_'
        table_resource = table_prefix+"publishedresources"
        column_prefix_resource = table_resource+'_'
        conditions={}
        conditions[column_prefix_resource+'id']=pub_resource_id
        conditions[column_prefix_resource+'Active']=True

        query_filter = [{'table':table_resource,'condition':conditions}]
        
        
        join_params = []
        published_records = rdbms_cls.get_data_generic (table_resource,join_params,query_filter)

        published_resources = []
        pr=None
        for pub in published_records:

            pr = {
            'id':pub[column_prefix_resource+'id'],
            'ResourceDefId':pub[column_prefix_resource+'ResourceId_id'],
            'PublishType':pub[column_prefix_resource+'PublishType'],
            'Form':json.loads(pub[column_prefix_resource+'Form']),
            'Definition':json.loads(pub[column_prefix_resource+'Definition']),
            'Description':pub[column_prefix_resource+'Description'],
            'Parameters':json.loads(pub[column_prefix_resource+'Parameters']),
            'Version':pub[column_prefix_resource+'Version']
            }

        return pr

    
    def get_published_resources_by_type(self,resource_type,org_id,lang,publish_type):

        
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+"resourcedef"
        column_prefix_resource = table_resource+'_'
        conditions = {}
        conditions[column_prefix_resource+'Organisation_id']=org_id
        query_filter = [{'table':table_resource,'condition':conditions}]

        #join resource params
        conditions = {}
        table_resource_params = table_prefix+"resourceparams"
        column_prefix_params = table_resource_params+'_'
        join_params = [{'table':table_resource_params,'join':{column_prefix_resource+'ResourceType_id':column_prefix_params+'id'}}]
        conditions[column_prefix_params+'ResourceCode']=resource_type
        query_filter.append({'table':table_resource_params,'condition':conditions})
        #join resourcedeflang
        conditions = {}
        table_resource_lang = table_prefix+"resourcedeflang"
        column_prefix_lang= table_resource_lang+'_'
        join_params.append({'table':table_resource_lang,'join':{column_prefix_resource+'id':column_prefix_lang+'ResourceDefId_id'}})
        conditions[column_prefix_lang+'Lang_id']=lang.id
        query_filter.append({'table':table_resource_lang,'condition':conditions})

        #join published resources
        conditions = {}
        table_resource_publish = table_prefix+"publishedresources"
        column_prefix_publish= table_resource_publish+'_'
        join_params.append({'table':table_resource_publish,'join':{column_prefix_resource+'id':column_prefix_publish+'ResourceId_id'}})        

        if (publish_type!='all'):
            conditions[column_prefix_publish+'PublishType'] = publish_type
        conditions[column_prefix_publish+'Active']=True
        
        query_filter.append({'table':table_resource_publish,'condition':conditions})
        
        
        published_records = rdbms_cls.get_data_generic (table_resource,join_params,query_filter)

        published_resources = []
        
        for pub in published_records:

            pr = {
            'id':pub[column_prefix_publish+'id'],
            'ResourceDefId':pub[column_prefix_resource+'id'],
            'PublishType':pub[column_prefix_publish+'PublishType'],
            'name':pub[column_prefix_lang+'Name']+' v'+str(pub[column_prefix_publish+'Version']),
            'Description':pub[column_prefix_publish+'Description'],
            #'Form':json.loads(pub[column_prefix_publish+'Form']),
            #'Parameters':json.loads(pub[column_prefix_publish+'Parameters']),
            #'Definition':json.loads(pub[column_prefix_publish+'Definition']),
            'Version':pub[column_prefix_publish+'Version']
            }
            published_resources.append(pr)

        return published_resources

    def get_published_resource_by_id(self,publish_id,org_id,lang):

        #published resources
        conditions = {}
        table_prefix='katalyonode_'
        table_resource_publish = table_prefix+"publishedresources"
        column_prefix_publish= table_resource_publish+'_'
        conditions[column_prefix_publish+'Active']=True
        conditions[column_prefix_publish+'id']=publish_id
        
        query_filter = [{'table':table_resource_publish,'condition':conditions}]
        
        join_params=[]
        
        published_records = rdbms_cls.get_data_generic (table_resource_publish,join_params,query_filter)
        
        for pub in published_records:

            pr = {
            'id':pub[column_prefix_publish+'id'],
            'ResourceDefId':pub[column_prefix_publish+'ResourceId_id'],
            'PublishType':pub[column_prefix_publish+'PublishType'],
            'Form':json.loads(pub[column_prefix_publish+'Form']),
            'Parameters':json.loads(pub[column_prefix_publish+'Parameters']),
            'Definition':json.loads(pub[column_prefix_publish+'Definition']),
            'Version':pub[column_prefix_publish+'Version']
            }

        return pr

    def get_published_resources(self,resource_id,org_id,publish_type):

        table_prefix = 'katalyonode_'
        table_resource = table_prefix+"publishedresources"
        column_prefix_resource = table_resource+'_'
        conditions={}
        conditions[column_prefix_resource+'ResourceId_id']=resource_id
        conditions[column_prefix_resource+'OrganisationId_id']=org_id
        conditions[column_prefix_resource+'Active']=True
        if (publish_type!='all'):
            conditions[column_prefix_resource+'PublishType'] = publish_type

        query_filter = [{'table':table_resource,'condition':conditions}]
        
        
        join_params = []
        published_records = rdbms_cls.get_data_generic (table_resource,join_params,query_filter)

        published_resources = []
        
        for pub in published_records:

            pr = {
            'id':pub[column_prefix_resource+'id'],
            'ResourceDefId':pub[column_prefix_resource+'ResourceId_id'],
            'PublishType':pub[column_prefix_resource+'PublishType'],
            #'Form':json.loads(pub[column_prefix_resource+'Form']),
            'Description':pub[column_prefix_resource+'Description'],
            #'Parameters':json.loads(pub[column_prefix_resource+'Parameters']),
            'Version':pub[column_prefix_resource+'Version']
            }
            published_resources.append(pr)

        return published_resources

    def get_published_resources_by_type_id(self,resource_id,publish_type,org_id,lang):

        
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+"resourcedef"
        column_prefix_resource = table_resource+'_'
        conditions = {}
        conditions[column_prefix_resource+'Organisation_id']=org_id
        query_filter = [{'table':table_resource,'condition':conditions}]
        join_params=[]
        #join resourcedeflang
        conditions = {}
        table_resource_lang = table_prefix+"resourcedeflang"
        column_prefix_lang= table_resource_lang+'_'
        join_params.append({'table':table_resource_lang,'join':{column_prefix_resource+'id':column_prefix_lang+'ResourceDefId_id'}})
        conditions[column_prefix_lang+'Lang_id']=lang.id
        query_filter.append({'table':table_resource_lang,'condition':conditions})

        #join published resources
        conditions = {}
        table_resource_publish = table_prefix+"publishedresources"
        column_prefix_publish= table_resource_publish+'_'

        join_params.append({'table':table_resource_publish,'join':{column_prefix_resource+'id':column_prefix_publish+'ResourceId_id'}})        
        conditions[column_prefix_publish+'ResourceId_id']=resource_id
        
        if (publish_type!='all'):
            conditions[column_prefix_publish+'PublishType'] = publish_type
        conditions[column_prefix_publish+'Active']=True
        
        query_filter.append({'table':table_resource_publish,'condition':conditions})
        
        
        published_records = rdbms_cls.get_data_generic (table_resource,join_params,query_filter)

        published_resources = []
        
        for pub in published_records:

            pr = {
            'id':pub[column_prefix_publish+'id'],
            'ResourceDefId':pub[column_prefix_resource+'id'],
            'PublishType':pub[column_prefix_publish+'PublishType'],
            'name':pub[column_prefix_lang+'Name']+' v'+str(pub[column_prefix_publish+'Version']),
            #'Form':json.loads(pub[column_prefix_publish+'Form']),
            #'Parameters':json.loads(pub[column_prefix_publish+'Parameters']),
            #'Definition':json.loads(pub[column_prefix_publish+'Definition']),
            'Version':pub[column_prefix_publish+'Version']
            }
            published_resources.append(pr)

        return published_resources

    def publish_resource(self,resource_to_publish):
        
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+'publishedresources'
        column_prefix = table_resource+'_'
        

        
        published_resources_data = {column_prefix+'Active':True,column_prefix+'Form':json.dumps(resource_to_publish['Form']),column_prefix+'Parameters':json.dumps(resource_to_publish['Parameters']),column_prefix+'Description':resource_to_publish['Description'],column_prefix+'Definition':json.dumps(resource_to_publish['Definition']),
            column_prefix+'ResourceId_id' : resource_to_publish['ResourceId'],column_prefix+'PublishType' : resource_to_publish['PublishType'],column_prefix+'Version' : resource_to_publish['Version'],column_prefix+'OrganisationId_id':resource_to_publish['OrganisationId']}

        if 'id' in resource_to_publish:
            resource_publish_id = resource_to_publish['id']
        else:
            resource_publish_id = None

        if resource_publish_id is None:
            published_resources_data.update({column_prefix+'CreatedBy_id' : resource_to_publish['UserId'], column_prefix+'CreatedDateTime':datetime.now(timezone.utc)})
            publishId = rdbms_cls.insert_data_generic (table_resource,published_resources_data)
            
        else:
            published_resources_data.update({column_prefix+'ChangedBy_id' : resource_to_publish['UserId'], column_prefix+'ChangedDateTime':datetime.now(timezone.utc)})
            
            publishId = rdbms_cls.update_data_generic (table_resource,resource_publish_id,published_resources_data)
        
        #save everything to file here
        pub_file = PUBLISH_DIR+resource_to_publish["ResourceId"]+".json"
        
        with open(pub_file, "w") as outfile:
            json_object = json.dumps(resource_to_publish['Form'])
            outfile.write(json_object)    
        return publishId
    
    # NOT USED !!!
    def is_version_published(self,resource_id,version):   # NOT USED !!!
       
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+'publishedresources'
        column_prefix = table_resource+'_'
        conditions={}
        conditions[column_prefix+'ResourceModelDefinitionId_id']=resource_id
        conditions[column_prefix+'Version']=version
        query_filter=[{'table':table_resource,'condition':conditions}]
        join_params=None
        #pdb.set_trace()
       
        published_records = rdbms_cls.get_data_generic (table_resource,join_params,query_filter)
        
        if len(published_records)>0:
            return True 
        else:
            return False    
        
    
    def get_lang_org(self,user):

        #org_id=None 
        #lang = None 
        #ueps = UserExtendedProperties.objects.filter(UserId=user)
        if user is None:
            pdb.set_trace()
        uepSA,langSA,orgSA = rdbms_cls.get_user_extended_prop(user.id)

        return uepSA.Organisation, langSA
        #for uep in ueps:
        #    org_id=uep.Organisation_id
        #    lang = uep.Lang
            
        #return org_id,lang

    def get_status_code_by_name (self,code_detail_name,code_head_name,lang_id=None):
      
      table_code_detail_name ='katalyonode_codesdetail'
      table_code_head_name = 'katalyonode_codeshead'
      table_code_detaillang_name ='katalyonode_codesdetaillang'
      query_filter=[]
      join_params=[]
      condition_head = {}

      condition_head[table_code_head_name+'_CodeHeadName'] = code_head_name
      condition_detail = {}

      condition_detail[table_code_detail_name+'_Name'] = code_detail_name

      condition_detaillang = {}
      condition_detaillang[table_code_detaillang_name+'_Lang_id'] = lang_id

      query_filter.append({'table':table_code_head_name,'condition':condition_head})

      query_filter.append({'table':table_code_detail_name,'condition':condition_detail})

      query_filter.append({'table':table_code_detaillang_name,'condition':condition_detaillang})

      join_params.append({'table':table_code_head_name,'join':{table_code_detail_name+'_CodesHeadId_id':table_code_head_name+'_id'}})
      join_params.append({'table':table_code_detaillang_name,'join':{table_code_detail_name+'_id':table_code_detaillang_name+'_CodesDetailId_id'}})      
      
      cds = rdbms_cls.get_data_generic (table_code_detail_name,join_params,query_filter)

      #cds = CodesDetail.objects.filter(CodesHeadId__CodeHeadName=code_head_name,Name=code_detail_name)
      code_value = None

      for cd in cds:
        code_value = cd['katalyonode_codesdetaillang_Value']
      
      return code_value

    def get_user_object_by_id(self,user_id):

        table_prefix = 'auth_'
        table_user = table_prefix+'user'
        column_prefix = table_user+'_'

        query_filter = [{'table':table_user,'condition':{column_prefix+'id':user_id}}]

        join_params = None
        
        user_records = rdbms_cls.get_data_generic (table_user,join_params,query_filter)
        user_object={'id':user_id}
        
        if len(user_records)==1:
            user_object['first_name']=user_records[0][column_prefix+'first_name']
            user_object['last_name']=user_records[0][column_prefix+'last_name']
            user_object['username']=user_records[0][column_prefix+'username']

        return user_object

    def IsUserResourceAdmin(self,resource_id,user_id,session):

        #pdb.set_trace()
        is_user = session.query(AdminUsers).filter(AdminUsers.user_id==user_id,AdminUsers.resourcedef_id==resource_id).all()

        if len(is_user)>0:
            return True

        is_group = session.query(AdminGroups,auth_user_groups)\
        .join(auth_user_groups, and_(auth_user_groups.user==user_id, AdminGroups.group_id == auth_user_groups.group))\
        .filter(AdminGroups.resourcedef_id==resource_id).all()

        if len(is_group)>0:
            return True

        return False

        
class PresentationElementClass():
    def __init__(self,pe=None,new_item=None):

        if pe is not None:
            self.set_value(pe,new_item)
        else:
            self.id = None
            self.PresentationId = None
            self.ElementTyNone = None
            self.SizeX = None
            self.Status = None
            self.ShowField = None
            self.Required = None
            self.ReadOnly = None
            self.ComponentName = None
            self.ComponentReference = None
            self.DirectiveName = None
            self.AngularDirectiveName = None
            self.Parameters = None
            self.PopulateMethod =None
            self.PopulateTyNone = None
            self.SrcWidgetId = None
            self.AutoSearch=None
            self.ShowSearchForm =None
            self.Related = None
            self.PopulateTyNoneParent = None
            self.PopulateParentMethod = None
            self.SrcParentWidgetId =None
            self.HasParentDataset = None
            self.ParentDatasetId = None
            self.InlineTask =None
            self.ShowHeader =None
            self.UseDefault = None
            self.DefaultValue = None
            self.UseSystemDate = None
            self.UpdateWithDefault = None
            self.NoUpdate=None
            self.Multiline=None
            self.UniqueId =None
            self.Search = None
            self.DatasetMapping = None
            self.PresentationTyNone = None
            self.NameField = None
            self.BackgroundColor = None
            self.TextAlign=None
            self.FontSize = None
            self.VersionFrom = None
            self.VersionTo = None
            self.OverrideTyNone = None
            self.Order = None
            self.ResourceTyNone = None
            self.ContainerElement = None
            self.UuidRef = None
            if new_item is not None:
                self.id = new_item['PresentationId']
    
    def set_value(self,pe,new_item):

        if pe is not None:
            self.id = pe.id
            self.PresentationId = pe.id
            self.ElementType = pe.ElementType
            #self.Facss = None #pe.Facss
            self.SizeX = pe.SizeX
            self.Status = pe.Status
            self.ShowField = pe.ShowField
            self.Required = pe.Required
            self.ReadOnly = pe.ReadOnly
            self.ComponentName = pe.DirectiveName
            self.ComponentReference = pe.AngularDirectiveName
            self.DirectiveName = pe.DirectiveName
            self.AngularDirectiveName = pe.AngularDirectiveName
            #if type(pe.Parameters)==str:
            #    self.Parameters = json.loads(pe.Parameters)
            #else:
            self.Parameters = pe.Parameters
            self.PopulateMethod =pe.PopulateMethod
            self.PopulateType = pe.PopulateType
            self.SrcWidgetId = pe.SrcWidgetId
            self.AutoSearch=pe.AutoSearch
            self.ShowSearchForm =pe.ShowSearchForm
            self.Related = pe.Related
            self.PopulateTypeParent = pe.PopulateTypeParent
            self.PopulateParentMethod = pe.PopulateParentMethod
            self.SrcParentWidgetId =pe.SrcParentWidgetId
            self.HasParentDataset = pe.HasParentDataset
            self.ParentDatasetId = pe.ParentDatasetId
            self.InlineTask =pe.InlineTask
            self.ShowHeader =pe.ShowHeader
            self.UseDefault = pe.UseDefault
            self.DefaultValue = pe.DefaultValue
            self.UseSystemDate = pe.UseSystemDate
            self.UpdateWithDefault = pe.UpdateWithDefault
            self.NoUpdate=pe.NoUpdate
            self.Multiline=pe.Multiline
            self.UniqueId =pe.UniqueId
            self.Search = pe.Search
            self.DatasetMapping = pe.DatasetMapping
            self.PresentationType = pe.PresentationType
            self.NameField = pe.NameField
            self.BackgroundColor = pe.BackgroundColor if hasattr(pe, 'BackgroundColor') else None
            self.TextAlign=pe.TextAlign if hasattr(pe, 'TextAlign') else None
            self.FontSize = pe.FontSize if hasattr(pe, 'FontSize') else None
            self.VersionFrom = pe.VersionFrom
            self.VersionTo = pe.VersionTo
            self.OverrideType = pe.OverrideType
            self.Order = pe.Order
            self.ResourceType = pe.ResourceType
            self.ContainerElement = pe.ContainerElement
            self.UuidRef = pe.UuidRef

        elif new_item is not None:
            self.id = new_item['PresentationId']
        else:
            self.id = None
        

class PeLangClass():
    def __init__(self,pe_lang=None):
        if pe_lang is not None:
            self.set_value(pe_lang)
        else:
            self.id = pe_lang
            self.PresentationLangId = pe_lang
            self.Lang = pe_lang
            self.Name = pe_lang
            self.Label = pe_lang
            self.Placeholder = pe_lang
            self.Subject = pe_lang
            self.Description = pe_lang
            self.ErrorMsg = pe_lang
            self.SearchText = pe_lang
            self.ValueLength = pe_lang
            self.LabelLength = pe_lang
            self.FormDef = pe_lang
            self.FormDefSearch = pe_lang
            self.ResponseMsg = pe_lang
            self.GridState = pe_lang
            self.ParametersLang = pe_lang
    
    def set_value(self,pe_lang):
        self.id = pe_lang.id
        self.PresentationLangId = pe_lang.id
        self.Lang = pe_lang.Lang
        self.Name = pe_lang.Name
        self.Label = pe_lang.Label
        self.Placeholder = pe_lang.Label
        self.Subject = pe_lang.Subject
        self.Description = pe_lang.Description
        self.ErrorMsg = pe_lang.ErrorMsg
        self.SearchText = pe_lang.SearchText
        self.ValueLength = pe_lang.ValueLength
        self.LabelLength = pe_lang.LabelLength
        self.FormDef = pe_lang.FormDef
        self.FormDefSearch = pe_lang.FormDefSearch
        self.ResponseMsg = pe_lang.ResponseMsg
        self.GridState = pe_lang.GridState
        #if type(pe_lang.ParametersLang)==str:
        #    self.ParametersLang = json.loads(pe_lang.ParametersLang)
        #else:
        self.ParametersLang = pe_lang.ParametersLang
        

class ResourceModelDefinitionClass():
    def __init__(self,resource_model_def=None):

        if resource_model_def is not None:
            self.set_value(resource_model_def)
        else:
            self.id = resource_model_def
            self.ResourceDefId = resource_model_def
            self.FieldType = resource_model_def
            self.GenericType = resource_model_def
            self.Dbtype = resource_model_def
            self.FieldName = resource_model_def
            self.ResourceModelStatus = resource_model_def
            self.CodeId = resource_model_def
            self.RequiredField = resource_model_def
            self.ExternalKey =resource_model_def
            self.Unique =resource_model_def
            self.Related = resource_model_def
            self.MaxLength = resource_model_def
            self.InitialVersion = resource_model_def
            self.Version = resource_model_def
            self.Deployed = resource_model_def
            self.DeployedVersion = resource_model_def
            self.DeployAction = resource_model_def
            self.DeployDate = resource_model_def
            self.DeployedBy = resource_model_def
            self.ResourceLink = resource_model_def
    
    def set_value(self,resource_model_def):
        

            self.id = resource_model_def.id
            self.ResourceDefId = resource_model_def.ResourceDefId
            self.FieldType = resource_model_def.FieldType
            self.GenericType = resource_model_def.GenericType
            self.Dbtype = resource_model_def.Dbtype
            self.FieldName = resource_model_def.FieldName
            if hasattr(resource_model_def,'ResourceModelStatus'):
                self.ResourceModelStatus = resource_model_def.ResourceModelStatus #resource_model_def.Status
            if hasattr(resource_model_def,'Status'):
                self.Status = resource_model_def.Status #resource_model_def.Status
                self.ResourceModelStatus = resource_model_def.Status #resource_model_def.Status      
            self.CodeId = resource_model_def.CodeId
            self.RequiredField = resource_model_def.RequiredField
            self.ExternalKey =resource_model_def.ExternalKey
            self.Unique =resource_model_def.Unique
            #self.Related = resource_model_def.Related_id
            self.Related = resource_model_def.Related
            self.MaxLength = resource_model_def.MaxLength
            self.InitialVersion = resource_model_def.InitialVersion
            self.Version = resource_model_def.Version
            self.Deployed = resource_model_def.Deployed
            self.DeployedVersion = resource_model_def.DeployedVersion
            self.DeployAction = resource_model_def.DeployAction
            self.DeployDate = resource_model_def.DeployDate
            self.DeployedBy = resource_model_def.DeployedBy
            self.ResourceLink = resource_model_def.ResourceLink


def TaskInitiationInstanceJson (taskInitiate):

    tInitList = []
    if type(taskInitiate)!=list:
        taskInitiate = [taskInitiate]

    for init in taskInitiate:
        tInit = {

            'id':init.id,
            'TaskDefId':init.TaskDefId, #object maybe needed
            'InitiatorId':init.InitiatorId, #object maybe needed
            'Status':init.Status, #object maybe needed
            'TaskType':init.TaskType, #object maybe needed
            'NoOfExeInstances':init.NoOfExeInstances,
            'ExeInstanceLimit':init.ExeInstanceLimit,
            'PreviousTaskId':init.PreviousTaskId,
            'TransactionId':init.TransactionId

        }
        tInitList.append(tInit)


    return tInitList

def TaskExecutionInstancesJson (taskExecute):
  
    tExeList = []
    if type(taskExecute)!=list:
        taskExecute = [taskExecute]

    for exe in taskExecute:
        tExe = {

            'id':exe.id,
            'TaskDefId':exe.TaskDefId, #object maybe needed
            'TaskInitiateId':exe.TaskInitiateId, #object maybe needed
            'ActualOwner':exe.ActualOwner, #object maybe needed
            'Status':exe.Status, #object maybe needed
            'TaskOutcomeCodeItemId':exe.TaskOutcomeCodeItemId, #object maybe needed
            'FormData':exe.FormData

        }
        tExeList.append(tExe)


    return tExeList
