"""
Resource base class

this is the base class that all resources must inherit

"""
import pdb
from django.core.validators import ValidationError
from django.db.models import Prefetch,Max
from katalyonode.models.models import ResourceDef,ResourceDefLang,PresentationElements,PresentationElementsLang,CodesDetail,CodesDetailLang,Language
import importlib
from datetime import datetime,timezone
import json
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass

import os
from katalyonode.settings import PUBLISH_DIR

from sqlalchemy.sql import func
from sqlalchemy import and_, update
from katalyonode.functions.rdbms.alchemymodels import auth_group, auth_user,  AdminGroups, AdminUsers, PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa,ResourceDefSa,ResourceDefLangSa
from katalyonode.functions.resources.base.resourcehelper import PresentationElementClass,PeLangClass,ResourceModelDefinitionClass



rdbms_cls = RdbmsClass()

class ResourceBaseClass:
    
    def __init__(self,_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):
                     
        self.id = _id
        self.RdlId = None
        self.ResourceType = _resource_type
        self.Name = _name
        self.Subject = _subject
        self.Description = _description
        #self.Status = _status
        self.Active = _status
        self.Language = _language
        self.OrganisationId = _org_id
        self.Version = _version
        self.Parameters = _parameters
        self.ParametersLang = {}
        self.ResourceType= None
        self.ResourceSubType= _resource_sub_type
        self.ResourceName = ''
        self.ResourceCode = ''
        self.ResourceExtended = {}
        self.ResourceForm = None
        self.ResourceFormJson = []
        self.NewResourceForm = None
        self.ExtendedForm = False
        self.User = _user
        self.PresentationElements = None
        self.FormType = None
        self.CountChanges = 0
        self.NewVersion = None
        self.CurrentVersion = None
        self.CurrentIndex = 0
        self.TabIndex = 0
        self.FormOverrideType='n'
        self.FormResourceType='o'
        self.FormVersion=0
        self.DefaultForm=None
        self.Published = False
        self.SyncDb=False
        self.TableCreated=False
        self.IsBlockchain=False
        self.Private = None
        self.ExcludeFromVersionUpdate = []
        self.NewFormItems = {}
        self.GenerateCss = True
        self.rdbms_cls = rdbms_cls
        self.CreatedDateTime = None
        self.ChangedDateTime = None
        self.CreatedBy = None
        self.ChangedBy = None
        self.CustomFields = None
        self.AdminGroups = []
        self.AdminUsers = []
        self.Session = None #self.get_session()
        #set default language
        self.rdbms_cls.SetLanguage(self.Language)
              
        if self.Parameters is not None:
            if 'externalDataSource' in self.Parameters:
                if self.Parameters['externalDataSource']:
                    self.CustomFields = self.Parameters['externalDataSource']
            if 'resource_admin_groups' in self.Parameters:
                self.AdminGroups = self.Parameters.get('resource_admin_groups', [])
            if 'resource_admins' in self.Parameters:
                self.AdminUsers = self.Parameters.get('resource_admins', [])
    
    def sync_alchemy_table(self,version,history):
            
            #sync_alchemy_table
            #check if you need to create or update dataset
            table_name,is_external = self.rdbms_cls.get_table_name(self.id,self.OrganisationId)
    
            if history:
                table_exists = self.rdbms_cls.inspect_if_table_exists(table_name+'h')
            else:
                table_exists = self.rdbms_cls.inspect_if_table_exists(table_name)
            
            if not table_exists:
                deploy = self.rdbms_cls.create_alchemy_table(table_name,self.id,self.ResourceCode,self.OrganisationId,version,self.User,history)
                self.update_resource_table_created()
            else:
                deploy = self.rdbms_cls.update_alchemy_table(self.id,self.User,self.OrganisationId,table_name,version,history) 
                if not self.TableCreated:
                    self.update_resource_table_created()
            return deploy

    def update_resource_table_created(self):
        
        table_prefix = 'katalyonode_'
        table_resourcedef = table_prefix+'resourcedef'
        column_prefix = table_resourcedef+'_'
        update_data = {column_prefix+'TableCreated':True}
        rdef_record = self.rdbms_cls.update_data_generic (table_resourcedef,self.id,update_data)

    def set_new_resource_data(self,resource_data):
        
        self.ResourceType = resource_data.get('ResourceType',None)
        self.Private = resource_data.get('Private', False)
        self.ParametersLang = resource_data.get('ParametersLang', {})
        if self.ResourceType is not None:
            self.ResourceName = self.ResourceType.get('ResourceName',None)  
            self.ResourceCode = self.ResourceType.get('ResourceCode',None)

    def get_session(self):
        self.Session = self.rdbms_cls.Session()
        return self.Session
        
    def set_session(self,session):
        self.Session = session

    def get_extended_resource_data(self):

        self.ResourceExtended = {'DefaultForm':self.DefaultForm}
        return self.ResourceExtended


    def get_definition(self):

        #retrieves resource definition from database
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+'resourcedef'
        column_prefix = table_resource+'_'
        
        table_resource_lang = table_prefix+'resourcedeflang'
        column_prefix_lang = table_resource_lang+'_'

        table_resource_type = table_prefix+'resourceparams'
        column_prefix_type = table_resource_type+'_'
        
        table_resourcedef_admingroups = table_prefix +'resourcedef_AdminGroups'
        column_prefix_groups =  table_resourcedef_admingroups+'_'
        
        #pdb.set_trace()
          
        query_filter = [{'table':table_resource,'condition':{column_prefix+'id':self.id}},
        {'table':table_resource_lang,'condition':{column_prefix_lang+'Lang_id':self.Language.id}}]

        join_params = [{'table':table_resource_lang,'join':{column_prefix+'id':column_prefix_lang+'ResourceDefId_id'}},
            {'table':table_resource_type,'join':{column_prefix+'ResourceType_id':column_prefix_type+'id'}}]
        
        resource_records = self.rdbms_cls.get_data_generic (table_resource,join_params,query_filter)
        
        #pdb.set_trace()
        if len(resource_records)>0:
            self.ResourceType = {
                    'id': resource_records[0][column_prefix_type+'id'],
                    'ResourceName': resource_records[0][column_prefix_type+'ResourceName'],
                    'ResourceCode': resource_records[0][column_prefix_type+'ResourceCode']
              }
                    
            helper = ResourceHelperClass()

            self.ResourceCode = resource_records[0][column_prefix_type+'ResourceCode']
            self.ResourceName = resource_records[0][column_prefix_type+'ResourceName']
            self.ResourceSubType = resource_records[0][column_prefix+'ResourceSubType']
            if resource_records[0][column_prefix_type+'DefaultForm'] is not None:
                self.DefaultForm = json.loads(resource_records[0][column_prefix_type+'DefaultForm'])

            self.get_extended_resource_data()
            self.Private = resource_records[0][column_prefix+'Private']
            self.IsBlockchain = resource_records[0][column_prefix+'IsBlockchain']
            self.OrganisationId = resource_records[0][column_prefix+'Organisation_id']
            self.TableCreated = resource_records[0][column_prefix+'TableCreated']

            self.Active = resource_records[0][column_prefix+'Active']
            self.Version = resource_records[0][column_prefix+'Version']
            self.CreatedDateTime = resource_records[0][column_prefix+'CreatedDateTime']
            self.ChangedDateTime = resource_records[0][column_prefix+'ChangedDateTime']
            self.CreatedBy = helper.get_user_object_by_id(resource_records[0][column_prefix+'CreatedBy_id'])
            self.ChangedBy = helper.get_user_object_by_id(resource_records[0][column_prefix+'ChangedBy_id'])
            params = resource_records[0][column_prefix+'Parameters']
            if params is not None:
                #pdb.set_trace()
                if type(params)  not in [dict,list]:
                    self.Parameters = json.loads(params)
                else:
                    self.Parameters = params
            else:
                self.Parameters = {}
            
            self.RdlId = resource_records[0][column_prefix_lang+'id']
            self.Name = resource_records[0][column_prefix_lang+'Name']
            self.Subject = resource_records[0][column_prefix_lang+'Subject']
            self.Description = resource_records[0][column_prefix_lang+'Description']
            
            params_lang = resource_records[0][column_prefix_lang+'ParametersLang']            
            if params_lang is not None:
                if type(params_lang)  not in [dict,list]:
                    self.ParametersLang = json.loads(params_lang)
                else:
                    self.ParametersLang = params_lang
            else:
                self.ParametersLang = {}


            #TODO-DONE - replace with SQLAlchemy
           
            #agroups = ResourceDef.objects.filter(id=self.id).first().AdminGroups.all()
            #ausers = ResourceDef.objects.filter(id=self.id).first().AdminUsers.all()
            
            session = rdbms_cls.Session()           
            with session.begin():  
                agroupsSA = session.query(auth_group).join(AdminGroups,auth_group.id==AdminGroups.group_id).filter(AdminGroups.resourcedef_id==self.id ).all()
                ausersSA = session.query(auth_user).join(AdminUsers,auth_user.id==AdminUsers.user_id).filter(AdminUsers.resourcedef_id==self.id ).all()
         
                #self.AdminGroups = []
                #self.AdminUsers = []  
                self.AdminGroups = []
                self.AdminUsers = []  

                if agroupsSA:
                    for g in agroupsSA:
                            self.AdminGroups.append( {'id':g.id,'name':g.name} )
                if ausersSA:
                    for u in ausersSA:
                            self.AdminUsers.append( {'id':u.id, 'name': u.first_name+' '+ u.last_name +' - '+ u.username} )
            
            #if agroups: 
            #       for g in agroups:
            #            self.AdminGroups.append( {'id':g.id,'name':g.name} )
            #if ausers:  
            #       for u in ausers:
            #            self.AdminUsers.append( {'id':u.id,'name':u.first_name+' '+u.last_name+' - '+u.username} )
                                   
            self.CustomFields = False
            if self.Parameters is not None:
                if 'externalDataSource' in self.Parameters:
                    if self.Parameters['externalDataSource']:
                        self.CustomFields = self.Parameters['externalDataSource']

    def save_definition(self):

        params = self.Parameters
        paramsLang = self.ParametersLang
        table_prefix = 'katalyonode_'
        table_resource = table_prefix+'resourcedef'
        column_prefix = table_resource+'_'
        if self.ResourceSubType is None or  self.ResourceSubType=='':
            self.ResourceSubType='standard'
            
        resource_def_data = {column_prefix+'Active':self.Active,column_prefix+'Private':self.Private,column_prefix+'ResourceType_id':self.ResourceType['id'],column_prefix+'ResourceSubType':self.ResourceSubType,column_prefix+'InitialVersion':1,column_prefix+'Version':1,column_prefix+'Parameters' : json.dumps(self.Parameters),                                                 column_prefix+'CreatedBy_id' : self.User.id, column_prefix+'CreatedDateTime':datetime.now(timezone.utc),column_prefix+'Organisation_id':self.OrganisationId,column_prefix+'TableCreated':False,column_prefix+'FormDefined':False}

        table_resource_lang = table_prefix+'resourcedeflang'
        column_prefix_lang = table_resource_lang+'_'
        resource_def_lang_data = {
                column_prefix_lang+'Lang_id':self.Language.id,column_prefix_lang+'Name':self.Name,column_prefix_lang+'Subject':self.Subject,column_prefix_lang+'Description':self.Description,column_prefix_lang+'ParametersLang' : json.dumps(paramsLang),
                column_prefix_lang+'CreatedBy_id' : self.User.id, column_prefix_lang+'CreatedDateTime':datetime.now(timezone.utc),column_prefix_lang+'Version':1,column_prefix_lang+'InitialVersion':1,column_prefix_lang+'InitiateForm':False,
                column_prefix_lang+'PresentationDef':'{}',column_prefix_lang+'PresentationDefExe':'{}',column_prefix_lang+'ExecuteForm':False,column_prefix_lang+'ResourceDefId_id':self.id}

        table_resource_type = table_prefix+'resourceparams'
        column_prefix_type = table_resource_type+'_'
        query_filter = [  {'table':table_resource_lang,'condition':{column_prefix_lang+'Lang_id':self.Language.id}},                  {'table':table_resource_lang,'condition':{column_prefix_lang+'ResourceDefId_id':self.id}}        ]
        #join sa resource_def
        join_params = [
            {'table':table_resource,'join':{column_prefix_lang+'ResourceDefId_id':column_prefix+'id'}}  ]
            
        if self.id is None:
            resourceId = self.rdbms_cls.insert_data_generic (table_resource,resource_def_data)
            self.id=resourceId[0]
            resource_def_lang_data[column_prefix_lang+'ResourceDefId_id']=self.id
            resourceLang = self.rdbms_cls.insert_data_generic (table_resource_lang,resource_def_lang_data)
        else:
            resourceId = self.rdbms_cls.update_data_generic (table_resource,self.id,resource_def_data)
            resourceLangRecord = self.rdbms_cls.get_data_generic (table_resource_lang, join_params, query_filter)
            
            if len(resourceLangRecord)>0:
                resourceLangId = resourceLangRecord[0][column_prefix_lang+'id']
                resourceLang = self.rdbms_cls.update_data_generic (table_resource_lang,resourceLangId,resource_def_lang_data)
            else:
                resourceLang = self.rdbms_cls.insert_data_generic (table_resource_lang,resource_def_lang_data)
        
                  
        groups_ids = []  
        users_ids = []          
        #users_ids.append(self.User.id)   #always grant current user  
        if self.AdminUsers is not None:
            for u in self.AdminUsers:
                users_ids.append(u['id'])    
        if self.AdminGroups is not None:
            for g in self.AdminGroups:
                groups_ids.append(g['id'])  
        
        if len(users_ids)==0:
            users_ids.append(self.User.id) 
       
        #rdefs = ResourceDef.objects.filter(id=self.id)
        #for rdef in rdefs:
        #          rdef.AdminUsers.set(users_ids)
        #          rdef.AdminGroups.set(groups_ids)
        #          rdef.save() 
        
        session=rdbms_cls.Session()
        with session.begin():
            #rdefs = ResourceDefSa.filter(ResourceDefSa.id==self.id).first()
            musers_del = session.query(AdminUsers).filter(AdminUsers.resourcedef_id==self.id).delete()
            mgroups_del = session.query(AdminGroups).filter(AdminGroups.resourcedef_id==self.id).delete()
            for ui in users_ids:
                    musers = AdminUsers(resourcedef_id=self.id, user_id=ui)
                    session.add(musers)
            for gi in groups_ids:
                    mgroups = AdminGroups(resourcedef_id=self.id, group_id = gi)
                    session.add(mgroups)          
               

    def get_form_definition(self,form_type,version=0):
        
        # MIGOR START
            
            self.set_form_type(form_type)
            
            if version==0:
                #pdb.set_trace()
                version_max_SA = self.Session.query(func.max(PresentationElementsSa.VersionTo) )\
                .filter(PresentationElementsSa.ResourceDefId==self.id,PresentationElementsSa.FormType==form_type,PresentationElementsSa.ResourceType=='o', PresentationElementsSa.Status=='Active').first()
                
                if version_max_SA is not None:
                    version = version_max_SA[0]             
                    if version is None:
                        version = 0
            """
            if version==0: #TODO -  MAKNUTI KAD PRORADI SQL ALCHEMY
                pdb.set_trace()
                #version_max = PresentationElements.objects.filter(ResourceDefId_id=self.id,FormType=form_type,ResourceType='o',Status='Active').aggregate(Max('VersionTo'))
                if version_max is not None:
                    if 'VersionTo__max' in version_max:
                        version = version_max.get('VersionTo__max')
                        if version is None:
                            version = 0
            """       

            if not self.ExtendedForm:
                PE=PresentationElementsSa
                PELANG=PresentationElementsLangSa
                queryset_SA = self.Session.query(PE, PELANG,ResourceModelDefinitionSa)\
                    .outerjoin(PELANG,and_(PE.id==PELANG.PresentationElementsId, PELANG.Lang==self.Language.id))\
                    .outerjoin(ResourceModelDefinitionSa, PE.ResourceModelDefinitionId==ResourceModelDefinitionSa.id)\
                    .filter( PE.ResourceDefId==self.id,  PE.FormType==form_type,  PE.Status=='Active' , PE.VersionFrom <= version, PE.VersionTo >= version)\
                    .order_by(PE.Order).all()
  
            """
            if not self.ExtendedForm: #TODO -  MAKNUTI KAD PRORADI SQL ALCHEMY  
                #queryset = PresentationElements.objects\
                #.filter( ResourceDefId_id = self.id, FormType=form_type, Status='Active', ResourceType='o',VersionFrom__lte = version, VersionTo__gte = version)\
                #.prefetch_related(Prefetch('presentationelementslang_set', queryset=PresentationElementsLang.objects.filter(Lang=self.Language)))\
                #.select_related('ResourceModelDefinitionId').order_by('Order')      
            else:    
                #pdb.set_trace()
                eQueryset = PresentationElements.objects.filter(ResourceDefId_id=resource_id,TaskDefId_id=task_def_id,FormType=form_type,VersionFrom__lte=version,VersionTo__gte=version,
                        OverrideType=override_type, ParentWidgetId_id=parent_id,ResourceType='t',Status='Active').select_related('OriginalPresentationElementId','ResourceModelDefinitionId').prefetch_related(Prefetch('presentationelementslang_set',
                            #queryset=PresentationElementsLang.objects.filter(Lang=self.Language)))#.order_by('Order')
                oPresentationIds = []
             
                extendedRow=None
                extendedColumn=None
            
                for element in eQueryset:
                  if element.OriginalPresentationElementId_id is not None:
                    oPresentationIds.append(element.OriginalPresentationElementId_id)
                  if element.ElementType=='row':
                    extendedRow = element.id
                  if element.ElementType=='gcol':
                    extendedColumn = element.id
            
            
                #oQueryset = PresentationElements.objects.filter(ResourceDefId_id=resource_id,Status='Active',ResourceType='o').exclude(id__in=oPresentationIds).prefetch_related('presentationelementslang',Prefetch('presentationelementslang_set',
                        #queryset=PresentationElementsLang.objects.filter(Lang=self.Language))).select_related('presentationelementslang','ResourceModelDefinitionId')#.order_by('Order')
              
                queryset = eQueryset | oQueryset
            
                queryset=queryset.order_by('Order')
                if oQueryset.count()>0:
                    for item in queryset:
                
                        if item.ContainerId_id in oPresentationIds and item.ResourceType=='o':
                  
                            if item.ElementType=='gcol':
                                item.ContainerId_id = extendedRow
                            else:
                                item.ContainerId_id = extendedColumn 
       
            """
            #self.PresentationElements = queryset
            self.PresentationElements = queryset_SA #MIGOR
            #self.PresentationElements = []
            self.PresentationElementsObjects=[]
            for item in queryset_SA:   
                pe=PresentationElementClass(item[0],None)
                pl =PeLangClass(item[1])
                rm = ResourceModelDefinitionClass(item[2])
                self.PresentationElementsObjects.append( (pe,pl,rm) )
   
    def myprint(self,d,acc):
            for k, v in d.items():
                if isinstance(v, dict):
                    myprint(v,acc)
                else:
                   if k == "Related":
                        acc.push(v)
                
    
    
    def generate_form(self):
    
        #pdb.set_trace() #performanse   rows=self.PresentationElements.filter(ElementType='row').all()     
        #rows = self.PresentationElements.filter(PresentationElementsSa.ElementType == 'row')
         
        tmp=[]
        for item in self.PresentationElementsObjects:
            if item[0].ElementType =='row':
                #for item in rows:
                    if item[0].ContainerElement is None or item[0].ContainerElement==item[0].UuidRef:
                        if item[0].VersionTo>  self.FormVersion:
                            self.FormVersion = item[0].VersionTo          
                        try:
                            module_path = 'katalyonode.functions.widgets.form.'+ item[0].DirectiveName    
                            module_ref = importlib.import_module(module_path)
                            widget_class = getattr(module_ref, item[0].DirectiveName)
                            widget_object = widget_class(item,self)
                            
                            if widget_object is not None:
                                    widget_object.generate_form() #resourceBASE
                                    widgetJson= widget_object.get_definition_json()
                                    self.ResourceFormJson.append(widgetJson)

                        except ModuleNotFoundError:
                            raise ValidationError("ModuleNotFoundError -->" + module_path)

            
    def save_form_definition(self,new_form):

        self.NewResourceForm = new_form
        #go through the form and save changes
        css_style=''
        for item in self.NewResourceForm:
            try:
                self.increment_index()
                module_path = 'katalyonode.functions.widgets.form.'+item['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, item['DirectiveName'])
                widget_object = widget_class(None,self,None,item)
                if widget_object is not None:
                    #try:
                        widget_object.get_element_definition()
                        widget_object.save_definition()
                    #except AttributeError:
                        #raise ValidationError("AttributeError -->" + str(vars(widget_object)))

            except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError -->" + module_path)
        
    def get_form_json(self):
        
        return self.ResourceFormJson

    def update_form_version(self):
        
        #PresentationElements.objects.filter(ResourceDefId_id=self.id, VersionTo=self.CurrentVersion)\
        #.exclude(id__in=self.ExcludeFromVersionUpdate).update(VersionTo=self.NewVersion,ChangedBy=self.User,ChangedDateTime=datetime.now(timezone.utc))
        session = self.Session
        peFields = session.execute( update(PresentationElementsSa)\
                    .where( PresentationElementsSa.ResourceDefId==self.id, PresentationElementsSa.VersionTo==self.CurrentVersion, PresentationElementsSa.id.not_in(self.ExcludeFromVersionUpdate))\
                    .values(VersionTo=self.NewVersion, ChangedBy=self.User.id, ChangedDateTime=datetime.now(timezone.utc)) )
                      
          
    def get_definition_json(self):
        
        print( type(self.Language))
        if type(self.Language)==Language: 
            lang = {'id':self.Language.id,'Code':self.Language.Code,'Name':self.Language.Name}
        else:
            #lang = self.Language # standard 
            lang = json.dumps(self.Language._asdict()) # when sql alchemy
            
        admingroups = []
        adminusers = []
        #pdb.set_trace()
        if self.AdminGroups is not None:
            for g in self.AdminGroups:
                admingroups.append( {'id':g['id'],'name':g['name']} )
        
        if self.AdminUsers is not None:
            for user in self.AdminUsers:
                adminusers.append( {'id':user['id'],'name':user['name']} )
        
        
        if self.Parameters is not None:
            #pdb.set_trace()
            if type(self.Parameters)  not in [dict,list]:
                params = json.loads(self.Parameters)
            else:
                params = self.Parameters
        else:
            params = {}

        if self.ParametersLang is not None:
            if type(self.ParametersLang)  not in [dict,list]:
                params_lang = json.loads(self.ParametersLang)
            else:
                params_lang = self.ParametersLang
        else:
            params_lang = {}

        resource_def={
            'id':self.id,
            'rdlId':self.RdlId,
            'ResourceType': self.ResourceType,
            'Name': self.Name,
            'Subject': self.Subject,
            'Description': self.Description,
            'Active': self.Active,
            'Language': lang,
            'AdminGroups': admingroups,
            'AdminUsers' : adminusers,
            'OrganisationId': self.OrganisationId,
            'IsBlockchain': self.IsBlockchain,
            'Version': self.Version,
            'Parameters': params ,
            'ParametersLang': params_lang ,
            'ResourceName': self.ResourceName,
            'ResourceCode': self.ResourceCode,
            'ResourceSubType': self.ResourceSubType,
            'ResourceExtended': self.ResourceExtended,
            'FormVersion': self.FormVersion,
            'SyncDb': self.SyncDb,
            'Published': self.Published,
            'CreatedDateTime': self.CreatedDateTime,
            'ChangedDateTime': self.ChangedDateTime,
            'CreatedBy': self.CreatedBy,
            'ChangedBy': self.ChangedBy
        }

        return resource_def

    def set_resource_published(self,publish_type):
        
        table_prefix = 'katalyonode_'
        table_publish = table_prefix+'publishedresources'
        column_prefix = table_publish+'_'
         
        query_filter = [{'table':table_publish,'condition':{column_prefix+'ResourceId_id':self.id,column_prefix+'Version':self.FormVersion,column_prefix+'PublishType':publish_type}}]
        
        join_params=[]

        publish_records = self.rdbms_cls.get_data_generic (table_publish,join_params,query_filter)
        #resource_records = self.rdbms_cls.get_data_generic (,resource_def_filter)
        if len(publish_records)>0:
            self.Published = True
        else:
            self.Published = False

    def create_widget_css(self,form_def,uuidRefParent):
    
        #go through the form and generate_css
        css_style=''
        for item in form_def:
            try:
                module_path = 'katalyonode.functions.widgets.form.'+item['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, item['DirectiveName'])

                widget_object = widget_class(None,self,None,item)
               
                if widget_object is not None:
                   
                    css_style+=widget_object.create_widget_css(uuidRefParent)
                   
            except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError in create_widget_css-->" + module_path)

        return css_style

    def create_resource_css(self):
    
        #go through the form and generate_css
        css_style=''
        for item in self.NewResourceForm:
            try:
                module_path = 'katalyonode.functions.widgets.form.'+item['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, item['DirectiveName'])

                widget_object = widget_class(None,self,None,item)
               
                if widget_object is not None:
                    css_style+=widget_object.create_widget_css(str(self.id))
                    
            except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError in create_resource_css --> " + module_path)
        
        pub_file=os.path.join(PUBLISH_DIR,str(self.id)+'.css')

        with open(pub_file, "w+") as outfile:
            outfile.write(css_style) 

    def get_saved_form(self):
        
        return self.NewResourceForm

    def set_form_type(self,form_type):
         self.FormType = form_type

    def set_new_version(self,new_version):
         self.NewVersion = new_version

    def set_current_version(self,current_version):
         self.CurrentVersion = current_version

    def set_override_type(self,override_type):
         self.FormOverrideType = override_type

    def set_resource_type(self,resource_type):
         self.FormResourceType = resource_type

    def add_change(self):
        self.CountChanges+=1

    def increment_index(self):
        self.CurrentIndex+=1

    def get_tab_index(self):
        return self.TabIndex