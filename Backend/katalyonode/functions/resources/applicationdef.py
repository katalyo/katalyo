import pdb
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass
#from katalyonode.models.models import ResourceLinks,UserExtendedProperties
from katalyonode.functions.rdbms.alchemymodels import ResourceLinksSa,UserExtendedPropertiesSa

class application (ResourceBaseClass):
    
        def __init__(self,_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):
            super().__init__(_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
            self.rlinks = []
        
        def get_extended_resource_data(self):
                
               
                session = self.rdbms_cls.Session()
                with session.begin():
                       
                        rlinks = session.query(ResourceLinksSa).filter(ResourceLinksSa.Resource1==self.id,ResourceLinksSa.LinkType.in_(['app-menu','default-page'])).all()
                        
                        rlink_list=[]
                        for rl in rlinks:
                                rlink = {'id':rl.id,'Resource1':rl.Resource1,'Resource2':rl.Resource2,'LinkType':rl.LinkType,'Parameters':rl.Parameters}
                                rlink_list.append(rlink)
                        self.rlinks = rlink_list
                        self.ResourceExtended = rlink_list
                return self.ResourceExtended

        def save_definition(self):

                super().save_definition()
               
                session = self.rdbms_cls.Session()
                with session.begin():
                        uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.UserId==self.User.id).all()
                        
                        for uep_rec in uep:
                                if uep_rec.Params is not None:
                                        if 'Onboarding' in uep_rec.Params:
                                                if uep_rec.Params['Onboarding']!='Completed':
                                                        uep_params=uep_rec.Params
                                                        uep_params['OnboardingAppId'] = self.id
                                                        uep_params['Onboarding'] = 'AppSetup'
                                                        uep_rec.Params = uep_params