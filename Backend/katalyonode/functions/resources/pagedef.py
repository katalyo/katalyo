import pdb
from katalyonode.functions.rdbms.alchemymodels import ResourceLinksSa,PagesSa,codesdetail
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass
from katalyonode.functions.resources.pagemenudef import pagemenu
from datetime import datetime,timezone
import json
from sqlalchemy import and_


class page (ResourceBaseClass):
    
        def __init__(self,_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):
            super().__init__(_id,_resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
            self.page = {}
            self.page_menu = []

        def get_extended_resource_data(self):
                

                #rlinks = ResourceLinks.objects.filter(Resource1=self.id,LinkType__in=['page-menu'])
                session = self.Session
                 
                rlinks = session.query(ResourceLinksSa).filter(ResourceLinksSa.Resource1==self.id,ResourceLinksSa.LinkType.in_(['page-menu'])).all()
                
                rlink_list=[]
                page_menu_id = None
                for rl in rlinks:
                        rlink = {'id':rl.id,'Resource1':rl.Resource1_id,'Resource2':rl.Resource2_id,'LinkType':rl.LinkType,'Parameters':rl.Parameters}
                        rlink_list.append(rlink)
                        page_menu_id = rl.Resource2_id
        
         
                self.rlinks = rlink_list
                self.ResourceExtended={}

                self.ResourceExtended['rlinks'] = rlink_list

                #page = Pages.objects.filter(ResourceDefId_id=self.id)#.prefetch_related("Users","Groups")
                page = session.query(PagesSa,codesdetail).outerjoin(codesdetail,and_(PagesSa.PageType==codesdetail.id,codesdetail.Lang==self.Language.id)).filter(PagesSa.ResourceDefId==self.id).all()
                if len(page)>0:
                        #page_type = GetCodesDetailByIdLangSerializer(page[0].PageType)
                        page_type = {'id':page[0][1].id,'name':page[0][1].Value}
                       
                        page_dict = {'id':page[0][0].id,'Target':page[0][0].Target,'Params':page[0][0].Params,'PageType':page_type,'Users':[],'Groups':[]}
                        self.ResourceExtended['PageType'] =page_type
                        self.ResourceExtended['Target'] =page[0][0].Target
                        self.ResourceExtended['id'] =page[0][0].id
                        
                else:
                        page_dict={}
                        
                self.page = page_dict
                
                self.ResourceExtended['page'] = self.page

                #get side menu
                menu_object = pagemenu(page_menu_id, 'pagmenu','','','',True,self.User,self.Language,self.OrganisationId,{},None) #TODO  - replace None,None
                menu_object.set_session(session)
                queryset=menu_object.get_menus(None,None,page_menu_id,1,True)
                menus = []
                for item in queryset:
                    itemDict={}
                    for col in item.keys():
                        val = getattr(item, col)
                        try:
                            val = json.loads(val)
                        except:
                            pass
                        
                        value = {col:val}
                        itemDict.update(value)
                    self.page_menu.append(itemDict)

                
                self.ResourceExtended['page_menu'] = self.page_menu
                self.ResourceExtended['DefaultForm']=self.DefaultForm
                
                return self.ResourceExtended

        def save_definition_not_used(self):

                super().save_definition()

                #save page here

                table_pages='katalyonode_pages'

                tbl_pages = self.rdbms_cls.get_table(table_pages)
               
                
                session = self.ParentResource.Session
                
          
                pages = session.query(tbl_pages).filter(tbl_pages.c[table_pages+'_ResourceDefId_id']==self.id).all()
                if len(pages)>0:
                        pages[0].Target = self.ResourceExtended['Target']
               
                else:
               
                        #pdb.set_trace()
                        pages_dict = {'Target':self.ResourceExtended['Target'],'Params':self.ResourceExtended['Params'],'PageType_id':self.ResourceExtended['PageType'],'CreatedBy_id':self.User.id,'CreatedDateTime':datetime.now(timezone.utc)}

                        session.query(tbl_pages).insert(**pages_dict)