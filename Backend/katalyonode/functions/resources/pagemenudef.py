import pdb
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass
from rest_framework.response import Response
import katalyonode.functions.rdbms
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from sqlalchemy import Table#, Column, Integer,Boolean, String, Text, ForeignKey,Numeric,Float,Date,Time,DateTime, MetaData
#from katalyonode.models.models import Pages,Menus
from katalyonode.functions.rdbms.alchemymodels import MenusSa,MenusUsersSa,MenusGroupsSa
#from django.utils import timezone
import json

class pagemenu (ResourceBaseClass):
    
        def __init__(self,_id,  _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):
            super().__init__(_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
            self.PageMenu = None
     

        def get_extended_resource_data(self):

                queryset=self.get_menus(None,None,self.id,1)
                #pageMenu = []
                menus = []
                for item in queryset:
                    itemDict={}
                    for col in item.keys():
                        val = getattr(item, col)
                        
                        try:
                            val = json.loads(val)   
                        except:
                            pass
                        #pdb.set_trace()
                        value = {col:val}
                        itemDict.update(value)
                    menus.append(itemDict)
            
               
                    
                
                    #for mi in menus:
                                      
                        #if mi['katalyonode_menus_MenuPosition'] in ['sidemenu']:
                            #pageMenu.append(mi)  
                self.PageMenu = menus
                self.ResourceExtended={'pageMenu':self.PageMenu}
                return self.ResourceExtended
        
        def Get_Children_Menus(self,mi,allMenus):
                children_menus = []
                for menu in allMenus:
                        if menu['katalyonode_menus_MenuPosition']  not in ['navbar','navbarright'] and menu['katalyonode_menus_Navbar'] is not None and menu['katalyonode_menus_Navbar']==mi['katalyonode_menus_id']:
                                children_menus.append(menu)
                return children_menus
        
        def get_menus(self,user,group_ids,menuId,type,only_active=False):
    
                objects = None
                #pdb.set_trace() #get_menus
                table_name='katalyonode_menus'
                table_users='katalyonode_menus_Users'
                table_groups='katalyonode_menus_Groups'
                table_pages='katalyonode_pages'
                table_rlinks='katalyonode_resourcelinks'
                
                tbl = self.rdbms_cls.get_table(table_name)
                tbl_users = self.rdbms_cls.get_table(table_users)
                tbl_groups = self.rdbms_cls.get_table(table_groups)
                tbl_pages = self.rdbms_cls.get_table(table_pages)
                tbl_rlinks = self.rdbms_cls.get_table(table_rlinks)
               
                #menus_users

                menus = self.Session.query(tbl,tbl_pages).filter(tbl.c[table_name+'_ResourceDefId_id']==menuId)
                 
                if only_active:
                    menus = menus.filter(tbl.c[table_name+'_Active']==only_active)

                menus = menus.outerjoin(tbl_pages,tbl_pages.c[table_pages+'_id'] ==tbl.c[table_name+'_TargetPage_id'])  

                if (type==0):
                    menu_users = self.Session.query(tbl_users.c[table_users+'_menus_id']).filter(tbl_users.c[table_users+'_user_id']==user.id)#.subquery()
                    menu_groups = self.Session.query(tbl_groups.c[table_groups+'_menus_id']).filter(tbl_groups.c[table_groups+'_group_id'].in_(group_ids))#.subquery()
                    all_menus = menu_users.union(menu_groups)
                    menus = menus.filter(tbl.c[table_name+'_Active']==True,tbl.c[table_name+'_id'].in_(all_menus))
                #elif (type==1):
                    #menus = menus.filter(tbl.c[table_name+'_ResourceDefId_id']==menuId)
                
                menus = menus.order_by(tbl.c[table_name+'_Order'],tbl.c[table_name+'_id'])
                results = menus.all()
                
            
                
                return  results
            
        def get_users_groups_menus(self,menuId,language):
                
                objects = None
                
                auth_users='auth_user'
                auth_groups='auth_group'
                table_users='katalyonode_menus_Users'
                table_groups='katalyonode_menus_Groups' 
              
                tbl_users = Table(table_users, self.rdbms_cls.Base.metadata, autoload_with=self.rdbms_cls.engine)
                tbl_groups = Table(table_groups, self.rdbms_cls.Base.metadata, autoload_with=self.rdbms_cls.engine)
                tbl_auth_users = Table(auth_users, self.rdbms_cls.Base.metadata, autoload_with=self.rdbms_cls.engine)
                tbl_auth_groups = Table(auth_groups, self.rdbms_cls.Base.metadata, autoload_with=self.rdbms_cls.engine)
                
                    
                menu_users = self.Session.query(tbl_users,tbl_auth_users).join(tbl_auth_users,tbl_users.c[table_users+'_user_id']==tbl_auth_users.c[auth_users+'_id']).filter(tbl_users.c[table_users+'_menus_id']==menuId).all()
                menu_groups = self.Session.query(tbl_groups,tbl_auth_groups).join(tbl_auth_groups,tbl_groups.c[table_groups+'_group_id']==tbl_auth_groups.c[auth_groups+'_id']).filter(tbl_groups.c[table_groups+'_menus_id']==menuId).all()
                    
               
                return  menu_users ,menu_groups
            
            
        def get_pages_for_user(self,user):
                
                return []

        def prepare_save(self,pageMenuItems ):
                
               self.PageMenu = pageMenuItems


        def save_full_menu(self):

                counter = 0
                ret_status=200
                msg=None
              
                for item in self.PageMenu:
                    counter=counter+1
                    msg,ret_status = self.process_menu_item(item,self.PageMenu,'sidemenu',counter)
                    if ret_status!=200:
                        break

                return msg,ret_status

        def process_menu_item(self,menuItem,allItems,menuPosition,order,containerId=None):
  
                menu_id = menuItem['katalyonode_menus_id'] if 'katalyonode_menus_id' in  menuItem  else None
                users = menuItem['Users']  if 'Users' in menuItem else []
                groups = menuItem['Groups']  if 'Groups' in menuItem else [] 
                users_ids = []
                groups_ids = []
                target_page = None
                target = None
                active=True
                icon=None
                menu_type=1
                delete_item=False
                target_page = None
                for u in users:
                        users_ids.append(u['id'])

                users_ids.append(self.User.id)  #always grant current user  

                for g in groups:
                        groups_ids.append(g['id'])


                if 'katalyonode_menus_TargetPage_id' in menuItem:
                        target_page = menuItem['katalyonode_menus_TargetPage_id']
                elif 'TargetPage' in menuItem:
                        if 'id' in menuItem['TargetPage']:
                                target_page = menuItem['TargetPage']['id']

                if 'katalyonode_menus_Target_id' in menuItem:
                        target = menuItem['katalyonode_menus_Target_id']
                elif 'Target' in menuItem:
                        if 'id' in menuItem['Target']:
                                target = menuItem['Target']['id']

                if 'katalyonode_menus_Active' in menuItem:
                        active = menuItem['katalyonode_menus_Active']

                if 'katalyonode_menus_Icon' in menuItem:
                        icon = menuItem['katalyonode_menus_Icon']

                if 'katalyonode_menus_MenuType' in menuItem:
                        menu_type = menuItem['katalyonode_menus_MenuType']

                if 'deleteItem' in menuItem:
                        delete_item = menuItem['deleteItem']
               
                if menu_type==1:
                        table_pages='katalyonode_pages'
                        tbl_pages = self.rdbms_cls.get_table(table_pages)
                        
                        pages = self.Session.query(tbl_pages).filter(tbl_pages.c[table_pages+'_ResourceDefId_id']==target).all()
                        
                        if len(pages)>0:
                               target_page = pages[0].katalyonode_pages_id
                        else:
                               return 'Error saving menu '+str(menu_id)+' -> page record not found in '+menuPosition+' ('+str(target_page)+')',400

                params = {'pageid':target}    
                ret_status = 200
                msg=''
                menu=None
                if menu_id is None:
  
                        menu = MenusSa(Name = menuItem['katalyonode_menus_Name'],
                        MenuType= menu_type,
                        MenuPosition= menuPosition,
                        TargetPage=target_page,
                        Target=target,
                        Params= params,
                        Icon= icon,
                        Order= order,
                        ParentItem=containerId,
                        ResourceDefId=self.id,
                        Active= active,
                        CreatedBy= self.User.id,
                        CreatedDateTime= datetime.now(timezone.utc))

                        self.Session.add(menu)
                        self.Session.flush()
                        if menu_type==2 or menu_type==3:
                            containerId = menu.id
                        else:
                            containerId=None

                        #menu.Users.set(users_ids)
                        for ui in users_ids:
                            musers = MenusUsersSa(MenusId=menu.id,UserId=ui)
                            self.Session.add(musers)
                        #menu.Groups.set(groups_ids)
                        for gi in groups_ids:
                            mgroups = MenusGroupsSa(MenusId=menu.id,GroupId=gi)
                            self.Session.add(mgroups)
                        #menu.save()
                elif menu_id is not None:

                        #menus = Menus.objects.filter(id=menu_id)
                        menus = self.Session.query(MenusSa).filter(MenusSa.id ==menu_id).all()
                    
                        if len(menus)==0:
                            msg='Menu with id = '+str(menu_id)+' is not found'
                            ret_status=400  
                        else:
                            
                            if delete_item:
                                for menu_del in menus:
                                    menu_del.delete()

                            else:
                                for menu in menus:
                                    if menu_type==2 or menu_type==3:
                                        containerId = menu.id
                                    else:
                                        containerId=None
                                    menu.Name = menuItem['katalyonode_menus_Name']
                                    menu.MenuType = menu_type
                                    menu.TargetPage =target_page
                                    menu.Target =target
                                    menu.MenuPosition = menuPosition
                                    menu.Params = params
                                    menu.ParentItem = containerId
                                    menu.Icon = icon
                                    #menu.Users.set(users_ids)
                                    #menu.Groups.set(groups_ids)
                                    menu.Order = order
                                    menu.Active = active
                                    menu.ChangedBy = self.User.id
                                    menu.ChangedDateTime =datetime.now(timezone.utc)
                                    musers_del = self.Session.query(MenusUsersSa).filter(MenusUsersSa.MenusId==menu.id).delete()
                                    mgroups_del = self.Session.query(MenusGroupsSa).filter(MenusGroupsSa.MenusId==menu.id).delete()
                                    for ui in users_ids:
                                        musers = MenusUsersSa(MenusId=menu.id,UserId=ui)
                                        self.Session.add(musers)
                                    #menu.Groups.set(groups_ids)
                                    for gi in groups_ids:
                                        mgroups = MenusGroupsSa(MenusId=menu.id,GroupId=gi)
                                        self.Session.add(mgroups)
                                        #menu.save()
                counter = 0

                if 'layout' in menuItem and ret_status==200:
                        
                        for mi in menuItem['layout']:
                                counter=counter+1
                                menu_position_side='sidemenu'
                                msg,ret_status=self.process_menu_item(mi, menuItem['layout'],menu_position_side,counter,containerId)
                                if ret_status!=200:
                                        break
                        return msg,ret_status
                else:
                        return msg,ret_status