######################## integration class ##########################
###############################################################################

import pdb
from django.utils import timezone
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass


class integration (ResourceBaseClass):
    
    def __init__(self,_id,  _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):

        super().__init__(_id,_resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
      