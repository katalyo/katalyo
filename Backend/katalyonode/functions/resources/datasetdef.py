######################## dataset class ##########################
###############################################################################

#from django.core.validators import ValidationError
#from django.db import IntegrityError, transaction
#from django.db.models import Prefetch
#from katalyonode.models.models import ResourceModelDefinition,PresentationElements,PresentationElementsHistory,PresentationElementsLang,ResourceDef,ErrorLog
#from katalyonode.models.models import PublishedResources,TaskInitiationInstanceToResource,TaskExecutionInstanceToResource,PresentationElements,TaskExecutionInstances
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa,ResourceDefSa,ResourceDefLangSa
from katalyonode.functions.rdbms.alchemymodels import TaskInitiationInstanceSa,TaskExecutionInstancesSa,TaskExecutionInstanceToResourceSa
import pdb
from sqlalchemy import and_,func
from django.utils import timezone
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass,ResourceHelperClass
import json
import importlib

class dataset (ResourceBaseClass):
    
    def __init__(self,_id,  _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):

        super().__init__(_id,  _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
        self.SyncDb=True
        self.DatabaseTableDefinition=[]
        self.GenerateCss = False

    def sync_alchemy_table(self,version,history):
            
            #sync_alchemy_table
            #check if you need to create or update dataset
            table_name,is_external = self.rdbms_cls.get_table_name(self.id,self.OrganisationId)
    
            if history:
                table_exists = self.rdbms_cls.inspect_if_table_exists(table_name+'h')
            else:
                table_exists = self.rdbms_cls.inspect_if_table_exists(table_name)
            
            if not table_exists:
                deploy = self.rdbms_cls.create_alchemy_table(table_name,self.id,self.ResourceCode,self.OrganisationId,version,self.User,history)
                self.update_dataset_table_created()
            else:
                deploy = self.rdbms_cls.update_alchemy_table(self.id,self.User,self.OrganisationId,table_name,version,history) 
                if not self.TableCreated:
                    self.update_dataset_table_created()
            return deploy

    def update_dataset_table_created(self):
        
        table_prefix = 'katalyonode_'
        table_resourcedef = table_prefix+'resourcedef'
        column_prefix = table_resourcedef+'_'
        update_data = {column_prefix+'TableCreated':True}
        rdef_record = self.rdbms_cls.update_data_generic (table_resourcedef,self.id,update_data)

    def get_definition_json(self):
        
        
        resource_def = super().get_definition_json()
        resource_def['TableCreated']= self.TableCreated

        return resource_def

    def get_db_definition(self,form_type,version=0):
        
        if len(self.DatabaseTableDefinition)==0:

            if self.PresentationElements is None:
                self.get_form_definition(form_type,version)
               
                for item in self.PresentationElements:
                    #pdb.set_trace()
                    if item[0].ResourceModelDefinitionId is not None:

                        try:
                            #pdb.set_trace()
                            module_path = 'katalyonode.functions.widgets.form.'+item[0].DirectiveName
                            module_ref = importlib.import_module(module_path)
                            widget_class = getattr(module_ref, item[0].DirectiveName)
                            widget_object = widget_class(item,self)
                                
                            if widget_object is not None:
                
                                widget_object.get_definition_json()
                                self.DatabaseTableDefinition.append(widget_object.get_definition_json())

                        except ModuleNotFoundError:
                            raise Exception("ModuleNotFoundError -->" + module_path)


    def get_db_definition_json(self):
        
        return self.DatabaseTableDefinition

    def generate_form(self):
        super().generate_form()
        self.set_resource_published('form')
    
    def process_form_data(self,form_data_list,rmds):

        rval = {}
        
        if type(form_data_list)==dict:
            form_data_list=[form_data_list]

        for form_data in form_data_list:

            for rmd in rmds:

                field_name = rmd['FieldName']
                widget_name = rmd['DirectiveName']
                item_id = None
                
                if 'ItemId' in rmd:
                    item_id=rmd['ItemId']
                if widget_name is not None and item_id is not None:
                  try:
                    module_path = 'katalyonode.functions.widgets.form.'+widget_name 
                    module_ref = importlib.import_module(module_path)
                    widget_class = getattr(module_ref, widget_name)
                    widget_object = widget_class(rmd,self)
                    if widget_object is not None:
                        rval = widget_object.process_widget(form_data,rmd)
                        form_data[field_name] = rval
                        
                  except ModuleNotFoundError:
                    raise Exception("ModuleNotFoundError --> " + module_path)
        return form_data_list
    
    def generate_query (self,widget_data):
  
        mainResource={}
        m2mData=[]
        
        if widget_data is not None:
          
            table_name,is_external=self.rdbms_cls.get_table_name(self.id,self.OrganisationId)
            if type(widget_data)==list:
                widget_data=widget_data[0]

            for field in widget_data.items():

                if type(field[1])==dict:
                    widget_name = field[1].get('widget_name',None)
                  
                    if widget_name is not None:

                      try:
                        module_path = 'katalyonode.functions.widgets.form.'+widget_name 
                        module_ref = importlib.import_module(module_path)
                        widget_class = getattr(module_ref, widget_name)
                        widget_object = widget_class(field[1],self)
                        if widget_object is not None:
                            rval = widget_object.generate_query(field[1])
                            if rval is not None:

                                if 'value' in rval:
                                    value = rval['value']
                                else:
                                    value = rval

                                if 'm2mData' in rval:
                                    if rval['m2mData'] is not None:
                                        if type(rval['m2mData'])==list and len(rval['m2mData'])>0:
                                            m2mData.append(rval['m2mData'])
                                        #elif type(rval['m2mData']) in [str,int]:
                                            #m2mData.append(rval['m2mData'])
                                
                                if value is not None:

                                    if is_external:
                                        mainResource[table_name+"_"+field[0]]=value
                                    else:
                                        mainResource[field[0]]=value
                                
                                #pdb.set_trace()

                      except ModuleNotFoundError:
                        raise Exception("ModuleNotFoundError --> " + module_path)

        return mainResource,m2mData    
    
    def get_columns_def (self,version):
          
        columns =  []
        columns_for_select = []
         
         
         #retrieves resource definition from database
        table_prefix = 'katalyonode_'
        table_presentationelements = table_prefix+'presentationelements'
        column_prefix = table_presentationelements+'_'
        
        table_presentationelements_lang = table_prefix+'presentationelementslang'
        column_prefix_lang = table_presentationelements_lang+'_'

        table_resource_model = table_prefix+'resourcemodeldefinition'
        column_prefix_model = table_resource_model+'_'
        #ResourceType='o',Status='Active',InitialVersion__lte=version,Version__lte=version
        query_filter = [{'table':table_presentationelements,'condition':{column_prefix+'ResourceDefId_id':self.id}},
                        {'table':table_presentationelements,'condition':{column_prefix+'ResourceType':'o'}},
                        {'table':table_presentationelements,'condition':{column_prefix+'Status':'Active'}},
                        {'table':table_presentationelements,'condition':{column_prefix+'VersionFrom':version},'operator':'<='},
                        {'table':table_presentationelements,'condition':{column_prefix+'VersionTo':version},'operator':'>='},
                        {'table':table_presentationelements_lang,'condition':{column_prefix_lang+'Lang_id':self.Language.id}}]

        join_params = [{'table':table_presentationelements_lang,'join':{column_prefix+'id':column_prefix_lang+'PresentationElementsId_id'}},
            {'table':table_resource_model,'join':{column_prefix+'ResourceModelDefinitionId_id':column_prefix_model+'id'}}]
        
        order_by = {column_prefix+'Order'}
        
        pe_records = self.rdbms_cls.get_data_generic (table_presentationelements,join_params,query_filter,order_by)
        main_table_name,is_external=self.rdbms_cls.get_table_name(self.id,self.OrganisationId)

        #checking for duplicates
        col_set = set()
        for item in pe_records:
            
            newcolumns = self.process_column(item,main_table_name,item[column_prefix+'id'],item[column_prefix+'ElementType'],item[column_prefix+'Parameters'],item[column_prefix_lang+'ParametersLang'],item[column_prefix_lang+'Label'],item[column_prefix_model+'FieldType'],item[column_prefix_model+'CodeId'],item[column_prefix+'NameField'],column_prefix_model,is_external)
            #pdb.set_trace()
            
            for col in newcolumns:
                name = col.get('name',None)
                if name not in col_set:
                    col_set.add(name)
                    columns.append(col)    

        return columns,columns_for_select

    def process_column(self,item,main_table_name,id_field,element_type,parameters,parameters_lang,label,field_type,code_id,name_field,column_prefix_model,is_external):

        newcolumns = []
        if field_type is None:
            field_type=''
        column_name = self.get_column_name(item,main_table_name,label,parameters_lang,field_type,column_prefix_model,is_external)
            
        core =  {'id':id_field,'visible': True, 'ElementType': element_type}
        #Phase 1 - fix this to work
        #ToDo Phase 2 - move all this to every widget object
        if (field_type=='codesdetail' and int(code_id)>0):
          newcolumns.append({ **core, 'name': column_name,'displayName': label,   'width': '15%' })
                        
                          
        elif field_type in ['user','group']:
         newcolumns.append({  **core, 'name': column_name,'displayName': label,    'width': '15%'})
                             
        elif field_type=='resourceid':
          newcolumns.append({  **core, 'name': column_name,'displayName': label,  'width': '15%'})
                             
        elif field_type=='date' or field_type=='time':
          #if item._prefetched_objects_cache['presentationelementslang_set'] is not None and len(item._prefetched_objects_cache['presentationelementslang_set'])>0:
            
            format1=''
            format2=''
                
            if parameters_lang is not None:
                if isinstance(parameters_lang, str):
                    parameters_lang = json.loads(parameters_lang)
                if 'format1' in parameters_lang:
                    
                    format1 = parameters_lang['format1']
                else:
                    format1=None
                if format1 is None:
                    format1=''
            
                if 'format2' in parameters_lang:
                    format2 = parameters_lang['format2']
                else:
                    format2=None
                if format2 is None:
                    format2=''
            
                
            if type(format1) is dict:
                if 'name' in format1:
                    format1 = format1['name']
            if type(format2) is dict:
                if 'name' in format2:
                    format2 = format2['name']
                
            format_date = format1+' '+format2

            if format_date.strip()=='':
                format_date='dd.MM.yyyy HH:mm:ss'
            
            newcolumns.append( {  **core, 'name': column_name,'displayName': label,
                           'enableCellEdit':True,'type':'date','cellFilter': "date:'"+format_date.strip()+"'",'width': '15%'})
          
        elif field_type.startswith('resource'):
            if field_type.startswith('resourcelink'):
                #for resource link
                newcolumns.append({  **core, 'name': column_name+'rec_id','displayName': label,'fieldType':item[column_prefix_model+'GenericType'],'relatedDatasetId':item[column_prefix_model+'Related_id'],'modelId':item[column_prefix_model+'id'],'width': '10%'})
                newcolumns.append({  **core, 'name': column_name+'def_id','visible':False,'displayName': label+'- Definition Id'})
                            #'cellTemplate':'<button type="button" class="btn btn-link" ng-click="grid.appScope.fd.getDatasetSingleRecord(row.entity.'+item.ResourceModelDefinitionId.FieldName+str(item.ResourceModelDefinitionId.id)+'def_id,row.entity.'+item.ResourceModelDefinitionId.FieldName+str(item.ResourceModelDefinitionId.id)+'rec_id)">{{row.entity.'+item.ResourceModelDefinitionId.FieldName+str(item.ResourceModelDefinitionId.id)+'rec_id}}</button>','width': '10%'})
         
            else:  #or dataset in dataset
                #get here from name fields
                if name_field is not None:# TODO da li je ovdje treba item ili peItem ????
                    if type(name_field)==str:
                        name_field=json.loads(name_field)
                    for fld in name_field:
                        #pdb.set_trace()
                        newcolumns.extend(self.process_column(fld,main_table_name,fld.get('id',None),fld.get('ElementType',None),fld.get('Parameters',{}),fld.get('ParametersLang',{}),fld.get('Label',None),fld.get('FieldType',None),fld.get('CodeId',0),fld.get('NameField',[]),'',False))

        elif field_type=='number':
            newcolumn= {  **core, 'name':column_name,'displayName': label  ,'width': '15%'}
                            #,'footerCellFilter': 'number:2'
            if parameters is not None and type(parameters) is not dict:
                parameters = json.loads(parameters)
            if 'NoOfDecimals' in parameters:
                newcolumn.update( {'NoOfDecimals': parameters['NoOfDecimals']})
            newcolumns.append(newcolumn)
                        
        else:
            newcolumn = { **core, 'name': column_name,'displayName': label ,'width': '15%'} 
            newcolumns.append(newcolumn)

        return newcolumns

    def get_column_name(self,item,table_name,label,parameters_lang,field_type,column_prefix_model,prependTable):
        
        name=''
        #table_prefix = 'katalyonode_'
        #table_presentationelements = table_prefix+'presentationelements'
        #column_prefix = table_presentationelements+'_'
        
        #table_presentationelements_lang = table_prefix+'presentationelementslang'
        #column_prefix_lang = table_presentationelements_lang+'_'

        #table_resource_model = table_prefix+'resourcemodeldefinition'
        #column_prefix_model = table_resource_model+'_'

        #MIGOR2020 novo da prikaze ID za ui-grid dataset u datasetu

        if (field_type=='codesdetail' and int(item[column_prefix_model+'CodeId'])>0):
            if type(item) is dict:
                name = item.get(column_prefix_model+'FieldName','')+'.name'
            else:
                if hasattr(item,column_prefix_model+'FieldName'):
                    name=item[column_prefix_model+'FieldName']+'.name'
                else:
                    name = None
            if prependTable and name is not None:
                name = table_name+"_"+name

        elif field_type in ['user','group']:
            if type(item) is dict:
                name = item.get(column_prefix_model+'FieldName','')+'.name'
            else:
                if hasattr(item,column_prefix_model+'FieldName'):
                    name=item[column_prefix_model+'FieldName']+'.name'
                else:
                    name = None
            if prependTable and name is not None:
                name = table_name+"_"+name
        elif field_type=='resourceid':
            
            if type(item) is dict:
                name = item.get(column_prefix_model+'FieldName',None)
            else:
                if hasattr(item,column_prefix_model+'FieldName'):
                    name=item[column_prefix_model+'FieldName']
                else:
                    name = None
                
        elif field_type=='date' or field_type=='time':
            
            if parameters_lang is not None:
                if isinstance(parameters_lang, str):
                    parameters_lang = json.loads(parameters_lang)
                
                if 'format1' in parameters_lang:
                    format1 = parameters_lang['format1']
                else:
                    format1=None
                if format1 is None:
                    format1=''
                if 'format2' in parameters_lang:
                    format2 = parameters_lang['format2']
                else:
                    format2=None
                if format2 is None:
                    format2=''
                if type(format1) is dict:
                    if 'name' in format1:
                        format1 = format1['name']
                if type(format2) is dict:
                    if 'name' in format2:
                        format2 = format2['name']
                format = format1+' '+format2
      
                if format.strip()=='':
                    format='dd.MM.yyyy HH:mm:ss:sss'
                  
                if type(item) is dict:
                    name = item.get(column_prefix_model+'FieldName',None)
                else:
                    if hasattr(item,column_prefix_model+'FieldName'):
                        name=item[column_prefix_model+'FieldName']
                    else:
                        name = None

                if prependTable and name is not None:
                  name = table_name+"_"+name  
        elif field_type=='number':
            if type(item) is dict:
                name = item.get(column_prefix_model+'FieldName',None)
            else:
                if hasattr(item,column_prefix_model+'FieldName'):
                    name=item[column_prefix_model+'FieldName']
                else:
                    name = None
            if prependTable and name is not None:
                name = table_name+"_"+name
        else:
            if type(item) is dict:
                name = item.get(column_prefix_model+'FieldName',None)
            else:
                if hasattr(item,column_prefix_model+'FieldName'):
                    name=item[column_prefix_model+'FieldName']
                else:
                    name = None

            if prependTable and name is not None:
                name = table_name+"_"+name
        
        generic_type = None
        max_length = None
        if type(item) is not dict:

            if hasattr(item,column_prefix_model+'GenericType'):
                generic_type=item[column_prefix_model+'GenericType']
            else:
                generic_type = None

            if hasattr(item,column_prefix_model+'MaxLength'):
                max_length=item[column_prefix_model+'MaxLength']

        if generic_type is not None and max_length is not None:
            if item[column_prefix_model+'GenericType']=='resource' and item[column_prefix_model+'MaxLength']==0:
                if type(item) is dict:
                    name = item.get(column_prefix_model+'FieldName',None)
                else:
                    if hasattr(item,column_prefix_model+'FieldName'):
                        name=item[column_prefix_model+'FieldName']
                    else:
                        name = None

                if prependTable and name is not None:
                    name = table_name+"_"+name
        return name

    
    def get_column_objects(self,item):
      
            columnObjects=[]
      
            if (item[column_prefix_model+'FieldType']=='codesdetail' and int(item[column_prefix_model+'CodeId'])>0):
              columnObjects.append( {'name': item[column_prefix_model+'FieldName']+'.name','displayName': item[column_prefix_lang+'Label'],
                               'width': '15%' })#,'cellTemplate':'{{grid.appScope.getCodeById(row.entity.Field28,12)}}' })
                             
                              
            elif item[column_prefix_model+'FieldType'] in ['user','group']:
              columnObjects.append( {'name': item[column_prefix_model+'FieldName']+'.name','displayName': item[column_prefix_lang+'Label'],
                             #  'cellFilter': 'getUser: row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)})
                                 'width': '15%'})
                                 #,'cellTemplate':'<span>{{grid.appScope.getUser(row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+')}}</span>'})
                                 
            elif item[column_prefix_model+'FieldType']=='resourceid':
              columnObjects.append( {'name': 'id','displayName': item[column_prefix_lang+'Label'],
                             #  'cellFilter': 'getUser: row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)})
                                 'width': '15%'})
            elif item[column_prefix_model+'FieldType']=='date' or item[column_prefix_model+'FieldType']=='time':
                if item[column_prefix_lang+'ParametersLang'] is not None:
                
                    if 'format1' in item[column_prefix_lang+'ParametersLang']: 
                      format1 = item[column_prefix_lang+'ParametersLang']['format1']
                    else:
                      format1=None
                    if format1 is None:
                      format1=''
                    if 'format2' in item[column_prefix_lang+'ParametersLang']: 
                      format2 = item[column_prefix_lang+'ParametersLang']['format2']
                    else:
                      format2=None
                      
                    if format2 is None:
                      format2=''
                      
                    if type(format1) is dict:
                      if 'name' in format1:
                          format1 = format1['name']
                    if type(format2) is dict:
                      if 'name' in format2:
                          format2 = format2['name']
                    format = format1+' '+format2
          
                    if format.strip()=='':
                      format='dd.MM.yyyy HH:mm:ss:sss'
                      
                    columnObjects.append( {'name': item[column_prefix_model+'FieldName'],'displayName': item[column_prefix_lang+'Label'],
                                     'enableCellEdit':True,'type':'date','cellFilter': "date:'"+format.strip()+"'",'width': '15%'})
              
            elif item[column_prefix_model+'FieldType'].startswith('resource'):
              if item[column_prefix_model+'FieldType'].startswith('resourcelink'):
                
                columnObjects.append({'name': item[column_prefix_model+'FieldName']+'rec_id','displayName': item[column_prefix_lang+'Label'],'width': '10%'})
                                       #'cellTemplate':'<button type="button" class="btn btn-link" ng-click="grid.appScope.fd.getDatasetSingleRecord(row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'def_id,row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'rec_id)">{{row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'rec_id}}</button>','width': '10%'})
                
                columnObjects.append({'name': item[column_prefix_model+'FieldName']+'def_id','visible':False,'displayName': item[column_prefix_lang+'Label']+'- Definition Id'})
              
              else:  
                if int(item[column_prefix_model+'MaxLength'])!=1:
                  #uzmi sva polja od related dataseta
                  columnObjects.append({'name': item[column_prefix_model+'FieldName']+'_id','displayName': item[column_prefix_lang+'Label'],'width': '15%'})
                                 #'cellTemplate':'<button type="button" class="btn btn-link" ng-click="grid.appScope.fd.getDatasetRelatedRecords('+str(item[column_prefix_model+'Related_id'])+',row.entity.id,'+str(item.ResourceModelDefinitionId.id)+')">View '+item._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label+'</button>','width': '15%'})
              
                else:
                  
                   
                  #columns.append({'name': item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'_id','displayName': item._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label,
                  #              'cellTemplate':'<button type="button" class="btn btn-link" ng-click="grid.appScope.getDatasetSingleRecord('+str(item[column_prefix_model+'Related_id'])+',row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'_id)">{{row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'_id'+'}}</button>','width': '10%'})
            
                  session = self.rdbms_cls.Session()
                  with session.begin():

                    #peRelated = PresentationElements.objects.select_related('ResourceModelDefinitionId').prefetch_related('presentationelementslang_set').filter(ResourceModelDefinitionId__ResourceDefId=item[column_prefix_model+'Related_id'],presentationelementslang__Lang_id=lang,Status='Active',ResourceType='o').distinct().order_by('Order')
                    peRelated = session.query(PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa).outerjoin(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==self.Language.id)).\
                                outerjoin(ResourceModelDefinitionSa,PresentationElementsSa.ResourceModelDefinitionId==ResourceModelDefinitionSa.id).filter(ResourceModelDefinitionSa.ResourceDefId==item[column_prefix_model+'Related_id'],PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceType=='o').order_by(PresentationElementsSa.Order).all()
                    for peItem in peRelated:
                          
                    #columnObjects.append({'name': GetColumnName(peItem,item._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label,item.ResourceModelDefinitionId_id),'displayName': item._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label+'_'+peItem._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label,
                     #            'cellTemplate':'<button type="button" class="btn btn-link" ng-click="grid.appScope.fd.getDatasetSingleRecord('+str(item[column_prefix_model+'Related_id'])+',row.entity.'+item[column_prefix_model+'FieldName']+str(item.ResourceModelDefinitionId.id)+'_id)">{{row.entity.'+GetColumnName(peItem,item._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label,item.ResourceModelDefinitionId_id)+'}}</button>','width': '10%'})
                        columnObjects.append({'name': self.get_column_name(peItem,item[column_prefix_lang+'Label'],item.ResourceModelDefinitionId_id),'displayName': item._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label+'_'+peItem._prefetched_objects_cache['presentationelementslang']._result_cache[0].Label,'width': '10%'})
            
            elif item[column_prefix_model+'FieldType']=='number':
              columnObjects.append({'name': item[column_prefix_model+'FieldName'],'displayName': item[column_prefix_lang+'Label']
                                #,'footerCellFilter': 'number:2'
                                ,'width': '15%'})
            else:
              columnObjects.append({'name': item[column_prefix_model+'FieldName'],'displayName': item[column_prefix_lang+'Label']
                                ,'width': '15%'})
            
            return columnObjects

    def  get_dataset_form_data(self,pElement,pes,src_task_instance_id,parent_id,populate_type,resource_record_id):
  
        #get source data
        rec_list = []
        l_prev_task_id=0
        recordId=0
        m2mIds = {}
        params = {}
        dictRecords = []
        params['user'] = self.User
        params['previous_task_id'] = 0
        
        session = self.rdbms_cls.Session()
        with session.begin():

            if resource_record_id>0:
                rec_list.append(resource_record_id)
            else:
                #TODO - add priority processing for multiple datasets (by dataset name)
                if populate_type[0:1]=='4':
                  populate_type='3e'
                tiitrs = None
                if populate_type in ['2i','3i']:
                  #tiitrs = TaskInitiationInstanceToResource.objects.filter(TaskInitiateId_id=src_task_instance_id,ResourceDefId=self.id)
                  tiitrs=session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.TaskInitiateId==src_task_instance_id,TaskInitiationInstanceToResourceSa.ResourceDefId==self.id).all()
                
                  if len(tiitrs)==0:
                    #teis = TaskExecutionInstances.objects.filter(TaskInitiateId_id=src_task_instance_id)
                    teis=session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskInitiateId==src_task_instance_id).all()
                
                    if len(teis)>0:
                      
                      #tiitrs = TaskExecutionInstanceToResource.objects.filter(TaskExecuteId=teis[0],ResourceDefId_id=self.id)
                      tiitrs=session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==teis[0].id,TaskExecutionInstanceToResourceSa.ResourceDefId==self.id).all()
                
                elif populate_type in ['2e','3e']:
                  #tiitrs = TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id=src_task_instance_id,ResourceDefId_id=self.id)
                  tiitrs=session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==src_task_instance_id,TaskExecutionInstanceToResourceSa.ResourceDefId==self.id).all()
                
                  if len(tiitrs)==0:
                    #get initiate task id
                    #teis = TaskExecutionInstances.objects.filter(id=src_task_instance_id)
                    teis = session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==src_task_instance_id).all()
                    if len(teis)>0:
                        
                      #tiitrs = TaskInitiationInstanceToResource.objects.filter(TaskInitiateId=teis[0].TaskInitiateId,ResourceDefId_id=self.id)
                      tiitrs=session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.TaskInitiateId==teis[0].TaskInitiateId,TaskInitiationInstanceToResourceSa.ResourceDefId==self.id).all()
                
                if tiitrs is not None and tiitrs.count()>0:
                  for tiitr in tiitrs:
                    rec_list.append(tiitr.ResourceId)
                
                else:
              
                  recDict = {}
                  
                  for pe in pes:
                    value=None
                
                    field_name=pe['FieldName']
                    
                    #process field by hooking up to custom funtion get_element_value
                    
                    #import module
                    try:
                      module_path = 'katalyonode.functions.widgets.form.'+pe['DirectiveName']
                      definitionFnModule =  importlib.import_module(module_path, pe['DirectiveName'])
                      if definitionFnModule is not None:
                         #get Fn from name
                         fnToCall = None
                         try:
                            fnToCall = getattr(definitionFnModule, 'get_element_value')
                         except AttributeError:
                            value = None  
                         if fnToCall is not None:
                            #call Fn
                            value = fnToCall(pe,rmd,params)
                    except ModuleNotFoundError:
                      value = None
                                 
                    useDefault = pe['UseDefault']
                    defaultValue=pe['DefaultValue']
                    updateWithDefault=pe['UpdateWithDefault']
                   
                    if defaultValue is not None and type(defaultValue) is str:
                        defaultValue = json.loads(defaultValue)

                    if defaultValue is not None: 
                      if 'id' in defaultValue:
                        defaultValue = defaultValue['id']
                      elif 'value' in defaultValue:
                        defaultValue = defaultValue['value']
                      elif 'Value' in defaultValue:
                        defaultValue = defaultValue['Value']
                      
                    if not useDefault and value is not None:

                      if field_name in recDict:
                        recDict[field_name]= value
                      else:
                        recDict.update({field_name:value})

                    if useDefault or updateWithDefault:
                      if field_name in recDict:
                        recDict[field_name] = defaultValue
                      else:
                        recDict.update({field_name:defaultValue})

                  dictRecords.append(recDict)    
                  
                  return dictRecords,recordId     
              
        #Get presentation elements and resource model def
          
        #pes = PresentationElements.objects.filter(ResourceDefId_id=self.id,TaskDefId_id=task_def_id,FormType=form_type,OverrideType=override_type,Status='Active',ResourceModelDefinitionId__ResourceDefId_id=self.id,ResourceModelDefinitionId__Status='Active').select_related('ResourceModelDefinitionId')
        #dovuci org_id
        recs =  self.rdbms_cls.get_data_filtered_in(self.id,self.OrganisationId,rec_list)
             
        for rec in recs:
            recordId = rec.id
            #recDict = model_to_dict(rec)
            recDict = dict((col, getattr(rec, col)) for col in rec.keys())
            for pe in pes:
                #rmd=pe.ResourceModelDefinitionId
              
                value = None
                field_name=pe['FieldName']
              

                 #ToDo - move this to widgets
                 #import module
                try:
                  module_path = 'katalyonode.functions.widgets.form.'+pe['DirectiveName']
                  definitionFnModule =  importlib.import_module(module_path, pe['DirectiveName'])
                  if definitionFnModule is not None:
                     #get Fn from name
                     fnToCall = None
                     try:
                        fnToCall = getattr(definitionFnModule, 'get_element_value')
                     except AttributeError:
                        value = None  
                     if fnToCall is not None:
                        #call Fn
                        value = fnToCall(pe,recDict,params)
                except ModuleNotFoundError:
                  value = None

             
                useDefault = pe['UseDefault']
                updateWithDefault=pe['UpdateWithDefault']
                
                if updateWithDefault or useDefault:
                    
                    defaultValue=pe.DefaultValue
                    if defaultValue is not None:
                      if type(defaultValue) is str:
                        if '{' and '}' in defaultValue:
                          defaultValue = json.loads(defaultValue)
                          if 'id' in defaultValue:
                            defaultValue = defaultValue['id']
                          elif 'value' in defaultValue:
                            defaultValue = defaultValue['value']
                      elif type(defaultValue) is dict:
                        if 'id' in defaultValue:
                          defaultValue = defaultValue['id']
                        elif 'value' in defaultValue:
                          defaultValue = defaultValue['value']
                    
                    recDict[field_name] = defaultValue

                if not useDefault and value is not None:
                    if field_name in recDict:
                        if recDict[field_name] is None:
                            recDict[field_name] = value
                    else:
                      recDict.update({field_name:value})
                    
                dictRecords.append(recDict)    

              
        return dictRecords,recordId

    def extract_form_data_search(self,in_filter,form): #get data from form
      
        resourceData={}
        select_related=[]
        parameters={}
        if in_filter is None:
            in_filter={}
            
        if form is not None:
            if len(form)==0:
                if 'id__in' in in_filter:
                    resourceData['id__in'] = {'type':'id__in', 'value': in_filter['id__in']}
           
            for row in form:
                if 'layout' in row:
              
                    for column in row['layout']:
              
                        rData,f,s_r, parameters = self.process_widget_data_layout_search(column,in_filter)
                        in_filter = f
                        resourceData.update(rData)
                        select_related.extend(s_r)
      
        return resourceData,in_filter,select_related, parameters #{'Field48': {'type': 'textbox', 'value': '4'}}



    def process_widget_data_layout_search(self,item,filter_data):
      
        #ToDo - move to classes
        resourceData={}
        queryParameters={}
        value=''
        item_id=None
        s_item_value=''  
        no_update=False
        resource_value = None
        select_related=[]
      
        if 'layout' in item:
            for element in item['layout']:
                rData,f,s_r, param = self.process_widget_data_layout_search (element,filter_data)
                filter_data = f
                resourceData.update(rData)
                queryParameters.update(param)
                select_related.extend(s_r)
        else:     
            
           
            try:
                r_val=None
                module_path = 'katalyonode.functions.widgets.form.'+item['DirectiveName']
                module_ref = importlib.import_module(module_path)
                widget_class = getattr(module_ref, item['DirectiveName'])
                widget_object = widget_class(item,self,None,None)
                
                if widget_object is not None:
                    
                    r_val,filter_data,select_related,queryParameters = widget_object.process_widget_search(item,filter_data)
                    
                    if r_val is not None:
                        fieldId = r_val.get('fieldId',None)
                        r_val['DirectiveName'] = item['DirectiveName']
                        if fieldId is not None:
                            resourceData[fieldId] = r_val
            except ModuleNotFoundError:
                  raise Exception('Module not found for widget '+item['DirectiveName'])
            
            """    
            if item['ElementType']=='resourcelink':
                  
                if 'Value' in item:
                    resource_value=item['Value']
                else:
                    resource_value=None
                    if 'itemValue' in item:
                        resource_value=item['itemValue']
                  
                    if resource_value is not None and len(resource_value)>0:
                        resource_value=resource_value[0]
                    else:
                        resource_value=None
                    
                resourceData[fieldId+'rec_id'] = {'type':item['ElementType'],'value':resource_value}
            """      
                  
        return resourceData,filter_data,select_related, queryParameters





    def process_resource_data_search (self,data):

        mainResource={}
        excludeResource={}
        m2mData=[]


        if data is not None:
            main_resource={}
            m2m_data=[]
            for field in data.items():
                
                #ErrorLog.objects.create(Error1={'filter':filter_data},Error2={'field1':field[1],'mainResource':mainResource},CreatedDateTime=timezone.now())     
        
                if field[1]['type']=='id__in':
              
                    value=field[1].get('value')
                    mainResource[field[0]]=value
                else:
                    value=field[1].get('value')
                    try:
                        r_val=None
                        module_path = 'katalyonode.functions.widgets.form.'+field[1]['DirectiveName']
                        module_ref = importlib.import_module(module_path)
                        widget_class = getattr(module_ref, field[1]['DirectiveName'])
                        widget_object = widget_class(field[1],self,None,None)
                
                        if widget_object is not None:
                    
                            main_resource,m2m_data = widget_object.prepare_data_search(field)

                    except ModuleNotFoundError:
                        raise Exception('Module not found for widget '+field[1]['DirectiveName'])

                    if 'search' in field[1]:
                        if (field[1]['search'] is not None):  #  (value=none u slucaju da je between!!!)
                    
                            if 'filter' in field[1]['search']: 
                                if type(field[1]['search'])==type(str()):

                                    filter_data=json.loads(field[1]['search'])['filter']

                                else:  filter_data=field[1]['search']['filter']

                                if filter_data is not None:
                                    if field[0] in main_resource:
                                        del main_resource[field[0]]
                                        
                                    if filter_data=='between': 
                                        if field[1]['search']['between1'] is not None: main_resource[field[0]+'__gte']=field[1]['search']['between1']
                                        if field[1]['search']['between2'] is not None: main_resource[field[0]+'__lte']=field[1]['search']['between2']   
                                    
                                    if filter_data=='onlist':

                                        if 'value' in field[1]['search']:
                                            if field[1]['search']['value'] is not None: #MIGOR
                                                if type(field[1]['search']['value'])==list:
                                                    if len(field[1]['search']['value'])>0:
                                                        if field[1]['search']['value']!=[{}]:
                                                            main_resource[field[0]+'__in']=[]
                                                            for item in field[1]['search']['value']:
                                                                if item is not None:
                                                                    if type(item)==dict:
                                                                        if 'id' in item:
                                                                            if item['id'] is not None:
                                                                                main_resource[field[0]+'__in'].append(item['id'])
                                                                    else:
                                                                        main_resource[field[0]+'__in'].append(item)

                                                elif type(field[1]['search']['value'])==dict:
                                                    item = field[1]['search']['value']
                                                    if item is not None: 
                                                        if 'id' in item:
                                                            if item['id'] is not None:
                                                                main_resource[field[0]]=item['id']
                                                         

                                                elif type(field[1]['search']['value']) in [int,str]:
                                                    if field[1]['search']['value'] is not None:
                                                        main_resource[field[0]]=field[1]['search']['value']
                                         
                                    if filter_data=='notonlist':
                                        if 'value' in field[1]['search']:
                                            if field[1]['search']['value'] is not None: #MIGOR
                                                if type(field[1]['search']['value'])==list:
                                                    if len(field[1]['search']['value'])>0:
                                                        if field[1]['search']['value']!=[{}]:
                                                            main_resource[field[0]+'__notin']=[]
                                                            for item in field[1]['search']['value']:
                                                                if item is not None:
                                                                    if type(item)==dict:
                                                                        if 'id' in item:
                                                                            if item['id'] is not None:
                                                                                main_resource[field[0]+'__notin'].append(item['id'])
                                                                    else:
                                                                        main_resource[field[0]+'__notin'].append(item)

                                                elif type(field[1]['search']['value'])==dict:
                                                    item = field[1]['search']['value']
                                                    if item is not None:
                                                        if 'id' in item:
                                                            if item['id'] is not None:
                                                                main_resource[field[0]+'__notin']=[item['id']]

                                                elif type(field[1]['search']['value']) in [int,str]:
                                                    if field[1]['search']['value'] is not None:
                                                        main_resource[field[0]+'__notin']=[field[1]['search']['value']]       
                                    if value is not None:
                                        if type(value) not in [list,dict]:
                                            if filter_data=='contains': main_resource[field[0]+'__icontains']=value
                                            if filter_data=='begins': main_resource[field[0]+'__istartswith']=value
                                            if filter_data=='ends': main_resource[field[0]+'__iendswith']=value
                                            if filter_data=='eq':                            
                                                main_resource[field[0]+'__exact']=value
                                              
                                            if filter_data=='gt': main_resource[field[0]+'__gte']=value # MIGOR TODO - vidjeti da li gt ili gte , lt ili lte !
                                            if filter_data=='lt': main_resource[field[0]+'__lte']=value
                                      
                                        
                                #ErrorLog.objects.create(Error1={'filter':filter},Error2={'mainR':mainResource,'data':data},CreatedDateTime=timezone.now())        
                    else:
                        if value is not None and value!=[{}]:
                            mainResource[field[0]]=value #MIGOR TODO - mozda i ovdje treba ako je datum value.date() ??
                
                mainResource.update(main_resource)
                m2mData.extend(m2m_data)
                #ErrorLog.objects.create(Error1={'filter':filter_data},Error2={'field1':field[1],'mainResource':mainResource},CreatedDateTime=timezone.now())     

        return mainResource,m2mData


    def get_dataset_data_filtered(self,filtered_data,columns,sr, parameters):
  
        datasetRecords = []
        selectedRecords = []
        relatedDict={}
        relatedDictIds={}
        querysetRelated = {}
        allFields = {}

        session = self.rdbms_cls.Session()
        session.begin()

        try:
            queryset =  self.rdbms_cls.get_data_filtered_sr(self.id,self.OrganisationId,filtered_data,columns,sr,parameters,session)
            #if queryset is None:
                #raise Exception ('Error retrieving data')
            """
            for item in queryset:
                rec_id = getattr(item,"resource"+str(self.id)+"_"+str(self.OrganisationId)+"_id",None)
                if rec_id is not None:
                    datasetRecords.append(rec_id)
                
                itemDict = item._asdict()
                
                for col in item.keys():
                    if hasattr(item[col],'_asdict'):
                        itemDict[col] = item[col]._asdict()
                
                itemDict['id'] = rec_id
                selectedRecords.append(itemDict)
            """
            #pdb.set_trace()
            #selectedRecords = queryset._asdict()
        except Exception as exc:
            #pdb.set_trace()
            if session:
                session.rollback()
                session.close()
                raise exc
        finally:
            if session:
                session.commit()
                session.close()

        return queryset #selectedRecords

    def get_dataset_data_filtered_union(self,unions,union_type):
  
        queries = []
        selectedRecords = []
        relatedDict={}
        relatedDictIds={}
        querysetRelated = {}
        allFields = {}

        session = self.rdbms_cls.Session()
        session.begin()

        try:

            for u_item in unions:

                queries.append(self.rdbms_cls.pre_build_query(u_item['resourceDefId'],self.OrganisationId,u_item['filterData'],u_item['columns'],u_item['literals'],u_item['m2m'],{},session))
                
            queryset = self.rdbms_cls.execute_union(queries[0],queries[1:len(queries)],union_type,session)
            """
            for item in queryset:
                rec_id = getattr(item,"resource"+str(self.id)+"_"+str(self.OrganisationId)+"_id",None)
                #rec_id = getattr(item,"resource"+str(self.id)+"_"+str(self.OrganisationId)+"_id",None)
                
                itemDict = item._asdict()
                
                for col in item.keys():
                    if hasattr(item[col],'_asdict'):
                        itemDict[col] = item[col]._asdict()
                
                itemDict['id'] = rec_id
                selectedRecords.append(itemDict)
            """
        except Exception as exc:
            if session:
                session.rollback()
                session.close()
                raise exc
        finally:
            if session:
                session.commit()
                session.close()

        return queryset #selectedRecords

    def get_dataset_m2m(self,related_id,record_id):
  
        selectedRecords = []
        
        session = self.rdbms_cls.Session()
        session.begin()

        try:
            
            queryset =  self.rdbms_cls.get_m2m_data_full (self.id,related_id,record_id,self.OrganisationId,0,session)
            #if queryset is None:
                #raise Exception ('Error retrieving data')
            """
            for item in queryset:
                rec_id = getattr(item,"resource"+str(self.id)+"_"+str(self.OrganisationId)+"_id",None)
                
                itemDict = item._asdict()
                
                for col in item.keys():
                    if hasattr(item[col],'_asdict'):
                        itemDict[col] = item[col]._asdict()
                
                itemDict['id'] = rec_id
                selectedRecords.append(itemDict)
            """
        except Exception as exc:
            if session:
                session.rollback()
                session.close()
                raise exc
        finally:
            if session:
                session.commit()
                session.close()

        return queryset #selectedRecords

    def get_dataset_m2m_filter(self,related_id,record_id,finalFilter):
  
        selectedRecords = []
        
        session = self.rdbms_cls.Session()
        
        with session.begin():

            
            queryset =  self.rdbms_cls.get_m2m_data_filter (self.id,related_id,record_id,self.OrganisationId,0,finalFilter,session)
        
        return queryset #selectedRecords
    

    def get_saved_form(self):
        
        self.get_form_definition(self.FormType,self.NewVersion)
        self.generate_form()
        form_json = self.get_form_json()
        return form_json

    def process_parent_dataset(self,element,widget_data,task_def_id,prev_task_id,task_instance_id,record,parent_dataset_id,parent_dataset_record_id):
    
        datasetId = 0
        datasetDefId=None
        form_type = 'e'
        task_source_id=0
        src_task_def_id  = 0
        parentDatasetId = None
        parentDatasetDefId = None
        if parent_dataset_record_id is None:
          parent_dataset_record_id=0
        if parent_dataset_id is None:
          parent_dataset_id=0
        helper = ResourceHelperClass()
        resource_type_code_id = helper.get_status_code_by_name('task','resourcetype');
        teis = None
        hasParentElement = element.HasParentDataset
        if (hasParentElement):
              #if 'populateTypeParent' in element:
                session = self.rdbms_cls.Session()
                with session.begin():

                    populateTypeParent = element.PopulateTypeParent
                   
                    if parent_dataset_record_id>0 and parent_dataset_id>0:
                    
                        parentDatasetDefId = parent_dataset_id
                        parentDatasetId = parent_dataset_record_id
                        populateTypeParent = '4'
                      
                    elif populateTypeParent[0:1]=='1':
                      
                        parentDatasetDefId = int(element.ParentDatasetId_id)
                        task_source_id = 0
                    elif populateTypeParent[0:1]=='2':
                        task_source_id = task_instance_id
                        src_task_def_id = task_def_id
                    elif populateTypeParent[0:1]=='3':
                        task_source_id = prev_task_id
                        
                        #teis=TaskExecutionInstances.objects.filter(id=prev_task_id)
                        teis = session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==prev_task_id).all()
                        for tei in teis:
                            src_task_def_id = tei.TaskDefId
                    
                    if populateTypeParent[2:3]=='d' or populateTypeParent[0:1]=='4':
                        #pdb.set_trace() 
                        if type(element.ParentDatasetId) == ResourceDefSa and element.ParentDatasetId.id is not None and element.ParentDatasetId.id > 0: #TODO - check what kind of resource should element be
                                datasetDefId = element.ParentDatasetId.id
                        elif element.ParentDatasetId is not None and element.ParentDatasetId>0:
                                datasetDefId = element.ParentDatasetId
                        else:
                          #get potential parents
                          potential_parents=[]
                          #teistr=TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id__in=[task_instance_id,prev_task_id])
                          teistr = session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId.in_([task_instance_id,prev_task_id])).all()
                          parent_resource_ids=[]      
                          for tei in teistr:
                            parent_resource_ids.append(tei.ResourceDefId_id)
                          
                          if element.UniqueId is not None:
                            unique_id = element.UniqueId.id
                          else:
                            unique_id=None 
                          
                          if unique_id is None:
                            unique_id = 0

                          related_id = element.Related_id
                          if related_id is None:
                            related_id=0
                          else:
                            related_id=int(element.Related_id)
                          if len(parent_resource_ids)>0:
                            #parents = PresentationElements.objects.filter(Related_id=related_id,Status='Active',ResourceDefId_id__in=parent_resource_ids,UuidRef=element.SrcParentWidgetId)
                            parents = session.query(PresentationElementsSa).filter(PresentationElementsSa.Related==related_id,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceDefId.in_(parent_resource_ids),PresentationElementsSa.UuidRef==element.SrcParentWidgetId).all()
                          elif unique_id>0:
                            #parents = PresentationElements.objects.filter(Related_id=related_id,Status='Active',ResourceDefId_id=parent_dataset_id,UniqueId_id=unique_id)
                            parents = session.query(PresentationElementsSa).filter(PresentationElementsSa.Related==related_id,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceDefId==parent_dataset_id,PresentationElementsSa.Unique==unique_id).all()

                          else:
                            #parents = PresentationElements.objects.filter(Related_id=related_id,Status='Active',ResourceDefId_id=parent_dataset_id)
                            parents = session.query(PresentationElementsSa).filter(PresentationElementsSa.Related==related_id,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceDefId==parent_dataset_id).all()

                          for parent in parents:
                            potential_parents.append(parent.ResourceDefId_id)
                            
                          #pes = PresentationElements.objects.filter(ResourceDefId_id=src_task_def_id,FormType=populateTypeParent[1:2],Status='Active',ElementType__in=['add','update','view','rlist'],Related_id__in=potential_parents).exclude(Related_id=related_id)
                          pes = session.query(PresentationElementsSa).filter(PresentationElementsSa.ResourceDefId==src_task_def_id,PresentationElementsSa.FormType==populateTypeParent[1:2],PresentationElementsSa.Status=='Active',PresentationElementsSa.ElementType.in_(['add','update','view','rlist']),PresentationElementsSa.Related.in_(potential_parents),PresentationElementsSa.Related!=related_id).all()
                          #pdb.set_trace() 
                          if len(pes)>0:
                            for pe in pes:
                              datasetDefId = pe.Related
                              
                          else:
                            datasetDefId = related_id

                    elif populateTypeParent[2:3]=='i':
                        datasetDefId = related_id
                    if task_source_id>0 or populateTypeParent[0:1]=='4':

                        getParent = False
                        if populateTypeParent[0:2] == '2i':
                            #tiitrs_parent=TaskInitiationInstanceToResource.objects.filter(TaskInitiateId_id = task_source_id,ResourceDefId_id = datasetDefId,PresentationElementUuidRef=element.SrcParentWidgetId)
                            tiitrs_parent = session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.TaskInitiateId==task_source_id,TaskInitiationInstanceToResourceSa.ResourceDefId==datasetDefId,TaskInitiationInstanceToResourceSa.PresentationElementUuidRef==element.SrcParentWidgetId).all()

                            getParent = True
                        elif populateTypeParent[0:2] == '2e':
                            #tiitrs_parent=TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id = task_source_id,ResourceDefId_id = datasetDefId,PresentationElementUuidRef=element.SrcParentWidgetId)
                            tiitrs_parent = session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==task_source_id,TaskExecutionInstanceToResourceSa.ResourceDefId==datasetDefId,TaskExecutionInstanceToResourceSa.PresentationElementUuidRef==element.SrcParentWidgetId).all()

                            getParent = True
                        elif populateTypeParent[0:2] =='3i':
                            #tiitrs_parent=TaskInitiationInstanceToResource.objects.filter(TaskInitiateId_id = task_source_id,ResourceDefId_id = datasetDefId)
                            tiitrs_parent = session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.TaskInitiateId==task_source_id,TaskInitiationInstanceToResourceSa.ResourceDefId==datasetDefId).all()

                            getParent = True
                        elif populateTypeParent[0:2] =='3e':
                            #tiitrs_parent=TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id = task_source_id,ResourceDefId_id = datasetDefId)
                            tiitrs_parent = session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==task_source_id,TaskExecutionInstanceToResourceSa.ResourceDefId==datasetDefId).all()

                            getParent = True
                        else:
                            
                            #tiitrs_parent=TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id = task_source_id,ResourceDefId_id = datasetDefId,PresentationElementUuidRef=element.SrcParentWidgetId)
                            tiitrs_parent = session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==task_source_id,TaskExecutionInstanceToResourceSa.ResourceDefId==datasetDefId,TaskExecutionInstanceToResourceSa.PresentationElementUuidRef==element.SrcParentWidgetId).all()

                            for tpi in tiitrs_parent:
                                parentDatasetId = tpi.ParentDatasetId
                                parentDatasetDefId = tpi.ParentDatasetDefId

                        if getParent:
                            for tpi in tiitrs_parent:
                                parentDatasetId = tpi.ResourceId
                                parentDatasetDefId = datasetDefId
                        
                        #TODO digni exception jer nece dobro vezati parent-a
                        
                        if parentDatasetDefId is not None:
                            upd_table_name = 'resource'+str(self.id)+'_'+str(self.OrganisationId)
                            field_id =  'resource'+str(parentDatasetDefId)+'_'+str(self.OrganisationId)+'_id'
                            #rmd = ResourceModelDefinition.objects.filter(ResourceDefId_id=element.ParentDatasetId_id,Status='Active',FieldType=upd_table_name)
                            #rmd = ResourceModelDefinition.objects.filter(ResourceDefId_id=element.ParentDatasetId,Status='Active',FieldType=upd_table_name)
                            rmd = session.query(ResourceModelDefinitionSa).filter(ResourceModelDefinitionSa.ResourceDefId==element.ParentDatasetId,ResourceModelDefinitionSa.Status=='Active',ResourceModelDefinitionSa.FieldType==upd_table_name)
                            
                            rmd_count= rmd.count()
                            if rmd_count>1:
                                raise Exception('Error - Multiple parent datasets found! Widget_id = '+str(element.id)+' Parent Dataset = '+str(element.ParentDatasetId_id)+' ElementType = '+element.ElementType+' FieldType = '+str(upd_table_name))
                            
                            if rmd_count==0:
                                raise Exception('Error - Parent dataset field definition is missing! Widget_id = '+str(element.id)+' Parent Dataset = '+str(element.ParentDatasetId_id)+' ElementType = '+element.ElementType+' FieldType = '+str(upd_table_name))
                        
                            if rmd_count==1:
                                if rmd[0].MaxLength==1:
                                    #raise Exception('Ušao u max_len==1 '+str(parentDatasetId)+', '+str(parentDatasetDefId)+', Widget_id = '+str(element.id)+' ElementType = '+element.ElementType)
                                    update_fk = {}
                                    update_fk[rmd[0].FieldName] = record.id
                                    update_fk[field_id] = parentDatasetId
                                    related_rec = self.rdbms_cls.update_data(update_fk,parentDatasetDefId,self.OrganisationId,self.User.id)
                                    if related_rec is None or related_rec[1][rmd[0].FieldName]!=record.id:
                                        raise Exception('Error - update parent dataset failed for max_lenght=1! Related rec='+str(related_rec)+' Widget_id = '+str(element.id)+' ElementType = '+element.ElementType)
                                else:
                                    #raise Exception('Ušao u max_len!=1 '+str(parentDatasetId)+', '+str(parentDatasetDefId)+' , this dataset is '+str(self.id)+' record = '+str(record.id)+', Widget_id = '+str(element.id)+' ElementType = '+element.ElementType)
                                   
                                    related_rec = self.rdbms_cls.insert_data_m2m_no_delete(parentDatasetDefId,self.id,parentDatasetId,[{'resourceDefId':self.id,'resourceId':[record.id]}],self.OrganisationId,self.User)
                                    if len(related_rec)==0:
                                        raise Exception('Error - Parent dataset record is not created found! Widget_id = '+str(element.id)+' ElementType = '+element.ElementType)
                        else:
                            raise Exception('Error - Parent dataset not found! Widget_id = '+str(element.id)+' ElementType = '+element.ElementType)
                    else:
                        raise Exception('Error - Parent source is invalid '+str(task_source_id))
        return parentDatasetDefId, parentDatasetId

    def get_parent_resources_for_child(self):
        
        
        parents = []
        session = self.rdbms_cls.Session()
        
        with session.begin():
            rmds = session.query(ResourceModelDefinitionSa,ResourceDefLangSa,ResourceDefSa).join(ResourceDefSa,and_(ResourceModelDefinitionSa.ResourceDefId==ResourceDefSa.id,ResourceDefSa.Active==True)).join(ResourceDefLangSa,and_(ResourceModelDefinitionSa.ResourceDefId==ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang==self.Language.id)).filter(ResourceModelDefinitionSa.Status=='Active',ResourceModelDefinitionSa.Related==self.id).all()
            for rmd in rmds:
                parent = {'id':rmd[0].ResourceDefId,'name':rmd[1].Name,'rmdId':rmd[0].id,'db_type':rmd[0].Dbtype,'max_length':rmd[0].MaxLength,'field_name':rmd[0].FieldName}
                parents.append(parent)
        return parents


    def get_form_fields(self,version):

       
        peElements = []
        session = self.rdbms_cls.Session()
        try:
            session.begin()
            if version==0:

                #form_type is missing
                version_max = session.query(func.max(PresentationElementsSa.VersionTo)).filter(PresentationElementsSa.ResourceDefId==self.id,PresentationElementsSa.Status=='Active').all()[0]

                #PresentationElements.objects.filter(ResourceDefId=resource_id,FormType=form_type,ResourceType='o',Status='Active').aggregate(Max('VersionTo'))
                if version_max is not None:
                    version = version_max[0]
                    if version is None:
                        version = 0

            peFields = session.query(PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa).join(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==self.Language.id)).join(ResourceModelDefinitionSa,PresentationElementsSa.ResourceModelDefinitionId==ResourceModelDefinitionSa.id).filter(PresentationElementsSa.ResourceDefId==self.id,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceModelDefinitionId>0,PresentationElementsSa.VersionFrom<=version,PresentationElementsSa.VersionTo>=version).all()
            
            for pe in peFields:
                try:
                    parameters = json.loads(pe[0].Parameters)
                except:
                    parameters = {}
                try:    
                    parameters_lang = json.loads(pe[1].ParametersLang)
                except:
                    parameters_lang = {}

                try:    
                    name_field = json.loads(pe[0].NameField)
                except:
                    name_field = []

                peElements.append({'id':pe[0].id,'name':pe[1].Label,'Label':pe[1].Label,'rmdId':pe[2].id,'FieldName':pe[2].FieldName,'GenericType':pe[2].GenericType,'ElementType':pe[0].ElementType,'NameField':name_field,'Parameters':parameters,'ParametersLang':parameters_lang,'FieldType':pe[2].FieldType,'CodeId':pe[2].CodeId})

        except Exception as exc:
            if session:
                session.rollback()
                session.close()
            raise exc
        finally:
            session.commit()
            session.close()

        
        return peElements