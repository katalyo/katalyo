######################## task class ##########################
###############################################################################

from django.core.validators import ValidationError
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass
from katalyonode.models.models import TaskParameters,TaskTransactions,TaskInitiationInstance,TaskExecutionInstances,PresentationElements,AuditTrailTaskStatus
from katalyonode.models.models import TaskInitiationInstanceToResource,TaskAssignments,TaskAssignmentDefUser,TaskAssignmentDefGroup
from katalyonode.models.models import TaskDefWidgetDependencies,CodesDetail,TaskCodesActions,PresentationElementsLang
import uuid
#from django.utils import timezone
from datetime import datetime,timezone,timedelta
from django.contrib.auth.models import User, Group
from katalyonode.functions.resources.base.resourcehelper import TaskInitiationInstanceJson,TaskExecutionInstancesJson
from katalyonode.functions.rdbms.alchemymodels import PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa,auth_user,auth_group,auth_user_groups,TaskTransactionsSa,TaskAssignmentDefUserSa,TaskAssignmentDefGroupSa
from katalyonode.functions.rdbms.alchemymodels import TaskInitiationInstanceSa,TaskExecutionInstancesSa,TaskExecutionInstanceToResourceSa,TaskAssignmentsSa,TaskParametersSa,CodesDetailSa,CodesHeadSa,codesdetail,TaskCodesActionsSa,LanguageSa
import importlib
from sqlalchemy import and_
import pdb


class task (ResourceBaseClass):
    
    def __init__(self,_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_public=False,_previous_task_id = None,_outcome_id=None,_resource_sub_type=None):

        super().__init__(_id,_resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
        self.TaskInitiate = None
        self.TaskExecute = None
        self.TaskParameters = None
        self.TaskTransactionId = None
        self.PreviousTaskId = _previous_task_id
        self.OutcomeId = _outcome_id
        self.PublicTask = _public
        self.Guid = None
        self.ParentDatasetId = None
        self.ParentDatasetRecordId = None 
        self.PotentialOwners = None 
        self.TaskData = None
        self.UniqueName = None
        #self.AutoExecute = None
        self.Guid = str(uuid.uuid4())
        if self.id is not None and self.Session is not None:
          self.set_task_parameters()



    def  initialise_initiate_task(self,_form_type,_previous_task_id,_outcome_id,_parent_dataset_id,_parent_dataset_record_id,_potential_owners,_task_data,_unique_name=None):

        self.FormType = _form_type
        if _previous_task_id is None:
            self.PreviousTaskId = 0
        else:
            self.PreviousTaskId = _previous_task_id
        if _outcome_id is None:
            self.OutcomeId = 0
        else:
            self.OutcomeId = _outcome_id
        self.ParentDatasetId = _parent_dataset_id
        self.ParentDatasetRecordId = _parent_dataset_record_id 
        self.PotentialOwners = _potential_owners 
        self.TaskData = _task_data
        self.UniqueName = _unique_name
        self.set_task_parameters()

    def set_task_parameters (self):
        #add all methods required for task setup

        
      if self.PublicTask and self.UniqueName is not None:
          self.get_public_task_parameters_def()
      elif not self.PublicTask:
        self.get_task_parameters_def()    
        
           #response =  {'msg':str(ve)+' Task id ='+str(self.id)}
           #response_status=403
           #return response,response_status

    def  get_definition_by_task_exe(self,_task_exe_id,_outcome_id,_task_data):

        #get task id by execute instance - ToDo create a function for this tht uses SQLAlchemy
        #texe = TaskExecutionInstances.objects.filter(id=_task_exe_id)
        texe = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==_task_exe_id).all()
                          
        for task_instance in texe:

          self.id=task_instance.TaskDefId
          self.TaskExecute = task_instance
        if _outcome_id is None:
            self.OutcomeId = 0
        else:
            self.OutcomeId = _outcome_id
        self.TaskData = _task_data
        self.set_task_parameters()    

    def  process_task_assignments(self):



        msg=''
        if self.TaskParameters.SelfAssigned:
          tas = TaskAssignmentsSa(UserId = self.User.id, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
          self.Session.add(tas)
          #tas = TaskAssignments.objects.create(UserId = self.User, TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=self.User,CreatedDateTime=timezone.now(),Claimed=False)
          
          #ToDo - redesign update to work with SqlAlchemy
          self.TaskInitiate.Status=self.get_status_code_by_name('ready','taskstatuses')
          self.TaskInitiate.ChangedBy = self.User.id
          #self.TaskInitiate.save()
          return 1,msg
       
        count_po=0
        users_ids=[]
        if self.TaskParameters.AssignTypeOwner in ['i','o']:
            #teis = TaskExecutionInstances.objects.filter(id=self.TaskInitiate.PreviousTaskId).select_related('TaskInitiateId')
            teis = self.Session.query(TaskExecutionInstancesSa,TaskInitiationInstanceSa).outerjoin(TaskInitiationInstanceSa,TaskExecutionInstancesSa.TaskInitiateId==TaskInitiationInstanceSa.id).filter(TaskExecutionInstancesSa.id==self.TaskInitiate.PreviousTaskId).all()
           
            if len(teis)==0:
              msg = 'Previous task not found! | Assign type is '+str(self.TaskParameters.AssignTypeOwner)+' | Task execute Id = '+str(self.TaskInitiate.PreviousTaskId_id)
              return count_po,msg  
            #search for previous task
            for tei in teis:
              if self.TaskParameters.AssignTypeOwner=='o':
                #user_obj= User.objects.get(id=tei.ActualOwner)
                #TaskAssignments.objects.create(UserId = tei[0].ActualOwner, TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=self.User,CreatedDateTime=timezone.now(),Claimed=False)
                ta = TaskAssignmentsSa(UserId = tei[0].ActualOwner, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
                self.Session.add(ta)
                #teis = TaskInitiationInstancesSa(TaskDefId=self.id,InitiatorId=self.User,Status=code_status_id,TaskType=self.TaskParameters.TaskType,TransactionId = str(self.TaskTransactionId),NoOfExeInstances=0,ExeInstanceLimit=exeLimit,PreviousTaskId_id=p_task_id,CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc))
                count_po=count_po+1
              elif self.TaskParameters.AssignTypeOwner=='i':
                #user_obj= User.objects.get(id=tei.TaskInitiateId__InitiatorId)
                #TaskAssignments.objects.create(UserId = tei.TaskInitiateId.InitiatorId, TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=self.User,CreatedDateTime=timezone.now(),Claimed=False)
                ta = TaskAssignmentsSa(UserId = tei[1].InitiatorId, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
                self.Session.add(ta)
                count_po=count_po+1
        
        elif self.TaskParameters.AssignTypeOwner in ['d']:
          #get the right user from dataset record
          
          
          #first get from task and if not found then directly from dataset
          rmdResources = []
          rmdResourcesAll = []
          peElements = []
          rmdFields = []   
          ResourceDefId = None
          
          #pes = PresentationElements.objects.filter(ElementType='user',UniqueId=self.TaskParameters.UniqueId,Status='Active',ResourceDefId_id__in=rmdResourcesAll).select_related('ResourceModelDefinitionId')
          pes = self.Session.query(PresentationElementsSa,ResourceModelDefinitionSa).join(ResourceModelDefinitionSa,ResourceModelDefinitionSa.id==PresentationElementsSa.ResourceModelDefinitionId).filter(PresentationElementsSa.ElementType=='user',PresentationElementsSa.UniqueId==self.TaskParameters.UniqueId,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceDefId.in_(rmdResourcesAll)).all()
          
          if pes.count()>0:
            for pe in pes:
              rmdResources.append(pe.ResourceDefId)
              rmdFields.append({'ResourceDefId':pe[0].ResourceDefId,'ItemId':pe[1].id,'FieldId': pe[1].FieldName +str(pe[1].id)+'_id'})
          else:
            return count_po,'Error retrieving potential owner from dataset--> Unique user identifier not found in dataset definition (158)' 
          
                  
          #tittr = TaskInitiationInstanceToResource.objects.filter(TaskInitiateId=self.TaskInitiate,ResourceDefId_id__in=rmdResources)
          tittr = self.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.TaskInitiateId==self.TaskInitiate.id,TaskInitiationInstanceToResourceSa.ResourceDefId.in_(rmdResources)).all()
          if tittr.count()>0:
            for rec in tittr:
              ResourceDefId = rec.ResourceDefId
              ResourceRecordId = rec.ResourceId
          else:
            #go to prev task execute
            #tettr = TaskExecutionInstanceToResource.objects.filter(TaskExecuteId=self.TaskInitiate.PreviousTaskId,ResourceDefId_id__in=rmdResources)
            tettr = self.Session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==self.TaskInitiate.PreviousTaskId,TaskInitiationInstanceToResourceSa.ResourceDefId.in_(rmdResources)).all()
          
            if tettr.count()>0:
              for rec in tettr:
                ResourceDefId = rec.ResourceDefId_id
                ResourceRecordId = rec.ResourceId
          
          if ResourceDefId is not None:
            #ToDo - fix this
            datasetRecords = self.rdbms_cls.get_data_filtered(ResourceDefId,self.OrganisationId,{'id':ResourceRecordId}, parameters={})
            if  datasetRecords is not None and datasetRecords.count()>0:
              datasetRecord = datasetRecords[0]
            if datasetRecord is not None:
              #get field which holds needed user id
             
              
              fieldId=''
              for field in rmdFields:
                if field['ResourceDefId'] == ResourceDefId:
                  fieldId = field['FieldId'] 
              userId = getattr(datasetRecord, fieldId,None)
              
              if userId is not None:
                #users_ids.append(userId)
                #ta = TaskAssignments.objects.create(UserId_id = userId, TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=self.User,CreatedDateTime=timezone.now(),Claimed=False)
                ta = TaskAssignmentsSa(UserId = userId, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
                self.Session.add(ta)
                if not ta:
                  return count_po,'Error retrieving potential owner from dataset--> TaskAssignments not created'
                else:
                  count_po+=1
              else:
                return count_po,'Error retrieving potential owner from dataset--> User is null'
            else:
              return count_po,'Error retrieving potential owner from dataset--> Dataset record not found'
        
          else:
            return count_po,'Error retrieving potential owner from dataset ---> Link to dataset not found'
            
          
        else:      
             
          if self.TaskParameters.AutoAssign:
            #users = User.objects.filter(assigneduser__TaskDefId_id=self.id,assigneduser__AssignGroup='potentialOwners').values()
            users = self.Session.query(auth_user,TaskAssignmentDefUserSa).join(TaskAssignmentDefUserSa,TaskAssignmentDefUserSa.AssigneeId==auth_user.id).filter(TaskAssignmentDefUserSa.TaskDefId==self.id,TaskAssignmentDefUserSa.AssignGroup=='potentialOwners').all()

            #groups = Group.objects.filter(assignedgroup__TaskDefId_id=self.id,assignedgroup__AssignGroup='potentialOwners').values()
            groups = self.Session.query(auth_group,TaskAssignmentDefGroupSa).join(TaskAssignmentDefGroupSa,TaskAssignmentDefGroupSa.AssigneeId==auth_group.id).filter(TaskAssignmentDefGroupSa.TaskDefId==self.id,TaskAssignmentDefGroupSa.AssignGroup=='potentialOwners').all()
            #not sure what this part does
          else:
            users=[]
            groups=[]
            
            if self.PotentialOwners is not None and len(self.PotentialOwners)>0:
              users = self.PotentialOwners['potentialOwnersUsers']
              groups = self.PotentialOwners['potentialOwnersGroups']  
            if len(users)==0 and len(groups)==0:
              msg = 'Please select user or group'
              return count_po,msg
         
        #obradi grupu
        if self.TaskParameters.AssignTypeOwnerGroup in ['d']:
        
          rmdFields = []
          rmdFieldsRelated = []
          relatedDatasets = []
          relatedDatasetObjects = []
          directDatasets = []
          taskDatasets = []
          #tittr = TaskInitiationInstanceToResource.objects.filter(TaskInitiateId=self.TaskInitiate)
          tittr = self.Session.query(TaskInitiationInstanceToResourceSa).filter(TaskInitiationInstanceToResourceSa.TaskInitiateId==self.TaskInitiate.id).all()
          
          if tittr.count()>0:
            for rec in tittr:
              taskDatasets.append({'ResourceDefId' : rec.ResourceDefId_id, 'ResourceRecordId' : rec.ResourceId})
          else:
            #go to prev task execute
            #tettr = TaskExecutionInstanceToResource.objects.filter(TaskExecuteId=self.TaskInitiate.PreviousTaskId,ResourceDefId_id__in=datasetsForSearch)
            tettr = self.Session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId==self.TaskInitiate.PreviousTaskId,TaskInitiationInstanceToResourceSa.ResourceDefId.in_(datasetsForSearch)).all()
          
           
            if tettr.count()>0:
              for rec in tettr:
                 taskDatasets.append({'ResourceDefId' : rec.ResourceDefId, 'ResourceRecordId' : rec.ResourceId})
          
          
          
          #pes = PresentationElements.objects.filter(ElementType='group',FormType='i',ResourceType='o',UniqueId=self.TaskParameters.UniqueIdGroup,Status='Active',ResourceDefId_id__in=relatedDatasets).select_related('ResourceModelDefinitionId')
          pes = self.Session.query(PresentationElementsSa,ResourceModelDefinitionSa).join(ResourceModelDefinitionSa,ResourceModelDefinitionSa.id==PresentationElementsSa.ResourceModelDefinitionId).filter(PresentationElementsSa.ElementType=='group',PresentationElementsSa.UniqueId==self.TaskParameters.UniqueIdGroup,PresentationElementsSa.Status=='Active').all()
          
          if pes.count()>0:
            for pe in pes:
              rmdFieldsRelated.append({'ResourceDefId':pe[1].ResourceDefId,'ItemId':pe[1].id,'FieldId': pe[1].FieldName +str(pe[1].id)+'_id'})
                       
          else:
            
            return count_po,'Error retrieving potential owner from dataset--> Unique group identifier not found in dataset definition (271)'

          for field in rmdFields:
            
            datasetRecords = self.rdbms_cls.get_data_filtered(field['ResourceDefId'],self.OrganisationId, {'id':field['ResourceRecordId']} , parameters={})
            for datasetRecord in datasetRecords:
              fieldId = field['FieldId'] 
              groupId = getattr(datasetRecord, fieldId,None)
              groups.append(groupId)  
          
          
          group_ids=[]  
          for rdo in relatedDatasetObjects:
            #first get m2m
            m2mRecords = self.rdbms_cls.get_m2m_data(rdo['ResourceDefId'],rdo['RelatedId'],rdo['ResourceRecordId'],rdo['ItemId'],self.OrganisationId,0) #resource_source_id,related_id,resource_record_id,model_id,type
            
            m2mItems = []
            for item in m2mRecords:
              m2mItems.append(item.ResourceRelatedId)
              
            datasetRecords = self.rdbms_cls.get_data_filtered(rdo['RelatedId'],self.OrganisationId,{'id__in':m2mItems}, parameters={})
               
            for relatedField in rmdFieldsRelated:
              for datasetRecord in datasetRecords:
                fieldId = relatedField['FieldId'] 
                groupId = getattr(datasetRecord, fieldId,None)
                #group_ids.append(groupId)  

                #gusers = User.objects.filter(groups__id=groupId)
                gusers = self.Session.query(auth_user_groups).filter(auth_user_groups.group==groupId).all()
            
            
                for guser in gusers:
                  if guser.user not in users_ids: 
                    #TaskAssignments.objects.create(UserId = guser, ForGroup_id=groupId,TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=self.User,CreatedDateTime=timezone.now(),Claimed=False)
                    ta = TaskAssignmentsSa(UserId = guser.user, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
                    self.Session.add(ta)
                    count_po=count_po+1
         
        
        elif self.TaskParameters.AssignTypeOwner not in ['i','o','d']:  
          #insert users
          for user in users:
              if type(user)==dict:
                user_id = user['id']
                users_ids.append(user_id)
              else:
                user_id = user[0].id
                users_ids.append(user_id)
              #user_obj= User.objects.get(id=user['id'])
              #TaskAssignments.objects.create(UserId =user_obj, TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=user_obj,CreatedDateTime=timezone.now(),Claimed=False)
              ta = TaskAssignmentsSa(UserId = user_id, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=user_id,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
              self.Session.add(ta)
              count_po=count_po+1
        
          #group_ids=[]
          #get users from groups
          for group in groups:
            #get users that have this group
            #gusers = User.objects.filter(groups__id=group['id'])
            if type(group)==dict:
                group_id = group['id']
            else:
               group_id = group[0].id

            gusers = self.Session.query(auth_user_groups).filter(auth_user_groups.group==group_id).all()
            for guser in gusers:
              #if guser.id not in users_ids: 
              #TaskAssignments.objects.create(UserId = guser, ForGroup_id=group['id'],TaskInitiationId = self.TaskInitiate, AssignGroup = 'potentialOwners', CreatedBy=guser,CreatedDateTime=timezone.now(),Claimed=False)
              ta = TaskAssignmentsSa(UserId = guser.user,ForGroup=group_id, TaskInitiationId = self.TaskInitiate.id, AssignGroup = 'potentialOwners', CreatedBy=guser.user,CreatedDateTime=datetime.now(timezone.utc),Claimed=False)
              self.Session.add(ta)
              count_po=count_po+1

        if count_po>0:
          user_id = None
          if self.User is not None:
            user_id=self.User.id
          self.TaskInitiate.Status=self.get_status_code_by_name('ready','taskstatuses')
          self.TaskInitiate.ChangedBy = user_id
          self.TaskInitiate.ChangedDateTime=datetime.now(timezone.utc)
          #self.TaskInitiate.save()
        
        return count_po,msg

    def process_task_status_change (self,taskAction,for_group):

            tii = None
            status=201
          
          
            msg=''
            status=200
            tiis=None
            teis = None

         
            if (self.TaskInitiate.id>0):
              
              #tiis =  TaskInitiationInstance.objects.filter(id=self.TaskInitiate.id).select_related()
              tiis = self.Session.query(TaskInitiationInstanceSa,TaskExecutionInstancesSa).outerjoin(TaskExecutionInstancesSa,TaskExecutionInstancesSa.TaskInitiateId==TaskInitiationInstanceSa.id).filter(TaskInitiationInstanceSa.id==self.TaskInitiate.id).all()
         
              for tii in tiis:
                if tii:
                  taskAction_code_id = self.get_status_code_by_name(taskAction,'taskstatuses')
                  
                  fromStatus = tii[1].Status
                  td = self.Session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==tii[0].TaskDefId).all()
                  #td = TaskParameters.objects.get(ResourceDefId=tii.TaskDefId)
                  
                  if (taskAction!=fromStatus):
                      if (taskAction=='claim'):
                        if tii[0].Status.Name in ['ready','InProgress']:
                          if self.TaskParameters.AutoStart:
                            taskAction='InProgress'
                          
                          taskAction_code_id = self.get_status_code_by_name(taskAction,'taskstatuses')
                          tii[0].Status=taskAction_code_id
                          tii[0].ChangeBy=self.User.id
                          tii[0].ChangeByDateTime = datetime.now(timezone.utc)
                          
                          
                          #tei = TaskExecutionInstances.objects.create( TaskDefId = tii.TaskDefId, TaskInitiateId = tii, Status_id = taskAction_code_id,ActualOwner = self.User,
                          #                               CreatedBy = self.User, CreatedDateTime=timezone.now())
                          tei = TaskExecutionInstancesSa( TaskDefId = tii[0].TaskDefId, TaskInitiateId = tii[0].id, Status = taskAction_code_id,ActualOwner = self.User.id,
                                                         CreatedBy = self.User.id, CreatedDateTime=datetime.now(timezone.utc))


                          self.Session.add(tei)
                          self.Session.flush()
                          taskType_code_id = self.get_status_code_by_name('oneinstancepergroup','_TASK_TYPES_')
                          
                          if (self.TaskParameters.TaskType==taskType_code_id):
                          
                            #TaskAssignments.objects.filter(TaskInitiationId = tii,ForGroup=for_group).update(Claimed=True, ChangedBy = self.User, ChangedDateTime=timezone.now())
                            tas = self.Session.query(TaskAssignmentsSa).filter(TaskAssignmentsSa.TaskInitiationId==tii[0].id,TaskAssignmentsSa.ForGroup==for_group).all()

                          else:
                            #TaskAssignments.objects.filter(TaskInitiationId = tii).update(Claimed=True, ChangedBy = self.User, ChangedDateTime=timezone.now())
                            tas = self.Session.query(TaskAssignmentsSa).filter(TaskAssignmentsSa.TaskInitiationId==tii[0].id).all()
                          
                          for ta in tas:
                            ta.Claimed=True
                            ta.ChangedBy=self.User.id
                            ta.ChangedDateTime = datetime.now(timezone.utc)

                          if tei.id is not None:
                            #tii.save()
                            
                            rval = self.process_task_action(fromStatus,taskAction)      
                            
                            if rval['status']!=200:
                              msg = 'Error processing task status actions '+rval['msg']
                              status= rval['status']
                              return msg,status,None,None

                            msg = 'Task claimed successfuly'
                            #teis = TaskExecutionInstances.objects.filter(id=tei.id)
                            teis = self.Session.query(TaskExecutionInstancesSa,TaskInitiationInstanceSa).outerjoin(TaskInitiationInstanceSa,TaskExecutionInstancesSa.TaskInitiateId==TaskInitiationInstanceSa.id).filter(TaskExecutionInstancesSa.id==tei.id).all()
         
                          else:
                            msg = 'Invalid task status '+taskAction
                            status=500
                            return msg,status,None,None
                      else:
                        
                        #teis = TaskExecutionInstances.objects.filter(id = task_execute_id).select_related()
                        teis = self.Session.query(TaskExecutionInstancesSa,TaskInitiationInstanceSa).outerjoin(TaskInitiationInstanceSa,TaskExecutionInstancesSa.TaskInitiateId==TaskInitiationInstanceSa.id).filter(TaskExecutionInstancesSa.id==task_execute_id).all()
         
                        for tei in teis:
                          
                          fromStatus = tei.Status.Name
                          taskAction_code_id = self.get_status_code_by_name(taskAction,'taskstatuses')
                          tei[0].ChangedBy=self.User.id
                          tei[0].ChangedDateTime = datetime.now(timezone.utc)
                          tei[0].Status=taskAction_code_id
                          rval = self.process_task_action(fromStatus,taskAction)       
                          if rval['status']!=200:
                            msg = 'Error processing task status actions '+rval['msg']
                            status= rval['status']
                            return msg,status,None,None
                          tei.save()
                          msg = 'Task status successfuly changed from '+ fromStatus +' to '+taskAction
                  #process task         
                  else:
                    msg='Task allready in status '+fromStatus
                    status=400
                    return msg,status,None,None
                  
                else:
                  msg='Task initiate instance not found'
                  status=400
                  return msg,status,None,None
                    
            else:
                #teis = TaskExecutionInstances.objects.filter(id = task_execute_id).select_related()
                teis = self.Session.query(TaskExecutionInstancesSa,TaskInitiationInstanceSa).outerjoin(TaskInitiationInstanceSa,TaskExecutionInstancesSa.TaskInitiateId==TaskInitiationInstanceSa.id).filter(TaskExecutionInstancesSa.id==task_execute_id).all()
         
                taskAction_code_id = self.get_status_code_by_name(taskAction,'taskstatuses')
                if len(teis)==0:
                  msg='Task execution instance does not exist! | Task id='+str(task_execute_id)
                  status=500
                  return msg,status,None,None
                for tei in teis:
                  fromStatus = tei[0].Status
                  if (taskAction_code_id!=fromStatus.id):
                    tei[0].ChangedBy=self.User.id
                    tei[0].ChangedDateTime = datetime.now(timezone.utc)
                    tei[0].Status=taskAction_code_id
                    
                    rval = self.process_task_action(fromStatus,taskAction) 
                    if rval['status']!=200:
                        msg = 'Error processing task status actions '+rval['msg']
                        status=rval['status']
                        self.Session.rollback()
                        return msg,status,None,None
                    msg = 'Task status successfuly changed from '+ str(fromStatus.Name) +' to '+str(taskAction)
                  else:
                    msg='Task already in status '+fromStatus.Name
                    status=400
                    return msg,status,None,None
                
            #ToDo - implement audit trail later
            #if tii is None:
                  #ToDo - migrate to SQLAlchemy
            #      tiis = TaskInitiationInstance.objects.filter(id=tei.TaskInitiateId_id)
               
            #      if len(tiis)>0:
            #        tii = tiis[0]
            #ToDo - migrate to SQLAlchemy - for now comment
            #atts = AuditTrailTaskStatus.objects.create(TaskInitiateId = tii, TaskExecuteId = tei,TypeOfRecord = '', Activity = 'Task status change', UserId = user,
            #                                            ActivityDate = timezone.now(), FromStatus = fromStatus , ToStatus = taskAction,   Comment = '')
           
            return msg,status,tiis,teis

      
    def process_outcome (self):
        
        rval={}
        rval['status']=200
        rval['msg'] = ''
        code_id1 =outcome_id
        #code_id2 = self.get_status_code_by_name(code_detail_name2,'taskstatuses')

        rval['CodeId1']=code_id1
        #rval['CodeId2']=code_id1
        #actions = TaskCodesActions.objects.filter (TaskDefId = TaskExecute.TaskDefId,CodeName1='taskoutcomes',OutcomeId_id=code_id1)
        actions = self.Session.query(TaskCodesActionsSa).filter(TaskCodesActionsSa.TaskDefId == self.id,TaskCodesActionsSa.CodeName1=='taskoutcomes',TaskCodesActionsSa.OutcomeId==code_id1).all()
         
        for action in actions:
         
          if action.ActionType in ['1']:
           
            rval = self.auto_process_task()        
          elif action.ActionType in ['2']:
            rval = self.auto_process_notification(action.actionParams.notificationId,prev_task_id,parent_dataset_id,parent_dataset_record_id,lang,org_id,current_user)  
            
        return rval
      
    def process_task_action (self,code_detail_name1,code_detail_name2):
        
        rval={}
        rval['status']=200
        rval['msg'] = ''
        code_id1 = self.get_status_code_by_name(code_detail_name1,'taskstatuses')
        code_id2 = self.get_status_code_by_name(code_detail_name2,'taskstatuses')

        rval['CodeId1']=code_id1
        rval['CodeId2']=code_id2
        #actions = TaskCodesActions.objects.filter (TaskDefId_id = self.id,CodeName1='taskstatuses',CodeId1_id=code_id1,CodeId2_id=code_id2)
        actions = self.Session.query(TaskCodesActionsSa).filter(TaskCodesActionsSa.TaskDefId == self.id,TaskCodesActionsSa.CodeName1=='taskstatuses',TaskCodesActionsSa.CodeId1==code_id1,TaskCodesActionsSa.CodeId2==code_id2).all()
        
        
        for action in actions:
          
          if action.ActionType in ['1']:
              
            rval = self.auto_process_task(action.InitiateTaskId)        
            
          elif action.ActionType in ['2']:
            
            rval = self.auto_process_notification(action.ActionParams['notificationId'],prev_task_id,parent_dataset_id,parent_dataset_record_id,task_type,lang,org_id,current_user)
        return rval


    def auto_process_task (self,task_def_id):
          
        #get data for Initiate task
        potential_owners = []
        outcome_id=0
        form_type='i'
        formDataDict={}

        #ToDo - change this completely to classes

        #get all presentationElements
        #pes = PresentationElements.objects.filter(ElementType__in=['add','update','view','mupdate','madd'],ResourceDefId_id=task_def_id,FormType=self.FormType,Status='Active')
        pes = self.Session.query(PresentationElementsSa,ResourceModelDefinitionSa).join(ResourceModelDefinitionSa,ResourceModelDefinitionSa.id==PresentationElementsSa.ResourceModelDefinitionId).filter(PresentationElementsSa.ElementType.in_(['add','update','view','mupdate','madd']),PresentationElementsSa.FormType==self.FormType,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceDefId==task_def_id).all()
          
        for pe in pes:
            #ToDo
            formData,recordId = GetDatasetFormData(pe[0].Related,self.FormType,'w',task_def_id,0,0,pe[0].PopulateType,0,org_id,current_user)

            #formData,recordId = GetDatasetFormData(resource_id,form_type,override_type,task_def_id,src_task_instance_id,parent_id,populate_type,resource_record_id,user_id)

            formDataDict.update({str(pe[0].id):formData})
        #dataset data is needed here

        transaction_id = 0
        #create new task object here - ToDo - this needs fixing
        rvalInit = self.initiate_task ();
        
        return rvalInit
        #return True

    def auto_execute_task (self,task_execute_id):

        #get taskData

        potential_owners = []
        #outcome_id=0
        form_type='e'
        #prev_task_id=0
        formDataDict={}

        #get all presentationElements
        #pes = PresentationElements.objects.filter(ElementType__in=['add','update','view','mupdate','madd'],ResourceDefId_id=task_def_id.id,FormType=form_type,Status='Active')
        pes = self.Session.query(PresentationElementsSa,ResourceModelDefinitionSa).join(ResourceModelDefinitionSa,ResourceModelDefinitionSa.id==PresentationElementsSa.ResourceModelDefinitionId).filter(PresentationElementsSa.ElementType.in_(['add','update','view','mupdate','madd']),PresentationElementsSa.FormType==self.FormType,PresentationElementsSa.Status=='Active',PresentationElementsSa.ResourceDefId==self.id).all()
        
        for pe in pes:
            #ToDo
            formData,recordId = GetDatasetFormData(pe[0].Related,form_type,'w',task_def_id,0,0,pe[0].PopulateType,0,org_id,current_user)
            
            
            formDataDict.update({str(pe.id):formData})

        
        rValExe = self.execute_task (task_execute_id,0,0,formDataDict);

        ret_message=rValExe[1]
        if ret_message=='' or ret_message is None:
          ret_message='Task executed successfuly!'
          
        return {'form_status': rValExe[0],'data':{'msg':rValExe[1]},'status':rValExe[2]}
        
    def process_previous_task (self):
      
        formData=None
        src_task_instance_id=0
      
       
        #teis = TaskExecutionInstances.objects.filter(id=self.PreviousTaskId)
        teis = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==self.PreviousTaskId).all()
             
        for tei in teis:
          formData = tei.FormData

        if formData is None:
          formData={}
        
        rvalExe = self.execute_task()
        
        return rvalExe


    def initiate_task (self):
            
            
        
        msg = ''
        rVal = {'data':'','status':200,'msg':msg}
        
        exeLimit=0
        
        exeLimit = self.TaskParameters.MaxActualOwners
        
        if self.TaskParameters.NoLimit:
          exeLimit = 0
          
        if self.TaskParameters.SelfAssigned:
          exeLimit = 1

        
        code_status_id = self.get_status_code_by_name('created','taskstatuses')
        code_status_id_completed = self.get_status_code_by_name('completed','taskstatuses')
        
        #find initiate instance for user
        #tiis = TaskInitiationInstance.objects.filter(TaskDefId=self.id,InitiatorId=self.User).exclude(Status_id=code_status_id).exclude(Status_id=code_status_id_completed)
        user_id = None
        if self.User is not None:
          user_id = self.User.id
        tiis = self.Session.query(TaskInitiationInstanceSa).filter(TaskInitiationInstanceSa.TaskDefId==self.id,TaskInitiationInstanceSa.Status!=code_status_id,TaskInitiationInstanceSa.Status!=code_status_id_completed)

        if user_id is not None:
          tiis = tiis.filter(TaskInitiationInstanceSa.InitiatorId==user_id)

        tiis = tiis.all()
        tii_ser = None
        tei_ser = None
        rid = None
        formDefinitionInJson=None
        name=''
        formDefined=None
        pif = None
        

        if self.TaskParameters.AutoInitiate and len(tiis)>0:
          self.TaskInitiate=tiis[0]
          #check if there is execute instance
          #teis = TaskExecutionInstances.objects.filter(TaskDefId_id = self.id, TaskInitiateId = self.TaskInitiate).exclude(Status_id=code_status_id_completed)
          teis = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.TaskDefId==self.id,TaskExecutionInstancesSa.TaskInitiateId==self.TaskInitiate.id,TaskExecutionInstancesSa.Status!=code_status_id_completed).all()
          if len(teis)>0:
            for tei in teis:
              tii_ser,tei_ser,rid,formDefinitionInJson,resourceDefinition = self.pre_execute_task(0,tei.id)
            
            if tii_ser is not None:
              
              return  {'data':{'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser,'taskExecuteInstance':tei_ser,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson},'status':200}
            else:
              return  {'data':{'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson},'status':200}
          
              
        #create 
        elif len(tiis)==0 or not self.TaskParameters.AutoInitiate:
          
          self.create_task_initiate_instance(exeLimit,code_status_id)

          
                
          if not self.TaskParameters.AutoInitiate:
            pif,msg_form = self.process_form() 
            
            if pif is None:
               rVal['data'] = {'msg':'Form processing failed '+msg}
               rVal['status']=500
               return rVal
            else:
              for message in msg_form:
                msg = msg + ' --> ' + message
             
         
        ta,msg_assign = self.process_task_assignments()

        if ta==0:
           rVal['data'] = {'msg':'Task assignments failed --> '+msg_assign}
           rVal['status']=500
           return rVal

        ptp,tei_created,msg_param,status = self.process_task_parameters(ta) 
        if status!=200:
          rVal['data'] = {'msg':'Task parameters processing failed -->'+msg+msg_param}
          rVal['status']=status
          return rVal
           
        if tei_created:
           task_execute_id = ptp.id
           tii_ser,tei_ser,rid,formDefinitionInJson,resourceDefinition = self.pre_execute_task(0,task_execute_id)  
        
        else:
           task_execute_id = 0
       
        pTask=None
        
        if self.PreviousTaskId>0 and self.OutcomeId>0:
          
           pTask,msg_p,status = self.process_previous_task()
           
           if pTask is None:
               rVal['data'] = {'msg':'Previous task processing failed-->'+msg_p}
               rVal['status']=500
               return rVal
           else:
             if status!=200:
               rVal['data'] = {'msg':'Previous task processing failed -->'+msg_p}
               rVal['status']=status
               return rVal
        rValExe = None
        if self.TaskParameters.AutoExecute:
         #auto-execute processing
         #select default outcome
         outcome_id = self.TaskParameters.DefaultOutcomeId
         
         rValExe = self.auto_execute_task(task_execute_id)
         if 'data' in rValExe:
           if 'msg' in rValExe['data']:
            msg= msg +' '+rValExe['data']['msg']
            
            rValExe['data']['status']=200
            
            if tii_ser is not None:
              rValExe['data']['taskInitiateInstance'] = tii_ser
            if tei_ser is not None:
              rValExe['data']['taskExecuteInstance']=tei_ser
           
         #return rValExe
        
        if self.TaskParameters.AutoInitiate:
          
          if tii_ser is not None and tei_ser is not None:
            return  {'data':{'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser,'taskExecuteInstance':tei_ser,'ret_val_exe':rValExe,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson},'status':200}
          elif tii_ser is None and tei_ser is not None:
            return  {'data':{'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser,'ret_val_exe':rValExe,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson},'status':200}
          elif tii_ser is not None and tei_ser is None:
            return  {'data':{'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser,'taskExecuteInstance':[],'ret_val_exe':rValExe,'taskdef':self.get_task_parameters_json(),'widgets':formDefinitionInJson},'status':200}
          else:
            return  {'data':{'resourceDefinition': resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':[],'ret_val_exe':rValExe,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson},'status':200}
          
        else:
            if self.TaskInitiate is not None:
              #tiis = TaskInitiationInstance.objects.filter(id=self.TaskInitiate.id)
              tiis = self.Session.query(TaskInitiationInstanceSa).filter(TaskInitiationInstanceSa.id==self.TaskInitiate.id).all()
          
              tii_ser =TaskInitiationInstanceJson(tiis)
              #tii_ser.is_valid()
            else:
              tii_ser=[]
              
            rVal['data']= {'msg':'Task '+str(self.TaskInitiate.id)+' initiated successfuly '+msg,'ret_value':pif,'taskInitiateInstance':tii_ser,'taskExecuteInstance':[],'ret_val_exe':rValExe,'taskdef': self.get_task_parameters_json(),'task_execute_id':task_execute_id,'task_initiate_id':self.TaskInitiate.id,'previous_task':pTask}
            rVal['status']=200
            return rVal
       
    def pre_execute_task(self,task_initiate_id,task_execute_id):
      
        resource_id=None
        parent_id=None
        tiis = {}
        teis = {}
        formDefined= False
        tii_ser=None
        tei_ser=None
        formDefinitionInJson=None
        name=None
            
        if task_execute_id==0:
          #tiis = TaskInitiationInstance.objects.filter(id = task_initiate_id).select_related()
          tiis = self.Session.query(TaskInitiationInstanceSa).filter(TaskInitiationInstanceSa.id==task_initiate_id).all()
          for tii in tiis:
            resource_id = tii.TaskDefId
            #teis = TaskExecutionInstances.objects.filter(TaskInitiateId_id = task_initiate_id).select_related()
            #tei_ser =TaskExecutionInstancesSerializer(data=teis,many=True)
            #tei_ser.is_valid() 
          tii_ser =TaskInitiationInstanceJson(tiis)
          #tii_ser.is_valid() 
        else:
          #teis = TaskExecutionInstances.objects.filter(id = task_execute_id).select_related()
          teis = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==task_execute_id).all()
          for tei in teis:
            resource_id = tei.TaskDefId
            
          tei_ser =TaskExecutionInstancesJson(teis)
          #tei_ser.is_valid() 
        
        form_type='e'
        self.get_form_definition(form_type)
        self.generate_form()
        formDefinitionInJson = self.get_form_json()
        self.get_definition()
        resourceDefinition = self.get_definition_json()
        return tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition
            
    def execute_task (self):
            
        msg=''
        
        if self.TaskData is not None:
          pef,msg_form = self.process_form() 
          if pef is None:
              msg = 'Form processing failed '
              status=500
              return pef,msg,status
          else:
            for message in msg_form:
              if message is not None and message!='':
                if msg=='':
                  msg = message
                else:
                  msg = msg + ' --> ' + message
           
        fromStatus = self.TaskExecute.Status

        if self.OutcomeId==0:
          self.OutcomeId=None
        if self.OutcomeId is not None:
          status='completed'
          status_code_id = self.get_status_code_by_name(status,'taskstatuses');
          self.TaskExecute.Status = status_code_id
          self.TaskExecute.TaskOutcomeCodeItemId = self.OutcomeId
          self.TaskExecute.ChangedBy = self.User.id
          self.TaskExecute.ChangeDateTime = datetime.now(timezone.utc)
          
        #Process outcome
        if self.TaskParameters.AutoExecute:
          rval_outcome = self.process_outcome()
          if rval_outcome['status']!=200:
              
              msg = 'Error processing task outcome actions ExecuteTask -->'+rval_outcome['msg']
              status = rval_outcome['status']
              return pef,msg,status

        if fromStatus != self.TaskExecute.Status:
 
          rval = self.process_task_action(fromStatus,status)
          if 'CodeId2' in rval:
            self.TaskExecute.Status = rval['CodeId2']
            save_task=True
          else:
            rval['status'] = 500
          if rval['status']!=200:
              msg = 'Error processing task status actions ExecuteTask -->'+rval['msg']
              status = rval['status']
              return pef,msg,status

          #ToDo - implement audit trail later
          #atts = AuditTrailTaskStatus.objects.create(TaskInitiateId_id = self.TaskExecute.TaskInitiateId_id, TaskExecuteId = self.TaskExecute,TypeOfRecord = '', Activity = 'Task status change', UserId = self.User,
           #                                          ActivityDate = timezone.now(), FromStatus = fromStatus , ToStatus = self.TaskExecute.Status,   Comment = '')
           
          
        
        if 'ResponseMsg' in self.TaskParameters.Parameters:
          msg = self.TaskDefinition.Parameters['ResponseMsg']+' '+msg
        
        status=200
        
        return pef,msg,status
            
                
    def process_task_parameters (self,count_po):
      
        status=''
        #actual_owners = User.objects.filter(tauserid__TaskInitiationId=self.TaskInitiate)
        actual_owners = self.Session.query(TaskAssignmentsSa).filter(TaskAssignmentsSa.TaskInitiationId==self.TaskInitiate.id).all()

        tei_created=False
        tei=None
        retStatus=200
        msg='Task parameters processing successful'
        
        status='ready'
        fromStatus = status
        status_code_id = self.get_status_code_by_name(status,'taskstatuses');
        if self.TaskInitiate.Status==status_code_id and self.TaskParameters.AutoClaim:
            status='claim'
        if self.TaskParameters.AutoStart:
            status='InProgress'

        status_code_id = self.get_status_code_by_name(status,'taskstatuses');   
        
        #cds = CodesDetail.objects.filter(id=self.TaskParameters.TaskType_id)#.select_related('CodesDetailHeadId')
        cds = self.Session.query(CodesDetailSa).filter(CodesDetailSa.id==self.TaskParameters.TaskType).all()

        user_id = None
        if self.User is not None:
          user_id = self.User.id

        for cd in cds:       
          if cd.Name=='oneinstance':
            
            status_code_created = 'created'
            status_code_ready = 'ready'
            
            rval = self.process_task_action(status_code_created,status_code_ready)      
            if rval['status']!=200:
              msg = 'Error processing task status actions for one instance --> created to ready '+rval['msg']
              retStatus= rval['status']
              return tei,tei_created,msg,retStatus
            if count_po==1:
              #insert TaskExeInstance
              
              #tei = TaskExecutionInstances.objects.create( TaskDefId_id = self.id, TaskInitiateId = self.TaskInitiate, Status_id = status_code_id,ActualOwner = actual_owners[0],
               #                                    CreatedBy = self.User, CreatedDateTime=timezone.now())
              tei = TaskExecutionInstancesSa(TaskDefId = self.id, TaskInitiateId = self.TaskInitiate.id, Status = status_code_id,ActualOwner = actual_owners[0].UserId,
                                                   CreatedBy = user_id, CreatedDateTime=datetime.now(timezone.utc))
             
              self.Session.add(tei)
              self.Session.flush()
              tei_created=True
              rval = self.process_task_action(fromStatus,status)      
              if rval['status']!=200:
                  msg = 'Error processing task status actions for one instance --> '+rval['msg']
                  retStatus= rval['status']
                  return tei,tei_created,msg,retStatus
              
              #msg = 'Task status successfuly changed from '+ fromStatus +' to '+taskAction
          if cd.Name=='oneinstanceperuser':
            for owner in actual_owners:
              #ToDo - migrate to SqlAlchemy
              #tei = TaskExecutionInstances.objects.create( TaskDefId_id = self.id, TaskInitiateId = self.TaskInitiate, Status_id = status_code_id,ActualOwner = owner,
               #                                    CreatedBy = self.User, CreatedDateTime=timezone.now())
              tei = TaskExecutionInstancesSa(TaskDefId = self.id, TaskInitiateId = self.TaskInitiate.id, Status = status_code_id,ActualOwner = actual_owners[0].AssigneeId,
                                                   CreatedBy = user_id, CreatedDateTime=datetime.now(timezone.utc))
              self.Session.add(tei)
              self.Session.flush()
              tei_created=True 
            
              #rval = self.process_task_action(tei,tei.id,fromStatus,status,0,0,lang,user_id)  
              rval = self.process_task_action(fromStatus,status)   # Da li je user_id ili current_user ???       
              if rval['status']!=200:
                  msg = 'Error processing task status actions for one instance per user --> '+rval['msg']
                  retStatus = rval['status']
                  return tei,tei_created,msg,retStatus
                
        if self.TaskParameters.AutoInitiate and not tei_created:
            
            #ToDo - migrate to SqlAlchemy
            #tei = TaskExecutionInstances.objects.create( TaskDefId_id = self.id, TaskInitiateId = self.TaskInitiate, Status_id = status_code_id,ActualOwner = actual_owners[0],
            #                                       CreatedBy = self.User, CreatedDateTime=timezone.now())
            tei = TaskExecutionInstancesSa(TaskDefId = self.id, TaskInitiateId = self.TaskInitiate.id, Status = status_code_id,ActualOwner = actual_owners[0].AssigneeId,
                                                   CreatedBy = user_id, CreatedDateTime=datetime.now(timezone.utc))

            self.Session.add(tei)
            self.Session.flush()
            tei_created=True
              #def self.process_task_action (taskExecuteObject,prev_task_id,code_detail_name1,code_detail_name2,parent_dataset_id,parent_dataset_record_id,lang,org_id,current_user):
            rval = self.process_task_action(fromStatus,status)  #da li je user_id ili current_user???
            #rval = self.process_task_action(tei,tei.id,fromStatus,status,0,0,lang,user_id)     
            if rval['status']!=200:
                msg = 'Error processing task status actions '+rval['msg']
                retStatus= rval['status']
                return tei,tei_created,msg,retStatus

                
        if tei_created:
            status_code_id = self.get_status_code_by_name(status,'taskstatuses');
            #tii = TaskInitiationInstance.objects.filter(id=self.TaskInitiate.id).update(Status_id=status_code_id)
            tiis = self.Session.query(TaskInitiationInstanceSa).filter(TaskInitiationInstanceSa.id==self.TaskInitiate.id).all()
            for tii in tiis:
              tii.Status=status_code_id

        return tei,tei_created,msg,retStatus

    def is_user_authorised_to_create_task (self):
      
       

          #Check assignment type
          assign_type_init = self.TaskParameters.AssignTypeInitiator
          #user from previous task
          if self.PreviousTaskId>0 and assign_type_init in ['o','i']:
              if assign_type_init=='o':
                #ToDo - migrate to SqlAlchemy
                #teis = TaskExecutionInstances.objects.filter(id=self.PreviousTaskId,ActualOwner_id=self.User.id)
                #check how to count
                teis = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==self.PreviousTaskId,TaskExecutionInstancesSa.ActualOwner==self.User.id).all()
                if len(teis)>0:
                  return True
                else:
                  return False
              elif assign_type_init=='i':
                #ToDo - migrate to SqlAlchemy
               #teis = TaskExecutionInstances.objects.filter(id=self.PreviousTaskId,TaskInitiateId__InitiatorId_id=self.User.id)
                teis = self.Session.query(TaskExecutionInstancesSa,TaskInitiationInstanceSa).join(TaskInitiationInstanceSa,and_(TaskInitiationInstanceSa.id==TaskExecutionInstancesSa.TaskInitiateId,TaskInitiationInstanceSa.InitiatorId==self.User.id)).filter(TaskExecutionInstancesSa.id==self.PreviousTaskId).all()
                if len(teis)>0:
                  return True
                else:
                  return False
        
        
          #Find users and groups for this task
          #ToDo - migrate to SqlAlchemy
          #tadu = TaskAssignmentDefUser.objects.filter(AssignGroup='taskInitiators',TaskDefId_id=self.id,AssigneeId=self.User)
          tadu = self.Session.query(TaskAssignmentDefUserSa).filter(TaskAssignmentDefUserSa.AssignGroup=='taskInitiators',TaskAssignmentDefUserSa.TaskDefId==self.id,TaskAssignmentDefUserSa.AssigneeId==self.User.id).all()
          if len(tadu)>0:
            if tadu[0].AssignType=='a':
              return True
            elif tadu[0].AssignType=='e':
              return False
            
          #check groups
          #ToDo - migrate to SqlAlchemy
          #tadg = TaskAssignmentDefGroup.objects.filter(AssignGroup='taskInitiators',TaskDefId_id=self.id)
          tadg = self.Session.query(TaskAssignmentDefGroupSa).filter(TaskAssignmentDefGroupSa.AssignGroup=='taskInitiators',TaskAssignmentDefGroupSa.TaskDefId==self.id).all()
          group_ids=[]
          group_ids_no=[]
          for group in tadg:
            if group.AssignType =='a':
              group_ids.append(group.AssigneeId)
            elif group.AssignType =='e':
              group_ids_no.append(group.AssigneeId)
          
          if len(group_ids)==0:
            return False
          else:
            #get all users for e
            
            #gusers_no = self.User.groups.filter(id__in=group_ids_no)
            gusers_no = self.Session.query(auth_user_groups).filter(auth_user_groups.user==self.User.id,auth_user_groups.group.in_(group_ids_no)).all()
            if len(gusers_no)>0:
              return False
            else:
              #gusers = self.User.groups.filter(id__in=group_ids)
              gusers = self.Session.query(auth_user_groups).filter(auth_user_groups.user==self.User.id,auth_user_groups.group.in_(group_ids)).all()
              if len(gusers)>0:
                return True
              else:
                return False

    def start_task (self):
        
        #StartTask
        
        
        #with self.Session.begin():
       

          deleteDate =  datetime.now(timezone.utc) - timedelta(minutes=600)
          #cleanup transactions
          #ttCleanup = TaskTransactions.objects.filter(CreatedDateTime__lt=deleteDate,CreatedBy_id=self.User.id).delete()
          ttCleanup = self.Session.query(TaskTransactionsSa).filter(TaskTransactionsSa.CreatedDateTime<deleteDate,TaskTransactionsSa.CreatedBy==self.User.id).delete()

          response_status=200 
          response = None
          
          is_authorised = self.is_user_authorised_to_create_task()
          if not is_authorised:
            response =  {'msg':'You are NOT authorised to create task with id '+str(self.id)}
            response_status=403
            return response,response_status

          if self.PreviousTaskId>0:
            if self.OutcomeId>0:
              #save outcome in previous task
              #tei = TaskExecutionInstances.objects.filter(id=self.PreviousTaskId).update(TaskOutcomeCodeItemId_id=self.OutcomeId)
              teis = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id==self.PreviousTaskId).all()
              
              for tei in teis:
                tei.TaskOutcomeCodeItemId=self.OutcomeId

          task_initiate_id=0
          task_execute_id=0
          self.TaskTransactionId=str(self.OrganisationId)+'-'+str(self.User.id)+'-'+str(self.id)+'-'+str(task_initiate_id)+'-'+str(task_execute_id)+'-'+self.Guid
          
          #if not self.TaskParameters.AutoInitiate start form with task transaction-id with data must be returned
          
          if not self.TaskParameters.AutoInitiate:
          
            #generate unique task transaction id

            formDefined = False
            name=''
            formDefinition=[]
            form_type='i'
            #ToDo - do this via class

            #formDefinition,resourceDefinition = GetResourceFormDB(task_id,form_type,language,0,0,'n',user,org_id,False)

            self.get_form_definition(form_type)
            self.generate_form()
            self.get_definition()
            formDefinition = self.get_form_json()
            resourceDefinition = self.get_definition_json()
            response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskdef': self.get_task_parameters_json(),'widgets':formDefinition,'procType':2,'formType':form_type}
           
          if self.TaskParameters.AutoInitiate:
              #if self.TaskParameters.InfiniteTask:
              #this is task that can not be completed
              formDefined = False
              name=''
              formDefinition=[]
            
              #find existing task
              code_status_inprogress = self.get_status_code_by_name('InProgress','taskstatuses')
              #texe = TaskExecutionInstances.objects.filter(TaskDefId = self.id,ActualOwner = self.User,Status=code_status_inprogress)
              texe = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.TaskDefId==self.id,TaskExecutionInstancesSa.ActualOwner==self.User.id,TaskExecutionInstancesSa.Status==code_status_inprogress).all()
              #if exe instance not found search for tasks to claim
              if len(texe)==0:
                #first search for initiate instance
                code_status_ready = self.get_status_code_by_name('ready','taskstatuses')
          
                ta_ids=[]
                #tas = TaskAssignments.objects.filter(UserId=self.User.id,AssignGroup='potentialOwners',TaskInitiationId__Status_id=code_status_ready).select_related('TaskInitiationId')
                tas = self.Session.query(TaskAssignmentsSa).join(TaskInitiationInstanceSa,and_(TaskAssignmentsSa.TaskInitiationId==TaskInitiationInstanceSa.id,TaskInitiationInstanceSa.Status==code_status_ready)).filter(TaskAssignmentsSa.UserId==self.User.id,TaskAssignmentsSa.AssignGroup=='potentialOwners').all()

                for ta in tas:
                  ta_ids.append(ta.TaskInitiationId)
                
                #tinit = TaskInitiationInstance.objects.select_related('TaskDefId','InitiatorId','Status').filter(TaskDefId_id=self.id,id__in=ta_ids,TaskDefId__resourcedeflang__Lang=self.Language,Status_id=code_status_ready).order_by('-id')
                #maybe join with resourcedef,resourcedeflang
                tinit = self.Session.query(TaskInitiationInstanceSa).filter(TaskInitiationInstanceSa.id.in_(ta_ids),TaskInitiationInstanceSa.TaskDefId==self.id,TaskExecutionInstancesSa.Status==code_status_ready).all()
             
                #initiate instance does not exist, create new task
                if len(tinit)==0:
                  #initiate task
                  form_type='e'
                  formDefinition = []
                  #outcome_id = 0
                  
                  #not initiate task or maybe yes   
                  rval = self.initiate_task ()
                  if 'data' in rval:
                    
                    response = rval['data']
                    response.update({'procType':3,'formType':form_type,'transactionId':self.TaskTransactionId})
                    response_status = rval['status']
                    return response,response_status    
                  elif 'msg' in rval:
                    response = rval['msg']
                    response.update({'procType':3,'formType':form_type,'transactionId':self.TaskTransactionId})
                    response_status = rval['status']
                    return response,response_status
             
                if len(tinit)>0:
                  form_type='e'
                  task_initiate_id=tinit[0].id
                  task_execute_id=0
                  tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition = self.pre_execute_task(task_initiate_id,task_execute_id)
            
                  if tii_ser is not None and tei_ser is not None:
                    response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                  elif tii_ser is not None:
                    response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                  elif tei_ser is not None:
                    response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                  else:
                    response = {'msg':'Error in PreExecuteTask function --> Start instance'}
                    response_status=500
              if len(texe)>0:
                self.TaskExecute = texe[0]
                form_type='e'
                task_initiate_id=self.TaskExecute.TaskInitiateId
                task_execute_id=self.TaskExecute.id
                tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition = self.pre_execute_task(task_initiate_id,task_execute_id)
          
                
              if tii_ser is not None and tei_ser is not None:
                response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              elif tii_ser is not None:
                response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              elif tei_ser is not None:
                response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              else:
                response = {'msg':'Error in PreExecuteTask function --> Execute instance'}
                response_status=500
          
            
          if response is not None and response_status==200:
              
              procType=1
              if 'procType' in response:
                  procType=response['procType']
                      
              if response_status==200 and self.TaskTransactionId is not None:
                  if procType!=4:
                      transParams = {'task_id':self.id,'previous_task_id':self.PreviousTaskId,'outcome_id':self.OutcomeId}
                      tt = TaskTransactionsSa(TransactionId=self.TaskTransactionId,Params=transParams,Status='created',CreatedBy=self.User.id,CreatedDateTime=datetime.now(timezone.utc))
                      #TaskTransactions.objects.create(TransactionId=self.TaskTransactionId,Params=transParams,Status='created',CreatedBy=self.User,CreatedDateTime=timezone.now())
                      self.Session.add(tt)

                      if not tt:
                          response_status = 400
                          response = {'msg':'Error initiating task --> Error creating transaction record '+str(response_status)}
              elif response_status==200 and transaction_id is None:
                  #response_status = 400
                  response = {'msg':'Error initiating task --> transaction_id is null = '+str(self.TaskTransactionId)+' | '+str(response_status)}
          else:
              #response_status = 400
              response = {'msg':'Error initiating task --> response from StartTask is null '+str(response_status)}
            
          return response,response_status

    def start_public_task (self):
      
          response_status=200    
          response = None
        
        
          if self.PreviousTaskId>0:
            if self.OutcomeId>0:
              #save outcome in previous task

              #tei = TaskExecutionInstances.objects.filter(id=self.PreviousTaskId).update(TaskOutcomeCodeItemId_id=self.OutcomeId)
              tei=self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstances.id==self.PreviousTaskId)

              try:
                if len(tei)>0:
                  tei[0].TaskOutcomeCodeItemId=self.OutcomeId
              except exc:
                response =  {'msg':'Error updating outcome for previous task --> previous task id = '+str(self.PreviousTaskId)+' '+str(exc)}
                response_status=500
                return response,response_status   
            #else:
              #response =  {'msg':'Outcome id is zero (0) --> Check task definition!'}
              #response_status=500
              #return response,response_status
          
          task_initiate_id=0
          task_execute_id=0
          
          #if not task_def.AutoInitiate start form with task transaction-id with data must be returned
          if not self.TaskParameters.AutoInitiate:
          
            #generate unique task transaction id
            self.TaskTransactionId=str(self.User.id)+'-'+str(self.id)+'-'+str(task_initiate_id)+'-'+str(task_execute_id)+'-'+self.Guid
            formDefined = False
            name=''
            formDefinition=[]
            form_type='i'
            self.get_form_definition(form_type)
            self.generate_form()
            self.get_definition()
            formDefinition = self.get_form_json()
            resourceDefinition = self.get_definition_json()

            response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskdef': self.get_task_parameters_json(),'widgets':formDefinition,'procType':2,'formType':form_type}
            
            return response,response_status
          
          if self.TaskParameters.AutoInitiate:
            #if task_def.InfiniteTask:
            #this is task that can not be completed
              formDefined = False
              name=''
              formDefinition=[]
            
              #find existing task
              code_status_inprogress = self.get_status_code_by_name('InProgress','taskstatuses')
          
              #texe = TaskExecutionInstances.objects.filter(TaskDefId = self.id,ActualOwner = self.User,Status=code_status_inprogress)
              texe = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstances.TaskDefId==self.id,TaskExecutionInstancesSa.ActualOwner==self.User.id,TaskExecutionInstancesSa.Status==code_status_inprogress)

              #if exe instance not found search for tasks to claim
              if len(texe)==0:
                #first search for initiate instance
                code_status_ready = self.get_status_code_by_name('ready','taskstatuses')
          
                ta_ids=[]
                #tas = TaskAssignments.objects.filter(UserId=self.User,AssignGroup='potentialOwners',TaskInitiationId__Status_id=code_status_ready).select_related('TaskInitiationId')
                tas = self.Session.query(TaskAssignmentsSa).join(TaskInitiationInstanceSa,and_(TaskAssignmentsSa.TaskInitiationId==TaskInitiationInstanceSa.id,TaskInitiationInstanceSa.Status==code_status_ready)).filter(TaskAssignmentsSa.UserId==self.User.id,TaskAssignmentsSa.AssignGroup=='potentialOwners').all()

                for ta in tas:
                  ta_ids.append(ta.TaskInitiationId)
                #tinit = TaskInitiationInstance.objects.select_related('TaskDefId','InitiatorId','Status').filter(TaskDefId_id=self.id,id__in=ta_ids,Status_id=code_status_ready).order_by('-id')
                rdefSubquery = self.Session.query(ResourceDefSa,ResourceDefLangSa).join(ResourceDefLangSa,and_(ResourceDefSa.id==ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang==self.Language.id)).subquery()

                tinit = self.Session.query(TaskInitiationInstanceSa,rdefSubquery).outerjoin(rdefSubquery,TaskInitiationInstanceSa.TaskDefId==rdefSubquery.id).\
                        filter(TaskInitiationInstanceSa.id.in_(ta_ids),TaskInitiationInstanceSa.TaskDefId==self.id,TaskExecutionInstancesSa.ActualOwner==self.User.id,TaskExecutionInstancesSa.Status==code_status_ready).order_by(TaskExecutionInstancesSa.id).all()
               
                #initiate instance does not exist, create new task
                if len(tinit)==0:
                  #initiate task
                  form_type='e'
                  formDefinition = []
                  #outcome_id = 0
                  
                  self.TaskTransactionId=str(self.TaskParameters.id)+'-'+str(self.TaskInitiate.id)+'-'+str(self.TaskExecute.id)+'-'+guid
                  #not initiate task or maybe yes   
                  #ToDo - fix all methods to take as little input params as possible
                  rval = self.initiate_task ()
                  
            
                  if 'data' in rval:
                    
                    response = rval['data']
                    response.update({'procType':3,'formType':form_type,'transactionId':self.TaskTransactionId})
                    response_status = rval['status']
                    return response,response_status    
                  elif 'msg' in rval:
              
                    return rval['msg'],rval['status']
             
                if len(tinit)>0:
                  form_type='e'
                  task_initiate_id=tinit[0].id
                  task_execute_id=0
                  tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition = self.pre_execute_task(task_initiate_id,task_execute_id)
            
                  self.TaskTransactionId=str(self.TaskParameters.id)+'-'+str(self.TaskInitiate.id)+'-'+str(self.TaskExecute.id)+'-'+selg.Guid
          
                  if tii_ser is not None and tei_ser is not None:
                    response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                  elif tii_ser is not None:
                    response = {'transactionId':self.TaskTransactionId,'resourceDefinition': resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                  elif tei_ser is not None:
                    response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                  else:
                    response = {'msg':'Error in PreExecuteTask function --> Start instance'}
                    response_status=500
              if len(texe)>0:
          
                form_type='e'
                task_initiate_id=texe[0].TaskInitiateId_id
                task_execute_id=texe[0].id
                tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition = self.pre_execute_task(task_initiate_id,task_execute_id)
          
                self.TaskTransactionId=str(self.TaskParameters.id)+'-'+str(self.TaskInitiate.id)+'-'+str(self.TaskExecute.id)+'-'+guid
              
              if tii_ser is not None and tei_ser is not None:
                response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              elif tii_ser is not None:
                response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              elif tei_ser is not None:
                response = {'transactionId':self.TaskTransactionId,'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': self.get_task_parameters_json(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              else:
                response = {'msg':'Error in PreExecuteTask function --> Execute instance'}
                response_status=500
            #else:
            #  response = {'msg':'Error in PreExecuteTask function --> Invalid parameters --> AutoInitiate is true and InfiniteTask is false'}
            #  response_status=500
            
          return response,response_status

    def get_task_parameters_def (self):

      
      tparams=self.Session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==self.id).all()
      for tp in tparams:
        self.TaskParameters = tp

      if self.TaskParameters is None:
        raise Exception('Invalid task parameters definition')
    
    def get_public_task_parameters_def (self):
               
      #self.TaskParameters = TaskParameters.objects.get(OrganisationId_id=self.OrganisationId,UniqueName=self.UniqueName)
      #self.Language = self.TaskParameters.DefaultLang
      
      tparams=self.Session.query(TaskParametersSa,LanguageSa).outerjoin(LanguageSa,TaskParametersSa.DefaultLang==LanguageSa.id).filter(TaskParametersSa.OrganisationId==self.OrganisationId,TaskParametersSa.UniqueName==self.UniqueName).all()
      for tp in tparams:
        self.TaskParameters = tp[0]
        self.Language = tp[1]

        if self.TaskParameters is None:
            raise Exception('Invalid task parameters definition')
          

    def get_task_parameters_json (self):

        tparams={
            'id':self.TaskParameters.id,
            'Parameters':self.TaskParameters.Parameters,
            'ResourceDefId' :self.TaskParameters.ResourceDefId,
            'TaskType' : self.TaskParameters.TaskType,
            'MaxActualOwners' : self.TaskParameters.MaxActualOwners,
            'NoLimit' : self.TaskParameters.NoLimit,
            'SelfAssigned':self.TaskParameters.SelfAssigned,
            'AutoAssign' :self.TaskParameters.AutoAssign,
            'AutoStart' : self.TaskParameters.AutoStart,
            'AutoClaim' : self.TaskParameters.AutoClaim,
            'AutoInitiate': self.TaskParameters.AutoInitiate,
            'AutoExecute':self.TaskParameters.AutoExecute,
            'InfiniteTask' : self.TaskParameters.InfiniteTask,
            'CanDelegate' : self.TaskParameters.CanDelegate,
            'CanHold' : self.TaskParameters.CanHold,
            'CanRelease' : self.TaskParameters.CanRelease,
            'TaskInitiatorsSaved' : self.TaskParameters.TaskInitiatorsSaved,
            'PotentialOwnersSaved' : self.TaskParameters.PotentialOwnersSaved,
            'TaskOutcomesSaved' : self.TaskParameters.TaskOutcomesSaved,
            'TaskOutcomeHeadId' : self.TaskParameters.TaskOutcomeHeadId,
            'DefaultOutcomeId' : self.TaskParameters.DefaultOutcomeId,
            'AssignTypeInitiator' : self.TaskParameters.AssignTypeInitiator,
            'AssignTypeInitiatorGroup' : self.TaskParameters.AssignTypeInitiatorGroup,
            'AssignTypeOwner' : self.TaskParameters.AssignTypeOwner,
            'AssignTypeOwnerGroup' :self.TaskParameters.AssignTypeOwnerGroup,
            'AssignTypeAdmin' : self.TaskParameters.AssignTypeAdmin,
            'UniqueId' :self.TaskParameters.UniqueId,
            'UniqueIdGroup' :self.TaskParameters.UniqueIdGroup,
            'PublicTask' : self.TaskParameters.PublicTask,
            'UniqueName' : self.TaskParameters.UniqueName,
            'OnMarketAsDef' : self.TaskParameters.OnMarketAsDef,
            'OnMarketAsService' : self.TaskParameters.OnMarketAsService


        }
       
        return tparams

    def process_form (self):

      ret_value = []
      response_msg = []
      resourceRecordId = 0
      
      
      for widget in self.TaskData.items():
        
        #Get presentation elements and resource model def (for now from presentation elements, later from published when implemented) ToDo
        
          pes = self.Session.query(PresentationElementsSa,PresentationElementsLangSa,ResourceModelDefinitionSa).outerjoin(PresentationElementsLangSa,and_(PresentationElementsSa.id==PresentationElementsLangSa.PresentationElementsId,PresentationElementsLangSa.Lang==self.Language.id)).outerjoin(ResourceModelDefinitionSa,PresentationElementsSa.ResourceModelDefinitionId==ResourceModelDefinitionSa.id).filter(PresentationElementsSa.id==widget[0],PresentationElementsSa.Status=='Active').all()

          for pElements in pes:
            pe=pElements[0]
            peLang=pElements[1]
            #ErrorLog.objects.create(Error1={'pe':pe},Error2={},CreatedDateTime=timezone.now())
            if pe.Status=='Active':
            
              #process widgets here - call execute widget fn
              widget_name = pe.DirectiveName
              if widget_name is not None:

                try:
                  module_path = 'katalyonode.functions.widgets.form.'+widget_name 
                  module_ref = importlib.import_module(module_path)
                  widget_class = getattr(module_ref, widget_name)
                  widget_object = widget_class((pe,peLang,None),self)
                  if widget_object is not None:
                    if self.FormType=='i':
                      task_instance_id = self.TaskInitiate.id
                    else:
                      task_instance_id = self.TaskExecute.id

                    rval = widget_object.process_widget(widget[1],pe,peLang,task_instance_id)
                    if 'ret_value' in rval:
                      ret_value.append(rval['ret_value'])
                    if 'ResponseMsg' in rval:
                      response_msg.append(rval['ResponseMsg'])
                except ModuleNotFoundError:
                  raise Exception("ModuleNotFoundError --> " + module_path)

      return   ret_value,response_msg

    def create_task_initiate_instance (self,exeLimit,code_status_id):

        if self.PreviousTaskId==0:
          p_task_id=None
        else:
          p_task_id = self.PreviousTaskId
      
        #tinit = self.Session.query(TaskInitiationInstancesSa).filter(TaskInitiationInstancesSa.TaskDefId==self.id,TaskInitiationInstancesSa.InitiatorId==self.User.id,TaskInitiationInstancesSa.Status==code_status_id,TaskInitiationInstancesSa.TaskType==self.TaskParameters.TaskType)
        user_id = None
        if self.User is not None:
          user_id = self.User.id

        tinit = TaskInitiationInstanceSa(TaskDefId=self.id,InitiatorId=user_id,Status=code_status_id,TaskType=self.TaskParameters.TaskType,TransactionId = str(self.TaskTransactionId),NoOfExeInstances=0,ExeInstanceLimit=exeLimit,PreviousTaskId=p_task_id,CreatedBy=user_id,CreatedDateTime=datetime.now(timezone.utc))
        
        self.Session.add(tinit)
        self.Session.flush()

        self.TaskInitiate = tinit 
        
        #TaskInitiationInstance.objects.create(TaskDefId_id=self.id,InitiatorId=self.User,Status_id=code_status_id,TaskType=self.TaskParameters.TaskType,TransactionId = str(self.TaskTransactionId),NoOfExeInstances=0,ExeInstanceLimit=exeLimit,PreviousTaskId_id=p_task_id,CreatedBy=self.User,CreatedDateTime=timezone.now())
              

    def create_task_execute_instance (self):

          texes = self.Session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.ResourceDefId==self.id).all()
          for texe in texes:
            self.TaskExecute = texe

    def get_status_code_by_name (self,code_detail_name,code_head_name):

        #cds = CodesDetail.objects.filter(CodesHeadId__CodeHeadName=code_head_name,Name=code_detail_name)
        cds = self.Session.query(CodesDetailSa,CodesHeadSa).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).filter(CodesHeadSa.CodeHeadName==code_head_name,CodesDetailSa.Name==code_detail_name).all()
        code_id = None
        for cd in cds:
            code_id = cd[0].id

        return code_id