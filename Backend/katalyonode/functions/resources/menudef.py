import pdb
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass,ResourceHelperClass
import katalyonode.functions.rdbms
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from sqlalchemy import Table,delete#, Column, Integer,Boolean, String, Text, ForeignKey,Numeric,Float,Date,Time,DateTime, MetaData
#from katalyonode.models.models import Pages,Menus
from katalyonode.functions.rdbms.alchemymodels import MenusSa,MenusUsersSa,MenusGroupsSa
#from django.utils import timezone
from sqlalchemy import and_,or_
from datetime import datetime, timezone

import json

g_rdbms_cls = RdbmsClass()

class menu (ResourceBaseClass):
    
        def __init__(self,_id,_resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):
            if _user is None:
                pdb.set_trace()
            super().__init__(_id,  _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
            self.rdbms_cls = g_rdbms_cls
            self.NavbarLeft = None
            self.NavbarRight = None

        def get_extended_resource_data(self):
            
                
                menus = []
                queryset=self.get_menus( self.User ,None,1,self.id,0)
                
                for item in queryset:
                    itemDict={}
                    for col in item.keys():
                        val = getattr(item, col) 
                        try:
                            val = json.loads(val)   
                        except:
                            pass
                        value = {col:val}
                        itemDict.update(value)

                    #pdb.set_trace()
                    tmp_users_groups = self.get_users_groups_menus( itemDict['katalyonode_menus_id'], self.Language)  
                    
                    itemDict['Users'] = []
                    itemDict['Groups'] = []
                    for item in tmp_users_groups[0]:
                        val_id =  getattr(item, 'auth_user_id')
                        val_fname = getattr(item, 'auth_user_first_name')
                        val_lname = getattr(item, 'auth_user_last_name')
                        val_username = getattr(item, 'auth_user_username')
                        val_name = val_fname+' '+val_lname+' - '+val_username
                        itemDict['Users'].append({'id':val_id,'name':val_name})
                    
                    for item in tmp_users_groups[1]:
                        val_id =  getattr(item, 'auth_group_id')
                        val_name = getattr(item, 'auth_group_name')
                        itemDict['Groups'].append({'id':val_id,'name':val_name})
                    
                    menus.append(itemDict)
               
                #process menus
                navbarLeft = []
                navbarRight = []
                for mi in menus:
                        
                        if mi['katalyonode_menus_MenuPosition'] in ['navbar','navbarright']:
                                children = self.Get_Children_Menus(mi,menus)
                                mi.update({'layout':children})
                                if mi['katalyonode_menus_MenuPosition']=='navbar':
                                        navbarLeft.append(mi)
                                else:
                                        navbarRight.append(mi)
                #pdb.set_trace()
                self.ResourceExtended= {'navbarItemsLeft':navbarLeft,'navbarItemsRight':navbarRight}
                return self.ResourceExtended
        
        def Get_Children_Menus(self,mi,allMenus):
                children_menus = []
                for menu in allMenus:
                        if menu['katalyonode_menus_MenuPosition']  not in ['navbar','navbarright'] and menu['katalyonode_menus_Navbar'] is not None and menu['katalyonode_menus_Navbar']==mi['katalyonode_menus_id']:
                                children_menus.append(menu)
                return children_menus
        
        
        def get_users_groups_menus(self,menuId,language):
                
                objects = None
                
                auth_users='auth_user'
                auth_groups='auth_group'
                table_users='katalyonode_menus_Users'
                table_groups='katalyonode_menus_Groups' 
              
                tbl_users = Table(table_users, self.rdbms_cls.metadata, autoload_with=self.rdbms_cls.engine)
                tbl_groups = Table(table_groups, self.rdbms_cls.metadata, autoload_with=self.rdbms_cls.engine)
                tbl_auth_users = Table(auth_users, self.rdbms_cls.metadata, autoload_with=self.rdbms_cls.engine)
                tbl_auth_groups = Table(auth_groups, self.rdbms_cls.metadata, autoload_with=self.rdbms_cls.engine)
       
                menu_users = self.Session.query(tbl_users,tbl_auth_users).join(tbl_auth_users,tbl_users.c[table_users+'_user_id']==tbl_auth_users.c[auth_users+'_id']).filter(tbl_users.c[table_users+'_menus_id']==menuId).all()
                menu_groups = self.Session.query(tbl_groups,tbl_auth_groups).join(tbl_auth_groups,tbl_groups.c[table_groups+'_group_id']==tbl_auth_groups.c[auth_groups+'_id']).filter(tbl_groups.c[table_groups+'_menus_id']==menuId).all()

                return  menu_users, menu_groups
                
          
        def get_menus(self,user,group_ids,type,resourceId,appId):
    
                objects = None
                table_resourcedef='katalyonode_resourcedef'
                table_menus='katalyonode_menus'
                table_users='katalyonode_menus_Users'
                table_groups='katalyonode_menus_Groups'
                table_pages='katalyonode_pages'
                table_rlinks='katalyonode_resourcelinks'
                
                tbl_resourcedef = self.rdbms_cls.get_table(table_resourcedef)
                tbl_menus = self.rdbms_cls.get_table(table_menus)
                tbl_users = self.rdbms_cls.get_table(table_users)
                tbl_groups = self.rdbms_cls.get_table(table_groups)
                tbl_pages = self.rdbms_cls.get_table(table_pages)
                tbl_rlinks = self.rdbms_cls.get_table(table_rlinks)

                    
                menus = self.Session.query(tbl_menus, tbl_pages,tbl_resourcedef)

                menus = menus.outerjoin(tbl_pages, tbl_pages.c[table_pages+'_id'] == tbl_menus.c[table_menus+'_TargetPage_id'])

                helper = ResourceHelperClass()
                org_id,language = helper.get_lang_org (user)

                menus = menus.join(tbl_resourcedef, tbl_resourcedef.c[table_resourcedef+'_id'] == tbl_menus.c[table_menus+'_ResourceDefId_id'])

                #pdb.set_trace()
                if appId>0:
                #get resource menu for application
                        menus = menus.join(tbl_rlinks,tbl_rlinks.c[table_rlinks+'_Resource2_id'] == tbl_menus.c[table_menus+'_ResourceDefId_id']).filter(tbl_rlinks.c[table_rlinks+'_Resource1_id'] == appId,  tbl_rlinks.c[table_rlinks+'_LinkType'] =='app-menu')

                if (type==0): #when in EXECUTE mode
                        menus = menus.filter()
                        menu_users = self.Session.query(tbl_users.c[table_users+'_menus_id']).filter(tbl_users.c[table_users+'_user_id']==user.id)#.subquery()
                        menu_groups = self.Session.query(tbl_groups.c[table_groups+'_menus_id']).filter(tbl_groups.c[table_groups+'_group_id'].in_(group_ids))#.subquery()
                        all_menus = menu_users.union(menu_groups).all()
                        all_menus_list = [x for t in all_menus for x in t]
                        menus = menus.filter(tbl_menus.c[table_menus+'_Active']==True,or_(tbl_menus.c[table_menus+'_id'].in_(all_menus_list),and_(tbl_resourcedef.c[table_resourcedef+'_Private']==False,tbl_resourcedef.c[table_resourcedef+'_Organisation_id']!=org_id)))

                elif (type==1): #when in definition mode
                        menus = menus.filter(tbl_menus.c[table_menus+'_ResourceDefId_id']==self.id)
                else: #not supposed to happen
                        menus = menus.filter(tbl_menus.c[table_menus+'_Active']==True)

                menus = menus.order_by(tbl_menus.c[table_menus+'_Order'],tbl_menus.c[table_menus+'_id'])

                results = menus.all()
           
                return  results
            
      
            
        def get_pages_for_user(self,user):
                
                return []

        def prepare_save(self,navbarLeft,navbarRight):
                
               self.NavbarLeft = navbarLeft
               self.NavbarRight = navbarRight


        def save_full_menu(self):

                counter = 0
                ret_status=200
                msg=None
                menu_position_navbar='navbar'
                
                for navbar in self.NavbarLeft:
                        counter=counter+1
                        msg,ret_status = self.process_menu_item(navbar,self.NavbarRight,menu_position_navbar,counter,None)


                if ret_status==200:
                #counter = 0

                        menu_position_navbar='navbarright'
                        for navbar in self.NavbarRight:
                                counter=counter+1
                   
                                msg,ret_status = self.process_menu_item(navbar,self.NavbarRight,menu_position_navbar,counter,None)
                                if ret_status!=200:
                                        break

                return msg,ret_status

        def process_menu_item(self,menuItem,allItems,menuPosition,order,navbarId):
  
                menu_id = menuItem['katalyonode_menus_id'] if 'katalyonode_menus_id' in  menuItem  else None
                users = menuItem['Users']  if 'Users' in menuItem else []
                groups = menuItem['Groups']  if 'Groups' in menuItem else [] 
                users_ids = []
                groups_ids = []
                target_page = None
                target = None
                active=True
                icon=None
                menu_type=1
                delete_item=False

                for u in users:
                        users_ids.append(u['id'])

                if (len(users_ids) == 0):
                        #pdb.set_trace()
                        users_ids.append(self.User.id)  #always grant current user  

                for g in groups:
                        groups_ids.append(g['id'])


                if 'katalyonode_menus_TargetPage_id' in menuItem:
                        target_page = menuItem['katalyonode_menus_TargetPage_id']
                elif 'TargetPage' in menuItem:
                        if 'id' in menuItem['TargetPage']:
                                target_page = menuItem['TargetPage']['id']

                if 'katalyonode_menus_Target_id' in menuItem:
                        target = menuItem['katalyonode_menus_Target_id']
                elif 'Target' in menuItem:
                        if 'id' in menuItem['Target']:
                                target = menuItem['Target']['id']

                if 'katalyonode_menus_Active' in menuItem:
                        active = menuItem['katalyonode_menus_Active']

                if 'katalyonode_menus_Icon' in menuItem:
                        icon = menuItem['katalyonode_menus_Icon']

                if 'katalyonode_menus_MenuType' in menuItem:
                        menu_type = menuItem['katalyonode_menus_MenuType']

                if 'deleteItem' in menuItem:
                        delete_item = menuItem['deleteItem']

                #pdb.set_trace()
                if menu_type==1:
                        table_pages='katalyonode_pages'
                        tbl_pages = self.rdbms_cls.get_table(table_pages)
                        
                        #pages = session.query(tbl_pages).filter(tbl_pages.c[table_pages+'_ResourceDefId_id']==target).all() #MIGOR becaouse not working for PageMenu
                        pages = self.Session.query(tbl_pages).filter(tbl_pages.c[table_pages+'_ResourceDefId_id']==target).all()
                        if len(pages)>0:
                                target_page = pages[0].katalyonode_pages_id
                        else:
                                return 'Error saving menu '+str(menu_id)+' -> page record not found in '+menuPosition+' ('+str(target_page)+')',400

                params = {'pageid':target}    
                ret_status = 200
                msg=''
                #pdb.set_trace()
                if menu_id is None:
                        menu = MenusSa(
                            Name = menuItem['katalyonode_menus_Name'],
                            MenuType= menu_type,
                            MenuPosition= menuPosition,
                            TargetPageId=target_page,
                            Target=target,
                            Params= json.dumps(params),
                            Icon= icon,
                            Order= order,
                            ParentItem = navbarId,
                            Navbar = navbarId,
                            ResourceDefId=self.id,
                            Active= active,
                            CreatedBy= self.User.id,
                            CreatedDateTime = datetime.now(timezone.utc) 
                            )

                        self.Session.add(menu)
                        self.Session.flush()

                        if navbarId is None and menu.MenuPosition in ['navbar','navbarright']:
                                menu.Navbar = menu.id
                                menu.ParentItem = menu.id
                                navbarId = menu.id
                        #menu.Users.set(users_ids)
                        #menu.Groups.set(groups_ids)
                        for ui in users_ids:
                            musers = MenusUsersSa(MenusId=menu.id,UserId=ui)
                            self.Session.add(musers)
                        #menu.Groups.set(groups_ids)
                        for gi in groups_ids:
                            mgroups = MenusGroupsSa(MenusId=menu.id,GroupId=gi)
                            self.Session.add(mgroups)
                        
                elif menu_id is not None:

                        #menus = Menus.objects.filter(id=menu_id)
                        menus = self.Session.query(MenusSa).filter(MenusSa.id ==menu_id).all()
                        if len(menus)==0:
                                msg='Menu with id = '+menu_id+' is not found'
                                ret_status=400  
                        else:
                        #pdb.set_trace()
                                menu=None
                                if delete_item:
                                        for menu_del in menus:
                                                musers_del = self.Session.execute(delete(MenusUsersSa).where(MenusUsersSa.MenusId==menu_del.id))
                                                mgroups_del = self.Session.execute(delete(MenusGroupsSa).where(MenusGroupsSa.MenusId==menu_del.id))
                                                self.Session.delete(menu_del)
                                else:
                                        for menu in menus:
                                                if navbarId is None:
                                                        navbarId = menu.Navbar
                                                menu.Name = menuItem['katalyonode_menus_Name']
                                                menu.MenuType = menu_type
                                                menu.TargetPageId =target_page
                                                menu.Target =target
                                                menu.MenuPosition = menuPosition
                                                menu.Params = json.dumps(params)
                                                menu.ParentItem = navbarId
                                                menu.Navbar = navbarId
                                                menu.Icon = icon
                                                menu.Order = order
                                                menu.Active = active
                                                menu.ChangedBy = self.User.id
                                                menu.ChangedDateTime = datetime.now(timezone.utc)
                                                #musers_del = self.Session.delete(self.Session.query(MenusUsersSa).filter(MenusUsersSa.MenusId==menu.id).all())
                                                musers_del = self.Session.execute(delete(MenusUsersSa).where(MenusUsersSa.MenusId==menu.id))
                                                #mgroups_del = self.Session.delete(self.Session.query(MenusGroupsSa).filter(MenusGroupsSa.MenusId==menu.id).all())
                                                mgroups_del = self.Session.execute(delete(MenusGroupsSa).where(MenusGroupsSa.MenusId==menu.id))
                                                
                                                for ui in users_ids:
                                                        musers = MenusUsersSa(MenusId=menu.id,UserId=ui)
                                                        self.Session.add(musers)
                                                for gi in groups_ids:
                                                        mgroups = MenusGroupsSa(MenusId=menu.id,GroupId=gi)
                                                        self.Session.add(mgroups)
                                                
                counter = 0

                if 'layout' in menuItem and ret_status==200:
                        for mi in menuItem['layout']:
                                counter=counter+1
                                menu_position_side='sidemenu'
                                msg,ret_status=self.process_menu_item(mi, menuItem['layout'],menu_position_side,counter,navbarId)
                                if ret_status!=200:
                                        break
                        return msg,ret_status
                else:
                        return msg,ret_status
                        