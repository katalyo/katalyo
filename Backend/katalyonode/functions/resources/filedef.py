######################## dataset class ##########################
###############################################################################

import pdb
from django.utils import timezone
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.resources.datasetdef import dataset
from sqlalchemy import Table, Column, Integer,Boolean, String, Text, ForeignKey,Numeric,Float,Date,Time,DateTime, MetaData
import pdb


class file (dataset):
    
    def __init__(self,_id,  _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):

        super().__init__(_id, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type) 
        
        
    def get_file_fields(self,columns):
        columns.append({'name': 'resource'+str(self.id)+'_'+str(self.OrganisationId)+'_'+'Name','displayName': 'File name','width': '10%'})
        columns.append({'name': 'resource'+str(self.id)+'_'+str(self.OrganisationId)+'_'+'FileType','displayName': 'File type','width': '10%'})
        columns.append({'name': 'resource'+str(self.id)+'_'+str(self.OrganisationId)+'_'+'Size','displayName': 'Size','width': '10%'})
        #columns.append({'name': 'resource'+str(self.id)+'_'+str(self.OrganisationId)+'_'+'Path','displayName': 'Path','width': '10%'})
        columns.append({'name': 'resource'+str(self.id)+'_'+str(self.OrganisationId)+'_'+'Sha512','displayName': 'File hash','width': '10%'})
        return columns

    def get_file_rec(self,rec_id):
        
        rec_list = [rec_id]
        recs =  self.rdbms_cls.get_data_filtered_in(self.id,self.OrganisationId,rec_list)
        return recs