######################## notification class ##########################
###############################################################################

import pdb
from django.utils import timezone
from katalyonode.functions.resources.datasetdef import dataset

class notification (dataset):
    
    def __init__(self,_id, _resource_admins, _resource_admin_groups, _resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version=None,_resource_sub_type=None):

        super().__init__(_id, _resource_admins,_resource_admin_groups,_resource_type,_name,_subject,_description,_status,_user,_language,_org_id,_parameters,_version,_resource_sub_type)
      