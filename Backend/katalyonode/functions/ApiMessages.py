"""

Api messages with language

"""

from katalyonode.functions.rdbms.alchemymodels import ApiMessagesSa,LanguageSa
from katalyonode.functions.rdbms.rdbms import RdbmsClass
import pdb

def GetMessageFromDatabaseByLangId(messageId,lang,params=[]):

	if type(lang) is not int:
		assert('Invalid language in api message module - lang id')

	rdbms_cls = RdbmsClass()
	session = rdbms_cls.Session()

	final_message = 'Error - message not found'

	with session.begin():
		message = session.query(ApiMessagesSa).filter(ApiMessagesSa.MessageRef==messageId,Lang=lang).first()
		final_message = message.Message.format(*params)

	return final_message

def GetMessageFromDatabaseByLangCode(messageId,lang,params=[]):

	if type(lang) is not str:
		assert('Invalid language in api message module - lang code')

	return ''

def GetMessage(messageId,lang,params=[]):
	
	final_message = None

	message =  g_api_messages.get(messageId+str(lang),None)
	if message is not None:
		message = message.get('message',None)
		final_message = message.format(*params)

	return final_message

def GetAllMessages():


	rdbms_cls = RdbmsClass()
	session = rdbms_cls.Session()
	messageDict ={}

	with session.begin():


		messages = session.query(ApiMessagesSa,LanguageSa).join(LanguageSa,ApiMessagesSa.Lang==LanguageSa.id).all()
		
		for msg in messages:
			messageDict[msg[0].MessageRef+str(msg[1].Code)] = {'id':msg[0].id,'msg_ref':msg[0].MessageRef,'message':msg[0].Message,'message_type':msg[0].MessageType,'lang_id':msg[1].id,'lang_code':msg[1].Code}

	return messageDict

g_api_messages = GetAllMessages()