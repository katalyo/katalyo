from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from django.contrib.auth.models import User, Group
from rest_framework import viewsets,permissions,authentication,pagination
import json
from katalyonode.settings import USE_BLOCKCHAIN
#from katalyonode.serializers.serializers import CookiesSerializer
from katalyonode.serializers.serializerssettings import GetCodesDetailByIdLangSerializer,GetCodeObjectByName,LanguageSerializer
#from katalyonode.serializers.serializers import OrganisationsSerializer
from django.forms.models import model_to_dict
from katalyonode.functions.blockchain.eth_middleware import get_ktlyo_balance
#this is for sending mails
from django.template import Context
from django.template.loader import render_to_string, get_template
from django.core.mail import EmailMessage
from django.contrib.auth import login
from rest_framework.authtoken.models import Token
#import katalyonode.models
from wsgiref.util import FileWrapper
from django.utils import timezone
import hashlib
from django.conf import settings
from rest_framework.permissions import AllowAny
from django.core.validators import validate_email
import requests
import pdb
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.rdbms.alchemymodels import SignupsSa,CookiesSa,OrganisationsSa,LanguageSa,UserExtendedPropertiesSa,GroupExtendedPropertiesSa,ResourceSubscriptionsSa,group_owners,GroupLangSa,auth_group,auth_user_groups
import base64
import os.path
import sys
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.utils import COMMASPACE, formatdate
from email import encoders
import os
from datetime import datetime,timezone

# Create your views here.


class Signup(viewsets.GenericViewSet):
   #queryset = Signups.objects.all()
   permission_classes = (AllowAny, )
   authentication_classes = ()

   def create(self, request):
  
        email = request.data['email']
        g_recaptcha_response = request.data['captchaResponse']
       
        r = requests.post(settings.GR_CAPTCHA_URL, {
        'secret': settings.GR_CAPTCHA_SECRET_KEY,
        'response': g_recaptcha_response})
        
        if not json.loads(r.content.decode())['success']:
          return Response('Invalid captcha',status=400)

        # check if e-mail is valid
        
        valid_email=None # ovdje staviti check e-maila
        
        try:
          validate_email(email)
          valid_email = True
        except Exception as e:
          valid_email = False
        
        if not valid_email:
          return Response(data='Please enter valid e-mail',status=500)
     
        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()

        with session.begin():
            su = session.query(SignupsSa).filter(SignupsSa.EMail==email).all()
  
            if len(su)>0:
              return Response(data='E-mail '+email+' is already registered with Katalyo',status=500)
            elif len(su)==0:
          
                hash_object_sha = hashlib.sha512(email.encode())
                
                signup = SignupsSa(EMail=email,SignupKey =hash_object_sha.hexdigest(), MailConfirmed = False, CreatedDateTime =datetime.now(timezone.utc))
                session.add(signup)
        
        
        if signup.id is not None:    
            #send e-mail
            subject = 'Katalyo signup confirmation'
            receivers = [email]
            confirmation_link = settings.APP_SERVER+'/register/'+signup.SignupKey
            ctx = {'email': email,'link': confirmation_link}
    
    
            sender = 'noreply@katalyo.com'
            sender_display = 'Katalyo<noreply@katalyo.com>'
            mail_message =  get_template('signup_email.html').render(ctx)

            try:
        
                send_mail_with_remote_host(sender_display,receivers, subject, mail_message)
            except Exception as err:
                response_data=str(err)+' It seems you have a problem with signup. Please try again later! If the error persists contact us at info@katalyo.com'
                return Response(data=response_data,status=500)
 
        
            return Response(data='Thank you for signing up with Katalyo',status=200)
        
        else:
            return Response(data='Error signing up, please contact us at info@katalyo.com',status=500)

def send_mail_with_local_host(sender,receivers, subject, text, files=[],server="localhost"):
    assert type(receivers)==list
    assert type(files)==list

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text,'html') )

    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(file))
        msg.attach(part)

    smtp = smtplib.SMTP(server)
    smtp.sendmail(sender,receivers, msg.as_string() )
    smtp.close()

def send_mail_with_remote_host(sender,receivers, subject, text, files=[],server=settings.MAIL_SERVER,port=settings.MAIL_PORT):
    assert type(receivers)==list
    assert type(files)==list

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = COMMASPACE.join(receivers)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach( MIMEText(text,'html') )

    for file in files:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(file,"rb").read() )
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(file))
        msg.attach(part)

    smtp = smtplib.SMTP(server,port)
    smtp.sendmail(sender,receivers, msg.as_string() )
    smtp.close()



def home(request):
    return render(request, 'index.html')

def create_message(to,sender, subject, message_text):
  """Create a message for an email.

  Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    subject: The subject of the email message.
    message_text: The text of the email message.

  Returns:
    An object containing a base64url encoded email object.
  """
  message = MIMEText(message_text)
  message['to'] = to
  message['from'] = sender
  message['subject'] = subject
  raw =  base64.urlsafe_b64encode(message.as_bytes())
  return {'raw': raw.decode() }


def create_gmail_service(EMAIL_FROM):

    sys.path.append('usr/katalyo/katalyo')
    SCOPES = ['https://www.googleapis.com/auth/gmail.send']
    SERVICE_ACCOUNT_FILE = '/usr/katalyo/katalyo/nth-station-273221-5c3a3f86641a.json'
    credentials = service_account.Credentials.from_service_account_file(SERVICE_ACCOUNT_FILE, scopes=SCOPES)
    delegated_credentials = credentials.with_subject(EMAIL_FROM)
    service = build('gmail', 'v1', credentials=delegated_credentials)
    return service



def send_message(service, user_id, in_message):
    """Send an email message.

    Args:
    service: Authorized Gmail API service instance.
    user_id: User's email address. The special value "me"
    can be used to indicate the authenticated user.
    message: Message to be sent.

     Returns:
        Sent Message.
    """

    try:
        # sendAsResource = {"sendAsEmail": "no-reply@katalyo.com",
        # "isDefault": True,
        #"replyToAddress": "existingalias@test.com",
        # "displayName": "Katalyo",
        # "isPrimary": False,
        # "treatAsAlias": False
        # }
        #send_as= (service.users().settings().sendAs().create(userId = user_id, body=sendAsResource).execute())
        message = (service.users().messages().send(userId=user_id, body=in_message).execute())
        #print 'Message Id: %s' % message['id']
        return message

    except errors.HttpError as e:
        raise Exception(e)
        #print 'An error occurred: %s' % error

def send_msg(request):

    if request.method == 'POST':
        name = request.POST.get('name')
        email = request.POST.get('email')
        msg = request.POST.get('msg')
        response_data = {}

        try:
            validate_email(email)
            valid_email = True
        except ValidationError:
            valid_email = False


        if not valid_email:
            response_data['result']='Invalid e-mail'
            return HttpResponse(json.dumps(response_data),content_type="application/json",status=500)


        subject = 'Received website message from '+name
        #receivers = 'ivica.ljubicic@gmail.com,igor.mocilac@gmail.com,daniel.stjepanovic1@gmail.com'
        receivers = ['ivica.ljubicic@gmail.com','igor.mocilac@gmail.com','daniel.stjepanovic1@gmail.com']
        sender = 'noreply@katalyo.com'
        sender_display = 'Katalyo<noreply@katalyo.com>'
        mail_message = 'Email = '+email+'\nMessage = '+msg

        try:
            #service = create_gmail_service(sender)
            #message = create_message(receivers,sender_display,subject,mail_message)
            #sent_msg = send_message(service,sender,message)
            send_mail_with_local_host(sender_display,receivers, subject, mail_message)
        except Exception as err:
            response_data['result'] = err+' We had a problem receiving your message. Please try again later! If the error persists contact us on any of our other channels.'
            return HttpResponse(json.dumps(response_data),content_type="application/json",status=500)
        #msg = EmailMessage(subject, message, to=receivers, from_email=senders)
        #msg.content_subtype = 'text'
        #msg.send()

        response_data['result'] = 'Your message was received successfuly. We will get back to you soon!'

        return HttpResponse(json.dumps(response_data),content_type="application/json")
    else:
        return HttpResponse(json.dumps({"nothing to see": "this isn't happening"}),content_type="application/json")

class CheckSignup(viewsets.GenericViewSet):
   #queryset = Signups.objects.all()
   permission_classes = (AllowAny, )
   authentication_classes = ()

   def create(self, request):
  
        
        if 'signupkey' in request.data:
          signupkey = request.data['signupkey']
        else:
          return Response(data='Invalid request --> signupkey not sent',status=500)
        
        if signupkey =='' or signupkey is None:
          return Response(data='Invalid request --> signupkey is empty',status=500)
        
        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()

        with session.begin():
            su = session.query(SignupsSa).filter(SignupsSa.SignupKey==signupkey).all()
  
            if len(su)==0:
              return Response(data='Signup key not found!',status=500)
            elif len(su)>0:
              
              if su[0].MailConfirmed:
                return Response(data='Mail already confirmed',status=500)
              else:
                return Response(data='ok',status=200)
          
class Register(viewsets.GenericViewSet):
    #queryset = Signups.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()

    def create(self, request):
  
        register = request.data
      
        signup_key=None
        if 'signup_key' in  register:
            signup_key = register['signup_key']
      
        if signup_key is None:
            return Response(data='Invalid request --> signup key is missing',status=400)
      
      
        org_type=None
        if 'organisation_type' in  register:
            org_type = register['organisation_type']
      
        if org_type is None:
            return Response(data='Invalid request --> organisation type is missing',status=400)
      
        org_name=None  
        if 'organisation_name' in  register:
            org_name = register['organisation_name']
        
        if not org_name:
            if org_type!="Personal":
                return Response(data='Invalid request --> org name is missing',status=400)
      
      
        rdbms_cls = RdbmsClass()
        session = rdbms_cls.Session()

        with session.begin():
            su = session.query(SignupsSa).filter(SignupsSa.SignupKey==signup_key).all()
        
            #su = Signups.objects.filter(SignupKey=signup_key)
      
            if len(su)==0:
                return Response(data='Invalid request --> signup key not found',status=400)
            else:
                e_mail = su[0].EMail
      
            #create user
            usrname=None
            if 'username' in  register:
                usrname = register['username']
      
            if usrname is None:
                return Response(data='Invalid request --> username not sent',status=400)
      
            password=None
            if 'password' in  register:
                password = register['password']
      
            if password is None:
                return Response(data='Invalid request --> password not sent',status=400)
      
            fname=None
            if 'fname' in  register:
                fname = register['fname']
        
            if fname is None:
                return Response(data='Invalid request --> first name not sent',status=400)
      
            lname=None
            if 'lname' in  register:
                lname = register['lname']
      
            if lname is None:
                return Response(data='Invalid request --> last name not sent',status=400)
      
            if org_type=="Personal":
                org_name=fname+" "+lname
        
            user, user_created = User.objects.get_or_create(username=usrname)
      
            if not user_created:
                if user is not None:
                    return Response(data='Error --> Username '+usrname+' exists. Please try a different username',status=400)
                else:
                    return Response(data='Error --> User record not found',status=400)
      
            if user is not None and user_created:
                user.set_password(password) # This line will hash the password
                user.is_superuser=False;
                user.email=e_mail
                user.first_name = fname
                user.last_name = lname
        
         
                #create organisation
                #ToDo SQLAlchemy migration
                organisation = OrganisationsSa(Name=org_name,Description=org_name,OrgType=org_type,CreatedBy=user.id,CreatedDateTime=datetime.now(timezone.utc),Active=True,Version=1)
                session.add(organisation) #Organisations.objects.get_or_create()
                session.flush()
                #create group everyone
                group_everyone_name="everyone"
                #ToDo SQLAlchemy migration
                group_everyone = auth_group(name=str(organisation.id)+"_"+group_everyone_name)
                session.add(group_everyone)
                session.flush()
                #get default language
                langs = session.query(LanguageSa).filter(LanguageSa.Code=='en-GB').all()
                lang = None
                if len(langs)>0:
                    lang = langs[0]
                else:
                    lang={'id':None}
                
                #create extendedUser
                #ToDo SQLAlchemy migration
                uep = UserExtendedPropertiesSa(UserId=user.id, Organisation=organisation.id,
                HideCustomizeHome = False,
                HideCreateResourceHome = False,
                HideSearchResourceHome = False,
                HideAppsHome = False,
                APIUser = False,
                Lang = lang.id,
                CreatedBy =user.id,
                CreatedDateTime = datetime.now(timezone.utc)) 
                session.add(uep)
                # = UserExtendedProperties.objects.get_or_create()
                session.flush()
                
                group_everyone_lang = GroupLangSa(GroupId=group_everyone.id,
                Name=group_everyone_name,
                Description=group_everyone_name,
                Lang=lang.id,
                CreatedBy =user.id,
                CreatedDateTime = datetime.now(timezone.utc))
                session.add(group_everyone_lang)

                #create here auth_user_groups

                aug = auth_user_groups(user=user.id,group=group_everyone.id)
                session.add(aug)
                #user.groups.add(group_everyone)

                #ToDo SQLAlchemy migration
                gep = GroupExtendedPropertiesSa(GroupId=group_everyone.id,Organisation = organisation.id,CreatedDateTime=datetime.now(timezone.utc),CreatedBy=user.id)
                session.add(gep)
                session.flush()
                #gep,gep_created = GroupExtendedProperties.objects.get_or_create(GroupId=group_everyone,Organisation = org,CreatedDateTime=timezone.now(),CreatedBy=user)
                if gep:
                    grp_owners = group_owners(groupextendedproperties=gep.id,user=user.id)

                else:
                    return Response(data='Invalid request --> group extended create error',status=400)
                
                user.save() #save user record
                organisation.GroupEveryone = group_everyone.id #update group everyone
            else:
                return Response(data='Invalid request --> user allready exist',status=400)
      
            #update signup record
            su[0].MailConfirmed=True
            su[0].ChangedDateTime = datetime.now(timezone.utc)
            #su[0].save()
       
            if user is not None:
                if user.is_active:
                    login(request, user)
            
                    #get token
                    #ToDo SQLAlchemy migration - not possible as Token.create function generates the token
                    token,created = Token.objects.get_or_create(user=user)
            
                    if token: 
                    #ovdje serializirati user objekt i poslati ga natrag
                        response = {'userid':user.id,'first_name': user.first_name,'last_name': user.last_name,'email':user.email,'is_superuser':user.is_superuser,
                                  'last_login':user.last_login,'username':user.username,'is_active':user.is_active,'token': token.key,'groups':[]}
                    else:
                        return Response(data='Error with auth token',status=401)
            
                    if uep:
             
                        #lang = LanguageSerializer(uep.Lang)
                
                        #org = OrganisationsSerializer(uep.Organisation)
              
                        uep_object = uep._asdict()
                        
                        if uep_object['Params'] is not None:
                            uep_object['Params']= json.loads(uep_object['Params'])
                        else:
                            uep_object['Params'] = {}
                  
                        uep_object.update({'TaskListHome':[]})
                        ktlyo_bal = 0
                        ktlyo_block = 0
                        eth_address = organisation.ETHAccount
                    
                        if USE_BLOCKCHAIN:
                            uep_object.update({'use_blockchain':True})
                            
                            if eth_address is not None:
                                ktlyo_block,ktlyo_bal = get_ktlyo_balance(eth_address)
                                uep_object.update({'ktlyo_balance':ktlyo_bal,'ktlyo_block':ktlyo_block})
                        else:
                            uep_object.update({'use_blockchain':False})
                    
                    
                        response.update({'uep':uep_object,'lang':lang._asdict(),'organisation':organisation._asdict()})
                  
                        #subscribe to Katalyo builder
                        #get Katalyo builder id - ToDo move this to some other class
                        helper = ResourceHelperClass()
                        ktlyo_builder = helper.get_status_code_by_name('ktlyo_builder_id','systemcodes',lang.id)
                        subs_type = 'application'
                        #subscribe
                        #ToDo SQLAlchemy migration
                        sub = ResourceSubscriptionsSa(ResourceId=ktlyo_builder,Active=True,UserId=user.id,CreatedBy=user.id,CreatedDateTime=datetime.now(timezone.utc),SubscriptionType=subs_type) 
                        rs = session.add(sub) #ResourceSubscriptions.objects.create()
              
               
            else:
                return Response(data='Error --> user not created',status=400)
      
        return Response(data={'msg':'Onboarding completed successfuly!','data':response},status=200)

class CookiesView(viewsets.GenericViewSet):
    #queryset = Cookies.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()
    #serializer_class=CookiesSerializer
    
    def list(self, request,eid):
        
        if 'eid' in self.kwargs:
            e_id = self.kwargs['eid']
        else:
            return Response(data='Error! Cookie id is missing',status=500)

        rdbms_cls = RdbmsClass()
        session = rdbms_cls.Session()

        with session.begin():
            cook = session.query(CookiesSa).filter(CookiesSa.eid==e_id).all()
            #cook = Cookies.objects.filter(eid=e_id)
      
            if len(cook) == 0:
                return Response(data='Error! Invalid cookie id',status=500)
            else:
                cookie = {}
                cookie['id'] = cook[0].eid
                cookie['time'] = cook[0].time
                cookie['ver'] = cook[0].ver
                cookie['apps'] = cook[0].apps
         
        return Response(data=cookie,status=200)

    def create(self, request,eid):
      
        eid = request.data['id']
        ver = request.data['ver']
        datetime = request.data['time']
        apps = request.data['apps']
        
        apps_json = apps
        
        rdbms_cls = RdbmsClass()
        session = rdbms_cls.Session()

        with session.begin():
            cook = session.query(CookiesSa).filter(CoociesSa.id==e_id).all()
            status = 500
            message = 'Error creating cookie'
            
            if len(cook)==0:
                cookie = CookieSa(eid=eid,ver=ver,time=datetime,apps=apps_json, CreatedDateTime = datetime.now(timezone.utc))
                session.add(cookie)
                status = 201
                message = 'Cookie created successfully'
                    
            elif len(cook)>0:
          
                status=500
                message = 'Error updating cookie id = '+eid
            
                for ck in cook:
           
                    ck.time = datetime
                    ck.ChangedDateTime = datetime.now(timezone.utc)
                    ck.apps = apps_json
                status = 200
                message = 'Cookie updated successfully'
            
        return Response(data={'msg':message},status=status)
