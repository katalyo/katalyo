from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser,FileUploadParser,FormParser, MultiPartParser
import json
from django.forms.models import model_to_dict
from pathlib import Path
import os
import hashlib
import uuid
import chardet as chardet
from katalyonode.settings import FILES_DIR
from django.template import RequestContext
from rest_framework import viewsets,permissions,authentication,pagination
from rest_framework.pagination import PageNumberPagination
from django.utils.dateparse import parse_datetime
from datetime import datetime,timedelta
#import katalyonode.models
from wsgiref.util import FileWrapper
from django.core.validators import ValidationError
from django.utils import timezone
#ResourceDef,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang,PresentationElementsTasks
#import katalyonode.dynamicmodels
from katalyonode.serializers.serializers import FilesSerializer
from katalyonode.serializers.serializerssettings import GetCodesDetailByIdLangSerializer
#from katalyonode.functions.datasets import mutant_get_data_filtered_in,mutant_insert_data,mutant_update_data_no_history
#from katalyonode.functions.formprocessing import ProcessUpdateRecord
#from katalyonode.functions.blockchainprocessing import send_transaction,get_table_rows
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from struct import *
import requests
import pdb
import xmltodict
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.rdbms.alchemymodels import ResourceDefSa
from katalyonode.functions.resources.filedef import file
from katalyonode.functions.widgets.form.ktUploadFile import ktUploadFile
from katalyonode.functions.widgets.form.ktTaskFiles import ktTaskFiles
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.ApiMessages import GetMessage

rdbms_cls = RdbmsClass()  
    
class FilesViewSet(viewsets.ModelViewSet):

    #serializer_class = FilesSerializer
    
    #def get_queryset(self):
        #resource_def_id = self.kwargs['resourceDefId']
        #resource_id = self.kwargs['resourceId']
        #return FilesToResource.objects.filter(ResourceDefId=resource_def_id,ResourceId=resource_id).select_related('FileId')   

    def list(self, request,widgetId,fileDefId,datasetDefId,datasetRecordId):
        
        if 'widgetId' in self.kwargs:
           widget_id=int(self.kwargs['widgetId'])
        else:
           widget_id = None

        if 'fileDefId' in self.kwargs:
           file_def_id=int(self.kwargs['fileDefId'])
        else:
           file_def_id = None
        
        if 'datasetDefId' in self.kwargs:
           dataset_def_id=int(self.kwargs['datasetDefId'])
        else:
           dataset_def_id = None

        if 'datasetRecordId' in self.kwargs:
           dataset_record_id=int(self.kwargs['datasetRecordId'])
        else:
           dataset_record_id = None
        
        user = request.user   
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user) 
        if file_def_id is None: 
          return Response(data='Invalid file definition id',status=400)
        
        #rfiles = mutant_get_data_filtered_in(resource_id,org_id,[file_id])
        rval=None
        session = rdbms_cls.Session()
        with session.begin():
            file_object = file(file_def_id,'file','','','','',user,language,org_id,{})
            file_object.set_session(session)
            #files_list = ktTaskFiles(widget_id,)
            task_files_fd = {'PresentationId':widget_id,'id':widget_id}
            files_list = ktTaskFiles(task_files_fd,file_object,None,None)
            
            rval = files_list.process_widget_execute()
        response = Response(data=rval,status=200)
        return response
##### MIGOR

class FileMetaDataUpdateViewSet(viewsets.GenericViewSet):
   #queryset = ResourceDefLang.objects.all()#.select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   
   def create(self, request):
        

        user = request.user
        file_meta_data = request.data.get('fileResourceData',{})
        file_id = request.data.get('fileId',0)
        publish_id = request.data.get('publishId',0)
        upload_file_fd = request.data.get('fd',None)
        task_instance_id = upload_file_fd.get('taskInstanceId',0)
        file_meta_def_id = request.data.get('fileMetaDefId',None)

        
        prev_task_id=request.data.get('prevTaskId',0)
        parent_dataset_id=request.data.get('parentDatasetDefId',0)
        parent_dataset_record_id=request.data.get('parentDatasetId',0)
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user) 

        id_field = 'resource'+str(file_meta_def_id)+'_'+str(org_id)+'_id'

        if type(file_meta_data)==list and len(file_meta_data)>0:
            if id_field in file_meta_data[0]:
                file_meta_data[0][id_field] = file_id
            else:
                file_meta_data[0].update({id_field:file_id})
        elif type(file_meta_data)==dict:
            if id_field in file_meta_data:
                file_meta_data[id_field] = file_id
            else:
                file_meta_data.update({id_field:file_id})
        else:
            Response(data={'data':file_meta_data,'msg':GetMessage('FileMetaDataUpdate-1',language.Code,[str(file_id)]) or 'Error saving file meta data! Invalid form object for file_id = ' +str(file_id) +' !'},status=500)

        rval={}
        session = rdbms_cls.Session()
        with session.begin():
            file_object = file(file_meta_def_id,'file','','','','',user,language,org_id,{},None)
            file_object.set_session(session)
            upload_file = ktUploadFile(upload_file_fd,file_object,None,None)
            rval = upload_file.update_meta_data(file_meta_data,task_instance_id,prev_task_id,parent_dataset_id,parent_dataset_record_id,publish_id)


        return Response(data={'data':{'ret_value':rval.get('ret_value',None),'record_id':rval.get('record',None)},'msg':GetMessage('FileMetaDataUpdate-2',language.Code,[str(file_id)]) or 'Meta data saved. Upload completed successfully for file_id = ' +str(file_id) +' !'},status=200)


class UploadFileViewSet(viewsets.GenericViewSet):
    parser_classes = (FormParser, MultiPartParser,)

    def create(self, request,widgetId):
        
        #pdb.set_trace() #IMPORT #FileUploadView #DBG
        

        if 'widgetId' in self.kwargs:
           widget_id=int(self.kwargs['widgetId'])
        else:
           widget_id = None

        up_files = request.FILES['file']
        resource_id = int(request.data.get('resource_id',0))

        if resource_id==0:
            Response(data='Error uplading file record for resource_id '+str(resource_id),status=500)
        #form_data = request.data['form_data']
        #resource_id = form_data['resource_id']
        directory = os.path.join(FILES_DIR, str(resource_id))
        if not os.path.exists(directory):
            os.makedirs(directory,exist_ok=True)
            
        
        user = request.user

        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user) 

        prefix = 'resource'+str(resource_id)+'_'+str(org_id)+'_'
        digest = ''
        file_fields= {prefix+'FileName':up_files.name,prefix+'FileSize': up_files.size,prefix+'FileType':up_files.content_type,prefix+'FilePath':'',prefix+'FileHash':digest}
        #insert into all file tables
        #file_fields.update(form_data)
     
        
        #props = request.session.get('UserProperties')
        #pdb.set_trace()

        fileTable = rdbms_cls.insert_data(file_fields,resource_id,org_id,user.id)
        f_dir = os.path.join(directory,up_files.name)

        if fileTable:
            ret_val = {'file_id':fileTable.id}
            file_name,file_ext = os.path.splitext(f_dir)
            f_dir = os.path.join(directory,str(resource_id)+'_'+str(fileTable.id)+file_ext)
            destination = open(f_dir, 'wb+')
            hasher = hashlib.md5()
            for chunk in up_files.chunks():
                destination.write(chunk)
                hasher.update(chunk)
        
            digest=hasher.hexdigest()
            destination.close()
            update_fields= {'id':fileTable[0],prefix+'FilePath':f_dir,prefix+'FileHash':digest}
            
            rd=None
            session = rdbms_cls.Session()
            with session.begin():

                rd = session.query(ResourceDefSa).filter(ResourceDefSa.id==resource_id).first()

            if rd and settings.USE_BLOCKCHAIN:
                if rd.IsBlockchain:
                #add record to blockchain
                 
                  datasetData = file_name
                  h =  digest
                  usr = str(user.id)
                  p_key=int(str(resource_id)+str(fileTable.id).zfill(12))
                  
                  trx_json = {'pKey':p_key,'datasetDef_Id':resource_id,'datasetRec_Id':fileTable.id,'dataset_Data': datasetData,'h':h,'v':0,'user':usr}
                  contract = 'kataly1.code'
                  action='createds'
                  
                  jsonResponseEOS,block_id,status = send_transaction(trx_json,contract,action)
                  #pdb.set_trace()
                  if status == 200:
                    bchain_id= int(jsonResponseEOS['processed']['action_traces'][0]['console'])
             
                    jsonResponseEOS2,status2 = get_table_rows('kataly1.code','dataset','kataly1.code','primary','uint64_t',1,bchain_id)
             
                  #save transaction id
                  if status==200:
                    table = 'resource'+str(resource_id)+'_'+str(org_id)
                    update_fields['id'] = fileTable.id
                    update_fields[table+'_TrxId'] = jsonResponseEOS['transaction_id']
                    update_fields[table+'_BChainRecId'] = bchain_id
                    update_fields[table+'_BlockId'] = str(block_id)
                    #result = mutant_update_data(mainResource,resource_id,org_id,user_id)
                    ret_val.update({'jsonResponseEOS':jsonResponseEOS,'jsonResponseEOS2':jsonResponseEOS2})
            fileTableUpdate,fileTableUpdateFirst = rdbms_cls.update_data_no_history(update_fields,resource_id,org_id,user)
        else:
            Response(data='Error creating file record for '+up_files.name,status=500)
                 
        return Response(data=ret_val,status=201)

class VerifyFileViewSet(viewsets.GenericViewSet):
    parser_classes = (FormParser, MultiPartParser,)

    def create(self, request):
        
        #pdb.set_trace() #IMPORT #FileUploadView #DBG
        up_files = request.FILES['file']
        blockchain_file_id = request.data['blockchain_file_id']
       
        user = request.user
        hasher = hashlib.md5()
        for chunk in up_files.chunks():
          hasher.update(chunk)
        
        digest=hasher.hexdigest()
             
        jsonResponseEOS,statusEOS = get_table_rows('kataly1.code','dataset','kataly1.code','primary','uint64_t',1,blockchain_file_id)
             
        
        if statusEOS!=200:
                  
            Response(data='Error verifying file '+str(blockchain_file_id),status=500)
                 
        return Response(data={'jsonResponseEOS':jsonResponseEOS,'fileHash':digest},status=statusEOS)
      
class DownloadFileViewSet(viewsets.GenericViewSet):
  
     def list(self, request,resourceId,fileId):
        
        if 'fileId' in self.kwargs:
           file_id=int(self.kwargs['fileId'])
        else:
           file_id = None
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None

        if file_id is None or resource_id is None: 
          return Response(data='Error in parameters',status=500)

        #get file from resource table
        user = request.user

        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user) 

        prefix = 'resource'+str(resource_id)+'_'+str(org_id)+'_'
        rval=None
        session = rdbms_cls.Session()
        response = None
        with session.begin():
            file_object = file(resource_id,'file','','','','',user,language,org_id,{})
            file_object.set_session(session)
            rfiles = file_object.get_file_rec(file_id)
            if len(rfiles)==1:
                rfile = rfiles[0]
            else:
                rfile = None
            if rfile:
                document = open(rfile[prefix+'FilePath'], 'rb')
                response = HttpResponse(FileWrapper(document), content_type=rfile[prefix+'FileType'])
                response['Content-Disposition'] = 'inline; filename="%s"' % rfile[prefix+'FileName']
            else:
                return Response(data='File not found',status=500) 
        return response