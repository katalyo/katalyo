from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser,FileUploadParser,FormParser, MultiPartParser
import json
#from django.forms.models import model_to_dict
import os
import uuid
from django.db.models import Q
#import katalyonode.functions.taskprocessing
from itertools import chain
from django.core import serializers
from katalyonode.settings import FILES_DIR
from django.template import RequestContext
from django.db.models import Prefetch
#from django.contrib.auth.models import User, Group
from rest_framework import viewsets,permissions,authentication,pagination
from rest_framework.pagination import PageNumberPagination
#import katalyonode.serializers.serializers 
from katalyonode.serializers.serializers import UserSerializer, GroupSerializer,OrganisationsSerializer
from katalyonode.serializers.serializerssettings import CodesSerializer,CodesDetailSerializer,CodesHeadSerializer,CodesHeadLangSerializer,CodesDetailLangSerializer,GetCodesDetailSerializer
from katalyonode.serializers.serializerssettings import AddUpdateCodesDetailSerializer,AddUpdateCodesDetailLangSerializer,GetCodesDetailByIdLangSerializer,GetCodesByLangSerializer
from katalyonode.serializers.serializersresources import GetResourceFormSerializer,SaveResourceFormSerializer,GetResourceDataSerializer,MutantModelSerializer,RelatedResourceSerializer
from katalyonode.serializers.serializerstasks import TaskParametersSerializer,AssignmentDefUserSerializer,AssignmentDefGroupSerializer,TaskAssignmentDefUserSerializer,TaskAssignmentDefGroupSerializer
from katalyonode.serializers.serializerstasks import TaskInitiationInstanceSerializer,InitiatedTasksSerializer,FormWidgetSerializer,TaskCodesActionsSerializer,SaveTaskParametersSerializer,TaskAssignmentsSerializer
from katalyonode.serializers.serializersresources import MutantModelRelatedSerializer,ResourceDefSerializer,ResourceDefLangSerializer,ResourceDefByLangSerializer,ResourceDefAllSerializer
from katalyonode.serializers.serializersresources import SaveResourceDefLangSerializer,GetResourceDefSerializer
from katalyonode.serializers.serializerstasks import ClaimedTasksSerializer,TaskExecutionInstancesSerializer,TaskOutcomeDefinitionSerializer
#import katalyonode.models
from datetime import datetime,timezone,timedelta
from rest_framework.permissions import AllowAny
from katalyonode.models.models import ResourceLinks,ResourceDef,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang,PresentationElementsTasks
from katalyonode.models.models import FilesTable,TaskParameters,TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskInitiationInstance,FilesToResource,TaskCodesActions,ErrorLog
from katalyonode.models.models import TaskInitiationInstance,TaskExecutionInstances,AuditTrailTaskStatus,TaskAssignments,TaskExecutionInstanceToResource,TaskDefWidgetDependencies
from katalyonode.models.models import TaskOutcomesHeadLang,TaskOutcomesDetailLang,TaskOutcomesHead,Menus,UserExtendedProperties,TaskOutcomesDetail,ResourceModelDefinition,TaskTransactions
#from katalyonode.functions.formprocessing import GetResourceFormDB,ProcessInitiateForm,ExtendResourceFormJSON
#from katalyonode.functions.formprocessing import ProcessExecuteForm,PreProcessResourceWidget
#from katalyonode.functions.taskprocessing import InitiateTask,ExecuteTask,ProcessTaskAssignments,ProcessTaskParameters,ProcessPreviousTask,ProcessTaskStatusChange,GetStatusCodeByName,PreExecuteTask
#from katalyonode.functions.taskprocessing import IsUserAuthorisedToCreateTask,StartTask,StartPublicTask
from katalyonode.views.viewsresources import StandardResultsSetPagination
from katalyonode.functions.widgets.form.ktTaskViewResource import ktTaskViewResource
from katalyonode.functions.widgets.form.ktTaskUpdateResource import ktTaskUpdateResource
from katalyonode.functions.resources.taskdef import task
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.rdbms.alchemymodels import TaskParametersSa,TaskAssignmentDefUserSa,TaskDefWidgetDependenciesSa,ResourceLinksSa,PresentationElementsSa,ResourceDefLangSa,TaskAssignmentDefUserSa,TaskAssignmentDefGroupSa
from katalyonode.functions.rdbms.alchemymodels import auth_user,auth_group,codesdetail,CodesHeadSa,TaskCodesActionsSa,TaskTransactionsSa,LanguageSa,TaskOutcomesDetailSa,TaskOutcomesDetailLangSa,TaskOutcomesHeadSa,TaskInitiationInstanceSa
from katalyonode.models.models import TaskParameters
from sqlalchemy import desc,and_
import pdb


rdbms_cls = RdbmsClass()
helper = ResourceHelperClass()

class TaskParametersViewSet(viewsets.ModelViewSet):
   queryset = TaskParameters.objects.all()
   #serializer_class = TaskParametersSerializer
   
   def create(self, request):
      
      params = request.data
      user = request.user
      #pdb.set_trace()

      org_id=None
      status=201

      
      org_id,language = helper.get_lang_org(user)

      if params['DefaultOutcomeId']==0:
        params['DefaultOutcomeId'] = None
      #pdb.set_trace()     
      if params.get('TaskOutcomeCodeId',None) == 0:
        params['TaskOutcomeCodeId'] = None
        params['TaskOutcomeHeadId'] = None


      #params['ChangedDateTime'] = timezone.now()
      #params['TaskOutcomeCodeId'] = 0
 
      #td = TaskParametersSerializer(data=params)
      session = rdbms_cls.Session()
      with session.begin():
        td = TaskParametersSa(ResourceDefId = params.get('ResourceDefId',None),
                              OrganisationId  =  org_id,
                              TaskType = params.get('TaskType',None),
                              MaxActualOwners = params.get('MaxActualOwners',0),
                              NoLimit =params.get('NoLimit',None),
                              SelfAssigned =params.get('SelfAssigned',False),
                              AutoAssign = params.get('AutoAssign',False),
                              AutoStart = params.get('AutoStart',False),
                              AutoClaim = params.get('AutoClaim',False),
                              AutoInitiate = params.get('AutoInitiate',False),
                              CanDelegate = params.get('CanDelegate',False),
                              CanHold =params.get('CanHold',False),
                              CanRelease = params.get('CanRelease',False),
                              AutoExecute = params.get('AutoExecute',False),
                              InfiniteTask = params.get('InfiniteTask',False),
                              TaskInitiatorsSaved = params.get('TaskInitiatorsSaved',False),
                              PotentialOwnersSaved = params.get('PotentialOwnersSaved',False),
                              TaskOutcomesSaved = params.get('TaskOutcomesSaved',False),
                              TaskOutcomeHeadId = params.get('TaskOutcomeHeadId',None),
                              DefaultOutcomeId = params.get('DefaultOutcomeId',None),
                              UniqueId =params.get('UniqueId',None),
                              UniqueIdGroup = params.get('UniqueIdGroup',None),
                              PublicTask = params.get('PublicTask',False),
                              OnMarketAsDef = params.get('OnMarketAsDef',False),
                              OnMarketAsService = params.get('OnMarketAsService',False),
                              DefaultLang = language.id,
                              AssignTypeInitiator = params.get('AssignTypeInitiator',None),
                              AssignTypeInitiatorGroup =  params.get('AssignTypeInitiatorGroup',None),
                              AssignTypeOwner =  params.get('AssignTypeOwner',None),
                              AssignTypeOwnerGroup =  params.get('AssignTypeOwnerGroup',None),
                              AssignTypeAdmin =  params.get('AssignTypeAdmin',None),
                              UniqueName = params.get('UniqueName',None),
                              Parameters =  json.dumps(params.get('Parameters',{})),
                              Version = params.get('Version',1),
                              CreatedBy = user.id,
                              CreatedDateTime = datetime.now(timezone.utc))
        session.add(td)
        session.flush()
        if params.get('SetupAssignments',False):
          #ToDo sqlalchemy migration
          #ta1=TaskAssignmentDefUser.objects.create(AssignType=params['AssignTypeInitiator'],AssignGroup='taskInitiators',VersionFrom=1,VersionTo=1,TaskDefId_id=params['ResourceDefId'],AssigneeId=user,CreatedBy=user,CreatedDateTime=timezone.now())
          ta1=TaskAssignmentDefUserSa(AssignType=params['AssignTypeInitiator'],AssignGroup='taskInitiators',VersionFrom=1,VersionTo=1,TaskDefId=params['ResourceDefId'],AssigneeId=user.id,CreatedBy=user.id,CreatedDateTime=datetime.now(timezone.utc))
          session.add(ta1)
          session.flush()
          if ta1.id>0:
            msg = 'Task parameters created successfuly!'
          else:
            msg = 'Error creating default task initiator!'
            status = 500
        else:
          msg = 'Task parameters created successfuly!'
        #pdb.set_trace()
      return Response(data={'data':td._asdict(),'msg':msg},status=status)
    
   def update(self, request,pk):
      
      params = request.data
      user = request.user
      
      if params['TaskOutcomeHeadId']==0:
         params['TaskOutcomeHeadId'] = None
      if params['DefaultOutcomeId']==0:
         params['DefaultOutcomeId'] = None
      
      tdefDict = None
      tdef = None
      session = rdbms_cls.Session()
      with session.begin():
        #tdef = TaskParameters.objects.get(id=pk)
        tdefs = session.query(TaskParametersSa).filter(TaskParametersSa.id==pk).all()
        if len(tdefs)>0:
          tdef = tdefs[0]
        else:
          return Response(data={'msg':'Task parameters update error 1'},status=500)

        if tdef is not None:
          params ['Version'] = tdef.Version+1 
          tdef.TaskType = params['TaskType']
          tdef.MaxActualOwners =  params['MaxActualOwners']
          tdef.SelfAssigned = params['SelfAssigned']
          tdef.AutoStart = params['AutoStart']
          tdef.AutoClaim = params['AutoClaim']
          tdef.AutoInitiate= params['AutoInitiate']
          tdef.AutoExecute= params['AutoExecute']
          tdef.CanDelegate= params['CanDelegate']
          tdef.CanHold= params['CanHold']
          tdef.CanRelease= params['CanRelease']
          tdef.TaskOutcomeHeadId= params['TaskOutcomeHeadId']
          tdef.DefaultOutcomeId= params['DefaultOutcomeId']
          tdef.Version = params['Version']
          tdef.ChangedDateTime= datetime.now(timezone.utc)
          tdef.ChangedBy= request.user.id
          tdefDict = tdef._asdict()
        else:
          return Response(data={'msg':'Task parameters update error 2'},status=500)

      return Response(data={'data':tdefDict,'msg':'Task parameters updated successfuly!'},status=200)
    
      
class TaskParametersByResourceIdViewSet(viewsets.ModelViewSet):
    queryset=TaskParameters
   #serializer_class = TaskParametersSerializer
    """
    def get_queryset(self):
        #pdb.set_trace()
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
        else:
          resource_id = None
        #ToDo sqlalchemy migration
        if resource_id is not None:
          queryset = TaskParameters.objects.filter(ResourceDefId=resource_id)
        else:
          queryset = TaskParameters.objects.all()
  
        return queryset
    """
    def list(self,request,resourceId):
        #pdb.set_trace()
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
        else:
          resource_id = None

        tp_json = {}
        task_params = []
        session = rdbms_cls.Session()
        with session.begin():

          if resource_id is not None:
            #queryset = TaskParameters.objects.filter(ResourceDefId=resource_id)
            queryset = session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==resource_id).all()
          else:
            queryset =  session.query(TaskParametersSa).all()
          
          for tp in queryset:
            tp_json['id']=tp.id
            tp_json['ResourceDefId']=tp.ResourceDefId
            tp_json['TaskType']=tp.TaskType
            tp_json['MaxActualOwners']= tp.MaxActualOwners 
            tp_json['SelfAssigned']=tp.SelfAssigned 
            tp_json['AutoStart']=tp.AutoStart 
            tp_json['AutoClaim']=tp.AutoClaim 
            tp_json['AutoInitiate']=tp.AutoInitiate 
            tp_json['AutoExecute']=tp.AutoExecute 
            tp_json['CanDelegate']=tp.CanDelegate 
            tp_json['CanHold']=tp.CanHold 
            tp_json['CanRelease']=tp.CanRelease 
            tp_json['TaskOutcomeHeadId']=tp.TaskOutcomeHeadId
            tp_json['DefaultOutcomeId']=tp.DefaultOutcomeId
            tp_json['Version']=tp.Version
            tp_json['CreatedDateTime']=tp.CreatedDateTime
            tp_json['CreatedBy']=tp.CreatedBy 
            tp_json['ChangedDateTime']=tp.ChangedDateTime
            tp_json['ChangedBy']=tp.ChangedBy 
            task_params.append(tp_json)

        return Response(data=task_params)
      
class TaskAssignmentsByResourceIdViewSet(viewsets.GenericViewSet):
    serializer_class = AssignmentDefUserSerializer
  
    def list(self, request,resourceId):
              
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        tp_json = {}
        users=[]
        groups = []
        #get data
        session = rdbms_cls.Session()
        with session.begin():
          #querysetUser =  TaskAssignmentDefUser.objects.filter(TaskDefId=resource_id).select_related('AssigneeId','TaskDefId').order_by('AssignGroup')
          #querysetGroup = TaskAssignmentDefGroup.objects.filter(TaskDefId=resource_id).select_related('AssigneeId','TaskDefId').order_by('AssignGroup')
          querysetUser =  session.query(TaskAssignmentDefUserSa,auth_user).join(auth_user,TaskAssignmentDefUserSa.AssigneeId==auth_user.id).filter(TaskAssignmentDefUserSa.TaskDefId==resource_id).order_by(TaskAssignmentDefUserSa.AssignGroup).all()
          querysetGroup = session.query(TaskAssignmentDefGroupSa,auth_group).join(auth_group,TaskAssignmentDefGroupSa.AssigneeId==auth_group.id).filter(TaskAssignmentDefGroupSa.TaskDefId==resource_id).order_by(TaskAssignmentDefGroupSa.AssignGroup).all()
         
          for u in querysetUser:
            users.append({
            'id': u[0].id,
            'AssigneeId': u[1]._asdict(),
            'TaskDefId' : u[0].TaskDefId,
            'AssignType' : u[0].AssignType,
            'SubAssignType' : u[0].SubAssignType,
            'AssignGroup' : u[0].AssignGroup,
            'CreatedBy' : u[0].CreatedBy,
            'CreatedDateTime' : u[0].CreatedDateTime,
            'ChangedBy' : u[0].ChangedBy,
            'ChangedDateTime' : u[0].ChangedDateTime
            })

          

          for g in querysetGroup:
            groups.append({
             'id': g[0].id,
            'AssigneeId': g[1]._asdict(),
            'TaskDefId' : g[0].TaskDefId,
            'AssignType' : g[0].AssignType,
            'SubAssignType' : g[0].SubAssignType,
            'AssignGroup' : g[0].AssignGroup,
            'CreatedBy' : g[0].CreatedBy,
            'CreatedDateTime' : g[0].CreatedDateTime,
            'ChangedBy' : g[0].ChangedBy,
            'ChangedDateTime' : g[0].ChangedDateTime

            })
          
          #td = TaskParameters.objects.filter(ResourceDefId=resource_id)  
          taskParams = session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==resource_id).all()
          #taskParams = None
          for tp in taskParams:
            tp_json['id']=tp.id
            tp_json['ResourceDefId']=tp.ResourceDefId
            tp_json['TaskType']=tp.TaskType
            tp_json['MaxActualOwners']= tp.MaxActualOwners 
            tp_json['SelfAssigned']=tp.SelfAssigned 
            tp_json['AutoStart']=tp.AutoStart 
            tp_json['AutoClaim']=tp.AutoClaim 
            tp_json['AutoInitiate']=tp.AutoInitiate 
            tp_json['AutoExecute']=tp.AutoExecute 
            tp_json['CanDelegate']=tp.CanDelegate 
            tp_json['CanHold']=tp.CanHold 
            tp_json['CanRelease']=tp.CanRelease 
            tp_json['TaskOutcomeHeadId']=tp.TaskOutcomeHeadId
            tp_json['DefaultOutcomeId']=tp.DefaultOutcomeId
            tp_json['Version']=tp.Version
            tp_json['CreatedDateTime']=tp.CreatedDateTime
            tp_json['CreatedBy']=tp.CreatedBy 
            tp_json['ChangedDateTime']=tp.ChangedDateTime
            tp_json['ChangedBy']=tp.ChangedBy 
            #pdb.set_trace() 
        return Response({'taskDefinition':tp_json,'users':users,'groups':groups})
      
   

class SaveTaskAssignmentsViewSet(viewsets.GenericViewSet):
    #serializer_class = AssignmentDefUserSerializer
    #queryset = TaskAssignmentDefUser.objects.select_related('AssigneeId')
    
    def create(self, request,taskId,assignType):
        
        
        if 'taskId' in self.kwargs:
          task_id=int(self.kwargs['taskId'])
        else:
          task_id = None
          Response(data={'msg':'Input parameter taskId is missing'},status=400)
        
        if 'assignType' in self.kwargs:
          assign_type_int=int(self.kwargs['assignType'])
        else:
          assign_type_int = None
          Response(data={'msg':'Input parameter assignType is missing'},status=400)
          
        assignUList=[];
        assignGList=[];
        #pdb.set_trace()
        ta = request.data.get('ta',None)
        self_assigned = request.data.get('SelfAssigned',False)
        task_type = request.data.get('taskType',None)
        public_task = request.data.get('PublicTask',False)
        unique_name = request.data.get('UniqueName','')

        user = request.user
        if public_task and (unique_name=='' or unique_name is None):
         return Response(data={'msg':'Task unique name is empty!'},status=400)
        
        session = rdbms_cls.Session()
        with session.begin():
          if public_task:
          #unique_check = TaskParameters.objects.filter(UniqueName = unique_name).exclude(ResourceDefId_id=task_id)
           unique_check=session.query(TaskParametersSa).filter(TaskParametersSa.UniqueName == unique_name,TaskParametersSa.ResourceDefId==task_id).all()
           if len(unique_check)!=0:
              return Response(data={'msg':'Task unique name '+unique_name+' already exists!'},status=400)
          #serializerU = TaskAssignmentDefUserSerializer(data=ta['users']['list'],context={'user':request.user},many=True)
          
          inUsers = ta.get('users',{}).get('list',[])
          for u in inUsers:

            if u.get('id',None) is None:
              uData = TaskAssignmentDefUserSa(AssigneeId = u.get('AssigneeId',None),
                                              TaskDefId = u.get('TaskDefId',None),
                                              AssignType = u.get('AssignType',None),
                                              SubAssignType = u.get('SubAssignType',None),
                                              AssignGroup = u.get('AssignGroup',None),
                                              VersionFrom = 1,
                                              VersionTo = 1,
                                              CreatedBy = user.id,
                                              CreatedDateTime = datetime.now(timezone.utc))
              session.add(uData)
              session.flush()
              assignUList.append(u.get('id',None))
            else:
              tadu = session.get(TaskAssignmentDefUserSa,u.get('id',None))
              tadu.AssigneId=u.get('AssigneeId',None)
              tadu.TaskDefId = u.get('TaskDefId',None)
              tadu.AssignType = u.get('AssignType',None)
              tadu.SubAssignType = u.get('SubAssignType',None)
              tadu.AssignGroup = u.get('AssignGroup',None)
              tadu.VersionFrom = u.get('VersionFrom',1)
              tadu.VersionTo = u.get('VersionTo',1)
              tadu.ChangedBy = user.id,
              tadu.ChangedDateTime = datetime.now(timezone.utc)
              assignUList.append(u.get('id',None))
          
          #serializerG = TaskAssignmentDefGroupSerializer(data=ta['groups']['list'],context={'user':request.user},many=True)
          inGroups = ta.get('groups',{}).get('list',[])
          for g in inGroups:

            if g.get('id',None) is None:
              gData = TaskAssignmentDefGroupSa(AssigneeId = g.get('AssigneeId',None),
                                              TaskDefId = g.get('TaskDefId',None),
                                              AssignType = g.get('AssignType',None),
                                              SubAssignType = g.get('SubAssignType',None),
                                              AssignGroup = g.get('AssignGroup',None),
                                              VersionFrom = 1,
                                              VersionTo = 1,
                                              CreatedBy = user.id,
                                              CreatedDateTime = datetime.now(timezone.utc))
              session.add(gData)
              session.flush()
              assignGList.append(g.get('id',None))
            else:
              tadg = session.get(TaskAssignmentDefGroupSa,g.get('id',None))
              tadg.AssigneId=g.get('AssigneeId',None)
              tadg.TaskDefId = g.get('TaskDefId',None)
              tadg.AssignType = g.get('AssignType',None)
              tadg.SubAssignType = g.get('SubAssignType',None)
              tadg.AssignGroup = g.get('AssignGroup',None)
              tadg.VersionFrom = g.get('VersionFrom',1)
              tadg.VersionTo = g.get('VersionTo',1)
              tadg.ChangedBy = user.id,
              tadg.ChangedDateTime = datetime.now(timezone.utc)
              assigGList.append(g.get('id',None))

          
          if assign_type_int==0:
            a_type='taskInitiators'
          else:
            a_type='potentialOwners'
          
   
          #TaskAssignmentDefUser.objects.filter(TaskDefId_id=task_id,AssignGroup=a_type).exclude(id__in=assignUList).delete()
          deleteUserAssignments = session.query(TaskAssignmentDefUserSa).filter(TaskAssignmentDefUserSa.TaskDefId==task_id,TaskAssignmentDefUserSa.AssignGroup==a_type,TaskAssignmentDefUserSa.id.not_in(assignUList)).delete()
         
          #TaskAssignmentDefGroup.objects.filter(TaskDefId_id=task_id,AssignGroup=a_type).exclude(id__in=assignGList).delete()
          deleteGroupAssignments = session.query(TaskAssignmentDefGroupSa).filter(TaskAssignmentDefGroupSa.TaskDefId==task_id,TaskAssignmentDefGroupSa.AssignGroup==a_type,TaskAssignmentDefGroupSa.id.not_in(assignGList)).delete()
         
         
          #pdb.set_trace()
          if assign_type_int==0:
            
            if 'tiAssignType' in ta['users']:
              tiAssignType=ta['users']['tiAssignType']
            else:
              tiAssignType = ''
            if 'tiAssignType' in ta['groups']:
              tiAssignTypeGroup=ta['groups']['tiAssignType']
            else:
              tiAssignTypeGroup = ''
            #TaskParameters.objects.filter(ResourceDefId_id=task_id).update(AssignTypeInitiator=tiAssignType,AssignTypeInitiatorGroup=tiAssignTypeGroup,AssignTypeAdmin='',PublicTask=public_task,UniqueName=unique_name,SelfAssigned=self_assigned,TaskInitiatorsSaved=True,TaskType=task_type)
            task_params_for_update=session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId == task_id).all()
            for upd in task_params_for_update:
              upd.AssignTypeInitiator=tiAssignType
              upd.AssignTypeInitiatorGroup=tiAssignTypeGroup
              upd.PublicTask=public_task
              upd.TaskType=task_type
              upd.PublicTask=public_task
              upd.UniqueName=unique_name
              upd.SelfAssigned=self_assigned
              upd.AssignTypeAdmin=''
              upd.TaskInitiatorsSaved=True
            
          else:
            if 'poAssignType' in ta['users']:
              poAssignType=ta['users']['poAssignType']
            else:
              poAssignType=None
           
            if 'poUniqueId' in ta['users']:
              poUniqueId=ta['users']['poUniqueId']
            else:
              poUniqueId=None
            
            if 'poAssignType' in ta['groups']:
              poAssignTypeGroup=ta['groups']['poAssignType']
            else:
              poAssignTypeGroup=None
              
            if 'poUniqueId' in ta['groups']:
              poUniqueIdGroup=ta['groups']['poUniqueId']
            else:
              poUniqueIdGroup=None
            #TaskParameters.objects.filter(ResourceDefId_id=task_id).update(TaskType=task_type,PublicTask=public_task,UniqueName=unique_name,SelfAssigned=self_assigned,AssignTypeOwner=poAssignType,AssignTypeOwnerGroup=poAssignTypeGroup,UniqueId=poUniqueId,UniqueIdGroup=poUniqueIdGroup,AssignTypeAdmin='',PotentialOwnersSaved=True)
            task_params_for_update=session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId == task_id).all()
            for upd in task_params_for_update:
              upd.TaskType=task_type
              upd.PublicTask=public_task
              upd.UniqueName=unique_name
              upd.SelfAssigned=self_assigned
              upd.AssignTypeOwner=poAssignType
              upd.AssignTypeOwnerGroup=poAssignTypeGroup
              upd.UniqueId=poUniqueId
              upd.UniqueIdGroup=poUniqueIdGroup
              upd.AssignTypeAdmin=''
              upd.PotentialOwnersSaved=True
          msg = 'permissions'
          if assign_type_int==1:
            msg='assignments'
        return Response(data={'msg':'Task '+msg+' saved successfuly'},status=200)

class GetTaskCodesActionsByIdViewSet(viewsets.GenericViewSet):
    serializer_class = TaskCodesActionsSerializer
    queryset = TaskCodesActions.objects.all()
    
    def list(self, request,taskId,lang,codeId):
        
        codes=[] 
        if 'taskId' in self.kwargs:
           task_id=int(self.kwargs['taskId'])
        else:
            return Response(data='Missing parameter taskId',status=400) 
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Missing parameter language',status=400)
        if 'codeId' in self.kwargs:
           code_id=int(self.kwargs['codeId'])
        else:
            return Response(data='Missing parameter codeId',status=400)
        
        session = rdbms_cls.Session()
        with session.begin():
          if code_id==0:
            #querysetTaskParams = TaskParameters.objects.filter(ResourceDefId_id=task_id)
            querysetTaskParams = session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==task_id).all()
          
            for td in querysetTaskParams:
                code_id=td.TaskOutcomeHeadId
        
       
          #querysetCodes = CodesDetailLang.objects.filter(CodesDetailId__CodesHeadId_id=code_id,Lang_id=language).select_related()
          querysetCodes = session.query(codesdetail,CodesHeadSa).join(CodesHeadSa,CodesHeadSa.id==code_id).filter(codesdetail.Lang == language).all()
          #querysetActions = TaskCodesActions.objects.filter(TaskDefId_id=task_id)
          querysetActions = session.query(TaskCodesActionsSa).filter(TaskCodesActionsSa.TaskDefId == task_id).all()
          for code in querysetCodes:
                #pdb.set_trace()
                codeItem = {'id1':code.CodesDetailId_id,'name1':code.Value,'actions':[]}
                for action in querysetActions:
                  if action.CodeId1_id==code.CodesDetailId_id or action.CodeId2_id==code.CodesDetailId_id:
                    actionItem = {'toaId':action.id,'type':action.ActionType,'id1':action.ActionId,'name1':action.ActionName,'id2':None,'name2':None,'taskId':action.InitiateTaskId}
                    #pdb.set_trace()
                    codeItem['actions'].append(actionItem)
                codes.append (codeItem);
       
        return Response(codes)
      
class GetTaskCodesActionsByNameViewSet(viewsets.GenericViewSet):
    serializer_class = TaskCodesActionsSerializer
    queryset = TaskCodesActions.objects.all()
    
    def list(self, request,taskId,lang,codeName):
        
        codes=[] 
        if 'taskId' in self.kwargs:
           task_id=int(self.kwargs['taskId'])
        else:
            return Response(data='Missing parameter taskId',status=400) 
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Missing parameter language',status=400)
        if 'codeName' in self.kwargs:
           code_name=self.kwargs['codeName']
        else:
            return Response(data='Missing parameter codeName',status=400)
        
        if code_name=='':  
          return Response(data='Parameter codeName is blank',status=400)
        
        #querysetTaskDef = TaskDef.objects.filter(ResourceDefId_id=task_id)
        session = rdbms_cls.Session()
        with session.begin():
          #querysetCodes = CodesDetailLang.objects.filter(CodesDetailId__CodesHeadId__CodeHeadName=code_name,Lang_id=language).select_related()
          querysetCodes = session.query(codesdetail,CodesHeadSa).join(CodesHeadSa,CodesHeadSa.CodeHeadName==code_name).filter(codesdetail.Lang == language).all()
          if len(querysetCodes)==0:
            return Response(data='Codes'+ code_name + 'not found',status=500)
          codeItem=None
          
          #querysetActions = TaskCodesActions.objects.filter(TaskDefId_id=task_id,CodeName1=code_name).select_related('CodeId1','CodeId2').order_by('CodeId1_id','CodeId2_id')
          querysetActions = session.query(TaskCodesActionsSa).filter(TaskCodesActionsSa.TaskDefId == task_id,TaskCodesActionsSa.CodeName1==code_name).order_by(TaskCodesActionsSa.CodeId1,TaskCodesActionsSa.CodeId2).all()
          #pdb.set_trace()
          oldCodeId1 = None
          for action in querysetActions:
            #pdb.set_trace()
            #codeItem={}
            actionItem = {'toaId':action.id,'type':action.ActionType,'id1':action.ActionId,'name1':action.ActionName,'taskId':action.InitiateTaskId,'actionParams':action.ActionParams}        
            
            if oldCodeId1 is None:
              #ToDo - change this to alchemy
              #qc1 = querysetCodes.filter(CodesDetailId=action.CodeId1)
              if len(qc1)>0:
                codeItem = {'id1':qc1[0][0].id,'value1':qc1[0][0].Name,'name1':qc1[0][0].Value,'actions':[]}
                
              if action.CodeId2 is not None:
                  #qc2 = querysetCodes.filter(CodesDetailId=action.CodeId2)
                  qc2=filter(lambda x: x.CodesDetailId == action.CodeId2, querysetCodes[0])
                  if len(qc2)>0:
                    codeItem['id2'] = qc2[0].id
                    codeItem['name2'] = qc2[0].Value
                    codeItem['value2'] = qc2[0].Name
              
              codeItem['actions'].append(actionItem)
            
            else:
              #pdb.set_trace()
              if oldCodeId1 == action.CodeId1 and oldCodeId2 == action.CodeId2:
                codeItem['actions'].append(actionItem)
                
              else:
                codes.append (codeItem)
                #qc1 = querysetCodes.filter(CodesDetailId=action.CodeId1)
                qc1=filter(lambda x: x.CodesDetailId == action.CodeId1, querysetCodes[0])
                if len(qc2)>0:
                  codeItem = {'id1':qc1[0].id,'value1':qc1[0].Name,'name1':qc1[0].Value,'actions':[]}
                if action.CodeId2 is not None:
                  #qc2 = querysetCodes.filter(CodesDetailId=action.CodeId2)
                  qc2=filter(lambda x: x.CodesDetailId == action.CodeId2, querysetCodes[0])
                  codeItem['id2'] = qc2[0].id
                  codeItem['name2'] = qc2[0].Value
                  codeItem['value2'] = qc2[0].Name
                codeItem['actions'].append(actionItem)  
            
            oldCodeId1 = action.CodeId1_id
            oldCodeId2 = action.CodeId2_id
          #pdb.set_trace()
          if codeItem is not None:
            codes.append (codeItem)
          #pdb.set_trace()
        return Response(codes)

class GetTaskOutcomesDefViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskCodesActionsSerializer
    queryset = TaskOutcomesHeadLang.objects.all()
    
    def list(self, request,lang):
         
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Parameter language is missing',status=404) 

        outcomesList=[]
        session = rdbms_cls.Session()
        with session.begin():
          #outcomesDefs = TaskOutcomesHeadLang.objects.filter(Lang_id=language,TaskOutcomeHeadId__Active=True).select_related()
          outcomesDefs=session.query(TaskOutcomesHeadSa,TaskOutcomesHeadLangSa).join(TaskOutcomesHeadLangSa,and_(TaskOutcomesHeadLangSa.TaskOutcomeHeadId==TaskOutcomesHeadSa.id,TaskOutcomesHeadLangSa.Lang==language)).filter(TaskOutcomesHeadSa.Active == True).all()
          for outcome in outcomesDefs:
            outcomesList.append({'id':outcome[0].id,'name':outcome[1].Name,'outcome_name':outcome[0].OutcomeName})
        
        return Response(outcomesList)

class GetTaskOutcomesDetailsViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskCodesActionsSerializer
    queryset = TaskCodesActions.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()
    def list(self, request,taskId,codeId,lang):
        
        outcomes=[] 
        if 'taskId' in self.kwargs:
           task_id=int(self.kwargs['taskId'])
        else:
            return Response(data='Missing parameter taskId',status=400) 
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Missing parameter language',status=400)
        if 'codeId' in self.kwargs:
           code_id=int(self.kwargs['codeId'])
        else:
            return Response(data='Missing parameter outcomeId',status=400)
        

        session = rdbms_cls.Session()
        with session.begin():

          if task_id > 0:
              if code_id==0:

                  #querysetTaskParams = TaskParameters.objects.filter(ResourceDefId_id=task_id)
                  querysetTaskParams = session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==task_id).all()
                  for td in querysetTaskParams:
                      code_id=td.TaskOutcomeHeadId
          
              #querysetOutcomes = TaskOutcomesDetailLang.objects.filter(TaskOutcomeDetailId__TaskOutcomeHeadId_id=code_id,Lang_id=language,TaskOutcomeDetailId__Active=True).select_related()      
              querysetOutcomes = session.query(TaskOutcomesDetailSa,TaskOutcomesDetailLangSa,TaskOutcomesHeadSa).join(TaskOutcomesHeadSa,and_(TaskOutcomesDetailSa.TaskOutcomeHeadId==TaskOutcomesHeadSa.id,TaskOutcomesHeadSa.id==code_id)).join(TaskOutcomesDetailLangSa,and_(TaskOutcomesDetailLangSa.Lang==lang,TaskOutcomesDetailLangSa.TaskOutcomeDetailId==TaskOutcomesDetailSa.id)).filter(TaskOutcomesDetailSa.Active==True).all()
          
          else:
              #querysetOutcomes = TaskOutcomesDetailLang.objects.filter(Lang_id=language,TaskOutcomeDetailId__Active=True).select_related()
              querysetOutcomes = session.query(TaskOutcomesDetailSa,TaskOutcomesDetailLangSa).join(TaskOutcomesDetailLangSa,TaskOutcomesDetailLangSa.Lang==lang,TaskOutcomesDetailLangSa.TaskOutcomeDetailId==TaskOutcomesDetailSa.id).filter(TaskOutcomesDetailSa.Active==True).all()
          
          #querysetActions = TaskCodesActions.objects.filter(TaskDefId=task_id)
          querysetActions = session.query(TaskCodesActionsSa).filter(TaskCodesActionsSa.TaskDefId == task_id).all()
          for outcome in querysetOutcomes:
                #pdb.set_trace()
                outcomeItem = {'id1':outcome[0].id,'name1':outcome[1].DisplayName,'icon':outcome[0].Icon,
                               'btnClass':outcome[0].BtnClass,'btnName':outcome[1].BtnName,'outcomeName':outcome[0].Name,'actions':[]}
                for action in querysetActions:
                  if action.OutcomeId==outcome[0].id:
                    actionItem = {'toaId':action.id,'type':action.ActionType,'id1':action.ActionId,'name1':action.ActionName,'id2':None,'name2':None,'taskId':action.InitiateTaskId,'actionParams':action.ActionParams}
                    #pdb.set_trace()
                    outcomeItem['actions'].append(actionItem)
                outcomes.append (outcomeItem)
       
        return Response(outcomes)

class TaskOutcomeDefinitionViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskCodesActionsSerializer
    queryset = TaskOutcomesHead.objects.all()
    
    
    def list(self, request,outcomeDefinitionId,lang):
         
        if 'outcomeDefinitionId' in self.kwargs:
           outcome_definition_id=int(self.kwargs['outcomeDefinitionId'])
        else:
            return Response(data='Missing parameter outcomeDefinitionId',status=400) 
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Missing parameter language',status=400)
        
        session = rdbms_cls.Session()
        with session.begin():
          #tohs = TaskOutcomesHead.objects.filter(id=outcomeDefinitionId).prefetch_related(Prefetch('TaskOutcomesHeadLang_taskoutcomeheadid',queryset=TaskOutcomesHeadLang.objects.filter(Lang_id=language)))
          tohs=session.query(TaskOutcomesHeadSa,TaskOutcomesHeadLangSa).join(TaskOutcomesHeadLangSa,and_(TaskOutcomesHeadLangSa.TaskOutcomeHeadId==TaskOutcomesHeadSa.id,TaskOutcomesHeadLangSa.Lang==language)).filter(TaskOutcomesHeadSa.id == outcomeDefinitionId).all()
           
          #serializer = TaskOutcomeDefinitionSerializer(data=tod,context={'user':request.user})
          #serializer.is_valid(raise_exception=True)
          outcomeDef = {}
          for toh in tohs:
            outcomeDef['id'] = toh[0].id
            outcomeDef['OutcomeName'] = toh[0].OutcomeName
            outcomeDef['Active'] = toh[0].Active
            #pdb.set_trace()
            outcomeDef['Name'] = toh[1].Name
          
        return Response(data=outcomeDef,status=200)
      
    def create(self, request,outcomeDefinitionId,lang):
         
        if 'outcomeDefinitionId' in self.kwargs:
           outcome_definition_id=int(self.kwargs['outcomeDefinitionId'])
        else:
            return Response(data='Missing parameter outcomeDefinitionId',status=400) 
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Missing parameter language',status=400)
        #serializer = TaskOutcomeDefinitionSerializer(data=request.data,context={'user':request.user})
        outcomes = request.data
        session = rdbms_cls.Session()
        with session.begin():

          for outcome_item in outcomes:

            if outcome_item['id'] is None:

              outcome =  TaskOutcomesHeadSa(Active=outcome_item['Active'],OutcomeName=outcome_item['OutcomeName'],CreatedBy =user.id, CreatedDateTime=datetime.now(timezone.utc))
              session.add(outcome)
              session.flush()
             
            
              outcomeLang = TaskOutcomesHeadLangSa(Lang=outcome_item['Lang'],Name=outcome_item['Name'],TaskOutcomeHeadId=outcome,CreatedBy= user.id,CreatedDateTime=datetime.now(timezone.utc))
              session.add(outcomeLang)
            elif outcome and not outcomeCreated:
              outcome = session.get(TaskOutcomesHeadSa,outcome_item.get('id',None))
              outcome.Active = outcome_item['Active']
              outcome.OutcomeName = outcome_item['OutcomeName']
              outcome.ChangedBy= user.id
              outcome.ChangedDateTime=datetime.now(timezone.utc)
              
              outcomeLangs = session.query(TaskOutcomesHeadLangSa).filter(TaskOutcomesHeadLang.TaskOutcomeHeadId==outcome.id,TaskOutcomesHeadLang.Lang==outcome_item['Lang'])
              for outcomeLang in outcomeLangs:
                outcomeLang.Name=outcome_item.get('Name','')
                outcomeLang.ChangedBy= user.id
                outcomeLang.ChangedDateTime=datetime.now(timezone.utc)
                
                outcome_item['id'] = outcome.id
                #return code
              else:
                raise Exception('Outcome language record for outcome %s not found',str(code.id))
            else:
              raise Exception('Outcome record %s not found',str(outcome_item['id']))

        return Response(data={'msg':'Outcome definition saved successfuly','data':outcomes},status=200)


class SaveTaskOutcomesViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskCodesActionsSerializer
    queryset = TaskCodesActions.objects.all()
    
    def create(self, request,taskId,lang):
         
        if 'taskId' in self.kwargs:
           task_id=int(self.kwargs['taskId'])
        else:
            return Response(status=404) 
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
            return Response(data='Missing parameter language',status=400)
        
        outcomes = request.data['outcomes']  
        has_outcomes = request.data['hasOutcomes']
        infinite_task= not has_outcomes
       
        
        session = rdbms_cls.Session()
        with session.begin():

          if has_outcomes:
            outcome_id = outcomes['outcomeId']
            if outcome_id!='' and outcome_id is not None:
              #pdb.set_trace()
              outcome_id = int (outcome_id)
            else:
              outcome_id=None
            
            default_outcome_id = outcomes['defaultOutcomeId']
            #pdb.set_trace()
            if default_outcome_id!='' and default_outcome_id is not None:
              #pdb.set_trace()
              default_outcome_id = int (default_outcome_id)
            else:
              default_outcome_id=None
            
            #TaskOutcomesDetail.objects.filter(TaskOutcomeHeadId_id=outcome_id).delete()
            #TaskCodesActions.objects.filter(TaskDefId_id=task_id,OutcomeName='taskoutcomes').delete()
            del1 = session.query(TaskOutcomesDetailSa).filter(TaskOutcomesDetailSa.TaskOutcomeHeadId==outcome_id).delete()
            del2 = session.query(TaskCodesActionsSa).filter(TaskCodesActionsSa.TaskDefId==task_id,TaskCodesActionsSa.OutcomeName=='taskoutcomes').delete()

            #serializer = TaskCodesActionsSerializer(data=outcomes['outcomes'],many=True,context={'task_id': task_id,'user':request.user,'lang':language,'TaskOutcomesHeadId':outcome_id,'outcome':'taskoutcomes','code_name1':'taskoutcomes','code_name2':'taskoutcomes'})
            for outcome in outcomes['outcomes']:
              outcomeId = outcome['id1']
              id1=None
              #now add or update all outcome definitions  
             
              #tods =  TaskOutcomesDetail.objects.filter(id=outcomeId,TaskOutcomeHeadId=outcome_id)
              tods =  session.query(TaskOutcomesDetailSa).filter(TaskOutcomesDetailSa.id==outcomeId,TaskOutcomesDetailSa.TaskOutcomeHeadId==outcome_id).all()
              if len(tods)>0:
                for to in tods:
                  to.Active = True
                  to.Name = outcome['outcomeName']
                  to.BtnClass = outcome['btnClass']
                  to.Icon = outcome['icon']
                  #to.TaskOutcomeHeadId_id  =TaskOutcomesHeadId
                  to.ChangedBy= user.id
                  to.ChangedDateTime=timezone.now()
                
                toLangs = session.query(TaskOutcomesDetailLangSa).filter(TaskOutcomesDetailLangSa.Lang==lang,TaskOutcomesDetailLangSa.TaskOutcomeDetailId==to.id).all()
                #pdb.set_trace()
                for toLang in toLangs:
                  #pdb.set_trace()
                  toLang.DisplayName=outcome['name1']
                  toLang.BtnName=outcome['btnName']
                  toLang.ChangedBy= user.id
                  toLang.ChangedDateTime=datetime.now(timezone.utc)
                  #pdb.set_trace()
                  outcome['id1'] = to.id
                #return code
                else:
                  raise Exception('Outcomes language record for outcome %s not found',str(to.id)) 
              else:
                to = TaskOutcomesDetailSa(Active=True,Name=outcome['outcomeName'],Icon=outcome['icon'],BtnClass=outcome['btnClass'],
                                          TaskOutcomeHeadId=outcome_id,CreatedBy= user.id,CreatedDateTime=datetime.now(timezone.utc))
                session.add(to)
                session.flush()
                if to.id is not None:
                  outcome['id1'] = to.id
                  outcomeId = outcome['id1']
                  toLang = TaskOutcomesDetailLangSa(Lang=lang,DisplayName=outcome['name1'],BtnName=outcome['btnName'],CreatedBy= user.id,
                                                      CreatedDateTime=datetime.now(timezone.utc),TaskOutcomeDetailId=to.id)
                  session.add(toLang)
            #TaskParameters.objects.filter(ResourceDefId_id=task_id).update(TaskOutcomeHeadId=outcome_id,DefaultOutcomeId=default_outcome_id,InfiniteTask=infinite_task,TaskOutcomesSaved=True)
            params = session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==task_id).all()
            for p in params:
              p.TaskOutcomeHeadId=outcome_id
              p.DefaultOutcomeId=default_outcome_id,
              p.InfiniteTask=infinite_task
              p.TaskOutcomesSaved=True
          else:
            #TaskParameters.objects.filter(ResourceDefId_id=task_id).update(InfiniteTask=infinite_task,TaskOutcomesSaved=True)
            params = session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==task_id).all()
            for p in params:
              p.InfiniteTask=infinite_task
              p.TaskOutcomesSaved=True
        return Response(outcomes['outcomes'])

#check if used
class SaveTaskCodesActionsViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskCodesActionsSerializer
    queryset = TaskCodesActions.objects.all()
    
    def create(self, request,taskId,codeHeadName):
         
        if 'taskId' in self.kwargs:
           task_id=int(self.kwargs['taskId'])
        else:
            return Response(data='Missing parameter taskId',status=400) 
        
        if 'codeHeadName' in self.kwargs:
           code_name=self.kwargs['codeHeadName']
        else:
            return Response(data='Missing parameter codeHeadName',status=400) 
        
        
        user = request.user
        code_name1 = code_name
        code_name2 = code_name

        
        codeActions = request.data

        session = rdbms_cls.Session()
        with session.begin():

          for action in codeActions:

            outcome=None
            TaskOutcomesHeadId = None
              
           
            actionsArr = action['actions']
            
            if 'id1' in action:
              id1 = action['id1']
             
            else:
              id1=None
              
            if 'id2' in action:
              id2 = action['id2']
             
            else:
              id2=None
            
            for act in actionsArr:
              
              if 'toaId' in act:
                toa_id=act['toaId']
              else:
                toa_id = None
              
              if 'actionParams' in act:
                action_params=action['actionParams']
              else:
                action_params = None
              
              if 'taskId' in act:
                task_init_id = action['taskId']
              else:
                task_init_id = None
                
              
              if toa_id is None:

                #taskcodesactions,created = TaskCodesActions.objects.get_or_create(id=toa_id,defaults={'TaskDefId_id' : task_id,'CodeId1_id' : id1,
                #                    'CodeName1':code_name1,'CodeId2_id' : id2,'CodeName2':code_name2,'OutcomeId_id':outcomeId,'OutcomeName':outcome,
                #                    'ActionName':action['name1'],'ActionId' : action['id1'],'ActionType'  : action['type'],'InitiateTaskId_id'  : task_init_id,
                #                    'CreatedBy'  : user,'CreatedDateTime'  : timezone.now(),'ActionParams':action_params})

                taskcodesactions = TaskCodesActionsSa(TaskDefId= task_id,CodeId1= id1,CodeName=code_name1,CodeId2=id2,CodeName2=code_name2,OutcomeId=outcomeId,OutcomeName=outcome,
                                    ActionName=act['name1'],ActionId = act['id1'],ActionType=act['type'],InitiateTaskId=task_init_id,CreatedBy= user.id,CreatedDateTime=datetime.now(timezone.utc),ActionParams=action_params)

                session.add(taskcodesactions)
                session.flush()
                act['toaId'] = taskcodesactions.id
             
              else:
                taskcodesactions = session.get(TaskCodesActionsSa,toa_id)
                taskcodesactions.TaskDefId = task_id
                taskcodesactions.CodeName1 = code_name1
                taskcodesactions.CodeName2 = code_name2
                taskcodesactions.CodeId1 = id1
                taskcodesactions.CodeId2 = id2
                taskcodesactions.ActionId = action['id1']
                taskcodesactions.ActionName = action['name1']
                taskcodesactions.ActionType = action['type']
                taskcodesactions.InitiateTaskId = action['taskId']
                taskcodesactions.ActionParams = action_params
                taskcodesactions.ChangedBy = user.id
                taskcodesactions.ChangedDateTime = datetime.now(timezone.utc)
                
                
        #serializer = TaskCodesActionsSerializer(data=request.data,many=True,context={'task_id': task_id,'user':request.user,'code_name1':code_name,'code_name2':code_name})

        
        return Response(codeActions)
#not used
class TaskStatusActionViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskCodesActionsSerializer
    queryset = TaskInitiationInstance.objects.all()
    
    def create(self, request,taskInitiateId,taskExecuteId,action,language,forGroup):
         
        if 'taskInitiateId' in self.kwargs:
           task_initiate_id=int(self.kwargs['taskInitiateId'])
        else:
           return Response(data='Task initiate parameter is missing',status=400)
        
        if 'taskExecuteId' in self.kwargs:
           task_execute_id=int(self.kwargs['taskExecuteId'])
        else:
           return Response(data='Task execute parameter is missing',status=400)
        if 'action' in self.kwargs:
           taskAction=self.kwargs['action']
        else:
           return Response(status=400)
          
        if 'forGroup' in self.kwargs:
           for_group=self.kwargs['forGroup']
        else:
           return Response(status=400)
          
        if 'language' in self.kwargs:
           lang=self.kwargs['language']
        else:
           return Response(data='Language parameter is missing',status=400)
          
        user=request.user
        org_id,language = helper.get_lang_org(user) 
          
        msg,status,tiis,teis = ProcessTaskStatusChange(user,org_id,task_initiate_id,task_execute_id,request,lang,taskAction,for_group)
        
        if (status!=201):
            return Response(data={'msg':msg},status=status)
          
        return Response(data={'msg':msg,'data':{'taskInitiateInstance':tiis.values(),'taskExecuteInstance':teis.values()}},status=201)
#check if used      
class PreInitiateTaskViewSet(viewsets.GenericViewSet):
    #serializer_class = GetResourceDefSerializer
    queryset = ResourceDef
    permission_classes = (AllowAny, )
    #authentication_classes = ()
   
    def create(self, request,taskDefId,prevTaskId,lang):
      
        if 'taskDefId' in self.kwargs:
           resource_id=int(self.kwargs['taskDefId'])
        else:
            return Response(data='Parameter taskDefId is missing',status=404)
        if 'prevTaskId' in self.kwargs:
           prev_task_id=int(self.kwargs['prevTaskId'])
        else:
            prev_task_id = 0
          
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None   
        
        form_type = 'i' 
        formData={}

        session = rdbms_cls.Session()
        with session.begin():
          #tdefs = TaskParameters.objects.filter(ResourceDefId = resource_id)
          tdefs=session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==resource_id).all()
          formDefined = False
          user=request.user
          org_id,language = helper.get_lang_org(user) 
          
          if 'ptp' in request.data:
            prevTaskPackage = request.data['ptp']
            request.session[str(user.id)+str(prev_task_id)] = prevTaskPackage
          else:
            prevTaskPackage=None
          #pdb.set_trace()
          name=''
          formDefinition=[]
        
          for tdef in tdefs:
            if (tdef.AutoInitiate):
              formDefinition = []
              outcome_id = 0
              transation_id = 0
              rval = InitiateTask (resource_id,prev_task_id,outcome_id,0,0,[],[],language,org_id,user,transaction_id,False)
              
              if 'data' in rval:
                return Response(data=rval['data'],status=rval['status'])
           
              elif 'msg' in rval:
                #pdb.set_trace()
                return Response(data=rval['msg'],status=rval['status'])
            else:
              
              formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,0,0,'n',user,org_id,False)
              
              #formData,resourceRecordId = GetDatasetFormData(resource_id,form_type,'w',task_def_id,0,0,'',0,user)
            
              #pdb.set_trace()
              return Response({'resourceDefinition':resourceDefinition,'taskdef': tdefs.values(),'widgets':formDefinition,'form_data':formData})
#not used
class ProcessDatasetForm(viewsets.GenericViewSet):
   #serializer_class = GetResourceDefSerializer
   queryset = ResourceDef
   permission_classes = (AllowAny, )
   #authentication_classes = ()
   
   def create(self, request,datasetRecordId,widgetId,transactionId):
      
      
      if 'datasetRecordId' in self.kwargs:
         dataset_record_id=int(self.kwargs['datasetRecordId'])
      else:
         return Response(data='datasetRecordId parameter missing',status=500)
      if 'widgetId' in self.kwargs:
         widget_id=int(self.kwargs['widgetId'])
      else:
         return Response(data='widgetId parameter missing',status=500)
      if 'transactionId' in self.kwargs:
         transaction_id=int(self.kwargs['transactionId'])
      else:
         return Response(data='widgetId parameter missing',status=500)
      
      form_data = request.data['form'] 
      current_user = request.user
      #check if not public is user authenticated
      session = rdbms_cls.Session()
      with session.begin():
        #tparams = TaskParameters.objects.get(ResourceDefId_id=task_id)
        tparams=session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==resource_id).all()
        
        if len(tparams)==0:
           return Response(data='Task definition for task'+str(resource_id)+' not found',status=500)
        else:
          tparam = tparams[0]
          if not tparam.PublicTask:
            if not current_user.is_authenticated:
              return Response(data='Unauthorised',status=401)
                  
            else:
              org_id,language = helper.get_lang_org(current_user) 
                 
          else:
            language = 'en-GB' #ToDo change this to task parameters
            org_id=tparams.OrganisationId
            current_user=None
        
        # get needed data for widget id
                    
        if len(tis)!=1:
           return Response(data='Erorr -> Invalid request. Task not found for transaction id = '+transaction_id,status=500) 
        
        
        rval,record =  ProcessUpdateResourceWidget(form_data,pElement,form_type,0,task_instance_id,dataset_record_id,0,0,org_id,user_id,lang,False)
                       #ProcessUpdateResourceWidget(widget[1],pe,'e',prev_task_id,task_execute.id,resourceRecordId,parent_dataset_id,parent_dataset_record_id,org_id,user_id,lang,autoExecute)

        if peLang.ResponseMsg is not None and peLang.ResponseMsg!='':
           response_msg = ProcessResponseMsg(record,peLang.ResponseMsg)
        else:
           response_msg=''
      
      return Response(data={'data':rval,'msg':response_msg},status=rval['status'])


class InitiateTaskViewSet(viewsets.GenericViewSet):
   #serializer_class = GetResourceDefSerializer
   queryset = ResourceDef
   permission_classes = (AllowAny, )
   #authentication_classes = ()
   
   def create(self, request,taskDefId,transactionId,datasetId,datasetRecordId):
      
      if 'taskDefId' in self.kwargs:
         resource_id=int(self.kwargs['taskDefId'])
      else:
          return Response(data='taskDefId parameter missing',status=500)
          
      if 'transactionId' in self.kwargs:
         transaction_id=self.kwargs['transactionId']
      else:
          return Response(data='transactionId parameter missing',status=500)
      
      if 'datasetId' in self.kwargs:
         parent_dataset_id=int(self.kwargs['datasetId'])
      else:
         return Response(data='datasetId parameter missing',status=500)
      if 'datasetRecordId' in self.kwargs:
         parent_dataset_record_id=int(self.kwargs['datasetRecordId'])
      else:
         return Response(data='datasetRecordId parameter missing',status=500)
      
      session = rdbms_cls.Session()
      with session.begin():

        #tis = TaskInitiationInstance.objects.filter(TransactionId=transaction_id)
        tis=session.query(TaskInitiationInstanceSa).filter(TaskInitiationInstanceSa.TransactionId==transaction_id).all()

        current_user = request.user
        #check if not public is user authenticated

        #tparams = TaskParameters.objects.get(ResourceDefId_id=resource_id)
        tparams=session.query(TaskParametersSa).filter(TaskParametersSa.ResourceDefId==resource_id).all()

        if len(tparams)==0:
           return Response(data='Task definition for task'+str(resource_id)+' not found',status=500)
        else:
          tparam = tparams[0]
          if not tparam.PublicTask:
              if not current_user.is_authenticated:
                 return Response(data='Unauthorised',status=401)
                  
              else:
                
                org_id,language = helper.get_lang_org(current_user) 
          else:
              lang = tparam.DefaultLang
              language = session.get(LanguageSa,lang)
              org_id=tparam.OrganisationId
              current_user=None
        #tt = TaskTransactions.objects.get(TransactionId=transaction_id)   
        tts = session.query(TaskTransactionsSa).filter(TaskTransactionsSa.TransactionId==transaction_id).all()
        tt=None
        if len(tts)>0:
          tt = tts[0]

        if len(tis)>0:
          
          if tt is not None:
            if tt.Status!='created':
              return Response(data='Erorr -> Task exists with transaction id = '+transaction_id,status=500)
          else:
            return Response(data='Transaction object '+transaction_id+' not found',status=500)
        if tt is not None:
           transParams = json.loads(tt.Params)
           prev_task_id = transParams['previous_task_id']
        else:
           return Response(data='Transaction object '+transaction_id+' not found',status=500)
         
        if 'previous_task_id' in transParams:
           prev_task_id = transParams['previous_task_id']
        else:
           return Response(data='Transaction object '+transaction_id+' is invalid.Previous task not found',status=500)
        
        if 'outcome_id' in transParams:
           outcome_id = transParams['outcome_id']
        else:
           return Response(data='Transaction object '+transaction_id+' is invalid.Outcome not found',status=500)

        form_type='i'
        #create task object
        task_object = task(resource_id,'task','','','','',current_user,language,org_id,{},None)
        if task_object is not None:
          task_object.set_session(session)
          task_object.initialise_initiate_task(form_type,prev_task_id,outcome_id,parent_dataset_id,parent_dataset_record_id,request.data['potentialOwners'],request.data['taskData'])
          rval = task_object.initiate_task ()
        
       
        if rval['status'] ==200:
           if not tparam.PublicTask:
              #ttDel = TaskTransactions.objects.filter(TransactionId=transaction_id,CreatedBy_id=current_user.id).delete()
             
              ttDel = session.query(TaskTransactionsSa).filter(TaskTransactionsSa.TransactionId==transaction_id,TaskTransactionsSa.CreatedBy==current_user.id).delete()
              if ttDel!=1:
                 return Response(data='Error deleting transaction object '+transaction_id,status=500)  
           else:
              #ttDel = TaskTransactions.objects.filter(TransactionId=transaction_id).delete()
              ttDel = session.query(TaskTransactionsSa).filter(TaskTransactionsSa.TransactionId==transaction_id).delete()
              if ttDel!=1:
                 return Response(data='Error deleting transaction object '+transaction_id,status=500)
        
        
        

        if tparam.Parameters is not None:
           if 'RestartTask' in tparam.Parameters:
              if tparam.Parameters['RestartTask']:
                 response,response_status = task_object.start_task(resource_id,0,prev_task_id) #check outcome_id
                 rval['data'].update({'restart_task':{'data':response,'status':response_status}})
             
        if 'data' in rval:
           return Response(data=rval['data'],status=rval['status'])
         
        elif 'msg' in rval:
           #pdb.set_trace()
           return Response(data=rval['msg'],status=rval['status'])
        
class GetTaskExecuteDataViewSet(viewsets.GenericViewSet):
    #serializer_class = TaskSerializer
    queryset = TaskInitiationInstance.objects.all()
    
    def list(self, request,taskInstanceId,lang):
        
        taskInstanceData = 'taskInstanceId='+taskInstanceId
       
        return Response(taskInstanceData)
      
class PreExecuteTaskViewSet(viewsets.GenericViewSet):
    #serializer_class = GetResourceDefSerializer
    queryset = TaskInitiationInstance
    #permission_classes = (AllowAny, )
 
    def list(self, request,taskInitiateId,taskExecuteId,lang): 
        if 'taskInitiateId' in self.kwargs:
           task_initiate_id=int(self.kwargs['taskInitiateId'])
        else:
            return Response(status=404)
        if 'taskExecuteId' in self.kwargs:
           task_execute_id=int(self.kwargs['taskExecuteId'])
        else:
            task_execute_id=0
            #pass
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = 'en-GB'    
        
        form_type = 'e'
       
        user = request.user
        org_id,lang = helper.get_lang_org (user)

        tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition = PreExecuteTask(task_initiate_id,task_execute_id,lang,form_type,user,org_id)
        #pdb.set_trace()
        #ToDo sqlalchemy migration
        #td = TaskParameters.objects.filter(ResourceDefId=resource_id)
        td,lang=session.query(TaskParametersSa,LanguageSa).join(LanguageSa,TaskParametersSa.DefaultLang==LanguageSa.id).filter(TaskParametersSa.ResourceDefId==resource_id).all()

        guid = str(uuid.uuid4())
        #pdb.set_trace()
        transaction_id=str(org_id)+'-'+str(user.id)+'-'+str(resource_id)+'-'+str(task_initiate_id)+'-'+str(task_execute_id)+'-'+guid
    
        if tii_ser is not None and tei_ser is not None:
          return Response({'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': td.values(),'widgets':formDefinitionInJson,'form_data':{},'procType':3,'formType':form_type,'transactionId':transaction_id})
        elif tii_ser is not None:
          return Response({'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': td.values(),'widgets':formDefinitionInJson,'form_data':{},'procType':3,'formType':form_type,'transactionId':transaction_id})
        elif tei_ser is not None:
           return Response({'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': td.values(),'widgets':formDefinitionInJson,'form_data':{},'procType':3,'formType':form_type,'transactionId':transaction_id})
        else:
           return Response(data={'msg':'Error in PreExecuteTask function'},status=400)
     
class ExecuteTaskViewSet(viewsets.GenericViewSet):
    #serializer_class = GetResourceDefSerializer
    queryset = TaskExecutionInstances
    
    def create(self, request,taskExecuteId,language):
      
        if 'taskExecuteId' in self.kwargs:
           task_exe_id=int(self.kwargs['taskExecuteId'])
        else:
           return Response(data='taskExecuteId parameter missing',status=400)
            #pass
       
        if 'language' in self.kwargs:
           lang=self.kwargs['language']
        else:
           return Response(data='Language parameter missing',status=400) 
        
        
        taskData = request.data['taskData']
        #pdb.set_trace()
        outcomeId = request.data['outcome'] 
        user = request.user
        
        org_id,lang = helper.get_lang_org (user)
        version = 0
        #create a task object here
        pif=None
        msg=None
        session = rdbms_cls.Session()
        with session.begin():
          task_obj = task(None,'task','','','','',user,lang,org_id,{},version)
          #get task definition by task execution id
          if task_obj is not None:
            task_obj.set_session(session)
            task_obj.get_definition_by_task_exe(task_exe_id,outcomeId,taskData)

            #execute_task
            pif,msg,ret_status = task_obj.execute_task()
          
          else:
            msg='Filed to create task object'
            ret_status = 500

          
        #pif,msg,ret_status =  ExecuteTask(task_id,0,outcomeId,0,0,taskData,lang,org_id,user,False)
        
        return Response(data={'msg':msg,'ret_value':pif},status=ret_status)
#not used    
class ProcessTaskViewSet(viewsets.GenericViewSet):
    #serializer_class = GetResourceDefSerializer
    queryset = TaskParameters
    
    def create(self, request,itemId,outcomeId,idType,previousTaskId,lang):
      
        if 'itemId' in self.kwargs:
           item_id=int(self.kwargs['itemId'])
        else:
            return Response(data='Parameter itemId is missing',status=404)
        if 'outcomeId' in self.kwargs:
           outcome_id=int(self.kwargs['outcomeId'])
        else:
            return Response(data='Parameter outcomeId is missing',status=404)
        
        if 'idType' in self.kwargs:
           id_type=self.kwargs['idType']
        else:
            return Response(data='Parameter idType is missing',status=404)
        
        if 'previousTaskId' in self.kwargs:
           previous_task_id=int(self.kwargs['previousTaskId'])
        else:
            return Response(data='Previous task id is missing',status=404)
          
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None   
        
        
        user=request.user
        #ToDo sqlalchemy migration
        ueps = UserExtendedProperties.objects.filter(UserId_id=user.id)
        org_id=None
        for uep in ueps:
          org_id = uep.Organisation_id
          
        task_id = None
        menu_object=None
        if id_type=='m':
          menu_id=item_id
          #ToDo sqlalchemy migration
          menu_defs = Menus.objects.filter(id = menu_id)
          menu_count=len(menu_defs)
          #pdb.set_trace()
          if menu_count!=1:
            return Response(data='Invalid definition of menu item. Found '+str(menu_count)+' menu objects',status=404)
         
          
          for menu in menu_defs:
            menu_object = menu
        
          
          #get parameters from menu definition
          response = None
          menu_type= menu_object.MenuType
          if menu_type==2:
            #get task parameters
            task_id = menu_object.TargetTask_id
          
        else:
          task_id = item_id
        
        #check if user is authorised
        #ToDo sqlalchemy migration
        tdefs = TaskParameters.objects.filter(ResourceDefId = task_id)
        
        if len(tdefs)>0:
          task_def = tdefs[0]
        else:
          response =  {'msg':'Task parameters missing for task id='+str(task_id)}
          return Response(data=response,status=500) 
        response = None
        
        is_authorised = IsUserAuthorisedToCreateTask(task_def,previous_task_id,user)
        if not is_authorised:
           response =  {'msg':'You are NOT authorised to create task with id '+str(task_id)}
           return Response(data=response,status=403) 
       
       
        
        #if (menu_object is not None and menu_object.AlwaysNew and id_type=='m') or id_type=='t':
        #pdb.set_trace()
        if not task_def.AutoInitiate:
            
          formDefined = False
          name=''
          formDefinition=[]
          form_type='i'
          formDefinition,resourceDefinition = GetResourceFormDB(task_id,form_type,language,0,0,'n',user,org_id,False)
          response = {'resourceDefinition':resourceDefinition,'taskdef': tdefs.values(),'widgets':formDefinition,'procType':2,'formType':form_type}
          return Response(response)
          
        if task_def.AutoInitiate:
          if task_def.InfiniteTask:
            #this is task that can not be completed
            formDefined = False
            name=''
            formDefinition=[]
            
            #find existing task
            code_status_inprogress = GetStatusCodeByName('InProgress','taskstatuses')

            texe = TaskExecutionInstances.objects.filter(TaskDefId = task_id,ActualOwner = user,Status=code_status_inprogress)
            
            #if exe instance not found search for tasks to claim
            if len(texe)==0:
              #first search for initiate instance
              code_status_ready = GetStatusCodeByName('ready','taskstatuses')
              
              ta_ids=[]
              #ToDo sqlalchemy migration
              tas = TaskAssignments.objects.filter(UserId=user,AssignGroup='potentialOwners',TaskInitiationId__Status_id=code_status_ready).select_related('TaskInitiationId')
              for ta in tas:
                ta_ids.append(ta.TaskInitiationId_id)
              #ToDo sqlalchemy migration
              tinit = TaskInitiationInstance.objects.select_related('TaskDefId','InitiatorId','Status').filter(TaskDefId_id=task_id,id__in=ta_ids,TaskDefId__resourcedeflang__Lang_id=language,Status_id=code_status_ready).order_by('-id')
              #initiate instance does not exist, create new task
              if len(tinit)==0:
                #initiate task
                form_type='e'
                formDefinition = []
                #outcome_id = 0
                
                transaction_id = 0
                #not initiate task or maybe yes   
                rval = InitiateTask (task_id,0,outcome_id,0,0,[],[],language,org_id,user,transaction_id,False)
                #pdb.set_trace()
                if 'data' in rval:
                  response = rval['data']
                  response.update({'procType':3,'formType':form_type})
                  
                  return Response(data=response,status=rval['status'])
     
                elif 'msg' in rval:
                 
                  return Response(data=rval['msg'],status=rval['status'])
                 
              if len(tinit)>0:
                form_type='e'
                task_initiate_id=tinit[0].id
                task_execute_id=0
                tii_ser,tei_ser,resource_id,formDefinitionInJson,resourceDefinition = PreExecuteTask(task_initiate_id,task_execute_id,lang,form_type,user)

                if tii_ser is not None and tei_ser is not None:
                  response = {'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': tdefs.values(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                elif tii_ser is not None:
                  response = {'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': tdefs.values(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                elif tei_ser is not None:
                  response = {'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': tdefs.values(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
                else:
                  response = {'data':{'msg':'Error in PreExecuteTask function --> Start instance'},'status':400}
              
            if len(texe)>0:
              
              form_type='e'
              task_initiate_id=texe[0].TaskInitiateId_id
              task_execute_id=texe[0].id
              tii_ser,tei_ser,resource_id,formDefinitionInJson,name,formDefined = PreExecuteTask(task_initiate_id,task_execute_id,lang,form_type,user,org_id)
        
              if tii_ser is not None and tei_ser is not None:
                response = {'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':tei_ser.data,'taskdef': tdefs.values(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              elif tii_ser is not None:
                response = {'resourceDefinition':resourceDefinition,'taskInitiateInstance':tii_ser.data,'taskExecuteInstance':[],'taskdef': tdefs.values(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              elif tei_ser is not None:
                response = {'resourceDefinition':resourceDefinition,'taskInitiateInstance':[],'taskExecuteInstance':tei_ser.data,'taskdef': tdefs.values(),'widgets':formDefinitionInJson,'procType':3,'formType':form_type}
              else:
                response = {'data':{'msg':'Error in PreExecuteTask function --> Execute instance'},'status':400}
                
          
        #pdb.set_trace()    
        return Response(response)  
       
class StartTaskViewSet(viewsets.GenericViewSet):
    #serializer_class = GetResourceDefSerializer
    queryset = TaskParameters
    
    def list(self, request,taskId,outcomeId,previousTaskId):
      
        
        #pdb.set_trace() #StartTaskViewSet
        if 'taskId' in self.kwargs:
          task_id=int(self.kwargs['taskId'])
        else:
          return Response(data='Parameter taskId is missing',status=500)
        
        if 'outcomeId' in self.kwargs:
          outcome_id=int(self.kwargs['outcomeId'])
        else:
          return Response(data='Outcome id is missing',status=500)
          
        if 'previousTaskId' in self.kwargs:
          previous_task_id=int(self.kwargs['previousTaskId'])
        else:
          return Response(data='Previous task id is missing',status=500)
        
        user=request.user
        
        
        org_id,language = helper.get_lang_org (user) 

        session = rdbms_cls.Session()
        with session.begin():
          #create task object
          task_object = task(task_id,'task','','','','',user,language,org_id,{},None)
          
          if task_object is not None:
            task_object.set_session(session)
            task_object.initialise_initiate_task(None,previous_task_id,outcome_id,0,0,None,None)
            response,response_status = task_object.start_task()
          
          else:
            response = 'Error - failed to create task object'
            response_status = 500

        return Response(data=response,status=response_status)  
                        
class StartPublicTaskViewSet(viewsets.GenericViewSet):
   #serializer_class = GetResourceDefSerializer
   queryset = TaskParameters
   permission_classes = (AllowAny, )
   authentication_classes = ()
    
   def list(self, request,orgId,taskName,taskId,outcomeId,previousTaskId):
      
      if 'orgId' in self.kwargs:
         org_id=int(self.kwargs['orgId'])
      else:
         return Response(data='Parameter organisation id is missing',status=500)
      
      if 'taskName' in self.kwargs:
         task_name=self.kwargs['taskName']
      else:
         return Response(data='Parameter taskName is missing',status=500)
         
      if 'taskId' in self.kwargs:
         task_id=int(self.kwargs['taskId'])
      else:
         return Response(data='Parameter taskId is missing',status=500)
        
      if 'outcomeId' in self.kwargs:
         outcome_id=int(self.kwargs['outcomeId'])
      else:
         return Response(data='Outcome id is missing',status=500)
          
      if 'previousTaskId' in self.kwargs:
         previous_task_id=int(self.kwargs['previousTaskId'])
      else:
         return Response(data='Previous task id is missing',status=500)
        
      user=request.user # this is anonymous  
      #cleanup transactions
      deleteDate =  datetime.now(timezone.utc) - timedelta(minutes=600)

      session = rdbms_cls.Session()
      with session.begin():
        ttCleanup = session.query(TaskTransactionsSa).filter(TaskTransactionsSa.CreatedDateTime<=deleteDate,TaskTransactionsSa.CreatedBy==None).delete()

        task_params = None
        response_status = 200
        #get task parameters
        
        if task_name=='' and task_id==0:
            response_status = 301
            response = {'msg':'Error initiating task --> Invalid task params (task_name,task_id)'}   
        
        elif task_name!='':
            #task_params = TaskParameters.objects.filter(OrganisationId_id=org_id,UniqueName=task_name)
            task_params=session.query(TaskParametersSa,LanguageSa).join(LanguageSa,TaskParametersSa.DefaultLang==LanguageSa.id).filter(TaskParametersSa.OrganisationId==org_id,TaskParametersSa.UniqueName==task_name).all()

        elif task_id>0:
            #task_params = TaskParameters.objects.filter(OrganisationId_id=org_id,ResourceDefId_id=task_id)
            task_params=session.query(TaskParametersSa,LanguageSa).join(LanguageSa,TaskParametersSa.DefaultLang==LanguageSa.id).filter(TaskParametersSa.OrganisationId==org_id,TaskParametersSa.ResourceDefId==task_id).all()
        
        if len(task_params)==0:
           response_status = 301
           response = {'msg':'Error initiating task --> Task params record not found'} 
        
        elif not task_params[0][0].PublicTask:
           response_status = 301
           response = {'msg':'Error initiating task --> Task is not public'} 
        
        if response_status!=200:
           return Response(data=response,status=response_status)   
        
        language=None
        if len(task_params)>0:
          task_id=task_params[0][0].ResourceDefId
          language = task_params[0][1]
      
        else:
          response = 'Error - invalid task parameters (language)'
          response_status = 500
        #response,response_status = StartPublicTask(task_params,outcome_id,previous_task_id,user)

        if response_status==200:
          #create task object
          task_object = task(task_id,'task','','','','',user,language,org_id,{},None,True)
          if task_object is not None:
            task_object.set_session(session)
            task_object.initialise_initiate_task(None,previous_task_id,outcome_id,0,0,None,None,task_name)

            response,response_status = task_object.start_public_task()
          
          else:
            response = 'Error - failed to create task object'
            response_status = 500
          
          if response is not None:
            if 'transactionId' in response:
              transaction_id = response['transactionId']
            else:
              transaction_id = None
            
            if response_status==200 and transaction_id is not None:
              transParams = json.dumps({'task_id':task_id,'previous_task_id':previous_task_id,'outcome_id':outcome_id})
              #tt = TaskTransactions.objects.create(TransactionId=transaction_id,Params=transParams,Status='created',CreatedDateTime=timezone.now())
              tt = TaskTransactionsSa(TransactionId=transaction_id,Params=transParams,Status='created',CreatedDateTime=datetime.now(timezone.utc))
              session.add(tt)
              if not tt:
                 response_status = 500
                 response = {'msg':'Error initiating task --> Error creating transaction record '+str(response_status)}
            else:
              response_status = 500
              response = {'msg':'Error initiating task --> transation_id is null '+str(response_status)}
          else:
            response_status = 500
            response = {'msg':'Error initiating task --> response from StartTask is null '+str(response_status)}
          #pdb.set_trace()
      return Response(data=response,status=response_status)  
 
#not used                              
class GetInitiatedTasksViewSet(viewsets.ModelViewSet):
    
   pagination_class = StandardResultsSetPagination
   
   def get_serializer_class(self):
        #pdb.set_trace()
        if 'type' in self.kwargs:
          ltype=self.kwargs['type']
        else:
          ltype = None
          
         #pdb.set_trace()
        if ltype == 'claim':
          serializer_class = TaskAssignmentsSerializer
        else:  
          serializer_class = ClaimedTasksSerializer
        
        return serializer_class
        
   def get_queryset(self):
        #pdb.set_trace()
        if 'type' in self.kwargs:
          ltype=self.kwargs['type']
        else:
          ltype = None
          
        if 'filter' in self.kwargs:
          lfilter=int(self.kwargs['filter'])
        else:
          lfilter = 0
          
        if 'lang' in self.kwargs:
          language=self.kwargs['lang']
        else:
          language = None
       
        current_user = self.request.user
        
        code_status_ready = GetStatusCodeByName('ready','taskstatuses')
        code_status_completed = GetStatusCodeByName('completed','taskstatuses')  
        if ltype=='execute':
          if lfilter==0:
               #ToDo sqlalchemy migration
               queryset = TaskExecutionInstances.objects.filter(TaskDefId__resourcedeflang__Lang_id=language).order_by('-id')#select_related('resourcedef','ActualOwner','CreatedBy','Status').filter(TaskDefId__resourcedeflang__Lang__contains=language).order_by('-id')
              
               #pdb.set_trace()
          else:
               #queryset = TaskExecutionInstances.objects.filter(ActualOwner=current_user,TaskDefId__resourcedeflang__Lang__contains=language).exclude(Status_id=code_status_completed).order_by('-id')#.select_related('resourcedef','ActualOwner','CreatedBy','Status').filter(ActualOwner=current_user,TaskDefId__resourcedeflang__Lang__contains=language).exclude(Status_id=code_status_completed).order_by('-id')
               #ToDo sqlalchemy migration
               queryset = ResourceDef.objects.filter(resourcedeflang__Lang_id=language).prefetch_related('resourcedeflang_set','taskexecutioninstances_set').exclude(taskexecutioninstances__Status_id=code_status_completed).order_by('-taskexecutioninstances__id')
               #queryset = TaskExecutionInstances.objects.all()
        else:
          #ovo treba promjeniti u TaskAssignments
          if lfilter==0:
              #ToDo sqlalchemy migration
              queryset = TaskInitiationInstance.objects.select_related('TaskDefId','InitiatorId','Status').filter(TaskDefId__resourcedeflang__Lang_id=language,Status_id=code_status_ready).prefetch_related(Prefetch('TaskAssignments_taskinitiationid',
                                          queryset=TaskAssignments.objects.filter(UserId=current_user,Claimed=False))).order_by('-id')
          else:
              ta_ids=[]
              #pdb.set_trace() 
              queryset = TaskAssignments.objects.filter(UserId=current_user,AssignGroup='potentialOwners',Claimed=False,TaskInitiationId__Status_id=code_status_ready).select_related('TaskInitiationId').order_by('-TaskInitiationId_id')
              #pdb.set_trace()
              #for ta in tas:
                #ta_ids.append(ta.TaskInitiationId_id)
              #queryset = TaskInitiationInstance.objects.select_related('TaskDefId','InitiatorId','Status').filter(id__in=ta_ids,TaskDefId__resourcedeflang__Lang__contains=language).prefetch_related(Prefetch('TaskAssignments_taskinitiationid',
                                          #queryset=TaskAssignments.objects.filter(UserId=current_user,Claimed=False))).order_by('-id')
        
        return queryset
      
class GetInitiatedTasksFilteredViewSet(viewsets.GenericViewSet):
      
   pagination_class = StandardResultsSetPagination
   queryset=ResourceDef.objects.all()
   #serializer_class=MutantModelSerializer
   
       
   def create(self, request,searchType,filter,lang):
        
      if 'searchType' in self.kwargs:
        ltype=self.kwargs['searchType']
      else:
        ltype = None
        
    
      if lang is not None:
         language=lang
      else:
         language = None

      datasetRecords = []
      selectedRecords = []
      relatedDict={}
      filter = request.data

     
      
      code_status_ready = GetStatusCodeByName('ready','taskstatuses')
      code_status_completed = GetStatusCodeByName('completed','taskstatuses')
      code_task_type_onepgroup = GetStatusCodeByName('oneinstancepergroup','_TASK_TYPE_')
      current_user = request.user
 
      filtered_data={}
      task_def_ids=[]
      task_status_ids=[]
      #pdb.set_trace() 
      if 'taskDefIds' in filter:
        if type(filter['taskDefIds'])==list:
          
          if len(filter['taskDefIds'])>0:
            for td in filter['taskDefIds']:
              if 'id' in td:
                if td['id'] is not None:
                  task_def_ids.append(td['id'])
        if len(task_def_ids)>0:

          if (ltype=='execute'):
            filtered_data['TaskDefId_id__in'] = task_def_ids
          else:
            filtered_data['TaskInitiationId__TaskDefId_id__in'] = task_def_ids
                
      if 'taskStatus' in filter:
        if type(filter['taskStatus'])==list:
          if len(filter['taskStatus'])>0:
            for ts in filter['taskStatus']:
              if 'id' in ts:
                if ts['id'] is not None:
                  task_status_ids.append(ts['id'])
        if len(task_status_ids)>0:
          
          if (ltype=='execute'):
            filtered_data['Status_id__in'] = task_status_ids
          else:
            filtered_data['TaskInitiationId__Status_id__in'] = task_status_ids
          
         
     
      queryset=None          
      if ltype=='execute':
            
             if 'currentUser' in filter:
              
                if filter['currentUser']:
        
                   #filtered_data['taskexecutioninstances__ActualOwner'] = current_user
                   filtered_data['ActualOwner'] = current_user
        
             #queryset = TaskExecutionInstances.objects.prefetch_related(Prefetch('TaskDefId__resourcedeflang_set',queryset=ResourceDefLang.objects.filter(Lang_id=language)),Prefetch('Status__CodesDetailLang',queryset=CodesDetailLang.objects.filter(Lang_id=language))).select_related('ActualOwner','CreatedBy').filter(**filtered_data).order_by('-id')
             queryset=session.query(TaskExecutionInstancesSa,ResourceDefLangSa,codesdetail).join(ResourceDefLangSa,and_(TaskExecutionInstancesSa.TaskDefId == ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang == language)).outerjoin(codesdetail,and_(ResourceDefLangSa.Status ==codesdetail.id,codesdetail.Lang==language)).filter(**filtered_data).order_by(desc(TaskExecutionInstancesSa.id)).all()  
           
      elif ltype=='claim':
        
            ta_ids=[]
            #queryset = TaskAssignments.objects.filter(UserId=current_user,AssignGroup='potentialOwners',Claimed=False).select_related('TaskInitiationId').filter(**filtered_data).order_by('-TaskInitiationId_id')
            queryset=session.query(TaskAssignmentsSa,TaskInitiationInstanceSa).join(TaskInitiationInstanceSa,TaskAssignmentsSa.TaskInitiationId==TaskInitiationInstanceSa.id).filter(TaskAssignmentsSa.UserId == current_user.id,TaskAssignmentsSa.AssignGroup=='potentialOwners',TaskAssignmentsSa.Claimed == False).filter(**filtered_data).order_by(desc(TaskAssignmentsSa.TaskInitiationId)).all()  
            p_assign=None
            #ErrorLog.objects.create(Error1={'filtered_data':filtered_data},CreatedDateTime=timezone.now())
            for assign in queryset:
                
              if assign[1].TaskType!=code_task_type_onepgroup:
                #pdb.set_trace()
                if p_assign==assign[1].id:
                  assign.delete()
                else:
                  assign.ForGroup = None
                     
              p_assign=assign[1].id               
            #tas = TaskAssignments.objects.filter(UserId=current_user,AssignGroup='potentialOwners',TaskInitiationId__Status_id=code_status_ready,Claimed=False).select_related('TaskInitiationId')
            #for ta in tas:
              #ta_ids.append(ta.TaskInitiationId_id)
            #queryset = TaskInitiationInstance.objects.select_related('TaskDefId','InitiatorId','Status').filter(id__in=ta_ids,TaskDefId__resourcedeflang__Lang__contains=language).prefetch_related(Prefetch('TaskAssignments_taskinitiationid',
                                        #queryset=TaskAssignments.objects.filter(UserId=current_user,Claimed=False))).filter(**filtered_data).order_by('-id')
      
      #pdb.set_trace()
      if queryset is None:
         return Response(data='Invalid parameter - > search type',status=500)
         
      page = self.paginate_queryset(queryset)
  
      if page is not None:
         result = page
      else:
         result=queryset
    
      if ltype=='execute':
         serializer = ClaimedTasksSerializer(result,many=True,context={'request': request})
      elif ltype=='claim':
         serializer = TaskAssignmentsSerializer(result,many=True,context={'request': request})
  
      if page is not None:
         #pdb.set_trace()
         return self.get_paginated_response(serializer.data)
      else:
         return Response(serializer.data)
   
         
       
        
           
class GetFormWidgetsViewSet(viewsets.GenericViewSet):

   serializer_class = FormWidgetSerializer
 
   def list(self, request,resourceId,taskId,presentationId,srcType,lang,version):
           
        if 'srcType' in self.kwargs:
          src_type=int(self.kwargs['srcType'])
        else:
          src_type = None
        
        if 'presentationId' in self.kwargs:
          presentation_id=self.kwargs['presentationId']
        else:
          presentation_id = None
          
        if 'taskId' in self.kwargs:
          task_id=int(self.kwargs['taskId'])
        else:
          task_id = None  
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
        else:
          resource_id = None
          
        if 'lang' in self.kwargs:
          lang=self.kwargs['lang']
        else:
          lang = None

        if 'version' in self.kwargs:
          ver=int(self.kwargs['version'])
        else:
          ver = None
        
        datasetAll_ids = []   
        dataset1_ids  = []
        dataset2_ids = []
                       
        #searching all elements where dataset in dataset 
        if resource_id is not None:
          
          session = rdbms_cls.Session()
          with session.begin():
            #rlinks= ResourceLinks.objects.filter(LinkType='FK', Resource1=resource_id)
            rlinks=session.query(ResourceLinksSa).filter(ResourceLinksSa.LinkType == 'FK',ResourceLinksSa.Resource1==resource_id).all()  
            for link in rlinks:
              datasetAll_ids.append(link.Resource2_id)
              dataset1_ids.append(link.Resource2_id)
  

            #rlinks=ResourceLinks.objects.filter(LinkType='FK', Resource2=resource_id)
            rlinks=session.query(ResourceLinksSa).filter(ResourceLinksSa.LinkType == 'FK',ResourceLinksSa.Resource2==resource_id).all()   
            for link in rlinks:
              datasetAll_ids.append(link.Resource1_id)
              dataset2_ids.append(link.Resource1_id)
        
           
            datasetAll_ids.append(resource_id)

            #pe = PresentationElements.objects.filter(ResourceDefId_id=task_id,Related_id__in=datasetAll_ids,Status='Active',VersionFrom__lte=ver,VersionTo__gte=ver).exclude(UuidRef=presentation_id)
            pe=session.query(PresentationElementsSa).filter(PresentationElementsSa.ResourceDefId == task_id,PresentationElementsSa.Related.in_(datasetAll_ids),PresentationElementsSa.Status=='Active',PresentationElementsSa.VersionFrom<=ver,PresentationElementsSa.VersionTo>=ver,PresentationElementsSa.UuidRef!=presentation_id).all()
                
            #pdb.set_trace()
            if pe is None:
              return pe
            else:
              initiate_widget_ids = []
              execute_widget_ids = []
              src_type_item=0
              pe_previous = None
              for pe_item in pe:
                  
                #rdls = ResourceDefLang.objects.filter(ResourceDefId_id=pe_item.ResourceDefId_id,Lang_id=lang).select_related()
                rdls=session.query(ResourceDefLangSa).filter(ResourceDefLangSa.ResourceDefId == pe_item.ResourceDefId,ResourceDefLangSa.Lang==lang).all()
                
                for rdl in rdls:
                      
                  if (pe_item.FormType=='e'):
                    execute_widget_ids.append({'id':len(execute_widget_ids),'relatedId':pe_item.Related,'parentElementId':pe_item.UuidRef,'form_type': pe_item.FormType,'task_def_id':pe_item.ResourceDefId,'task_name':rdl.Name,'type':pe_item.ElementType,'src_type_id':src_type_item})
                  else:
                    initiate_widget_ids.append({'id':len(initiate_widget_ids),'relatedId':pe_item.Related,'parentElementId':pe_item.UuidRef,'form_type': pe_item.FormType,'task_def_id':pe_item.ResourceDefId,'task_name':rdl.Name,'type':pe_item.ElementType,'src_type_id':src_type_item})
                  
              #pe_previous = pe_item
        else:
          return None
        #pdb.set_trace()
        return Response({'initiate_widgets':initiate_widget_ids,'execute_widgets':execute_widget_ids})

#check if used   
class GetWidgetDependenciesViewSet(viewsets.GenericViewSet):

   serializer_class = FormWidgetSerializer
   permission_classes = (AllowAny, )
   #authentication_classes = ()
   
   def list(self, request,taskDefId,presentationId,formType):
           
        if 'taskDefId' in self.kwargs:
          task_def_id=int(self.kwargs['taskDefId'])
        else:
          task_def_id = None
        
        if 'presentationId' in self.kwargs:
          presentation_id=int(self.kwargs['presentationId'])
        else:
          presentation_id = None
        if 'formType' in self.kwargs:
          form_type=self.kwargs['formType']
        else:
          form_type = None
        
        wds = None
        tItem=[]
        oldWd=None
        #wd=None
        DEI=[]
        session = rdbms_cls.Session()
        with session.begin():
         
          if task_def_id is not None:
              
              #wds = TaskDefWidgetDependencies.objects.filter(TaskDefId_id=task_def_id,PresentationId_id=presentation_id,FormType=form_type,InterfacePresentationId__Status='Active').select_related('InterfacePresentationId').order_by('InterfacePresentationId') 
              wds=session.query(TaskDefWidgetDependenciesSa).filter(TaskDefWidgetDependenciesSa.TaskDefId == task_def_id,TaskDefWidgetDependenciesSa.PresentationId==presentation_id,TaskDefWidgetDependenciesSa.FormType==form_type).all()
              
          if wds is not None and len(wds)>0:
              for wd in wds:
                  if oldWd is None or oldWd.InterfacePresentationId_id==wd.InterfacePresentationId_id:
                      if wd.Params is not None:
                        DEI.append({'name':wd.FieldName,'value':wd.Status,'params':wd.Params})
                      else:
                        DEI.append({'name':wd.FieldName,'value':wd.Status})
                  else:
                      tItem.append ({'name':oldWd.InterfacePresentationId.ElementType,'widgetId':oldWd.InterfacePresentationId_id,'datasetId':oldWd.InterfacePresentationId.Related_id,'taskId':oldWd.TaskDefId_id,'DEI':DEI})    
                      DEI=[]
                      #pdb.set_trace()
                      if wd.Params is not None:
                        DEI.append({'name':wd.FieldName,'value':wd.Status,'params':wd.Params})
                      else:
                        DEI.append({'name':wd.FieldName,'value':wd.Status})
                        
                  oldWd=wd
              if len(wds)>0:  
                tItem.append ({'name':wd.InterfacePresentationId.ElementType,'widgetId':wd.InterfacePresentationId_id,'datasetId':wd.InterfacePresentationId.Related_id,'taskId':wd.TaskDefId_id,'DEI':DEI})    

          else:
           return Response(data=tItem,status=200)
        
        #pdb.set_trace()
        return Response(data=tItem,status=200)

class SaveTaskFormDataViewSet(viewsets.GenericViewSet):

   #serializer_class = FormWidgetSerializer
   queryset = TaskExecutionInstances.objects.all()
   
   def create(self, request,taskExecuteId):
      
        if 'taskExecuteId' in self.kwargs:
           task_execute_id=int(self.kwargs['taskExecuteId'])
        else:
            return Response(data='taskExecuteId parameter missing',status=400)
            #pass
        
        
        formData = request.data
        current_user = request.user
        #tei = TaskExecutionInstances.objects.filter(id=task_execute_id).update(FormData = formData,ChangedDateTime=timezone.now(),ChangedBy=current_user)
        session = rdbms_cls.Session()
        with session.begin():

          #rdl = ResourceDefLang.objects.filter(ResourceDefId=resource_id,Lang_id=lang)
          tei=session.query(TaskExecutionInstancesSa).filter(TaskExecutionInstancesSa.id == task_execute_id).all()
          #pdb.set_trace()
          if len(tei) == 1:
            tei.FormData = formData
            tei.ChangedDateTime=datetime.now(timezone.utc)
            tei.ChangedBy = current_user.id
            return Response (status=200)
          else:
            return Response(data='Form not saved',status=400)
        
class SaveTaskBtnsViewSet(viewsets.GenericViewSet):

   #serializer_class = FormWidgetSerializer
   queryset = ResourceDefLang.objects.all()
   
   def create(self, request,resourceId,lang):
      
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           return Response(data='resourceId parameter missing',status=400)
        
        if 'lang' in self.kwargs:
           lang=self.kwargs['lang']
        else:
           return Response(data='language parameter missing',status=400)
            #pass
        
        btnsDef = request.data
        current_user = request.user
        session = rdbms_cls.Session()
        with session.begin():

          #rdl = ResourceDefLang.objects.filter(ResourceDefId=resource_id,Lang_id=lang)
          rdl=session.query(ResourceDefLangSa).filter(ResourceDefLangSa.ResourceDefId == resource_id,ResourceDefLangSa.Lang==lang).all()
          #update(InitFormBtnDef = btnsDef,ChangedDateTime=timezone.now(),ChangedBy=current_user)
          #pdb.set_trace()
          #if rdl == 1:
          if len(rdl)==1:
            rdl[0].InitFormBtnDef = btnsDef
            rdl[0].ChangedDateTime=datetime.now(timezone.utc)
            rdl[0].ChangedBy=current_user.id
            
            return Response (data='Task buttons saved successfuly',status=200)
          else:
            return Response(data='Task buttons NOT saved!',status=500)
        
            
class SaveTaskToResourceViewSet(viewsets.GenericViewSet):

   #serializer_class = FormWidgetSerializer
   queryset = TaskExecutionInstanceToResource
   
   def create(self, request,taskExecuteId,datasetDefId,datasetRecordId,presentationId):
  
        user_id = request.user
        session = rdbms_cls.Session()
        with session.begin():
          #teitr=TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id = taskExecuteId,PresentationElementId_id = presentationId)
          teitr=session.query(TaskExecutionInstanceToResourceSa).filter(TaskExecutionInstanceToResourceSa.TaskExecuteId == taskExecuteId,TaskExecutionInstanceToResourceSa.PresentationElementId==presentationId).all()
          if len(teitr)==0:
            #teitr_rec=TaskExecutionInstanceToResource.objects.create( ResourceDefId_id = datasetDefId, TaskExecuteId_id = taskExecuteId, ResourceId = datasetRecordId,
             #                                       CreatedBy = user_id,CreatedDateTime=timezone.now(), PresentationElementId_id =presentationId)
            teitr_rec = TaskExecutionInstanceToResourceSa(ResourceDefId = datasetDefId, TaskExecuteId = taskExecuteId, ResourceId = datasetRecordId,CreatedBy = user_id.id,CreatedDateTime=datetime.now(timezone.utc), PresentationElementId =presentationId)
            session.add(teitr_rec)
          else:
            for item in teitr:
              item.ChangedBy = user_id.id
              item.ChangedDateTime = datetime.now(timezone.utc)
              item.ResourceDefId = datasetDefId
              item.ResourceId = datasetRecordId
        return Response (status=200)

class GetTaskViewResourceViewSet(viewsets.ModelViewSet):

    def list(self, request,widgetId,taskDefId,taskInstanceId,formType,datasetDefId,datasetRecordId):
        
        if 'widgetId' in self.kwargs:
           widget_id=int(self.kwargs['widgetId'])
        else:
           widget_id = None
        if 'taskInstanceId' in self.kwargs:
          task_instance_id=int(self.kwargs['taskInstanceId'])
        else:
          task_instance_id = None

        if 'taskDefId' in self.kwargs:
          task_def_id=int(self.kwargs['taskDefId'])
        else:
          task_def_id = None

        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = None

        if 'datasetDefId' in self.kwargs:
           dataset_def_id=int(self.kwargs['datasetDefId'])
        else:
           dataset_def_id = None

        if 'datasetRecordId' in self.kwargs:
           dataset_record_id=int(self.kwargs['datasetRecordId'])
        else:
           dataset_record_id = None
        
        user = request.user   
        
        org_id,language = helper.get_lang_org (user) 
        session = rdbms_cls.Session()
        rval=None
        with session.begin():
          task_object = task(task_def_id,'task','','','','',user,language,org_id,{})
          task_object.set_session(session)
          view_resource_fd = {'PresentationId':widget_id,'id':widget_id}
          view_resource = ktTaskViewResource(view_resource_fd,task_object,None,None)
          rval = view_resource.pre_process_widget(widget_id,task_instance_id,form_type,dataset_def_id,dataset_record_id)
        response = Response(data=rval,status=200)
        return response

class GetTaskUpdateResourceViewSet(viewsets.ModelViewSet):

    def list(self, request,widgetId,taskDefId,taskInstanceId,formType,datasetDefId,datasetRecordId):
        
        if 'widgetId' in self.kwargs:
           widget_id=int(self.kwargs['widgetId'])
        else:
           widget_id = None
        if 'taskInstanceId' in self.kwargs:
          task_instance_id=int(self.kwargs['taskInstanceId'])
        else:
          task_instance_id = None

        if 'taskDefId' in self.kwargs:
          task_def_id=int(self.kwargs['taskDefId'])
        else:
          task_def_id = None

        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = None

        if 'datasetDefId' in self.kwargs:
           dataset_def_id=int(self.kwargs['datasetDefId'])
        else:
           dataset_def_id = None

        if 'datasetRecordId' in self.kwargs:
           dataset_record_id=int(self.kwargs['datasetRecordId'])
        else:
           dataset_record_id = None
        
        user = request.user   
        
        org_id,language = helper.get_lang_org (user) 
        session = rdbms_cls.Session()
        rval=None
        with session.begin():
          task_object = task(task_def_id,'task','','','','',user,language,org_id,{})
          task_object.set_session(session)
          update_resource_fd = {'PresentationId':widget_id,'id':widget_id}
          update_resource = ktTaskUpdateResource(update_resource_fd,task_object,None,None)
        
          rval = update_resource.pre_process_widget(widget_id,task_instance_id,form_type,dataset_def_id,dataset_record_id)
        
        response = Response(data=rval,status=200)
        return response

class SaveGridStateViewSet(viewsets.GenericViewSet):

   #serializer_class = FormWidgetSerializer
   queryset =  PresentationElementsLang
   
   def create(self, request,taskDefId,presentationId,lang):
        
        if 'presentationId' in self.kwargs:
          presentation_id=int(self.kwargs['presentationId'])
        else:
           return Response(data='presentationId parameter missing',status=400)
        if 'taskDefId' in self.kwargs:
          task_def_id=int(self.kwargs['taskDefId'])
        else:
          return Response(data='Task id parameter missing',status=400)
        if 'lang' in self.kwargs:
          language=self.kwargs['lang']
        else:
          return Response(data='Lang parameter missing',status=400)
        
        user_id = request.user
        session = rdbms_cls.Session()
        with session.begin():
          #peLang=PresentationElementsLang.objects.filter(PresentationElementsId_id = presentation_id,Lang_id=language)
          peLang=Session.query(PresentationElementsLangSa).filter(PresentationElementsLangSa.PresentationElementsId == presentation_id,PresentationElementsLangSa.Lang==language).all()
          
          state = request.data
          #pdb.set_trace()
          for pel in peLang:
            pel.GridState =  state
          if pel is not None:
            return Response (data={'msg':'Grid state saved'},status=200)
          else:
            return Response (data='Grid state save error --> element not found',status=500)