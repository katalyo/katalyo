from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser,FileUploadParser,FormParser, MultiPartParser
import json
from django.forms.models import model_to_dict
import os
import sys
from sqlalchemy import func, update
import pandas
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
from django.utils.decorators import method_decorator
from rest_framework.permissions import AllowAny
#import katalyonode.functions.taskprocessing
from itertools import chain
from django.core import serializers
from django.contrib.auth import login,logout,authenticate
from katalyonode.settings import FILES_DIR
from django.template import RequestContext
from django.db.models import Prefetch
from django.db import transaction
from django.contrib.auth.models import User, Group
from rest_framework import viewsets,permissions,authentication
from django.db.models.functions import Concat
from django.db.models import Value
from django.contrib.auth.models import AnonymousUser
#from django.core.exceptions import ObjectDoesNotExist
#import katalyonode.serializers.serializers 
#from katalyonode.functions.datasets import get_table_name, mutant_get_data_filtered,mutant_get_data_filtered_sr,get_columns_def

from katalyonode.serializers.serializersresources import ResourceDefSerializer,ResourceDefLangSerializer,GetTransformDetailSerializer
from katalyonode.serializers.serializersresources import ResourceDefByLangSerializer,ResourceDefAllSerializer,SaveDatasetTransformDetailSerializer
from katalyonode.serializers.serializersresources import SaveResourceDefLangSerializer,GetResourceDefSerializer,ResourceDefSubsSerializer,ResourceSubscriptionsSerializer
from katalyonode.serializers.serializersresources import GetResourceFormSerializer,SaveResourceFormSerializer,GetResourceDataSerializer,MutantModelSerializer,RelatedResourceSerializer
from katalyonode.serializers.serializersresources import MutantModelRelatedSerializer,GetDatasetMappingHeadSerializer,GetResourceFieldsSerializer,GetDatasetTransformHeadSerializer,SaveDatasetMappingDetailSerializer
from katalyonode.serializers.serializerssettings import ResourceParamsSerializer
#import katalyonode.models
from wsgiref.util import FileWrapper
from django.utils import timezone
from django.db.models import Max
from django.forms.models import model_to_dict
from katalyonode.models.models import ResourceDef,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang,PresentationElementsTasks
from katalyonode.models.models import ResourceModelDefinition,FilesTable,TaskDef,TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskInitiationInstance,TaskInitiationInstanceToResource,FilesToResource,TaskCodesActions
from katalyonode.models.models import TaskExecutionInstances,AuditTrailTaskStatus,TaskAssignments,TaskExecutionInstanceToResource,TaskDefWidgetDependencies,DatasetMappingHead,DatasetMappingHeadLang,Language,PublishedResources
from katalyonode.models.models import DatasetTransformHead,DatasetTransformHeadLang,DatasetMappingDetail,DatasetTransformDetail,UserExtendedProperties,Pages,ErrorLog,ResourceSubscriptions,ResourceParams,ResourceLinks,ResourceWidgetLinks,WidgetDefinition
from katalyonode.functions.resources.datasetdef import dataset
from katalyonode.functions.resources.filedef import file
#from katalyonode.functions.datasets import mutant_get_data_filtered,mutant_get_data_sa,mutant_get_data_filtered_in,mutant_get_m2m_data_in,mutant_update_data,mutant_insert_data_m2m_v2
#from katalyonode.functions.datasets import mutant_get_m2m_data_full,mutant_delete_data,get_columns_def,get_file_fields
#create_alchemy_table,create_mutant_table,define_alchemy_table_attr,get_alchemy_table_class,deploy_alchemy_table

#from katalyonode.functions.formprocessing import GetResourceFormDB,ProcessInitiateForm,ExtendResourceFormJSON
#from katalyonode.functions.formprocessing import ProcessExecuteForm,PreProcessResourceWidget,ExtractFormDataSearch,ProcessResourceData,ProcessResourceDataSearch,ProcessWidgetData
#from katalyonode.functions.formprocessing import ApplyMappingPackage,GetDatasetFormData
#from katalyonode.functions.resourceutilityfn import IsUserResourceAdmin
#from katalyonode.functions.taskprocessing import ExecuteTask,ProcessTaskAssignments,ProcessTaskParameters,ProcessPreviousTask,ProcessTaskStatusChange,GetStatusCodeByName
import pdb
from django.template.loader import render_to_string
#from weasyprint import HTML
import tempfile
import importlib
from datetime import datetime,timezone
from django.core.validators import ValidationError
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.resources.base.resourcebase import ResourceBaseClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.rdbms.alchemymodels import  ResourceModelDefinitionSa, AdminUsers,AdminGroups,ResourceLinksSa, PresentationElementsSa, LanguageSa, ResourceWidgetLinksSa, WidgetDefinitionSa, DatasetMappingHeadSa, DatasetMappingHeadLangSa, ResourceDefSa,ResourceDefLangSa,ResourceSubscriptionsSa,ResourceParamsSa,OrganisationsSa,PagesSa,PresentationElementsLangSa
from sqlalchemy import and_,or_
import pandas


rdbms_cls = RdbmsClass()
helper = ResourceHelperClass()
 
class Parser(object):

  sep = ';'

  def filter_query(self, model_class):
    #pdb.set_trace()
    #model_class = self._get_model_class(query) # returns the query's Model
    #raw_filters = request.args.getlist('filter')
    raw_filters = ["Active;__eq__;true", "ChangedBy;__eq__;10"]
    for raw in raw_filters:
      try:
        key, op, value = raw.split(self.sep, 3)
      except ValueError:
        raise APIError(400, 'Invalid filter: %s' % raw)
      column = getattr(model_class, key, None)
      if not column:
        raise APIError(400, 'Invalid filter column: %s' % key)
      if op == 'in':
        filt = column.in_(value.split(','))
      else:
        try:
          attr = filter(
            lambda e: hasattr(column, e % op),
            ['%s', '%s_', '__%s__']
          )[0] % op
        except IndexError:
          raise APIError(400, 'Invalid filter operator: %s' % op)
        if value == 'null':
          value = None
        filt = getattr(column, attr)(value)
      query = query.filter(filt)
    return query
    
    
# Create your views here.

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 10000
    
    def set_page_size(ps):
      page_size = ps
    
    def get_paginated_response(self, data):
   
      return Response({
        "count": self.page.paginator.count,
        "countItemsOnPage": self.page_size,
        'next': self.get_next_link(),
        'previous': self.get_previous_link(),
        'results': data
      })

def ProcessWidgetExecute(params):
    
    if 'paginate' in params:
        paginate = params['paginate']
    else:
        paginate = False
    
    resource_id = params['resource_id']
    presentation_id = params['presentation_id']
    form_type = params['form_type']
    language = params['language']
    task_def_id = params['task_def_id']
    user = params['user']
    
    
    org_id,language = helper.get_lang_org (user) 
      
   
    #get presentation elements
    #pes = PresentationElements.objects.filter(id=presentation_id)  
    pe = session.query(PresentationElementsSa).filter( PresentationElementsSa.id == presentation_id).first()
 

    p_element = None
    #for pe in pes:
    p_element = pe
    
    if p_element is None:
        return Response(data='Widget definition number '+(presentation_id)+' is not found.',status=500)
    #get form
    #check if something from here needs to come from the client
    
    override_type = "f"
    parent_initiate_id = 0
    src_task_instance_id=0
    parent_initiate_id = presentation_id
    parent_id= 0
    populate_type=p_element.PopulateType
    resource_record_id = 0
    formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
    #get data
    formData,recordId = GetDatasetFormData(resource_id,form_type,override_type,task_def_id,src_task_instance_id,parent_id,populate_type,resource_record_id,org_id,user)
    
    formDefinitionCard=None
    #if presentation-type=card get card layout
    if p_element.Parameters is not None:
        if p_element.Parameters['PresentationType']=="card":
            override_type = "c"
            formDefinitionCard,resourceDefinitionCard = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
            
    #process from
    results = None
    selectedRecords=[]
    queryset=[]
    filter = None
    columns = get_columns_def(resource_id,task_def_id,form_type,org_id,language)
    #if auto search is checked then get data
    if p_element.AutoSearch:
        
        rData,f,select_related,parameters = ExtractFormDataSearch(formData,formDefinition,user)
        filter = f
                
        resourceData,m2mData = ProcessResourceDataSearch(rData,resource_id,org_id)
        
        queryset =  mutant_get_data_filtered_sr(resource_id,org_id,resourceData,select_related, parameters={})
        if not paginate:
            selectedRecords = GetDatasetDataFilteredPaginatedFast(filter,user,resource_id,org_id,None,queryset,language,None)
    
        ErrorLog.objects.create(Error1={'rdata':rData,'resourceData':resourceData,'count':len(queryset)},Error2={'formDef':formDefinition,'m2m':m2mData,'formData':formData,'filter':filter},CreatedDateTime=datetime.now(timezone.utc)) 
            
    response = {}
    response['resourceDefinition'] = resourceDefinition
    response['formDefinition'] = formDefinition
    response['formDefinitionCard'] = formDefinitionCard
    response['formData'] = formData
    response['columns'] = columns
    response['selectedRecords'] = selectedRecords
    response['queryset'] = queryset
    response['org_id'] = org_id
    response['filter'] = filter
    
    return response
 
 
class TaskViewPdfViewSet(viewsets.GenericViewSet):
      
    queryset = ResourceDefLang.objects.all()
     
    def list(self, request,resourceId,presentationId,taskDefId,formType,lang):
        
        import datetime
        
        resource_id = presentation_id = task_def_id = None
        form_type = 'i'
        language = 'en-GB' 
        #pdb.set_trace()
        print(datetime.datetime.now())
          
        if 'resourceId' in self.kwargs: 
            resource_id=int(self.kwargs['resourceId'])
        
        if 'presentationId' in self.kwargs:     
            presentation_id=int(self.kwargs['presentationId'])
     
        if 'taskDefId' in self.kwargs:   
            task_def_id=int(self.kwargs['taskDefId'])
            
        if 'formType' in self.kwargs:   
            form_type=self.kwargs['formType']
        
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
   
        user=request.user
        params={}
        params['presentation_id'] = presentation_id
        params['resource_id'] = resource_id 
        params['form_type'] = form_type 
        params['language'] = language  
        params['task_def_id'] = task_def_id
        params['user'] = user
        
        response1 = ProcessWidgetExecute(params)
        results = response1['selectedRecords']   
        columns = response1['columns']
        
        pdfcolumns=['id']
        for col in columns[1:]:
            name=col['name'].split('.')
            pdfcolumns.append(name[0])
        
        data=[]  
        datalist=[]

        for rec in response1['selectedRecords']:
            new=[]
            for col in pdfcolumns:
                record=rec[col]
                if type(record)==dict:
                    data.append({col:record['name']}) # we dont want code number but name
                    new.append(record['name'])
                else:
                    data.append({col:record})
                    new.append(record)
            datalist.append(new)
        #pdb.set_trace()       
        html_string = render_to_string('pdf.html', {'datalist': datalist, 'columns':response1['columns']})
        #ILJ otkomentirati ako se koristi
        #html = HTML(string=html_string)
        #result = html.write_pdf()
       
        # Creating http response
        response = HttpResponse(content_type='application/pdf;')
        response['Content-Disposition'] = 'inline; filename=list_people.pdf'
        response['Content-Transfer-Encoding'] = 'binary'
        output = tempfile.NamedTemporaryFile(mode='r+b') # open file  
        output.write(result)
        output.seek(0) 
        response.write(output.read())
        print(datetime.datetime.now())
        return response        
        #return Response({'resourceDefinition':response['resourceDefinition'],'widgets':response['formDefinition'],'widgetsCard':response['formDefinitionCard'],'formData':response['formData'],'columns':response['columns'],'data':results})

#VIEW PDF END
    
class ResourceDefViewSet(viewsets.ModelViewSet):
    queryset = ResourceDef.objects.all()
    serializer_class = ResourceDefSerializer
          
class GetResourceDefByLangViewSet(viewsets.ReadOnlyModelViewSet):

   serializer_class = ResourceDefByLangSerializer
    
   def list(self, request, lang): 
   #def get_queryset(self):
    
      #pdb.set_trace() #GetResourceDefByLangViewSet
      if 'pk' in self.kwargs:
        code_id=self.kwargs['pk']
      else:
        code_id = None
  
      if 'lang' in self.kwargs:
        lang = self.kwargs['lang']
      else:
        lang = 'en-GB'    
           
      session = rdbms_cls.Session()
      with session.begin():
          code_status_id = GetStatusCodeByName('a','codestatus')
          
          #ret = ResourceDef.objects.filter(Status_id=code_status_id)\
          #.prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang=lang)))\
          #.prefetch_related('Status','ResourceType','Tags','CreatedBy','ChangedBy').order_by('id')
          
          #pdb.set_trace()
          
          rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa)\
                .join(ResourceDefLangSa,ResourceDefLangSa.ResourceDefId == ResourceDefSa.id)\
                .filter(ResourceDefSa.Status_id==code_status_id, ResourceDefLangSa.Lang == lang )

          return rdef_sa.all()
          
class GetResourceDefByLangFilteredViewSet(viewsets.ReadOnlyModelViewSet):

   #queryset = ResourceDef.objects.all()
   #serializer_class = ResourceDefByLangSerializer
   pagination_class = StandardResultsSetPagination
   
   def create(self,request,lang):
    
      if 'lang' in self.kwargs:
        language = self.kwargs['lang']
      else:
        language = None   
      
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 
      
      session = rdbms_cls.Session()
      where_statement = {}
      
      page = int( request.GET.get('page', 1)  ) 
      page_size = int (request.GET.get('page_size', 10) )
      
      with session.begin():
        
          query = session.query(ResourceDefSa,ResourceDefLangSa, ResourceParamsSa)
              
          active = request.data.get('Active', True)
          if active:
            query = query.filter(ResourceDefSa.Active == active)
            
          if 'Name' in request.data:
            if request.data['Name'] is not None and request.data['Name']!='': 
                query = query.filter(ResourceDefLangSa.Name.ilike('%'+request.data['Name']+'%'))
                where_statement.update({'resourcedeflang__Name__icontains':request.data['Name']})
           
          if 'Subject' in request.data:
            if request.data['Subject'] is not None and request.data['Subject']!='': 
                query = query.filter(ResourceDefLangSa.Subject.ilike('%'+request.data['Subject']+'%'))
                where_statement.update({'resourcedeflang__Subject__icontains':request.data['Subject']})
          
          if 'Description' in request.data:
            if request.data['Description'] is not None and request.data['Description']!='': 
                query = query.filter(ResourceDefLangSa.Description.ilike('%'+request.data['Description']+'%'))
                where_statement.update({'resourcedeflang__Description__icontains':request.data['Description']})
         
          if 'id' in request.data:
            if request.data['id'] is not None and request.data['id']!='': 
                query = query.filter(ResourceDefSa.id == int(request.data['id']))
                where_statement.update({'id':request.data['id']})
          
          if 'ResourceType' in request.data:
            if request.data['ResourceType'] is not None:
              if len(request.data['ResourceType'])>0:
                    resource_types=[]
                    for rt in request.data['ResourceType']:
                      resource_types.append(rt['id'])
                    query = query.filter(ResourceDefSa.ResourceType.in_(resource_types) )
                    where_statement.update({'ResourceType__in':resource_types})
          
          if 'ResourceTags' in request.data:
            if request.data['ResourceTags'] is not None:
              if len(request.data['ResourceTags'])>0:
                    resource_tags=[]
                    for rtg in request.data['ResourceTags']:
                      resource_tags.append(rtg['id'])
                    query = query.filter(ResourceDefSa.Tags.in_(resource_tags) )
                    where_statement.update({'Tags__in':resource_tags})
         
          #ToDo migrate to SQLAlchemy - DONE

          #tmp = Parser().filter_query(ResourceDef)   

          query = query.join(ResourceDefLangSa, and_(ResourceDefLangSa.Lang == language.id,ResourceDefSa.id == ResourceDefLangSa.ResourceDefId)).join(ResourceParamsSa, ResourceDefSa.ResourceType==ResourceParamsSa.id).filter(ResourceDefSa.Organisation == org_id).order_by(ResourceDefSa.id)    
          count = query.count()
          
          # query = query.limit(page_size)
          #if page: 
          #query = query.offset(page*page_size)
          
          defSa = query.limit(page_size).offset((page-1)*page_size).all()

          #rdef = ResourceDef.objects.filter(**where_statement).filter(Organisation_id=org_id).prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang_id=language.id))).prefetch_related('Status','ResourceType','Tags','CreatedBy','ChangedBy').order_by('id')
          #page = self.paginate_queryset(rdef)
                    
          #if page is not None:
          #  
          #  rdef_ser  = ResourceDefByLangSerializer(page,many=True)
          #  ret_data = self.get_paginated_response(rdef_ser.data)
          #else:
          #  rdef_ser = ResourceDefByLangSerializer(page,many=True)
          #  ret_data= Response(data=rdef_ser.data)
          
          ret = {  }
          ret["results"]=[]     
          for item in defSa:
             retItem= item[0]._asdict()
             retItem['resourcedeflang_set'] = [item[1]._asdict()]
             retItem['ResourceType'] = item[2]._asdict()
             ret["results"].append(retItem)
               
          
          ret["count"]  = count 
          ret["countItemsOnPage"] = len(ret["results"])
          ret["next"] = ""
          ret["previous"] = ""
          
          
      return Response(data=ret)
      #return ret_data
      
class GetResourceDefByLangByType_old_ViewSet(viewsets.ReadOnlyModelViewSet):

   # DA LI SE OVO KORISTI ???
   serializer_class = ResourceDefByLangSerializer
    
   def list(status, request):
    
        #code_id = self.kwargs['pk']
        if 'pk' in self.kwargs:
            code_id=self.kwargs['pk']
        else:
            code_id = None
  
        if 'lang' in self.kwargs:
            lang = self.kwargs['lang']
        else:
            lang = 'en-GB'
        if 'resource_type' in self.kwargs:
            resource_type = self.kwargs['resource_type']
        else:
            resource_type = None  
      
        user = self.request.user
        
        org_id,language = helper.get_lang_org (user) 
          
      
        code_status_id = GetStatusCodeByName('a','codestatus')     
        #resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')
        
        #return ResourceDef.objects.filter(ResourceType__ResourceCode=resource_type,Status_id=code_status_id,Organisation_id=org_id).prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang=language))).prefetch_related('Status','ResourceType','Tags','CreatedBy','ChangedBy').order_by('id')
      
        rdefs_sa = session.query(ResourceDefSa)\
                    .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id == ResourceDefLangSa.ResourceDefId, ResourceDefLangSa.Lang==language.id))\
                    .join(ResourceParamsSa, ResourceDefSa.ResourceType == ResourceParamsSa.id)\
                    .filter(or_(ResourceDefSa.Private == False, ResourceDefSa.Organisation == org_id))\
                    .filter( ResourceParamsSa.ResourceCode == resource_type, ResourceDefSa.Status == code_status_id )
          
        return rdefs_sa.all()
 
class GetResourceDefByLangByTypeViewSet(viewsets.GenericViewSet):

   #serializer_class = ResourceDefByLangSerializer
    
   def list(self,resource_type):
    
        #code_id = self.kwargs['pk']
        if 'pk' in self.kwargs:
            code_id=self.kwargs['pk']
        else:
            code_id = None
  
        if 'resource_type' in self.kwargs:
            resource_type = self.kwargs['resource_type']
        else:
            resource_type = None  
      
        user = self.request.user
        
        org_id,language = helper.get_lang_org (user) 
          
        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()

        resources = []
        with session.begin():
            rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa).join(ResourceParamsSa,and_(ResourceDefSa.ResourceType==ResourceParamsSa.id, ResourceParamsSa.ResourceCode==resource_type))\
            .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id==ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang==language.id))\
            .filter(or_(ResourceDefSa.Private==False,ResourceDefSa.Organisation==org_id)).all()

            for rd_item in rdefs_sa:
                resources.append({'id':rdefs_sa[0].id,'name':rdefs_sa[1].Name})
        
        return Response(resources)

class GetResourceDefByTypeForWidgetViewSet(viewsets.GenericViewSet):

   #serializer_class = ResourceDefByLangSerializer
   session = rdbms_cls.Session()
   queryset = ResourceDefLang.objects.all()

   def list(self,request,lang,resource_type):
      
      if 'lang' in self.kwargs:
        lang = self.kwargs['lang']
      else:
        lang = 'en-GB'
      if 'resource_type' in self.kwargs:
        resource_type = self.kwargs['resource_type']
      else:
        resource_type = ''
      
      resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')
      #rdefs=ResourceDefLang.objects.filter(Lang=lang,ResourceDefId__ResourceType=resource_type_n).select_related().order_by('ResourceDefId_id')
      with session.begin():
         rdefs=session.query(ResourceDefLangSa).join(ResourceDefSa, ResourceDefLangSa.ResourceDefId ==  ResourceDefSa.id)\
            .filter(ResourceDefLangSa.Lang==lang, ResourceDefSa.ResourceType==resource_type_n).order_by(ResourceDefId.id)
    
      
         resourceList=[]
         for resource in rdefs.all():
            resourceList.append({'id':resource.ResourceDefId,'relatedId':resource.ResourceDefId,'parentElementId':0,'name':resource.Name,'subject':resource.Subject,'type':'','src_type_id':3})
              
         return Response(resourceList)
    
class GetRelatedDatasetForWidgetViewSet(viewsets.GenericViewSet):

   #serializer_class = ResourceDefByLangSerializer
   
   queryset = PresentationElementsLang.objects.all()

   def list(self,request,datasetId,lang):
  
     if 'datasetId' in self.kwargs:
        dataset_id = self.kwargs['datasetId']
     else:
        dataset_id = 0
        
     if 'lang' in self.kwargs:
        lang = self.kwargs['lang']
     else:
        lang = 'en-GB'
      
     #ToDo SqlAlchemy upgrade DONE
     session = rdbms_cls.Session()
     datasetList=[]
     with session.begin():
          #pe=PresentationElements.objects.filter(Related_id=dataset_id,ElementType='resource',Status='Active')     
          pe = session.query(PresentationElementsSa).filter(Related==dataset_id , ElementType=='resource',Status=='Active')
 
          datasetFilter=[]
          taskList=[]
          for dataset in pe.all():
            #datasetFilter.append(dataset.ResourceDefId_id)     
            datasetFilter.append(dataset.ResourceDefId)             
            
          #ToDo SqlAlchemy upgrade   DONE
          #rdefs = ResourceDefLang.objects.filter(ResourceDefId_id__in = datasetFilter,Lang=lang).select_related()
          rdefs = session.query(ResourceDefLangSa).filter(ResourceDefId.in_(datasetFilter), Lang==lang)
          
          for ds in rdefs.all():
            datasetList.append({'id':ds.ResourceDefId,'name':ds.Name}) 
          
     #return Response({'datasets':datasetList,'tasks':taskList})
     return Response(datasetList)
        
class GetResourceDefListByLangByTypeViewSet(viewsets.GenericViewSet):

   #queryset=ResourceDefLang.objects.all()
    
   def list(self,request,lang,resource_type):
      
    if 'lang' in self.kwargs:
        language = self.kwargs['lang']
    else:
        language = None
    if 'resource_type' in self.kwargs:
        resource_type = self.kwargs['resource_type']
    else:
        resource_type = ''
      
    #resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')
    user = self.request.user
    
    org_id,language = helper.get_lang_org (user) 
      
    #ToDo SqlAlchemy upgrade   DONE
    rdbms_cls = RdbmsClass()

    session = rdbms_cls.Session()
    resourceList=[]
    with session.begin():
        rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa).join(ResourceParamsSa,and_(ResourceDefSa.ResourceType==ResourceParamsSa.id, ResourceParamsSa.ResourceCode==resource_type))\
        .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id==ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang==language.id))\
        .filter(or_(ResourceDefSa.Private==False,ResourceDefSa.Organisation==org_id)).order_by(ResourceDefSa.id).all()

        for rd_item in rdefs_sa:
            if rd_item[1] is not None:
                name = rd_item[1].Name
            else:
                name = 'Not found'
            resourceList.append({'id':rd_item[0].id,'name':name})

          
    return Response(resourceList)

class GetSystemPagesViewSet(viewsets.GenericViewSet):
    
   def list(self,request):

    user = self.request.user
    
    org_id,language = helper.get_lang_org (user) 
    
    page_type_code_id = GetStatusCodeByName('system','pagetypes')

    rdbms_cls = RdbmsClass()

    session = rdbms_cls.Session()
    resourceList=[]
    resource_type='page'
    with session.begin():
        rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa).join(ResourceParamsSa,and_(ResourceDefSa.ResourceType==ResourceParamsSa.id, ResourceParamsSa.ResourceCode==resource_type))\
        .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id==ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang==language.id))\
        .join(PagesSa,ResourceDefSa.id==PagesSa.ResourceDefId,PagesSa.PageType==page_type_code_id)\
        .filter(ResourceDefSa.Active==True,or_(ResourceDefSa.Private==False,ResourceDefSa.Organisation==org_id)).all()

        for rd_item in rdefs_sa:
            resourceList.append({'id':rd_item[0].id,'name':rd_item[1].Name})

          
    return Response(resourceList)
      
class GetResourceDefListByLangByTypeFilterViewSet(viewsets.GenericViewSet):

   queryset=ResourceDefLang.objects.all()
    
   def create(self,request,lang,resource_type,selectedDataset):
      
      if 'lang' in self.kwargs:
        language = self.kwargs['lang']
      else:
        language = None
      if 'resource_type' in self.kwargs:
        resource_type = self.kwargs['resource_type']
      else:
        resource_type = ''
      if 'selectedDataset' in self.kwargs:
        selected_dataset = int(self.kwargs['selectedDataset'])
      else:
        selected_dataset = 0
      
      #resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')
      if 'search' in request.data:
        search = request.data['search']
      else:
        search=''
      
      rdefs1 = None
      
      user = self.request.user
      
      org_id,language = helper.get_lang_org (user) 
      
        
      session = rdbms_cls.Session()
      with session.begin():

          if search != '':   # IF SEARCH FLITER
             if selected_dataset != 0: # SEARCH AND SELECTED
                #rdefs=ResourceDefLang.objects.filter( Q(ResourceDefId_id=selected_dataset, ResourceDefId__Organisation_id=org_id) | Q(Lang=language,ResourceDefId__Organisation=org_id,   ResourceDefId__ResourceType__ResourceCode=resource_type,Name__icontains=search)).select_related().order_by('Name')    
                rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa)\
                    .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id==ResourceDefLangSa.ResourceDefId, ResourceDefLangSa.Lang==language.id))\
                    .filter(or_(ResourceDefSa.Private==False, ResourceDefSa.Organisation==org_id))\
                    .join(ResourceParamsSa, ResourceDefSa.ResourceType==ResourceParamsSa.id)\
                    .filter( _or (ResourceDefSa.id == selected_dataset, and_(ResourceParamsSa.ResourceCode == resource_type, ResourceDefLangSa.contains(search)) )).all()
                
             else: #SEARCH BUT NOT SELECTED
                #rdefs=ResourceDefLang.objects.filter(Lang=language,ResourceDefId__Organisation_id=org_id,ResourceDefId__ResourceType__ResourceCode=resource_type,Name__icontains=search).select_related().order_by('Name')
                rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa)\
                    .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id == ResourceDefLangSa.ResourceDefId, ResourceDefLangSa.Lang==language.id))\
                    .join(ResourceParamsSa, ResourceDefSa.ResourceType == ResourceParamsSa.id)\
                    .filter(or_(ResourceDefSa.Private == False, ResourceDefSa.Organisation == org_id))\
                    .filter( ResourceParamsSa.ResourceCode == resource_type, ResourceDefLangSa.contains(search) ).all()
          
          else: # NO SEARCH FILTER 
              if selected_dataset != 0:
                  #rdefs=ResourceDefLang.objects.filter(Lang=language,ResourceDefId__Organisation_id=org_id,ResourceDefId__ResourceType__ResourceCode=selected_dataset).select_related().order_by('Name')
                  rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa)\
                    .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id == ResourceDefLangSa.ResourceDefId, ResourceDefLangSa.Lang==language.id))\
                    .join(ResourceParamsSa, and_(ResourceDefSa.ResourceType == ResourceParamsSa.id, ResourceParamsSa.ResourceCode == resource_type))\
                    .filter(or_(ResourceDefSa.Private == False, ResourceDefSa.Organisation==org_id)).all()
              else:
                  #rdefs=ResourceDefLang.objects.filter(Lang=language.id,ResourceDefId__Organisation_id=org_id,ResourceDefId__ResourceType__ResourceCode=resource_type).select_related().order_by('Name')
                  rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa)\
                    .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id == ResourceDefLangSa.ResourceDefId, ResourceDefLangSa.Lang == language.id))\
                    .join(ResourceParamsSa, and_(ResourceDefSa.ResourceType == ResourceParamsSa.id, ResourceParamsSa.ResourceCode == resource_type))\
                    .filter(or_(ResourceDefSa.Private == False, ResourceDefSa.Organisation == org_id)).all()
        
          resourceList=[]
          resourceList2=[]     
          for (rdef, rdeflang) in rdefs_sa:
                #resourceList2.append((item[0]._asdict(),item[1]._asdict()) )
                resourceList2.append ( { 'id': rdef.id, 'name': rdeflang.Name})
            
            
      #for resource in rdefs:
      #  resourceList.append({'id':resource.ResourceDefId_id,'name':resource.Name})
      #if rdefs1 is not None and rdefs1.count()>0:
      #  for resource in rdefs1:
      #    resourceList.append({'id':resource.ResourceDefId_id,'name':resource.Name})
      
      #pdb.set_trace()
      
      return Response(resourceList2)
        
class GetResourceDefListByLangByTypeUserViewSet(viewsets.GenericViewSet):

   queryset=ResourceDefLang.objects.all()
    
   def list(self,request,lang,resource_type,userId):
      
     if 'lang' in self.kwargs:
        language = self.kwargs['lang']
     else:
        language = None
     #language = self.kwargs.get('lang', None)
     
     if 'resource_type' in self.kwargs:
        resource_type = self.kwargs['resource_type']
     else:
        resource_type = ''
     if 'userId' in self.kwargs:
        user_id = int(self.kwargs['userId'])
     else:
        user_id = ''  
           
     user = self.request.user
     org_id,language = helper.get_lang_org (user) 
     
     session = rdbms_cls.Session()
     with session.begin():
      myTasks=[]
      #user = User.objects.get(id = user_id)
      user = session.query(UserSa).where(id = user_id).first()
     
      userGroups = []
      for group in user.groups.all():
        userGroups.append(group.id)
      #ToDo SqlAlchemy upgrade DONE
      #initiatorUsers= TaskAssignmentDefUser.objects.filter(AssigneeId=user)
      #initiatorGroups=TaskAssignmentDefGroup.objects.filter(AssigneeId_id__in=userGroups)
      
      initiatorUsers = session.query(TaskAssignmentDefUsersSa).filter(AssigneeId==user.id)
      initiatorGroups =session.query(TaskAssignmentDefGroupSa).filter(AssigneeId.in_(userGroups))
          
    
      for taksInitiator in initiatorUsers.all():
           #myTasks.append(taksInitiator.TaskDefId_id)
           myTasks.append(taksInitiator.TaskDefId)
      for taksInitiator in initiatorGroups.all():
           #myTasks.append(taksInitiator.TaskDefId_id)
           myTasks.append(taksInitiator.TaskDefId)
      
      resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')

      #rdefs=ResourceDefLang.objects.filter(Lang=language,ResourceDefId__ResourceType=resource_type_n,ResourceDefId_id__in=myTasks).select_related()#.prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang=lang)))
      rdefs= session.query(ResourceDefLangSa,ResourceDefSa)\
          .join(ResourceDefSa, ResourceDefSa.id == ResourceDefLangSa.ResourceDefId)\
          .filter(ResourceDefLangSa.Lang==language, ResourceDefSa.ResourceType==resource_type_n, ResourceDefSa.in_(myTasks))
         
      resourceList=[]
      for (reslang,resource) in rdefs.all():
        resourceList.append({'id':reslang.ResourceDefId,'name':resource.Name})
          
      return Response(resourceList)

class GetResourceDefByLangByTypeUserViewSet(viewsets.ReadOnlyModelViewSet):

   #serializer_class = ResourceDefByLangSerializer
   #pagination_class = StandardResultsSetPagination
   
   #def get_queryset(self):
   def list(self, request, lang, resource_type):
    
      language = None
      resource_type=''
      
      if 'lang' in self.kwargs:
        language = self.kwargs['lang']
 
      if 'resource_type' in self.kwargs:
        resource_type = self.kwargs['resource_type']

      
      myTasks=[]
      
      session = rdbms_cls.Session()
      with session.begin():
      
          user = self.request.user
          
          org_id,language = helper.get_lang_org (user) 

          userGroups = []
          for group in user.groups.all():
            userGroups.append(group.id)
          #ToDo SqlAlchemy upgrade DONE
          #initiatorUsers= TaskAssignmentDefUser.objects.filter(AssigneeId=user.id)
          #initiatorGroups=TaskAssignmentDefGroup.objects.filter(AssigneeId_id__in=userGroups)
          
          initiatorUsers = session.query(TaskAssignmentDefUsersSa).filter(AssigneeId==user.id)
          initiatorGroups =session.query(TaskAssignmentDefGroupSa).filter(AssigneeId.in_(userGroups))
              
          for taksInitiator in initiatorUsers.all():
            #myTasks.append(taksInitiator.TaskDefId_id)
            myTasks.append(taksInitiator.TaskDefId)
          for taksInitiator in initiatorGroups.all():
            #myTasks.append(taksInitiator.TaskDefId_id)
            myTasks.append(taksInitiator.TaskDefId)
          
          resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')
          #return ResourceDef.objects.filter(ResourceType_id=resource_type_n,id__in=myTasks).order_by('id').prefetch_related(Prefetch('resourcedeflang_set',
          #                                    queryset=ResourceDefLang.objects.filter(Lang=language)))

          rdefSA = session.query(ResourceDefSa, ResourceDefLangSa).join(ResourceDefLangSa, ResourceDefSa.id == ResourceDefLangSa.ResourceDefId)\
          .filter(ResourceDefSa.ResourceType == resource_type_n, id.in_(myTasks), ResourceDefLangSa.Lang ==language)
          
          return rdefSA.all()
   
class GetResourceDefByLangByTypePaginatedViewSet(viewsets.ReadOnlyModelViewSet):

   serializer_class = ResourceDefByLangSerializer
   pagination_class = StandardResultsSetPagination
   
   #def get_queryset(self):
   def list(self, request, lang, resource_type):
        if 'lang' in self.kwargs:
          language = self.kwargs['lang']
        else:
          language = 'en-GB'
        if 'resource_type' in self.kwargs:
          resource_type = self.kwargs['resource_type']
        else:
          resource_type = ''
        
        user = self.request.user
        
        org_id,language = helper.get_lang_org (user) 
          
          
        #resource_type_n = GetStatusCodeByName(resource_type,'resourcetype')
        #pdb.set_trace()
        #return ResourceDef.objects.filter(ResourceType__ResourceCode=resource_type,Organisation_id=org_id).order_by('id').prefetch_related(Prefetch('resourcedeflang_set',
        #                                    queryset=ResourceDefLang.objects.filter(Lang=language)))
        rdefs_sa = session.query(ResourceDefSa)\
                    .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id == ResourceDefLangSa.ResourceDefId, ResourceDefLangSa.Lang==language.id))\
                    .join(ResourceParamsSa, ResourceDefSa.ResourceType == ResourceParamsSa.id)\
                    .filter(or_(ResourceDefSa.Private == False, ResourceDefSa.Organisation == org_id))\
                    .filter( ResourceParamsSa.ResourceCode == resource_type )
        
        return rdef_sa.all()

#radi
class SaveResourceDefViewSet(viewsets.ModelViewSet):
    queryset = ResourceDefLang.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))

    def create(self, request):

        resource_data = request.data
        user = request.user
        session = rdbms_cls.Session()
        
        
        org_id,language = helper.get_lang_org (user) 
           
        resource_def_id = resource_data.get('ResourceDefId',None)
        if resource_def_id is not None:
            resource_type = resource_def_id.get('ResourceType',None) 
        
            if resource_type is not None:

                resource_type_code = resource_type.get('ResourceCode',None)
            else:
                resource_type_code = None
            resource_status = resource_def_id.get('Status',None)
        else:
            resource_type_code = None
            resource_status = None

        resource_name = resource_data.get('Name',None)  
        resource_subject = resource_data.get('Subject',None)
        resource_description = resource_data.get('Description',None)
        
        resource_params = resource_data.get('Parameters',{})
        if resource_type_code is not None:

          try:
             with session.begin():
                module_path = 'katalyonode.functions.resources.'+resource_type_code+'def' 
                module_ref = importlib.import_module(module_path)
                resource_class = getattr(module_ref, resource_type_code)
                resource_object = resource_class(None, resource_type_code,resource_name,resource_subject,resource_description,resource_status,user,language,org_id,resource_params,None,resource_sub_type)
                
                if resource_object is not None:
                        resource_object.set_session(session)
                        resource_object.set_new_resource_data(resource_data)
                        resource_object.save_definition()

          except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError --> " + module_path)

        response_json = resource_object.get_definition_json()
        return Response(data= response_json,status=201)

#novo da se makne na klijentu "ResourceDefId"
class SaveResourceDefViewSet2(viewsets.ModelViewSet):
    queryset = ResourceDefLang.objects.all().select_related()

    def create(self, request):

        resource_data = request.data
        user = request.user
        
        org_id,language = helper.get_lang_org (user) 
        resource_type_code = None
         
        resource_id = resource_data.get('id',None)
        resource_type = resource_data.get('ResourceType',None)
        if resource_type is not None:
                resource_type_code = resource_type.get('ResourceCode',None)
        #resource_status = resource_data.get('Status',None)
        resource_active = resource_data.get('Active',True)
        resource_sub_type = resource_data.get('ResourceSubType',None) 
        resource_name = resource_data.get('Name',None)  
        resource_subject = resource_data.get('Subject',None)
        resource_description = resource_data.get('Description',None)
        resource_params = resource_data.get('Parameters',{})
        resource_params['resource_admins'] = resource_data.get('AdminUsers',None) 
        resource_params['resource_admin_groups'] = resource_data.get('AdminGroups',None)
        
        #pdb.set_trace()
        session = rdbms_cls.Session()
        if resource_type_code is not None:

            try:
                with session.begin():
                    module_path = 'katalyonode.functions.resources.'+resource_type_code+'def' 
                    module_ref = importlib.import_module(module_path)
                    resource_class = getattr(module_ref, resource_type_code)   
                    
                    resource_object = resource_class(resource_id,  resource_type_code,resource_name,resource_subject,resource_description,resource_active,user,language,org_id,resource_params,None,resource_sub_type)
                    resource_object.set_session(session)

                    if resource_object is not None:
                            resource_object.set_new_resource_data(resource_data)
                            resource_object.save_definition()

            except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError --> " + module_path)
        #pdb.set_trace()  
        response_json = resource_object.get_definition_json()
        return Response(data=response_json,status=201)

class GetResourceFormViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     permission_classes = (AllowAny, )
     authentication_classes = ()
     
     def list(self, request,resourceId,formType,lang,version):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'

        if 'version' in self.kwargs:
           ver=int(self.kwargs['version'])
        else:
           ver = 0
        
        if 'lang' in self.kwargs:
           language=int(self.kwargs['lang'])
        else:
           language = None
        
        user=request.user
        org_id=None
        
        session = rdbms_cls.Session()
        with session.begin():
        
            if type(user) is not AnonymousUser:  
                org_id,language = helper.get_lang_org (user)
                language = language.id
            else:
                #ToDo SqlAlchemy upgrade DONE
                #language = Language.objects.get(id=language)
                language = session.query(LanguageSa).filter(LanguageSa.id == language).first()
                #pdb.set_trace()
                      
            if ver==0:
                #ToDo SqlAlchemy upgrade DONE
                #version_max = PresentationElements.objects.filter(ResourceDefId=resource_id,FormType=form_type,ResourceType='o',Status='Active').aggregate(Max('VersionTo'))              
                version_max = session.query(func.max(PresentationElementsSa.VersionTo) )  \
                .filter(PresentationElementsSa.ResourceDefId==resource_id, PresentationElementsSa.FormType==form_type, PresentationElementsSa.ResourceType=='o', PresentationElementsSa.Status=='Active').first()
                
                #pdb.set_trace()
                if version_max is not None:
                    #if 'VersionTo__max' in version_max:
                   ver = version_max[0]
                   if ver is None:
                        ver = 0
            #ToDo SqlAlchemy upgrade DONE
            #rdef = ResourceDef.objects.get(id=resource_id)
            
            rdef_rtype = session.query(ResourceDefSa, ResourceParamsSa).join(ResourceParamsSa, ResourceParamsSa.id == ResourceDefSa.ResourceType)\
            .filter(ResourceDefSa.id==resource_id).first()
            
            resource_type = 'dataset'
            rdef = None
            rtype = None
            if rdef_rtype is not None:
                #resource_type=rdef.ResourceType.ResourceCode
                rdef = rdef_rtype[0]
                rtype = rdef_rtype[1]
                resource_type = rtype.ResourceCode
            else:
                raise Exception("Error => Resource "+str(resource_id)+" not found")

            #get from class
            #session = rdbms_cls.Session()   
            try:
                    #with session.begin():
                    module_path = 'katalyonode.functions.resources.'+resource_type+'def'
                    
                    module_ref = importlib.import_module(module_path)
                    resource_class = getattr(module_ref, resource_type)
                    
                    resource_object = resource_class(resource_id,resource_type,'','','','',user,language,org_id,{},None)
                    resource_object.set_session(session)
                    if resource_object is not None:
                        #try:
                            resource_object.get_form_definition(form_type,ver)
                            resource_object.generate_form()
                            resource_object.get_definition()
                            formDefinition = resource_object.get_form_json()
                            resourceDefinition = resource_object.get_definition_json()
                        #except AttributeError:
                            #raise ValidationError("AttributeError --> " + str(vars(resource_object)))
            except ModuleNotFoundError:
                    raise ValidationError("ModuleNotFoundError --> " + module_path)

        return Response({'resourceDefinition':resourceDefinition,'widgets':formDefinition})

class GetResourceFormWithDataViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     
     def list(self, request,resourceDefId,resourceId,formType,lang):
        
        if 'resourceDefId' in self.kwargs:
           resource_def_id=int(self.kwargs['resourceDefId'])
        else:
           resource_def_id = None
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None 
        
        user = request.user
        
        
        org_id,language = helper.get_lang_org (user) 
        
        
        formDefinition,resourceDefinition = GetResourceFormDB(resource_def_id,form_type,language,0,0,'w',user,org_id,False)
       
        #formDefinitionInJsonWithData = GetResourceFormJSONWithData(formDefinition,resource_def_id,resource_id,user)
        #formData = GetDatasetFormData(formDefinition,resource_def_id,resource_id,user)
        formData,recordId = GetDatasetFormData(resource_def_id,form_type,'n',0,0,0,'',resource_id,org_id,user)
        
        return Response({'resourceDefinition':resourceDefinition,'widgets':formDefinition,'form_data':formData})

class GetResourceFormExtendedViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     
     def list(self, request,resourceId,taskDefId,formType,parentId,overrideType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'taskDefId' in self.kwargs:
           task_def_id=int(self.kwargs['taskDefId'])
        else:
           task_def_id = None   
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        if 'parentId' in self.kwargs:
           parent_id=int(self.kwargs['parentId'])
        else:
           parent_id = None
           
        if 'overrideType' in self.kwargs:
           override_type=self.kwargs['overrideType']
        else:
           override_type = 'w'
        
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None 
        
        name=''
        formDefined=False
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
        rdef = ResourceDef.objects.get(id=resource_id)
        resource_type = 'dataset'
        if rdef is not None:
            resource_type=rdef.ResourceType.ResourceCode

        #get from class
        session = rdbms_cls.Session()
        try:
            with session.begin():
                module_path = 'katalyonode.functions.resources.'+resource_type+'def'
                
                module_ref = importlib.import_module(module_path)
                resource_class = getattr(module_ref, resource_type)
                
                resource_object = resource_class(resource_id, resource_type,'','','','',user,language,org_id,{},None)
                resource_object.set_session(session)
                if resource_object is not None:
                    #try:
                        resource_object.get_form_definition(form_type)
                        resource_object.generate_form()
                        resource_object.get_definition()
                        formDefinition = resource_object.get_form_json()
                        resourceDefinition = resource_object.get_definition_json()
                    #except AttributeError:
                        #raise ValidationError("AttributeError --> " + str(vars(resource_object)))
        except ModuleNotFoundError:
                raise ValidationError("ModuleNotFoundError --> " + module_path)

        return Response({'resourceDefinition':resourceDefinition,'widgets':formDefinition})

        #formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_id,override_type,user,org_id,True)
        #pdb.set_trace()
        
        
        return Response({'resourceDefinition':resourceDefinition,'widgets':formDefinition})
        
        
        
class GetResourceFormExtendedMultipleViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     
     def create(self, request,resourceId,taskDefId,formType,parentId,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'taskDefId' in self.kwargs:
           task_def_id=int(self.kwargs['taskDefId'])
        else:
           task_def_id = None   
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        if 'parentId' in self.kwargs:
           parent_id=int(self.kwargs['parentId'])
        else:
           parent_id = None
        
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        name=''
        formDefined=False
        forms={}
        overrideTypes = request.data
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
        
        for ot in overrideTypes:
          formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_id,ot,user,org_id,True)
          forms['widgets'+ot] = formDefinition
        
        
        return Response({'resourceDefinition':resourceDefinition,'widgets':forms})

#not used
"""
class GetResourceFormExtendedWithDataViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     permission_classes = (AllowAny, )
     #authentication_classes = ()
   
     def list(self, request,resourceId,taskInitiateId,taskExecuteId,populateType,parentId,parentInitiateId,prevTaskInstanceId,formType,widgetType,resourceRecordId,overrideType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'taskInitiateId' in self.kwargs:
           task_initiate_id=int(self.kwargs['taskInitiateId'])
        else:
           task_initiate_id = None
      
        if 'taskExecuteId' in self.kwargs:
           task_execute_id=int(self.kwargs['taskExecuteId'])
        else:
           task_execute_id = None
           
        if 'populateType' in self.kwargs:
           populate_type=self.kwargs['populateType']
        else:
           populate_type = 3
        if 'parentId' in self.kwargs:
           parent_id=int(self.kwargs['parentId'])
        else:
           parent_id = 0
        if 'parentInitiateId' in self.kwargs:
           parent_initiate_id=int(self.kwargs['parentInitiateId'])
        else:
           parent_initiate_id = 0
           
        if 'prevTaskInstanceId' in self.kwargs:
           prev_task_instance_id=int(self.kwargs['prevTaskInstanceId'])
        else:
           prev_task_instance_id = 0
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        
        if 'widgetType' in self.kwargs:
           widget_type=int(self.kwargs['widgetType'])
        else:
           widget_type = 3
           
        if 'resourceRecordId' in self.kwargs:
           resource_record_id=int(self.kwargs['resourceRecordId'])
        else:
           resource_record_id = 0
                
        if 'overrideType' in self.kwargs:
           override_type=self.kwargs['overrideType']
        else:
           override_type = 'w'
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        #pdb.set_trace()
        user_id=request.user
        
        org_id,language = helper.get_lang_org (user) 
        session = rdbms_cls.Session()
        with session.begin():
          
            name=''
            formDefined = False
            task_def_id=0
            task_instance_id = 0
            src_task_instance_id = 0
            search_data=None
            if (form_type=='i'):
                src_task_instance_id = prev_task_instance_id
                #pes = PresentationElements.objects.filter(id=parent_initiate_id)
                pe = session.query(PresentationElementsSa).filter( id == parent_initiate_id)
     
                #for pe in pes:
                task_def_id = pe.ResourceDefId
                
            else:
              #src_task_instance_id=0
              if task_initiate_id>0:
                #tiis = TaskInitiationInstance.objects.filter(id=task_initiate_id)
                tii = session.query(TaskInitiationInstanceSa).where(id == task_initiate_id).first()
                  
                #for tii in tiis:
                task_def_id = tii.TaskDefId_id
                taskObject=tii
                task_instance_id = task_initiate_id
                if populate_type in ['2i','2e']:
                  src_task_instance_id = task_initiate_id
                elif populate_type in ['3i']:
                  #ToDo - migrate to SQLAlchemy DONE
                  #teis = TaskExecutionInstances.objects.filter(id=tii.PreviousTaskId_id)
                  tei = session.query(TaskExecutionInstancesSa).where(id == task_execute_id).first()
                  
                  #for tei in teis:
                  #task_def_id = tei.TaskDefId_id
                  src_task_instance_id = tei.TaskInitiateId_id
                elif populate_type in ['3e']:
                  src_task_instance_id = tii.PreviousTaskId_id
                  
              elif task_execute_id>0: 
                #ToDo - migrate to SQLAlchemy DONE
                #teis = TaskExecutionInstances.objects.filter(id=task_execute_id)
                tei = session.query(TaskInitiationInstanceSa).where(id == task_execute_id).first()
                 
                #for tei in teis:
                task_def_id = tei.TaskDefId_id
                taskObject=tei
                task_instance_id = task_execute_id
                
                if populate_type in ['2i']:
                  src_task_instance_id = tei.TaskInitiateId_id
                elif populate_type in ['2e']:
                  src_task_instance_id = task_execute_id
                elif populate_type in ['3i']:
                  #ToDo - migrate to SQLAlchemy DONE
                  #tiis = TaskInitiationInstance.objects.filter(id=tei.TaskInitiateId_id)
                  tii = session.query(TaskInitiationInstanceSa).where(id == tei.TaskInitiateId).first()
                  
                  #"for tii in tiis:
                  prev_exe = tii.PreviousTaskId
                  #teis = TaskExecutionInstances.objects.where(id==prev_exe)
                  tei = session.query(TaskExecutionInstancesSa).where(id == prev_exe).first()
                  
                  #for tei in teis:
                  src_task_instance_id = tei.TaskInitiateId
                elif populate_type in ['3e']:
                  #tiis = TaskInitiationInstance.objects.filter(id=tei.TaskInitiateId_id)
                  tii = session.query(TaskInitiationInstanceSa).where(id == tei.TaskInitiateId).first()
                  
                  #for tii in tiis:
                  src_task_instance_id = tii.PreviousTaskId
                elif populate_type[0:1] in ['4']:
                  src_task_instance_id = prev_task_instance_
                  #pes = PresentationElements.objects.filter(id=parent_initiate_id)
                  pe = session.query(PresentationElementsSa).where(id == parent_initiate_id)
                  
                  #for pe in pes:
                  task_def_id = pe.ResourceDefId
                  search_data = pe.Search
              else:
                #pes = PresentationElements.objects.filter(id=parent_initiate_id)
                pe = session.query(PresentationElementsSa).where( id == parent_initiate_id)
     
                #for pe in pes:
                task_def_id = pe.ResourceDefId
                search_data = pe.Search
            if populate_type in ['2i','3i']:
               source_form_type='i'
            else:
               source_form_type='e'
      
            
            formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user_id,org_id,True)
            
            formDefinitionInJsonExtended = ExtendResourceFormJSON(formDefinition,resource_id,task_def_id,parent_id,form_type)
            #ErrorLog.objects.create(Error1={'widget_type':widget_type,'parent':parent_initiate_id},Error2={'mapping':pes[0].DatasetMapping},CreatedDateTime=datetime.now(timezone.utc)) 
            #pdb.set_trace()
            if widget_type!=4:
              formData,recordId = GetDatasetFormData(resource_id,form_type,override_type,task_def_id,src_task_instance_id,parent_id,populate_type,resource_record_id,org_id,user_id)
            else:
              if search_data is None:
                #pes = PresentationElements.objects.filter(id=parent_initiate_id)
                pe = session.query(PresentationElementsSa).filter( id == parent_initiate_id)
     
                #for pe in pes:
                search_data = pe.Search
                
              if search_data is not None and len(search_data)>0:
                rData,f,selected_related,parameters = ExtractFormDataSearch(search_data[0],formDefinitionInJsonExtended,user_id) 
                formData = [f]
              else:
                formData=[]
            
            #process mapping
            #pes = PresentationElements.objects.filter(id=parent_initiate_id)
            pes = session.query(PresentationElementsSa).filter( id == parent_initiate_id)
     
            
            for pe in pes:
              if pe.DatasetMapping is not None:
                dmap_list = []
                if type(pe.DatasetMapping) == str:
                    dmap = json.loads(pe.DatasetMapping)
                elif type(pe.DatasetMapping) is list:
                    
                    dmap = pe.DatasetMapping
                   
                    for dm in dmap:
                        if 'id' in dmap:
                            dmap_list.append(dm['id'])
                        elif isinstance(dm, int):
                            dmap_list.append(dm)
                
                #mappingPackages = DatasetMappingHead.objects.filter(id__in=dmap_list).select_related()
                mappingPackages = session.query(DatasetMappingHeadSa).filter(id.in_(dmap_list)).all()
                
                for mph in mappingPackages:
                  formData,resourceRecordId = ApplyMappingPackage(formData,resource_id,task_def_id,task_initiate_id,task_execute_id,prev_task_instance_id,form_type,parent_initiate_id,mph,resource_record_id,request.session,org_id,user_id.id)
            
            preProcess=None
                
            if widget_type==3 and task_execute_id>0:
        
              if pe is not None:  
                preProcess= PreProcessResourceWidget (formData,pe,prev_task_instance_id,parent_initiate_id,taskObject,user_id,form_type,resourceRecordId,0,0,'view',language,org_id,False)
              else:
                return Response(data={'msg':'Pre-processing error-->presentation elements definition not found'},status=400)
            else:
              preProcess={}
            
            if preProcess is not None:
              return Response({'resourceDefinition':resourceDefinition,'resourceRecordId': resourceRecordId,'widgets':formDefinitionInJsonExtended,'form_data':formData})
            else:
              return Response(data={'msg':'Pre-processing error'},status=400)



#not used
class GetResourceFormExtendedWithDataMultipleViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     
     def create(self, request,resourceId,taskInitiateId,taskExecuteId,populateType,parentId,parentInitiateId,prevTaskInstanceId,formType,widgetType,resourceRecordId,overrideType,parentDatasetDefId,rmdId,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'taskInitiateId' in self.kwargs:
           task_initiate_id=int(self.kwargs['taskInitiateId'])
        else:
           task_initiate_id = None
      
        if 'taskExecuteId' in self.kwargs:
           task_execute_id=int(self.kwargs['taskExecuteId'])
        else:
           task_execute_id = None
           
        if 'populateType' in self.kwargs:
           populate_type=self.kwargs['populateType']
        else:
           populate_type = 3
        if 'parentId' in self.kwargs:
           parent_id=int(self.kwargs['parentId'])
        else:
           parent_id = 0
        if 'parentInitiateId' in self.kwargs:
           parent_initiate_id=int(self.kwargs['parentInitiateId'])
        else:
           parent_initiate_id = 0
           
        if 'prevTaskInstanceId' in self.kwargs:
           prev_task_instance_id=int(self.kwargs['prevTaskInstanceId'])
        else:
           prev_task_instance_id = 0
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        
        if 'widgetType' in self.kwargs:
           widget_type=int(self.kwargs['widgetType'])
        else:
           widget_type = 3
           
        if 'resourceRecordId' in self.kwargs:
           resource_record_id=int(self.kwargs['resourceRecordId'])
        else:
           resource_record_id = 0
                
        if 'overrideType' in self.kwargs:
           override_type=self.kwargs['overrideType']
        else:
           override_type = 'w'
        
        if 'parentDatasetDefId' in self.kwargs:
           parent_dataset_def_id=int(self.kwargs['parentDatasetDefId'])
        else:
           parent_dataset_def_id = 0
        
        if 'rmdId' in self.kwargs:
           rmd_id=int(self.kwargs['rmdId'])
        else:
           rmd_id = 0
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None 
        
        user_id=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
          
        task_def_id=None
        
        if (form_type=='i'):
            src_task_instance_id = prev_task_instance_id
            #pes = PresentationElements.objects.filter(id=parent_initiate_id)
            pe = session.query(PresentationElementsSa).filter( id == parent_initiate_id)).first()
       
 
            #for pe in pes:
            task_def_id = pe.ResourceDefId
            
        else:
          #src_task_instance_id=0
          if task_initiate_id>0:
            #tiis = TaskInitiationInstance.objects.filter(id=task_initiate_id)
            for tii in tiis:
              task_def_id = tii.TaskDefId_id
              taskObject=tii
            task_instance_id = task_initiate_id
            if populate_type in ['2i','2e']:
              src_task_instance_id = task_initiate_id
            elif populate_type in ['3i']:
              #teis = TaskExecutionInstances.objects.filter(id=tii.PreviousTaskId_id)
              for tei in teis:
                #task_def_id = tei.TaskDefId_id
                src_task_instance_id = tei.TaskInitiateId_id
            elif populate_type in ['3e']:
              src_task_instance_id = tii.PreviousTaskId_id
              
          elif task_execute_id>0: 
            #teis = TaskExecutionInstances.objects.filter(id=task_execute_id)
            for tei in teis:
              task_def_id = tei.TaskDefId_id
              taskObject=tei
            task_instance_id = task_execute_id
            
            if populate_type in ['2i']:
              src_task_instance_id = tei.TaskInitiateId_id
            elif populate_type in ['2e']:
              src_task_instance_id = task_execute_id
            elif populate_type in ['3i']:
              tiis = TaskInitiationInstance.objects.filter(id=tei.TaskInitiateId_id)
              for tii in tiis:
                #task_def_id = tei.TaskDefId_id
                prev_exe = tii.PreviousTaskId_id
              teis = TaskExecutionInstances.objects.filter(id=prev_exe)
              for tei in teis:
                src_task_instance_id = tei.TaskInitiateId_id
            elif populate_type in ['3e']:
              tiis = TaskInitiationInstance.objects.filter(id=tei.TaskInitiateId_id)
              for tii in tiis:
                src_task_instance_id = tii.PreviousTaskId_id
              
        if populate_type in ['2i','3i']:
           source_form_type='i'
        else:
           source_form_type='e'
       
        if populate_type in ['1i','1e']:
          src_task_instance_id = 0
        
        
        forms = []
        formsData = []
        recordsIds=[]
        name=''
        formDefined = False       
        user=request.user
        filter_arr = request.data['filter']
        form = request.data['form']
        if len(filter_arr)>0:
          filter=filter_arr[0];
        else:
          return Response(data='Invalid search form --> Filter not found',Status=500)
        
        
        org_id,language = helper.get_lang_org (user) 
        
        
        formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
        
        if resource_record_id==0 and form is not None:
          ret, parmeters = ExtractFormDataSearch(filter,form,user)
          qFilter,m2mData = ProcessResourceDataSearch(ret,resource_id,org_id)
              
          queryset =  mutant_get_data_filtered(mutant_get_model(resource_id,org_id),qFilter, parameters={})
        elif rmd_id>0:
          #get related
          filter = []
          
          relatedQueryset = mutant_get_m2m_data (parent_dataset_def_id,resource_id,resource_record_id,rmd_id,org_id,0)
          for item in relatedQueryset:
            filter.append(item.ResourceRelatedId)
          queryset =  mutant_get_data_filtered_in(mutant_get_model(resource_id,org_id),filter)
          
        else:
          filter = []
          for item in request.data:
            filter.append(item['id'])
          queryset =  mutant_get_data_filtered_in(mutant_get_model(resource_id,org_id),filter)
        
        for rec in queryset:
          
          resource_record_id = rec.id
          formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
          #formDefinitionInJsonExtendedWithData,resourceRecordId = ExtendResourceFormJSONWithData(formDefinition,resource_id,0,0,0,'',resource_record_id,user_id)
          formData,resourceRecordId = GetDatasetFormData(resource_id,form_type,override_type,task_def_id,src_task_instance_id,parent_id,populate_type,resource_record_id,user_id)
          forms.append(formDefinition)
          formsData.append(formData)
          
          
          #formDefinitionInJsonExtendedWithData=None
          recordsIds.append(rec.id)
          
        
        return Response({'resourceDefinition':resourceDefinition,'resourceRecordId': recordsIds,'widgets':forms,'form_data':formsData})


#not used
class GetResourceFormAddMultipleViewSet(viewsets.GenericViewSet):
    queryset = ResourceDefLang.objects.all()
    session.rdbms_cls.Session()
     
    def create(self, request,resourceId,taskInitiateId,taskExecuteId,parentId,parentInitiateId,prevTaskInstanceId,formType,widgetType,overrideType,lang):
        
      with session.begin():  
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
           
        if 'taskInitiateId' in self.kwargs:
           task_initiate_id=int(self.kwargs['taskInitiateId'])
        else:
           task_initiate_id = None
      
        if 'taskExecuteId' in self.kwargs:
           task_execute_id=int(self.kwargs['taskExecuteId'])
        else:
           task_execute_id = None
           
        if 'populateType' in self.kwargs:
           populate_type=self.kwargs['populateType']
        else:
           populate_type = 3
        if 'parentId' in self.kwargs:
           parent_id=int(self.kwargs['parentId'])
        else:
           parent_id = 0
        if 'parentInitiateId' in self.kwargs:
           parent_initiate_id=int(self.kwargs['parentInitiateId'])
        else:
           parent_initiate_id = 0
           
        if 'prevTaskInstanceId' in self.kwargs:
           prev_task_instance_id=int(self.kwargs['prevTaskInstanceId'])
        else:
           prev_task_instance_id = 0
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
        
        if 'widgetType' in self.kwargs:
           widget_type=int(self.kwargs['widgetType'])
        else:
           widget_type = 3
           
        if 'resourceRecordId' in self.kwargs:
           resource_record_id=int(self.kwargs['resourceRecordId'])
        else:
           resource_record_id = 0
                
        if 'overrideType' in self.kwargs:
           override_type=self.kwargs['overrideType']
        else:
           override_type = 'w'
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        user_id=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
          
        src_task_instance_id = prev_task_instance_id
        
        task_def_id = None
        
        if (form_type=='i'):
            src_task_instance_id = prev_task_instance_id
            #pes = PresentationElements.objects.filter(id=parent_initiate_id)
            pe = session.query(PresentationElementsSa).filter( id == parent_initiate_id).first()
       
            #for pe in pes:
            task_def_id = pe.ResourceDefId
            
        else:
        
          if task_initiate_id>0:
            #tiis = TaskInitiationInstance.objects.filter(id=task_initiate_id)
            tii = session.query(TaskInitiationInstanceSa).filter( id == task_initiate_id).first() 
     
            #for tii in tiis:
            task_def_id = tii.TaskDefId
            taskObject=tii
              
          elif task_execute_id>0: 
            #teis = TaskExecutionInstances.objects.filter(id=task_execute_id)
            tei = session.query(TaskExecutionInstanceSa).filter( id == task_execute_id).first() 
            
            #for tei in teis:
            task_def_id = tei.TaskDefId
            taskObject=tei
            
        
        forms = []
        recordsIds=[]
        name=''
        formDefined = False       
        filter_in = []       
        #formDefinition,name,formDefined = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,True)
        
        filter = request.data
        
        
        if 'records' in filter:
          filter_records = filter['records']
        else:
          filter_records=None
        if 'resourceDefId' in filter:
          filter_resource = filter['resourceDefId']
        else:
          filter_resource=None  
      
        
        if filter_records is not None:
          for item in filter_records:  
            filter_in.append(item['id'])
        
            
        queryset =  mutant_get_data_filtered_in(mutant_get_model(resource_id,org_id),filter_in)
        
        #pes = PresentationElements.objects.filter(id=parent_initiate_id)
        pes = session.query(PresentationElementsSa).filter( id == parent_initiate_id)
          
        user=request.user
          
        for item in queryset:
          
          formDefinition,resourceDefinition = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,user,org_id,True)
          #formDefinitionPackage = formDefinition
          
          for pe in pes.all():
            
            if pe.DatasetMapping is not None:
           
              #mappingPackages = DatasetMappingHead.objects.filter(id__in=pe.DatasetMapping).select_related()
              mappingPackages = session.query(DatasetMappingHeadSa).filter(id.in_(pe.DatasetMapping))
              
              #formDefinition,name,formDefined = GetResourceFormDB(resource_id,form_type,language,task_def_id,parent_initiate_id,override_type,True)
              
              resource_record_id=item.id
              for mph in mappingPackages.all():
               
                formDefinition,resourceRecordId = ApplyMappingPackage(formDefinition,task_def_id,task_initiate_id,task_execute_id,prev_task_instance_id,form_type,parent_initiate_id,mph,resource_record_id,request.session,org_id,user_id.id)
                
          
          forms.append(formDefinition)
                
        return Response({'resourceDefinition':resourceDefinition,'resourceRecordId': recordsIds,'widgets':forms})
               
#not used
class SaveDatasetRecordsMultipleViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     
     def create(self, request,resourceId):
        
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
          if resource_id<=0:
            return Response(data='Invalid dataset definition id ('+str(resource_id)+')',status=400)    
        else: 
            return Response(data='Missing dataset definition id (resourceId)',status=400)
          
        forms = request.data
        user_id = request.user
        try:
          for form in forms:
            
            m2mData=[]
            datasetRecord,m2mData = ProcessResourceData(ProcessWidgetData(form,user_id),resource_id)
            
            if 'id' in datasetRecord:
              if datasetRecord['id'] is not None:
                rec = mutant_update_data(datasetRecord,resource_id,user_id)
              else:
                rec = mutant_insert_data(datasetRecord,resource_id,user_id)
            else:
              rec = mutant_insert_data(datasetRecord,resource_id,user_id)
            #if not upd:
              #raise Exception()
            if rec:
              ins_rec=mutant_insert_data_m2m_v2 (rec.id,m2mData,user_id,True)
            
          return Response(data="Update successful",status=200)
        
        except Exception as exc:
        
          return Response(data="Error --> "+str(exc),status=400)


#not used  
class GetResourceFormDbViewSet(viewsets.GenericViewSet):
     queryset = ResourceDefLang.objects.all()
     permission_classes = (AllowAny, )
     authentication_classes = ()
     def list(self, request,resourceId):
        
       
           
        #if 'resourceId' in self.kwargs:
         # resource_id=self.kwargs['resourceId']
        #else:
         # resource_id = None
        
        #pe_set = ResourceDefLang.objects.filter(ResourceDefId=resourceId)
        widgets = []
        #for pe in pe_set:
       
        container1 = Container (1,1,1,'widget','Active',2,2,0,0,100)
       
        #ovdje treba povuci sve elemente iz widgeta
        layout_s1 = FormLayout(1,12,"Testname",True,'list',"Error poruka",'placeholder')
        container1.add_layout(layout_s1)
        widgets.append(container2);
          
        serializer = GetResourceFormSerializer(widgets, many=True)
         
        return Response(serializer.data)

#not used
class GetResourceDataViewSet(viewsets.GenericViewSet):
      
      queryset = ResourceDef.objects.all()
      
      pagination_class = StandardResultsSetPagination
      
      def get_serializer_class(self):
         
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
        else:
          resource_id = None
         
        user=self.request.user
        org_id=None
    
        ueps = UserExtendedProperties.objects.filter(UserId_id=user.id)
         
        for uep in ueps:
          org_id = uep.Organisation_id
        MutantModelRelatedSerializer.Meta.model = mutant_get_model(resource_id,org_id).model_class()
      
        return MutantModelRelatedSerializer
        
      def list(self, request,resourceId,taskDefId,formType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        if 'taskDefId' in self.kwargs:
            task_def_id=int(self.kwargs['taskDefId'])
        else:
            task_def_id = 0
        
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = None
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        
        resourceData={}
        m2mData=[]
     
        datasetRecords = []
        selectedRecords = []
        relatedDict={}
        relatedDictIds={}
        querysetRelated = {}
        user=request.user
        org_id=None
        
        ueps = UserExtendedProperties.objects.filter(UserId_id=user.id)
        for uep in ueps:
          org_id = uep.Organisation_id
        
        queryset =  mutant_get_data(mutant_get_model(resource_id,org_id))
        for item in queryset:
          datasetRecords.append(item.id)
          itemDict = model_to_dict(item)
          selectedRecords.append(itemDict)
        #filter = {}
        #relatedIds=[]
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',MaxLength=1).exclude(Related_id=0).order_by('Related_id')
        for rmd in rmds:
          relatedRecords = mutant_get_m2m_data_in(resource_id,rmd.Related_id,datasetRecords,rmd.id,org_id,0)
          relatedDict[rmd.id] = []
          relatedDictIds[rmd.Related_id] = []
          querysetRelated[rmd.id] = []
          for rr in relatedRecords:
            
            relatedDict[rmd.id].append({'id':rr.ResourceRelatedId,'sourceId':rr.ResourceSourceId,'relatedId':rmd.Related_id,'rmdId':rmd.id,'rmdFieldName':rmd.FieldName})
            relatedDictIds[rmd.Related_id].append(rr.ResourceRelatedId)
          
         
          for rdid in relatedDictIds.items():
            
            querysetRelated[rmd.id] = mutant_get_data_filtered_in(mutant_get_model(rmd.Related_id,org_id),rdid[1])  
              
        for record in selectedRecords:
          
          for rd in relatedDict.items():
           
            for item in rd[1]:
             
              if record['id'] == item['sourceId']:
                
                record[item['rmdFieldName']+'_id']=item['id']
                
                for relatedRec in querysetRelated[rd[0]]:
                  relatedRecDict=PopulateFKColumns(model_to_dict(relatedRec),item['relatedId'],language)
                  
                  if relatedRecDict['id']==item['id']:
                    for column in relatedRecDict.items():
                      
                        #if column[0].startswith('Field'):
                        record[column[0]] = column[1]
                   
                  
          
        rmd_items=[]
        user_items=[]
        group_items=[]
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',GenericType__in=['list','user','group'])
        for rmd in rmds:
          if rmd.GenericType=='list':
            rmd_items.append(rmd.CodeId)
          elif rmd.GenericType=='user':
            user_items.append(rmd.id)
          else:
            group_items.append(rmd.id)
        cds = CodesDetailLang.objects.filter(Lang=language,CodesDetailId__CodesHeadId_id__in=rmd_items).select_related()
        if len(user_items)>0:
          users = User.objects.all()
        if len(group_items)>0:
          groups = Group.objects.all()
        
        for item in selectedRecords:
          
          for rmd in rmds:
            field=rmd.FieldName
            if rmd.GenericType=='list':
              
              for cd in cds:
                value = item[field]
                if cd.CodesDetailId_id==value:
                  
                  item[field] = {'id':cd.CodesDetailId_id,'name':cd.Value}
                  
            if len(user_items)>0:
              for user in users:
                value = item[field]
                if user.id==value:
                  item[field]={'id':user.id,'name':user.first_name+' '+user.last_name+' - '+user.username}
            
            if len(group_items)>0:
              for group in groups:
                value = item[field]
                if group.id==value:
                  item[field]={'id':group.id,'name':group.name}
                  
        columnsDef = get_columns_def(resource_id,task_def_id,form_type,org_id,language)
        
        rdef = ResourceDef.objects.get(id=resource_id).select_related()
        if rdef.ResourceType.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
        
        if selectedRecords is not None and columnsDef is not None:
          return Response({'data':selectedRecords,'columnsDef':columnsDef})
        else:
          return Response(status=404)

"""
class GetDatasetDataViewSet(viewsets.GenericViewSet):
      
    queryset = ResourceDef.objects.all()
      
    def create(self, request,resourceId):
        
        if 'resourceId' in self.kwargs:
           pub_resource_id=int(self.kwargs['resourceId'])
        else:
           pub_resource_id = None
      
        resourceData={}
        m2mData=[]
     
        datasetRecords = []
        selectedRecords = []
        relatedDict={}
        relatedDictIds={}
        querysetRelated = {}
        user=request.user

        filter_data_raw = request.data
        
        
        org_id,language = helper.get_lang_org (user) 
        
        published_resource = helper.get_published_resource(pub_resource_id) 

        #process definition
        filter_data = {}
        resource_id = published_resource['ResourceDefId']
        definition = published_resource['Definition']
        for key,value in filter_data_raw.items():
            for rmd in definition:
                if rmd['FieldName']==key or key=='id':
                    id_key = 'resource'+str(resource_id)+'_'+str(org_id)+'_id'
                    if key=='id' or key.endswith('_id') and value is not None:
                        filter_data[id_key]=value
                    elif not key.endswith('_id') and value is not None:
                        filter_data[key]=value

        rdbms_cls = RdbmsClass()
        rdbms_cls.SetLanguage(language)
        
        datasetRecords = rdbms_cls.get_data_filtered(resource_id,org_id,filter_data,None)
        selectedRecords = []
        for item in datasetRecords:
            itemDict = item._asdict()
            for col in item.keys():
                if hasattr(item[col],'_asdict'):
                    itemDict[col] = item[col]._asdict()
            selectedRecords.append(itemDict)

        return Response(selectedRecords)
        
""" 
#not used
class GetResourceDataWithModelViewSet(viewsets.GenericViewSet):
      queryset = ResourceDef.objects.all()
     
      def get_serializer_class(self):
         
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
        else:
          resource_id = None
        
         
        user=self.request.user
        org_id=None
        for uep in ueps:
          org_id = uep.Organisation_id
        MutantModelRelatedSerializer.Meta.model = mutant_get_model(resource_id,org_id).model_class()
      
        return MutantModelRelatedSerializer
        
      def list(self, request,resourceId,taskDefId,modelId,formType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        if 'taskDefId' in self.kwargs:
            task_def_id=int(self.kwargs['taskDefId'])
        else:
            task_def_id = 0
            
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = None
           
        if 'modelId' in self.kwargs:
            model_id=int(self.kwargs['modelId'])
        else:
            model_id = 0    
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        resourceData={}
        m2mData=[]
     
        datasetRecords = []
        selectedRecords = []
        relatedDict={}
        relatedDictIds={}
        querysetRelated = {}
        user=request.user
        
          
        queryset =  mutant_get_data(mutant_get_model(resource_id,org_id))
        
        page = self.paginate_queryset(queryset)
        if page is not None:
          for item in page:
            datasetRecords.append(item.id)
            itemDict = model_to_dict(item)
            selectedRecords.append(itemDict)
          
        else:
          
          for item in queryset:
            datasetRecords.append(item.id)
            itemDict = model_to_dict(item)
            selectedRecords.append(itemDict)
        #filter = {}
        #relatedIds=[]
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',MaxLength=1).exclude(Related_id=0).order_by('Related_id')
        for rmd in rmds:
          relatedRecords = mutant_get_m2m_data_in(resource_id,rmd.Related_id,datasetRecords,rmd.id,org_id,0)
          relatedDict[rmd.id] = []
          relatedDictIds[rmd.Related_id] = []
          querysetRelated[rmd.id] = []
          for rr in relatedRecords:
            
            relatedDict[rmd.id].append({'id':rr.ResourceRelatedId,'sourceId':rr.ResourceSourceId,'relatedId':rmd.Related_id,'rmdId':rmd.id,'rmdFieldName':rmd.FieldName})
            relatedDictIds[rmd.Related_id].append(rr.ResourceRelatedId)
          
         
          for rdid in relatedDictIds.items():
            
            querysetRelated[rmd.id] = mutant_get_data_filtered_in(mutant_get_model(rmd.Related_id,org_id),rdid[1])  
              
        for record in selectedRecords:
          
          for rd in relatedDict.items():
           
            for item in rd[1]:
             
              if record['id'] == item['sourceId']:
                
                record[item['rmdFieldName']+'_id']=item['id']
                
                for relatedRec in querysetRelated[rd[0]]:
                  relatedRecDict=PopulateFKColumns(model_to_dict(relatedRec),item['relatedId'],language)
                  
                  if relatedRecDict['id']==item['id']:
                    for column in relatedRecDict.items():
                      
                      #if column[0].startswith('Field'):
                        record[column[0]] = column[1]
                   
                  
          
        rmd_items=[]
        user_items=[]
        group_items=[]
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',GenericType__in=['list','user','group'])
        for rmd in rmds:
          if rmd.GenericType=='list':
            rmd_items.append(rmd.CodeId)
          elif rmd.GenericType=='user':
            user_items.append(rmd.id)
          else:
            group_items.append(rmd.id)
        cds = CodesDetailLang.objects.filter(Lang=language,CodesDetailId__CodesHeadId_id__in=rmd_items).select_related()
        if len(user_items)>0:
          users = User.objects.all()
        if len(group_items)>0:
          groups = Group.objects.all()
        
        for item in selectedRecords:
          
          for rmd in rmds:
            field=rmd.FieldName
            if rmd.GenericType=='list':
              
              for cd in cds:
                value = item[field]
                if cd.CodesDetailId_id==value:
                  
                  item[field] = {'id':cd.CodesDetailId_id,'name':cd.Value}
                  
            if len(user_items)>0:
              for user in users:
                value = item[field]
                if user.id==value:
                  item[field]={'id':user.id,'name':user.first_name+' '+user.last_name+' - '+user.username}
            
            if len(group_items)>0:
              for group in groups:
                value = item[field]
                if group.id==value:
                  item[field]={'id':group.id,'name':group.name}
                  
        columnsDef = get_columns_def(resource_id,task_def_id,formType,org_id,language)
        
        rdef = ResourceDef.objects.get(id=resource_id)
        if rdef.ResourceType.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
            
        if selectedRecords is not None and columnsDef is not None:
          if page is not None:
            
            selectedRecords = self.get_paginated_response(selectedRecords)
                  
          
          return Response({'data':selectedRecords,'columnsDef':columnsDef})
        else:
          return Response(status=404)
   
  
#not used     
class GetResourceDataPaginatedViewSet(viewsets.ModelViewSet):
      
      pagination_class = StandardResultsSetPagination
     
      def get_serializer_class(self):

         if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
         else:
           resource_id = None
        
         
         user=self.request.user
         
         org_id,language = helper.get_lang_org (user)

         resource_type_code,status_id = helper.get_resource_type(resource_id)

         MutantModelSerializer.Meta.model = mutant_get_model(resource_id,org_id).model_class()
      
         return MutantModelSerializer
              
      def get_queryset(self):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        user=self.request.user
        
        org_id,language = helper.get_lang_org (user) 
        
          
        queryset =  mutant_get_data(mutant_get_model(resource_id,org_id))
        
        #populate queryset with related (users and codedetail) - need lang for this
        rmd_items=[]
        user_items=[]
        group_items=[]
        
        datasetRecords = []
        relatedDict={}
        
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',GenericType__in=['list','user','group'])
        for rmd in rmds:
          if rmd.GenericType=='list':
            rmd_items.append(rmd.CodeId)
          elif rmd.GenericType=='user':
            user_items.append(rmd.id)
        
          else:
            group_items.append(rmd.id)
        cds = CodesDetailLang.objects.filter(Lang=language,CodesDetailId__CodesHeadId_id__in=rmd_items).select_related()
        if len(user_items)>0:
          users = User.objects.all()
        if len(group_items)>0:
          group = Group.objects.all()
        
        
        
        for item in queryset:
          
          datasetRecords.append(item.id)
          for rmd in rmds:
            field=rmd.FieldName+'_id'
            if rmd.GenericType=='list':
              
              for cd in cds:
                value = getattr(item, field,None)
                if cd.CodesDetailId_id==value:
                  
                  setattr(item, field,{'id':cd.CodesDetailId_id,'name':cd.Value})
                  
            if len(user_items)>0:
              for user in users:
                value = getattr(item, field,None)
                if user.id==value:
                  setattr(item, field,{'id':user.id,'name':user.first_name+' '+user.last_name+' ('+user.username+')'})
        
        
            if len(group_items)>0:
              for group in groups:
                value = getattr(item, field,None)
                if group.id==value:
                  setattr(item, field,{'id':group.id,'name':group.name})
        
        
        #set related fields
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',MaxLength=1).exclude(Related_id=0).order_by('Related_id')
        for rmd in rmds:
          relatedRecords = mutant_get_m2m_data_in(resource_id,rmd.Related_id,datasetRecords,rmd.id,org_id,0)
          relatedDict[rmd.id] = []
          
          if relatedRecords is not None:
            for rr in relatedRecords:
                if rr is not None:
                    relatedDict[rmd.id].append({'id':rr.ResourceRelatedId,'sourceId':rr.ResourceSourceId,'rmdFieldName':rmd.FieldName})
        
        for record in queryset: 
          for rd in relatedDict.items():
           
            for item in rd[1]:
              
              if record.id == item['sourceId']:
                
                setattr(record,item['rmdFieldName']+'_id',item['id'])

        return queryset
"""
class GetResourceDataPaginatedFilterViewSet(viewsets.GenericViewSet):
      
      #pagination_class = StandardResultsSetPagination
      #queryset=ResourceDef.objects.all()
      #serializer_class=MutantModelSerializer
      
      def get_serializer_class(self):
      #def get_my_serializer(resource_id):
         
         if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
         else:
           resource_id = None
        
           
         MutantModelSerializer.Meta.model = mutant_get_model(resource_id,org_id).model_class()
      
         return MutantModelSerializer
              
      def get_queryset(self):
      #def get_my_qs(filter,resource_id):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        filter_arr = None
        form = None
        filter = None
         
        if 'filter' in self.request.data:
            filter_arr=self.request.data['filter']
            
        if 'form' in self.request.data:
            form = self.request.data['form']
            
        if len(filter_arr)>0:
          filter=filter_arr[0];
        #elif:
          #return Response(data='Invalid search form --> Filter not found',Status=500)
        
        user = self.request.user
        
        
        org_id,language = helper.get_lang_org (user) 
        
          
        queryset = GetDatasetDataFiltered(filter,form,user,resource_id,org_id)  
        
        return queryset

 
      
      def create(self, request,resourceId,lang):
         
        resource_id = int(resourceId) if resourceId is not None else None
        language=lang if lang is not None else None
        resourceData={}
        m2mData=[]
        filter_arr = request.data['filter']  if 'filter' in request.data else None
        form = request.data['form']  if 'form' in request.data else None
        columns_for_select = request.data['columns'] if 'columns' in request.data else None
        if filter_arr is None or filter_arr==[] or filter_arr=={}:
           filter_arr = [{}]
         
        if len(filter_arr)>0:
          filter=filter_arr[0];
        else:
          return Response(data='Invalid search form --> Filter not found',status=500)
      
        user=request.user
        session = rdbms_cls.Session()
        
        org_id,language = helper.get_lang_org (user) 
        
        
        if form is None:
            form = []
        
        with session.begin():
            resource_object = dataset(resource_id,'dataset','','','','',user,language,org_id,{})
            resource_object.set_session(session)

            rData,f,select_related, parameters = resource_object.extract_form_data_search(filter,form)
            filter = f
            
            resourceData,m2mData = resource_object.process_resource_data_search(rData)
            
            if columns_for_select is None:
                columns_for_select=[]
                 #queryset =  resource_object.rdbms_cls.get_data_filtered_sr(resource_id,org_id,resourceData,columns_for_select,select_related, parameters)
            #else:
                #queryset =  resource_object.rdbms_cls.get_data_filtered(resource_id,org_id,resourceData, parameters)
            resource_object.rdbms_cls.SetOrganisation(org_id)
            selectedRecords = resource_object.get_dataset_data_filtered(resourceData,columns_for_select,select_related,parameters) 
            
            if selectedRecords is not None:
                return Response(selectedRecords)
            else:
                return Response(status=500)
"""           
#not used - ToDo check
class GetResourceDataPaginatedAutoViewSet(viewsets.GenericViewSet):
      
      pagination_class = StandardResultsSetPagination
       
      def list(self, request,resourceId,presentationId,taskInstanceId,formType,lang):
        
        if resourceId is not None:
           resource_id=int(resourceId)
        else:
           resource_id = None
        if presentationId is not None:
           presentation_id=int(presentationId)
        else:
           presentation_id = None
        if taskInstanceId is not None:
           task_instance_id=int(taskInstanceId)
        else:
           task_instance_id = None
           
        if formType is not None:
           form_type=formType
        else:
           form_type = 'i'
           
        if lang is not None:
           language=lang
        else:
           language = None
       
        resourceData={}
        m2mData=[]
        
        user=request.user
        org_id,language = helper.get_lang_org (user) 
         
        #get filter
        
        #we need previous task ID and then check
        DatasetId = 0
        DatasetDefId = 0
        linked_widget_id = 0
        filter = []
        #ToDo SqlAlchemy upgrade DONE
        
        session = rdbms_cls.Session()
        with session.begin():
            
            #pes = PresentationElements.objects.filter(id=presentation_id)
            pe = session.query(PresentationElementsSa).filter( id == presentation_id).first()
            
            #for pe in pes:
            linked_widget_id = pe.SrcWidgetId
            if form_type =='i':
                  #ToDo SqlAlchemy upgrade   DONE
                  #tiitrs=TaskInitiationInstanceToResource.objects.filter(TaskInitiateId_id = task_instance_id,PresentationElementId_id=linked_widget_id)
                  tpi = session.query(TaskInitiationInstanceToResourceSa)\
                            .filter(TaskInitiateId == task_instance_id, PresentationElementId ==linked_widget_id).first()
                  
                  #for tpi in tiitrs:
                  DatasetId = tpi.ResourceId
                  DatasetDefId = tpi.ResourceDefId_id
            
            elif form_type =='e':
                  #ToDo SqlAlchemy upgrade DONE
                  tiitrs=TaskExecutionInstanceToResource.objects.filter(TaskExecuteId_id = task_instance_id,PresentationElementId_id=linked_widget_id)
                  tpi = session.query(TaskExecutionInstanceToResourceSa)\
                            .filter(TaskExecuteId == task_instance_id, PresentationElementId == linked_widget_id).first()
                  
                  
                  #for tpi in tiitrs:
                  DatasetId = tpi.ResourceId
                  DatasetDefId = tpi.ResourceDefId_id
            
            if resource_id==DatasetDefId:
              filter=[DatasetId]
              
            else:
              #ToDo SqlAlchemy upgrade DONE
              #rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=DatasetDefId,Related_id=resource_id,Status='Active')
              rmd = session.query(ResourceModelDefinitionSa).filter(ResourceDefId==DatasetDefId, RelatedId==resource_id, Status=='Active').first()
             
              model_id=0
              #for rmd in rmds:
              model_id = rmd.id
              m2ms = mutant_get_m2m_data (DatasetDefId,resource_id,DatasetId,model_id,org_id,0)
              if m2ms is not None:
                for m2m in m2ms:
                  filter.append(m2m.ResourceRelatedId)
            
            #pdb.set_trace() #migor
            #queryset =  mutant_get_data_filtered_in(mutant_get_model(resource_id,org_id),filter) #migor
            queryset =  mutant_get_data_filtered_in(resource_id,org_id,filter)
            page = self.paginate_queryset(queryset)
            if page is not None:
              selectedRecords = GetDatasetDataFilteredPaginated(filter,user,resource_id,org_id,page,None,language)
            else:
              selectedRecords=[]
            
            if selectedRecords is not None:
              if page is not None:
                
                return self.get_paginated_response(selectedRecords)
              else:
                return Response(selectedRecords)
            else:
              return Response(status=404)
          
#not used        
def  PopulateFKColumns(item,resource_id,language):        

        rmd_items=[]
        user_items=[]
        group_items=[]
        
        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active',GenericType__in=['list','user','group'])
        for rmd in rmds:
          if rmd.GenericType=='list':
            rmd_items.append(rmd.CodeId)
          elif rmd.GenericType=='user':
            user_items.append(rmd.id)
          else:
            group_items.append(rmd.id)
        cds = CodesDetailLang.objects.filter(Lang=language,CodesDetailId__CodesHeadId_id__in=rmd_items).select_related()
        if len(user_items)>0:
          users = User.objects.all()
        if len(group_items)>0:
          groups = Group.objects.all()
        
    
        for rmd in rmds:
         field=rmd.FieldName
         if rmd.GenericType=='list':
           
           for cd in cds:
             value = item[field]
             if cd.CodesDetailId_id==value:
               
               item[field] = {'id':cd.CodesDetailId_id,'name':cd.Value}
               
         if len(user_items)>0:
           for user in users:
             value = item[field]
             if user.id==value:
               item[field]={'id':user.id,'name':user.first_name+' '+user.last_name+' - '+user.username}
         
         if len(group_items)>0:
           for group in groups:
             value = item[field]
             if group.id==value:
               item[field]={'id':group.id,'name':group.name}
        
        return item
#not used
def  PopulateFKColumnsFast(item,rmds,cds,users,groups):        
    
    for rmd in rmds:
        field=rmd.FieldName
        if field in item:
            if rmd.GenericType=='list':
                for cd in cds:
                    value = item[field]
                    if cd.CodesDetailId_id==value:
                        item[field] = {'id':cd.CodesDetailId_id,'name':cd.Value}
               
         
            for user in users:
                value = item[field]
                if user.id==value:
                    item[field]={'id':user.id,'name':user.first_name+' '+user.last_name+' - '+user.username}
             
             
            for group in groups:
                value = item[field]
                if group.id==value:
                    item[field]={'id':group.id,'name':group.name}
        
    return item

"""

class GetResourceColumnsViewSet(viewsets.GenericViewSet):
      queryset = ResourceDef.objects.all()
     
      def list(self, request,resourceId,resourceType,lang,ver):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        if 'resourceType' in self.kwargs:
            resource_type=self.kwargs['resourceType']
        else:
            resource_type = 'dataset'
            
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        if 'ver' in self.kwargs:
           version=int(self.kwargs['ver'])
        else:
           version = None

        #pdb.set_trace()#GetResourceColumnsViewSet #DBG
        user=request.user
        
        session = rdbms_cls.Session()
        with session.begin():
        
            org_id,language = helper.get_lang_org (user) 
            columnsDef=None
            #Create dataset object
            publish = helper.get_published_resource(resource_id)
            if publish is None:
               return Response(data={'msg':'Resource definition not found - viewsresources.py - GetResourceColumnsViewSet'},status=500) 
            resource_def_id = publish['ResourceDefId']
            if resource_type=='dataset':     
                dataset_obj = dataset(resource_def_id,resource_type,'','','','',user,language,org_id,{},version)
                dataset_obj.set_session(session)
                if dataset_obj is not None:

                    columnsDef,columnsForQuery = dataset_obj.get_columns_def(version)
            
            elif resource_type=='file':
                file_obj = file(resource_def_id,resource_type,'','','','',user,language,org_id,{},version)
                file_obj.set_session(session)
                if file_obj is not None:

                    columnsDef,columnsForQuery = file_obj.get_columns_def(version)
                    #columnsDef = file_obj.get_file_fields(columnsDef)
        

            if columnsDef is not None:
              return Response(data={'columnsDef':columnsDef,'columnsForQuery':columnsForQuery})
            else:
              return Response(data={'msg':'Columns not generated'},status=500)


"""
#not used - ToDo check
class GetResourceDataFilteredViewSet(viewsets.GenericViewSet):
      #queryset = ResourceDef.objects.all()
      session = rdbms_cls.Session()

     
      def create(self, request,resourceId,taskDefId,formType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        if 'taskDefId' in self.kwargs:
            task_def_id=int(self.kwargs['taskDefId'])
        else:
            task_def_id = 0
            
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        
         
        resourceData={}
        m2mData=[]
     
        datasetRecords = []
        selectedRecords = []
        relatedDict={}
        relatedDictIds={}
        querysetRelated = {}
        filter = request.data
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        #ToDo SqlAlchemy upgrade DONE       
        rdef = ResourceDef.objects.get(id=resource_id)
        
 
        with session.begin():
             (rdef , params) = session.query(ResourceDefSa, ResourceParamsSa)\
             .join(ResourceParamsSa, ResourceDefSa.ResourceType == ResourceParamsSa.id).where(ResourceDefSa.id == resource_id).first()
        
        #if rdef.ResourceType.ResourceCode == 'file':
        if params.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
            
        if selectedRecords is not None and columnsDef is not None:
          return Response({'data':selectedRecords,'columnsDef':columnsDef})
        else:
          return Response(status=404)

        selectedRecords = GetDatasetDataFiltered(filter,user,resource_id,org_id)
        
        columnsDef = get_columns_def(resource_id,task_def_id,form_type,org_id,language)
        
        #rdef = ResourceDef.objects.get(id=resource_id)
        with session.begin():
            (rdef , params) = session.query(ResourceDefSa, ResourceParamsSa)\
            .join(ResourceParamsSa, ResourceDefSa.ResourceType == ResourceParamsSa.id).where(ResourceDefSa.id == resource_id).first()
        
        #if rdef.ResourceType.ResourceCode == 'file':
        if params.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
            
        if selectedRecords is not None and columnsDef is not None:
          return Response({'data':selectedRecords,'columnsDef':columnsDef})
        else:
          return Response(status=404)

"""
class GetResourceDataDirectFilterViewSet(viewsets.GenericViewSet):
      #queryset = ResourceDef.objects.all()

     
      def create(self, request,resourceId,widgetId,formType,lang):
           
        resource_id=None
        widget_id = 0
        form_type = 'i'
        language = None
        search_filter = request.data
        
        if 'resourceId' in self.kwargs:     resource_id=int(self.kwargs['resourceId']) 
        if 'widgetId' in self.kwargs:       widget_id=int(self.kwargs['widgetId']) 
        if 'formType' in self.kwargs:       form_type=self.kwargs['formType']    
        if 'lang' in self.kwargs:           language=self.kwargs['lang']
          
        resourceData={}
        selectedRecords=[]
        columns=[]
        columns_for_select=[]
 

        idin = search_filter.get('id__in',None)
        only_columns = False
        if idin is not None and idin in [[{}],[[{}]]]:
            del search_filter['id__in']
            only_columns=True

        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        session = rdbms_cls.Session()
        with session.begin(): 
                   
            if widget_id>0:
                #ToDo SqlAlchemy upgrade DONE
                #p_element_langs = PresentationElementsLang.objects.filter(PresentationElementsId_id=widget_id,Lang=language)
        
                p_element_langs = session.query(PresentationElementsLangSa).where(PresentationElementsLangSa.PresentationElementsId==widget_id, PresentationElementsLangSa.Lang==language.id).first()
                #'PresentationElementsLangSa' object has no attribute 'count'
                if p_element_langs:
                    #p_element_lang = p_element_langs[0]
                    p_element_lang = p_element_langs #for sqlalchemy
                else:
                    return Response(data={'msg':'Widget definition not found for widget_id='+str(widget_id)},status=500)
                
                if p_element_lang.GridState is not None:
                    grid_state=json.loads(p_element_lang.GridState)

                    columns = grid_state.get('columnsDef',[])
                    columns_for_select = grid_state.get( 'columnsQuery',[])
            
            if only_columns:
                return Response({'data':selectedRecords,'columnsDef':columns,'columnsForQuery':columns_for_select})
                
            dataset_obj = dataset(resource_id,'dataset','','','','',user,language,org_id,{})
            dataset_obj.set_session(session)
            
            selectedRecords = dataset_obj.get_dataset_data_filtered(search_filter,columns_for_select,None,{})
            
            if selectedRecords is not None and columns is not None:
              return Response({'data':selectedRecords,'columnsDef':columns,'columnsForQuery':columns_for_select})
            else:
              return Response(status=500)
          
"""
#not used - ToDo check
class GetResourceDataRelatedViewSet(viewsets.GenericViewSet):
      queryset = ResourceDef.objects.all()

     
      def create(self, request,datasetId,recordId,relatedId,taskDefId,modelId,formType,lang):
        
        if 'datasetId' in self.kwargs:
            dataset_id=int(self.kwargs['datasetId'])
        else:
            dataset_id = None
            
        if 'recordId' in self.kwargs:
            record_id=int(self.kwargs['recordId'])
        else:
            record_id = None
            
        if 'relatedId' in self.kwargs:
            related_id=int(self.kwargs['relatedId'])
        else:
            related_id = None
        
        if 'taskDefId' in self.kwargs:
            task_def_id=int(self.kwargs['taskDefId'])
        else:
            task_def_id = 0
        
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
           
        if 'modelId' in self.kwargs:
            model_id=int(self.kwargs['modelId'])
        else:
            model_id = Npne
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        
        mainResource={}
        m2mData=[]
        m2mIds = []
        m2mFilter={}
        selectedRecords=[]
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
          
        #get related records
        m2mData = mutant_get_m2m_data(dataset_id,related_id,record_id,model_id,org_id,0)
        if m2mData is not None:
          for item in m2mData:
            m2mIds.append(item.ResourceRelatedId)
        
        #m2mFilter['ResourceSourceId']=record_id
        
        m2mFilter['id__in']= m2mIds
        
        #get related records
        queryset =  mutant_get_data_filtered(mutant_get_model(related_id,org_id),m2mFilter, parameters={})
        
        selectedRecords = GetDatasetDataFilteredPaginated(m2mFilter,user,related_id,org_id,None,queryset,language) 
        form_type='i'
        columnsDef = get_columns_def(related_id,task_def_id,form_type,org_id,language)
        
        rdef = ResourceDef.objects.get(id=resource_id)
        if rdef.ResourceType.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
            
            
        if selectedRecords is not None and columnsDef is not None:
          return Response({'data':selectedRecords,'columnsDef':columnsDef})
        else:
          return Response(status=404)
       
    
#not used - ToDo check
class GetResourceDataWithRelatedViewSet(viewsets.GenericViewSet):
      queryset = ResourceDef.objects.all()

     
      def create(self, request,datasetId,relatedId,recordId,taskDefId,modelId,formType,lang):
        
        if 'datasetId' in self.kwargs:
            dataset_id=int(self.kwargs['datasetId'])
        else:
            dataset_id = None
            
        if 'recordId' in self.kwargs:
            record_id=int(self.kwargs['recordId'])
        else:
            record_id = None
            
        if 'relatedId' in self.kwargs:
            related_id=int(self.kwargs['relatedId'])
        else:
            related_id = None
        if 'taskDefId' in self.kwargs:
            task_def_id=int(self.kwargs['taskDefId'])
        else:
            task_def_id = 0
        
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
           
        if 'modelId' in self.kwargs:
            model_id=int(self.kwargs['modelId'])
        else:
            model_id = 0
            
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        
        mainResource={}
        m2mData=[]
        m2mIds = []
        selectedRecords=[]
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
        #get related records
        
        m2mData = mutant_get_m2m_data(dataset_id,related_id,record_id,model_id,org_id,0)
        if m2mData is not None:
          for item in m2mData:
            m2mIds.append(item.ResourceRelatedId)
        
        #get all records
        queryset =  mutant_get_data_related(mutant_get_model(related_id,org_id))
        for item in queryset:
          itemDict = model_to_dict(item)
          selectedRecords.append(itemDict)
        
        columnsDef = get_columns_def(related_id,task_def_id,form_type,org_id,language)
        
        rdef = ResourceDef.objects.get(id=resource_id)
        if rdef.ResourceType.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
            
        if queryset is not None and columnsDef is not None:
          return Response({'data':selectedRecords,'columnsDef':columnsDef,'m2m':m2mIds})
        else:
          return Response(status=404)


"""
class GetResourceDataM2mViewSet(viewsets.GenericViewSet):
      queryset = ResourceDef.objects.all()

     
      def create(self, request,widgetId,datasetId,relatedId,recordId):
        
        if 'widgetId' in self.kwargs:
            widget_id=int(self.kwargs['widgetId'])
        else:
            widget_id = None
        
        if 'datasetId' in self.kwargs:
            dataset_id=int(self.kwargs['datasetId'])
        else:
            dataset_id = None
            
        if 'recordId' in self.kwargs:
            record_id=int(self.kwargs['recordId'])
        else:
            record_id = None
            
        if 'relatedId' in self.kwargs:
            related_id=int(self.kwargs['relatedId'])
        else:
            related_id = None
        
        
        search_filter = request.data.get('filter_data',{})
        form_data = request.data.get('form_data',[])
        mainResource={}
        m2mData=[]
        m2mIds = []
        selectedRecords=[]
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user)
        rdbms_cls = RdbmsClass()
        session = rdbms_cls.Session()
        with session.begin():
            if widget_id>0:
                    #ToDo SqlAlchemy upgrade DONE
 
                    #p_element_langs = PresentationElementsLang.objects.filter(PresentationElementsId_id=widget_id,Lang=language)
                    p_element_langs = session.query(PresentationElementsLangSa).filter(PresentationElementsLangSa.PresentationElementsId==widget_id,PresentationElementsLangSa.Lang==language.id).all()
                    if len(p_element_langs)>0:
                        p_element_lang = p_element_langs[0]
                    else:
                        return Response(data={'msg':'Widget definition not found for widget_id='+str(widget_id)},status=500)
                
                    if p_element_lang.GridState is not None:
                        grid_state=json.loads(p_element_lang.GridState)

                        columns = grid_state.get('columnsDef',[])
                        columns_for_select = grid_state.get( 'columnsQuery',[])
            
            dataset_obj = dataset(dataset_id,'dataset','','','','',user,language,org_id,{})
            dataset_obj.set_session(session)
            
            #process search
            rData,tmp_filter,select_related, parameters = dataset_obj.extract_form_data_search(search_filter,form_data)          
            finalFilter,m2mData = dataset_obj.process_resource_data_search(rData)
            selectedRecords = dataset_obj.get_dataset_m2m_filter(related_id,record_id,finalFilter)  
            if selectedRecords is not None and columns is not None:
              return Response({'data':selectedRecords,'columnsDef':columns,'columnsForQuery':columns_for_select})
            else:
              return Response(status=500)
       
            if queryset is not None and columnsDef is not None:
              return Response({'data':selectedRecords,'columnsDef':columnsDef,'m2m':m2mIds})
            else:
              return Response(status=404)

#not used
"""
class SaveResourceFormOldViewSet(viewsets.GenericViewSet):
   queryset = ResourceDefLang.objects.all()#.select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))

  
   #def get_serializer_class(request):
        #return AddUpdateResourceFormSerializer()
   def get_serializer_context(self):
    
       context = super(SaveResourceFormViewSet, self).get_serializer_context()
        
       return context
       #return {'resourceId': self.resourceId}
       #return {'request': self.request}
        
   
    
   def create(self, request,formType,resourceId,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
       
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = None
           
        #if 'taskDefId' in self.kwargs:
           #task_def_id=int(self.kwargs['taskDefId'])
        #else:
           #task_def_id = None
        #if task_def_id == 0:
          #task_def_id = None
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        
        #Check if admin
        
        user = request.user
        
        isAdmin = IsUserResourceAdmin(resource_id,user)
        if not isAdmin:
            return Response(data='You are not authorised to change this resource !',status=404)
        
        resourceModelIds = []
        datasetWidgets = []
        widgetLinks = []
        vData = []
        count_changes = 0
        new_items = {}
        
          #get version
        version = None
        new_version = 1
        
        version_max = PresentationElements.objects.filter(ResourceDefId=resource_id,FormType=form_type,ResourceType='o',Status='Active').aggregate(Max('VersionTo'))
        
        version_max = session.query(func.max(PresentationElementsSa.VersionTo).label('VersionTo__max')).filter(ResourceDefId==resource_id,FormType==form_type,ResourceType=='o', Status='Active').first()
 
        if version_max is not None:
            if 'VersionTo__max' in version_max:
               version = version_max['VersionTo__max']
        
        if version is not None:
            new_version = version + 1
            
        count=0
        
        #pdb.set_trace()  #SaveResourceFormViewSet
        
        
        org_id,language = helper.get_lang_org (user) 
         
        #for row in request.data:
        
         # count = count+1
        serializer = SaveResourceFormSerializer(data=request.data,many=True,context={'resourceId': resource_id,'formType':form_type,'user':user,'lang':language,'version':version,'org_id':org_id,'new_version':new_version})
        
        if serializer.is_valid(raise_exception=True):
            rVal = serializer.save()
            
            for item in rVal:
              
              for rrmi in item['resourceModelIds']:
                resourceModelIds.append(rrmi)
              for dw in item['datasetWidgets']:
                datasetWidgets.append (dw)
              for wl in item['widgetLinks']:
                widgetLinks.append (wl)
              
              count_changes+=item['count_changes']
              vData.append(item['vData'])
              #new_items.update(item['new_items'])
        #delete the ones that do not exist any more
        #ResourceModelDef.objects.filter(ResourceDefId_id=resource_id).exclude(id__in=resourceModelIds).delete()
        
        #process widget links
        #for wl in widgetLinks:
          
        for row in vData:

          if 'layout' in row:
            for widget in row['layout']:
              if 'layout' in widget:
                for layout in widget['layout']:
                  if layout['ElementType'] in ('files','uploader','view','update','rlist','mupdate','add','madd'):
                    #Element.GridState  must be merged in Parameters 
                    if layout['ElementType'] == "rlist":
                        layout['Parameters']['GridState']=layout['GridState']
                        
                    if 'tItemValue' in layout:
                      tItemValue = layout['tItemValue']
                      if tItemValue is not None:
                        for tItem in tItemValue:
                          if tItem is not None:
                            
                            for dw in datasetWidgets:
                              if dw is not None:
                                
                                if 'localId' in dw and 'localWidgetId' in tItem:
                                  if tItem['localWidgetId']==dw['localId']:
                                    
                                    tItem['widgetId'] = dw['PresentationId']
                                      
                              
                            if 'DEI' in tItem:  
                                for dei in tItem['DEI']:
                                
                                    if tItem['widgetId'] is not None:
                                    
                                        if 'params' in dei:
                                            params = dei['params']
                                        else:
                                            params=None
                                  
                                 
                                    tdwds = TaskDefWidgetDependencies.objects.filter(FieldName=dei['name'],PresentationId_id=layout['PresentationId'],InterfacePresentationId_id=tItem['widgetId'],
                                                        TaskDefId_id=resource_id,FormType=form_type)
                                    for tdwd in tdwds:
                                        if tdwd.Status!=dei['value']:
                                            tdwd.ChangeDateTime = datetime.now(timezone.utc)
                                            tdwd.ChangedBy = request.user
                                            tdwd.Status=dei['value']
                                            tdwd.Params = params
                                            tdwd.save()
                                            count_changes+=1
                                    widgetObject=None
                                    if len(tdwds)==0:
                                        widgetObject = {'FieldName':dei['name'],'PresentationId_id':layout['PresentationId'],'InterfacePresentationId_id':tItem['widgetId'],
                                                        'TaskDefId_id':resource_id,'FormType':form_type,'Status':dei['value'],'Params':params,'CreatedDateTime':datetime.now(timezone.utc),'CreatedBy':request.user}
                                    if widgetObject is not None:                    
                                        tdwdNew = TaskDefWidgetDependencies.objects.create(**widgetObject)
                                        if tdwdNew:
                                            count_changes+=1 
                            
                             
                              
                              
        strJson=json.dumps(vData)
              
        #for item in vData:
        formCreated = False
        
        if len(vData[0]['layout'])>0 :
          formCreated = True
        
        updForm = ResourceDefLang.objects.filter(ResourceDefId=resource_id,Lang=language)
             
        if form_type =='e':
             for rdl in updForm:
                rdl.PresentationDefExe=strJson
                rdl.ExecuteForm=formCreated
                rdl.save()
              
        else:
             for rdl in updForm:
                rdl.PresentationDef=strJson
                rdl.InitiateForm=formCreated
                rdl.save()
                updRDef = ResourceDef.objects.filter(id=resource_id).update(FormDefined=formCreated) 
                rdefSA = session.update(ResourceDefSa).where(ResourceDefSa.id ==resource_id).values(Parameters=params)
 
        
        #PresentationElements.objects.filter(ResourceDefId=resource_id,FormType=form_type,ResourceType='o').exclude(ElementType='row').exclude(UpdateTransaction=True).update(Status='NotActive')
        message='Form saved successfuly!'
        msg_type='success'
        update_form=True
        if count_changes==0:
            message='No changes were made to  form!'
            msg_type='info'
            update_form=False
            new_version = version
            
        return Response({'formData':vData,'version':new_version,'msg':message,'updateForm':update_form,'msgType':msg_type,'new_items':new_items})
  

  
   def update(self, request,formType,resourceId):
      
      
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None
           
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = None
        
        
        strJson=json.dumps(request.data)
        
        ResourceDefLang.objects.filter(ResourceDefId=resource_id).update(PresentationDef=strJson)
          
        #for pe in pe_set:
            #pe.PresentationDef = strJson
            #pe.save()
        return Response(request.data)
"""
class SaveResourceFormViewSet(viewsets.GenericViewSet):
   
   def create(self, request,formType,resourceId,lang):
               
        resource_id = None    
        form_type = None    
        language = None   
       
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']         
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
   
        new_items=[]
      
        user = request.user
        if form_type not in ['i','e']:
            form_type='i'
        session = rdbms_cls.Session()
        with session.begin():
          
            isAdmin = helper.IsUserResourceAdmin(resource_id,user.id,session)  #Check if admin
            if not isAdmin:
                return Response(data='You are not authorised to change this resource !',status=404)
            
            version = None
            new_version = 1
            #ToDo SqlAlchemy upgrade DONE
            #version_max = PresentationElements.objects.filter(ResourceDefId=resource_id,FormType=form_type,ResourceType='o').aggregate(Max('VersionTo'))
            
            version_max = session.query(func.max(PresentationElementsSa.VersionTo)  )\
            .filter(PresentationElementsSa.ResourceDefId == resource_id, PresentationElementsSa.FormType == form_type, PresentationElementsSa.ResourceType=='o').first()

            if version_max is not None:
                #if 'VersionTo__max' in version_max:
                version = version_max[0]
            
            if version is not None:
                new_version = version + 1
                
            count=0
                  
            org_id,language = helper.get_lang_org (user)        
            
            #create class here
            resource_object = None
            #rdef = ResourceDef.objects.get(id=resource_id)
            rdef, param = session.query(ResourceDefSa, ResourceParamsSa)\
            .join(ResourceParamsSa, ResourceDefSa.ResourceType == ResourceParamsSa.id)\
            .filter(ResourceDefSa.id == resource_id).first()
            
            resource_type = 'dataset'
            if rdef is not None:
                resource_type=param.ResourceCode
                try:
                     module_path = 'katalyonode.functions.resources.'+resource_type+'def'
                     #pdb.set_trace()
                     module_ref = importlib.import_module(module_path)
                     resource_class = getattr(module_ref, resource_type.lower())
                    
                     resource_object = resource_class(resource_id,resource_type,'','','','',user,language,org_id,{},None)
                     resource_object.set_session(session)
                     
                     if resource_object is not None:
                         resource_object.get_definition()
                         resource_object.set_form_type(form_type)
                         resource_object.set_current_version(version) 
                         resource_object.set_new_version(new_version) 
                         resource_object.save_form_definition(request.data)
                except ModuleNotFoundError:
                        raise ValidationError("ModuleNotFoundError --> " + module_path)

                message='Form saved successfuly!'
                msg_type='success'
                update_form=True
                if resource_object.CountChanges>0:
                    #pdb.set_trace()
                    resource_object.update_form_version()
                    if resource_object.GenerateCss:
                        resource_object.create_resource_css()
                    new_form = resource_object.get_saved_form()
                    changes=True
                else:
                    message='No changes were made to  form!'
                    msg_type='info'
                    update_form=False
                    new_version = version
                    new_form = []
                    changes=False
                
                return Response({'formData':new_form,'version':new_version,'msg':message,'msgType':msg_type,'new_items':new_items,'has_changes':changes})

class SendNotificationViewSet(viewsets.GenericViewSet):
    queryset = ResourceDefLang.objects.all()#.select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))

    def create(self, request,resourceId):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
       
        #Check if admin
        
        user = request.user
        
        notification = request.data
        
        
        org_id,language = helper.get_lang_org (user) 
         
        
        snd,msg = SendNotification(notification,user)  

        #save resource parameters
        if snd:
            return Response(data=msg,status=200)
        else:
            return Response(data=msg,status=500)

class SaveResourceDefParametersViewSet(viewsets.GenericViewSet):
    queryset = ResourceDefLang.objects.all()#.select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))

    def create(self, request,resourceId):
        
        user = request.user
        resource_params = request.data
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
       
        
        isAdmin =  helper.IsUserResourceAdmin(resource_id,user.id,session)
        if not isAdmin:
            return Response(data='You are not authorised to change this resource !',status=404)
        
        org_id,language = helper.get_lang_org (user) 
           
        #ToDo SqlAlchemy upgrade DONE
        session = rdbms_cls.Session()
         
        #upd_count=ResourceDef.objects.filter(id=resource_id).update(Parameters=resource_params,ChangedBy=user,ChangedDateTime=datetime.now(timezone.utc))    
        
        upd_count = session.update(ResourceDefSa).where(ResourceDefSa.id == resource_id).values(Parameters=resource_params,ChangedBy=user,ChangedDateTime=datetime.now(timezone.utc))
    
 
 
        if upd_count.count()==1:
            return Response(data='Resource params saved successfuly !',status=200)
        else:
            return Response(data='Error updating parametrs !',status=500)


class GetFormFieldsByTypeViewSet(viewsets.GenericViewSet):
   queryset = PresentationElements.objects.all()
  
   def list(self, request,resourceId,relatedId,elementType,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        
        if 'relatedId' in self.kwargs:
           related_id=int(self.kwargs['relatedId'])
        else:
           related_id = None
           
        if 'elementType' in self.kwargs:
           element_type=self.kwargs['elementType']
        else:
           element_type = None
     
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
           
        #search = request.data['search']
        user = request.user
        
        org_id,language = helper.get_lang_org (user)
        #ToDo SqlAlchemy upgrade DONE
        #peFields = PresentationElements.objects.filter(ResourceDefId=related_id,Related_id=resource_id,ElementType=element_type,ResourceType='o',Status='Active',ResourceModelDefinitionId_id__gt=0,ResourceModelDefinitionId__Status='Active').prefetch_related(Prefetch('presentationelementslang_set',
        #                                                  queryset=PresentationElementsLang.objects.filter(Lang=language))).order_by('presentationelementslang__Label')
        
        peFields = session.query(PresentationElementsSa,PresentationElementsLangSa)\
            .join(ResourceModelDefinitionSa, ResourceModelDefinitionSa.id == PresentationElementsSa.ResourceModelDefinitionId)\
            .join(PresentationElementsLangSa, PresentationElementsLangSa.PresentationElementsId == PresentationElementsSa.id)\
            .filter(PresentationElementsLangSa.Lang == language).order_by(PresentationElementsLangSa.Label)
 
        
        peElements = []
        for (pe, pelang) in peFields:
          peElements.append({'id':pe.ResourceModelDefinitionId,'name':pelang.Label})
          
        return Response (peElements)

class GetResourceModelForDeploymentViewSet(viewsets.GenericViewSet):
   
   queryset = PresentationElements.objects.all()
  
   def list(self, request,resourceId,version,lang):
   
        resource_id = version = None
        language = None
        
        if 'resourceId' in self.kwargs: resource_id=int(self.kwargs['resourceId'])
        if 'version' in self.kwargs:    version=int(self.kwargs['version'])
        if 'lang' in self.kwargs:       language=self.kwargs['lang']
        
        user = request.user
        session= rdbms_cls.Session()
        with session.begin():
           
            org_id,language = helper.get_lang_org (user)
            #ToDo SqlAlchemy upgrade DONE
            #peFields = PresentationElements.objects.filter ( 
            #        VersionFrom__lte=version,
            #        VersionTo__gte=version,
            #        ResourceModelDefinitionId__ResourceDefId_id=resource_id,
            #        Status='Active',
            #        ResourceModelDefinitionId_id__gt=0,
            #        ResourceModelDefinitionId_id__isnull=False).exclude(ElementType='resourceid').exclude(ResourceModelDefinitionId__Status='NotActive').exclude(ResourceModelDefinitionId__DeployAction='N')\
            #        .prefetch_related(Prefetch('presentationelementslang_set',queryset=PresentationElementsLang.objects.filter(Lang=language)) ).order_by('presentationelementslang__Label').order_by('id')
           
            peFields = session.query(PresentationElementsSa,PresentationElementsLangSa , ResourceModelDefinitionSa)\
                .join(ResourceModelDefinitionSa, ResourceModelDefinitionSa.id == PresentationElementsSa.ResourceModelDefinitionId)\
                .join(PresentationElementsLangSa, PresentationElementsLangSa.PresentationElementsId == PresentationElementsSa.id)\
                .filter( PresentationElementsSa.ResourceDefId == resource_id,
                    PresentationElementsSa.VersionFrom <= version,
                    PresentationElementsSa.VersionTo >= version,
                    PresentationElementsSa.Status == 'Active',
                    PresentationElementsSa.ResourceModelDefinitionId > 0, 
                    PresentationElementsSa.ElementType != 'resourceid',
                    ResourceModelDefinitionSa.DeployAction !='N',
                    ResourceModelDefinitionSa.Status != 'NotActive',
                    PresentationElementsLangSa.Lang == language.id)
            """                   
                    .order_by(PresentationElementsLangSa.Label,PresentationElementsLangSa.id)
                    #.prefetch_related(Prefetch('presentationelementslang_set',queryset=PresentationElementsLang.objects.filter(Lang=language)) ).order_by('presentationelementslang__Label').order_by('id')
            """  
            peElements = []
            #pdb.set_trace() #GetResourceModelForDeploymentViewSet
            
            for (pe, presentationElementsLangPrefetched,  resmodeldef) in peFields.all():
                #presentationElementsLangPrefetched = getattr(pe,'_prefetched_objects_cache',None)
                if presentationElementsLangPrefetched is not None: 
                    #if 'presentationelementslang_set' in presentationElementsLangPrefetched:
                        PeLang=presentationElementsLangPrefetched
                        #PeLang = presentationElementsLangPrefetched['presentationelementslang_set']
                        if PeLang and pe.ResourceModelDefinitionId is not None:
                            peElements.append({'id':resmodeldef.id,'dbtype':resmodeldef.Dbtype,'field':resmodeldef.FieldName,'deployed':resmodeldef.Deployed,'version':resmodeldef.Version,'status':resmodeldef.Status,'deployAction':resmodeldef.DeployAction,
                                'deployedVersion':resmodeldef.DeployedVersion,'deployDate':resmodeldef.DeployDate,'required':resmodeldef.RequiredField,'maxLength':resmodeldef.MaxLength,'label':PeLang.Label})
                        else:
                            peElements.append({'id':str(pe.id)+' Error: resource model definition or language record is missing','dbtype':None,'field':None,'deployed':None,'version':None,'status':None,'deployAction':None,
                                'deployedVersion':None,'deployDate':None,'required':None,'maxLength':None,'label':None})
                        
            return Response (peElements)

class GetFormFieldsViewSet(viewsets.GenericViewSet):
   #queryset = PresentationElements.objects.all()
  
   def list(self, request,resourceId,lang,ver):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
     
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        if 'ver' in self.kwargs:
            version = int(self.kwargs['ver'])
        else:
            version = 0
        #search = request.data['search']
        
        user = request.user
        
        org_id,language = helper.get_lang_org (user)
        session = rdbms_cls.Session()
        with session.begin():
            dataset_object = dataset(resource_id,'dataset','','','','',user,language,org_id,{})
            dataset_object.set_session(session)
            peElements = dataset_object.get_form_fields(version)

        return Response (peElements)
       
#not used - ToDo check    
"""    
class GetResourceDataListViewSet(viewsets.GenericViewSet):
    
    pagination_class = StandardResultsSetPagination
    queryset = PresentationElements.objects.all()
  
    def create(self, request,resourceId,presentationId,lang):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        
        if 'presentationId' in self.kwargs:
           presentation_id=int(self.kwargs['presentationId'])
        else:
           presentation_id = None
     
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
        
        rmds = []
        pids = []
        rval = []
        filter = {}
        fields=None
        users = None
        groups = None
        codes = None
        isIdIn = False
        resourceData={}
        m2mData=[]
        
        if 'filter' in request.data:
            filter_arr = request.data['filter']
        else:
            filter_arr = None
        
        if 'form' in request.data:    
            form = request.data['form']
        else:
            form=None
        
            
        if filter_arr is None or filter_arr==[] or filter_arr=={} or filter_arr=="":
          filter_arr = [{}]
        
        if type(filter) is dict:
            filter = filter_arr
        elif type(filter) is list: 
            if len(filter_arr)>0:
                filter=filter_arr[0];
            else:
                return Response(data='Invalid search form --> Filter not found',status=500)
        
        #ErrorLog.objects.create(Error1={'filter_arr':filter_arr},Error2={'filter':filter},CreatedDateTime=datetime.now(timezone.utc))
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
         
        
        if form is None:
            form = []
        
        #ErrorLog.objects.create(Error1={'Start':"miro"},Error2={},CreatedDateTime=datetime.now(timezone.utc)) 
    
        
        rData,f,select_related, parameters = ExtractFormDataSearch(filter,form,user)
        filter = f
        
        resourceData,m2mData = ProcessResourceDataSearch(rData,resource_id,org_id)
        
        queryset =  mutant_get_data_filtered(resource_id,org_id,resourceData,parameters={})
        
        nameDefs = PresentationElements.objects.get(id=presentation_id)
        nameField = nameDefs.NameField
        
        #ErrorLog.objects.create(Error1={'Start1':"Nakon nameField"},Error2={'nameField':nameField},CreatedDateTime=datetime.now(timezone.utc)) 
    
        fields=None
        name_fields=None
        if nameField is not None:
          for element in nameField:
            pids.append(element['id']) 
          
          fields = PresentationElements.objects.filter(id__in=pids)   
          fields = session.query(PresentationElementsSa).filter( id.in_(pids))
        
        if fields is not None:
            name_fields={}
            for fld in fields:
                column = fld.ResourceModelDefinitionId.FieldName
                name_fields[column]=column
        
        #page = self.paginate_queryset(queryset)
        
        #page = queryset
        
        
        selectedRecords = GetDatasetDataFilteredPaginatedFast(filter,user,resource_id,org_id,None,queryset,language,name_fields) 
        
        if selectedRecords is not None:
            return Response(selectedRecords)
        else:
            return Response(status=404)

#not used - ToDo check
def getResourceDataListAll(resource_id,presentation_id,org_id):
        fields   = name_fields = None 
        pids = []
        filter = {}
 
        nameDefs = PresentationElements.objects.get(id=presentation_id)
        nameField = nameDefs.NameField
        
        if nameField is not None:
            for element in nameField:
                pids.append(element['id']) 
            fields = PresentationElements.objects.filter(id__in=pids)   
            fields = session.query(PresentationElementsSa).filter( id.in_(pids))
            
     
        concat_name_fields = list()
        if fields is not None:
            name_fields={}
            for fld in fields:
                column = fld.ResourceModelDefinitionId.FieldName
                if fld.ElementType=='resourceid':
                    column = 'resource'+str(resource_id)+'_'+str(org_id)+'_id' 
                concat_name_fields.append(column)
                name_fields[column]=column
        
        selectedRecords =  mutant_get_data_sa(resource_id,org_id,concat_name_fields)
        return selectedRecords
   
  
"""

@method_decorator(transaction.non_atomic_requests, name='dispatch')
class GetResourceDataListAllViewSet(viewsets.GenericViewSet):
    
    #pagination_class = StandardResultsSetPagination
    queryset = PresentationElements.objects.all()
  
    def list(self, request,resourceId,presentationId,lang):
              
        resource_id = presentation_id = fields  = org_id  = name_fields = None 
        pids = []
        filter = {}
        language = None
        
        if 'resourceId' in self.kwargs:        resource_id=int(self.kwargs['resourceId'])
        if 'presentationId' in self.kwargs:    presentation_id=int(self.kwargs['presentationId'])
        if 'lang' in self.kwargs:              language=self.kwargs['lang']
 
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        session = rdbms_cls.Session()
         
        nameDefs = PresentationElements.objects.get(id=presentation_id)
        nameField = nameDefs.NameField
        
        with session.begin():
        
            if nameField is not None:
                for element in nameField:
                    pids.append(element['id']) 
                #fields = PresentationElements.objects.filter(id__in=pids) 
                fields = session.query(PresentationElementsSa).filter( id.in_(pids))
                  
                            
            concat_name_fields = list()
            if fields is not None:
                name_fields={}
                for fld in fields:
                    column = fld.ResourceModelDefinitionId.FieldName
                    if fld.ElementType=='resourceid':
                        column = 'resource'+str(resource_id)+'_'+str(org_id)+'_id' 
                    concat_name_fields.append(column)
                    name_fields[column]=column
            
            selectedRecords =  mutant_get_data_sa(resource_id,org_id,concat_name_fields)
           
            if selectedRecords is not None:
                return Response(selectedRecords)
            else:
                return Response(status=404)

        
class ResourceDefViewSet(viewsets.ModelViewSet):
    serializer_class = ResourceDefSerializer
    queryset = ResourceDef.objects.all().select_related()
    
class ResourceDefLangViewSet(viewsets.ModelViewSet):
    serializer_class = ResourceDefLangSerializer
    queryset = ResourceDefLang.objects.select_related().all()
    
    
class ResourceDefByLangViewSet(viewsets.ReadOnlyModelViewSet):
    #serializer_class = ResourceDefByLangSerializer
    session = rdbms_cls.Session()
    
    
    #def get_queryset(self):
    def list(self, request, lang):  
      
      
      language= self.kwargs['lang']
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 
      
      with session.begin():

          if lang is not None:
            #queryset = ResourceDef.objects.prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang=language))) #resourcedeflang__Lang=lang)
            queryset = session.query(ResourceDefSa).join(ResourceDefSa, ResourceDefLangSa.ResourceDefLangSa == ResourceDefSa.id)\
            .filter(ResourceDefLangSa.Lang == language).all()
           
          else:
            #queryset = ResourceDef.objects.all()
            queryset = session.query(ResourceDefSa).all()
            #query_set = ResourceDefLang.objects.filter(resourcedeflang__Lang=lang).select_related()
          
          return queryset
   
    
class ResourceDefAllViewSetNew(viewsets.ModelViewSet):
    #serializer_class = ResourceDefAllSerializer
    queryset = ResourceDef.objects.select_related().all()

    def list(self, request):
        
        if 'pk' in self.kwargs:
           resource_id=self.kwargs['pk']
        else:
           resource_id = None

        user = request.user
        
        org_id,language = helper.get_lang_org (user)
        session = rdbms_cls.Session()
        with session.begin():
            if resource_type_code is not None:
                    try:
                        module_path = 'katalyonode.functions.resources.'+resource_type_code+'def' 
                        module_ref = importlib.import_module(module_path)
                        resource_class = getattr(module_ref, resource_type_code)
                        resource_object = resource_class(None, resource_type_code,'','','','',user,language,org_id,resource_params,None)
                        resource_object = set_session(session)
                        
                        if resource_object is not None:
                                resource_object.get_definition()

                    except ModuleNotFoundError:
                        raise ValidationError("ModuleNotFoundError --> " + module_path)

                    response_json = resource_object.get_definition_json()
                    return Response(data=response_json,status=200)
            else:
               return Response(data='Resource type ' + resource_type_code + 'not found',status=404)
         


class ResourceDefAllViewSet(viewsets.GenericViewSet):
    #serializer_class = ResourceDefAllSerializer
    #queryset = ResourceDef.objects.select_related().all()

    def list(self, request,resourceId):
            
        resource_id = None
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])

        user = request.user
        
        org_id,language = helper.get_lang_org (user)
        #pdb.set_trace()

        resource_type_code,status_id = helper.get_resource_type(resource_id)
        session = rdbms_cls.Session()
        resource_object = None
        with session.begin():

            if resource_type_code is not None:
                try:

                        module_path = 'katalyonode.functions.resources.'+resource_type_code+'def' 
                        module_ref = importlib.import_module(module_path)
                        resource_class = getattr(module_ref, resource_type_code)
                        resource_object = resource_class(resource_id,resource_type_code,'','','','',user,language,org_id,{},None) #TODO - 'None,None' must be replaced
                        resource_object.set_session(session)
                        
                        if resource_object is not None:
                            
                                resource_object.get_definition()

                except ModuleNotFoundError:
                    raise Exception("ModuleNotFoundError --> " + module_path)
            else:
                Response(data='Resource parameters record not found!',status=500)
            response_json = {}
            if resource_object is not None:
                response_json = resource_object.get_definition_json()
            else:
                Response(data='Resource definition does not exist!',status=500)
            return Response(data=response_json,status=200)

        #codeItem = {'id':code[0].id,'Active':code[0].Active,'UserGeneratedId':code[0].UserGeneratedId,'Description':code[1].Description,'CodeType':code_type,'CodeType.name':code_type_name,'CodeName':code[1].CodeName,'CodeHeadName':code[0].CodeHeadName} 
        #return Response(data=codeItem)
    
#@method_decorator(transaction.non_atomic_requests, name='dispatch') 
class SyncdbMutantResourceForm(viewsets.GenericViewSet):
    serializer_class = GetResourceDefSerializer
    queryset = ResourceDef
    
    #def get_queryset(self):
       
     # resourceId= self.kwargs['resourceDefId']
      #if resourceId is not None:
       # queryset = ResourceModelDef.objects.filter(ResourceDefId=resourceId)
      
     # return queryset
    

     # some fields (eg GeoDjango) require additional SQL to be executed

    def list(self, request,resourceId):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None
        
        return Response(status=200)
        
    def create(self, request,resourceId,version,resource_type):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None
                
        if 'version' in self.kwargs:
           version=self.kwargs['version']
        else:
           version = None
       
        user = request.user
        
        org_id,language = helper.get_lang_org (user) 
         
        
        
        #alchemy_table = create_alchemy_table(resource_id,version,org_id,user)
        #pdb.set_trace()
        create_mutant_table(resource_id,user,org_id,'resource'+resource_id,version,'katalyonode',False)
        #create history table
        create_mutant_table(resource_id,user,org_id,'resource'+resource_id,version,'katalyonode',True)
        if (resource_type=='File'):
            mutant_add_file_fields (resource_id,org_id,'katalyonode')
            mutant_add_file_fields (resource_id,org_id,'katalyonode',True)
            
        return Response(status=200)

class SyncdbResourceForm(viewsets.GenericViewSet):
    serializer_class = GetResourceDefSerializer
    queryset = ResourceDef
    
    #def get_queryset(self):
       
     # resourceId= self.kwargs['resourceDefId']
      #if resourceId is not None:
       # queryset = ResourceModelDef.objects.filter(ResourceDefId=resourceId)
      
     # return queryset
    

     # some fields (eg GeoDjango) require additional SQL to be executed

    def list(self, request,resourceId):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None
        
        return Response(status=200)
        
    def create(self, request,resourceId,version):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None
                
        if 'version' in self.kwargs:
           version=self.kwargs['version']
        else:
           version = None
       
        user = request.user
        language=None
        
        org_id,language = helper.get_lang_org (user) 
         
        #org_id=1
        
        #define a resource class
        rdef = ResourceDef.objects.get(id=resource_id)
        resource_type = None
        if rdef is not None:
            resource_type=rdef.ResourceType.ResourceCode
        if resource_type is None:
            return Response(status=400)
        try:
          with session.begin():
        
            module_path = 'katalyonode.functions.resources.'+resource_type+'def'
            #pdb.set_trace()
            module_ref = importlib.import_module(module_path)
            resource_class = getattr(module_ref, resource_type)
         
            resource_object = resource_class(resource_id,resource_type,'','','','',user,language,org_id,{},None)
            resource_object.set_session(session)
            if resource_object is not None:
                if not resource_object.SyncDb:
                    return Response(status=201)
                #sync table
                tbl=resource_object.sync_alchemy_table(version,False)
                #sync history table
                tbl_h=resource_object.sync_alchemy_table(version,True)
        except ModuleNotFoundError:
            raise ValidationError("ModuleNotFoundError --> " + module_path)
  
        return Response(status=200)


class SaveResourceDataViewSet(viewsets.ModelViewSet):# !!! TODO - IZGLEDA DA SE OVO VISE NE KORISTI ???
    
    def get_queryset(self):
             
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        user=self.request.user
        
        org_id,language = helper.get_lang_org (user) 
         
          
        model = mutant_get_model(resource_id,org_id).model_class()
        return model.objects.all()           

    def get_serializer_class(self):
        
        if 'resourceId' in self.kwargs:
          resource_id=int(self.kwargs['resourceId'])
        else:
          resource_id = None
         
        user=self.request.user
        
        org_id,language = helper.get_lang_org (user) 
         
          
        MutantModelSerializer.Meta.model = mutant_get_model(resource_id,org_id).model_class()
      
        return MutantModelSerializer

#not used
"""
class SaveResourceDataNewViewSet(viewsets.GenericViewSet):
    
    #serializer_class = MutantModelSerializer
    #queryset = User.objects.all()
    
    def create(self, request,resourceId):
      mainResource={}
      m2mData=[]
      org_id=None
      #pdb.set_trace() #DBG #SaveResourceDataNewViewSet
      
      if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
      else:
          return Response(data={'msg':'Invalid request - parameter resourceId is missing'},status=400)
      
      user=request.user
      
      org_id,language = helper.get_lang_org (user) 
       
          
      model = mutant_get_model(resource_id,org_id).model_class()
      form_type='i'
      pes = PresentationElements.objects.filter(ResourceDefId_id=resource_id,FormType=form_type,OverrideType='n',Status='Active',ResourceModelDefinitionId__ResourceDefId_id=resource_id,ResourceModelDefinitionId__Status='Active').select_related('ResourceModelDefinitionId').prefetch_related(Prefetch('presentationelementslang_set', queryset=PresentationElementsLang.objects.filter(Lang=language)))
    
            mainResource,m2mData = ProcessResourceData(ProcessWidgetData(request.data,pes,user),resource_id)
        
      record = mutant_insert_data(mainResource,resource_id,org_id,user)   
   
      if 'id' in mainResource:
        id = mainResource['id']
      else:
        id=None     

      if record and len(m2mData)>0:
          mutantm2m =  mutant_insert_data_m2m_v2(record.id,org_id,m2mData,user,False) # MIGOR - PROMJENIO DA PRORADI saveResourceData 
         #mutantm2m =  mutant_insert_data_m2m(resource_id,related_id,mutantModel.id,m2mData,org_id,user) # MIGOR - PROMJENIO DA PRORADI saveResourceData 
      if record is not None:
        return Response (status=201)
      else:
        return Response(data={'msg':'Error inserting data'},status=400)

"""

class DeleteDatasetRecordViewSet(viewsets.GenericViewSet):
    
    #serializer_class = MutantModelSerializer
    #queryset = User.objects.all()
    
    def create(self, request,resourceDefId,resourceId):
      
      if 'resourceDefId' in self.kwargs:
           resource_def_id=int(self.kwargs['resourceDefId'])
      else:
          return Response(data={'msg':'Invalid request - parameter resourceDefId is missing'},status=400)
      
      
      if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
      else:
          return Response(data={'msg':'Invalid request - parameter resourceId is missing'},status=400)
      
      user=request.user
      #ToDo - check if user authorised
      
      
      org_id,language = helper.get_lang_org (user) 
       
          
      #model = mutant_get_model(resource_def_id,org_id).model_class()
      del_result = mutant_delete_data(resource_def_id,resource_id,org_id,user)
     
      if del_result[0]>0:
        return Response (data={'msg':'Record '+str(resource_id)+ ' deleted successfuly'},status=200)
      else:
        return Response(data={'msg':'Error deleting data '+str(del_result)},status=500)

#not used - ToDo check
"""
class GetM2MFieldDataViewSet(viewsets.ReadOnlyModelViewSet):
   
      queryset = ResourceDef.objects.all()

     
      def list(self, request,resourceId,itemId,formType,lang):
        
        if 'itemId' in self.kwargs:
           item_id=int(self.kwargs['itemId'])
        else:
           item_id = None
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        
        if 'formType' in self.kwargs:
           form_type=self.kwargs['formType']
        else:
           form_type = 'i'
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
           
        user=request.user
        org_id,language = helper.get_lang_org (user) 
         
        #get field definition
        
        #ToDo SqlAlchemy upgrade DONE
        session = rdbms_cls.Session()
        
        #m2m_field = ResourceModelDefinition.objects.get(pk=item_id)   
        with session.begin():
            m2m_field = session.query(ResourceModelDefinitionSa).where(pk == item_id)
         
        #get Related and Get ResourceDefId for m2m table
        resource_def_id =  int(m2m_field.ResourceDefId)
        
        related =  int(m2m_field.Related_id)
        
        #get ids of related resource
        m2m_ids =  mutant_get_data_filtered(mutant_get_model_m2m(resource_def_id,related,org_id),{'ResourceSourceDefId' :resource_def_id,'ResourceSourceId' : resource_id},parameters={})
        
        #create list
        related_resource_list=[]
        for item in m2m_ids:  
          related_resource_list.append(item.ResourceRelatedId)
        
        #get data
        queryset =  mutant_get_data_filtered_in(mutant_get_model(related,org_id),related_resource_list)
        

        #get colmn def
        columnsDef = get_columns_def(related,0,form_type,org_id,language)
        
        #rdef = ResourceDef.objects.get(id=resource_id)
        with session.begin():
            rdef = session.query(ResourceDefSa).where(id == resource_id)
        
        if rdef.ResourceType.ResourceCode == 'file':
            columnsDef = get_file_fields(resource_id,org_id,columnsDef)
        
        if queryset is not None and columnsDef is not None:
          return Response({'data':queryset.values(),'columnsDef':columnsDef})
        else:
          return Response(status=404)
        
#not used - ToDo check
class AddRelatedResourcesViewSet(viewsets.GenericViewSet):
   queryset = ResourceDef.objects.all()#.select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   serializer_class = RelatedResourceSerializer
   
   def list(self, request,itemId,resourceId):
    
      return Response(status=200)
    
   def create(self, request,itemId,resourceId):
        
        if 'resourceId' in self.kwargs:
           resource_id=int(self.kwargs['resourceId'])
        else:
           resource_id = None
        
        if 'itemId' in self.kwargs:
           item_id=int(self.kwargs['itemId'])
        else:
           item_id = None
        
        user = request.user
        
        org_id,language = helper.get_lang_org (user) 
         
        #get field definition
        m2m_field = ResourceModelDefinition.objects.get(pk=item_id)
        
        #get Related and Get ResourceDefId for m2m table
        resource_def_id =  int(m2m_field.ResourceDefId_id)
        
        delRelated = mutant_get_data_filtered(mutant_get_model_m2m(resource_def_id,m2m_field.Related_id,org_id),{'ResourceSourceDefId' :resource_def_id,'ResourceSourceId' : resource_id},parameters={}).delete()
        
        ins_m2m = mutant_insert_data_m2m(m2m_field.Related_id,resource_def_id,resource_id,request.data,user)
               
        
        
        return Response(201)

"""

#done
class GetResourceFieldsViewSet(viewsets.ModelViewSet):
   #queryset = DatasetMappingHead.objects.all().select_related()
   #serializer_class = GetResourceFieldsSerializer
   session = rdbms_cls.Session()
   
   def list(self, request, resourceId, lang):
   #def get_queryset(self):
      
      if 'resourceId' in self.kwargs:
        resource_id = self.kwargs['resourceId']
      else:
        return Response(data={'data':None,'msg':'Missing paremeter resourceId'},status=400)
      if 'lang' in self.kwargs:
        language = self.kwargs['lang']
      else:
        language = None
      
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 

      try:
        #ToDo SqlAlchemy upgrade DONE
        #rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active').prefetch_related(Prefetch('PresentationElements_resourcemodeldefinitionid',
        #        queryset=PresentationElements.objects.filter(Status='Active',ResourceType='o').prefetch_related(Prefetch('presentationelementslang_set',
        #          queryset=PresentationElementsLang.objects.filter(Lang=language))))).distinct().order_by('id')
            
        with session.begin():
            rmds2 = session.query(ResourceModelDefinitionSa)\
                .join(PresentationElementsSa, PresentationElementsSa.ResourceDefId == ResourceModelDefinitionSa.id)\
                .join(PresentationElementsLangSa, PresentationElementsLangSa.PresentationElementsId ==  PresentationElementsLangSa.id)\
                .filter(ResourceDefId == dataset2Id, Status=='Active',ResourceType=='o', Lang=language).distinct().order_by('id')


        return rmds2.all()
        
      except Exception as exc:
        
        return Response(data={'data':None,'msg':str(exc)},status=500)

#done      
class GetDatasetMappingHeadViewSet(viewsets.ViewSet):
   #queryset = DatasetMappingHead.objects.all().select_related()
   #serializer_class = GetDatasetMappingHeadSerializer
   
   #def get_queryset(self):
   def list(self, request, fromDs, toDs, lang):
   
    
    if 'fromDs' in self.kwargs:  
       fromDs = int(self.kwargs['fromDs'] )
    else:
       fromDs = 0
     
    if 'toDs' in self.kwargs:  
       toDs = int(self.kwargs['toDs'] )
    else:
       toDs = 0
  
    if 'lang' in self.kwargs:  
       language = self.kwargs['lang'] 
    else:
       language=None
        
    user = self.request.user
    
    org_id,language = helper.get_lang_org (user) 

    filter = {}
    if fromDs>0:
        filter['Dataset1_id'] = fromDs
    if toDs>0:
        filter['Dataset2_id'] = toDs
         
    #ToDo SqlAlchemy upgrade  - DONE
    #dhms = DatasetMappingHead.objects\
    #    .filter(**filter)\
    #    .select_related().prefetch_related(Prefetch('DatasetMappingHeadLang_datasetmappingheadid',queryset=DatasetMappingHeadLang.objects.filter(Lang=language.id))).order_by('id')
    
    session = rdbms_cls.Session()
    res=[]
    with session.begin():
        query = session.query(DatasetMappingHeadSa, DatasetMappingHeadLangSa)\
        .join(DatasetMappingHeadLangSa, DatasetMappingHeadSa.id == DatasetMappingHeadLangSa.DatasetMappingHeadId)\
        .filter(DatasetMappingHeadLangSa.Lang == language.id)\
        #.filter(DatasetMappingHeadSa.Dataset1 == filter['Dataset1_id'], DatasetMappingHeadSa.Dataset1 == 
        #.all()
    
        if fromDs>0:
            query = query.filter(DatasetMappingHeadSa.Dataset1 == fromDs)
        if toDs>0:
            query = query.filter(DatasetMappingHeadSa.Dataset2 == toDs)
        
        query = query.all()
        
        for item in query:
            (head, headlang) = item
            res.append(head._asdict())
    
    #return dhms 
    return Response(data=res)
#done
class GetResourceParamsViewSet(viewsets.ModelViewSet):
   queryset = ResourceParams.objects.all().select_related().order_by('id')
   serializer_class = ResourceParamsSerializer
   
   #def get_queryset(self):
     
      #try:
        #rp = ResourceParams.objects.all().order_by('id')

    #    return rp
    #  except Exception as exc:
        
    #    return Response(data={'data':None,'msg':str(exc)},status=400)
    
class SaveDatasetMappingHeadViewSet(viewsets.GenericViewSet):
   queryset = DatasetMappingHead.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   #serializer_class = SaveCodesDetailSerializer
   
   def list(self, request, lang):
      
      if 'lang' in self.kwargs:
        language = self.kwargs['lang']
      else:
        language = None
     
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 

      try:
       
       #get data
        #ToDo SqlAlchemy upgrade DONE
        #dhms = DatasetMappingHead.objects.all().prefetch_related( Prefetch('DatasetMappingHeadLang_datasetmappingheadid',queryset=DatasetMappingHeadLang.objects.filter(Lang=language)))
        session = self.Session()
        with session.begin(): 
            (dmhSa, dmhlangSa) = session.query(DatasetMappingHeadSa, DatasetMappingHeadLangSa)\
            .join (DatasetMappingHeadLangSa, DatasetMappingHeadLangSa.DatasetMappingHeadId == DatasetMappingHeadSa.id)\
            .filter(DatasetMappingHeadLang.Lang == language).first()
        
 
        #if savecdl: 
        if True:
          #return Response(data= dhms.values(),status=200)
          return Response(data= dmhSa,status=200)
        else:
          return Response(data={'data':None,'msg':'Serializer in null'},status=400)
      except ValueError as ve:
        return Response(data={'data':None,'msg':ve},status=400)
      except Exception as exc:
        return Response(data={'data':None,'msg':exc},status=400)

        
 
   def create(self, request, mappingId,lang):
      
      mapping_id = None
      language = None 
      if 'mappingId' in self.kwargs: mapping_id = self.kwargs['mappingId']   
      if mapping_id=='undefined':    mapping_id=None     
      if 'lang' in self.kwargs:      language=self.kwargs['lang']
     
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 

      session = rdbms_cls.Session() 
      try:
        with session:
            sdmds=5
            
            if mapping_id is not None: #ako je našao postojećeg
                #ToDo SqlAlchemy upgrade DONE
                #row = DatasetMappingHead.objects.get(id=mapping_id)
                row = session.query(DatasetMappingHeadSa).where (id == mapping_id)
                row.MappingName=request.data['mappingName']
                row.Active=True
                row.Dataset1=request.data['dataset1']['id']
                row.Dataset2=request.data['dataset2']['id']
                row.MappingType=request.data['mappingType']['id']
                row.ChangedDateTime = datetime.now(timezone.utc)
                row.ChangedBy = request.user.id
                session.commit()
                #row.save()
            else:#novi
                #ToDo SqlAlchemy upgrade DONE
                row = DatasetMappingHead.objects.create(id=mapping_id,MappingName=request.data['mappingName'],Active=True,Dataset1_id=request.data['dataset1']['id'],Dataset2_id=request.data['dataset2']['id'],MappingType_id=request.data['mappingType']['id'], CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)      
                new = DatasetMappingHeadSa(id=mapping_id,MappingName=request.data['mappingName'],Active=True,Dataset1=request.data['dataset1']['id'],Dataset2=request.data['dataset2']['id'],MappingTypeId=request.data['mappingType']['id'], CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)      
                session.add(new)
                session.commit()
            try:
              maplang = DatasetMappingHeadLang.objects.get(DatasetMappingHeadId_id=row.id,Lang=language)
              maplang = session.query(DatasetMappingHeadLangSa).where(DatasetMappingHeadId==row.id, Lang==language)
              maplang.Name=request.data['mappingDisplayName']
              maplang.ChangedDateTime = datetime.now(timezone.utc)
              maplang.ChangedBy = request.user.id
              session.add(new)
              session.commit()
              #maplang.save()
            except DatasetMappingHeadLang.DoesNotExist: #ako je novi
              #ToDo SqlAlchemy upgrade DONE
              DatasetMappingHeadLang.objects.create(DatasetMappingHeadId_id=row.id,Lang=language, Name=request.data['mappingDisplayName'],CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)
              new =  DatasetMappingHeadLangSa(  
                            DatasetMappingHeadId_id=row.id,
                            Lang=language, 
                            Name=request.data['mappingDisplayName'],
                            CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)                      
              session.add(new)
              
            return Response(data= {'data':None,'msg':'Mapping saved successfuly'},status=200)
           
         
      except Exception as exc:
            
            traceback_details = PrepareExeptionMessage(sys.exc_info()) 
            return Response(data={'data':None,'msg':traceback_details},status=400)


class SaveDatasetTransformHeadViewSet(viewsets.GenericViewSet):
 
    def create(self, request, transformId,lang):
      
      params={}
      language = None
      
      if 'transformId' in self.kwargs:  transform_id = self.kwargs['transformId']  
      if transform_id=='undefined':     transform_id=None  
      if 'lang' in self.kwargs:         language=self.kwargs['lang']
            
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 
      
      if request.data['dataset1']:
            params['dataset1']=request.data['dataset1']['id']
      if request.data['dataset1Field']:
            params['field1']=request.data['dataset1Field']['id']
      if request.data['dataset2']:
            params['dataset2']=request.data['dataset2']['id']
      if request.data['dataset2Field']:
            params['field2']=request.data['dataset2Field']['id']

      session = rdbms_cls.Session()
      try:
        with session.begin():
            if transform_id is not None: #ako postoji transform onda update
                    row = session.query(DatasetTransformHeadSa).where(id==transform_id)
                    #row = DatasetTransformHead.objects.get(id=transform_id)
                    row.TransformName=request.data['transformName']
                    #row.Active=True
                    row.FieldType1_id=request.data['fieldType1']['id']
                    row.FieldType2_id=request.data['fieldType2']['id']
                    row.ChangedDateTime = datetime.now(timezone.utc)
                    row.ChangedBy = request.user.id
                    row.Params=params
                    #row.save()
            else:# kreira novi transform
                #ToDo SqlAlchemy upgrade DONE
                #row = DatasetTransformHead.objects.create(TransformName=request.data['transformName'],FieldType1_id=request.data['fieldType1']['id'],FieldType2_id=request.data['fieldType2']['id'],Params=params, CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)
                new = DatasetTransformHeadSa(
                            TransformName = request.data['transformName'], 
                            FieldType1_id=request.data['fieldType1']['id'], 
                            FieldType2_id=request.data['fieldType2']['id'], 
                            Params=params, 
                            CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)
                session.add(new)
            #pdb.set_trace()#IMPORT #TRANSFORM
             
            try:
              #trlang = DatasetTransformHeadLang.objects.get(TransformationHeadId_id=row.id,Lang=language)
              trlang = session.query(DatasetTransformHeadLangSa).where(TransformationHeadId == row.id, Lang==language)
              trlang.Name=request.data['transformDisplayName']
              trlang.ChangedDateTime = datetime.now(timezone.utc)
              trlang.ChangedBy = request.user.id
              session.commit()
              #trlang.save()
            except DatasetTransformHeadLang.DoesNotExist: #ako je novi
              #ToDo SqlAlchemy upgrade DONE
              DatasetTransformHeadLang.objects.create(TransformationHeadId_id=row.id,Lang=language, Name=request.data['transformDisplayName'],CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)
              new = DatasetTransformHeadLangSa(
                        TransformationHeadId=row.id,
                        Lang=language, 
                        Name=request.data['transformDisplayName'],
                        CreatedDateTime=datetime.now(timezone.utc), CreatedBy=request.user)       
              session.add(new)                        
                                
        return Response(data= {'data':None,'msg':'Transform saved successfuly'},status=200)
       
      except Exception as exc:   
        traceback_details = PrepareExeptionMessage(sys.exc_info()) 
        return Response(data={'data':None,'msg':traceback_details},status=400)
 

class GetDatasetMappingDetailViewSet(viewsets.GenericViewSet):
   queryset = DatasetMappingHead.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   #serializer_class = SaveCodesDetailSerializer
   
   def list(self, request, mappingId,lang):
      
      if 'mappingId' in self.kwargs:
        mapping_id = self.kwargs['mappingId']
      else:
        mapping_id = None
      
      if 'lang' in self.kwargs:
        language = self.kwargs['lang']
      else:
        language = None
     
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 
      try:
       
       #get data
        #ToDo SqlAlchemy upgrade DONE
        dmh = DatasetMappingHead.objects.filter(id=mapping_id).prefetch_related(Prefetch('DatasetMappingDetail_datasetmappingheadid', queryset=DatasetMappingDetail.objects.all().select_related('Field1')))
        
        session = self.Session()
        with session.begin(): 
            dmhSA, dmhDetailsSA  = session.query(DatasetMappingHeadSa, DatasetMappingDetailsSa)\
            .join (DatasetMappingDetailsSa, DatasetMappingDetailsSa.DatasetMappingHeadId == DatasetMappingHeadSa.id)\
            .filter(DatasetMappingHeadSa.id == mapping_id).first()

        #if dmh.count()>0:         
        #  dataset1Id = dmh[0].Dataset1_id
        #  dataset2Id = dmh[0].Dataset2_id
        #  dmds = dmh[0]._prefetched_objects_cache['DatasetMappingDetail_datasetmappingheadid']
        #else:
        #  dataset1Id=None
        #  dataset2Id=None
        #  dmds = None
          
        if dmhSA.count()>0:         
          dataset1Id = dmhSA.Dataset1
          dataset2Id = dmhSA.Dataset2_id
          dmds = dmhDetailsSA
        else:
          dataset1Id=None
          dataset2Id=None
          dmds = None
          
        #ToDo SqlAlchemy upgrade DONE
        #rmds1 = ResourceModelDefinition.objects.filter(ResourceDefId_id=dataset1Id,Status='Active')\
        #    .prefetch_related(Prefetch('PresentationElements_resourcemodeldefinitionid', queryset=PresentationElements.objects.filter(Status='Active',ResourceType='o')\
        #    .prefetch_related(Prefetch('presentationelementslang_set', queryset=PresentationElementsLang.objects.filter(Lang=language)))))

        with session.begin():
            rmds1 = session.query(ResourceModelDefinitionSa, PresentationElementsSa , PresentationElementsLangSa)\
            .join(PresentationElementsSa, PresentationElementsSa.ResourceDefId == ResourceModelDefinitionSa.id)\
            .join(PresentationElementsLangSa, PresentationElementsLangSa.PresentationElementsId ==  PresentationElementsLangSa.id)\
            .filter(ResourceModelDefinitionSa.ResourceDefId == dataset1Id, ResourceModelDefinitionSa.Status=='Active', ResourceModelDefinitionSa.ResourceType=='o', PresentationElementsLangSa.Lang==language)

        #rmds2 = ResourceModelDefinition.objects.filter(ResourceDefId_id=dataset2Id,Status='Active')\
        #    .prefetch_related(Prefetch('PresentationElements_resourcemodeldefinitionid',  queryset=PresentationElements.objects.filter(Status='Active',ResourceType='o')\
        #        .prefetch_related(Prefetch('presentationelementslang_set',  queryset=PresentationElementsLang.objects.filter(Lang=language)))))
        
        with session.begin():
            rmds2 = session.query(ResourceModelDefinitionSa, PresentationElementsSa , PresentationElementsLangSa)\
            .join(PresentationElementsSa, PresentationElementsSa.ResourceDefId == ResourceModelDefinitionSa.id)\
            .join(PresentationElementsLangSa, PresentationElementsLangSa.PresentationElementsId ==  PresentationElementsLangSa.id)\
            .filter(ResourceModelDefinitionSa.ResourceDefId == dataset2Id, ResourceModelDefinitionSa.Status=='Active', ResourceModelDefinitionSa.ResourceType=='o', PresentationElementsLangSa.Lang==language)

        resourceModel = []              
        for (rmd2, pesa, pelang) in rmds2:      
          resourceModelItem = {'id':rmd2.id,'type':rmd2.GenericType,'name':pelang.Label,'fields':[],'fields2':[]}
          for dmd in dmds: 
            if rmd2.id==dmd.Field2_id:
              for rmd1 in rmds1:
                if rmd1.id == dmd.Field1_id:
                  if dmd.RecType==1 or dmd.RecType is None:
                    resourceModelItem['fields'].append({'id':rmd1.id,'GenericType':rmd1.GenericType,'PresentationElements_resourcemodeldefinitionid':[{'presentationelementslang_set':[{'Label':pelang.Label}]}] })
                  elif dmd.RecType==2:
                    resourceModelItem['fields2'].append({'id':rmd1.id,'GenericType':rmd1.GenericType,'PresentationElements_resourcemodeldefinitionid':[{'presentationelementslang_set':[{'Label':pelangLabel}]}]})
                  
          resourceModel.append(resourceModelItem)
            
        
        return Response(data= resourceModel,status=200)
     
      except Exception as exc:
        raise exc
        traceback_details = PrepareExeptionMessage(sys.exc_info())
        
        return Response(data={'data':None,'msg':traceback_details},status=400)
        
class SaveDatasetMappingDetailViewSet(viewsets.GenericViewSet):
   #queryset = DatasetMappingHead.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   #serializer_class = SaveDatasetMappingDetailSerializer
   
   def create(self, request, mappingId):
      
      if 'mappingId' in self.kwargs:
        mapping_id = self.kwargs['mappingId']
      else:
        mapping_id = None
     
      try:
        #ToDo SqlAlchemy upgrade - DONE
        #DatasetMappingDetail.objects.filter(DatasetMappingHeadId_id=mapping_id).delete() 
        #sdmds = SaveDatasetMappingDetailSerializer (data=request.data,many=True,context={'request':request,'mappingId':mapping_id})
        
        with session.begin():
            deleted = session.query(DatasetMappingDetailSa).filter(DatasetMappingDetailSa.DatasetMappingHeadId== mapping_id).delete()
        
            for item in request.data:
                new = DatasetMappingDetailSa(**item)
                session.add(new)
            
        #pdb.set_trace()
                
        #if sdmds.is_valid(raise_exception=True):
        #    sdmds.save()
        
        return  Response(data= {'data':None,'msg':'Mapping saved successfuly'},status=200)
        #if sdmds:
        #  return Response(data= {'data':None,'msg':'Mapping saved successfuly'},status=200)
        #else:
        #  return Response(data={'data':None,'msg':'Serializer in null'},status=400)
     
      except Exception as exc:
        
        traceback_details = PrepareExeptionMessage(sys.exc_info()) 
        return Response(data={'data':None,'msg':traceback_details},status=400)
      
class GetDatasetTransformHeadViewSet(viewsets.ModelViewSet):
  serializer_class = GetDatasetTransformHeadSerializer
  session = rdbms_cls.Session()
  
  def list(self, request, lang):
  #def get_queryset(self):
      
    if 'lang' in self.kwargs:
        lang = self.kwargs['lang']
    else:
        lang = None
    
    user = self.request._user
    
    org_id,language = helper.get_lang_org (user) 
     
     
    #ToDo SqlAlchemy upgrade -DONE
    #dth = DatasetTransformHead.objects.all().select_related().prefetch_related(Prefetch('DatasetTransformHeadLang_transformationheadid',queryset=DatasetTransformHeadLang.objects.filter(Lang=language)))
    with session.begin():
        query= session.query(DatasetTransformHeadSa, DatasetTransformHeadLangSa)\
        .join(DatasetTransformHeadLangSa, DatasetTransformHeadLangSa.TransformationHeadId == DatasetTransformHeadSa.id).filter(DatasetTransformHeadLangSa.Lang == language)
    
    #pdb.set_trace()
    
    #return dth
    return query.all()
   

class GetTransformDetailViewSet(viewsets.ModelViewSet):
   #queryset = DatasetTransformDetail.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   #serializer_class = GetTransformDetailSerializer
   
   def list(self, request, transformId):
   #def get_queryset(self):
      
      if 'transformId' in self.kwargs:
        transform_id = self.kwargs['transformId']
      else:
        transform_id = None
      #ToDo SqlAlchemy upgrade - DONE
      cd1 = aliased(codesdetail)
      cd2 = aliased(codesdetail)
      
      #transform = DatasetTransformDetail.objects.filter(TransformationHeadId_id=transform_id).select_related('CodeDetail1','CodeDetail2').order_by('id')
      #pdb.set_trace()
      with session.begin():  
        query = session.query(DatasetTrasnformDetailSa, DatasetTransformHeadSa, cd1, cd2)\
        .join(DatasetTransformHeadSa, DatasetTrasnformDetailSa.TransformationHeadId == DatasetTransformHeadSa.id)\
        .join(cd1, DatasetTrasnformDetailSa.CodeDetail1 == cd1.id)\
        .join(cd2, DatasetTrasnformDetailSa.CodeDetail2 == cd2.id)\
        .filter(TransformationHeadId_id==transform_id).order_by(id)
        
      #return transform
      return query.first()
      
    
class SaveTransformDetailViewSet(viewsets.GenericViewSet):
   queryset = DatasetTransformDetail.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   #serializer_class = SaveCodesDetailSerializer
   
   def create(self, request, transformId):
      
      if 'transformId' in self.kwargs:
        transform_id = self.kwargs['transformId']
      else:
        transform_id = None
     
      try:
        
        #ToDo SqlAlchemy upgrade - DONE
        #sdtds = SaveDatasetTransformDetailSerializer (data=request.data,many=True,context={'request':request,'transformId':transform_id})
 
        #pdb.set_trace()
        session = rdbms_cls.Session()
        with session.begin():        
            for data in request.data:
                item = DatasetTransformDetailSa(**data)
                session.add(item)
  
        #if sdtds.is_valid(raise_exception=True):
        #  rval = sdtds.save()
        
        #if sdtds:
        return Response(data= {'data': rval,'msg':'Transformation saved successfuly'},status=200)
        #else:
        #  return Response(data={'data':None,'msg':'Serializer in null'},status=400)
     
      except Exception as exc:
        
        traceback_details = PrepareExeptionMessage(sys.exc_info()) 
        return Response(data={'data':None,'msg':traceback_details},status=400)

class SavePageDefViewSet(viewsets.GenericViewSet):
    
    #serializer_class = MutantModelSerializer
    #queryset = User.objects.all()
    
    def create(self, request,lang):
      if 'lang' in self.kwargs:
           language=self.kwargs['lang']
      else:
          return Response(data={'msg':'Invalid request - parameter language is missing'},status=400)
      
      data = request.data
      resource_id=None
      user = request.user
      user = request.user
      
      org_id,language = helper.get_lang_org (user) 
      resource = data['resource']
      save_type = data['save_type']
      users=[]
      groups=[]
      users_ids = []
      
      #pdb.set_trace()
      if 'AdminUsers' in resource:
        users = resource['AdminUsers']            
      if 'AdminGroups' in resource:
        groups = resource['AdminGroups']       
      for u in users:
        if u['id']  != user.id:
            users_ids.append(u['id'])
    
      #always grant current user  
      users_ids.append(user.id)    
      
      groups_ids = []    
      for g in groups:
        groups_ids.append(g['id'])   
      if 'id' in resource:
        resource_id = resource['id']
      else:
        resource_id = None
        return Response(data='Error saving page--> Resource id is not provided',status=400)
        
      page_name = ''
      page_subject = ''
      page_description = ''
      
      #pdb.set_trace()
      session = rdbms_cls.Session()

      #pdb.set_trace()
      if save_type in ["info","layout"]:
        if save_type=="info":
            rdl_id = resource['rdlId']
            page_name = resource['Name']
            page_subject = resource['Subject']
            page_description = resource['Description']
            page_params = resource.get('ParametersLang','{}')
            #ToDo SqlAlchemy upgrade - DONE
            #pdb.set_trace()
            
            #ResourceDefLang.objects.filter(id=rdl_id).update(Name=page_name,Subject=page_subject,Description=page_description,ParametersLang=page_params)
            
            with session.begin():
                session.execute( update(ResourceDefLangSa).where(ResourceDefLangSa.id == rdl_id)\
                .values( Name=page_name, Subject=page_subject, Description=page_description, ParametersLang=json.dumps(page_params)) )
       
            #pages = Pages.objects.filter(ResourceDefId_id=resource_id)
            with session.begin():
                pages = session.query(PagesSa).filter(PagesSa.ResourceDefId==resource_id)
            
                if pages.count()==0:
                    #page = Pages.objects.create(Target=resource['ResourceExtended']['Target'], PageType_id=resource['ResourceExtended']['PageType']['id'],ResourceDefId=resource_id)
                    new = PagesSa(Target = resource['ResourceExtended']['Target'], PageType = resource['ResourceExtended']['PageType']['id'], ResourceDefId=resource_id)                       
                    session.add(new)      
                else:
                    for page in pages:
                        page.Target = resource['ResourceExtended']['Target']
                        page.PageType = resource['ResourceExtended']['PageType']['id']
                        page.ResourceDefId = resource_id

        else:
            #ToDo SqlAlchemy upgrade - DONE
          
            #pages = Pages.objects.filter(ResourceDefId_id=resource_id)
            session =  rdbms_cls.Session()
            with session.begin():
                pages = session.query(PagesSa).filter(PagesSa.ResourceDefId==resource_id)
            
            if len(pages)==0:
                #page = Pages.objects.create(Target=resource['ResourceExtended']['Target'],PageType_id=resource['ResourceExtended']['PageType']['id'],ResourceDefId_id=resource_id)
                with session.begin():
                    new = PagesSa(Target = resource['ResourceExtended']['Target'], PageType = resource['ResourceExtended']['PageType']['id'], ResourceDefId=resource_id)                       
                    session.add(new)      

            params = resource['Parameters']
          
            #ResourceDef.objects.filter(id=resource_id).update(Parameters=params)
            with session.begin():
                rdefSA = session.update(ResourceDefSa).where(ResourceDefSa.id ==resource_id).values(Parameters=params)
      else:       
            #rdefs = ResourceDef.objects.filter(id=resource_id)
            #for rdef in rdefs:
            #          rdef.AdminUsers.set(users_ids)
            #          rdef.AdminGroups.set(groups_ids)
            #          rdef.save()
            #pdb.set_trace()
            with session.begin():
               musers_del = session.query(AdminUsers).filter(AdminUsers.resourcedef_id==resource_id).delete()
               mgroups_del = session.query(AdminGroups).filter(AdminGroups.resourcedef_id==resource_id).delete()
               for ui in users_ids:
                       musers = AdminUsers(resourcedef_id=resource_id, user_id=ui)
                       session.add(musers)
               for gi in groups_ids:
                       mgroups = AdminGroups(resourcedef_id=resource_id, group_id = gi)
                       session.add(mgroups)
                                                               
      
      return Response (data='Page '+save_type+' saved successfuly',status=200)

class ResourceSubscriptionsViewSet(viewsets.GenericViewSet):
    queryset = ResourceDef.objects.all()
     
    def list(self, request,subscriptionType):
        
        if 'subscriptionType' in self.kwargs:
           subs_type=self.kwargs['subscriptionType']
        else:
           subs_type = 'application'
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        self.kwargs.update({'lang':language})  
        #subs=ResourceSubscriptions.objects.filter(UserId=user,SubscriptionType=subs_type).select_related('ResourceId')
        #pdb.set_trace()
        subs = []
        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()

        with session.begin():
            rdefs_sa = session.query(ResourceDefSa,ResourceDefLangSa,ResourceSubscriptionsSa,OrganisationsSa.Name).join(ResourceParamsSa,and_(ResourceDefSa.ResourceType==ResourceParamsSa.id, ResourceParamsSa.ResourceCode==subs_type))\
            .outerjoin(OrganisationsSa,ResourceDefSa.Organisation==OrganisationsSa.id)\
            .outerjoin(ResourceSubscriptionsSa,and_(ResourceDefSa.id==ResourceSubscriptionsSa.ResourceId,ResourceSubscriptionsSa.SubscriptionType==subs_type,ResourceSubscriptionsSa.UserId==user.id))\
            .outerjoin(ResourceDefLangSa,and_(ResourceDefSa.id==ResourceDefLangSa.ResourceDefId,ResourceDefLangSa.Lang==language.id))\
            .filter(or_(ResourceDefSa.Private==False,ResourceDefSa.Organisation==org_id)).all()
        

            for rec in rdefs_sa:
                active_sub=False
                sub_id=None
                name = 'Lang record error - '+str(rec[0].id)
                org_name='Organisation name error!'
                if rec[1] is not None:
                    name = rec[1].Name
                if rec[3] is not None:
                    org_name = rec[3]

                if rec[2] is not None:
                    active_sub = getattr(rec[2] ,'Active')
                    sub_id = getattr(rec[2] ,'id')


                subs.append({'id':rec[0].id,'Name':name,'ActiveSub':active_sub,'SubId':sub_id,'OrgName':org_name})

        return Response(data=subs,status=200)
    
    def create(self, request,subscriptionType):
        
        if 'subscriptionType' in self.kwargs:
           subs_type=self.kwargs['subscriptionType']
        else:
           subs_type = 'application'
        
        subscription_data=request.data
        if 'resource' in subscription_data:
            resource = subscription_data['resource']
        else:
            Response(data={'msg':'Resource data is missing'},status=500)
        if 'action' in subscription_data:
            action = subscription_data['action']
        else:
            Response(data={'msg':'Action is missing'},status=500)
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
         
        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()
        status=200
        with session.begin():

            #for sub in resource:
            if action=='subscribe':
                
                active = True
                
                if resource['SubId'] is None:
                    rs = ResourceSubscriptionsSa(ResourceId=resource['id'],Active=True,UserId=user.id,CreatedBy=user.id,CreatedDateTime=datetime.now(timezone.utc),SubscriptionType=subs_type)
                    rs_session =  session.add(rs)
                    message="Subscribed successfuly!"

                    do_update = False
                    
                else:
                    message="Subscribed successfuly!"
                    do_update = True
            else:
                active = False
                message="Unsubscribed successfuly!"
                do_update = True
            
            if do_update:    
                sub_id = resource['SubId']
                rs_session = session.query(ResourceSubscriptionsSa).filter(ResourceSubscriptionsSa.id==sub_id).update({ResourceSubscriptionsSa.Active:active,ResourceSubscriptionsSa.ChangedBy:user.id,ResourceSubscriptionsSa.ChangedDateTime:datetime.now(timezone.utc),ResourceSubscriptionsSa.UserId:user.id}, synchronize_session = False)
                if rs_session!=1:
                     message=action+" error!"
                     status=500
        
        if status==200 and  not do_update:
            sub_id = rs.id
            
        return Response(data={'data':{'SubId':sub_id,'ActiveSub':active},'msg':message},status=status)

class GetPublishedResourcesViewSet(viewsets.GenericViewSet):
    #queryset = PublishedResources.objects.all()
     
    def list(self, request,resourceType,publishType):
        
        if 'resourceType' in self.kwargs:
           resource_type=self.kwargs['resourceType']
        else:
           resource_type = None

        if 'publishType' in self.kwargs:
           publish_type=self.kwargs['publishType']
        else:
           publish_type = 'all'
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
          
        

        

        published_resources = helper.get_published_resources_by_type(resource_type,org_id,language,publish_type)

        return Response(data=published_resources,status=200)


class GetPublishedResourcesByTypeIdViewSet(viewsets.GenericViewSet):
    #queryset = PublishedResources.objects.all()
     
    def list(self, request,resourceId,publishType):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_type = None

        if 'publishType' in self.kwargs:
           publish_type=self.kwargs['publishType']
        else:
           publish_type = 'all'
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
          
        

        
        #pdb.set_trace()
        published_resources = helper.get_published_resources_by_type_id(resource_id,publish_type,org_id,language)

        return Response(data=published_resources,status=200)
        
class GetPublishedResourceByIdViewSet(viewsets.GenericViewSet):
    #queryset = PublishedResources.objects.all()
     
    def list(self, request,publishId):
        
        if 'publishId' in self.kwargs:
           publish_id=self.kwargs['publishId']
        else:
           publish_id = None

        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
          
        

        

        published_resource = helper.get_published_resource_by_id(publish_id,org_id,language)

        return Response(data=published_resource,status=200)

class PublishedResourcesViewSet(viewsets.GenericViewSet):
    #queryset = PublishedResources.objects.all()
     
    def list(self, request,resourceId,publishType):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None

        if 'publishType' in self.kwargs:
           publish_type=self.kwargs['publishType']
        else:
           publish_type = 'all'
        
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        #pdb.set_trace() 
        

        

        published_resources = helper.get_published_resources(resource_id,org_id,publish_type)

        return Response(data=published_resources,status=200)
        
    def create(self, request,resourceId,publishType):
        
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None

        if 'publishType' in self.kwargs:
           publish_type=self.kwargs['publishType']
        else:
           publish_type = None

        if publish_type is None:
            return Response(data={'msg':'Invalid publish type!'},status=500)               

        user=request.user
        
        session= rdbms_cls.Session()
        with session.begin():
        
            org_id,language = helper.get_lang_org (user) 
            
            resource_type,status = helper.get_resource_type(resource_id)
            publish = request.data

            publish['PublishType'] = publish_type
            publish['OrganisationId'] = org_id
            publish['UserId'] = user.id
            
            #publish Parameters
            publish['Parameters'] = {}
            
            #publish Definition - this is model definition

            publish['Definition'] = {}
            

            if resource_type is not None:

              try:
                   
                     module_path = 'katalyonode.functions.resources.'+resource_type+'def' 
                     module_ref = importlib.import_module(module_path)
                     resource_class = getattr(module_ref, resource_type)
                     #pdb.set_trace()
                     resource_object = resource_class(resource_id,resource_type,'','','','',user,language,org_id,{})
                     resource_object.set_session(session)
                     
                     if resource_object is not None:
                         
                         resource_object.get_definition()
                         publish['Parameters'] = resource_object.get_definition_json()
                         publish['Parameters']['CreatedDateTime'] = None
                         publish['Parameters']['ChangedDateTime'] = None
                         resource_object.get_db_definition('i',publish['Version'])
                         publish['Definition'] = resource_object.get_db_definition_json()
                         if resource_object.SyncDb and resource_object.ResourceSubType=='standard':
                             #sync table
                             tbl=resource_object.sync_alchemy_table(publish['Version'],False)
                             #sync history table
                             tbl_h=resource_object.sync_alchemy_table(publish['Version'],True)

              except ModuleNotFoundError:
                    raise ValidationError("ModuleNotFoundError --> " + module_path)

            publish_id = helper.publish_resource(publish)

            if publish_id:
                message = 'Resource published successfuly!'
                status_id=200
            else:
                message = 'Publish failed!'
                status_id = 500
            return Response(data={'msg':message},status=status_id)  


class SaveResourceLinksViewSet(viewsets.GenericViewSet):
    
    queryset = ResourceLinks.objects.all()
     
    def create(self, request):
        
        rlinks_input = request.data
        user=request.user
        if type(rlinks_input)==list:
            rlinks= rlinks_input
        else:
            rlinks = [rlinks_input]
        #pdb.set_trace()    
        
        session = rdbms_cls.Session()
        with session.begin():
            for rlink in rlinks:
                if rlink['Resource2'] == 0:   # zero means delete rlink
                        #ToDo SqlAlchemy upgrade DONE
                        #ResourceLinks.objects.filter(id=rlink['id']).delete()   
                        query = session.query(ResourceLinksSa).filter(ResourceLinksSa.id == rlink['id']).delete()
                        
                else:
                    #ToDo SqlAlchemy upgrade DONE
                    #rlink_obj,created = ResourceLinks.objects.get_or_create( id=rlink['id'],defaults={'Resource1_id':rlink['Resource1'],'Resource2_id':rlink['Resource2'],'LinkType':rlink['LinkType'],'CreatedBy':user,'CreatedDateTime':datetime.now(timezone.utc)})
                    defaults={'Resource1':rlink['Resource1'],'Resource2':rlink['Resource2'],'LinkType':rlink['LinkType'],'CreatedBy':user.id,'CreatedDateTime':datetime.now(timezone.utc)}
                    rlink_obj = session.query(ResourceLinksSa).filter(ResourceLinksSa.id == rlink['id']).first()
                    if not rlink_obj:
                        rlink_obj = ResourceLinksSa(**defaults)
                        session.add(rlink_obj)
                        #session.commit()
                    else:   #if rlink_obj and not created:
                        rlink_obj.Resource1=rlink['Resource1']
                        rlink_obj.Resource2=rlink['Resource2']
                        rlink_obj.LinkType=rlink['LinkType']
                        rlink_obj.ChangedBy=user.id
                        rlink_obj.ChangedDateTime=datetime.now(timezone.utc)
                        #session.commit()
                        #rlink_obj.save()
        
        message = 'Saved successfuly!'    

        return Response(data={'msg':message},status=200)        
    
class GetWidgetToolboxViewSet(viewsets.GenericViewSet):
    
    queryset = ResourceWidgetLinks.objects.all()
     
    def list(self, request,resourceType):
        
        toolbox = []
        #ToDo SqlAlchemy upgrade DONE
     
        #rwlinks = ResourceWidgetLinks.objects.filter(WidgetId__Status='Active',ResourceId__ResourceCode=resourceType).select_related('WidgetId',"ResourceId")
        #.order_by('ToolboxOrder') 
        session = rdbms_cls.Session()
        with session.begin():
            rwlinks2 = session.query(ResourceWidgetLinksSa, ResourceParamsSa, WidgetDefinitionSa)\
            .join(ResourceParamsSa, ResourceParamsSa.id == ResourceWidgetLinksSa.ResourceId)\
            .join(WidgetDefinitionSa, ResourceWidgetLinksSa.WidgetId == WidgetDefinitionSa.id)\
            .filter(WidgetDefinitionSa.Status == 'Active', ResourceParamsSa.ResourceCode == resourceType).order_by(ResourceWidgetLinksSa.ToolboxOrder).all()
            
        old_group = None
        count=0

        #pdb.set_trace() #ResourceWidgetLinksSa'
        for item in rwlinks2:
            (rwlink, param, widgetdef) = item
            if old_group is None or (old_group!=rwlink.ToolboxGroup and old_group is not None):
                toolbox.append({'id':count,'ElementType':'toolboxgroup','ToolboxGroup':rwlink.ToolboxGroup})    
                old_group = rwlink.ToolboxGroup 
                count=count+1
            
            widget={'id':count,'ElementType':widgetdef.ElementType,'Status':widgetdef.Status,'HideFromToolbox':rwlink.HideFromToolbox,'DirectiveName':widgetdef.WidgetName,'AngularDirectiveName':widgetdef.WidgetReference,'ToolboxGroup':rwlink.ToolboxGroup}
            
            if widgetdef.Definition is not None:
                widget.update(json.loads(widgetdef.Definition))
            if widgetdef.Parameters is not None:
                widget.update({'Parameters':json.loads(widgetdef.Parameters)})
            toolbox.append(widget)
            count=count+1
            

        return Response(data=toolbox,status=200)    

class GetExistingResourceRelationshipsViewSet(viewsets.GenericViewSet):
     
    def list(self, request,resourceId):
        
        if 'resourceId' in self.kwargs:
           resource_id=self.kwargs['resourceId']
        else:
           resource_id = None

        user=request.user
        
        session = rdbms_cls.Session()
        with session.begin():
        
            org_id,language = helper.get_lang_org (user) 
            dataset_object = dataset(resource_id,'dataset','','','','',user,language,org_id,{})
            dataset_object.set_session(session)

            resource_list = dataset_object.get_parent_resources_for_child()

        return Response(data=resource_list,status=200)


def getDataWithPandas(resource_id,org_id):

    rdbms_cls = RdbmsClass()
    table_name,is_external = get_table_name(resource_id,org_id)
    data = pandas.read_sql_table(table_name,rdbms_cls.engine,coerce_float=False)
    #pdb.set_trace() #getDataWithPandas  
    return data

#not used
"""
@method_decorator(transaction.non_atomic_requests, name='dispatch')
class GetResourceDataPandasViewSet(viewsets.GenericViewSet):
   
    #queryset = PresentationElements.objects.all()
      
    def list(self, request,resourceId,presentationId,taskId,formType, lang):
            
        def get_fields_recursive(input,out):
            
            item = {}

            if isinstance(input, list):
                for item in input:
                    get_fields_recursive(item,out)
            elif isinstance(input, dict):
                for key,value in input.items():
                    if not (isinstance(value, dict) or isinstance(value, list) or (value is None))  :    
                        if key in ('FieldName', 'Label'  ,'FieldType', 'codeId', 'GenericType','Related'): 
                            item[key] = value
                    get_fields_recursive(value,out )
                if "FieldName" in item:
                    out[item['FieldName']] = item
            
            
        resource_id = presentation_id = fields = users = groups = codes = fields = name_fields = org_id = None
        language = None
        rmds =pids = rval = m2mData = []
        filter = resourceData = {}
        isIdIn = False
        
        if 'resourceId' in self.kwargs:   resource_id=int(self.kwargs['resourceId']) 
        if 'presentationId' in self.kwargs:   presentation_id=int(self.kwargs['presentationId'])
        if 'lang' in self.kwargs:    language=self.kwargs['lang']
       
        user=request.user
        
        org_id,language = helper.get_lang_org (user) 
        
        nameDefs = PresentationElements.objects.get(id=presentation_id)
        nameField = nameDefs.NameField
        
        if nameField is not None:
          for element in nameField:
            pids.append(element['id']) 
          
        fields = PresentationElements.objects.filter(id__in=pids)   
        
        concat_name_fields = list()
        if fields is not None:
            name_fields={}
            for fld in fields:
                column = fld.ResourceModelDefinitionId.FieldName
                if fld.ElementType=='resourceid':
                    column = 'resource'+str(resource_id)+'_'+str(org_id)+'_id'
                    
                concat_name_fields.append(column)
                name_fields[column]=column
        

        selectedRecords =  mutant_get_data_sa(resource_id,org_id,concat_name_fields)      
        
        pdata = getDataWithPandas(nameDefs.Related_id,nameDefs.OrganisationId_id)
        
        try:
            aggregateField = nameDefs.Parameters['AggregateField']['FieldName']
            calculateField = nameDefs.Parameters['CalculateField']['FieldName']
            calculateFunction = nameDefs.Parameters['CalculateFunction']['name']
            chartType = nameDefs.Parameters['chartType']['name']
   
        except KeyError as e:
                return Response(data='Missing chart properties ' + str(e),status=400) 
        
        
        if (aggregateField[-3:] == '_id'):
            aggregateField='id'
        
        if calculateFunction == 'sum':
                 result = pdata.groupby([aggregateField])[calculateField].sum()
        if calculateFunction == 'mean':
                result = pdata.groupby([aggregateField])[calculateField].mean()
        if calculateFunction == 'min':
                result = pdata.groupby([aggregateField])[calculateField].min()
        if calculateFunction == 'max':
                result = pdata.groupby([aggregateField])[calculateField].max()
        if calculateFunction == 'count':
                result = pdata.groupby([aggregateField])[calculateField].count()
         

        rdefs = ResourceDefLang.objects.filter(Lang=language,ResourceDefId__Organisation_id=org_id,ResourceDefId__ResourceType__ResourceCode='').select_related()#.prefetch_related(Prefetch('resourcedeflang_set', queryset=ResourceDefLang.objects.filter(Lang=lang)))

        allFields={}
        usersDict = {}
         
        #columns = get_columns_def (resource_id,taskId,'i',org_id,lang)
        
        #formDefinition,resourceDefinition = GetResourceFormDB(resource_id,'i',language,0,0,'w',user,org_id,False)
        #get_fields_recursive(formDefinition,allFields)
        
        users = User.objects.all()
        if len(users)>0:
           for user in users:
             usersDict[user.id] = user.first_name+ ' ' + user.last_name
        
        # MAKE LABELS
        #pdb.set_trace() #pandas debug
        labels =  result.index.to_list()
        

        rmds = ResourceModelDefinition.objects.filter(ResourceDefId_id=resource_id,Status='Active').order_by('id')
        for rmd in rmds:
            if rmd.FieldName == nameDefs.Parameters['AggregateField']['FieldName']:
                    #found aggregate rmd

                    if  rmd.GenericType == 'resourceid':
                            pass
                    elif rmd.GenericType == 'list': 
                            codeLang = CodesDetailLang.objects.get(Lang=language,CodesDetailId=rmd.CodeId)  
                    elif rmd.GenericType == 'user':
                            print(usersDict[rmd.CodeId])
                    elif rmd.GenericType == 'resource' :
                            relatedPes = PresentationElements.objects.get(id=nameDefs.Parameters['AggregateField']['NameField']['id'])
                            ret = getResourceDataListAll(rmd.Related,presentation_id,org_id)
                            for item in ret:
                                labels[item['id']] = item['Field' + str (relatedPes.ResourceModelDefinitionId_id) ]
                    else: #textbox,number...etc..
                         #pdb.set_trace()
                         a=3
                                 
   
        return Response({'data': result.to_dict(), 'parameters':  nameDefs.Parameters, 'labels': labels})
   
        if selectedRecords is not None:
            return Response(selectedRecords)
        else:
            return Response(status=404)
    
"""
def PrepareExeptionMessage(exc_info):
  
  exc_info_r = {'filename': '',
                         'lineno'  : 0,
                         'name'    : '',
                         'type'    : '',
                         'message' : 'Error preparing exception info', # or see traceback._some_str()
                      }
    
  if len(exc_info)>2:
    exc_info_r = {'filename': exc_info[2].tb_frame.f_code.co_filename,
                         'lineno'  : exc_info[2].tb_lineno,
                         'name'    : exc_info[2].tb_frame.f_code.co_name,
                         'type'    : exc_info[0].__name__,
                         'message' : str(exc_info[1]), # or see traceback._some_str()
                        }
  return exc_info_r