from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser,FileUploadParser,FormParser, MultiPartParser
import json
from django.forms.models import model_to_dict
from pathlib import Path
import os
import hashlib
import uuid
import chardet as chardet
#import katalyonode.functions.taskprocessing
from itertools import chain
from django.core import serializers
from django.contrib.auth import login,logout,authenticate,BACKEND_SESSION_KEY
from katalyonode.settings import FILES_DIR,USE_BLOCKCHAIN
from django.template import RequestContext
from django.db.models import Prefetch
from django.contrib.auth.models import User, Group
from rest_framework import viewsets,permissions,authentication,pagination
from rest_framework.pagination import PageNumberPagination
#from django.contrib.auth import update_session_auth_hash
#import katalyonode.serializers.serializers 
from katalyonode.serializers.serializers import UserSerializer, GroupSerializer,OrganisationsSerializer,FilesSerializer,GroupExtendedSerializer,UserExtendedSerializer
from katalyonode.serializers.serializerssettings import CodesSerializer,CodesDetailSerializer,CodesHeadSerializer,CodesHeadLangSerializer,CodesDetailLangSerializer,GetCodesDetailSerializer,GetCodesByLangSerializer
from katalyonode.serializers.serializerssettings import AddUpdateCodesDetailSerializer,AddUpdateCodesDetailLangSerializer,GetCodesDetailByIdLangSerializer
from katalyonode.serializers.serializersresources import GetResourceDefByIdLangSerializer,LanguageSerializer
from katalyonode.models.models import DatasetTransformHead,DatasetTransformHeadLang,DatasetMappingDetail,DatasetTransformDetail,UserExtendedProperties,GroupExtendedProperties,GroupLang
from django.utils.dateparse import parse_datetime
from datetime import datetime,timedelta,timezone
#import katalyonode.models
from wsgiref.util import FileWrapper
from django.core.validators import ValidationError
from django.utils import timezone
from katalyonode.models.models import ResourceDef,ResourceDefLang,Organisations,CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,PresentationElements,PresentationElementsLang,PresentationElementsTasks
from katalyonode.models.models import FilesTable,TaskDef,TaskAssignmentDefUser,TaskAssignmentDefGroup,TaskInitiationInstance,FilesToResource,TaskCodesActions,MappingsTable
from katalyonode.models.models import TaskInitiationInstance,TaskExecutionInstances,AuditTrailTaskStatus,TaskAssignments,TaskExecutionInstanceToResource,TaskDefWidgetDependencies,ResourceModelDefinition
from rest_framework.authtoken.models import Token
#import katalyonode.dynamicmodels
#from katalyonode.functions.dataset import 
#from katalyonode.functions.datasets import get_model_data,mutant_get_data_filtered,mutant_get_data_filtered_in,mutant_insert_data_m2m_no_delete,mutant_insert_data_m2m_v2
#from katalyonode.functions.datasets import mutant_get_data_related,mutant_get_model_m2m,get_columns_def,mutant_insert_data,mutant_insert_data_m2m,mutant_get_m2m_data,mutant_update_data
#from katalyonode.functions.formprocessing import ProcessInitiateForm,ExtendResourceFormJSON,ProcessResourceData
#from katalyonode.functions.formprocessing import ProcessExecuteForm,PreProcessResourceWidget,ProcessWidgetData
#from katalyonode.functions.taskprocessing import InitiateTask,ExecuteTask,ProcessTaskAssignments,ProcessTaskParameters,ProcessPreviousTask,ProcessTaskStatusChange,GetStatusCodeByName
#from katalyonode.functions.blockchainprocessing import send_transaction
from katalyonode.functions.blockchain.eth_middleware import get_ktlyo_balance
from django.core.exceptions import ObjectDoesNotExist
from struct import *
import requests
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.resources.datasetdef import dataset
from katalyonode.functions.rdbms.alchemymodels import group_owners, auth_user, auth_user,auth_group,auth_user_groups, GroupLangSa, GroupExtendedPropertiesSa, UserExtendedPropertiesSa,TokenSa,LanguageSa,OrganisationsSa
from sqlalchemy import and_
from katalyonode.functions.ApiMessages import GetMessage
import pdb
import xmltodict
from sqlalchemy.orm import aliased
from katalyonode.functions import crypto, hashers

#from django.contrib.auth.hashers import (
#    check_password, is_password_usable, make_password,
#)

# Create your views here.
rdbms_cls = RdbmsClass()
helper = ResourceHelperClass()

def index(request):
    context = RequestContext(request).flatten()
    
    return render(request,'index.html', context)
    
def printme(request):
    context = RequestContext(request).flatten()
    return render(request,'print.html', context)

def landing(request):
    context = RequestContext(request).flatten()
    return render(request,'landing.html', context)
    
def howitworks(request):
    context = RequestContext(request).flatten()
    return render(request,'how-it-works.html', context)
    
#Django REST API classes
class UserViewSet(viewsets.GenericViewSet):
     
    queryset = User.objects.all()
    #serializer_class = UserSerializer
    
    def list(self, request):
    #def get_queryset(self):
      
      user = self.request.user
      retValue=None
      #uep = UserExtendedProperties.objects.get(UserId=user)
      
      #uepSA,langSA,orgSA =  rdbms_cls.get_user_extended_prop(user.id)   
      
      session = rdbms_cls.Session()
      

      UEP_alias = aliased(UserExtendedPropertiesSa,name="uep")
                            
      with session.begin():
          subquery = session.query(UEP_alias.Organisation)\
              .join(auth_user, and_(UEP_alias.UserId == auth_user.id, auth_user.id == user.id) ).subquery()
          
          uepSA = session.query(auth_user )\
          .join(UserExtendedPropertiesSa, auth_user.id == UserExtendedPropertiesSa.UserId)\
          .filter(UserExtendedPropertiesSa.Organisation.in_(subquery)).all()
                       
          #pdb.set_trace()
          
          
          if uepSA:
            #organisation = uepSA.Organisation
            #ToDo SQLAlchemy migration
            #ueps = UserExtendedProperties.objects.filter(Organisation=organisation)
            #userIds=[]
            users=[]
            for usr in uepSA:
              #userIds.append(usrext.UserId)
              u= usr._asdict()
              u['name'] = u['first_name'] + ' ' + u['last_name'] + ' - '+u['username']
              #User.first_name+ ' ' + User.last_name + ' - '+ User.username
              users.append( u)
              
            #ToDo SQLAlchemy migration
            #queryset=User.objects.filter(id__in=userIds)
       
      #pdb.set_trace()
      return Response(data=users)
      #return queryset


class UserExtendedViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users with extended properties to be viewed or edited.
    """
    queryset = UserExtendedProperties.objects.all()
    serializer_class = UserExtendedSerializer
    """
    def get_queryset(self):
      
      
      user = self.request.user
      #ToDo SQLAlchemy migration
      #uep = UserExtendedProperties.objects.get(UserId=user)
      
      if uep is None:
        queryset=None
      else:
        organisation = uep.Organisation
        language = uep.Lang
        #ToDo SQLAlchemy migration
        #queryset = UserExtendedProperties.objects.select_related().filter(Organisation=organisation).order_by('UserId__id')
        
      return queryset
    def get_serializer_context(self):
        user = self.request.user
        #ToDo SQLAlchemy migration
        #uep = UserExtendedProperties.objects.get(UserId=user)
        language = uep.Lang.id
        context = super(UserExtendedViewSet, self).get_serializer_context()
        context.update({"user_lang": language})
        return context
    """
    def list(self,request):

        user = self.request.user
        org_id,lang = helper.get_lang_org(user)
        users_list=[]
        session = rdbms_cls.Session()
        with session.begin():        
            usr= session.query(auth_user,UserExtendedPropertiesSa,auth_user_groups,GroupLangSa).\
                outerjoin(auth_user_groups,auth_user.id==auth_user_groups.user).outerjoin(GroupLangSa,auth_user_groups.group==GroupLangSa.GroupId).\
                join(UserExtendedPropertiesSa,auth_user.id == UserExtendedPropertiesSa.UserId).filter(UserExtendedPropertiesSa.Organisation==org_id).order_by(auth_user.id).all()
            last_user_id=0
            groups = []

            for u in usr:

                if last_user_id==0:
                    uep_dict = u[1]._asdict()
                    uep_dict['UserId'] = u[0]._asdict()
                    if u[3] is not None:
                        group = u[3]._asdict()
                        group['id'] = group['GroupId']
                        group['name'] = group['Name']
                        groups.append(group)
                        uep_dict['UserId']['groups'] = groups
                elif last_user_id>0 and last_user_id!=u[0].id:
                    users_list.append(uep_dict)
                    groups = []
                    uep_dict = u[1]._asdict()
                    uep_dict['UserId'] = u[0]._asdict()
                    
                    if u[3] is not None:
                        group = u[3]._asdict()
                        group['id'] = group['GroupId']
                        group['name'] = group['Name']
                        groups.append(group)
                    uep_dict['UserId']['groups'] = groups
                else:
                    if u[3] is not None:
                        group = u[3]._asdict()
                        group['id'] = group['GroupId']
                        group['name'] = group['Name']
                        groups.append(group)
                    
                
                last_user_id=u[0].id

            users_list.append(uep_dict)
        return Response(data=users_list)

class GroupViewSet(viewsets.ViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    #ToDo SQLAlchemy migration - DONE
    queryset = Group.objects.all()
    #serializer_class = GroupSerializer
    
    def list(self, request):
    #def get_queryset(self):   
        
        lang = None
        queryset=None
        session = rdbms_cls.Session()           
        with session.begin(): 
            user = self.request.user  
            uepSA,langSA,orgSA = rdbms_cls.get_user_extended_prop(user.id)  # UserExtendedPropertiesSa
            #uepSa = session.query(AdminGroups, auth_group).join(AdminGroups,auth_group.id==AdminGroups.group_id).filter(AdminGroups.resourcedef_id==self.id ).first()
  
            #ToDo SQLAlchemy migration - DONE
            #uep = UserExtendedProperties.objects.get(UserId=user)
            retvalue=[]
            #if uep:
            #  organisation = uep.Organisation
            #  lang = uep.Lang
            #  #ToDo SQLAlchemy migration - DONE
            #  queryset = Group.objects.filter(GroupExtendedProperties_groupid__Organisation=organisation)\
            #  .prefetch_related(Prefetch('GroupLang_groupid',GroupLang.objects.filter(Lang=lang))).select_related()
                
            if uepSA:
                organisation = uepSA.Organisation
                lang = uepSA.Lang
                #pdb.set_trace()
                result = session.query(GroupExtendedPropertiesSa, GroupLangSa )\
                    .join(GroupLangSa , and_(GroupLangSa.GroupId == GroupExtendedPropertiesSa.GroupId, GroupLangSa.Lang == lang ) )\
                    .filter(GroupExtendedPropertiesSa.Organisation == organisation)\
                    .all()

                for (g, glang) in result:
                    #gg.append((g[0]._asdict(), g[1]._asdict()))
                    retvalue.append({'id':glang.GroupId , 'name': str(organisation) + '_' + glang.Name})
            
            #pdb.set_trace()
            return Response(data=retvalue)
            #return queryset
    
    
class GroupExtendedViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups with extended properties to be viewed or edited.
    """
    #ToDo SQLAlchemy migration
    queryset = GroupExtendedProperties.objects.all()
    serializer_class = GroupExtendedSerializer
    """
    def get_queryset(self):
      
      
      user = self.request.user
      #ToDo SQLAlchemy migration
      #uep = UserExtendedProperties.objects.get(UserId=user)
      
      if uep is None:
        queryset=None
      else:
        organisation = uep.Organisation
        language = uep.Lang
        #ToDo SQLAlchemy migration
        #queryset = GroupExtendedProperties.objects.select_related('GroupId').filter(Organisation=organisation).order_by('GroupId__id')#.prefetch_related(Prefetch('groups_set__user_set', queryset=User.objects.all()))
        
      return queryset
    
    def get_serializer_context(self):
        user = self.request.user
        #ToDo SQLAlchemy migration
        #uep = UserExtendedProperties.objects.get(UserId=user)
        language = uep.Lang
        context = super(GroupExtendedViewSet, self).get_serializer_context()
        context.update({"user_lang": language})
        return context
    """
    def list(self,request):

        user = self.request.user
        org_id,lang = helper.get_lang_org(user)
        groups_list=[]
        session = rdbms_cls.Session()
        with session.begin():
            #group_owners_sub = session.query(auth_user_groups,GroupExtendedPropertiesSa).join(GroupExtendedPropertiesSa,auth_user_groups.group_id==GroupExtendedPropertiesSa.GroupId).subquery()
            groups= session.query(auth_group,GroupExtendedPropertiesSa,GroupLangSa,auth_user_groups,auth_user).join(GroupExtendedPropertiesSa,auth_group.id == GroupExtendedPropertiesSa.GroupId).\
                        outerjoin(auth_user_groups,auth_user_groups.group == auth_group.id).outerjoin(auth_user,auth_user.id==auth_user_groups.user).join(GroupLangSa,and_(auth_group.id==GroupLangSa.GroupId,GroupLangSa.Lang==lang.id)).\
                        filter(GroupExtendedPropertiesSa.Organisation==org_id).order_by(auth_group.id).all()
            last_group_id = 0
            users=[]
            for group in groups:

                if last_group_id==0:
                    gep_dict = group[1]._asdict()
                    gep_dict['GroupId'] = group[0]._asdict()
                    gep_dict['GroupName']=group[2].Name
                    gep_dict['GroupOwners']=[]
                    for owner in group[1].GroupOwners:
                        gep_dict['GroupOwners'].append(owner._asdict())
                    if group[4] is not None:
                        user = group[4]._asdict()
                        #user['id'] = user['UserId']
                        user['name'] = user['first_name']+' '+user['last_name']+' - '+user['username']
                        users.append(user)
                        gep_dict['GroupId']['users'] = users
                elif last_group_id>0 and last_group_id!=group[0].id:
                    groups_list.append(gep_dict)
                    users = []
                    gep_dict = group[1]._asdict()
                    gep_dict['GroupId'] = group[0]._asdict()
                    gep_dict['GroupName']=group[2].Name
                    gep_dict['GroupOwners']=[]
                    for owner in group[1].GroupOwners:
                        gep_dict['GroupOwners'].append(owner._asdict())
                    if group[4] is not None:
                        user = group[4]._asdict()
                        user['name'] = user['first_name']+' '+user['last_name']+' - '+user['username']
                        users.append(user)
                    gep_dict['GroupId']['users'] = users
                else:
                    if group[4] is not None:
                        user = group[4]._asdict()
                        #user['id'] = user['UserId']
                        user['name'] = user['first_name']+' '+user['last_name']+' - '+user['username']
                        users.append(user)
                last_group_id=group[0].id

            groups_list.append(gep_dict)

        return Response(data=groups_list)

class SaveUserViewSet(viewsets.GenericViewSet):
    #ToDo SQLAlchemy migration
    queryset = UserExtendedProperties.objects.all() 
    #serializer_class = GetNavbarSerializer
   
    def create(self, request):
    
        user = request.user
        euser = request.data
        euser_id = None
        password = None
        groups_dict = []
    
        if 'UserId' in euser:
            if 'id' in euser['UserId']:
                euser_id=euser['UserId']['id']    
        username = euser['UserId']['username']  
        if 'password' in euser['UserId']:
            password = euser['UserId']['password']      
            
        session = rdbms_cls.Session()
        with session.begin():
        
            email = euser['UserId']['email']
            fname = euser['UserId']['first_name']
            lname = euser['UserId']['last_name']
            
            helper = ResourceHelperClass()
            org_id,language = helper.get_lang_org (user) 
            lang = language.id
                
            #pdb.set_trace()
            #get or create user
            if euser_id is not None:
                #ToDo SQLAlchemy migration
                #usr = User.objects.get(id=euser_id)
                usr = session.query(auth_user).where(auth_user.id == euser_id).first()
                created = None
            else:
                #ToDo SQLAlchemy migration
                #usr, created = User.objects.get_or_create(username=username)
                usr = session.query(auth_user).where(auth_user.username == username).first()
                created = None
                if not usr:
                        usr = auth_user(username=username)
                        session.add(usr)
                        created = True                                       
            #if user created or found
            if usr:
                #if created set password and other properties
                if not created and euser_id is None:
                    return Response (data={'data':euser,'msg': GetMessage('SaveUser-1',language.Code,[username]) or 'Username '+username+' allready exists'},status=500)
                
                if created and euser_id is None:
                    if password:
                        #usr.set_password(password) # This line will hash the password
                        usr.password = crypto.make_password(password)
                    else:
                        return Response (data={'data':euser,'msg':GetMessage('SaveUser-2',language.Code) or 'Password not set'},status=500)
                    usr.is_superuser=False;
                
                
                #update other properties  
                #check for username
                msg_username=""
                if usr.username!=username:
                    #ToDo SQLAlchemy migration DONE 
                    #chk_user = User.objects.filter(username=username)
                    chk_user = session.query(auth_user).where(auth_user.username == username).first()
                    
                    if len(chk_user)==0:
                        usr.username = username    
                    else:
                        msg_username=GetMessage('SaveUser-3',language.Code) or ". Username could not be changed"
                
                usr.email = email
                usr.first_name = fname
                usr.last_name = lname
                usr.is_staff = False
                usr.is_active = True
                usr.date_joined = datetime.now(timezone.utc)
                #usr.groups.set(groups) MIGOR TODO - nisam siguran da li ovo treba ili je odradjeno s ovim dole
                
                session.flush()

                #SETTING GROUPS
                query = session.query(auth_user_groups).filter(auth_user_groups.user == usr.id ).delete()
                groups=[]
                if 'groups' in euser['UserId']:
                    groups_dict = euser['UserId']['groups']                
                    for gr in groups_dict:
                        groups.append(gr['id'])
                        aug = auth_user_groups( user = usr.id , group= gr['id'])
                        session.add(aug)            
                #usr.save()         
            else:     
                return Response (data={'data':euser,'msg':GetMessage('SaveUser-4',language.Code) or 'Error creating or retrieving user'},status=500)  
       
            #get or create  UserExtendedProperties
            #ToDo SQLAlchemy migration
            #uep,e_created = UserExtendedProperties.objects.get_or_create(UserId_id=usr.id,
            #                                                            defaults={'Organisation_id': org_id,'HideCustomizeHome' : False,'HideCreateResourceHome' : False,'HideAppsHome' : False,
            #                                                             'HideSearchResourceHome' : False,'Lang_id' : lang,'CreatedDateTime':timezone.now(),
            #                                                             'CreatedBy_id':user.id,'APIUser':False})    

            uep = session.query(UserExtendedPropertiesSa).where(UserExtendedPropertiesSa.UserId == usr.id).first()
            e_created = None
            if not uep:
                        uep = UserExtendedPropertiesSa(UserId = usr.id,  Organisation=org_id, HideCustomizeHome = False, HideCreateResourceHome = False, HideAppsHome = False,
                                     HideSearchResourceHome = False,Lang = lang, CreatedDateTime=timezone.now(), CreatedBy = user.id, APIUser=False)
                        session.add(uep) 
                        e_created = True        
                        
            #if successful set msg and save if required 
            if uep:
                if not e_created:
                    uep.Lang_id = lang
                    uep.ChangedBy = user.id
                    uep.ChangedDateTime = timezone.now()
                    #uep.save()
                    msg=GetMessage('SaveUser-5',language.Code)+msg_username or 'User saved successfuly'+msg_username
                else:
                    msg_username=GetMessage('SaveUser-3',language.Code) or ". Username could not be changed"
            
            usr.email = email
            usr.first_name = fname
            usr.last_name = lname
            usr.groups.set(groups)
            usr.save() 
            
        #else:
            
            #return Response (data={'data':euser,'msg':GetMessage('SaveUser-4',language.Code) or 'Error creating or retrieving user'},status=500)  
   
        #get or create  UserExtendedProperties
        #ToDo SQLAlchemy migration
        #uep,e_created = UserExtendedProperties.objects.get_or_create(UserId_id=usr.id,
        #                                                            defaults={'Organisation_id': org_id,'HideCustomizeHome' : False,'HideCreateResourceHome' : False,'HideAppsHome' : False,
        #                                                           'HideSearchResourceHome' : False,'Lang_id' : lang,'CreatedDateTime':timezone.now(),
        #                                                          'CreatedBy_id':user.id,'APIUser':False}) 
        s_users_all = []
        session=rdbms_cls.Session()
        uep_object=None
        with session.begin():

            uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.UserId==usr.id).first()

            #if successful set msg and save if required 
            if uep:
                uep.Lang = lang
                uep.ChangedBy = user.id
                uep.ChangedDateTime = datetime.now(timezone.utc)
                msg=GetMessage('SaveUser-5',language.Code)+msg_username or 'User saved successfuly'+msg_username
            else:
                uep = UserExtendedPropertiesSa(UserId=usr.id,Organisation= org_id,HideCustomizeHome=False,HideCreateResourceHome=False,HideAppsHome= False,
                                                HideSearchResourceHome=False,Lang= lang,CreatedDateTime=datetime.now(timezone.utc),
                                                CreatedBy=user.id,APIUser=False)
                session.add(uep)
                session.flush()
                msg=GetMessage('SaveUser-6',language.Code) or 'User created successfuly'
            #Error creating or updating UserExtendedProperties
            #else:
            #   return Response (data={'data':euser,'msg':GetMessage('SaveUser-7',language.Code) or 'Error creating user extended properties'},status=500)
        
            #ToDo SQLAlchemy migration
            #u_ser = UserExtendedSerializer (uep,context={'request':request,'user_lang':lang})
            
            uep_object = uep._asdict()
            user_obj={'id':usr.id,'first_name':usr.first_name,'last_name':usr.last_name,'username':usr.username,'Lang':language._asdict()}
            uep_object['UserId'] = user_obj
            #get all users
            #ueps = UserExtendedProperties.objects.filter(Organisation_id=org_id).order_by('UserId__id')
            ueps = session.query(UserExtendedPropertiesSa,auth_user).join(auth_user,auth_user.id==UserExtendedPropertiesSa.UserId).filter(UserExtendedPropertiesSa.Organisation==org_id).order_by(UserExtendedPropertiesSa.UserId).all()
            userIds=[]
            for u in ueps:
              u_o = u[0]._asdict()
              u_o['UserId'] = u[1]._asdict()
              s_users_all.append(u_o)
        
        return Response (data={'user':uep_object,'users':s_users_all,'msg':msg},status=200)

class SaveGroupViewSet(viewsets.GenericViewSet):
  #ToDo SQLAlchemy migration
  queryset = GroupExtendedProperties.objects.all() 
  #serializer_class = GetNavbarSerializer
   
  def create(self, request):
    
    session = rdbms_cls.Session()
    user = request.user
    group = request.data  
    user_ids=[]
    uep = None
    group_owners_list=[]
    group_owners_ids=[]
    group_id = None
    
    if 'GroupId' in group:
        if 'users' in group['GroupId']:
            users_object = group['GroupId']['users']
            for usr in users_object:
                user_ids.append(usr['id'])    
        if 'id' in group['GroupId']:
            group_id=group['GroupId']['id'] 
    if 'GroupOwners' in group:
        group_owners_list=group['GroupOwners']
                   
    for go in group_owners_list:
      group_owners_ids.append(go['id'])
    
    with session.begin():
        uep = session.query(UserExtendedPropertiesSa).where(UserExtendedPropertiesSa.UserId == user.id).first()  
        org_id = uep.Organisation
        lang = uep.Lang

        #grp,created = Group.objects.get_or_create(id=group_id,defaults={'name':str(org_id)+'_'+group['GroupName']})
        created = None
        if group_id:   
            grp = session.query(auth_group).where(auth_group.id == group_id).first()
        else:
            grp = session.query(auth_group).where(auth_group.name == str(org_id)+'_'+group['GroupName']).first()   
        if not grp:
                grp = auth_group(name=str(org_id)+'_'+group['GroupName'])
                session.add(grp) 
                created = True  
                session.flush()
        #pdb.set_trace()
        if grp and not created:  
          grp.name=str(org_id)+'_'+group['GroupName']
          #grp.user_set.set(user_ids)
          query = session.query(auth_user_groups).filter(auth_user_groups.group == grp.id ).delete()
          for uid in user_ids:
             new = auth_user_groups ( user = uid , group = grp.id)
             session.add(new)
          
          #grp.save()
          #try:
          #grp_lang = GroupLang.objects.get(GroupId=grp,Lang=uep.Lang)
          grp_lang = session.query(GroupLangSa).filter(GroupLangSa.GroupId == grp.id , GroupLangSa.Lang == uep.Lang).first()
          if grp_lang:
              grp_lang.Name = group['GroupName']
              grp_lang.Description = group['GroupName']
              grp_lang.ChangedBy = user.id
              grp_lang.ChangedDateTime = datetime.now(timezone.utc)
              #grp_lang.save()
          else:
              new = GroupLangSa(GroupId= grp.id, Lang=uep.Lang, Name = group['GroupName'], Description = group['GroupName'], CreatedBy = user.id, CreatedDateTime = datetime.now(timezone.utc)  )   
              session.add(new)
              #session.commit()
                       
          #except GroupLang.DoesNotExist:
          #  #ToDo SQLAlchemy migration
          #  grp_lang = GroupLang.objects.create(GroupId=grp,Lang=uep.Lang,Name = group['GroupName'],Description = group['GroupName'],CreatedBy = user,CreatedDateTime = timezone.now())    
          
          #ToDo SQLAlchemy migration
          #geps = GroupExtendedProperties.objects.filter(GroupId=grp)
          gep = session.query(GroupExtendedPropertiesSa).where(GroupExtendedPropertiesSa.GroupId==grp.id).first()
          #pdb.set_trace()
          #for gep in geps.all():
            #gep.GroupOwners.set(group_owners_ids)
          if gep:
              session.query(group_owners).filter(group_owners.groupextendedproperties == gep.id).delete()
              for owner_id in group_owners_ids:
                    new = group_owners(groupextendedproperties = gep.id, user = owner_id )
                    session.add(new)
               
              gep.ChangedBy = user.id
              gep.ChangedDateTime = timezone.now()
              #gep.save()
              msg='Group saved successfuly'
        elif grp and created:
          #grp.user_set.set(user_ids)
          #grp.save()
          session.query(auth_user_groups).where(auth_user_groups.group == grp.id).delete()
          for uid in user_ids:
             new = auth_user_groups ( user = uid , group = grp.id)
             session.add(new)
          
          #ToDo SQLAlchemy migration
          #grp_lang = GroupLang.objects.create(GroupId=grp,Lang=uep.Lang,Name = group['GroupName'],Description = group['GroupName'],CreatedBy = user,CreatedDateTime = timezone.now())
          new = GroupLangSa(GroupId= grp.id,Lang=uep.Lang, Name = group['GroupName'],Description = group['GroupName'],CreatedBy = user.id,CreatedDateTime = timezone.now())
          session.add(new)
          
          #ToDo SQLAlchemy migration
          #gep,created = GroupExtendedProperties.objects.get_or_create(GroupId=grp,Organisation = uep.Organisation,CreatedDateTime=timezone.now(),CreatedBy_id=user.id)
          gep = session.query(GroupExtendedPropertiesSa).where(GroupExtendedPropertiesSa.GroupId==grp.id).first()

          created = None
          if not gep:
                 gep = GroupExtendedPropertiesSa(GroupId= grp.id, Organisation = uep.Organisation,CreatedDateTime=timezone.now(),CreatedBy=user.id)
                 session.add(gep)
                 created = True 
                 session.flush()
          #pdb.set_trace() 
          if created and gep:
                #gep.GroupOwners.set(group_owners_ids)
                session.query(group_owners).filter(group_owners.groupextendedproperties == gep.id).delete()
                for owner in group_owners_ids:
                      new = group_owners(groupextendedproperties = gep.id, user = owner )
                      session.add(new) 
                
                #gep.save()
          elif not created and grp:
            #ToDo SQLAlchemy migration
            #gep,created = GroupExtendedProperties.objects.get_or_create(GroupId=grp,Organisation = uep.Organisation,CreatedDateTime=timezone.now(),CreatedBy_id=user.id)
            gep = session.query(GroupExtendedPropertiesSa).where(GroupExtendedPropertiesSa.GroupId==grp.id).first()
            created = None
            if not gep:
                  gep = GroupExtendedPropertiesSa(GroupId=grp.id, Organisation = uep.Organisation,CreatedDateTime=timezone.now(),CreatedBy=user.id)
                  session.add(gep)
                  created = True
                
            if created and gep:
                  #gep.GroupOwners.set(group_owners_ids)
                  #gep.save()
                  session.query(group_owners).where(group_owners.groupextendedproperties == gep.id).delete()
                  for owner in group_owners_ids:
                      new = group_owners(groupextendedproperties = gep.id, user = owner )
                      session.add(new)                
              
            elif not created and gep:
                  #gep.GroupOwners.set(group_owners_ids)
                  session.query(group_owners).where(group_owners.groupextendedproperties == gep.id).delete()
                  for owner in group_owners_ids:
                      new = group_owners(groupextendedproperties = gep.id, user = owner.id )
                      session.add(new)    
                       
                  gep.ChangedDateTime = timezone.now()
                  gep.ChangedBy_id = user.id
                  #gep.save()
            
          else:
            return Response (data={'data':group,'msg':'Error creating group extended properties'},status=500)
          
          msg='Group created successfuly'
        
        else:
           
           return Response (data={'data':group,'msg':'Error creating group'},status=500)
        
        #g_ser = GroupExtendedSerializer (gep,context={'request':request,'user_lang':lang})
        #get all groups
        if not uep:
            #uep = UserExtendedProperties.objects.get(UserId_id=user.id)
            uep = session.query(UserExtendedPropertiesSa).where(UserExtendedPropertiesSa.UserId==user.id).first()
            
        #ToDo SQLAlchemy migration
        #geps = GroupExtendedProperties.objects.filter(Organisation=uep.Organisation).order_by('GroupId_id')
        geps = session.query(GroupExtendedPropertiesSa).where(GroupExtendedPropertiesSa.Organisation == uep.Organisation)
        groupIds=[]
        for g in geps.all():
            groupIds.append(g.GroupId)
        
        # ToDo SQLAlchemy migration
        #groups_all=Group.objects.filter(id__in=groupIds)
        groups_all= session.query(auth_group).where(auth_group.id.in_(groupIds))
        groups_all_json = [] 
        for g in groups_all:
               groups_all_json.append(g._asdict())
        #s_groups_all = GroupSerializer(groups_all,many=True,context={'request':request,'user_lang':lang})
      
        msg='Success!'
        return Response (data={'group': grp._asdict(),'groups':groups_all_json ,'msg':msg },status=200)

class OrganisationsViewSet(viewsets.ModelViewSet):
    #ToDo SQLAlchemy migration
    queryset = Organisations.objects.all()
    serializer_class = OrganisationsSerializer
    
    
class LoginViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)
    #authentication_classes = ()
    
    def create(self, request):
      
        username = request.data['username']
        password = request.data['password']
        session = rdbms_cls.Session()
        with session.begin():
      
            
            userSA = session.query(auth_user).where(auth_user.username == username).first()
            isauthenticated =  hashers.check_password(password, userSA.password)  
            
        #if BACKEND_SESSION_KEY=='django_remote_auth_ldap.backend.RemoteUserLDAPBackend':
        #    username =  request.META['REMOTE_USER']
        #    user = authenticate(remote_user=username)
        #else:  
        user = authenticate(username=username, password=password)
       
        if user is not None:
            if user.is_active:
            
                login(request, user)
                session = rdbms_cls.Session()
                with session.begin():
                    uep,lang,org = session.query(UserExtendedPropertiesSa, LanguageSa, OrganisationsSa)\
                    .outerjoin(LanguageSa,LanguageSa.id == UserExtendedPropertiesSa.Lang)\
                    .outerjoin(OrganisationsSa, OrganisationsSa.id == UserExtendedPropertiesSa.Organisation)\
                    .filter(UserExtendedPropertiesSa.UserId == user.id).first()
                
                    #token can't be replaced with SQL Alchemy as it actually creates the token with this command
                    token,created = Token.objects.get_or_create(user=user)

                    #token = session.query(TokenSa).filter(TokenSa.user_id==user.id).first()
                
                    #if not token:
                        #token  =session.add(TokenSa(user_id=user.id,created=datetime.now(timezone.utc)))
                        #created = True
            
            
            
                    if token: 
                    #serialize user object
                        response = {'userid':user.id,'first_name': user.first_name,'last_name': user.last_name,'email':user.email,'is_superuser':user.is_superuser,
                                  'last_login':user.last_login,'username':user.username,'is_active':user.is_active,'token': token.key} #,'groups':user.get_extended_groups()}
                    else:
                        return Response(status=401)
            
                    if uep:
                        
                        
                        #searialize language
                        self.kwargs.update({'lang':lang})
                        
                        uep_object = uep._asdict()
                        if uep_object['Params'] is None:
                            uep_object['Params'] = {}
                        elif type(uep_object['Params'])!=dict:
                            uep_object['Params'] = json.loads(uep_object['Params'])
                        
                        org_params = None
                        advanced_user_props_def = None
                        if org.Parameters is not None:
                            org_params = json.loads(org.Parameters)
                            advanced_user_props_def = org_params.get('AdvancedUserPropertiesDef',None)
                            

                        if advanced_user_props_def is None:
                            advanced_user_props_def = []

                        uep_object['AdvancedProperties']={}
                        #process advanced params
                        for param in advanced_user_props_def:
                            #get advanced user params
                            resource_id = param.get('resourceId',None)   
                            if resource_id is not None:
                                #get advanced properties record
                                resource_object = dataset(resource_id,param.get('resourceType','dataset'),'','','','',user,lang,org.id,{},None,param.get('resourceSubType','standard'))
                                resource_object.set_session(session)
                                records = resource_object.get_dataset_data_filtered({param.get('UserIdField',''):user.id},[],[],{})
                                if len(records)>0:
                                    uep_object['AdvancedProperties'].update(records[0])

                                #uep_object['Params'].update({'AdvancedProperties': uep_object['AdvancedProperties']})
                        
                        uep.AdvancedParams =  json.dumps({'AdvancedProperties':uep_object['AdvancedProperties']},default=str)
                        del uep_object['AdvancedParams']
                        #task_list = GetResourceDefByIdLangSerializer(uep[0]._prefetched_objects_cache['TaskListHome'],many=True)
                  
                        #uep_object.update({'TaskListHome':task_list.data})
                        ktlyo_bal = 0
                        ktlyo_block = 0
                        eth_address = org.ETHAccount
                        if USE_BLOCKCHAIN:
                            uep_object.update({'use_blockchain':True})
                            if eth_address is not None:
                                ktlyo_block,ktlyo_bal = get_ktlyo_balance(eth_address)
                                uep_object.update({'ktlyo_balance':ktlyo_bal,'ktlyo_block':ktlyo_block})
                        else:
                            uep_object.update({'use_blockchain':False})
                        
                        org_dict = org._asdict()
                        if org_params is not None:
                            org_dict['DefaultApp'] = org_params.get('DefaultApp',org_params)
                        response.update({'uep':uep_object,'lang':lang._asdict(),'organisation':org_dict,'auth_type':'token'})

                        #put user into session (session does not work with token auth)
                        request.session['OrganisationId'] = org.id
                        request.session['UserLang'] = lang.Code
                        request.session['UserLangId'] = lang.id
                        request.session['UserLangObject'] = lang._asdict()
                
                return Response(data=response,status=200)
           
            else:
                # Return a 'disabled account' error message
                return Response(status=403)
        else:
            # Return an 'invalid login' error message.
            return Response(status=401)

class LoginSsoViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    #permission_classes = (permissions.AllowAny,)
    
    def create(self, request):
      
      #pdb.set_trace()
      user = request.user
      
      if user is not None:
        if user.is_active:
            
            login(request, user)
           
            #get extended properties
            #uep = UserExtendedProperties.objects.filter(UserId=user).select_related().prefetch_related('TaskListHome')
            uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.UserId==user.id).first()
            response = {'userid':user.id,'first_name': user.first_name,'last_name': user.last_name,'email':user.email,'is_superuser':user.is_superuser,
                                  'last_login':user.last_login,'username':user.username,'is_active':user.is_active,'groups':user.groups.values()}
            if uep:
             
              #lang = GetCodesDetailByIdLangSerializer(uep.Lang)
              #org = OrganisationsSerializer(uep[0].Organisation,context={'request':request})
              lang = uep.Lang
              org = uep.Organisation
              
              uep_object = uep._asdict()
              if uep_object['Params'] is None:
                uep_object['Params'] = {}
              elif type(uep_object['Params'])!=dict:
                uep_object['Params'] = json.loads(uep_object['Params'])
              
              #task_list = GetResourceDefByIdLangSerializer(uep[0]._prefetched_objects_cache['TaskListHome'],many=True)
              
              #uep_object.update({'TaskListHome':task_list.data})
              ktlyo_bal = 0
              ktlyo_block = 0
              eth_address = org.data['ETHAccount']
              if USE_BLOCKCHAIN and eth_address is not None:
                #eth_address = org.data['ETHAccount']
                #pdb.set_trace()
                ktlyo_block,ktlyo_bal = get_ktlyo_balance(eth_address)
                uep_object.update({'ktlyo_balance':ktlyo_bal,'ktlyo_block':ktlyo_block,'use_blockchain':True})
              else:
                uep_object.update({'use_blockchain':False})
              #uep_object.update({'BlockchainAddress':org.data['ETHAccount']})
              response.update({'uep':uep_object,'lang':lang.data,'organisation':org.data,'auth_type':'sso'})
              #put user into session
              request.session['OrganisationId'] = org.data['id']
              request.session['UserLang'] = lang.data['value']
              request.session['UserLangId'] = lang.data['id']
            return Response(data=response,status=200)
           
        else:
            # Return a 'disabled account' error message
            return Response(status=403)
      else:
        # Return an 'invalid login' error message.
        return Response(status=401)

   
class LogoutViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer
    #ToDo SQLAlchemy migration
    queryset = User.objects.all()
    #permission_classes = (permissions.AllowAny,)
    #authentication_classes = ()
    
    def create(self, request):
      
      user = request.user
  
      #deleted = Token.objects.filter(user=user).delete()
      session=rdbms_cls.Session()
      with session.begin():
          deleted=session.query(TokenSa).filter(TokenSa.user_id==user.id).delete()
          if not deleted:
            logout(request)
            return Response(data='Token not found error',status=500)  
          logout(request)
      
      return Response(status=200)

class ChangePasswordRequestViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer
    #queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()

    def create(self, request):
        
        email = request.data['email']

        users = session.query(auth_user).filter(auth_user.email==email).all()

        if len(users)==1:
                usr=users[0]
                uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.UserId==usr.id).first()
                #generate hash
                hasher = hashlib.sha512()
                guid = str(uuid.uuid4()).encode('utf-8')
                hasher.update(guid) 
                digest=hasher.hexdigest()
                #update uep
                validDate =  timezone.now() + timezone.timedelta(minutes=600)
                uep.ChangePassDigest = digest
                uep.ChangePassValid = validDate
                uep.ChangePassRequested = True
                #send email
                msg = "Request to change the password was successful! Please check your e-mail."
                return Response(data={'msg':msg,'digest':digest},status=200)
        elif len(users)>1:
            return Response(data={'msg':'Multiple users found for email!'},status=500) 
        else:
            return Response(data={'msg':'Invalid request - email not found!'},status=500) 

class ResetPasswordRequestViewSet(viewsets.GenericViewSet):
  
    
    def list(self, request,userId):
        
        
        rdbms_cls = RdbmsClass()
        session = rdbms_cls.Session()
        with session.begin():

            usr = session.get(auth_user,userId)
        
            if usr is not None:
                
                uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.UserId==usr.id).first()
                #generate hash
                hasher = hashlib.sha512()
                guid = str(uuid.uuid4()).encode('utf-8')
                hasher.update(guid) 
                digest=hasher.hexdigest()
                #update uep
                validDate =  datetime.now(timezone.utc) + timezone.timedelta(minutes=600)
                uep.ChangePassDigest = digest
                uep.ChangePassValid = validDate
                uep.ChangePassRequested = True
                msg = "Request to reset the password was successful!"
                return Response(data={'msg':msg,'digest':digest},status=200)
            elif len(users)>1:
                return Response(data={'msg':'Multiple users found!'},status=500) 
            else:
                return Response(data={'msg':'Invalid request - user not found!'},status=500) 

class CheckChangePasswordViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer
    #queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)
    authentication_classes = ()
    
    def create(self, request):
      
        digest = request.data['digest']
        lang = request.data.get('lang','hr-HR')
        #pdb.set_trace() #CHANGEPASS #DBG
        #ToDo SQLAlchemy migration
        #uep = UserExtendedProperties.objects.filter(ChangePassDigest=digest, ChangePassRequested = True)
        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()
        
        with session.begin():

            uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.ChangePassDigest==digest,UserExtendedPropertiesSa.ChangePassRequested==True).all()
          
            if len(uep)==0:
                return Response(data={'msg':GetMessage('CheckChangePassword-1',lang) or 'Invalid password change request!'},status=500)
            elif len(uep)>1:
                return Response(data={'msg':GetMessage('CheckChangePassword-2',lang) or 'Error: digest is not unique!'},status=500)
            elif len(uep)==1:
                if uep[0].ChangePassValid < datetime.now(timezone.utc):
                    return Response(data={'msg':GetMessage('CheckChangePassword-3',lang) or 'Expired password request!'},status=500)
                else:
                    return Response(data={'user_id':uep[0].UserId},status=200)
            
        return Response(data={'msg':GetMessage('CheckChangePassword-4',lang) or 'Improbable error!'},status=500)

class ChangePasswordWithAuthViewSet(viewsets.GenericViewSet):
    serializer_class = UserSerializer
    
    def create(self, request):
      
        pass1 = request.data['password1']
        pass2 = request.data['password2']
        #lang = request.data['lang']
        if (pass1==pass2):
            return Response(data={'msg':'Passwords is the same as current! Plase select a different password!'},status=500)    
        
        user = request.user
        if user:
            usr = User.objects.get(id=user.id)
            usrSa = session.query(auth_user).where(auth_user.id == user.id).first()
            #def check_password(password, encoded, setter=None, preferred='default'):
            if usr.check_password(pass1):
                usr.set_password(pass2) 
                usr.save()
                novipsw = make_password(pass2)
                update_session_auth_hash(request, user)  # Important!
                #ToDo SQLAlchemy migration
                t_del=Token.objects.filter(user=usr).delete()
                token = Token.objects.create(user=usr)
                if token:
                    return Response(data={'token': token.key,'msg':'Password changed successfuly!'},status=200)
                else:
                    return Response(data={'msg':'Error generating auth token!'},status=400) 
            else:
                return Response(data={'msg':'Invalid password!'},status=400) 
        else:
            return Response(data={'msg':'Invalid request - authentication is required!'},status=400)  

class ChangePasswordViewSet(viewsets.GenericViewSet):
    #ToDo SQLAlchemy migration
    serializer_class = UserSerializer
    #queryset = User.objects.all()
    permission_classes = (permissions.AllowAny,)
    #authentication_classes = ()
    
    def create(self, request):
      
        username = request.data['username']
        pass1 = request.data['password1']
        pass2 = request.data['password2']
        user_id = int(request.data['user_id'])
        digest = request.data['digest']
        r_type = int(request.data['type'])
        lang = request.data.get('lang','hr-HR')
        #pdb.set_trace() #CHANGEPASS #DBG
        rdbms_cls = RdbmsClass()
        session = rdbms_cls.Session()

        with session.begin():
        
            if r_type==0:
        
                if (pass1!=pass2):
                    return Response(data={'msg':GetMessage('ChangePassword-1',lang) or 'Passwords must match!'},status=500)    
            
                uep = session.query(UserExtendedPropertiesSa).filter(UserExtendedPropertiesSa.ChangePassDigest==digest,UserExtendedPropertiesSa.ChangePassRequested==True).all()
          
                #uep = UserExtendedProperties.objects.filter(ChangePassDigest=digest,ChangePassValid__gt=timezone.now(), ChangePassRequested = True)
            
                if len(uep)==0:
                    return Response(data={'msg':GetMessage('ChangePassword-2',lang) or 'Invalid password change request!'},status=500)  
                elif len(uep)>1:
                    return Response(data={'msg':GetMessage('ChangePassword-3',lang) or 'Internal error, multiple users with same digest!'},status=500)  
                else:
                    try:
                        user = User.objects.get(id=user_id,username=username)
                    except:
                        return Response(data={'msg':GetMessage('ChangePassword-4',lang) or 'Password change failed!'},status=500)
                    if user:
                        user.set_password(pass1) 
                        uep[0].ChangePassDigest = ''
                        uep[0].ChangePassRequested = False
                        uep[0].ChangePassValid = None
                        uep[0].LastPasswordChange = datetime.now(timezone.utc)
                        user.save()
                        return Response(data={'msg':GetMessage('ChangePassword-5',lang) or 'Password changed successfuly!'},status=200)
    
                return Response(data={'msg':GetMessage('ChangePassword-4',lang) or 'Password change failed!'},status=500)
            else:
                if (pass1==pass2):
                    return Response(data={'msg':GetMessage('ChangePassword-6',lang) or 'Passwords is the same as current! Plase select a different password!'},status=500)    
            
                user = request.user
                if user:
                    #ToDo SQLAlchemy migration
                    usr = User.objects.get(id=user.id)
                    if usr.check_password(pass1):
                        usr.set_password(pass2) 
                        uep = UserExtendedProperties.objects.filter(UserId=usr)
                        if uep:
                            uep[0].ChangePassDigest = ''
                            uep[0].ChangePassRequested = False
                            uep[0].ChangePassValid = None
                            uep[0].LastPasswordChange = timezone.now()
                            
                            usr.save()
                            update_session_auth_hash(request, user)  # Important!
                            #ToDo SQLAlchemy migration
                            t_del=Token.objects.filter(user=usr).delete()
                            token = Token.objects.create(user=usr)
                            if token:
                                return Response(data={'token': token.key,'msg':GetMessage('ChangePassword-5',lang) or 'Password changed successfuly!'},status=200)
                            else:
                                return Response(data={'msg':GetMessage('ChangePassword-7',lang) or 'Error generating auth token!'},status=500) 
                    else:
                        return Response(data={'msg':GetMessage('ChangePassword-8',lang) or 'Invalid password!'},status=500) 
                else:
                    return Response(data={'msg':GetMessage('ChangePassword-9',lang) or 'Invalid request - authentication is required!'},status=500)     

   
class ImportResourceViewSet(viewsets.GenericViewSet):
  
    session = rdbms_cls.Session()
    
    def getUserOrgId(self, user):
        #ToDo SQLAlchemy migration
        #ueps = UserExtendedProperties.objects.filter(UserId_id=user.id)
        with session.begin():
             ueps = session.query(UserExtendedPropertiesSa).where(UserExtendedPropertiesSa.UserId == user.id).first()
        org_id=None
        #for uep in ueps:
        org_id = ueps.Organisation_id
        return org_id
        
    def getFileSize(self, fileName):
        document = open(fileName, 'rb')
        document.seek(0, os.SEEK_END)
        size = document.tell()
        document.close
        return size
    
    def getCodes(self, codeheaduniquename):
        #ToDo SQLAlchemy migration 
        #codeHeadId=CodesHead.objects.get(CodeHeadName=codeheaduniquename).id
        with session.begin():
            codeHeadId= session.query(CodesHeadSa).where(CodesHeadSa.CodeHeadName == codeheaduniquename).first.id
        codemap={}
        #ToDo SQLAlchemy migration
        with session.begin():
            query= session.query(CodesDetailSa).filter(CodesDetailSa.CodesHeadId == codeHeadId)
            #for i in CodesDetail.objects.filter(CodesHeadId_id=codeHeadId):       
            for i in query.all():
                codemap[i.id]=i.Name
            return codemap
        
    def getCodesById(self,codeHeadId): 
        codemap={}
        with session.begin():
            query= session.query(CodesDetailSa).filter(CodesDetailSa.CodesHeadId == codeHeadId)
            #for i in CodesDetail.objects.filter(CodesHeadId_id=codeHeadId):   
            for i in query.all():
                codemap[i.id]=i.Name
            return codemap
    
    def getMutantRecordsAsDict(self, resurceId,  org_id):
        datasetRecords=selectedRecords=[]
        queryset =  mutant_get_data(mutant_get_model(resurceId,org_id))
        for item in queryset:
                 datasetRecords.append(item.id)
                 itemDict = model_to_dict(item)
                 selectedRecords.append(itemDict)
        return selectedRecords
        
    def getMapingAndTransform(self, org_id, resourceId, mappingsArray):    #prepare data and transformation mappings
        
        #ToDo SQLAlchemy migration
        #resourceModelDef = ResourceModelDefinition.objects.filter(ResourceDefId=resourceId,Status='Active')
        with session.begin():
            resourceModelDef = session.query(ResourceModelDefinitionSa).filter(ResourceModelDefinitionSa.ResourceDefId==resourceId, ResourceModelDefinitionSa.Status=='Active')
        resourceFields={}
        isUniqueField={}
        for f in resourceModelDef.all():
            resourceFields[f.id]=f.Dbtype
            isUniqueField[f.id]=f.Unique #TODO - iskoristiti poslije da ne bude duplih             
        map={}# map= mapiranje file na resurs, map={'ID': 'id', 'TEKST': 'Field357'}
        tmap={} # tmap - transformacijska relacija,{'miro': 5, 'pero': 3, 'slavko': 4}
        transform={} # transform - {'TEKST': 'usertransform'} koji file field ide na koju transformaciju
        codeMap1={'text':'TextItem1','code':'','group':'','user':'User1_id'} # text->user transformacija, vidi model DatasetTransformDetail 
        codeMap2={'text':'TextItem2','code':'','group':'','user':'User2_id'} #trenutno samo text->user vidi model DatasetTransformDetail 
               
        #pdb.set_trace() #DBG #getMapingAndTransform #IMPORT
        for f in mappingsArray:
            fieldName=transformname=''
            fieldId=f['itemID']
            for cc in f['cells']: #find transform if any
                if 'type' in cc: #ako postoji 'type' kreiraj transformname              
                      if cc['type']=='TRANSFORM':
                          #transformname=cc['name']
                          if 'dataset' in cc['params']:
                                transformname ='related_dataset'
                                transformname ='codes' #TODO - maknuti kad se uvede 'codes' kod odabira transformacije
                          if 'codes' in cc['params']:
                                transformname='codes'                        
                      else: raise  ValidationError('Nepoznat TRANSFORM '+cc['type'])
                else: 
                    fieldName=cc['name']
            #pdb.set_trace()
            if fieldName!='': 
                tmap[fieldName]={}
                map[fieldName]={}
                map[fieldName]['fieldId']=fieldId
                
                if resourceFields[fieldId]=='id':   map[fieldName]['fieldName']='id' #ako polje sadržava id onda je ime polja=id a ne Field123
                else:   map[fieldName]['fieldName']='Field'+str(fieldId) #ako nije id onda mapiramo file kolone na polja (npr Field123)
                    
                if transformname=='codes':
                    map[fieldName]['transform']=transformname
                    for i in CodesDetail.objects.all():                    
                            tmap[fieldName][i.Name]= i.id              
                
                if transformname=='related_dataset':
                    #pdb.set_trace()#IMPORT #MAPPING #TRANSFORM #DBG
                    map[fieldName]['resourceDefId']=cc['params']['dataset']['id']
                    map[fieldName]['transform']=transformname
                    if resourceFields[fieldId]=='manytomanyfielddefinition':     
                        queryset =  mutant_get_data(mutant_get_model(map[fieldName]['resourceDefId'],org_id))    # poslije sve ove komande dole zamjeniti sa getMutantRecordsAsDict
                        datasetRecords=selectedRecords=[]
                        for item in queryset:
                            itemDict = model_to_dict(item)
                            tmap[fieldName][itemDict['Field'+str(cc['params']['field']['id'])].upper()]=item.id # tmap ['Drzava']= [ {'Hrvatska': 1}, {'Austrija': 2} ...
                                              
                if transformname=='usertransform':
                  with session.begin():
                    transform[fieldName]=transformname
                    #ToDo SQLAlchemy migration DONE
                    #t=DatasetTransformHead.objects.get(TransformName=transformname) 
                    t = session.query(DatasetTransformHeadSa).where(DatasetTransformHeadSa.TransformName==transformname).first()
                    typeFrom=codeMap1[CodesDetail.objects.get(id=t.FieldType1_id).Name]# typeFrom: 'TextItem1'
                    typeTo=codeMap2[CodesDetail.objects.get(id=t.FieldType2_id).Name]  # typeTo: 'User2_id'
                    #ToDo SQLAlchemy migration DONE
                    #transformsRAW = DatasetTransformDetail.objects.filter(TransformationHeadId_id=t.id).select_related('CodeDetail1','CodeDetail2').order_by('id')
                    transformsRAW = session.query(DatasetTransformDetailSa)\
                    .filter(DatasetTransformDetailSa.TransformationHeadId==t.id).order_by('id')
                    
                    
                    #puni transformacijsku matricu sa ulaznom i izlazom (npr TextItem1 i User1_id) za vrijednosti iz DatasetTransformDetail(filter=transformname)
                    for trow in transformsRAW.all():
                            # trow su redci iz DatasetTransformDetail tablice za odabranu transformaciju
                            tmap[vars(trow)[typeFrom]]=vars(trow)[typeTo] # zapravo tmap[typeFrom]=typeTo (ali tako baca gresku)
        return (map,tmap) 
        #   ID, NAZIV I DRZAVA su kolone u FILEu
        
        # pp map = {'DRZAVA': 'Field370', 'ID': 'id', 'NAZIV': 'Field369'}
        # pp tmap {'Austrija': 2, 'Hrvatska': 1, 'Slovenija': 3}
       

    def list(self, request,fileId,resourceId,mappingsId):# ImportResourceViewSet
      #try:
        line = 'init 1'
        import csv,json 
        file_id = None
        resource_id = None
        mappings_id = None
        if 'fileId' in self.kwargs:    file_id=int(self.kwargs['fileId'])          
        if 'resourceId' in self.kwargs:  resource_id=int(self.kwargs['resourceId'])       
        if 'mappingsId' in self.kwargs:   mappings_id=int(self.kwargs['mappingsId'])       
        if file_id is None:      return Response(status=404)
        if resource_id is None:  return Response(status=404)       
        line = 'init 2'  
        org_id=self.getUserOrgId(request.user) #get user org_id   
        rfile = FilesTable.objects.get(id=file_id)  
        
        size=self.getFileSize(Path(rfile.Path) / rfile.Name)
        
        #TODO - prebaciti transformacije u Codes ?
        
        #pdb.set_trace() #DBG_MAPPING ImportResourceViewSet
        line = 'init 3'
        #load fileds mapping from mappingTable into JSON
        mappingsTable=MappingsTable.objects.get(id=mappings_id) # mapping_id is unique
        mappingsArray=json.loads(mappingsTable.ResourceMapping.replace("'","\""))
        line='init 4'
        # mappingsArray je array mapiranja polja iz dataset modela
        # [{'cells': [{'name': 'ID'}],    'itemID': 356, 'name': 'ResourceId',    'placeholder': 'Dataset id',     'presentationId': 5266},
        # {'cells': [{'name': 'TEKST'}],  'itemID': 357,   'name': 'Text',    'placeholder': 'Text',    'presentationId': 5267}],
                     
        map={} # koja FILE polja se mapiraju na koji resurs + da li ima TRANSFORM! NPR. map = {'ID': 'Field356', 'TEKST': 'Field357'},
        tmap={} # transform mapping - koja vrijednost na koju vrijednost! BITNO! {'miro': 5, 'pero': 3, 'slavko': 4}  
        (map,tmap) = self.getMapingAndTransform(org_id,resourceId, mappingsArray)
        line = 'init 6'
        #pdb.set_trace() #DBG #IMPORT #MAPPING #TRANSFORM #read file and import it using transform
        with open(Path(rfile.Path) / rfile.Name, 'rb') as f:
            rawdata=f.read(1000)
            detection=chardet.detect(rawdata) #  detection={'encoding': 'EUC-JP', 'confidence': 0.99
            f.close()
            if detection['confidence'] <= 0.45: raise ValidationError('Nepoznat encoding datoteke, konvertirajte u UTF')

        with open(Path(rfile.Path) / rfile.Name, encoding=detection['encoding']) as f:
            temp_lines = f.readline() + '\n' + f.readline()
            dialect = csv.Sniffer().sniff(temp_lines, delimiters=',;|')
            f.close()
        with open(Path(rfile.Path) / rfile.Name, encoding=detection['encoding']) as f:
            reader = csv.reader(f,dialect) 
            content=[]
            header=next(reader) #read first line 
            for line in reader: #row= Dict('ID':'1', 'NAZIV': 'Maribor', 'DRZAVA', 'Slovenija')
                row={}
                for i in range(len(line)):
                    row[header[i]]=line[i] # create Dict

                newRecord={}
                m2mData={}
                for key, value in map.items(): # key,value = ('ID', {'fieldId': 368, 'fieldName': 'id'})
                    #pdb.set_trace()#IMPORT #MAPPING #TRANSFORM #DBG
                    if key in row:
                        if 'transform' in value:
                            #  row = OrderedDict([('ID', '1'), ('NAZIV', 'Maribor'), ('DRZAVA', 'Slovenija')])
                            # m2mData = [{'resourceSourceDefId': 222, 'modelDefinitionId': 370, 'resourceDefId': 218, 'resourceId': [2]}]

                            if value['transform']=='related_dataset':
                                #pdb.set_trace() #DBG #IMPORT #MAPPING #TRANSFORM
                                m2mData[key] = [{'resourceSourceDefId': int(resourceId), 'modelDefinitionId': map[key]['fieldId'], 'resourceDefId': map[key]['resourceDefId'], 'resourceId': [tmap[key][row[key].upper()]]}]
                                #TODO - Trenutno radimo insert za svaki row u bazu, treba keširati i onda na kraju sve odjednom zapisati
                            elif value['transform']=='codes':
                                #pdb.set_trace()
                                codevalue = row.pop(key)
                                if codevalue:
                                    if codevalue in tmap[key]:
                                        newRecord[value['fieldName']]=tmap[key][codevalue] #MIGOR
                                    else:
                                        raise Exception(value['fieldName']+ ':"'+codevalue + '" is not valid code! (' + str(line) + ')')
                            else:
                                newRecord[value['fieldName']+'_id']=tmap[row.pop(key)] #MIGOR TODO - vidjeti ifologiju kad dodajem id a kad ne
                           
                        else:
                            if value['fieldName']=='id':
                                newRecord['resource' + str(resourceId) + '_'+ str(org_id) + '_id']= row.pop(key)
                            else:
                                newRecord[value['fieldName']]= row.pop(key)
                #pdb.set_trace() #DBG #IMPORT #MAPPING #TRANSFORM
                record =  mutant_insert_data(newRecord,resource_id, org_id, request.user) #  newRecord = {'id': '1', 'Field369': 'Maribor'}
                for key,value in m2mData.items():
                    mutantm2m =  mutant_insert_data_m2m_v2(record.id,org_id,value,request.user,False)  #TODO - vidjeti zašto ne gazi vrijednos              
                content=content+[row.items()]          # TODO - provjeriti zasto se vraca content
        f.close
        return Response(data={'fileId':rfile.id,'resourceId':resource_id,'size':size,'content':content},status=201)
      #except Exception as e:
        #pdb.set_trace() #DBG #IMPORT #MAPPING #TRANSFORM
      #  return Response(data={'msg': str(e) + ' (' + str(line) + ')'  },status=501)



         
class getMappingsListViewSet(viewsets.GenericViewSet):
    queryset = MappingsTable.objects.all()
     
    def list(self, request,resourceDefId):
        
        if 'resourceDefId' in self.kwargs:
           resourceDef_id=int(self.kwargs['resourceDefId'])
        else:
           resourceDef_id = None
           
        #ToDo SQLAlchemy migration                       
        mappingsData= MappingsTable.objects.filter(ResourceDefId_id=resourceDef_id) # Choices are: FileFields, Name, ResourceDefId, ResourceDefId_id, ResourceMapping, id
        datasetList=[]
        for ds in mappingsData:
            #ds.name='Mapping_'+str(ds.id) #privremeno
            datasetList.append({'id':ds.id,'name':ds.Name,'resourceMapping':ds.ResourceMapping,'fileFields':ds.FileFields})

        
        return Response(data=datasetList,status=201)
       
class putMappingsViewSet(viewsets.GenericViewSet):
    queryset = MappingsTable.objects.all()
    
    def create(self, request,resourceDefId,mappingsId): 
          
        
        if 'resourceDefId' in self.kwargs:
           resourceDef_id=int(self.kwargs['resourceDefId'])
        else:
           resourceDef_id = None
        
        if 'mappingsId' in self.kwargs:
           mappingsId_id=int(self.kwargs['mappingsId'])
        else:
           mappingsId_id = None
 
        up_ = request
        user = request.user
        strJson=json.dumps(request.data)
        tmp=request.data

        mappingsData = request.data['mappingsData']
        fileFields=request.data['fileFields']
        
        name = request.data['name']
        
        #if (mappingsId_id): 
        #mapping,isCreated = MappingsTable.objects.get_or_create(id=mappingsId_id,ResourceDefId_id=resourceDef_id,ResourceMapping=mappingsData,Name=name,FileFields=fileFields)
        #ToDo SQLAlchemy migration
        mapping = MappingsTable.objects.get(id=mappingsId_id )
        if mapping:  
          mapping.ResourceDefId_id=resourceDef_id
          mapping.ResourceMapping=mappingsData
          mapping.Name=name
          mapping.FileFields=fileFields
          mapping.save()
        else:
          return Response('Mappings not found!',status=401) 
        
        return Response('Mappings data saved. Upload completed successfully !',status=201) 
       
class putMappingsNewViewSet(viewsets.GenericViewSet):
    queryset = MappingsTable.objects.all()
    
    def create(self, request,resourceDefId): 
          
        #pdb.set_trace() #DBG #SAVE #MAPPING #TRANSFORM
        if 'resourceDefId' in self.kwargs:
           resourceDef_id=int(self.kwargs['resourceDefId'])
        else:
           resourceDef_id = None
 
        up_ = request
        user = request.user
        strJson=json.dumps(request.data)
        tmp=request.data

        mappingsData = request.data['mappingsData']
        fileFields=request.data['fileFields']
        
        session = rdbms_cls.Session()
        with session.begin():  
            name = request.data['name']
            #ToDo SQLAlchemy migration DONE
            #mapping,isCreated = MappingsTable.objects.get_or_create(ResourceDefId_id=resourceDef_id,ResourceMapping=mappingsData,Name=name,FileFields=fileFields)
            mapping = session.query(MappingsTableSa).filter(MappingsTableSa.ResourceDefId == resourceDef_id).first()
            if mapping:
                isCreated=True
            else:
                new= MappingsTableSa(ResourceMapping=mappingsData,Name=name,FileFields=fileFields)
                session.add(new)
           
            if isCreated:    
                return Response('Mappings data saved. Upload completed successfully !',status=201) 
            else:
                return Response('Mappings already exists!',status=401)
          
