from katalyonode.functions.ktBlockchainprocessing import get_table_rows,get_transaction,send_generic_transaction
from katalyonode.functions.blockchain.eth_middleware import get_token_balance,get_public_vars
#from katalyonode.models.models import ResourceDef,UserExtendedProperties,Organisations
from katalyonode.functions.rdbms.alchemymodels import OrganisationsSa
#from katalyonode.functions.datasets import get_columns_def
from rest_framework import viewsets,permissions,authentication
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
import pdb

class GetBlockchainDatasetRecordsViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    
    def create(self, request): 

        user = request.user
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user) 
        post_data=request.data
        
        p_key = post_data['p_key']
        contract = post_data['contract']
        dataset = post_data['dataset']
        limit = post_data['limit']
        resource_id = post_data['resource_id']
        language = post_data['language']
        index_position=1 #'primary'
        key_type = '' #uint64_t
        
        jsonResponse,rstatus = get_table_rows(contract,dataset,contract,index_position,key_type,limit,p_key)
        
        form_type='i'
        
        columnsDef = get_columns_def(resource_id,0,form_type,org_id,language)
        
        ret_value = {'data':jsonResponse,'columns':columnsDef}
        
        return Response(data=ret_value,status=rstatus)

class ExecuteBlockchainFunctionViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    
    def create(self, request): 

        user = request.user
        
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user) 
          
        post_data=request.data
        
        contract = post_data['contract']
        function = post_data['function']
        params = post_data['params']
        account = 'katalyotoken'
        
        trx_json = {'from':params['Field399'],'to':params['Field400'],'quantity':params['Field401'],'memo':params['Field402']}
        #pdb.set_trace()
        jsonResponse = send_generic_transaction(trx_json,contract,function,account)
        return Response(data=jsonResponse,status=jsonResponse[2])

class GetKtlyoTokenBalanceViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    
    def create(self, request): 

        user = request.user
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user)
          
        post_data=request.data
        address = post_data['address']
        contract = "0x24E3794605C84E580EEA4972738D633E8a7127c8";
        abi = [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"owner","type":"address"},{"indexed":True,"internalType":"address","name":"spender","type":"address"},{"indexed":False,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":True,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"anonymous":False,"inputs":[{"indexed":True,"internalType":"address","name":"from","type":"address"},{"indexed":True,"internalType":"address","name":"to","type":"address"},{"indexed":False,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}]
       
        block,balance,block_eth,balance_eth = get_token_balance(address,contract,abi)
        return Response(data={'block':block,'block_eth':block_eth,'balance':balance,'balance_eth':balance_eth},status=200)
    
class GetContractPublicVarsViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    permission_classes = (AllowAny, )
    authentication_classes = ()
    def create(self, request): 

        user = request.user
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user)
          
        post_data=request.data
        contract = post_data['contract']
        abi = post_data['abi']
        
        public_vars,block= get_public_vars(contract,abi)
        
        return Response(data={'block':block,'vars':public_vars},status=200)

class SaveEthAccountViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    
    def create(self, request): 

        user = request.user
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user)
          
        eth_acc=request.data['address']

        rdbms_cls = RdbmsClass()

        session = rdbms_cls.Session()
        with session.begin():
            #eth_save = Organisations.objects.filter(id=org_id).update(ETHAccount=eth_acc)

            eth_save = session.query(OrganisationsSa).filter(OrganisationsSa.id==org_id).update({'ETHAccount':eth_acc})

        if eth_save==1:
            msg = 'ETH account saved!'
            r_status = 200
        else:
            msg = 'Error saving ETH account!'
            r_status = 500
        return Response(data=msg,status=r_status)
    
class GetBlockchainTransactionViewSet(viewsets.GenericViewSet):
    #queryset = ResourceDef.objects.all()
    
    def create(self, request): 

        user = request.user
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org (user)
          
        post_data=request.data
        
        transaction_id = post_data['transaction_id']
        block_num_hint = post_data['block_num_hint']
        
        jsonResponse,rstatus = get_transaction(transaction_id,block_num_hint)
 
        return Response(data=jsonResponse,status=rstatus)

class EosKatalyoHello(viewsets.GenericViewSet):

  #queryset= FilesToResource.objects.all()
    
  def list(self, request): 
    
    cert_name = 'cert1'
    doc= {'Field1':1}
    h1 = 'hash1'
    h2 = 'hash2'
    user = 'user'
    trx_json = {'cert_name':cert_name,'doc':json.dumps(doc),'h1':h1,'h2':h2,'user':user}
    contract = 'kataly1.code'
    action='addcert'
    jsonResponse,status = send_transaction(trx_json,contract,action)
    
    return Response(jsonResponse,status)
    

def import_cert(FILES_DIR, file_name,resource_def_id,resource_id,user):
  
    #TODO naci neki 4 jer moci ne valja 3
    with open(FILES_DIR+"\\\\"+file_name) as fd:
        doc = xmltodict.parse(fd.read())
  
    cert=doc['Certificate']
    CertificateID={} 
    Parties=[]
    Plates=[]
  
  
    #print('---CertificateID--')
   
    CertificateID['CertificateGroup']=cert['CertificateID']['CertificateGroup']
    CertificateID['CertificateNumber']=cert['CertificateID']['CertificateNumber']
    CertificateID['CertificateRevisonNumber']=cert['CertificateID']['CertificateRevisonNumber']
    CertificateID['KindOfCertifiedMaterials']=cert['KindOfCertifiedMaterials']
  
    CertificateID['CertificateDate']=cert['CertificateDate']
    CertificateID['OrdererOrderNumber']=cert['OrdererOrderNumber']
    CertificateID['OrderDate']=cert['OrderDate']
    CertificateID['SupplierOrderNumber']=cert['SupplierOrderNumber']
  
    if CertificateID['KindOfCertifiedMaterials']=='P':
        type=cert['MaterialUnitPlateCoilRepeat']['MaterialUnitPlateCoil']
    if CertificateID['KindOfCertifiedMaterials']=='F':
        type=cert['MaterialUnitFloorRepeat']['MaterialUnitFloor']

    Item={}
    
    if CertificateID['KindOfCertifiedMaterials']=='P':
        Item['MaterialIdentification']=x['MaterialIdentification']
    if CertificateID['KindOfCertifiedMaterials']=='F':
        Item['FloorIdentification']=x['FloorIdentification']
    
    
    Item['ItemNo']=x['Item']['IdItem']['ItemNo']
    Item['SubItemNo']=x['Item']['IdItem']['SubItemNo']
    Item['SteelStandard']= x['Item']['SteelQuality']['SteelStandard']
    Item['SteelGradeName']=x['Item']['SteelQuality']['SteelGradeName']
    #Item['Characteristics']=[]
    Item['Characteristics2']={}
    
    for ch in x['Item']['OrderCharacteristicRepeat']['Characteristic']:
        cc=dict(ch)
        #Item['Characteristics'].append(cc)
        
        if cc['CodeCharacteristic']=='Length':
            Item['Characteristics2']['Length']=cc['ValueCharacteristic']+ cc['ValueMeasureUnit']
        if cc['CodeCharacteristic']=='Width':
            Item['Characteristics2']['Width']=cc['ValueCharacteristic']+ cc['ValueMeasureUnit']
        if cc['CodeCharacteristic']=='Thickness':
            Item['Characteristics2']['Thickness']=cc['ValueCharacteristic']+ cc['ValueMeasureUnit']
        if cc['CodeCharacteristic']=='WeightPerPiece':
            Item['Characteristics2']['WeightPerPiece']=cc['ValueCharacteristic']+ cc['ValueMeasureUnit']
    
    print('Heat...')
    Heat={}
    if 'Heat' in x:
        Heat['HeatNumber']=x['Heat']['HeatNumber']
    
  
    mapping_id=11
  
    #insert certificate
    exe_dict_update(CertificateID,mapping_id,resource_def_id,resource_id,user)  
  
  
    #print(CertificateID)
      
  
    #print('-----Parties-----')    
    for x in cert['PartyRepeat']['Party']:
        Parties.append(dict(x))
        #Parties[cert['PartyRepeat']['Party']]=
          
    party_ids=[]
    party_resource_def_id = 129
    material_resource_def_id  = 128
    mapping_id=16
    for party in Parties:
        p = exe_dict_insert_child(party,mapping_id,resource_def_id,resource_id,party_resource_def_id,user)
        party_ids.append(p.id)
  
    model_id=158
    #link parties to certificate
    mutant_insert_data_m2m_no_delete(resource_def_id, party_resource_def_id,resource_id,model_id, [{'resourceDefId':party_resource_def_id,'resourceId':party_ids}], user);
  
    #print(Parties)
  
  
     
    #1. insert CertificateID(dobijem ID)
    #2. insert partija(pokupiti listu ID)
    #3. insertm2m  povezati 1 i 2
  
  

def exe_dict_update(certificate,mappings_id,resource_def_id,resource_id,user):
  
       
      
    new=extract_data(mappings_id,certificate)
        
        
        
    new['id']=resource_id
    mutantModel =  mutant_update_data(new,resource_def_id, user) 

def exe_dict_insert_child (party,mappings_id,resource_def_id,resource_id,party_resource_def_id,user):
  
    new=extract_data(mappings_id,party)
        
        
    #new['id']=resource_id
    mutantModel =  mutant_insert_data(new,party_resource_def_id, user)
        
    return mutantModel
        
def extract_data(mappings_id,row): # TODO - migor - Da li se ovo koristi? za import file imam novu verziju
  
    mappingsTable=MappingsTable.objects.filter(id=mappings_id)
    mson=[]
    codeMap1={'text':'TextItem1','code':'','group':'','user':'User1_id'} #trenutno samo text->user
    codeMap2={'text':'TextItem2','code':'','group':'','user':'User2_id'} #trenutno samo text->user
    for map in mappingsTable:
        m=map.ResourceMapping
            
        mson=json.loads(m.replace("'","\""))
        new={}
        map={} #inverzno mapiranje
        transform={} #trenutno ogranicenje na samo 1 transformaciju
        
        for f in mson:
            fieldName=''
            transformname=''
            for cc in f['cells']: 
                if 'type' in cc: #ako postoji 'type' kreiraj transformname              
                    if cc['type']=='TRANSFORM':
                        transformname=cc['name']
                    else: 
                        raise  ValidationError('Nepoznat TRANSFORM '+cc['type'])
                    
                else: 
                    fieldName=cc['name']
                
                if fieldName!='':
                    map[fieldName]='Field'+str(f['itemID'])
                    # MIGOR TODO - napraviti array da podrzava vise od 1 transformacije!
                    if transformname!='':
                        transform[fieldName]=transformname
                        t=DatasetTransformHead.objects.get(TransformName=transformname)
                        typeFrom=codeMap1[CodesDetail.objects.get(id=t.FieldType1_id).Name]
                        typeTo=codeMap2[CodesDetail.objects.get(id=t.FieldType2_id).Name]
                        transformsRAW = DatasetTransformDetail.objects.filter(TransformationHeadId_id=t.id).select_related('CodeDetail1','CodeDetail2').order_by('id')
                        tmap={}
                        for trow in transformsRAW:
                            tmap[vars(trow)[typeFrom]]=vars(trow)[typeTo]
        for key, value in map.items():
            if key in row:
                if row[key]:
                    if key in transform:
                        new[value+'_id']=tmap[row.pop(key)] #MIGOR TODO - vidjeti ifologiju kad dodajem id a kad ne
                    else:
                        new[value]= row.pop(key)
    return new
