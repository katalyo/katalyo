from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser,FileUploadParser,FormParser, MultiPartParser
import json
from django.forms.models import model_to_dict
import os
import sys
from django.core.exceptions import ValidationError
from itertools import chain
from django.core import serializers
from katalyonode.settings import FILES_DIR
from django.template import RequestContext
from django.db.models import Prefetch
from django.contrib.auth.models import User, Group
import hashlib
import uuid
from rest_framework import viewsets,permissions,authentication
from rest_framework.permissions import AllowAny
#import katalyonode.serializers.serializers 
from katalyonode.serializers.serializerssettings import CodesSerializer,CodesDetailSerializer,CodesHeadSerializer
from katalyonode.serializers.serializerssettings import CodesHeadLangSerializer,CodesDetailLangSerializer,GetCodesDetailSerializer
from katalyonode.serializers.serializerssettings import AddUpdateCodesDetailSerializer,AddUpdateCodesDetailLangSerializer,GetCodesDetailByIdLangSerializer
from katalyonode.serializers.serializerssettings import AddMenuSerializer,GetMenuSerializer,GetNavbarSerializer,GetPagesSerializer,SaveCodesDetailSerializer,CodesHeadSimpleSerializer,MenusSerializer
#import katalyonode.models
from rest_framework.authtoken.models import Token
#from katalyonode.functions.taskprocessing import StartTask
from katalyonode.functions.resources.menudef import menu #get_menus,get_users_groups_menus
from katalyonode.functions.resources.pagemenudef import pagemenu 
#from katalyonode.functions.resourceutilityfn import processMenuItem
from django.utils import timezone
from katalyonode.models.models import CodesHead,CodesHeadLang,CodesDetail,CodesDetailLang,Menus,Pages,UserExtendedProperties,TaskTransactions,ErrorLog,Organisations,Language
from django.db.models import Q
#import katalyonode.dynamicmodels
import pdb
from datetime import datetime
from katalyonode.functions.resources.base.resourcehelper import ResourceHelperClass
from katalyonode.functions.rdbms.rdbms import RdbmsClass
from katalyonode.functions.rdbms.alchemymodels import CodesHeadSa, CodesHeadLangSa, codesdetail, CodesDetailSa
from sqlalchemy import and_,or_


rdbms_cls = RdbmsClass()

# Create your views here.

class CodesHeadViewSet(viewsets.GenericViewSet):
    #queryset = CodesHead.objects.all() #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
    #serializer_class = CodesHeadSimpleSerializer
    """
    def get_queryset(self):
        
        This view should return a list of all the codes for
        the id of the codes head as determined by the id portion of the URL.
        
        if 'lang' in self.kwargs:
          language=self.kwargs['lang']
        else:
          language = None
          
        if 'id' in self.kwargs:
          code_id=self.kwargs['id']
        else:
          code_id = None
        
        
        return CodesHead.objects.filter(id=code_id).prefetch_related(Prefetch('CodesHeadLang',
                                                            queryset=CodesHeadLang.objects.filter(Lang_id=language))) 
    """
    def list(self,request,codeId,lang):
        """
        This view should return a list of all the codes for
        the id of the codes head as determined by the id portion of the URL.
        """
        if 'lang' in self.kwargs:
          language=self.kwargs['lang']
        else:
          language = None
          
        if 'codeId' in self.kwargs:
          code_id=self.kwargs['codeId']
        else:
          code_id = None
        user = request.user
        helper = ResourceHelperClass()
        org_id,lang = helper.get_lang_org(user)

        session = rdbms_cls.Session()
        codeItem = None
        with session.begin():

            codes = session.query(CodesHeadSa,CodesHeadLangSa).join(CodesHeadLangSa,and_(CodesHeadSa.id==CodesHeadLangSa.CodesHeadId,CodesHeadLangSa.Lang == lang.id)).filter(CodesHeadSa.id==code_id).all()
            
            for code in codes:
                code_type_json = code[0].CodeType or '{}'
                code_type_name = None

                code_type = json.loads(code_type_json)
                code_type_name = code_type.get('name',None)
                codeItem = {'id':code[0].id,'Active':code[0].Active,'UserGeneratedId':code[0].UserGeneratedId,'Description':code[1].Description,'CodeType':code_type,'CodeType.name':code_type_name,'CodeName':code[1].CodeName,'CodeHeadName':code[0].CodeHeadName}
        
        return Response(data=codeItem)
        
        
#check if used      
class CodesHeadLangViewSet(viewsets.ModelViewSet):
    serializer_class = CodesHeadLangSerializer
    
    def get_queryset(self):
        """
        This view should return a list of all the codes for
        the id of the codes head as determined by the id portion of the URL.
        """
        code_id = self.kwargs['id']
        return CodesHeadLang.objects.filter(CodesHeadId=code_id)
    
class CodesViewSet(viewsets.GenericViewSet):
    queryset = CodesHead.objects.all().prefetch_related(Prefetch('CodesHeadLang', queryset=CodesHeadLang.objects.filter(OnInsert=True))) #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
    serializer_class = CodesSerializer
    
    def list(self, request,lang):
              
        if 'lang' in self.kwargs:
            language=self.kwargs['lang']
        else:
            language = None
       
        user = request.user
        helper = ResourceHelperClass()
        org_id,lang = helper.get_lang_org(user)

        #sql alchemy migration - done
        codeItems = []

        session = rdbms_cls.Session()
        #pdb.set_trace()

        with session.begin():

            codes = session.query(CodesHeadSa,CodesHeadLangSa).join(CodesHeadLangSa,and_(CodesHeadSa.id==CodesHeadLangSa.CodesHeadId,CodesHeadLangSa.Lang == lang.id)).filter(CodesHeadSa.OrganisationId==org_id).all()
            
            for code in codes:
                code_type = None
                code_type_name = None
                if code[0].CodeType is not None:
                    code_type = json.loads(code[0].CodeType)
                    code_type_name = code_type.get('name',None)

                codeItems.append({'id':code[0].id,'CodeName':code[1].CodeName,'UserGeneratedId':code[0].UserGeneratedId,'Active':code[0].Active,'CodeHeadName':code[0].CodeHeadName,'Description':code[1].Description,'CodeType':code_type,'CodeType.name':code_type_name,'Lang':code[1].Lang,'Version':code[0].Version})
        
        return Response(data=codeItems)
    
    def create(self, request,lang):
              
        if 'lang' in self.kwargs:
            language=self.kwargs['lang']
        else:
            language = 1
         
        user = request.user
        helper = ResourceHelperClass()
        org_id,lang = helper.get_lang_org(user)
        msg='Code saved successfuly'
        #pdb.set_trace()
        try:
            code_data = request.data
            code_id = None
            if 'id' in code_data:
                code_id = code_data['id']

            session = rdbms_cls.Session()
            codeHead = None

            with session.begin():
                code_head_name = code_data.get('CodeHeadName',None)  
                code_type = json.dumps(code_data.get('CodeType',{}))
                user_gen = code_data.get('UserGeneratedId',False)
                code_active = code_data.get('Active',False)
                code_name = code_data.get('CodeName',None) 
                desc = code_data.get('Description',None) 
                if code_head_name is None or code_type is None or code_name is None:
                    return Response(data='Code head name or code type is null',status=500)    
               
                if code_id is None: #add new code head if code_id is None
                    codeHead = CodesHeadSa(CodeHeadName=code_head_name, Active = code_active, Version=1,UserGeneratedId=user_gen,OrganisationId=org_id,CodeType = code_type,CreatedBy=user.id,CreatedDateTime=datetime.now(timezone.utc))
                    session.add(codeHead)
                    session.flush()

                    if codeHead.id is not None:
                        codeHeadLang = CodesHeadLangSa(CodeName=code_name,Description = desc,CodesHeadId=codeHead.id,Lang=language,CreatedBy=user.id,CreatedDateTime=datetime.now(timezone.utc))
                        session.add(codeHeadLang)
                        session.flush()
                        if codeHeadLang.id is not None:
                            msg = 'Code created successfully'
                    else:
                          return Response(data='Code head record not created error',status=500)    
                else:

                    codeHeads = session.query(CodesHeadSa,CodesHeadLangSa).join(CodesHeadLangSa,and_(CodesHeadSa.id==CodesHeadLangSa.CodesHeadId,CodesHeadLangSa.Lang==language)).filter(CodesHeadSa.id==code_id).all()
                    for ch in codeHeads:
                        codeHead = ch[0]
                        ch[0].CodeHeadName = code_head_name
                        ch[0].UserGeneratedId = user_gen
                        ch[0].CodeType = code_type
                        ch[0].Active = code_active
                        ch[1].CodeName = code_name
                        ch[1].Description = desc

            return Response(data={'msg':msg,'data':codeHead.id},status=200)
        except Exception as exc:
        
            return Response(data="Error --> "+str(exc),status=500)
      
            
class CodesDetailViewSet(viewsets.GenericViewSet):
    #serializer_class = CodesDetailSerializer
    #queryset = CodesDetail.objects.all()
     
    def list(self, request,lang):
        
        if 'pk' in self.kwargs:
         code_id=self.kwargs['pk']
        else:
         code_id = None
         
        if 'lang' in self.kwargs:
         lang = self.kwargs['lang']
        else:
         lang = None
        
        codesList = []
        user = request.user
        helper = ResourceHelperClass()
        org_id,lang = helper.get_lang_org(user)

        session = rdbms_cls.Session()
        
        with session.begin():

            codes = session.query(CodesDetailSa,codesdetail,CodesHeadSa).join(codesdetail, and_(CodesDetailSa.id==codesdetail.id,codesdetail.Lang == lang.id)).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).filter(CodesDetailSa.Active==True,CodesDetailSa.CodesHeadId==code_id).all()
            userId=None
            for code in codes:
                
                params = code[0].Params or '{}'
                codesList.append({'id':code[0].id,'Name':code[0].Name,'Value':code[1].Value,'Params':json.loads(params),'Active':code[0].Active,'CodesHeadId':code[0].CodesHeadId})
            
                
        """
        if code_id is None:     
          codeDetails = CodesDetail.objects.all().prefetch_related(Prefetch('CodesDetailLang',       queryset=CodesDetailLang.objects.filter(Lang_id=lang))) 
        else:
          codeDetails = CodesDetail.objects.all()
        
        serializer = CodeDetailsSerializer(data=codeDetails,many=True)  
        serializer.is_valid()
        """
        return Response(data=codeList)
      
      
class CodesDetailLangViewSet(viewsets.ModelViewSet):
#class CodesHeadLangViewSet(generics.ListAPIView):
    queryset = CodesDetailLang.objects.all() #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
    serializer_class = CodesDetailLangSerializer


class CodesListViewSet(viewsets.GenericViewSet):
    queryset = CodesHeadLang.objects.all() #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
    permission_classes = (AllowAny, )
    authentication_classes = ()
   
    def list(self, request,lang):
              
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None

        #user = request.user
        #helper = ResourceHelperClass()
        #org_id,lang = helper.get_lang_org(user)
        
        codesList = []
        session = rdbms_cls.Session()
        with session.begin():

            codes = session.query(CodesHeadSa,CodesHeadLangSa).join(CodesHeadLangSa,and_(CodesHeadSa.id==CodesHeadLangSa.CodesHeadId,CodesHeadLangSa.Lang == language)).filter(CodesHeadSa.Active==True).all()
            
            for code in codes:  
                codesList.append({'id':code[0].id,'name':code[1].CodeName})


        return Response(codesList)

class GetCodesByNameViewSet(viewsets.GenericViewSet):
#class CodesHeadLangViewSet(generics.ListAPIView):
    #queryset = CodesDetailLang.objects.all() #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
    permission_classes = (AllowAny, )
    authentication_classes = ()
   
    def list(self, request,codeName,lang):
        
        if 'codeName' in self.kwargs:
           code_name=self.kwargs['codeName']
        else:
           code_name = ''
           
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None

        codesList = GetCodesDetailsByType(code_name,language,'name')
        
        return Response(codesList)



#radi

class CodesDetailListViewSet(viewsets.GenericViewSet):
#class CodesHeadLangViewSet(generics.ListAPIView):
    queryset = CodesHeadLang.objects.all() #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
    
   
    def list(self, request,codeId,lang):
              
        if 'lang' in self.kwargs:
           language=self.kwargs['lang']
        else:
           language = None
           
        if 'codeId' in self.kwargs:
           code_id=self.kwargs['codeId']
        else:
           code_id = 0


        user = request.user
        helper = ResourceHelperClass()
        org_id,lang = helper.get_lang_org(user)
        
        codeList = []
        session = rdbms_cls.Session()
        
        with session.begin():

            codes = session.query(CodesDetailSa,codesdetail,CodesHeadSa).join(codesdetail, and_(CodesDetailSa.id==codesdetail.id, codesdetail.Lang == lang.id)).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).filter(CodesDetailSa.Active==True,CodesDetailSa.CodesHeadId==code_id).all()
            userId=None
            for code in codes:
                if userId is None:
                    userId = code[2].UserGeneratedId
                    if userId:
                        cd_id = code[0].Name
                    else:
                        cd_id = code[0].id
                
                codesList.append({'id':cd_id,'name':code[1].Value,'actions':[]})
            
        return Response(codesList)


#Check if used
class GetCodesDetailViewSet(viewsets.ReadOnlyModelViewSet):

   serializer_class = GetCodesDetailSerializer
    
   def get_queryset(self):
        
          
      return CodesDetail.objects.all().prefetch_related(Prefetch('CodesDetailLang', queryset=CodesDetailLang.objects.all()))
    

class GetCodesDetailByIdLangViewSet(viewsets.GenericViewSet):

   #serializer_class = GetCodesDetailByIdLangSerializer
   #queryset = CodesDetail.objects.all() #chain(CodesHead.objects.all(), CodesHeadLang.objects.all()) 
   #permission_classes = (AllowAny, )
   #authentication_classes = () 
    
   def list(self, request, id, lang):
    #def get_queryset(self):   
    language = None
    code_id = 0
    
    if 'lang' in self.kwargs:         language = self.kwargs['lang']               
    if 'id' in self.kwargs:         code_id = self.kwargs['id']
               
    #ret = CodesDetail.objects.filter(CodesHeadId=code_id).prefetch_related(Prefetch('CodesDetailLang',    
    #        queryset=CodesDetailLang.objects.filter(Lang_id=lang).exclude(Lang__isnull=True))).order_by('id')
        
    ret2 = []
    session = rdbms_cls.Session()
    with session.begin():
        query= session.query(CodesDetailSa, codesdetail).join( codesdetail, CodesDetailSa.id == codesdetail.id)\
        .filter(and_ (CodesDetailSa.CodesHeadId==code_id, codesdetail.Lang == language, codesdetail.Lang.is_not(None)) ).order_by(CodesDetailSa.id).all()

        for cd in query:
            (code,  code['name']) = (cd[0]._asdict(), cd[1].Value)
            ret2.append(code)
        
    return Response(data=ret2)

class GetCodesDetailByCodeIdLangViewSet(viewsets.GenericViewSet):

    #serializer_class = GetCodesDetailSerializer
    #queryset = CodesDetail.objects.all()
    
    def list(self, request,id,lang):
        
        if 'id' in self.kwargs:
         code_id=self.kwargs['id']
        else:
         code_id = None
         
        if 'lang' in self.kwargs:
         language = self.kwargs['lang']
        else:
         language = None
        
        
        codeList = []
        session = rdbms_cls.Session()
        
        with session.begin():

            if code_id is not None:
              
                codes = session.query(CodesDetailSa,codesdetail,CodesHeadSa).join(codesdetail,and_(CodesDetailSa.id==codesdetail.id,codesdetail.Lang == language)).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).filter(CodesHeadSa.id==code_id).order_by(CodesDetailSa.id).all()
                
            else:
                codes = session.query(CodesDetailSa,codesdetail,CodesHeadSa).join(codesdetail,and_(CodesDetailSa.id==codesdetail.id,codesdetail.Lang == language)).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).order_by(CodesDetailSa.id).all()
       
            for code in codes:
                params = code[0].Params or '{}'
                codeList.append({'id':code[0].id,'Name':code[0].Name,'Active':code[0].Active,'Value':code[1].Value,'Params':json.loads(params)}) 
        
        return Response(data=codeList)    

#check if used
class AddUpdateCodesDetailViewSet(viewsets.ModelViewSet):
   queryset = CodesDetailLang.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
   serializer_class = AddUpdateCodesDetailLangSerializer
   
   def create(self, request):
      
      codesData = request.data
      #user = request.user
      #codesData['CreatedBy'] = user.id
      #codesData['CreatedDateTime'] = timezone.now()
      
      
      aucdl = AddUpdateCodesDetailLangSerializer(data=codesData,context={'request':request})  
      if aucdl.is_valid(raise_exception=True):
          rVal = aucdl.save()
          
      if rVal:   
        return Response(status=201)
      else:
        return Response(status=400)

#ToDo - migrate to SQLAlchemy
class SaveCodesDetailViewSet(viewsets.GenericViewSet):
    queryset = CodesDetailLang.objects.all().select_related()#prefetch_related(Prefetch('codesdetaillang_set', queryset=CodesDetailLang.objects.all()))
    #serializer_class = SaveCodesDetailSerializer
   
    def codes2delete(self, smaller, bigger):
        smallset = set()
        bigset = set()
        for i in smaller: 
            smallset.add(i['id'])
        for i in bigger: 
            bigset.add(i['id'])
            
        return list(bigset - smallset)
           
    
    def create(self, request,codeHeadId,lang):
          
        code_head_id = None
        language = 1
      
        if 'codeHeadId' in self.kwargs:
            code_head_id=int(self.kwargs['codeHeadId'])
  
        if 'lang' in self.kwargs:
            language = int(self.kwargs['lang'])
          
        r_msg='No message'
        
        codesData = request.data
        user = request.user
       
        session = rdbms_cls.Session()
        with session.begin():
            for code in codesData:

                #this is the start of serializer

                #code,codeCreated =  CodesDetail.objects.get_or_create(id=validated_data['id'], defaults={'Active':validated_data['Active'],'Name':validated_data['Name'],'Params':validated_data['Params'],'CodesHeadId_id':codeHeadId})
                if code.get('id',None) is None:
                    codeItem = CodesDetailSa(Active = code.get('Active',True),Name = code.get('Name',''),Params = json.dumps(code.get('Params',{})),CodesHeadId = code_head_id)

                    session.add(codeItem)
                    session.flush()
                    code['id'] = codeItem.id

                    codeItemLang = codesdetail(Lang=language,Value=code.get('Value',''),CreatedBy= user.id, CreatedDateTime=datetime.now(timezone.utc),id=codeItem.id)
                    session.add(codeItemLang)
                    session.flush()
                else:
                    codeItem = session.query(CodesDetailSa,codesdetail).join(codesdetail,and_(CodesDetailSa.id==codesdetail.id,codesdetail.Lang==language)).filter(CodesDetailSa.id==code.get('id',None)).all()
                    if len(codeItem)==1:
                        codeItem[0][0].Active = code.get('Active',True)
                        codeItem[0][0].Name = code.get('Name','')
                        codeItem[0][0].Params = json.dumps(code.get('Params',{}))
                        codeItem[0][0].CodesHeadId =code_head_id
                        codeItem[0][1].Value=code.get('Value','')
                        codeItem[0][1].ChangedBy= user.id


            session.flush()
               
            #return codes to be refreshed on the client
            codeList  = GetCodesDetailsByType(code_head_id,language,'id')
            code_name = GetCodeHeadNameById(code_head_id) 
            rval={'code':codesData,'code_head_name':code_name,'codeList':codeList}

            # USPOREDIM SAVECDL I CODELIST - RAZLIKU OBRISEM (KOD BRISANJA KODOVA)
            if len(codesData) < len(codeList): # user has deleted some codes, we must also           
                deleteList = self.codes2delete(codesData, codeList)      
                objects_to_delete = session.query(codesdetail).filter(codesdetail.id.in_(deleteList))
                objects_to_delete.delete()

                objects_to_delete1 = session.query(CodesDetailSa).filter(CodesDetailSa.id.in_(deleteList))
                objects_to_delete1.delete()
            
           

            return Response(data={'data':rval, 'msg':'Codes saved successfuly'}, status=200)
        #else:
            #return Response(data={'data':None,'msg':'Save failed'}, status=500)


class AddMenuViewSet(viewsets.ModelViewSet):
   queryset = Menus.objects.all().select_related()
   serializer_class = AddMenuSerializer
   
   def create(self, request):
      
      menuData = request.data

      ams = AddMenuSerializer(data=menuData,context={'request':request})  
      if ams.is_valid(raise_exception=True):
          rVal = ams.save()
          
      if rVal:   
        return Response(status=201)
      else:
        return Response(status=400)
      
class GetPagesForUserViewSet(viewsets.GenericViewSet):
    queryset = Pages.objects.all()#.select_related('ResourceDefId').prefetch_related('Users', 'Users__groups','ResourceDefId__resourcedeflang_set')
    #serializer_class = GetPagesSerializer
   
    def get_queryset(self):
    
        if 'lang' in self.kwargs:
            lang = self.kwargs['lang']
        else:
            lang = None
         
        groups = []
        user = self.request.user
        group_qs =user.groups.all()
        for group in group_qs:
            groups.append(group.id)
    
        queryset = self.queryset.filter(Q(Users = user.id) | Q(Groups__in=groups)).select_related('PageType__CodesHeadId__OrganisationId','ResourceDefId','ResourceDefId__Status','PageType','PageType__CodesHeadId','ResourceDefId__ResourceType').prefetch_related('Users','Groups').order_by('id')
        return queryset
    
    def list(self,request,lang):

        groups = []
        user = self.request.user

        pages= []
        group_qs =user.groups.all()
        for group in group_qs:
            groups.append(group.id)
        

        return Response(data=pages,status=200)
    

class GetMenusForPageViewSet(viewsets.ModelViewSet):
   queryset = Pages.objects.all().select_related()
   serializer_class = GetMenuSerializer
   
   def get_queryset(self):
    
    if 'lang' in self.kwargs:
         lang = self.kwargs['lang']
    else:
         lang = 'en-GB'
         
    if 'page' in self.kwargs:
         page = self.kwargs['page']
    else:
         page = ''
    groups = []
    user = self.request.user
    for group in user.groups.all():
      groups.append(group.id)
    
    return Pages.objects.filter(Q(Menus__Groups__in=groups) | Q(Menus__Users=user.id),~Q(Menus__MenuType__istartswith='navbar')).select_related().order_by('id','Menus__Order').distinct()
                                #| Q(MenuType='navbar',Q(Groups__in=groups) | Q (Users=user))).select_related()

class ProcessMenuClickViewSet(viewsets.GenericViewSet):
    
    queryset = Menus
    
    def list(self, request,menuId):
      
        if 'menuId' in self.kwargs:
          menu_id=int(self.kwargs['menuId'])
        else:
          return Response(data='Menu id is missing',status=500)
        
        user=request.user
        #ErrorLog.objects.create(Error1={'Before menu':'Before menu'},Error2={'user':user.id,'menu':str(menu_id)},CreatedDateTime=timezone.now())  
        
        menu_object=None
        menu_defs = Menus.objects.filter(id = menu_id)
        menu_count=len(menu_defs)
        if menu_count!=1:
          return Response(data='Invalid definition of menu item. Found '+str(menu_count)+' menu objects',status=500)
        
        task_id=None
        for menu in menu_defs:
          menu_object = menu
        
        if menu_object is not None:
          menu_type= menu_object.MenuType
          if menu_type==2:
            #get task id
            task_id = menu_object.TargetTask_id
        else:
          return Response(data='Invalid definition of menu item. Found '+str(menu_count)+' menu objects',status=500)
        
        
        #ErrorLog.objects.create(Error1={'Before start task':'Before'},Error2={'task_id':task_id},CreatedDateTime=timezone.now())  
        if task_id is not None:
          response,response_status = StartTask(task_id,0,0,user)
          
        else:
          return Response(data='Task not found in menu definition',status=500)
        #ErrorLog.objects.create(Error1={'After start task':'After'},Error2={},CreatedDateTime=timezone.now())  
        
        return Response(data=response,status=response_status)

class GetMenusViewSet(viewsets.GenericViewSet):

   queryset = Menus.objects.all()
  
   def list(self, request,lang,type,appId):
        
        groups = []
        menus = []
        #user = self.request.user
        user = request.user
        helper = ResourceHelperClass()
        org_id,language = helper.get_lang_org(user)
        
        for group in user.groups.all():
          groups.append(group.id)
        ltype = int(type)
        lappId = int(appId)
        #pdb.set_trace()
        
        session = rdbms_cls.Session()
        with session.begin():
            menu_obj = menu(None,'menu','','','',True, user,language,org_id,{}) 
            menu_obj.set_session(session)  
            queryset=menu_obj.get_menus(user,groups,ltype,0,lappId)
            
            for item in queryset:
                itemDict={}
                for col in item.keys():
                    val = getattr(item, col)
                    try:
                        val = json.loads(val)   
                    except:
                        pass
                    
                    value = {col:val}
                    itemDict.update(value)
                menus.append(itemDict)
            
            #pdb.set_trace()
            for item in menus:
                  tmp_users_groups = get_users_groups_menus(item['katalyonode_menus_id'],org_id, user,language,session)  
                  item['Users'] = tmp_users_groups[0]
                  item['Groups'] = tmp_users_groups[1]
                    
        return Response (data=menus,status=200)


def get_users_groups_menus(menuId, org_id, user, language,session):
        

        users_list = []
        groups_list = []

        

        menu_obj = menu(menuId,'menu','','','',True,user,language,org_id,{},None)
        menu_obj.set_session(session) 
        users,groups=menu_obj.get_users_groups_menus(menuId,language)
      
        for item in users:
            val_id =  getattr(item, 'auth_user_id')
            val_fname = getattr(item, 'auth_user_first_name')
            val_lname = getattr(item, 'auth_user_last_name')
            val_username = getattr(item, 'auth_user_username')
            val_name = val_fname+' '+val_lname+' - '+val_username
            itemDict={'id':val_id,'name':val_name}
            users_list.append(itemDict)
        
        for item in groups:
            val_id =  getattr(item, 'auth_group_id')
            val_name = getattr(item, 'auth_group_name')
            itemDict={'id':val_id,'name':val_name}
            groups_list.append(itemDict)
            
        return users_list,groups_list

class GetUserGroupsMenusViewSet(viewsets.GenericViewSet):

   queryset = Menus.objects.all()

   def list(self, request,menuId,lang):
           
        session = rdbms_cls.Session()
        with session.begin():
            user = request.user
            helper = ResourceHelperClass()
            org_id,language = helper.get_lang_org(user)
            temp_user_groups = get_users_groups_menus(menuId,org_id, user, language, session)

            return Response (data={'Users':temp_user_groups[0],'Groups':temp_user_groups[1]},status=200)

#check if used    
class SaveMenuViewSet(viewsets.GenericViewSet):
   queryset = Menus.objects.all() 
   #serializer_class = GetNavbarSerializer
   
   def create(self, request,lang):
    
    user = request.user
    data = request.data
    lang = self.kwargs['lang']  if 'lang' in self.kwargs else 'en-GB'
    menu_id = data['katalyonode_menus_id'] if 'katalyonode_menus_id' in  data  else None
    users = data['Users']  if 'Users' in data else []
    groups = data['Groups']  if 'Groups' in data else [] 
    users_ids = []
    groups_ids = []
    
    if len(data)==0:
      return Response (data={'data':data,'msg':'Error --> menu data missing'},status=400)
      
    for u in users:
      users_ids.append(u['id'])
      
    users_ids.append(user.id)  #always grant current user  
    
    for g in groups:
      groups_ids.append(g['id'])
    
     #SaveMenuViewSet
    target_page = None
    if 'TargetPage' in data:     
          target_page = data['TargetPage'] if data['TargetPage'] is None else data['TargetPage']['id']
   
    if 'Target' in data:       
          target = data['Target'] if data['Target'] is None else data['Target']['id']
     
    if 'katalyonode_menus_Active' in data:
      active = data['katalyonode_menus_Active']
    else:
      active=False
    
    if 'katalyonode_menus_Icon' in data:
      icon = data['katalyonode_menus_Icon']
    else:
      icon=None 
    
    pages = Pages.objects.filter(ResourceDefId_id=target)
    if len(pages)>0:
        target_page = pages[0].id
    else:
        Response (data={'msg':'Error saving menu -> page record not found'},status=400)
    params = {'pageid':target}    
    ret_status = 200
    
    if menu_id is None:
      menu = Menus.objects.create(Name = data['katalyonode_menus_Name'],
      MenuType= data['katalyonode_menus_MenuType'],
      MenuPosition= data['katalyonode_menus_MenuPosition'],
      TargetPage_id=target_page,
      Target_id=target,
      Params= params,
      ParentItem= data['katalyonode_menus_ParentItem'],
      Navbar= data['katalyonode_menus_Navbar'],
      AlwaysNew= data['katalyonode_menus_AlwaysNew'],
      Icon= icon,
      Order= data['katalyonode_menus_Order'],
      ResourceDefId_id=1,
      Active= active,
      CreatedBy= user,
      CreatedDateTime= timezone.now())
      
      msg='Menu created successfuly'
      if not data['katalyonode_menus_Navbar']:
        menu.Navbar = menu.id
      menu.Users.set(users_ids)
      menu.Groups.set(groups_ids)
      menu.save()
    elif menu_id is not None:
      menus = Menus.objects.filter(id=menu_id)
      if len(menus)==0:
        msg='Menu with id = '+menu_id+' is not found'
        ret_status=400  
      else:
        
        msg='Changes saved successfuly'  
        for menu in menus:
          menu.Name = data['katalyonode_menus_Name']
          menu.MenuType = data['katalyonode_menus_MenuType']
          menu.TargetPage_id =target_page
          menu.Target_id =target
          menu.MenuPosition = data['katalyonode_menus_MenuPosition']
          menu.Params = params
          menu.ParentItem = data['katalyonode_menus_ParentItem']
          menu.Navbar = data['katalyonode_menus_Navbar']
          menu.AlwaysNew = data['katalyonode_menus_AlwaysNew']
          menu.Icon = icon
          menu.Users.set(users_ids)
          menu.Groups.set(groups_ids)
          menu.Order = data['katalyonode_menus_Order']
          menu.Active = active
          menu.ChangedBy = user
          menu.ChangedDateTime =timezone.now()
          menu.save()
    elif menu_id is not None:
          menus = Menus.objects.filter(id=menu_id)
          if len(menus)==0:
            msg='Menu with id = '+menu_id+' is not found'
            ret_status=400  
          else:
            
            msg='Changes saved successfuly'  
            for menu in menus:
                  menu.Name = data['katalyonode_menus_Name']
                  menu.MenuType = data['katalyonode_menus_MenuType']
                  menu.TargetPage_id =target_page
                  menu.Target_id =target
                  menu.MenuPosition = data['katalyonode_menus_MenuPosition']
                  menu.Params = params
                  menu.ParentItem = data['katalyonode_menus_ParentItem']
                  menu.Navbar = data['katalyonode_menus_Navbar']
                  menu.AlwaysNew = data['katalyonode_menus_AlwaysNew']
                  menu.Icon = icon
                  menu.Users.set(users_ids)
                  menu.Groups.set(groups_ids)
                  menu.Order = data['katalyonode_menus_Order']
                  menu.Active = active
                  menu.ChangedBy = user
                  menu.ChangedDateTime =timezone.now()
                  menu.save()
    data['katalyonode_menus_id'] = menu.id
    data['katalyonode_menus_Navbar'] = menu.Navbar
    
    return Response (data={'data':data,'msg':msg},status=ret_status)

class SaveFullMenuViewSet(viewsets.GenericViewSet):
   queryset = Menus.objects.all() 
   
   def create(self, request):
    
    user = request.user
    data = request.data
    navbarLeft = data.get('navbarItemsLeft',None)
    navbarRight = data.get('navbarItemsRight',None)
    pageMenuItems = data['sideMenuItems'] if 'sideMenuItems' in data else None
    
    menuId = data['resource']
    helper = ResourceHelperClass()

    org_id,language = helper.get_lang_org(user)
    menu_obj = None
    #pdb.set_trace()
    
    session = rdbms_cls.Session()
    with session.begin():
           
        if pageMenuItems:
            menu_obj = pagemenu(menuId,'menu','','','',True,user,language,org_id,{},None)
            menu_obj.set_session(session) 
            menu_obj.prepare_save(pageMenuItems)    
        else:
            menu_obj = menu(menuId,'menu','','','',True,user,language,org_id,{},None)
            menu_obj.set_session(session) 
            menu_obj.prepare_save(navbarLeft,navbarRight)
        msg,ret_status=menu_obj.save_full_menu()
        r_val = menu_obj.get_extended_resource_data()

        if ret_status==200:
            msg='Menus saved successfuly'
        r_val['msg']=msg
            
        #return Response(data={'data':r_val,'msg':msg},status=ret_status)
        return Response (data=r_val,status=ret_status)

#not used
"""

class SaveMenuOrderViewSet(viewsets.GenericViewSet):
   queryset = Menus.objects.all() 
   #serializer_class = GetNavbarSerializer
   
   def create(self, request,lang):
    
    if 'lang' in self.kwargs:
         lang = self.kwargs['lang']
    else:
         lang = 'en-GB'
    
    user = request.user
    order = 0
    menus = request.data
    
    for menuItem in menus:
      
      menu = Menus.objects.get(id=menuItem['katalyonode_menus_id'])
      
      if menu:
        menuItem['katalyonode_menus_Order'] = order
        menu.Order = order
        menu.save()
      order=order+1
    
    
    
    return Response (data={'data':menus,'msg':'Order of menu items saved successfuly'},status=200)

#not used  

class SaveHomePageViewSet(viewsets.GenericViewSet):
   queryset = UserExtendedProperties.objects.all() 
   #serializer_class = GetNavbarSerializer
   
   def create(self, request):
  
    user = request.user
    count = 0
    home_page_props = request.data
    ret_status=200
    
    uep = UserExtendedProperties.objects.get(UserId=user)
    
    if uep is not None:
      
      if 'HideCustomizeHome' in home_page_props:
        
        uep.HideCustomizeHome = home_page_props['HideCustomizeHome']
        #home_page_props['HideCustomizeHome'] = home_page_props['HideCustomizeHomeForSave']
        count=count+1
    
      if 'HideCreateResourceHome' in home_page_props:
        uep.HideCreateResourceHome = home_page_props['HideCreateResourceHome']
        count=count+1
      
      if 'HideSearchResourceHome' in home_page_props:
        uep.HideSearchResourceHome = home_page_props['HideSearchResourceHome']
        count=count+1
      
      if 'HideAppsHome' in home_page_props:
        uep.HideAppsHome = home_page_props['HideAppsHome']
        count=count+1
      
      
      task_list_ids = []
      if 'TaskListHome' in home_page_props:
        task_list = home_page_props['TaskListHome']
        for task in task_list:
            task_list_ids.append(task['id'])
        uep.TaskListHome = task_list_ids
        count=count+1
      else:
        uep.TaskListHome = task_list_ids
        count=count+1
      
      if count>0:
        msg = 'Your homepage is saved successfuly'
        uep.save()
      else:
        msg = 'No changes'
    else:
      status = 400
      msg = 'Current user not found'
    
    return Response (data={'data':home_page_props,'msg':msg},status=ret_status)
"""

#migrate to sqlalchemy after migration to Fast API
class GenerateAPiKeysViewSet(viewsets.GenericViewSet):
    queryset = Token.objects.all() 
    #serializer_class = GetNavbarSerializer
   
    def create(self, request):
    
        user = request.user
        api_user = request.data
    
        uuid_user = api_user['api_id']
        
        if uuid_user is None:
            org_id = request.session.get('OrganisationId')
            lang = request.session.get('UserLangId')
            org = Organisations.objects.get(id = org_id)
            
            if org.ApiUser is None:
                hasher = hashlib.sha512()
                guid = str(uuid.uuid4()).encode('utf-8')
                hasher.update(guid) 
                digest=hasher.hexdigest()
                uuid_user = str(org_id)+'-'+digest
                
                usr, created = User.objects.get_or_create(username=uuid_user)
                if created:
                    usr.set_password(uuid_user) # This line will hash the password
                    usr.is_superuser=False;
                    usr.email = ''
                    usr.first_name = 'Api'
                    usr.last_name = 'User'
                    usr.save() 
                    uep,e_created = UserExtendedProperties.objects.get_or_create(UserId_id=usr.id,Organisation = org,
                                                                         defaults={'HideCustomizeHome' : False,'HideCreateResourceHome' : False,'HideAppsHome' : False,
                                                                         'HideSearchResourceHome' : False,'Lang_id' : lang,'CreatedDateTime':timezone.now(),
                                                                         'CreatedBy_id':user.id})
                    org.ApiUser = usr
                    org.save()
        else:
            usr = User.objects.get(username=uuid_user)
        token,created = Token.objects.get_or_create(user=usr)
        
        msg='API keys generated successfully'
        if token:
            return Response (data={'api_id':uuid_user,'api_key':token.key,'msg':msg},status=200)
        else:
            msg='Error generating api keys'
            return Response (data={'api_id':uuid_user,'msg':msg},status=500)
    
def GetCodesDetailsByType(code,language,type_of_code):
        
    
    session = rdbms_cls.Session()
    codesList=[]
        
    with session.begin():

        if type_of_code=='name':  
            #codesLang = CodesDetailLang.objects.filter(CodesDetailId__CodesHeadId__CodeHeadName=code,CodesDetailId__Active=True,Lang_id=language).select_related()
            codes = session.query(CodesDetailSa,codesdetail,CodesHeadSa).join(codesdetail,and_(CodesDetailSa.id==codesdetail.id, codesdetail.Lang == language)).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).filter(CodesDetailSa.Active==True,CodesHeadSa.CodeHeadName==code).all()
                
        else:
            #codesLang = CodesDetailLang.objects.filter(CodesDetailId__CodesHeadId_id=code,CodesDetailId__Active=True,Lang_id=language).select_related()
            codes = session.query(CodesDetailSa,codesdetail,CodesHeadSa).join(codesdetail ,and_(CodesDetailSa.id==codesdetail.id,codesdetail.Lang == language)).join(CodesHeadSa,CodesDetailSa.CodesHeadId==CodesHeadSa.id).filter(CodesDetailSa.Active==True,CodesDetailSa.CodesHeadId==code).all()
        
        userId=None
        for code in codes:
            if userId is None:
                userId = code[2].UserGeneratedId
            if userId:
                code_id = code[0].Name
                aid=code[0].id
            else:
                code_id = code[0].id
                aid = code[0].Name 
            params = code[0].Params or '{}'
            codesList.append({'id':code_id,'value':aid,'name':code[1].Value,'active':code[0].Active,'params':json.loads(params)})
      
    return codesList

def GetCodeHeadNameById(code_head_id):
    
    session = rdbms_cls.Session()
        
    with session.begin():
        codes_heads = session.query(CodesHeadSa).filter(CodesHeadSa.id==code_head_id).all()
    
    if len(codes_heads)>0:
        return codes_heads[0].CodeHeadName
    else:
        return None
  
  