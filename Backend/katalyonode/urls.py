"""KatalyoTest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin
from katalyonode.views.views import printme, index,landing,howitworks

from rest_framework import routers
from katalyonode.views import views,viewssettings,viewsresources,viewstasks,viewspublic,viewsfiles,viewsblockchain
from django.views.generic import TemplateView
from katalyonode.functions.widgets.task.urls import router as task_router
from katalyonode.functions.widgets.integration.urls import router as integration_router
from katalyonode.functions.widgets.resource.urls import router as resource_router
from django.conf import settings

#handler400 = 'rest_framework.exceptions.bad_request'
#handler500 = 'katalyonode.exceptions.api_500_handler'


router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'usersextended', views.UserExtendedViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'groupsextended', views.GroupExtendedViewSet)
router.register(r'generatepdf/(?P<resourceId>.+)/(?P<presentationId>.+)/(?P<taskDefId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.TaskViewPdfViewSet, basename="ResourceDefLang")
router.register(r'resource-def', viewsresources.ResourceDefViewSet)
router.register(r'resource-def-lang', viewsresources.ResourceDefLangViewSet,basename="ResourceDefLang")
router.register(r'resource-def-by-lang/(?P<lang>.+)', viewsresources.ResourceDefByLangViewSet, basename="ResourceDef")
router.register(r'resource-def-all/(?P<resourceId>[^/.]+)', viewsresources.ResourceDefAllViewSet,basename="ResourceDef")
router.register(r'resource-def-all-new', viewsresources.ResourceDefAllViewSetNew)
router.register(r'get-resource-def-bylang/(?P<lang>.+)', viewsresources.GetResourceDefByLangViewSet,basename="ResourceDef")
router.register(r'get-resource-def-bylang-filtered/(?P<lang>.+)', viewsresources.GetResourceDefByLangFilteredViewSet,basename="ResourceDef")
router.register(r'get-resource-def-bylang-bytype/(?P<lang>.+)/(?P<resource_type>[^/.]+)', viewsresources.GetResourceDefByLangByTypeViewSet,basename="ResourceDef")
router.register(r'get-resource-def-bylang-bytype-paginated/(?P<lang>.+)/(?P<resource_type>[^/.]+)', viewsresources.GetResourceDefByLangByTypePaginatedViewSet,basename="ResourceDef")
router.register(r'get-resource-def-bytype-forwidget/(?P<lang>.+)/(?P<resource_type>[^/.]+)', viewsresources.GetResourceDefByTypeForWidgetViewSet,basename="ResourceDefLang")
router.register(r'get-related-dataset-forwidget/(?P<datasetId>.+)/(?P<lang>.+)', viewsresources.GetRelatedDatasetForWidgetViewSet,basename="PresentationElementsLang")
router.register(r'get-resource-def-list-bylang-bytype/(?P<lang>.+)/(?P<resource_type>[^/.]+)', viewsresources.GetResourceDefListByLangByTypeViewSet,basename="ResourceDefLang")
router.register(r'get-resource-def-list-bylang-bytype-filter/(?P<lang>.+)/(?P<resource_type>.+)/(?P<selectedDataset>.+)', viewsresources.GetResourceDefListByLangByTypeFilterViewSet,basename="ResourceDefLang")
router.register(r'get-resource-def-list-bylang-bytype-user/(?P<lang>.+)/(?P<resource_type>.+)/(?P<userId>.+)', viewsresources.GetResourceDefListByLangByTypeUserViewSet,basename="ResourceDefLang")
router.register(r'get-resource-def-bylang-bytype-user/(?P<lang>.+)/(?P<resource_type>.+)', viewsresources.GetResourceDefByLangByTypeUserViewSet,basename="ResourceDefLang")
router.register(r'save-resource-def', viewsresources.SaveResourceDefViewSet,basename="ResourceDefLang")
router.register(r'save-resource-def2', viewsresources.SaveResourceDefViewSet2,basename="ResourceDefLang")
router.register(r'organisations/(?P<lang>[^/.]+)', views.OrganisationsViewSet)
router.register(r'codes/(?P<lang>[^/.]+)', viewssettings.CodesViewSet,basename="CodesHead")
router.register(r'codes-head/(?P<codeId>.+)/(?P<lang>.+)', viewssettings.CodesHeadViewSet,basename="CodesHead")
router.register(r'codes-detail', viewssettings.CodesDetailViewSet,basename="CodesDetail")
router.register(r'get-codes-detail', viewssettings.GetCodesDetailViewSet,basename="CodesDetail")
router.register(r'get-codes-detail-bycodeidlang/(?P<id>[^/.]+)/(?P<lang>[^/.]+)', viewssettings.GetCodesDetailByCodeIdLangViewSet,basename="CodesDetail")
router.register(r'get-codes-detail-byidlang/(?P<id>.+)/(?P<lang>.+)', viewssettings.GetCodesDetailByIdLangViewSet,basename="CodesDetailLang")
router.register(r'save-codes-detail', viewssettings.AddUpdateCodesDetailViewSet,basename="CodesDetailLang")
router.register(r'save-codes-detail/(?P<codeHeadId>.+)/(?P<lang>.+)', viewssettings.SaveCodesDetailViewSet,basename="CodesDetail")
router.register(r'save-resource-form/(?P<formType>.+)/(?P<resourceId>.+)/(?P<lang>.+)', viewsresources.SaveResourceFormViewSet,basename="ResourceDefLang")
router.register(r'task-parameters', viewstasks.TaskParametersViewSet)
router.register(r'task-parameters-byresourceid/(?P<resourceId>.+)', viewstasks.TaskParametersByResourceIdViewSet,basename="TaskDef")
#router.register(r'task-assignment-byresourceid/(?P<resourceId>.+)', views.TaskAssignmentsByResourceIdViewSet,basename="TaskAssignmentDefUser")
router.register(r'task-assignment-byresourceid/(?P<resourceId>.+)', viewstasks.TaskAssignmentsByResourceIdViewSet,basename="TaskAssignmentDefUser")
router.register(r'save-task-assignments/(?P<taskId>.+)/(?P<assignType>.+)', viewstasks.SaveTaskAssignmentsViewSet ,basename="TaskAssignmentDefUser")
router.register(r'save-task-outcomes/(?P<taskId>.+)/(?P<lang>.+)', viewstasks.SaveTaskOutcomesViewSet)
router.register(r'get-task-outcomes-def/(?P<lang>.+)', viewstasks.GetTaskOutcomesDefViewSet)
router.register(r'save-task-codes-actions/(?P<taskId>.+)/(?P<codeHeadName>.+)', viewstasks.SaveTaskCodesActionsViewSet)
router.register(r'get-task-codes-actions-byid/(?P<taskId>.+)/(?P<lang>.+)/(?P<codeId>.+)', viewstasks.GetTaskCodesActionsByIdViewSet,basename="TaskCodesActions")
router.register(r'get-task-outcomes-details/(?P<taskId>.+)/(?P<codeId>.+)/(?P<lang>.+)', viewstasks.GetTaskOutcomesDetailsViewSet,basename="TaskCodesActions")
router.register(r'get-task-codes-actions-byname/(?P<taskId>.+)/(?P<lang>.+)/(?P<codeName>.+)', viewstasks.GetTaskCodesActionsByNameViewSet,basename="TaskCodesActions")
router.register(r'get-task-execute-data/(?P<taskInstanceId>.+)/(?P<lang>.+)', viewstasks.GetTaskExecuteDataViewSet,basename="ResourceDefLang")
router.register(r'get-resource-form/(?P<resourceId>.+)/(?P<formType>.+)/(?P<lang>.+)/(?P<version>.+)', viewsresources.GetResourceFormViewSet,basename="TaskInitiationInstance")
router.register(r'get-resource-form-with-data/(?P<resourceDefId>.+)/(?P<resourceId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceFormWithDataViewSet,basename="ResourceDefLang")
router.register(r'get-resource-form-extended/(?P<resourceId>.+)/(?P<taskDefId>.+)/(?P<formType>.+)/(?P<parentId>.+)/(?P<overrideType>.+)/(?P<lang>.+)', viewsresources.GetResourceFormExtendedViewSet,basename="ResourceDefLang")
router.register(r'get-resource-form-extended-multiple/(?P<resourceId>.+)/(?P<taskDefId>.+)/(?P<formType>.+)/(?P<parentId>.+)/(?P<lang>.+)', viewsresources.GetResourceFormExtendedMultipleViewSet,basename="ResourceDefLang")
#router.register(r'get-resource-form-extended-with-data/(?P<resourceId>.+)/(?P<taskInitiateId>.+)/(?P<taskExecuteId>.+)/(?P<populateType>.+)/(?P<parentId>.+)/(?P<parentInitiateId>.+)/(?P<prevTaskInstanceId>.+)/(?P<formType>.+)/(?P<widgetType>.+)/(?P<resourceRecordId>.+)/(?P<overrideType>.+)/(?P<lang>.+)', viewsresources.GetResourceFormExtendedWithDataViewSet,basename="ResourceDefLang")
#router.register(r'get-resource-form-extended-with-data-multiple/(?P<resourceId>.+)/(?P<taskInitiateId>.+)/(?P<taskExecuteId>.+)/(?P<populateType>.+)/(?P<parentId>.+)/(?P<parentInitiateId>.+)/(?P<prevTaskInstanceId>.+)/(?P<formType>.+)/(?P<widgetType>.+)/(?P<resourceRecordId>.+)/(?P<overrideType>.+)/(?P<parentDatasetDefId>.+)/(?P<rmdId>.+)/(?P<lang>.+)', viewsresources.GetResourceFormExtendedWithDataMultipleViewSet,basename="ResourceDefLang")
#router.register(r'get-resource-form-add-multiple/(?P<resourceId>.+)/(?P<taskInitiateId>.+)/(?P<taskExecuteId>.+)/(?P<parentId>.+)/(?P<parentInitiateId>.+)/(?P<prevTaskInstanceId>.+)/(?P<formType>.+)/(?P<widgetType>.+)/(?P<overrideType>.+)/(?P<lang>.+)', viewsresources.GetResourceFormAddMultipleViewSet,basename="ResourceDefLang")
#router.register(r'save-dataset-records-multiple/(?P<resourceId>.+)', viewsresources.SaveDatasetRecordsMultipleViewSet,basename="ResourceDef")
#router.register(r'get-resource-data/(?P<resourceId>.+)/(?P<taskDefId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataViewSet,basename="ResourceDef")
router.register(r'get-dataset-data/(?P<resourceId>.+)', viewsresources.GetDatasetDataViewSet,basename="ResourceDef")
#router.register(r'get-resource-data-model/(?P<resourceId>.+)/(?P<taskDefId>.+)/(?P<modelId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataWithModelViewSet,basename="ResourceDef")
#router.register(r'get-resource-data-paginated/(?P<resourceId>.+)/(?P<lang>.+)', viewsresources.GetResourceDataPaginatedViewSet,basename="ResourceDef")
router.register(r'get-resource-data-paginated-filter/(?P<resourceId>.+)/(?P<lang>.+)', viewsresources.GetResourceDataPaginatedFilterViewSet,basename="ResourceDef")
#router.register(r'get-resource-data-paginated-auto/(?P<resourceId>.+)/(?P<presentationId>.+)/(?P<taskInstanceId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataPaginatedAutoViewSet,basename="ResourceDef")
router.register(r'get-resource-columns/(?P<resourceId>.+)/(?P<resourceType>.+)/(?P<lang>.+)/(?P<ver>.+)', viewsresources.GetResourceColumnsViewSet,basename="ResourceDef")
#router.register(r'get-resource-data-filtered/(?P<resourceId>.+)/(?P<taskDefId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataFilteredViewSet,basename="ResourceDef")
router.register(r'get-resource-data-direct-filter/(?P<resourceId>.+)/(?P<widgetId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataDirectFilterViewSet,basename="ResourceDef")
#router.register(r'get-resource-data-related/(?P<datasetId>.+)/(?P<recordId>.+)/(?P<relatedId>.+)/(?P<taskDefId>.+)/(?P<modelId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataRelatedViewSet,basename="ResourceDef")
#router.register(r'get-resource-data-with-related/(?P<datasetId>.+)/(?P<relatedId>.+)/(?P<recordId>.+)/(?P<taskDefId>.+)/(?P<modelId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataWithRelatedViewSet,basename="ResourceDef")
router.register(r'get-resource-data-m2m/(?P<widgetId>.+)/(?P<datasetId>.+)/(?P<relatedId>.+)/(?P<recordId>.+)', viewsresources.GetResourceDataM2mViewSet,basename="ResourceDef")
router.register(r'get-resource-model-for-deployment/(?P<resourceId>.+)/(?P<version>.+)/(?P<lang>.+)', viewsresources.GetResourceModelForDeploymentViewSet,basename="PresentationElements")
#router.register(r'get-m2m-field-data/(?P<itemId>.+)/(?P<resourceId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetM2MFieldDataViewSet,basename="ResourceDef")
#router.register(r'add-related-resources/(?P<itemId>.+)/(?P<resourceId>.+)', viewsresources.AddRelatedResourcesViewSet,basename="ResourceDef")
#router.register(r'save-resource-data/(?P<resourceId>.+)', viewsresources.SaveResourceDataNewViewSet,basename="ResourceDef")
router.register(r'delete-dataset-record/(?P<resourceDefId>.+)/(?P<resourceId>.+)', viewsresources.DeleteDatasetRecordViewSet,basename="ResourceDef")
router.register(r'get-mappings-list/(?P<resourceDefId>.+)', views.getMappingsListViewSet,basename="FilesTable")
router.register(r'put-mappings-new/(?P<resourceDefId>.+)', views.putMappingsNewViewSet,basename="FilesTable")
router.register(r'put-mappings/(?P<resourceDefId>.+)/(?P<mappingsId>.+)', views.putMappingsViewSet,basename="FilesTable")
router.register(r'get-dataset-mapping-head/(?P<fromDs>.+)/(?P<toDs>.+)/(?P<lang>.+)', viewsresources.GetDatasetMappingHeadViewSet,basename="DatasetMappingHead")
router.register(r'save-dataset-mapping-head/(?P<mappingId>.+)/(?P<lang>.+)', viewsresources.SaveDatasetMappingHeadViewSet,basename="DatasetMappingHead")
router.register(r'save-dataset-transform-head/(?P<transformId>.+)/(?P<lang>.+)', viewsresources.SaveDatasetTransformHeadViewSet,basename="DatasetTransformHead")
router.register(r'get-dataset-mapping-detail/(?P<mappingId>.+)/(?P<lang>.+)', viewsresources.GetDatasetMappingDetailViewSet,basename="DatasetMappingDetail")
router.register(r'save-dataset-mapping-details/(?P<mappingId>.+)', viewsresources.SaveDatasetMappingDetailViewSet,basename="DatasetMappingDetail")
router.register(r'get-dataset-transform-head/(?P<lang>.+)', viewsresources.GetDatasetTransformHeadViewSet,basename="DatasetTransformHead")
router.register(r'get-dataset-transform-details/(?P<transformId>.+)', viewsresources.GetTransformDetailViewSet,basename="DatasetTransformDetail")
router.register(r'save-dataset-transform-details/(?P<transformId>.+)', viewsresources.SaveTransformDetailViewSet,basename="DatasetTransformDetail")
router.register(r'get-resource-fields/(?P<resourceId>.+)/(?P<lang>.+)', viewsresources.GetResourceFieldsViewSet,basename="ResourceModelDefinition")
router.register(r'import-resource/(?P<fileId>.+)/(?P<resourceId>.+)/(?P<mappingsId>.+)', views.ImportResourceViewSet,basename="FilesTable")   
router.register(r'syncdb-resource-form/(?P<resourceId>.+)/(?P<version>.+)', viewsresources.SyncdbResourceForm,basename="ResourceModelDef")
router.register(r'login', views.LoginViewSet)
router.register(r'login-sso', views.LoginSsoViewSet)
router.register(r'logout', views.LogoutViewSet)
#router.register(r'codes-headlang', views.CodesHeadLangViewSet,basename="CodesHeadLang")
router.register(r'codes-headlang/(?P<id>.+)', viewssettings.CodesHeadLangViewSet,basename="CodesHeadLang")
router.register(r'codes-list/(?P<lang>.+)', viewssettings.CodesListViewSet,basename="CodesHeadLang")
router.register(r'get-codes-by-name/(?P<codeName>.+)/(?P<lang>.+)', viewssettings.GetCodesByNameViewSet,basename="CodesDetail")
router.register(r'codes-detail-list/(?P<codeId>.+)/(?P<lang>.+)', viewssettings.CodesDetailListViewSet,basename="CodesDetail")
router.register(r'download-file/(?P<resourceId>.+)/(?P<fileId>.+)', viewsfiles.DownloadFileViewSet,basename="ResourceDef")
router.register(r'files/(?P<widgetId>.+)/(?P<fileDefId>.+)/(?P<datasetDefId>.+)/(?P<datasetRecordId>.+)', viewsfiles.FilesViewSet,basename="FilesTable")
#router.register(r'codes-headlang', views.CodesHeadLangViewSet,basename="CodesHeadLang")
#router.register(r'codes-detail/(?P<id>.+)', views.CodesDetailViewSet,basename="CodesDetail")
#router.register(r'codes-detail/(?P<lang>.+)', views.CodesDetailViewSet,basename="CodesDetail")
router.register(r'codes-detaillang/(?P<id>.+)', viewssettings.CodesDetailLangViewSet,basename="CodesDetailLang")
router.register(r'pre-initiate-task/(?P<taskDefId>.+)/(?P<prevTaskId>.+)/(?P<lang>.+)', viewstasks.PreInitiateTaskViewSet,basename="ResourceDef")
router.register(r'initiate-task/(?P<taskDefId>.+)/(?P<transactionId>.+)/(?P<datasetId>.+)/(?P<datasetRecordId>.+)', viewstasks.InitiateTaskViewSet,basename="ResourceDef")
router.register(r'pre-execute-task/(?P<taskInitiateId>.+)/(?P<taskExecuteId>.+)/(?P<lang>.+)', viewstasks.PreExecuteTaskViewSet,basename="ResourceDef")
router.register(r'task-status-action/(?P<taskInitiateId>.+)/(?P<taskExecuteId>.+)/(?P<action>.+)/(?P<language>.+)/(?P<forGroup>.+)', viewstasks.TaskStatusActionViewSet,basename="TaskInitiationInstance")
router.register(r'execute-task/(?P<taskExecuteId>.+)/(?P<language>.+)', viewstasks.ExecuteTaskViewSet,basename="ResourceDef")
router.register(r'process-task/(?P<itemId>.+)/(?P<outcomeId>.+)/(?P<idType>.+)/(?P<previousTaskId>.+)/(?P<lang>.+)', viewstasks.ProcessTaskViewSet,basename="TaskDef")
router.register(r'process-menu-click/(?P<menuId>.+)', viewssettings.ProcessMenuClickViewSet,basename="Menus")
router.register(r'start-task/(?P<taskId>.+)/(?P<outcomeId>.+)/(?P<previousTaskId>.+)', viewstasks.StartTaskViewSet,basename="TaskParameters")
router.register(r'start-public-task/(?P<orgId>.+)/(?P<taskName>.+)/(?P<taskId>.+)/(?P<outcomeId>.+)/(?P<previousTaskId>.+)', viewstasks.StartPublicTaskViewSet,basename="TaskParameters")
router.register(r'get-initiated-tasks/(?P<type>.+)/(?P<filter>.+)/(?P<lang>.+)', viewstasks.GetInitiatedTasksViewSet,basename='TaskInitiationInstance')
router.register(r'get-initiated-tasks-filtered/(?P<searchType>.+)/(?P<filter>.+)/(?P<lang>.+)', viewstasks.GetInitiatedTasksFilteredViewSet,basename='TaskInitiationInstance')
router.register(r'get-form-widgets/(?P<resourceId>.+)/(?P<taskId>.+)/(?P<presentationId>.+)/(?P<srcType>.+)/(?P<lang>.+)/(?P<version>.+)', viewstasks.GetFormWidgetsViewSet,basename="PresentationElements")
router.register(r'save-task-form-data/(?P<taskExecuteId>.+)', viewstasks.SaveTaskFormDataViewSet,basename="TaskExecutionInstances")
router.register(r'save-task-to-resource/(?P<taskExecuteId>.+)/(?P<datasetDefId>.+)/(?P<datasetRecordId>.+)/(?P<presentationId>.+)', viewstasks.SaveTaskToResourceViewSet,basename="TaskExecutionToResource")
router.register(r'save-grid-state/(?P<taskDefId>.+)/(?P<presentationId>.+)/(?P<lang>.+)', viewstasks.SaveGridStateViewSet,basename="PresentationElementsLang")
router.register(r'get-menus-for-page/(?P<page>.+)/(?P<lang>.+)', viewssettings.GetMenusForPageViewSet,basename="Menus")
router.register(r'get-menus/(?P<lang>.+)/(?P<type>.+)/(?P<appId>.+)', viewssettings.GetMenusViewSet)
router.register(r'get-users-groups-menus/(?P<menuId>.+)/(?P<lang>.+)', viewssettings.GetUserGroupsMenusViewSet)
router.register(r'get-pages-for-user/(?P<lang>.+)', viewssettings.GetPagesForUserViewSet,basename="Pages")
router.register(r'task-outcome-definition/(?P<outcomeDefinitionId>.+)/(?P<lang>.+)', viewstasks.TaskOutcomeDefinitionViewSet)
router.register(r'get-widget-dependencies/(?P<taskDefId>.+)/(?P<presentationId>.+)/(?P<formType>.+)', viewstasks.GetWidgetDependenciesViewSet,basename="TaskDefWidgetDependencies")
router.register(r'save-menu/(?P<lang>.+)', viewssettings.SaveMenuViewSet,basename="Menus")
router.register(r'save-full-menu', viewssettings.SaveFullMenuViewSet,basename="Menus")
#router.register(r'save-menu-order/(?P<lang>.+)', viewssettings.SaveMenuOrderViewSet,basename="Menus")
router.register(r'save-page-def/(?P<lang>.+)', viewsresources.SavePageDefViewSet,basename="Pages")
router.register(r'task-outcome-definition/(?P<outcomeDefinitionId>.+)/(?P<lang>.+)', viewstasks.TaskOutcomeDefinitionViewSet)
router.register(r'get-form-fields/(?P<resourceId>.+)/(?P<lang>.+)/(?P<ver>.+)', viewsresources.GetFormFieldsViewSet,basename="PresentationElements")
router.register(r'get-form-fields-by-type/(?P<resourceId>.+)/(?P<relatedId>.+)/(?P<elementType>.+)/(?P<lang>.+)', viewsresources.GetFormFieldsByTypeViewSet)
#router.register(r'get-resource-data-list/(?P<resourceId>.+)/(?P<presentationId>.+)/(?P<lang>.+)', viewsresources.GetResourceDataListViewSet)
router.register(r'get-resource-data-list-all/(?P<resourceId>.+)/(?P<presentationId>.+)/(?P<lang>.+)', viewsresources.GetResourceDataListAllViewSet)
#router.register(r'get-resource-data-pandas/(?P<resourceId>.+)/(?P<presentationId>.+)/(?P<taskId>.+)/(?P<formType>.+)/(?P<lang>.+)', viewsresources.GetResourceDataPandasViewSet)
router.register(r'save-task-btns/(?P<resourceId>.+)/(?P<lang>.+)', viewstasks.SaveTaskBtnsViewSet,basename="ResourceDefLang")
#router.register(r'save-home-page', viewssettings.SaveHomePageViewSet,basename="UserExtendedProperties")
router.register(r'save-group', views.SaveGroupViewSet,basename="GroupExtendedProperties")
router.register(r'save-user', views.SaveUserViewSet,basename="UserExtendedProperties")
router.register(r'signup', viewspublic.Signup,basename="Signup")
router.register(r'check-signup', viewspublic.CheckSignup,basename="Signup")
router.register(r'register', viewspublic.Register,basename="Signup")
router.register(r'change-password-request', views.ChangePasswordRequestViewSet,basename="UserExtendedProperties")
router.register(r'reset-password-request/(?P<userId>.+)', views.ResetPasswordRequestViewSet,basename="UserExtendedProperties")
router.register(r'check-change-password', views.CheckChangePasswordViewSet,basename="UserExtendedProperties")
router.register(r'change-password', views.ChangePasswordViewSet,basename="UserExtendedProperties")
router.register(r'change-password-with-auth', views.ChangePasswordWithAuthViewSet,basename="UserExtendedProperties")
router.register(r'eos-katalyo-hello', viewsblockchain.EosKatalyoHello,basename="FilesToResource")
router.register(r'cookies/(?P<eid>\w+|)', viewspublic.CookiesView,basename="Cookies")
router.register(r'resource-subscriptions/(?P<subscriptionType>.+)', viewsresources.ResourceSubscriptionsViewSet,basename="ResourceSubscriptions")
router.register(r'get-resource-params', viewsresources.GetResourceParamsViewSet,basename="ResourceParams")
router.register(r'get-system-pages', viewsresources.GetSystemPagesViewSet,basename="Pages")
router.register(r'save-resource-def-parameters/(?P<resourceId>.+)', viewsresources.SaveResourceDefParametersViewSet)
router.register(r'send-notification/(?P<resourceId>.+)', viewsresources.SendNotificationViewSet)
#router.register(r'saveNewCode', views.SaveNewCodeViewSet)
router.register(r'upload-file/(?P<widgetId>.+)', viewsfiles.UploadFileViewSet,basename="FilesTable")
router.register(r'file-meta-data-update', viewsfiles.FileMetaDataUpdateViewSet,basename="FilesTable")
router.register(r'generate-api-keys', viewssettings.GenerateAPiKeysViewSet,basename="Token")
router.register(r'get-blockchain-dataset-records', viewsblockchain.GetBlockchainDatasetRecordsViewSet,basename="ResourceDef")
router.register(r'get-blockchain-transaction', viewsblockchain.GetBlockchainTransactionViewSet,basename="ResourceDef")
router.register(r'execute-blockchain-function', viewsblockchain.ExecuteBlockchainFunctionViewSet,basename="ResourceDef")
router.register(r'verify-file', viewsfiles.VerifyFileViewSet,basename="FilesTable")
router.register(r'get-ktlyo-balance', viewsblockchain.GetKtlyoTokenBalanceViewSet,basename="ResourceDef")
router.register(r'save-eth-account', viewsblockchain.SaveEthAccountViewSet,basename="Organisation")
router.register(r'get-contract-public-vars', viewsblockchain.GetContractPublicVarsViewSet,basename="ResourceDef")
router.register(r'save-resource-links', viewsresources.SaveResourceLinksViewSet,basename="ResourceLinks")
router.register(r'get-widget-toolbox/(?P<resourceType>.+)', viewsresources.GetWidgetToolboxViewSet,basename="ResourceWidgetLinks")
router.register(r'published-resources/(?P<resourceId>.+)/(?P<publishType>.+)', viewsresources.PublishedResourcesViewSet,basename="PublishedResources")
router.register(r'get-published-resources/(?P<resourceType>.+)/(?P<publishType>.+)', viewsresources.GetPublishedResourcesViewSet,basename="PublishedResources")
router.register(r'get-published-resources-by-type-id/(?P<resourceId>.+)/(?P<publishType>.+)', viewsresources.GetPublishedResourcesByTypeIdViewSet,basename="PublishedResources")
router.register(r'get-published-resource-by-id/(?P<publishId>.+)', viewsresources.GetPublishedResourceByIdViewSet,basename="PublishedResources")
router.register(r'get-task-view-resource/(?P<widgetId>.+)/(?P<taskDefId>.+)/(?P<taskInstanceId>.+)/(?P<formType>.+)/(?P<datasetDefId>.+)/(?P<datasetRecordId>.+)', viewstasks.GetTaskViewResourceViewSet,basename="TaskExecutionInstanceToResource")
router.register(r'get-task-update-resource/(?P<widgetId>.+)/(?P<taskDefId>.+)/(?P<taskInstanceId>.+)/(?P<formType>.+)/(?P<datasetDefId>.+)/(?P<datasetRecordId>.+)', viewstasks.GetTaskUpdateResourceViewSet,basename="TaskExecutionInstanceToResource")
router.register(r'get-existing-resource-relationships/(?P<resourceId>.+)', viewsresources.GetExistingResourceRelationshipsViewSet,basename="ResourceDef")

router.registry.extend(task_router.registry)
router.registry.extend(integration_router.registry)  
router.registry.extend(resource_router.registry)  


# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.

urlpatterns = [
    url(r'^$', index,name='index'),
    url(r'^printme', printme,name='printme'),# TODO - NE radi 
    url(r'^landing/', landing,name='landing'),
    url(r'^web',  TemplateView.as_view(template_name='landing.html'), name='about'),
    url(r'^how-it-works/', howitworks,name='how-it-works'),
    url(r'^admin/', admin.site.urls),#,namespace='django_admin')),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/', include(router.urls)),
  ]

#if settings.DEBUG:
  #import debug_toolbar
  #urlpatterns = [
   #     url(r'^__debug__/', include(debug_toolbar.urls)),
    #] + urlpatterns
