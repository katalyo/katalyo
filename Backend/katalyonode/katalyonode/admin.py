from django.contrib import admin

# Register your models here.
from .models.models import ResourceDef, ResourceDefLang, ResourceModelDefinition, Organisations, Addresses

class OrganisationsAdmin(admin.ModelAdmin):
    list_display = ('pk','Name',)

admin.site.register(ResourceDef)
admin.site.register(ResourceDefLang)
admin.site.register(ResourceModelDefinition)
admin.site.register(Organisations,OrganisationsAdmin)
admin.site.register(Addresses)